# Shining

This is the repository for our renderer Shining. It has been used in production to render seasons 2, 3 and 4 of the [Rabbid's Invasion TV Series](https://www.youtube.com/channel/UC7KswdJ3yn5yzqUU4wWQHNg).

**Disclaimer**: This project is archived: development has been stopped in April 2019 and is open sourced only for reference and sharing. **We provide no support on it** and you probably need to make some adjustments if you want to compile and run Shining.

## Repository structure

-   scripts: cmake scripts and linux/windows bash/bat scripts
-   lib: source code for shining library and 3rd party libraries (both precompiled or sourced)
-   tools: source code for executables shib (shining batch), shid (shining daemon), shiv (shining viewer)
-   shaders: source code for our OSL shaders
-   templates: template files that are filed with NLTemplate library (such as html report)

## Building Shining

Folders scripts/linux and scripts/windows contain scripts to build Shining on supported architectures. These scripts just call cmake commands with the following paths:

-   The folder "build" will contain the build solution (visual studio solution on windows and makefiles on windows).
-   The folder "dist" will contain the installed version of the software (runtime libraries, executables and compiled shaders). Both Release and Debug versions can be built and installed in their own directory under "dist".

For Linux, the created solution has its configuration set with CMAKE_BUILD_TYPE. It is necessary to reconfigure cmake to switch between Debug and Release solution.

It should be ok to change build and dist directories (putting them out of source directories) by changing build scripts on running cmake commands manually with correct arguments.

## Supported architectures

At Ubisoft Motion Pictures, we work on the following architectures:

-   Windows 10 with Visual Studio 2015 x64 Intel Xeon CPU
-   Linux CentOS 7 with gcc 6.4 x64 Intel Xeon CPU

We do not guarantee that our code compile or work on other architectures and its up to you to check. On a near future we plan to upgrade to GCC 8+ and Visual Studio 2017 with no backward compatibility support.
