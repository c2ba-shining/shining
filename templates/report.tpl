<html>
<head>
  <title>Shining Statistics Report</title>

  <style type="text/css">
  <!--
  body {
    width: 100%;
    font-family: "Arial", sans-serif;
    color: white;
    background-color: rgba(40, 40, 40, 1);
    padding: 0px;
    margin: 0px;
  }
  #title {
    margin:0px;
    padding: 5px;
    background: #181818;
  }
  #content {
    margin-left: 10px;
  }
  #renderFrameChartDiv {
    background-color: rgba(40, 40, 40, 1);
    color: white;
    width: 10px;
    height: 100%;
    float:right;
    position: fixed !important;
    right: 0%;
    padding-left: 10px;
    border-left-style: solid;
    border-left-width: 2px;
    border-left-color: #999999;
    display: block;
    top: 0px;
    bottom: 0px;
    padding-top: 5px;
    display:none;
  }
  #renderFrameChartDiv canvas {
    padding-top: 100px;
  }
  #renderFrameChartDivCheck {
    top: 5px;
    right: 5px;
    position: fixed !important;
  }
  #buttonStackDiv {
    margin-right: 70px;
  }
  #buttonStack button {
    margin-left: 5px;
    margin-bottom: 5px;
    border: none; /* Remove borders */
    padding: 7px 14px; /* Add some padding */
    cursor: pointer; /* Add a pointer cursor on mouse-over */
    background-color: rgba(56,56,56,1);
    color: white;
  }
  #buttonStack button:hover {
    background: rgba(100,100,100,1);
  }
  tr.title {
    text-align: center;
    font-weight: bold;
  }
  .number {
    text-align: right;
    padding-left: 20px;
  }
  td {
    padding-right:5px;
  }
  .clear {
    clear: both;
  }
  .costPanel {
    float: left;
    margin: 10px;
    width: 40%;
  }
  .costPanel img {
    width: 100%;
    border-style: solid;
    border-width: 1px;
  }
  .costPanel table {
    width: 50%;
    margin-left: 25%;
  }
  .info_name {
    font-weight: bold;
  }
  #perf_images {
    display: {{ perfImagesDisplayMode }};
  }
  /* The switch - the box around the slider */
  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
  }
  .ic_cell {
    width: 16px;
    height: 16px;
    background-color: #404040;
  }
  .footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    color: white;
    text-align: center;
    border-top-style: solid;
    border-top-width: 2px;
    border-top-color: #999999;
    background-color: rgba(40, 40, 40, 1);
  }
  /* Hide default HTML checkbox */
  .switch input {display:none;}
  /* The slider */
  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
  }
  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
  }
  input:checked + .slider {
    background-color: #555;
  }
  input:focus + .slider {
    box-shadow: 0 0 1px #555;
  }
  input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
  }
  /* Rounded sliders */
  .slider.round {
    border-radius: 34px;
  }
  .slider.round:before {
    border-radius: 50%;
  }
  /* Tooltip container */
  .tooltip {
    cursor:pointer;
  }

  /* Tooltip text */
  .tooltip .tooltiptext {
    visibility: hidden;
    background-color: #555;
    color: #fff;
    text-align: center;
    padding: 10px;
    border-radius: 6px;

    /* Position the tooltip text */
    position: absolute;
    z-index: 1;
    /*bottom: 125%;
    left: 50%;*/
    left: 0px;
    display: block;
  }

  /* Show the tooltip text when you mouse over the tooltip container */
  .tooltip:hover .tooltiptext {
    visibility: visible;
  }

  .clickable {
    cursor:pointer;
  }
  -->
  </style>

  <script type="text/javascript">
    var report = {{ report }};
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
</head>
<body>
  <div id="renderFrameChartDiv">
    <div id="buttonStackDiv"><span id="buttonStack"><button>renderFrame</button></span></div>
    <canvas id="renderFrameChart"></canvas>
  </div>

  <div id="renderFrameChartDivCheck">
    <label class="switch">
      <input type="checkbox">
      <span class="slider round"></span>
    </label>
  </div>

  <h1 id="title">Shining Statistics Report</h1>

  <div id="content">

    <h1 id="informations">Informations</h1>

    <p>
      <ul>
        <li><span class="info_name">shib command line</span>: {{ shibCommandLine }}</li>
        <li><span class="info_name">shining version</span>: {{ shiningVersion }}</li>
        <li><span class="info_name">shining description</span>: {{ shiningDescription }}</li>
        <li><span class="info_name">vertigo version</span>: {{ vertigoVersion }}</li>
        <li><span class="info_name">VDB filename</span> : {{ vdbFilename }}</li>
      </ul>
    </p>

    <h1 id="statistics">Statistics</h1>
    <h2>Shining Batch Overview</h2>
    <table id="overviewTable">
      <tr class="title"><td>Name</td><td>Value</td><td>Ratio</td><td>Units</td></tr>
    </table>

    <h2>Scene</h2>
    <table id="sceneTable">
      <tr class="title"><td>Name</td><td>Value</td><td>Ratio</td><td>Units</td></tr>
    </table>

    <h2>Renderer</h2>
    <table id="rendererTable">
      <tr class="title"><td>Name</td><td>Value</td><td>Ratio</td><td>Units</td></tr>
    </table>

    <h3>RenderFrame</h3>
    <div>
      <div>
        <div>
          <label>Min Global Ratio:</label> <input id="renderFrameGlobalRatioThreshold" class="number" min="0" max="100" value="0" />
        </div>
        <table id="renderFrameTable">
          <tr class="title"><td>Name</td><td>Value</td><td>Ratio</td><td>Global Ratio</td><td>Units</td></tr>
        </table>
      </div>
    </div>

    <div class="clear"></div>

    <div id="perf_images">
      <h1 id="shading_cost">Shading Cost</h1>
      <div class="costPanel"><h2>Total</h2>
        <img src="data:image/png;base64,{{ totalShadingTime }}" alt="totalShadingTime.png" />
        <table id="totalShadingTable">
          <tr class="title"><td>Name</td><td>Value (usecs)</td></tr>
        </table>
      </div>
      <div class="costPanel"><h2>Average</h2>
        <img src="data:image/png;base64,{{ avgShadingTime }}" alt="avgShadingTime.png" />
        <table id="avgShadingTable">
          <tr class="title"><td>Name</td><td>Value (usecs)</td></tr>
        </table>
      </div>
      <div class="clear"></div>

      <h1 id="shadow_cost">Shadow Cost</h1>
      <div class="costPanel"><h2>Total</h2>
        <img src="data:image/png;base64,{{ totalShadowTime }}" alt="totalShadowTime.png" />
        <table id="totalShadowTable">
          <tr class="title"><td>Name</td><td>Value (usecs)</td></tr>
        </table>
      </div>
      <div class="costPanel"><h2>Average</h2>
        <img src="data:image/png;base64,{{ avgShadowTime }}" alt="avgShadowTime.png" />
        <table id="avgShadowTable">
          <tr class="title"><td>Name</td><td>Value (usecs)</td></tr>
        </table>
      </div>
      <div class="clear"></div>
    </div>

    <h1 id="texture_log">Texture Log</h1>
    <div id="texture_log_content">
    </div>

    <h1 id="osl_log">OSL Log</h1>
    <div id="osl_log_content">
    </div>

    <h1 id="lights_report">Lights</h1>
    <h2>Attributes</h2>
    <div id="lights_report_content">
    </div>

    <h2>Stats</h2>
    <div id="light_stats_report_content">
    </div>

    <div id="importance_caching_report_container">
      <h2>Importance Caching</h2>
      <div id="light_importance_caching_content">
        <table id="light_importance_caching_table">
        </table>

        <div id="light_importance_caching_tile_stats">
        </div>
      </div>

      <div style="width:100%">
          <canvas id="icTileChart"></canvas>
      </div>

      <table style="width:100%">
          <tr>
              <td style="width:50%" >
                  <canvas id="icFChart"></canvas>
              </td>
              <td style="width:50%" >
                  <canvas id="icTChart"></canvas>
              </td>
          </tr>
          <tr>
              <td style="width:50%" >
                  <canvas id="icUChart"></canvas>
              </td>
              <td style="width:50%" >
                  <canvas id="icCChart"></canvas>
              </td>
          </tr>
      </table>
    </div>

  </div>
</body>

<script>
  function completeStatsTree(tree, parent, fullName)
  {
    var sum = 0;
    var units = undefined;
    var childrenCount = 0;
    for (i in tree)
    {
      if (typeof tree[i] === 'object')
      {
        var child = completeStatsTree(tree[i], tree, fullName ? fullName + "_" + tree[i]["name"] : tree[i]["name"]);

        sum += child["value"];

        if (childrenCount == 0) {
          units = child["units"];
        }
        else if (units != child["units"]) {
          units = undefined;
        }

        ++childrenCount;
      }
    }

    if (tree["name"] === undefined)
    {
      tree["value"] = undefined;
      tree["units"] = undefined;
    }
    else if (typeof(tree["value"]) === 'undefined') // if no value recorded for this node, affect the sum
    {
      tree["value"] = sum;
      tree["overrided"] = true;
      tree["units"] = units;
    }
    else
    {
      tree["overrided"] = false;

      if (childrenCount != 0)
      {
        var remainingTime = tree["value"] - sum;
        tree["other"] = { "value": remainingTime, "name": "other", "overrided": true, "units": tree["units"], "childrenCount": 0, "fullName": fullName + "_other", "parent": tree };
        ++childrenCount;
      }
    }

    tree["childrenCount"] = childrenCount;
    tree["parent"] = parent;

    return tree;
  }

  function buildStatsTree(list)
  {
    var tree = {}
    for(var i in list)
    {
      var tokens = list[i].name.split("_");
      var units = tokens[tokens.length - 1];
      tokens.splice(tokens.length - 1, 1)

      var current = tree;
      for (var j in tokens)
      {
        if(typeof(current[tokens[j]]) === 'undefined')
        {
          var fullNameList = [];
          for (var k = 0; k <= j; ++k)
            fullNameList.push(tokens[k]);
          current[tokens[j]] = { "name": tokens[j], "fullName": fullNameList.join("_") };
        }
        current = current[tokens[j]];
      }

      current["value"] = list[i].value;
      current["units"] = units;
    }

    completeStatsTree(tree, undefined, tree["name"]);

    return tree;
  }

  function visitStatsTree(tree, visitTreeData, currentDepth = 0)
  {
    var visitChildren = true;
    if (tree["name"] !== undefined && visitTreeData["enterNode"] !== undefined)
    {
      var result = visitTreeData["enterNode"](tree, currentDepth);
      if (result !== undefined && result == false)
        visitChildren = false;
    }

    if (visitChildren && (visitTreeData["maxDepth"] === undefined || currentDepth < visitTreeData["maxDepth"]))
    {
      for (i in tree)
      {
        if (i != "parent" && typeof tree[i] === 'object')
          visitStatsTree(tree[i], visitTreeData, tree["name"] !== undefined ? currentDepth + 1 : currentDepth);
      }
    }

    if (tree["name"] !== undefined && visitTreeData["leaveNode"] !== undefined)
      visitTreeData["leaveNode"](tree, currentDepth);
  }

  function detachChild(tree, childName)
  {
    var childTree = tree[childName];
    childTree["parent"] = undefined;
    tree[childName] = undefined;
    return childTree;
  }

  function valueToString(value) 
  {
    if(typeof value === 'string' || value instanceof String) {
      return value;
    }

    if(typeof value === 'number' || value instanceof Number) {
      if(value > 0 && value < 100) {
        value = value.toFixed(3)
      }
      else {
        value = value.toFixed(0)
      }
      return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '&nbsp;')
    }

    return value;
  }

  function fillTable(instances, paramName, paramIdx, tableElem)
  {
    instances.sort(function (a, b) { return (b.hasOwnProperty(paramName) ? b[paramName][paramIdx] : 0) - (a.hasOwnProperty(paramName) ? a[paramName][paramIdx] : 0); });
    for(var i=0; i<Math.min(instances.length, 5); ++i) {
      var inst = instances[i];
      tableElem.append('<tr><td>'+inst.name+'</td><td class="number">'+valueToString(inst.hasOwnProperty(paramName) ? inst[paramName][paramIdx] : 0)+'</td></tr>');
    }
  }

  function fillTableFromStatsTree(tree, tableElem, className, minGlobalRatio)
  {
    visitStatsTree(tree,
    {
      "globalRatios": [],
      "enterNode": function(tree, depth)
      {
        var ratio = 1.0;

        var parent = tree["parent"];
        if (parent === undefined) 
        {
          // The root has a 100% ratio
          dom += '<td style="'+ colorStyleAttr + '" class="number">' + (ratio * 100).toFixed(2) + ' %</td>'
        }
        else 
        {
          // Compute ratio from parent value
          if (parent["value"] !== undefined && parent["value"] > 0 && tree["value"] > 0)
          {
            ratio = tree["value"] / parent["value"];
            dom += '<td style="'+ colorStyleAttr + '" class="number">' + (ratio * 100).toFixed(2) + ' %</td>';
          }
          else if (parent["value"] === undefined)
          {
            ratio = 1.0;
            dom += '<td style="'+ colorStyleAttr + '">' + (ratio * 100).toFixed(2) + '</td>';
          }
          else
          {
            ratio = 0.0;
            dom += '<td style="'+ colorStyleAttr + '"></td>'; // Don't display zero ratio fields
          }
        }

        var globalRatio = ratio;
        if (this.globalRatios.length > 0)
          globalRatio *= this.globalRatios[this.globalRatios.length - 1];

        this.globalRatios.push(globalRatio);

        if (globalRatio < minGlobalRatio)
          return false; // Stop the visit

        var styleAttr = 'padding-left:' + (10 + depth * 20) + 'px;'
        var lighter = 255;
        var darkest = 70;
        var maxDepth = 6;
        var depthScale = Math.min(depth, maxDepth) / maxDepth;
        var greyValue = Math.floor(lighter * (1 - depthScale) + depthScale * darkest);
        var textColor = (greyValue < 128) ? 255 : 0;
        var colorStyleAttr = 'background-color:rgba(' + greyValue + ',' + greyValue + ',' + greyValue + ',1);color:rgba(' + textColor + ',' + textColor + ',' + textColor + ',1);';
        var dom = '<tr id="' + tree["fullName"] + '" class="' + className + '"><td style="' + styleAttr + colorStyleAttr + '">' + tree["name"] + (tree["overrided"] ? ' *' : '')+'</td><td style="'+ colorStyleAttr + '" class="number">'+valueToString(tree["value"])+'</td>'

        if (ratio > 0) {
          dom += '<td style="'+ colorStyleAttr + '" class="number">' + (ratio * 100).toFixed(2) + ' %</td>';
        }
        else {
          dom += '<td style="'+ colorStyleAttr + '"></td>'; // Don't display zero global ratio fields
        }

        if (globalRatio > 0) {
          dom += '<td style="'+ colorStyleAttr + '" class="number">' + (globalRatio * 100).toFixed(2) + ' %</td>';
        }
        else {
          dom += '<td style="'+ colorStyleAttr + '"></td>'; // Don't display zero global ratio fields
        }

        dom += '<td style="'+ colorStyleAttr + '" class="number">' + tree["units"] + '</td>';

        tableElem.append(dom);
      },
      "leaveNode": function(tree, depth) {
        this.globalRatios.pop();
      }
    });

    $('.' + className).children().each(function(i) {
      $(this)[0].originalColor = $(this).css("background-color");
    })
  }

  var seed = 1;
  function random() 
  {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
  }

  function buildChartData(tree)
  {
    dataSet =
    {
      data: [],
      backgroundColor: []
    }
    labels = []
    seed = 1;
    visitStatsTree(tree,
    {
      "enterNode": function(tree, depth)
      {
        if (((tree["childrenCount"] == 0) && depth == 0) || depth == 1)
        {
          dataSet.data.push(tree.value);
          dataSet.backgroundColor.push('rgb(' + Math.floor(random() * 255) + ', ' + Math.floor(random() * 255) + ', ' + Math.floor(random() * 255) + ')');
          labels.push(tree.name);
        }
      },
      "maxDepth": 1
    });

    return { datasets: [ dataSet ], labels: labels };
  }
  
  var rendererTree = buildStatsTree(report["renderer_report"].report);
  var renderFrameTree = detachChild(rendererTree, "renderFrame");
  var sceneTree = buildStatsTree(report["scene_report"].report);
  var shibTree = buildStatsTree(report["shib_report"].report);
  var chartTree = renderFrameTree;

  var config = 
  {
      // The type of chart we want to create
      type: 'pie',

      stack: [ [ chartTree["name"], chartTree ] ],

      currentDepth: 0,

      // The data for our dataset
      data: buildChartData(chartTree),

      // Configuration options go here
      options: 
      {
        onClick: function(e, a)
        {
          if (a.length == 0)
            return;

          var name = config.data.labels[a[0]._index]; // Name of te child to display
          var newTree = config.stack[config.currentDepth][1][name]; // Tree to display

          if (typeof newTree === 'undefined')
            return;

          config.unselectStackElements();

          // If the user has clicked to another child than the next in the stack, empty the stack up to where we are now
          if ((config.currentDepth + 1) < config.stack.length && config.stack[config.currentDepth + 1][0] != name)
          {
            config.stack.splice(config.currentDepth + 1, config.stack.length - (config.currentDepth + 1));
          }

          // Display new stats
          config.setTree(newTree, config.currentDepth + 1);

          // Update stack
          if (config.stack.length == config.currentDepth)
            config.stack.push([ name, newTree ]);

          config.updateButtonStack();
          config.selectStackElements();
        },
        legend:
        {
          position: 'bottom'
        },
        title:
        {
          display: true,
          text: chartTree["name"]
        },
        animation:
        {
          duration: 0
        } 
      },

      unselectStackElements: function()
      {
        for (var i in config.stack)
        {
          $("#" + config.stack[i][1]["fullName"]).children("td").each(function(i) {
            $(this).css("background-color", $(this)[0].originalColor);
          });
        }
        $("#" + config.stack[config.currentDepth][1]["fullName"]).children("td").each(function(i) {
          $(this).css("background-color", $(this)[0].originalColor);
        });
      },

      selectStackElements: function()
      {
        for (var i in config.stack) {
          $("#" + config.stack[i][1]["fullName"]).children().css("background-color", "#5DAFC6");
        }
        $("#" + config.stack[config.currentDepth][1]["fullName"]).children().css("background-color", "#5DC678");
      },

      setTree: function(tree, depth)
      {
        var newData = buildChartData(tree);
        config.data = newData;
        config.currentDepth = depth;
        config.options.title.text = tree["name"];
        window.myDoughnut.update();
      },

      setFromFullName: function(fullName)
      {
        config.unselectStackElements();

        var foundIdx = -1;
        for (var i in config.stack)
        {
          if (config.stack[i][1]["fullName"] == fullName)
          {
            foundIdx = i;
            break;
          }
        }

        if (foundIdx >= 0)
        {
          config.setTree(config.stack[foundIdx][1], foundIdx);
        }
        else
        {
          var elements = fullName.split("_");
          elements.splice(0, 1);
          var current = config.stack[0];
          config.stack.splice(1, config.stack.length - 1);
          for (var i in elements)
          {
            var newElmt = [ elements[i], current[1][elements[i]] ]
            config.stack.push(newElmt)
            current = newElmt;
          }

          config.setTree(current[1], config.stack.length - 1);
        }

        config.updateButtonStack();
        config.selectStackElements();
      },

      updateButtonStack: function()
      {
        var buttonStack = document.getElementById('buttonStack');
        buttonStack.innerHTML = "";

        for (var i in config.stack)
        {
          var newButton = document.createElement("button");
          newButton.innerHTML = config.stack[i][0];
          newButton.tree = config.stack[i][1];
          newButton.buttonDepth = i;
          newButton.addEventListener('click', function() {
            config.unselectStackElements();
            config.setTree(this.tree, parseInt(this.buttonDepth));
            config.selectStackElements();
          });
          buttonStack.appendChild(newButton);
        }
      }
  };

  var distribLabelToColor = 
  {
    "F": "#B30202",
    "U": "#B35202",
    "T": "#016B6B",
    "C": "#028F02"
  };

  function buildICChart(lightArray, tileStats, distribIdx, distribLabel)
  {
    if (!window.icLightCharts)
        window.icLightCharts = {}

    var chart = null;
    if (window.icLightCharts[distribLabel])
    {
        chart = window.icLightCharts[distribLabel];
        chart.data.labels = [];
        chart.data.datasets.data = [];
    }
    else
    {
        chart = window.icLightCharts[distribLabel] = new Chart(document.getElementById('ic' + distribLabel + 'Chart').getContext('2d'),
            {
                type: 'bar',
                data:
                {
                    labels: [],
                    datasets: [
                    {
                        label: distribLabel,
                        data: [],
                        backgroundColor: distribLabelToColor[distribLabel]
                    }]
                },
                options:
                {
                    scales:
                    {
                        xAxes: [ { ticks: { beginAtZero:true } }],
                        yAxes: [ { ticks: { beginAtZero:true } }]
                    }
                }
            });
    }

    for (var lightIdx = 0; lightIdx < lightArray.length; ++lightIdx)
    {
        var distribName = tileStats.weightDistribNames[distribIdx * lightArray.length + lightIdx];
        if (distribName == distribLabel)
        {
            chart.data.labels.push(lightArray[lightIdx].name);
            chart.data.datasets[0].data.push(tileStats.weights[distribIdx * lightArray.length + lightIdx]);
        }
    }

    chart.update();
  }

  function onClickICTableCell(distribIdx, lightIdx, lightArray, tileStats)
  {
    buildICChart(lightArray, tileStats, distribIdx, "F");
    buildICChart(lightArray, tileStats, distribIdx, "T");
    buildICChart(lightArray, tileStats, distribIdx, "U");
    buildICChart(lightArray, tileStats, distribIdx, "C");

    $("#distribIdxLabel").empty();
    $("#distribIdxLabel").append(" Distrib " + distribIdx);

    var fullSum = 0;
    var unoccludedSum = 0;
    for (var lIdx = 0; lIdx < lightArray.length; ++lIdx)
    {
        fullSum += tileStats["perPixelContribs_full"][distribIdx * lightArray.length + lIdx];
        unoccludedSum += tileStats["perPixelContribs_unoccluded"][distribIdx * lightArray.length + lIdx];
    }
    var r1 = $('<tr class="title"><td>light:</td></tr>');
    var r3 = $('<tr><td>F contrib:</td></tr>');
    var r2 = $('<tr><td>F %:</td></tr>');
    for (var lIdx = 0; lIdx < lightArray.length; ++lIdx)
    {
        if (tileStats["perPixelContribs_full"][distribIdx * lightArray.length + lIdx] > 0)
        {
            var distribName = tileStats["weightDistribNames"][distribIdx * lightArray.length + lIdx];
            r1.append('<td style="background-color:' + distribLabelToColor[distribName] + '">' + lIdx + '</td>');
            r3.append('<td>' + tileStats["perPixelContribs_full"][distribIdx * lightArray.length + lIdx].toFixed(3) + '</td>')
            var ratio = tileStats["perPixelContribs_full"][distribIdx * lightArray.length + lIdx] / fullSum;
            var greyValue = Math.round(ratio * 255);
            var color = greyValue > 128 ? "black" : "white";
            r2.append('<td style="background-color:rgba(' + greyValue + ',' + greyValue + ',' + greyValue + ', 1);color:' + color + '">' + (100 * ratio).toFixed(3) + '</td>');
        }
    }
    var t = $('#icFullContribTable');
    t.empty();
    t.append(r1);
    t.append(r3);
    t.append(r2);

    var r1 = $('<tr class="title"><td>light:</td></tr>');
    var r3 = $('<tr><td>U contrib:</td></tr>');
    var r2 = $('<tr><td>U %:</td></tr>');
    for (var lIdx = 0; lIdx < lightArray.length; ++lIdx)
    {
        if (tileStats["perPixelContribs_unoccluded"][distribIdx * lightArray.length + lIdx] > 0)
        {
            var distribName = tileStats["weightDistribNames"][distribIdx * lightArray.length + lIdx];
            r1.append('<td style="background-color:' + distribLabelToColor[distribName] + '">' + lIdx + '</td>');
            r3.append('<td>' + tileStats["perPixelContribs_unoccluded"][distribIdx * lightArray.length + lIdx].toFixed(3) + '</td>')
            var ratio = tileStats["perPixelContribs_unoccluded"][distribIdx * lightArray.length + lIdx] / unoccludedSum;
            var greyValue = Math.round(ratio * 255);
            var color = greyValue > 128 ? "black" : "white";
            r2.append('<td style="background-color:rgba(' + greyValue + ',' + greyValue + ',' + greyValue + ', 1);color:' + color + '">' + (100 * ratio).toFixed(3) + '</td>');
        }
    }
    var t = $('#unoccludedContribTable');
    t.empty();
    t.append(r1);
    t.append(r3);
    t.append(r2);
  }

  function buildICTable(lightArray, tileStats)
  {
    var table = $('<table style="width: 100%;height: 500px; overflow: auto; display: block"></table>');
    var row = $('<tr class="title"></tr>');
    row.append($('<td> </td>'));
    var lightsToInclude = []
    for (var lightIdx = 0; lightIdx < lightArray.length; ++lightIdx)
    {
      var onlyC = true;
      for (var distribIdx = 0; onlyC && distribIdx < tileStats["distribCount"]; ++distribIdx)
      {
          var distribName = tileStats["weightDistribNames"][distribIdx * lightArray.length + lightIdx];
          if (distribName != "C")
              onlyC = false;
      }
      if (!onlyC)
      {
          row.append($('<td style="width:100px">' + lightIdx + '</td>'));
          lightsToInclude.push(lightIdx);
      }
    }
/*    table.append(row);*/

    for (var distribIdx = 0; distribIdx < tileStats["distribCount"]; ++distribIdx)
    {
        row = $('<tr></tr>');
/*      row.append($("<td>" + distribIdx + "</td>"));*/
      for (var l in lightsToInclude)
      {
          var lightIdx = lightsToInclude[l];
          var distribName = tileStats["weightDistribNames"][distribIdx * lightArray.length + lightIdx];
          if (distribName != "C")
            var cell = $('<td class="clickable" style="background-color:' + distribLabelToColor[distribName] + '"></td>');
          else
            var cell = $('<td class="clickable"></td>')
          row.append(cell);
          cell.click(function(distribIdx, lightIdx)
          {
            return function() {
                onClickICTableCell(distribIdx, lightIdx, lightArray, tileStats);
            };
          }(distribIdx, lightIdx));
      }
      table.append(row);
    }
    table.append(row);
    return table;
  }

  function buildTileICChart(lightArray, tileStats)
  {
    var chart = null;
    if (window.icLightTileChart)
    {
        chart = window.icLightTileChart;
        chart.data.labels = [];
        chart.data.datasets[0].data = [];
        chart.data.datasets[0].backgroundColor = [];
    }
    else
    {
        chart = window.icLightTileChart = new Chart(document.getElementById('icTileChart').getContext('2d'),
            {
                type: 'bubble',
                data:
                {
                    labels: [],
                    datasets: [
                    {
                        label: "(" + tileStats.tileCoords[0] + ", " + tileStats.tileCoords[1] + ")",
                        data: [],
                        backgroundColor: []
                    }]
                },
                options:
                {
                    legend:false,
                    aspectRatio: 1,
                    scales: {
                        xAxes: [ { ticks: { beginAtZero:true } }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                max: tileStats.distribCount,
                                stepSize: 10
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index',
                        callbacks: {
                            footer: function(tooltipItems, data) {
                                return "pdf : " + data.datasets[tooltipItems[0].datasetIndex].data[tooltipItems[0].index].weight;
                            },
                            label: function(tooltipItems, data) {
                                var elmt = data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
                                return "Distrib " + elmt.distribIdx + " Light " + elmt.lightIdx + " (" + lightArray[elmt.lightIdx].name + ")";
                            }
                        },
                        footerFontStyle: 'normal'
                    },
                    hover: {
                        mode: 'index',
                        intersect: true
                    },
                    onClick: function(event, tooltipItems) {
                        if (tooltipItems.length > 0)
                        {
                            var elmt = chart.data.datasets[0].data[tooltipItems[0]._index];
                            onClickICTableCell(elmt.distribIdx, elmt.lightIdx, lightArray, tileStats);
                        }
                    }
                }
            });
    }

    for (var lightIdx = 0; lightIdx < lightArray.length; ++lightIdx)
    {
        for (var distribIdx = 0; distribIdx < tileStats["distribCount"]; ++distribIdx)
        {
            var distribName = tileStats.weightDistribNames[distribIdx * lightArray.length + lightIdx];
            var lightName = lightArray[lightIdx].name;

            if (distribName != "C")
            {
                chart.data.datasets[0].data.push({
                    x: lightIdx,
                    y: distribIdx,
                    //r: 10 * tileStats.weights[distribIdx * lightArray.length + lightIdx],
                    r: 5,
                    weight: tileStats.weights[distribIdx * lightArray.length + lightIdx],
                    lightIdx: lightIdx,
                    distribIdx: distribIdx
                });
                chart.data.datasets[0].backgroundColor.push(distribLabelToColor[distribName]);
            }
        }
    }

    chart.update();
  }

  function onClickICTile(stats, lightArray)
  {
      var tileLightContribTable = $('<table style="margin-bottom: 16px"></table>');
      var row1 = $('<tr class="title"></tr>');
      row1.append($('<td>light: </td>'));
      var row2 = $('<tr></tr>');
      row2.append($('<td>T contrib: </td>'));
      var contribSum = 0;
      for (var lightIdx = 0; lightIdx < lightArray.length; ++lightIdx)
      {
        if (stats["lightContribsOnTile_contrib"][lightIdx] > 0)
        {
            row1.append($('<td>' + lightIdx + '</td>'));
            row2.append($('<td>' + stats["lightContribsOnTile_contrib"][lightIdx].toFixed(3) + '</td>'));
            contribSum += stats["lightContribsOnTile_contrib"][lightIdx];
        }
      }
      tileLightContribTable.append(row1);
      tileLightContribTable.append(row2);
      var row3 = $('<tr></tr>');
      row3.append($('<td>%: </td>'));
      for (var lightIdx = 0; lightIdx < lightArray.length; ++lightIdx)
      {
        if (stats["lightContribsOnTile_contrib"][lightIdx] > 0)
        {
            var ratio = stats["lightContribsOnTile_contrib"][lightIdx] / contribSum;
            var greyValue = Math.round(ratio * 255);
            var color = greyValue > 128 ? "black" : "white";
            row3.append('<td style="background-color:rgba(' + greyValue + ',' + greyValue + ',' + greyValue + ', 1);color:' + color + '">' + (100 * ratio).toFixed(3) + '</td>');
        }
      }
      tileLightContribTable.append(row3);

      ///

      var legendRow = $('<tr class="title"><td></td></tr>');
      var directLightingContribSumsRow = $('<tr><td>directLighting</td></tr>');
      var directLightingProbaRow = $('<tr><td>directLightingProba</td></tr>');
      var indirectLightingContribSumsRow = $('<tr><td>indirectLighting</td></tr>');
      var indirectLightingProbaRow = $('<tr><td>indirectLightingProba</td></tr>');
      for (var distribName in distribLabelToColor)
      {
          legendRow.append('<td style="background-color:' + distribLabelToColor[distribName] + '"> ' + distribName + ' </td>');
          var dlC = stats["directLightingContribSums"][distribName].slice(0);
          var rgb = [0,0,0];
          for (var c in dlC) {
            dlC[c] /= stats["pixelCount"];
            rgb[c] = Math.round(Math.min(255, dlC[c] * 255));
          }
          var colorString = 'background-color:rgba(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ',1);';
          directLightingContribSumsRow.append('<td style="' + colorString + '">(' + dlC[0].toFixed(3) + ", " + dlC[1].toFixed(3) + ", " + dlC[2].toFixed(3) + ')</td>');
          directLightingProbaRow.append('<td>' + stats["directProbabilities"][distribName] + '</td>')
          var ilC = stats["indirectLightingContribSums"][distribName].slice(0);
          for (var c in ilC) {
            ilC[c] /= stats["pixelCount"];
            rgb[c] = Math.round(Math.min(255, ilC[c] * 255));
          }
          var colorString = 'background-color:rgba(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ',1);';
          indirectLightingContribSumsRow.append('<td style="' + colorString + '">(' + ilC[0].toFixed(3) + ", " + ilC[1].toFixed(3) + ", " + ilC[2].toFixed(3) + ')</td>');
          indirectLightingProbaRow.append('<td>' + stats["indirectProbabilities"][distribName] + '</td>')
      }
      var legendTable = $('<table style="margin-bottom: 16px"></table>');
      legendTable.append(legendRow);
      legendTable.append(directLightingContribSumsRow);
      legendTable.append(directLightingProbaRow);
      legendTable.append(indirectLightingContribSumsRow);
      legendTable.append(indirectLightingProbaRow);

      $("#light_importance_caching_tile_stats").empty();
      $("#light_importance_caching_tile_stats").append('<h3>Tile (' + stats["tileCoords"][0] + ','+ stats["tileCoords"][1] + ')<span id="distribIdxLabel"></span></h3>');
      var fullContribTable = $('<table id="icFullContribTable" style="margin-bottom: 16px"></table>');
      var unoccludedContribTable = $('<table id="unoccludedContribTable" style="margin-bottom: 16px"></table>');
      $("#light_importance_caching_tile_stats").append(legendTable);
      $("#light_importance_caching_tile_stats").append(tileLightContribTable);
      $("#light_importance_caching_tile_stats").append(fullContribTable);
      $("#light_importance_caching_tile_stats").append(unoccludedContribTable);
      //$("#light_importance_caching_tile_stats").append(buildICTable(lightArray, stats));
      buildTileICChart(lightArray, stats);
  }

  function setupLightsReport()
  {
    var lightArray = report.renderer_report.lights_report.visible_lights;
    var lightsPerType = {};
    var lightsAttrPerType = {};
    for (var i in lightArray)
    {
      var type = lightArray[i].type;
      if (!lightsPerType[type])
      {
        lightsPerType[type] = [];
        lightsAttrPerType[type] = [];

        for (var attrName in lightArray[i])
        {
          if (attrName != "type")
            lightsAttrPerType[type].push(attrName);
        }
      }
      lightsPerType[lightArray[i].type].push(i);
    }
    for (var type in lightsAttrPerType)
    {
      $("#lights_report_content").append('<h2>' + type + '</h2>');
      var newTable = $("<table></table>");
      var row = '<td>index</td>';
      for (var i in lightsAttrPerType[type])
      {
        var attrName = lightsAttrPerType[type][i];
        if (attrName != "localToWorld")
          row += '<td>' + attrName + '</td>';
      }
      newTable.append('<tr class="title">' + row + '</tr>');
      for (var i in lightsPerType[type])
      {
        var lightIdx = lightsPerType[type][i];
        var row = '<td>' + lightIdx + '</td>';
        for (var i in lightsAttrPerType[type])
        {
          var attrName = lightsAttrPerType[type][i];
          if (attrName == "color" || attrName == "colorHeuristic" || attrName == "shadowColor")
          {
            var rgb = lightArray[lightIdx][attrName];
            var r = Math.round(Math.min(rgb[0] * 255, 255));
            var g = Math.round(Math.min(rgb[1] * 255, 255));
            var b = Math.round(Math.min(rgb[2] * 255, 255));
            var colorString = 'background-color:rgba(' + r + ',' + g + ',' + b + ',1);';
            row += '<td style="background-color:rgba(64, 64, 64, 1);"><div style="' + colorString + 'width:32px;height:16px;margin:0 auto;"> </div></td>';
          }
          else if (attrName != "localToWorld")
          {
            row += '<td style="background-color:rgba(64, 64, 64, 1);text-align:center;padding-left:8px;padding-right:8px">' + lightArray[lightIdx][attrName] + '</td>';
          }
        }
        newTable.append('<tr>' + row + '</tr>');
      }
      $("#lights_report_content").append(newTable);
    }
    var lightStats = report.renderer_report.lights_report.visible_light_stats;
    if (lightStats.length > 0)
    {
        var lightStatsTable = $("<table></table>");
        var row = '<td>index</td><td>name</td>';
        var sums = {}
        for (var statName in lightStats[0])
        {
          row += '<td>' + statName + '</td>';
          sums[statName] = 0;
        }
        lightStatsTable.append('<tr class="title">' + row + '</tr>');
        for (var i in lightStats)
        {
          for (var statName in lightStats[i])
            sums[statName] += lightStats[i][statName];
        }
        for (var i in lightStats)
        {
          var row = '<td>' + i + '</td><td style="text-align:center;background-color:rgba(64, 64, 64, 1);">' + lightArray[i]["name"] + '</td>';
          for (var statName in lightStats[i])
          {
            var ratio = lightStats[i][statName] / sums[statName];
            row += '<td style="text-align:center;background-color:rgba(64, 64, 64, 1);">' + lightStats[i][statName] + ' | ' + (ratio * 100).toFixed(2) + '% </td>';
          }
          lightStatsTable.append('<tr>' + row + '</tr>');
        }
        $("#light_stats_report_content").append(lightStatsTable);
    }

    var tileCount = report.renderer_report["tileCount"];
    var lightICStats = report.renderer_report.lights_report.importance_caching;
    if (!lightICStats)
    {
      $("#importance_caching_report_container").remove();
      return;
    }

    var lightICTileStats = lightICStats.tiles;
    var lightICTable = $("#light_importance_caching_table");

    var row = $("<tr></tr>");
    for (var j = 0; j < tileCount[1]; ++j)
    {
        for (var i = 0; i < tileCount[0]; ++i)
        {
              var tileStat = lightICTileStats["tile_" + i + "_" + (tileCount[1] - j - 1)];
              if (tileStat)
              {
                  var pixelCount = tileStat["pixelCount"];
                  var all = [ tileStat["directLightingContribSums"]["F"],
                              tileStat["directLightingContribSums"]["U"],
                              tileStat["directLightingContribSums"]["T"],
                              tileStat["directLightingContribSums"]["C"],
                              tileStat["indirectLightingContribSums"]["F"],
                              tileStat["indirectLightingContribSums"]["U"],
                              tileStat["indirectLightingContribSums"]["T"],
                              tileStat["indirectLightingContribSums"]["C"] ];
                  var rgb = [0, 0, 0];
                  for (var c = 0; c < 3; ++c)
                  {
                      for (var a in all)
                          rgb[c] += all[a][c];
                      rgb[c] = Math.round(Math.min((rgb[c] / pixelCount) * 255, 255));
                  }
                  var colorString = 'background-color:rgba(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ',1);';
                  var element = $('<td class="ic_cell ic_filled_cell clickable" style="'+colorString+'"></td>');
                  element.click(function(stats){
                      return function() {
                        onClickICTile(stats, lightArray)
                      }
                  }(tileStat));

                  element.hover(function(stats){
                      return function() 
                      {
                          $("#ic_tile_coords").empty();
                          $("#ic_tile_coords").append("(" + stats["tileCoords"][0] + ", " + stats["tileCoords"][1] + ")");
                      }
                  }(tileStat));

                  row.append(element);
              } 
        }
    }
    row.append('<td id=ic_tile_coords></td>');
    lightICTable.append(row);

    for (var tile in lightICTileStats)
    {
      onClickICTile(lightICTileStats[tile], lightArray); // Display first tile data
      break;
    }
  }

  window.onload = function() 
  {
    var ctx = document.getElementById('renderFrameChart').getContext('2d');
    window.myDoughnut = new Chart(ctx, config);
    config.updateButtonStack();

    fillTable(report.scene_report.instance_report, 'shading_total_usecs', 0, $("#totalShadingTable"));
    fillTable(report.scene_report.instance_report, 'shading_total_usecs', 1, $("#totalShadowTable"));
    fillTable(report.scene_report.instance_report, 'shading_avg_usecs', 0, $("#avgShadingTable"));
    fillTable(report.scene_report.instance_report, 'shading_avg_usecs', 1, $("#avgShadowTable"));

    fillTableFromStatsTree(shibTree, $("#overviewTable"), "overviewTableRow", 0.0);
    fillTableFromStatsTree(sceneTree, $("#sceneTable"), "sceneTableRow", 0.0);
    fillTableFromStatsTree(rendererTree, $("#rendererTable"), "rendererTableRow", 0.0);

    var fillRenderFrameTable = function()
    {
      $(".renderFrameTableRow").remove();
      fillTableFromStatsTree(renderFrameTree, $("#renderFrameTable"), "renderFrameTableRow", parseFloat($("#renderFrameGlobalRatioThreshold")[0].value) / 100.0);

      $(".renderFrameTableRow").each(function(i) { 
        $(this).click(function() {
          config.setFromFullName($(this)[0].id);
        });
      });
      config.selectStackElements();
    }

    $( "#renderFrameGlobalRatioThreshold" ).change(function() 
    {
      fillRenderFrameTable();
    });

    $( "#renderFrameChartDivCheck" ).change(function() 
    {
      if ( $("#renderFrameChartDiv").css('display') == 'none' ) {
        $("#renderFrameChartDiv").width("40%");
        config.selectStackElements();
      } 
      else {
        $("#renderFrameChartDiv").width("10px");
        config.unselectStackElements();
      }
      $("#renderFrameChartDiv").toggle();
    });

    fillRenderFrameTable();

    var stringToHTML = function(s) {
      return s.replace(/ /g, '&nbsp;').replace(/\n/g, '<br />')
    }

    if(report.renderer_report.texture_report) {
      $("#texture_log_content").html(stringToHTML(report.renderer_report.texture_report))
    }
    if(report.renderer_report.osl_report) {
      $("#osl_log_content").html(stringToHTML(report.renderer_report.osl_report))
    }

    if(report.renderer_report.lights_report)
      setupLightsReport();
  };
</script>

</html>
