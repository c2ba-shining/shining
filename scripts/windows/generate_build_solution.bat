set DIR=%~dp0
cmake -DBUILD_DIRECTORY=%DIR%..\..\build -P %DIR%..\extract_3rdparty.cmake

pushd %DIR%..\..\build

cmake -G "Visual Studio 14 2015 Win64" ^
    -DBUILD_SHARED_LIBS=0 ^
    -DCMAKE_INSTALL_PREFIX=..\dist\ ^
    -DGLFW_BUILD_DOCS=0 ^
    -DGLFW_BUILD_EXAMPLES=0 ^
    -DGLFW_BUILD_TESTS=0 ^
    -DGLFW_DOCUMENT_INTERNALS=0 ^
    -DGLFW_INSTALL=0 ^
    -DBUILD_SHIV=1 ^
    ..

popd