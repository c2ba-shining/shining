set DIR=%~dp0
call %DIR%\generate_build_solution.bat
cmake --build %DIR%..\..\build --config Debug --target INSTALL --parallel
cmake --build %DIR%..\..\build --config Release --target INSTALL --parallel