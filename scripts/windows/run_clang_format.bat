set DIR=%~dp0

@echo off

pushd %DIR%..\..\lib\shining\
call :RUNFORMAT
popd

pause

:RUNFORMAT
for /R %%i in (*.cpp *.h *.hpp) do (
echo Formatting file %%i
clang-format.exe -i %%i
)
goto :eof