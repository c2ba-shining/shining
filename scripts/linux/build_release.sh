DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
bash $DIR/generate_build_solution.sh Release
cmake --build $DIR/../../build --target install --parallel
