
#define ARRAY_SIZE 16

void
layeredTexture
(
    int     _attributes_size,
    color   _attributes_color     [ARRAY_SIZE],
    float   _attributes_alpha     [ARRAY_SIZE],
    int     _attributes_blendMode [ARRAY_SIZE],
    int _attributes_visible[ARRAY_SIZE],
    output color _outColor,
    output float _outColorA
  )
{

    //
    // Use "not premult" to allow alpha from separate texture.
    // Load the textures as "straight"
    // 
    
	color bc = color(0, 0, 0);
	float ba = 0;
    for (int i = _attributes_size-1; i >= 0 ; --i)
    {
        
        if ( _attributes_visible[i] == 0)
        {
            continue;
        }
        
        // the layer stack composite color and alpha
        color cc;
        float ca;
        
        // the top layer
        color fc = _attributes_color[i];
        float fa = _attributes_alpha[i];

        // hardcoded values from TextureBlendMode values in Types.h	
        int blendMode = _attributes_blendMode[i];
        if (blendMode == 0 )        						// None										    
        {						
            // not premult
            cc = fc * fa;														    
            
            // premult
            //cc = fc;
            
            ca = fa;														    
        }																			
        else if (blendMode == 1)   							// Over											
        {																			
            // not premultiplied
            cc = (1 - fa ) * bc + fa * fc;		
            
            // premultiplied
            //cc = (1 - fa ) * bc + fc;
            
            ca = clamp(ba + fa - (ba * fa), 0, 1);	
        }																			
        else if (blendMode == 4)								// Add			
        {		
            // not premult
            cc = bc + (fc * fa);					                            
            
            // premult
            //cc = bc + fc ;
            
            ca = ba;						                                    
        }
        else if (blendMode == 5)
        {                                                   // Subtract
            // Not premult
            cc = max(bc - (fc * fa), 0.0);

            //premult
            //cc = max(bc - fc, 0.0);

            ca = ba;
        }
        else if (blendMode == 6)   							// Multiply                                    
        {																			
            // Not premult
            cc = bc * (fc * fa + 1 - fa);                              
            
            // premult
            //cc = bc * (fc + 1 - fa);                              
            
            ca = ba;						                                    
        }
        else if (blendMode == 7)
        {                                                   // Difference
            // Not premult
            cc = abs(bc - (fc * fa));

            //premult
            //cc = abs(bc - fc);

            ca = ba;
        }
        else if (blendMode == 8)   							// Lighten                                      
        {																			
            // premult
            cc = max(fc, bc) * fa + bc * (1 - fa);					
            
            //not premult
            //cc = max(fc, bc* fa)  + bc * (1 - fa);					
            
            ca = ba;						                                    
        }																			
        else if (blendMode == 9)   							// Darken                                       
        {																			
            // not premult
            cc = min(fc, bc) * fa + bc * (1 - fa);					        
            
            // premult
            //cc = min(fc, bc *fa) + bc * (1 - fa);		
            
            ca = ba;						                                    
        }																			
        else if (blendMode == 20)   							// PS Overlay                                  
        {	
            color c;																		
            for (int i = 0; i < 3; i++)
            {
                if (bc[i] < 0.5)
                    c[i] = 2 * fc[i] * bc[i];
                else
                    c[i] = 1 - 2 * (1 - bc[i]) * (1 - fc[i]);
            }
            cc = c * fa + bc * (1 - fa);
            ca = ba;						                                    
        }				
        else   // None										    
        {						
            // not premult
            cc = fc * fa;														    
            
            // premult
            //cc = fc;
            
            ca = fa;														    
        }	
        
        ba = ca;
        bc = cc;
    }
    
    _outColor = bc;
    _outColorA = ba;


}
