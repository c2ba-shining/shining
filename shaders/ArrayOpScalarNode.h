
shader
SHADER_NAME
    [[ string help = "SHADER_NAME" ]]
(

    int    _inputs_size            = 0,
    TYPE   _inputs[ARRAY_SIZE]     = {0},
    int    _operator               = 0,

    output TYPE _outValue = 0
  )
{
    if (_operator == OP_NONE)
    {
    }
    else if (_operator == OP_ADD || _operator == OP_AVERAGE)
    {
        for (int i = 0; i < _inputs_size; i++)
        {
            _outValue += _inputs[i];
        }
        if (_operator == OP_AVERAGE)
        {
            _outValue /= _inputs_size;
        }    
    }
    else if (_operator == OP_SUBSTRACT)
    {
        _outValue = _inputs[0];
        for (int i = 1; i < _inputs_size; i++)
        {
            _outValue -= _inputs[i];
        }
    }    
}
