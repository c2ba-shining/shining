
#define PERLIN 0
#define TIME_SPACE 4

float fractalSimplex4D(point pos, float time, int _depthMax, float _ratio, float _frequencyRatio)
{
    float noiseLevel = 0;
    float persistence = 1.0;
    float frequencyFactor = 1.0;
    int count = max(1, _depthMax);
    for (int i = 1; i <= count; i++)
    {
       // Vertigo uses Simplex, Maya uses perlin, make Shining closer to Vertigo
       noiseLevel += persistence * noise("simplex", pos * frequencyFactor, time * frequencyFactor);
       persistence *= _ratio;
       frequencyFactor *= _frequencyRatio;
    }
    return noiseLevel;
}
 
 float fractalSimplex3D(point pos, int _depthMax, float _ratio, float _frequencyRatio)
{
    float noiseLevel = 0;
    float persistence = 1.0;
    float frequencyFactor = 1.0;
    int count = max(1, _depthMax);
    for (int i = 1; i <= count; i++)
    {
        // Vertigo uses Simplex, Maya uses perlin, make Shining closer to Vertigo
        noiseLevel += persistence * noise("simplex", pos * frequencyFactor);
        persistence *= _ratio;
        frequencyFactor *= _frequencyRatio;
    }
    return noiseLevel;
}

