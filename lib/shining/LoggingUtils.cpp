#include "LoggingUtils.h"
#include "Shining_p.h"

namespace shn
{

#ifdef __WIN32__

void localtime(struct tm * out, const time_t * time)
{
    localtime_s(out, time);
}

#else

void localtime(struct tm * out, const time_t * time)
{
    localtime_r(time, out);
}

#endif

} // namespace shn