#pragma once

#include "UString.h"
#include <sstream>
#include <string>
#include <vector>

WARNING_IGNORE_PUSH

#include <OpenImageIO/strutil.h>

WARNING_IGNORE_POP

namespace shn
{

using OIIO_NAMESPACE::string_view;

inline std::vector<string_view> split(const string_view & str, const string_view & sep)
{
    using namespace OIIO_NAMESPACE;
    std::vector<string_view> result;
    Strutil::split(str, result, sep);
    return result;
}

using OIIO_NAMESPACE::Strutil::starts_with;

using std::to_string;

template<typename T>
std::string to_string(T && v)
{
    std::stringstream ss;
    ss << v;
    return ss.str();
}

} // namespace shn