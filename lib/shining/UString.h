#pragma once

#include "Shining_p.h"
#include "TypeUtils.h"

WARNING_IGNORE_PUSH

#include <OpenImageIO/ustring.h>

WARNING_IGNORE_POP

namespace shn
{

using OIIO::ustring;

inline const ustring & toUstring(const ustring & str)
{
    return str;
}

inline ustring toUstring(const char * str)
{
    return ustring(str);
}

} // namespace shn

namespace std
{

template<class Key>
struct hash;

template<>
struct hash<shn::ustring>
{
    size_t operator()(const shn::ustring & s) const
    {
        return s.hash();
    }
};

} // namespace std