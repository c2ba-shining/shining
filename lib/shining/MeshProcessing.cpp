#include "Attributes.h"
#include "Displacement.h"
#include "IMathUtils.h"
#include "LoggingUtils.h"
#include "SceneImpl.h"
#include "Shining_p.h"
#include "sampling/PiecewiseDistributions.h"
#include <float.h>
#include <unordered_set>

// OpenSubdiv compilation tweaks
#ifdef _MSC_VER
// clang-format off
#define and &&
#define and_eq &=
#define bitand &
#define bitor |=
#define compl = ~
#define not !
#define not_eq !=
#define or ||
#define or_eq |=
#define xor ^/**/
#define xor_eq ^=
// clang-format on
#define _USE_MATH_DEFINES

#define _CRT_SECURE_NO_WARNINGS
/*
#define _SECURE_SCL 0
#define _HAS_ITERATOR_DEBUGGING 0
*/
#include <math.h>

#endif

#include <opensubdiv/far/patchMap.h>
#include <opensubdiv/far/patchTableFactory.h>
#include <opensubdiv/far/primvarRefiner.h>
#include <opensubdiv/far/topologyDescriptor.h>
namespace osd3 = OpenSubdiv::OPENSUBDIV_VERSION;

#include <algorithm>
#include <array>
#include <memory>
#include <numeric>
#include <unordered_map>

namespace shn
{
// Vertex container implementation.
struct OSDVertex
{

    OSDVertex()
    {
        Clear();
    }

    OSDVertex(OSDVertex const & src)
    {
        _data[0] = src._data[0];
        _data[1] = src._data[1];
        _data[2] = src._data[2];
    }

    OSDVertex(const float x, const float y, const float z)
    {
        _data[0] = x;
        _data[1] = y;
        _data[2] = z;
    }

    OSDVertex(const float * p)
    {
        _data[0] = p[0];
        _data[1] = p[1];
        _data[2] = p[2];
    }

    void Clear(void * = 0)
    {
        _data[0] = _data[1] = _data[2] = 0.0f;
    }

    void AddWithWeight(OSDVertex const & src, float weight)
    {
        _data[0] += weight * src._data[0];
        _data[1] += weight * src._data[1];
        _data[2] += weight * src._data[2];
    }

    float Distance(OSDVertex const & other)
    {
        return std::sqrt(std::pow(other._data[0] - _data[0], 2) + std::pow(other._data[1] - _data[1], 2) + std::pow(other._data[2] - _data[2], 2));
    }

    void SetData(float x, float y, float z)
    {
        _data[0] = x;
        _data[1] = y;
        _data[2] = z;
    }

    void SetData(const float * data)
    {
        _data[0] = data[0];
        _data[1] = data[1];
        _data[2] = data[2];
    }

    const float * GetData() const
    {
        return _data;
    }

    const float operator[](int32_t index) const
    {
        return _data[index];
    }

    float & operator[](int32_t index)
    {
        return _data[index];
    }

    bool operator==(const OSDVertex & other) const
    {
        return (_data[0] == other._data[0] && _data[1] == other._data[1] && _data[2] == other._data[2]);
    }

    bool operator<(const OSDVertex & other) const
    {
        return _data[0] < other._data[0] || (!(other._data[0] < _data[0]) && (_data[1] < other._data[1] || (!(other._data[1] < _data[1]) && _data[2] < other._data[2])));
    }

    OSDVertex operator+(const OSDVertex & other) const
    {
        OSDVertex result;
        result._data[0] = _data[0] + other._data[0];
        result._data[1] = _data[1] + other._data[1];
        result._data[2] = _data[2] + other._data[2];
        return result;
    }

    OSDVertex operator*(const float a) const
    {
        OSDVertex result;
        result._data[0] = _data[0] * a;
        result._data[1] = _data[1] * a;
        result._data[2] = _data[2] * a;
        return result;
    }

private:
    float _data[3];
};

const std::pair<SceneImpl::Mesh *, SceneImpl::Mesh *> SceneImpl::allocateOneMeshDataLevel(Scene::Id meshId)
{
    // base is an array element.
    // allocate a new mesh, and swap them in the array
    Mesh * base = &m_meshes[meshId];
    Mesh * processed = new Mesh;
    processed->processingAttributes.copy(base->processingAttributes); // copy processing attributes (subdivisions, displacement, ...) from base to result
    std::swap(*processed, *base); // swap content, now "processed" is in the array, "base" on the heap
    std::swap(processed, base); // swap ptr
    m_meshes[meshId].baseMesh = base;
    return std::make_pair(base, processed);
}

static int32_t SubdivideUniformMesh(
    SceneImpl * scene, Scene::Id meshId, SceneImpl::Mesh & processed, SceneImpl::Mesh & base, int32_t maxLevel, int32_t method, int32_t vertexBoundary, int32_t fvarBoundary,
    bool fvarPropagateCornerValue, int32_t creasingMethod);

static int32_t SubdivideAdaptiveMesh(
    SceneImpl * scene, Scene::Id meshId, SceneImpl::Mesh & processed, SceneImpl::Mesh & base, int32_t maxLevel, int32_t method, int32_t vertexBoundary, int32_t fvarBoundary,
    bool fvarPropagateCornerValue, int32_t creasingMethod);

static void TriangulateMesh(SceneImpl::Mesh & processed, const SceneImpl::Mesh & base);

static int32_t computeLODSubdivisionLevel(SceneImpl::Mesh & m, M44f & projMat, float edgeLength, int32_t maxLevel);

// conversion options from OSD2 to OSD3
static void convertSubdivisionOptions(
    int32_t vertexBoundary, int32_t fvarBoundary, bool fvarPropagateCornerValue, osd3::Sdc::Options::VtxBoundaryInterpolation & vbi, osd3::Sdc::Options::FVarLinearInterpolation & fvli);

static int32_t mergeFVarValues(SceneImpl::Mesh & m, std::vector<int32_t> & fvarSources, int32_t fvarCount);

int32_t SceneImpl::buildMeshStaticAccel(Scene::Id meshId, Scene::Id instanceId)
{
    struct Vertex
    {
        float x, y, z, r;
    };
    struct Triangle
    {
        int v0, v1, v2;
    };

    assert(meshId < m_meshes.size());
    assert(instanceId < m_instances.size());
    Mesh * m = &(m_meshes[meshId]);
    const Instance & instance = m_instances[instanceId];

    // Build acceleration structures
    assert(m->positionAttrIndex >= 0);
    int32_t countVertices = m->vertexAttributes[m->positionAttrIndex].count;
    int32_t faceCount = m->faces.count;
    int32_t triangleCount = m->faces.dataInt ? std::accumulate(m->faces.dataInt, m->faces.dataInt + faceCount, 0, [](int x, int y) { return x + (y - 2); }) : faceCount;
    assert(m->positionAttrIndex >= 0 && "NO POSITION ATTRIBUTE");

    Mesh::VertexAttribute * positions = &m->vertexAttributes[m->positionAttrIndex];
    const int32_t * const topology = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].dataInt;

    unsigned rtcMeshId = rtcNewTriangleMesh(m_rtcStaticScene, RTC_GEOMETRY_STATIC, triangleCount, countVertices, 1);
    Triangle * triangles = (Triangle *) rtcMapBuffer(m_rtcStaticScene, rtcMeshId, RTC_INDEX_BUFFER);

    for (int32_t j = 0; j < triangleCount; ++j)
    {
        triangles[j].v0 = topology[j * 3];
        triangles[j].v1 = topology[j * 3 + 1];
        triangles[j].v2 = topology[j * 3 + 2];
    }
    rtcUnmapBuffer(m_rtcStaticScene, rtcMeshId, RTC_INDEX_BUFFER);

    Vertex * vertices = (Vertex *) rtcMapBuffer(m_rtcStaticScene, rtcMeshId, RTC_VERTEX_BUFFER);
    const float * positions0 = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat;
    for (int32_t j = 0, k = countVertices; j < k; ++j)
    {
        V3f v(0.f);
        instance.itransform.transforms[0].multVecMatrix(V3f(positions0[j * 3], positions0[j * 3 + 1], positions0[j * 3 + 2]), v);
        vertices[j].x = v.x;
        vertices[j].y = v.y;
        vertices[j].z = v.z;
    }
    rtcUnmapBuffer(m_rtcStaticScene, rtcMeshId, RTC_VERTEX_BUFFER);
    rtcSetMask(m_rtcStaticScene, rtcMeshId, instance.visibility);
    m_rtcStaticMeshIds[rtcMeshId] = instanceId;
    return 0;
}

int32_t SceneImpl::buildMeshStaticInstancedAccel(Scene::Id meshId)
{
    struct Vertex
    {
        float x, y, z, r;
    };
    struct Triangle
    {
        int v0, v1, v2;
    };

    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);

    RTCScene scene = rtcDeviceNewScene(m_rtcDevice, RTC_SCENE_STATIC, getRTCAlgorithmFlags());

    // Build acceleration structures
    assert(m->positionAttrIndex >= 0);
    int32_t countVertices = m->vertexAttributes[m->positionAttrIndex].count;
    int32_t faceCount = m->faces.count;
    int32_t triangleCount = m->faces.dataInt ? std::accumulate(m->faces.dataInt, m->faces.dataInt + faceCount, 0, [](int x, int y) { return x + (y - 2); }) : faceCount;
    assert(m->positionAttrIndex >= 0 && "NO POSITION ATTRIBUTE");

    Mesh::VertexAttribute * positions = &m->vertexAttributes[m->positionAttrIndex];
    const int32_t * const topology = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].dataInt;

    unsigned rtcMeshId = rtcNewTriangleMesh(scene, RTC_GEOMETRY_STATIC, triangleCount, countVertices, 1);
    Triangle * triangles = (Triangle *) rtcMapBuffer(scene, rtcMeshId, RTC_INDEX_BUFFER);

    for (int32_t j = 0; j < triangleCount; ++j)
    {
        triangles[j].v0 = topology[j * 3];
        triangles[j].v1 = topology[j * 3 + 1];
        triangles[j].v2 = topology[j * 3 + 2];
    }
    rtcUnmapBuffer(scene, rtcMeshId, RTC_INDEX_BUFFER);

    Vertex * vertices = (Vertex *) rtcMapBuffer(scene, rtcMeshId, RTC_VERTEX_BUFFER);
    const float * positions0 = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat;
    for (int32_t j = 0, k = countVertices; j < k; ++j)
    {
        vertices[j].x = positions0[j * 3];
        vertices[j].y = positions0[j * 3 + 1];
        vertices[j].z = positions0[j * 3 + 2];
        m->bounds.extendBy(fv3ToV3f((const float *) &(vertices[j])));
    }
    rtcUnmapBuffer(scene, rtcMeshId, RTC_VERTEX_BUFFER);
    rtcCommit(scene);

    m_primitives[meshId] = MotionPrimitive(m->bounds, scene, PrimitiveType::TRIANGLES);
    m->processed = true;
    return 0;
}

int32_t SceneImpl::buildMeshMotionAccel(Scene::Id meshId)
{
    struct Vertex
    {
        float x, y, z, r;
    };
    struct Triangle
    {
        int v0, v1, v2;
    };

    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);

    const size_t count = m->vertexAttributes[m->positionAttrIndex].frameCount - 1;
    std::vector<RTCScene> scenes;
    std::vector<float> times;
    scenes.reserve(count);
    times.reserve(count);

    for (int32_t i = 0; i < count; ++i)
    {
        RTCScene scene = rtcDeviceNewScene(m_rtcDevice, RTC_SCENE_STATIC, getRTCAlgorithmFlags());
        // Build acceleration structures
        assert(m->positionAttrIndex >= 0);
        int32_t countVertices = m->vertexAttributes[m->positionAttrIndex].count;
        int32_t faceCount = m->faces.count;
        int32_t triangleCount = m->faces.dataInt ? std::accumulate(m->faces.dataInt, m->faces.dataInt + faceCount, 0, [](int x, int y) { return x + (y - 2); }) : faceCount;
        assert(m->positionAttrIndex >= 0 && "NO POSITION ATTRIBUTE");

        Mesh::VertexAttribute * positions = &m->vertexAttributes[m->positionAttrIndex];
        const int32_t * const topology = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].dataInt;

        unsigned rtcMeshId = rtcNewTriangleMesh(scene, RTC_GEOMETRY_STATIC, triangleCount, countVertices, 2);
        Triangle * triangles = (Triangle *) rtcMapBuffer(scene, rtcMeshId, RTC_INDEX_BUFFER);

        for (int32_t j = 0; j < triangleCount; ++j)
        {
            triangles[j].v0 = topology[j * 3];
            triangles[j].v1 = topology[j * 3 + 1];
            triangles[j].v2 = topology[j * 3 + 2];
        }
        rtcUnmapBuffer(scene, rtcMeshId, RTC_INDEX_BUFFER);

        Vertex * vertices0 = (Vertex *) rtcMapBuffer(scene, rtcMeshId, RTC_VERTEX_BUFFER0);
        Vertex * vertices1 = (Vertex *) rtcMapBuffer(scene, rtcMeshId, RTC_VERTEX_BUFFER1);
        const float * positions0 = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat[i];
        const float * positions1 = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat[i + 1];
        for (int32_t j = 0, k = countVertices; j < k; ++j)
        {
            vertices0[j].x = positions0[j * 3];
            vertices0[j].y = positions0[j * 3 + 1];
            vertices0[j].z = positions0[j * 3 + 2];
            vertices1[j].x = positions1[j * 3];
            vertices1[j].y = positions1[j * 3 + 1];
            vertices1[j].z = positions1[j * 3 + 2];
            // vertices1[j] = {positions1[j*3] - positions0[j*3], positions1[j*3 + 1] - positions0[j*3 + 1], positions1[j*3 + 2]-positions0[j*3 + 2], 0.f};
            m->bounds.extendBy(fv3ToV3f((const float *) &(vertices0[j])));
            m->bounds.extendBy(fv3ToV3f((const float *) &(vertices1[j])));
        }
        rtcUnmapBuffer(scene, rtcMeshId, RTC_VERTEX_BUFFER0);
        rtcUnmapBuffer(scene, rtcMeshId, RTC_VERTEX_BUFFER1);
        rtcCommit(scene);
        scenes.emplace_back(scene);
        times.emplace_back(m->vertexAttributes[m->positionAttrIndex].timeValues[i]);
    }
    m_primitives[meshId] = MotionPrimitive(m->bounds, scenes.size(), scenes.data(), times.data(), PrimitiveType::TRIANGLES);
    m->processed = true;
    return 0;
}

int32_t SceneImpl::buildVolumeMeshStaticAccel(Scene::Id instanceId)
{
    struct Vertex
    {
        float x, y, z, r;
    };
    struct Triangle
    {
        int v0, v1, v2;
    };

    assert(instanceId < m_instances.size());
    const Instance & instance = m_instances[instanceId];
    Scene::Id meshId = instance.meshId;
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);

    // Build acceleration structures
    assert(m->positionAttrIndex >= 0 && "NO POSITION ATTRIBUTE");
    Mesh::VertexAttribute * positions = &m->vertexAttributes[m->positionAttrIndex];
    const int32_t * const topology = m->topologies[positions->topology].dataInt;
    int32_t verticesCount = positions->count;
    int32_t faceCount = m->faces.count;
    int32_t triangleCount = m->faces.dataInt ? std::accumulate(m->faces.dataInt, m->faces.dataInt + faceCount, 0, [](int x, int y) { return x + (y - 2); }) : faceCount;

    unsigned rtcVolumeId = rtcNewTriangleMesh(m_rtcVolumeScene, RTC_GEOMETRY_STATIC, triangleCount, verticesCount, 1);
    Triangle * triangles = (Triangle *) rtcMapBuffer(m_rtcVolumeScene, rtcVolumeId, RTC_INDEX_BUFFER);
    for (int32_t j = 0; j < triangleCount; ++j)
    {
        triangles[j].v0 = topology[j * 3];
        triangles[j].v1 = topology[j * 3 + 1];
        triangles[j].v2 = topology[j * 3 + 2];
    }
    rtcUnmapBuffer(m_rtcVolumeScene, rtcVolumeId, RTC_INDEX_BUFFER);

    Vertex * vertices = (Vertex *) rtcMapBuffer(m_rtcVolumeScene, rtcVolumeId, RTC_VERTEX_BUFFER);
    const float * positions0 = (const float *) positions->dataFloat;
    for (int32_t j = 0; j < verticesCount; ++j)
    {
        V3f v(0.f);
        instance.itransform.transforms[0].multVecMatrix(V3f(positions0[j * 3], positions0[j * 3 + 1], positions0[j * 3 + 2]), v);
        vertices[j].x = v.x;
        vertices[j].y = v.y;
        vertices[j].z = v.z;
    }
    rtcUnmapBuffer(m_rtcVolumeScene, rtcVolumeId, RTC_VERTEX_BUFFER);
    rtcSetMask(m_rtcVolumeScene, rtcVolumeId, instance.visibility);
    m_rtcVolumeSceneIds[rtcVolumeId] = instanceId;

    return 0;
}

#if 0
int32_t SceneImpl::buildMeshAccel(Scene::Id meshId)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);

    // Build acceleration structures
    assert(m->positionAttrIndex >= 0);
    int32_t countVertices = m->vertexAttributes[m->positionAttrIndex].count;
    int32_t faceCount = m->faces.count;
    int32_t triangleCount = m->faces.dataInt ? std::accumulate(m->faces.dataInt, m->faces.dataInt+faceCount,
                                                                0, [](int x, int y) { return x+(y-2); })
                                             : faceCount;
    m->boundingBox = e::BBox3f(embree::NegInfTy());
    assert(m->positionAttrIndex >= 0 && "NO POSITION ATTRIBUTE");

    Mesh::VertexAttribute * positions = &m->vertexAttributes[m->positionAttrIndex];
    const int32_t * const topology = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].dataInt;
    if (positions->frameCount == 1)
    {
        e::RTCGeometry * accel = embree::rtcNewTriangleMesh(triangleCount, countVertices, "bvh4.triangle4");
        e::RTCTriangle * triangles = (e::RTCTriangle*) e::rtcMapTriangleBuffer(accel);
        e::Vec3fa * vertices = (e::Vec3fa*) e::rtcMapPositionBuffer(accel);
        const float * positions0 = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat;

        //shndebug("SceneImpl::computeAccelerationStructure : Mesh -> %d : %d faces", meshId, triangleCount);

        for (int32_t j = 0; j < triangleCount; ++j)
        {
            int vId1 = topology[j*3+0];
            int vId2 = topology[j*3+1];
            int vId3 = topology[j*3+2];
            //shndebug("SceneImpl::computeAccelerationStructure : Face -> %lu [%d : %d : %d]", j, vId1, vId2, vId3);
            int id0 = 0x7FFFFFFF;  // the intersector will put the id0 of the virtual object (aka the instance id)
                                   // see virtual_object_anim_intersector1.h line 62
                                   //     if (obj.id0 != 0x7FFFFFFF && ray.id0 == 0x7FFFFFFF) ...
            int id1 = j;  // the face id
            triangles[j] = embree::RTCTriangle(vId1, vId2, vId3, id0, id1);
        }

        for (int32_t j = 0, k = countVertices; j < k; ++j)
        {
            embree::Vec3fa v(positions0[j*3], positions0[j*3 + 1], positions0[j*3 + 2]);
            //shndebug("SceneImpl::computeAccelerationStructure : Position -> %lu : [%f : %f : %f]", j, v.x, v.y, v.z);
            vertices[j] = e::Vec3fa(v.x, v.y, v.z);
            m->boundingBox.grow(vertices[j]);
        }
        rtcUnmapTriangleBuffer(accel);
        rtcUnmapPositionBuffer(accel);
        rtcBuildAccel(accel, "objectsplit");

        if ((int32_t) m_primitives.size() < meshId + 1) {
            m_primitives.resize(meshId + 1);
            initPrimitive(meshId);
        }
        else {
            freePrimitive(meshId);
        }

        m_primitives[meshId].accelerators.push_back(accel);
        m_primitives[meshId].intersectors1.push_back(rtcQueryIntersector1(accel, "default"));
        m_primitives[meshId].intersectors4.push_back(rtcQueryIntersector4(accel, "hybrid"));
        m_primitives[meshId].times.push_back(0.f);
        
        rtcCleanupGeometry(accel);
        //e::rtcSetVerbose(1);
        m->processed = true;
        return 0;
    }

    for (int32_t i = 0; i < m->vertexAttributes[m->positionAttrIndex].frameCount-1; ++i)
    {
        e::RTCGeometry * accel = embree::rtcNewTriangleMesh(triangleCount, countVertices * 2, "bvh4mb.triangle4i");
        e::RTCTriangle * triangles = (e::RTCTriangle*) e::rtcMapTriangleBuffer(accel);
        e::Vec3fa * vertices = (e::Vec3fa*) e::rtcMapPositionBuffer(accel);

        const float * positions0 = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat[i];
        const float * positions1 = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat[i+1];

        //shndebug("SceneImpl::computeAccelerationStructure : Animated Mesh -> %d : %d faces %d frames", meshId, triangleCount, positions->frameCount);

        for (int32_t j = 0; j < triangleCount; ++j)
        {
            int vId1 = topology[j*3+0];
            int vId2 = topology[j*3+1];
            int vId3 = topology[j*3+2];
            //shndebug("SceneImpl::computeAccelerationStructure : Face -> %lu [%d : %d : %d]", j, vId1, vId2, vId3);
            int32_t id0 = (meshId | 0x80000000);
            triangles[j] = embree::RTCTriangle(vId1*2, vId2*2, vId3*2, id0, j);
        }

        for (int32_t j = 0, k = countVertices; j < k; ++j)
        {
            e::Vec3fa v0(positions0[j*3], positions0[j*3 + 1], positions0[j*3 + 2]);
            e::Vec3fa v1(positions1[j*3], positions1[j*3 + 1], positions1[j*3 + 2]);

            e::Vec3fa mv = e::Vec3fa(0.f);
            mv = v1 - v0;
            vertices[j*2] = e::Vec3fa(v0.x, v0.y, v0.z);
            vertices[j*2+1] = e::Vec3fa(mv.x, mv.y, mv.z);
            m->boundingBox.grow(v0);
            m->boundingBox.grow(v1);
        }
        rtcUnmapTriangleBuffer(accel);
        rtcUnmapPositionBuffer(accel);
        rtcBuildAccel(accel, "objectsplit");

        if ((int32_t) m_primitives.size() < meshId + 1) {
            m_primitives.resize(meshId + 1);
            initPrimitive(meshId);
        }

        m_primitives[meshId].accelerators.push_back(accel);
        m_primitives[meshId].intersectors1.push_back(rtcQueryIntersector1(accel, "default"));
        m_primitives[meshId].intersectors4.push_back(0);
        m_primitives[meshId].times.push_back(m->vertexAttributes[m->positionAttrIndex].timeValues[i]);
        
        rtcCleanupGeometry(accel);
    }
    m->processed = true;
    return 0;
}
#endif

int32_t SceneImpl::computeDisplacement(const std::vector<size_t> & instanceIds, OslRenderer * renderer)
{
    std::unordered_set<Scene::Id> alreadyProcessedMeshes;
    for (int32_t i = 0; i < (int32_t)instanceIds.size(); ++i)
    {
        Instance * instance = &m_instances[instanceIds[i]];
        Scene::Id meshId = instance->meshId;

        // Don't add twice a mesh to displacement queue.
        // This is a limitation in Shining Instantiation, due to Displacement pre-processing.
        // We can't use different transforms and displacement shading trees to displace an instantiated mesh,
        // because the mesh will not be the same for each instances, and we break the Instantiation paradigm.
        // We choose to not support instantiated displace with different shading tree, or a instance-transform-dependent shading tree (eg. with Noise3D node), yet.
        if (!alreadyProcessedMeshes.insert(meshId).second)
            continue;

        assert(meshId < m_meshes.size());
        Mesh * m = &m_meshes[meshId];
        int32_t displaced = m->processingAttributes.get<int32_t>(Attr_Displaced, 0);
        if (m->processed || m->positionAttrIndex < 0 || m->vertexAttributes[m->positionAttrIndex].count <= 0 || displaced == 0)
            continue;

        // If the top mesh is a displacement one, we don't need to recreate it, but only to reuse it
        if (m->step != Mesh::DISPLACEMENT)
        {
            // create displaced mesh (copy of the uppermost base mesh - original or subdivided mesh)
            Mesh * result = nullptr;
            std::tie(m, result) = allocateOneMeshDataLevel(meshId);
            assert(&m_meshes[meshId] == result);
            result->copy(*m);
            result->step = Mesh::DISPLACEMENT;
        }

        Mesh * displacedMesh = &m_meshes[meshId]; // get again the pointer, because it could be swapped before
        assert(displacedMesh->baseMesh);
        Mesh * notDisplacedMesh = displacedMesh->baseMesh;

        // gather unique vertices from topology
        int32_t uniqueVerticesCount = 0;
        int32_t * uniqueVtxTopoIds = (int32_t *) displacedMesh->processingAttributes.getPtr(Attr_DispUniqueVtxTopoIds);
        if (!uniqueVtxTopoIds)
        {
            struct TopoToVertex
            {
                TopoToVertex(): topologyId(0), vertexId(0)
                {
                }
                TopoToVertex(int32_t topo, int32_t vertex): topologyId(topo), vertexId(vertex)
                {
                }
                inline bool operator==(const TopoToVertex & other) const
                {
                    return vertexId == other.vertexId;
                }
                inline bool operator<(const TopoToVertex & other) const
                {
                    return vertexId < other.vertexId;
                }

                int32_t topologyId;
                int32_t vertexId;
            };

            std::vector<TopoToVertex> topoVertexTuple;
            int32_t topoPosCount = notDisplacedMesh->topologies[notDisplacedMesh->vertexAttributes[notDisplacedMesh->positionAttrIndex].topology].count;
            for (int32_t j = 0; j < topoPosCount; ++j)
                topoVertexTuple.push_back(TopoToVertex(j, notDisplacedMesh->topologies[notDisplacedMesh->vertexAttributes[notDisplacedMesh->positionAttrIndex].topology].dataInt[j]));
            std::sort(topoVertexTuple.begin(), topoVertexTuple.end());
            auto it = std::unique(topoVertexTuple.begin(), topoVertexTuple.end());
            topoVertexTuple.resize(std::distance(topoVertexTuple.begin(), it));
            uniqueVerticesCount = static_cast<int32_t>(topoVertexTuple.size());
            uniqueVtxTopoIds = new int32_t[uniqueVerticesCount];
            for (int32_t j = 0; j < uniqueVerticesCount; ++j)
            {
                uniqueVtxTopoIds[j] = topoVertexTuple[j].topologyId;
            }
            displacedMesh->processingAttributes.set<int32_t>(Attr_DispUniqueVtxTopoIds, ATTR_TYPE_NAMES[ATTR_TYPE_INT], uniqueVtxTopoIds, uniqueVerticesCount, 1, Scene::USE_BUFFER);
        }
        else
        {
            uniqueVerticesCount = displacedMesh->processingAttributes.getAttribute(Attr_DispUniqueVtxTopoIds)->count;
        }

        int32_t frameCount = notDisplacedMesh->vertexAttributes[notDisplacedMesh->positionAttrIndex].frameCount;
        for (int32_t j = 0; j < frameCount; ++j)
        {
            if (frameCount == 1)
            {
                M44f localToWorld = instance->itransform.transforms[0];
                renderer->setDisplacement(DisplacementData(
                    (const float *) notDisplacedMesh->vertexAttributes[notDisplacedMesh->positionAttrIndex].dataFloat, (const float *) notDisplacedMesh->vertexAttributes[notDisplacedMesh->normalAttrIndex].dataFloat,
                    (const float *) notDisplacedMesh->vertexAttributes[notDisplacedMesh->texCoordAttrIndex].dataFloat, notDisplacedMesh->vertexAttributes[notDisplacedMesh->texCoordAttrIndex].countComponent, uniqueVtxTopoIds,
                    notDisplacedMesh->topologies[notDisplacedMesh->vertexAttributes[notDisplacedMesh->positionAttrIndex].topology].dataInt, notDisplacedMesh->topologies[notDisplacedMesh->vertexAttributes[notDisplacedMesh->normalAttrIndex].topology].dataInt,
                    notDisplacedMesh->topologies[notDisplacedMesh->vertexAttributes[notDisplacedMesh->texCoordAttrIndex].topology].dataInt, (float *) displacedMesh->vertexAttributes[displacedMesh->positionAttrIndex].dataFloat,
                    uniqueVerticesCount, 0, instanceIds[i], localToWorld));
            }
            else
            {
                // case of animated mesh
                float time = notDisplacedMesh->vertexAttributes[notDisplacedMesh->positionAttrIndex].timeValues[j];
                M44f localToWorld = slerp(instance->itransform, time);
                renderer->setDisplacement(DisplacementData(
                    notDisplacedMesh->vertexAttributes[notDisplacedMesh->positionAttrIndex].dataFloat[j], notDisplacedMesh->vertexAttributes[notDisplacedMesh->normalAttrIndex].dataFloat[j],
                    (const float *) notDisplacedMesh->vertexAttributes[notDisplacedMesh->texCoordAttrIndex].dataFloat, notDisplacedMesh->vertexAttributes[notDisplacedMesh->texCoordAttrIndex].countComponent, uniqueVtxTopoIds,
                    notDisplacedMesh->topologies[notDisplacedMesh->vertexAttributes[notDisplacedMesh->positionAttrIndex].topology].dataInt, notDisplacedMesh->topologies[notDisplacedMesh->vertexAttributes[notDisplacedMesh->normalAttrIndex].topology].dataInt,
                    notDisplacedMesh->topologies[notDisplacedMesh->vertexAttributes[notDisplacedMesh->texCoordAttrIndex].topology].dataInt,
                    (float *) displacedMesh->vertexAttributes[displacedMesh->positionAttrIndex].dataFloat[j], uniqueVerticesCount, time, instanceIds[i], localToWorld));
            }
        }
    }
    renderer->commitDisplacement(this);

    return 0;
}

int32_t SceneImpl::computeNormals(Scene::Id meshId, ComputeNormalCause cause)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &m_meshes[meshId];

    if (m->processed)
        return 1;

    if (m->normalAttrIndex == -1)
        return 1;

    bool needRecompute = false;
    if (cause == ComputeNormalCause::AFTER_SUBDIVISION)
    {
        // subdiv conditions
        const int32_t subdivLevel = m->processingAttributes.get<int32_t>(Attr_SubdivLevel, 0);
        const bool computeNormals = (bool)m->processingAttributes.get<int32_t>(Attr_SubdivComputeNormals, 1);
        const bool onlyDisplacement = (bool) m->processingAttributes.get<int32_t>(Attr_DispOnlyDisplacement, 0);
        needRecompute = subdivLevel > 0 && computeNormals && !onlyDisplacement;
    }
    else if (cause == ComputeNormalCause::AFTER_DISPLACEMENT)
    {
        // displacement condition
        const int32_t displaced = m->processingAttributes.get<int32_t>(Attr_Displaced, 0);
        needRecompute = (displaced == 1);
    }

    if (!needRecompute)
        return 2;

    int32_t countVertices = m->vertexAttributes[m->positionAttrIndex].count;
    int32_t faceCount = m->faces.count;
    int32_t triangleCount = m->faces.dataInt ? std::accumulate(m->faces.dataInt, m->faces.dataInt + faceCount, 0, [](int x, int y) { return x + (y - 2); }) : faceCount;
    SceneImpl::Mesh::VertexAttribute * positions = &m->vertexAttributes[m->positionAttrIndex];
    SceneImpl::Mesh::VertexAttribute * normals = &m->vertexAttributes[m->normalAttrIndex];

    const int32_t * const tVertex = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].dataInt;
    const int32_t * const tNormal = m->topologies[m->vertexAttributes[m->normalAttrIndex].topology].dataInt;
    const int32_t countComponent0 = positions->countComponent;
    const int32_t countComponent1 = normals->countComponent;
    const int32_t frameCount = positions->frameCount;

    for (int32_t i = 0; i < frameCount; ++i)
    {
        const float * __restrict__ va0 = (const float *) positions->dataFloat;
        float * va1 = (float *) normals->dataFloat;
        if (frameCount > 1)
        {
            va0 = (const float *) positions->dataFloat[i];
            va1 = (float *) normals->dataFloat[i];
        }

        memset(va1, 0, normals->count * countComponent1 * sizeof(float));
        for (int32_t j = 0; j < triangleCount; ++j)
        {
            int vId1 = tVertex[j * 3 + 0];
            int vId2 = tVertex[j * 3 + 1];
            int vId3 = tVertex[j * 3 + 2];
            int nId1 = tNormal[j * 3 + 0];
            int nId2 = tNormal[j * 3 + 1];
            int nId3 = tNormal[j * 3 + 2];
            V3f va01(va0[vId1 * countComponent0], va0[vId1 * countComponent0 + 1], va0[vId1 * countComponent0 + 2]);
            V3f va02(va0[vId2 * countComponent0], va0[vId2 * countComponent0 + 1], va0[vId2 * countComponent0 + 2]);
            V3f va03(va0[vId3 * countComponent0], va0[vId3 * countComponent0 + 1], va0[vId3 * countComponent0 + 2]);
            V3f n0 = ((va02 - va01).cross(va03 - va01)).normalize();
            va1[nId1 * countComponent1] += n0[0];
            va1[nId1 * countComponent1 + 1] += n0[1];
            va1[nId1 * countComponent1 + 2] += n0[2];
            va1[nId2 * countComponent1] += n0[0];
            va1[nId2 * countComponent1 + 1] += n0[1];
            va1[nId2 * countComponent1 + 2] += n0[2];
            va1[nId3 * countComponent1] += n0[0];
            va1[nId3 * countComponent1 + 1] += n0[1];
            va1[nId3 * countComponent1 + 2] += n0[2];
        }
        for (int32_t j = 0; j < triangleCount; ++j)
        {
            int nId1 = tNormal[j * 3 + 0];
            int nId2 = tNormal[j * 3 + 1];
            int nId3 = tNormal[j * 3 + 2];
            V3f va11(va1[nId1 * countComponent1], va1[nId1 * countComponent1 + 1], va1[nId1 * countComponent1 + 2]);
            V3f va12(va1[nId2 * countComponent1], va1[nId2 * countComponent1 + 1], va1[nId2 * countComponent1 + 2]);
            V3f va13(va1[nId3 * countComponent1], va1[nId3 * countComponent1 + 1], va1[nId3 * countComponent1 + 2]);
            va11.normalize();
            va12.normalize();
            va13.normalize();
            va1[nId1 * countComponent1] = va11[0];
            va1[nId1 * countComponent1 + 1] = va11[1];
            va1[nId1 * countComponent1 + 2] = va11[2];
            va1[nId2 * countComponent1] = va12[0];
            va1[nId2 * countComponent1 + 1] = va12[1];
            va1[nId2 * countComponent1 + 2] = va12[2];
            va1[nId3 * countComponent1] = va13[0];
            va1[nId3 * countComponent1 + 1] = va13[1];
            va1[nId3 * countComponent1 + 2] = va13[2];
        }
    }
    return 0;
}

int32_t SceneImpl::computeInstanceFaceAreaCDF(Scene::Id instanceId, const OslRenderer * renderer)
{
    assert(instanceId < m_instances.size());
    Instance * instance = &(m_instances[instanceId]);
    if (!renderer->isEmissiveInstance(instance->name.c_str()))
        return 1;

    const Mesh * const m = &(m_meshes[instance->meshId]);
    const int32_t faceCount = m->faces.count;
    if (m->processed ||faceCount <= 0)
        return 1;

    const Mesh::VertexAttribute * const posVAttr = &(m->vertexAttributes[m->positionAttrIndex]);
    const Attribute * const posTopology = &(m->topologies[posVAttr->topology]);
    const float * cdfTimes = nullptr;
    int32_t cdfFrameCount = 1;
    if (posVAttr->frameCount != 0 || instance->itransform.frameCount != 0)
    {
        if (posVAttr->frameCount > instance->itransform.frameCount) // use vertices position times
        {
            cdfTimes = posVAttr->timeValues;
            cdfFrameCount = posVAttr->frameCount;
        }
        else // use transform matrix times
        {
            cdfTimes = &(instance->itransform.times[0]);
            cdfFrameCount = instance->itransform.frameCount;
        }
    }
    instance->faceAreaCDF.setTimes(cdfTimes, cdfFrameCount);
    instance->meshAreaTimes.insert(instance->meshAreaTimes.begin(), cdfTimes, cdfTimes + cdfFrameCount);
    instance->meshArea.resize(cdfFrameCount);

    for (int32_t f = 0; f < cdfFrameCount; ++f)
    {
        instance->faceAreaCDF.resizeDistribution(f, faceCount);
        float time = instance->faceAreaCDF.getTimeFromFrame(f);
        const float * positions = (const float *) posVAttr->dataFloat;
        if (posVAttr->frameCount > 1)
        {
            int32_t posFrame = std::upper_bound(posVAttr->timeValues, posVAttr->timeValues + posVAttr->frameCount, time) - posVAttr->timeValues - 1;
            positions = posVAttr->dataFloat[posFrame];
        }
        const M44f localToWorld = slerp(instance->itransform, time);

        const auto faceArea = [positions, posTopology, localToWorld, faceCount](int32_t faceId) -> float {
            const int32_t posId1 = posTopology->dataInt[faceId * 3 + 0];
            const int32_t posId2 = posTopology->dataInt[faceId * 3 + 1];
            const int32_t posId3 = posTopology->dataInt[faceId * 3 + 2];
            const V3f v1(positions[posId1 * 3], positions[posId1 * 3 + 1], positions[posId1 * 3 + 2]);
            const V3f v2(positions[posId2 * 3], positions[posId2 * 3 + 1], positions[posId2 * 3 + 2]);
            const V3f v3(positions[posId3 * 3], positions[posId3 * 3 + 1], positions[posId3 * 3 + 2]);
            const V3f wsV1 = multVecMatrix(localToWorld, v1);
            const V3f wsV2 = multVecMatrix(localToWorld, v2);
            const V3f wsV3 = multVecMatrix(localToWorld, v3);
            return length(cross(wsV2 - wsV1, wsV3 - wsV1)) * 0.5f;
        };

        buildDistribution1D(faceArea, instance->faceAreaCDF.getDistributionFromFrame(f), faceCount, &(instance->meshArea[f]));
    }

    return 0;
}

int32_t SceneImpl::subdivideMesh(Scene::Id meshId)
{
    assert(meshId < m_meshes.size());

    Mesh * base = NULL;
    Mesh * result = NULL;
    if (m_meshes[meshId].baseMesh)
    {
        result = &m_meshes[meshId];
        base = m_meshes[meshId].baseMesh;
    }
    else
    {
        base = &m_meshes[meshId];
    }

    if (result && result->processed)
        return 1;

    int32_t subdivLevel = base->processingAttributes.get<int32_t>(Attr_SubdivLevel, 0);
    const bool subdivAdaptive = static_cast<bool>(base->processingAttributes.get<int32_t>(Attr_SubdivAdaptive, 0));
    const bool onlyDisplacement = static_cast<bool>(base->processingAttributes.get<int32_t>(Attr_DispOnlyDisplacement, 0));
    if (subdivLevel > 0 && base->positionAttrIndex >= 0 && base->vertexAttributes[base->positionAttrIndex].count > 0 && !onlyDisplacement)
    {
        const int32_t uniformSubdivLevel = (subdivAdaptive) ? (subdivLevel / 2.f) + 0.5f : subdivLevel;
        const int32_t method = base->processingAttributes.get<int32_t>(Attr_SubdivMethod, 0);
        const int32_t vertex = base->processingAttributes.get<int32_t>(Attr_SubdivVertexInterpolateBoundary, 0);
        const int32_t fvar = base->processingAttributes.get<int32_t>(Attr_SubdivFVarInterpolateBoundary, 0);
        const int32_t prop = base->processingAttributes.get<int32_t>(Attr_SubdivFVarPropagateCornerValue, 0);
        const int32_t creasing = base->processingAttributes.get<int32_t>(Attr_SubdivCreasingMethod, 0);
        if (!result)
        {
            std::tie(base, result) = allocateOneMeshDataLevel(meshId);
            result->step = Mesh::SUBDIVISION;
        }
        assert(&m_meshes[meshId] == result);
        SubdivideUniformMesh(this, meshId, *result, *base, uniformSubdivLevel, method, vertex, fvar, !!prop, creasing);
#if 0 // 11-12-2018: Deactivate adaptive subdivision. It's never used and not fonctionnal on texcoord subdivisions
        if (subdivAdaptive)
        {
            const int32_t adaptiveSubdivLevel = subdivLevel - uniformSubdivLevel;
            base = result;
            result = nullptr;
            std::tie(base, result) = allocateOneMeshDataLevel(meshId);
            result->step = Mesh::SUBDIVISION;
            assert(&m_meshes[meshId] == result);
            SubdivideAdaptiveMesh(this, meshId, *result, *base, adaptiveSubdivLevel, method, vertex, fvar, !!prop, creasing);
        }
#endif
    }
    else if (m_meshes[meshId].faces.dataInt)
    {
        for (const auto & instance : m_instances)
        {
            if (instance.meshId == meshId)
            {
                SHN_LOGWARN << "Mesh named [" << instance.name << "] isn't triangulated. May cause future issues";
                break;
            }
        }

        // triangulate
        if (!result)
        {
            std::tie(base, result) = allocateOneMeshDataLevel(meshId);
        }
        TriangulateMesh(*result, *base);
    }
    return 0;
}

int32_t SubdivideUniformMesh(
    SceneImpl * scene, Scene::Id meshId, SceneImpl::Mesh & processed, SceneImpl::Mesh & base, int32_t maxLevel, int32_t method, int32_t vertexBoundary, int32_t fvarBoundary,
    bool fvarPropagateCornerValue, int32_t creasingMethod)
{
    osd3::Sdc::Options::VtxBoundaryInterpolation vbi;
    osd3::Sdc::Options::FVarLinearInterpolation fvli;
    convertSubdivisionOptions(vertexBoundary, fvarBoundary, fvarPropagateCornerValue, vbi, fvli);

    // Gather VertexAttribute data for OSD conversion
    const int32_t mainTopologyId = base.vertexAttributes[base.positionAttrIndex].topology;
    const Attribute & mainTopology = base.topologies[mainTopologyId];
    std::vector<int32_t> vertexSources; // vertexAttributes using the main topology
    std::vector<int32_t> fvarSources;
    for (int32_t i = 0; i < base.vertexAttributes.size(); ++i)
    {
        if (base.vertexAttributes[i].topology == mainTopologyId)
            vertexSources.push_back(i);
        else
            fvarSources.push_back(i);
    }
    int32_t fvarCount = fvarSources.size();

    typedef osd3::Far::TopologyDescriptor Descriptor;

    Descriptor osdMeshDesc;
    osdMeshDesc.numVertices = base.vertexAttributes[base.positionAttrIndex].count;
    osdMeshDesc.numFaces = base.faces.count;
    std::vector<int32_t> vertsPerFace(base.faces.count);
    for (int32_t f = 0; f < base.faces.count; ++f)
    {
        vertsPerFace[f] = base.faces.dataInt ? base.faces.dataInt[f] : 3;
    }
    osdMeshDesc.numVertsPerFace = &vertsPerFace[0];
    osdMeshDesc.vertIndicesPerFace = mainTopology.dataInt;

    // set creases data
    int32_t vCreaseCount = 0, dumbInt = 0;
    const float * vertexCreases = (const float *) base.processingAttributes.getPtr(Attr_VertexCreases, 0, &vCreaseCount, &dumbInt);
    std::vector<int32_t> vertexCreaseIndices;
    if (vCreaseCount == osdMeshDesc.numVertices && vertexCreases)
    {
        vertexCreaseIndices.resize(vCreaseCount);
        for (int32_t v = 0; v < vCreaseCount; ++v)
            vertexCreaseIndices[v] = v;
        osdMeshDesc.numCorners = vCreaseCount;
        osdMeshDesc.cornerVertexIndices = &vertexCreaseIndices[0];
        osdMeshDesc.cornerWeights = vertexCreases;
    }

    int32_t edgeCount = 0;
    const float * edgeCreases = (const float *) base.processingAttributes.getPtr(Attr_EdgeCreases, 0, &edgeCount, &dumbInt);
    const int32_t * vertsPerEdges = (const int32_t *) base.processingAttributes.getPtr(Attr_VertsPerEdge);
    if (edgeCount > 0 && edgeCreases && vertsPerEdges)
    {
        osdMeshDesc.numCreases = edgeCount;
        osdMeshDesc.creaseVertexIndexPairs = vertsPerEdges;
        osdMeshDesc.creaseWeights = edgeCreases;
    }

    std::vector<Descriptor::FVarChannel> channels;
    if (fvarCount > 0)
    {
        mergeFVarValues(base, fvarSources, fvarCount);
        channels.resize(fvarCount);
        for (int32_t i = 0; i < fvarCount; ++i)
        {
            channels[i].numValues = base.topologies[base.vertexAttributes[fvarSources[i]].topology].count;
            channels[i].valueIndices = base.topologies[base.vertexAttributes[fvarSources[i]].topology].dataInt;
        }

        osdMeshDesc.numFVarChannels = fvarCount;
        osdMeshDesc.fvarChannels = &channels[0];
    }

    // Refine topology
    osd3::Sdc::SchemeType type = (osd3::Sdc::SchemeType) method;
    osd3::Sdc::Options osdOptions;
    osdOptions.SetVtxBoundaryInterpolation(vbi);
    osdOptions.SetFVarLinearInterpolation(fvli);
    osdOptions.SetCreasingMethod(osd3::Sdc::Options::CreasingMethod(creasingMethod));
    osd3::Far::TopologyRefiner * refiner = osd3::Far::TopologyRefinerFactory<Descriptor>::Create(osdMeshDesc, osd3::Far::TopologyRefinerFactory<Descriptor>::Options(type, osdOptions));

    // sanity check
    osd3::Far::TopologyLevel level0 = refiner->GetLevel(0);
    for (int32_t f = 0; f < base.faces.count; ++f)
    {
        const osd3::Far::ConstIndexArray & fverts = level0.GetFaceVertices(f);
        const int32_t faceSize = fverts.size();

        if (type == osd3::Sdc::SCHEME_LOOP && faceSize != 3)
        {
            shndebug("Trying to create a Loop subdivision with non-triangle face");
            return -1;
        }
        for (int32_t i = 0; i < faceSize; ++i)
        {
            int32_t origin = fverts[i];
            int32_t destination = fverts[(i + 1) % faceSize];
            int32_t opposite = level0.FindEdge(origin, destination);
            if (origin < 0 || destination < 0)
            {
                shndebug("An edge was specified that connected a non existent vertex");
                return -1;
            }
            if (origin == destination)
            {
                shndebug("An edge was specified that connected a vertex to itself");
                return -1;
            }
            if (opposite >= 0 && level0.GetEdgeFaces(opposite).size() > 2)
            {
                shndebug("A non-manifold edge incident to more than 2 faces was found");
                return -1;
            }
        }
    }

    osd3::Far::TopologyRefiner::UniformOptions refineOptions(maxLevel);
    refineOptions.fullTopologyInLastLevel = true;
    refiner->RefineUniform(refineOptions);

    // Update Shining mesh attributes
    osd3::Far::TopologyLevel const & refLastLevel = refiner->GetLevel(maxLevel);
    const int32_t refinedFaceCount = refLastLevel.GetNumFaces();

    processed.positionAttrIndex = base.positionAttrIndex;
    processed.normalAttrIndex = base.normalAttrIndex;
    processed.texCoordAttrIndex = base.texCoordAttrIndex;
    processed.shadingGroupAttrIndex = -1;

    processed.faces.count = refinedFaceCount * 2; // must triangulate
    processed.faces.countComponent = 1;
    processed.faces.dataInt = nullptr; // triangles

    // Set main topology data
    scene->enableMeshTopology(meshId, mainTopologyId + 1);
    attrSet(
        &processed.topologies[mainTopologyId], processed.topologies[mainTopologyId].dataInt, processed.topologies[mainTopologyId].dataFloat,
        refinedFaceCount * 6, // 2 triangles/face
        1,
        (const int *) nullptr, // allocate only
        Scene::COPY_BUFFER);
    int32_t * vertexIds = const_cast<int32_t *>(processed.topologies[mainTopologyId].dataInt);
    for (int32_t f = 0; f < refinedFaceCount; ++f)
    {
        osd3::Far::ConstIndexArray fverts = refLastLevel.GetFaceVertices(f);
        assert(fverts.size() == 4);

        // We alternate the topology for each pair of triangles in order to break patterns
        // that may appear when the original topology is dirty.
        // (it may be visible after displacement when the subdiv level is not low enough
        // to match the frequency of the displacement texture)
        if (f % 2 == 0)
        {
            vertexIds[6 * f + 0] = fverts[0];
            vertexIds[6 * f + 1] = fverts[1];
            vertexIds[6 * f + 2] = fverts[2];

            vertexIds[6 * f + 3] = fverts[0];
            vertexIds[6 * f + 4] = fverts[2];
            vertexIds[6 * f + 5] = fverts[3];
        }
        else
        {
            vertexIds[6 * f + 0] = fverts[1];
            vertexIds[6 * f + 1] = fverts[2];
            vertexIds[6 * f + 2] = fverts[3];

            vertexIds[6 * f + 3] = fverts[3];
            vertexIds[6 * f + 4] = fverts[0];
            vertexIds[6 * f + 5] = fverts[1];
        }
    }

    // Set FVar topologies data
    for (int32_t i = 0; i < fvarCount; ++i)
    {
        // const int32_t refinedFVarFaceCount = refLastLevel.GetNumFVarValues(i)/4;
        scene->enableMeshTopology(meshId, i + 2); // 1-offset + main topology
        attrSet(
            &processed.topologies[i + 1], processed.topologies[i + 1].dataInt, processed.topologies[i + 1].dataFloat,
            refinedFaceCount * 6, // 2 triangles/face
            1,
            (const int *) nullptr, // allocate only
            Scene::COPY_BUFFER);
        int32_t * fvarIds = const_cast<int32_t *>(processed.topologies[i + 1].dataInt);
        for (int32_t f = 0; f < refinedFaceCount; ++f)
        {
            osd3::Far::ConstIndexArray fvverts = refLastLevel.GetFaceFVarValues(f, i);
            assert(fvverts.size() == 4);

            // See comment above
            if (f % 2 == 0)
            {
                fvarIds[6 * f + 0] = fvverts[0];
                fvarIds[6 * f + 1] = fvverts[1];
                fvarIds[6 * f + 2] = fvverts[2];

                fvarIds[6 * f + 3] = fvverts[0];
                fvarIds[6 * f + 4] = fvverts[2];
                fvarIds[6 * f + 5] = fvverts[3];
            }
            else
            {
                fvarIds[6 * f + 0] = fvverts[1];
                fvarIds[6 * f + 1] = fvverts[2];
                fvarIds[6 * f + 2] = fvverts[3];

                fvarIds[6 * f + 3] = fvverts[3];
                fvarIds[6 * f + 4] = fvverts[0];
                fvarIds[6 * f + 5] = fvverts[1];
            }
        }
    }

    // init shining mesh vertex attributes
    const int32_t attributeCount = base.vertexAttributes.size();
    assert(attributeCount == vertexSources.size() + fvarSources.size());
    scene->enableMeshVertexAttributes(meshId, attributeCount);
    for (int32_t i = 0; i < attributeCount; ++i)
    {
        processed.vertexAttributes[i].name = base.vertexAttributes[i].name;
        processed.vertexAttributes[i].topology = base.vertexAttributes[i].topology;
        processed.vertexAttributes[i].countComponent = base.vertexAttributes[i].countComponent;
        processed.vertexAttributes[i].frameCount = base.vertexAttributes[i].frameCount;
    }
    osd3::Far::PrimvarRefiner primvarRefiner(*refiner);

    // Interpolate vertex attributes
    for (int32_t a = 0; a < vertexSources.size(); ++a)
    {
        int32_t attrId = vertexSources[a];
        auto & baseAttr = base.vertexAttributes[attrId];
        auto & attr = processed.vertexAttributes[attrId];

        int32_t vbufferSize = refiner->GetNumVerticesTotal();
        std::vector<OSDVertex> vbuffer(vbufferSize);
        OSDVertex * verts = &vbuffer[0];
        int32_t refinedVertexCount = refLastLevel.GetNumVertices();
        int32_t refinedFirstVertexOffset = vbufferSize - refinedVertexCount;

        if (attr.frameCount > 1)
        {
            attrTimedSet(
                &attr, attr.dataFloat, attr.dataInt, refinedVertexCount, attr.countComponent, attr.frameCount, baseAttr.timeValues, (const float **) nullptr /* allocate only */,
                Scene::COPY_BUFFER);
        }
        else
        {
            attrTimedSet(&attr, attr.dataFloat, attr.dataInt, refinedVertexCount, attr.countComponent, (const float *) nullptr /* allocate only */, Scene::COPY_BUFFER);
        }

        for (int32_t frame = 0; frame < attr.frameCount; ++frame)
        {
            // get base mesh data
            const float * srcDataFloat = (baseAttr.frameCount > 1) ? (const float *) baseAttr.dataFloat[frame] : (const float *) baseAttr.dataFloat;
            for (int v = 0, nVerts = baseAttr.count; v < nVerts; ++v)
            {
                for (int32_t i = 0; i < baseAttr.countComponent; ++i)
                    verts[v][i] = srcDataFloat[v * baseAttr.countComponent + i];
            }

            OSDVertex * srcVert = verts;
            for (int32_t lvl = 1; lvl <= maxLevel; ++lvl)
            {
                OSDVertex * dstVert = srcVert + refiner->GetLevel(lvl - 1).GetNumVertices();
                primvarRefiner.Interpolate(lvl, srcVert, dstVert);
                srcVert = dstVert;
            }

            // update Shining mesh
            float * dstDataFloat = (attr.frameCount > 1) ? (float *) attr.dataFloat[frame] : (float *) attr.dataFloat;
            for (int32_t v = 0; v < refinedVertexCount; ++v)
            {
                for (int32_t i = 0; i < attr.countComponent; ++i)
                    dstDataFloat[v * attr.countComponent + i] = verts[refinedFirstVertexOffset + v][i];
            }
        }
    }

    // Interpolate fvar attributes
    for (int32_t a = 0; a < fvarSources.size(); ++a)
    {
        int32_t attrId = fvarSources[a];
        auto & baseAttr = base.vertexAttributes[attrId];
        auto & attr = processed.vertexAttributes[attrId];

        int32_t fvbufferSize = refiner->GetNumFVarValuesTotal(a);
        std::vector<OSDVertex> fvbuffer(fvbufferSize);
        OSDVertex * fvars = &fvbuffer[0];
        int32_t refinedFVarCount = refLastLevel.GetNumFVarValues(a);
        int32_t refinedFirstFVarOffset = fvbufferSize - refinedFVarCount;

        if (attr.frameCount > 1)
        {
            attrTimedSet(
                &attr, attr.dataFloat, attr.dataInt, refinedFVarCount, attr.countComponent, attr.frameCount, baseAttr.timeValues, (const float **) nullptr /* allocate only */,
                Scene::COPY_BUFFER);
        }
        else
        {
            attrTimedSet(&attr, attr.dataFloat, attr.dataInt, refinedFVarCount, attr.countComponent, (const float *) nullptr /* allocate only */, Scene::COPY_BUFFER);
        }

        for (int32_t frame = 0; frame < attr.frameCount; ++frame)
        {
            // get base mesh data
            const float * srcDataFloat = (baseAttr.frameCount > 1) ? (const float *) baseAttr.dataFloat[frame] : (const float *) baseAttr.dataFloat;
            for (int fv = 0, nFVars = baseAttr.count; fv < nFVars; ++fv)
            {
                for (int32_t i = 0; i < baseAttr.countComponent; ++i)
                    fvars[fv][i] = srcDataFloat[fv * baseAttr.countComponent + i];
            }

            OSDVertex * srcFVars = fvars;
            for (int lvl = 1; lvl <= maxLevel; ++lvl)
            {
                OSDVertex * dstFVars = srcFVars + refiner->GetLevel(lvl - 1).GetNumFVarValues(a);
                primvarRefiner.InterpolateFaceVarying(lvl, srcFVars, dstFVars, a);
                srcFVars = dstFVars;
            }

            // update Shining mesh
            float * dstDataFloat = (attr.frameCount > 1) ? (float *) attr.dataFloat[frame] : (float *) attr.dataFloat;
            for (int32_t fv = 0; fv < refinedFVarCount; ++fv)
            {
                for (int32_t i = 0; i < attr.countComponent; ++i)
                    dstDataFloat[fv * attr.countComponent + i] = fvars[refinedFirstFVarOffset + fv][i];
            }
        }
    }

    delete refiner;

    return 0;
}

osd3::Far::ConstIndexArray getFVarPatchVertices(const osd3::Far::PatchTable * patchTable, int32_t patchId)
{
    return osd3::Far::ConstIndexArray(&(patchTable->GetFVarValues(0)[patchId * 4]), 4);
}

// evaluate limit on patch using (s,t) coordinate
typedef std::pair<float, float> PUV;
OSDVertex evalLimit(int32_t faceId, float s, float t, osd3::Far::PatchMap & patchmap, const osd3::Far::PatchTable * patchTable, OSDVertex * vertices)
{
    osd3::Far::PatchTable::PatchHandle const * handle = patchmap.FindPatch(faceId, s, t);
    assert(handle);
    float pWeights[20], dsWeights[20], dtWeights[20];
    patchTable->EvaluateBasis(*handle, s, t, pWeights, dsWeights, dtWeights);
    osd3::Far::ConstIndexArray const cvs = patchTable->GetPatchVertices(*handle);
    OSDVertex vert;
    for (int cv = 0; cv < cvs.size(); ++cv)
        vert.AddWithWeight(vertices[cvs[cv]], pWeights[cv]);
    return vert;
}

OSDVertex mix(OSDVertex v1, OSDVertex v2, float a)
{
    return (v1 * (1 - a)) + (v2 * a);
}

OSDVertex evalFVarLimit(int32_t faceId, osd3::Far::ConstIndexArray & cvs, const PUV & patchUV, OSDVertex * faceVaryings)
{
    OSDVertex controlFVars[4];
    for (int32_t i = 0; i < 4; ++i)
        controlFVars[i] = faceVaryings[cvs[i]];

    OSDVertex fvar;
    fvar = mix(mix(controlFVars[0], controlFVars[1], patchUV.first), mix(controlFVars[3], controlFVars[2], patchUV.first), patchUV.second);
    return fvar;
}

PUV patternTransition[15][24] = {
    // 1
    { PUV(1.f, 1.f),   PUV(0.5f, 0.f),  PUV(0.f, 1.f),   PUV(1.f, 1.f),   PUV(1.f, 0.f),   PUV(0.5f, 0.f),  PUV(0.f, 1.f),   PUV(0.5f, 0.f),
      PUV(0.f, 0.f),   PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 2
    { PUV(0.f, 1.f),   PUV(1.f, 0.5f),  PUV(0.f, 0.f),   PUV(0.f, 1.f),   PUV(1.f, 1.f),   PUV(1.f, 0.5f),  PUV(0.f, 0.f),   PUV(1.f, 0.5f),
      PUV(1.f, 0.f),   PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 3
    { PUV(0.f, 0.f),   PUV(0.f, 1.f),   PUV(0.5f, 0.f),  PUV(0.f, 1.f),   PUV(1.f, 1.f),   PUV(1.f, 0.5f),  PUV(0.5f, 0.f),  PUV(1.f, 0.5f),
      PUV(1.f, 0.f),   PUV(0.f, 1.f),   PUV(1.f, 0.5f),  PUV(0.5f, 0.f),  PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 4
    { PUV(0.f, 0.f),   PUV(0.5f, 1.f),  PUV(1.f, 0.f),   PUV(0.f, 0.f),   PUV(0.f, 1.f),   PUV(0.5f, 1.f),  PUV(1.f, 0.f),   PUV(0.5f, 1.f),
      PUV(1.f, 1.f),   PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 5
    { PUV(0.f, 0.f),   PUV(0.f, 1.f),   PUV(0.5f, 0.f),  PUV(0.f, 1.f),   PUV(0.5f, 0.f),  PUV(0.5f, 1.f),  PUV(0.5f, 1.f),  PUV(0.5f, 0.f),
      PUV(1.f, 1.f),   PUV(0.5f, 0.f),  PUV(1.f, 1.f),   PUV(1.f, 0.f),   PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 6
    { PUV(0.f, 0.f),   PUV(0.f, 1.f),   PUV(0.5f, 1.f),  PUV(0.f, 0.f),   PUV(1.f, 0.5f),  PUV(1.f, 0.f),   PUV(0.5f, 1.f),  PUV(1.f, 1.f),
      PUV(1.f, 0.5f),  PUV(0.f, 0.f),   PUV(0.5, 1.f),   PUV(1.f, 0.5f),  PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 7
    { PUV(0.f, 0.f),   PUV(0.f, 1.f),   PUV(0.5f, 0.f),  PUV(0.f, 1.f),   PUV(0.5f, 1.f),  PUV(0.5f, 0.f),  PUV(0.5f, 1.f),  PUV(1.f, 1.f),
      PUV(1.f, 0.5f),  PUV(0.5f, 1.f),  PUV(1.f, 0.5f),  PUV(0.5f, 0.f),  PUV(0.5f, 0.f),  PUV(1.f, 0.5f),  PUV(1.f, 0.f),   PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 8
    { PUV(1.f, 0.f),   PUV(0.f, 0.5f),  PUV(1.f, 1.f),   PUV(1.f, 0.f),   PUV(0.f, 0.f),   PUV(0.f, 0.5f),  PUV(1.f, 1.f),   PUV(0.f, 0.5f),
      PUV(0.f, 1.f),   PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 9
    { PUV(0.f, 0.f),   PUV(0.f, 0.5f),  PUV(0.5f, 0.f),  PUV(0.f, 0.5f),  PUV(0.f, 1.f),   PUV(1.f, 1.f),   PUV(0.f, 0.5f),  PUV(1.f, 1.f),
      PUV(0.5f, 0.f),  PUV(0.5f, 0.f),  PUV(1.f, 1.f),   PUV(1.f, 0.f),   PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 10
    { PUV(0.f, 0.f),   PUV(0.f, 0.5f),  PUV(1.f, 0.5f),  PUV(0.f, 0.f),   PUV(1.f, 0.5f),  PUV(1.f, 0.f),   PUV(0.f, 0.5f),  PUV(1.f, 1.f),
      PUV(1.f, 0.5f),  PUV(0.f, 0.5f),  PUV(0.f, 1.f),   PUV(1.f, 1.f),   PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 11
    { PUV(0.f, 0.f),   PUV(0.f, 0.5f),  PUV(0.5f, 0.f),  PUV(0.f, 0.5f),  PUV(1.f, 0.5f),  PUV(0.5f, 0.f),  PUV(0.5f, 0.f),  PUV(1.f, 0.5f),
      PUV(1.f, 0.f),   PUV(0.f, 0.5f),  PUV(1.f, 1.f),   PUV(1.f, 0.5f),  PUV(0.f, 0.5f),  PUV(0.f, 1.f),   PUV(1.f, 1.f),   PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 12
    { PUV(0.f, 0.f),   PUV(0.f, 0.5f),  PUV(1.f, 0.f),   PUV(0.f, 0.5f),  PUV(0.f, 1.f),   PUV(0.5f, 1.f),  PUV(0.f, 0.5f),  PUV(0.5f, 1.f),
      PUV(1.f, 0.f),   PUV(0.5f, 1.f),  PUV(1.f, 1.f),   PUV(1.f, 0.f),   PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 13
    { PUV(0.f, 0.f),   PUV(0.f, 0.5f),  PUV(0.5f, 0.f),  PUV(0.f, 0.5f),  PUV(0.5f, 1.f),  PUV(0.5f, 0.f),  PUV(0.f, 0.5f),  PUV(0.f, 1.f),
      PUV(0.5f, 1.f),  PUV(0.5f, 0.f),  PUV(0.5f, 1.f),  PUV(1.f, 0.f),   PUV(0.5f, 1.f),  PUV(1.f, 1.f),   PUV(1.f, 0.f),   PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 14
    { PUV(0.f, 0.f),   PUV(0.f, 0.5f),  PUV(1.f, 0.5f),  PUV(0.f, 0.f),   PUV(1.f, 0.5f),  PUV(1.f, 0.f),   PUV(0.f, 0.5f),  PUV(0.f, 1.f),
      PUV(0.5f, 1.f),  PUV(0.f, 0.5f),  PUV(0.5f, 1.f),  PUV(1.f, 0.5f),  PUV(0.5f, 1.f),  PUV(1.f, 1.f),   PUV(1.f, 0.5f),  PUV(-1.f, -1.f),
      PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f), PUV(-1.f, -1.f) },
    // 15
    { PUV(0.f, 0.f),  PUV(0.f, 0.5f),  PUV(0.5f, 0.5f), PUV(0.f, 0.f),  PUV(0.5f, 0.5f), PUV(0.5f, 0.f),  PUV(0.5f, 0.f), PUV(0.5f, 0.5f),
      PUV(1.f, 0.5f), PUV(0.5f, 0.f),  PUV(1.f, 0.5f),  PUV(1.f, 0.f),  PUV(0.f, 0.5f),  PUV(0.f, 1.f),   PUV(0.5f, 1.f), PUV(0.f, 0.5f),
      PUV(0.5f, 1.f), PUV(0.5f, 0.5f), PUV(0.5f, 0.5f), PUV(0.5f, 1.f), PUV(1.f, 1.f),   PUV(0.5f, 0.5f), PUV(1.f, 1.f),  PUV(1.f, 0.5f) }
};

// SceneImpl::Mesh & processed, SceneImpl::Mesh & base,
//     int32_t maxLevel, int32_t method, int32_t vertexBoundary, int32 fvarBoundary, bool fvarPropagateCornerValue, int32_t creasingMethod
int32_t SubdivideAdaptiveMesh(
    SceneImpl * scene, Scene::Id meshId, SceneImpl::Mesh & processed, SceneImpl::Mesh & base, int32_t maxLevel, int32_t method, int32_t vertexBoundary, int32_t fvarBoundary,
    bool fvarPropagateCornerValue, int32_t creasingMethod)
{
    // not ready yet
    assert(0);

    osd3::Sdc::Options::VtxBoundaryInterpolation vbi;
    osd3::Sdc::Options::FVarLinearInterpolation fvli;
    convertSubdivisionOptions(vertexBoundary, fvarBoundary, fvarPropagateCornerValue, vbi, fvli);

    // Gather VertexAttribute data for OSD conversion
    const int32_t mainTopologyId = base.vertexAttributes[base.positionAttrIndex].topology;
    const Attribute & mainTopology = base.topologies[mainTopologyId];
    std::vector<int32_t> vertexSources; // vertexAttributes using the main topology
    std::vector<int32_t> fvarSources;
    for (int32_t i = 0; i < base.vertexAttributes.size(); ++i)
    {
        if (base.vertexAttributes[i].topology == mainTopologyId)
            vertexSources.push_back(i);
        else
            fvarSources.push_back(i);
    }
    int32_t fvarCount = fvarSources.size();

    typedef osd3::Far::TopologyDescriptor Descriptor;

    Descriptor osdMeshDesc;
    osdMeshDesc.numVertices = base.vertexAttributes[base.positionAttrIndex].count;
    osdMeshDesc.numFaces = base.faces.count;
    std::vector<int32_t> vertsPerFace(base.faces.count);
    for (int32_t f = 0; f < base.faces.count; ++f)
    {
        vertsPerFace[f] = base.faces.dataInt ? base.faces.dataInt[f] : 3;
    }
    osdMeshDesc.numVertsPerFace = &vertsPerFace[0];
    osdMeshDesc.vertIndicesPerFace = mainTopology.dataInt;

    // set creases data
    int32_t vCreaseCount = 0, dumbInt = 0;
    const float * vertexCreases = (const float *) base.processingAttributes.getPtr(Attr_VertexCreases, 0, &vCreaseCount, &dumbInt);
    std::vector<int32_t> vertexCreaseIndices;
    if (vCreaseCount == osdMeshDesc.numVertices && vertexCreases)
    {
        vertexCreaseIndices.resize(vCreaseCount);
        for (int32_t v = 0; v < vCreaseCount; ++v)
            vertexCreaseIndices[v] = v;
        osdMeshDesc.numCorners = vCreaseCount;
        osdMeshDesc.cornerVertexIndices = &vertexCreaseIndices[0];
        osdMeshDesc.cornerWeights = vertexCreases;
    }

    int32_t edgeCount = 0;
    const float * edgeCreases = (const float *) base.processingAttributes.getPtr(Attr_EdgeCreases, 0, &edgeCount, &dumbInt);
    const int32_t * vertsPerEdges = (const int32_t *) base.processingAttributes.getPtr(Attr_VertsPerEdge);
    if (edgeCount > 0 && edgeCreases && vertsPerEdges)
    {
        osdMeshDesc.numCreases = edgeCount;
        osdMeshDesc.creaseVertexIndexPairs = vertsPerEdges;
        osdMeshDesc.creaseWeights = edgeCreases;
    }

    std::vector<Descriptor::FVarChannel> channels;
    if (fvarCount > 0)
    {
        mergeFVarValues(base, fvarSources, fvarCount);
        channels.resize(fvarCount);
        for (int32_t i = 0; i < fvarCount; ++i)
        {
            channels[i].numValues = base.topologies[base.vertexAttributes[fvarSources[i]].topology].count;
            channels[i].valueIndices = base.topologies[base.vertexAttributes[fvarSources[i]].topology].dataInt;
        }

        osdMeshDesc.numFVarChannels = fvarCount;
        osdMeshDesc.fvarChannels = &channels[0];
    }

    // Refine topology
    osd3::Sdc::SchemeType type = (osd3::Sdc::SchemeType) method;
    osd3::Sdc::Options osdOptions;
    osdOptions.SetVtxBoundaryInterpolation(vbi);
    osdOptions.SetFVarLinearInterpolation(fvli);
    osdOptions.SetCreasingMethod(osd3::Sdc::Options::CreasingMethod(creasingMethod));
    osd3::Far::TopologyRefiner * refiner = osd3::Far::TopologyRefinerFactory<Descriptor>::Create(osdMeshDesc, osd3::Far::TopologyRefinerFactory<Descriptor>::Options(type, osdOptions));

    // sanity check
    osd3::Far::TopologyLevel level0 = refiner->GetLevel(0);
    for (int32_t f = 0; f < base.faces.count; ++f)
    {
        const osd3::Far::ConstIndexArray & fverts = level0.GetFaceVertices(f);
        const int32_t faceSize = fverts.size();

        if (type == osd3::Sdc::SCHEME_LOOP && faceSize != 3)
        {
            shndebug("Trying to create a Loop subdivision with non-triangle face");
            return -1;
        }
        for (int32_t i = 0; i < faceSize; ++i)
        {
            int32_t origin = fverts[i];
            int32_t destination = fverts[(i + 1) % faceSize];
            int32_t opposite = level0.FindEdge(origin, destination);
            if (origin < 0 || destination < 0)
            {
                shndebug("An edge was specified that connected a non existent vertex");
                return -1;
            }
            if (origin == destination)
            {
                shndebug("An edge was specified that connected a vertex to itself");
                return -1;
            }
            if (opposite >= 0 && level0.GetEdgeFaces(opposite).size() > 2)
            {
                shndebug("A non-manifold edge incident to more than 2 faces was found");
                return -1;
            }
        }
    }

    osd3::Far::TopologyRefiner::AdaptiveOptions adaptOpts(maxLevel);
    adaptOpts.useSingleCreasePatch = true;
    refiner->RefineAdaptive(adaptOpts);
    osd3::Far::PrimvarRefiner primvarRefiner(*refiner);
    osd3::Far::PatchTableFactory::Options patchOptions(maxLevel);
    patchOptions.endCapType = osd3::Far::PatchTableFactory::Options::ENDCAP_GREGORY_BASIS;
    patchOptions.generateFVarTables = true;
    patchOptions.shareEndCapPatchPoints = false;
    const osd3::Far::PatchTable * patchTable = osd3::Far::PatchTableFactory::Create(*refiner, patchOptions);
    osd3::Far::PatchMap patchmap(*patchTable);

    // init shining mesh
    const int32_t attributeCount = base.vertexAttributes.size();
    const int32_t topologyCount = base.topologies.size();
    assert(attributeCount == vertexSources.size() + fvarSources.size());
    scene->enableMeshVertexAttributes(meshId, attributeCount);
    scene->enableMeshTopology(meshId, topologyCount);

    processed.positionAttrIndex = base.positionAttrIndex;
    processed.normalAttrIndex = base.normalAttrIndex;
    processed.texCoordAttrIndex = base.texCoordAttrIndex;
    processed.shadingGroupAttrIndex = -1;

    // Interpolate vertex attributes
    for (int32_t a = 0; a < attributeCount; ++a)
    {
        bool isFVar = (a >= vertexSources.size());
        int32_t channelId = (isFVar) ? a - vertexSources.size() : 0;

        int32_t attrId = (!isFVar) ? vertexSources[a] : fvarSources[channelId];
        auto & baseAttr = base.vertexAttributes[attrId];
        auto & attr = processed.vertexAttributes[attrId];
        attr.name = baseAttr.name;
        attr.topology = baseAttr.topology;
        attr.count = 0;
        attr.countComponent = baseAttr.countComponent;
        attr.frameCount = baseAttr.frameCount;

        for (int32_t frame = 0; frame < attr.frameCount; ++frame)
        {
            int32_t refinedDataCount = (!isFVar) ? refiner->GetNumVerticesTotal() + patchTable->GetNumLocalPoints() : refiner->GetNumFVarValuesTotal(channelId);
            std::vector<OSDVertex> vbuffer(refinedDataCount);
            OSDVertex * values = &vbuffer[0];
            const float * srcDataFloat = (baseAttr.frameCount > 1) ? (const float *) baseAttr.dataFloat[frame] : (const float *) baseAttr.dataFloat;
            for (int32_t v = 0; v < baseAttr.count; ++v)
            {
                for (int32_t i = 0; i < baseAttr.countComponent; ++i)
                    values[v][i] = srcDataFloat[v * baseAttr.countComponent + i];
            }

            OSDVertex * srcValues = values;
            for (int32_t lvl = 1; lvl <= maxLevel; ++lvl)
            {
                OSDVertex * dstValues;
                if (!isFVar)
                {
                    dstValues = srcValues + refiner->GetLevel(lvl - 1).GetNumVertices();
                    primvarRefiner.Interpolate(lvl, srcValues, dstValues);
                }
                else
                {
                    dstValues = srcValues + refiner->GetLevel(lvl - 1).GetNumFVarValues(channelId);
                    primvarRefiner.InterpolateFaceVarying(lvl, srcValues, dstValues, channelId);
                }
                srcValues = dstValues;
            }

            if (!isFVar)
                patchTable->ComputeLocalPointValues(&values[0], &values[refiner->GetNumVerticesTotal()]);

            assert(attr.topology < topologyCount);
            bool isTopoProcessed = (processed.topologies[attr.topology].count > 0);
            std::vector<float> dataValues(refinedDataCount * attr.countComponent);
            for (int32_t v = 0; v < refinedDataCount; ++v)
            {
                const float * d = values[v].GetData();
                for (int32_t i = 0; i < attr.countComponent; ++i)
                    dataValues[attr.countComponent * v + i] = d[i];
            }
            std::vector<int32_t> topoIds;
            // buffer of depth per fvar vertex. Used for fvar topology reconstruction
            std::vector<int32_t> fvarDepth;
            if (isFVar)
                fvarDepth.resize(refinedDataCount);

            int32_t patchArraysCount = patchTable->GetNumPatchArrays();
            int32_t patchCursor = 0;
            for (int32_t i = 0; i < patchArraysCount; ++i)
            {
                osd3::Far::PatchDescriptor pDesc = patchTable->GetPatchArrayDescriptor(i);
                int32_t patchesCount = patchTable->GetNumPatches(i);
                for (int32_t j = 0; j < patchesCount; ++j)
                {
                    osd3::Far::PatchParam pParam = patchTable->GetPatchParam(i, j);
                    unsigned short transition = pParam.GetTransition();
                    osd3::Far::Index faceId = pParam.GetFaceId();
                    int32_t depth = pParam.GetDepth();
                    float gU = (float) pParam.GetU();
                    float gV = (float) pParam.GetV();
                    float frac = pParam.GetParamFraction();

                    osd3::Far::ConstIndexArray cvs = (!isFVar) ? patchTable->GetPatchVertices(i, j) : getFVarPatchVertices(patchTable, patchCursor);
                    ++patchCursor;

                    if (pDesc.GetType() == osd3::Far::PatchDescriptor::REGULAR)
                    {
                        // generate vertices attr from limit using quad UVs
                        std::vector<PUV> patchUVs(4); // { {0,0}, {1,0}, {1,1}, {0,1} };
                        std::vector<osd3::Far::Index> patchIds(4);
                        patchIds[0] = (!isFVar) ? cvs[5] : cvs[0];
                        patchUVs[0] = PUV(0.f, 0.f);
                        patchIds[1] = (!isFVar) ? cvs[6] : cvs[1];
                        patchUVs[1] = PUV(1.f, 0.f);
                        patchIds[2] = (!isFVar) ? cvs[10] : cvs[2];
                        patchUVs[2] = PUV(1.f, 1.f);
                        patchIds[3] = (!isFVar) ? cvs[9] : cvs[3];
                        patchUVs[3] = PUV(0.f, 1.f);

                        if (transition & 1)
                            patchUVs.push_back(PUV(0.5f, 0.f));
                        if (transition & 2)
                            patchUVs.push_back(PUV(1.f, 0.5f));
                        if (transition & 4)
                            patchUVs.push_back(PUV(0.5f, 1.f));
                        if (transition & 8)
                            patchUVs.push_back(PUV(0.f, 0.5f));
                        if (transition == 15)
                            patchUVs.push_back(PUV(0.5f, 0.5f));

                        int32_t addedVertices = 0;
                        for (int32_t k = 0; k < patchUVs.size(); ++k)
                        {
                            float s = gU * frac + patchUVs[k].first * frac;
                            float t = gV * frac + patchUVs[k].second * frac;
                            OSDVertex newValue = (!isFVar) ? evalLimit(faceId, s, t, patchmap, patchTable, values) : evalFVarLimit(faceId, cvs, patchUVs[k], values);

                            if (k < patchIds.size())
                            {
                                // update the vertex attribute value
                                dataValues[patchIds[k] * attr.countComponent + 0] = newValue[0];
                                dataValues[patchIds[k] * attr.countComponent + 1] = newValue[1];
                                dataValues[patchIds[k] * attr.countComponent + 2] = newValue[2];

                                if (isFVar)
                                    fvarDepth[patchIds[k]] = depth;
                            }
                            else
                            {
                                // add the new generated vertex
                                patchIds.push_back(dataValues.size() / attr.countComponent);
                                for (int32_t c = 0; c < attr.countComponent; ++c)
                                    dataValues.push_back(newValue[c]);
                                if (isFVar)
                                    fvarDepth.push_back(depth);

                                ++addedVertices;
                            }
                        }

                        if (!isTopoProcessed)
                        {
                            if (addedVertices == 0)
                            {
                                if (!isFVar)
                                {
                                    topoIds.push_back(cvs[5]);
                                    topoIds.push_back(cvs[6]);
                                    topoIds.push_back(cvs[10]);
                                    topoIds.push_back(cvs[10]);
                                    topoIds.push_back(cvs[9]);
                                    topoIds.push_back(cvs[5]);
                                }
                                else
                                {
                                    topoIds.push_back(cvs[0]);
                                    topoIds.push_back(cvs[1]);
                                    topoIds.push_back(cvs[2]);
                                    topoIds.push_back(cvs[2]);
                                    topoIds.push_back(cvs[3]);
                                    topoIds.push_back(cvs[0]);
                                }
                            }
                            else
                            {
                                // build transition patch topology
                                int32_t genTrisCount = 0;
                                if (addedVertices == 1)
                                    genTrisCount = 3;
                                else if (addedVertices == 2)
                                    genTrisCount = 4;
                                else if (addedVertices == 3)
                                    genTrisCount = 5;
                                else if (addedVertices == 5)
                                    genTrisCount = 8;

                                for (int32_t k = 0; k < 3 * genTrisCount; ++k)
                                {
                                    PUV crtUV = patternTransition[transition - 1][k];
                                    for (int32_t uv = 0; uv < patchUVs.size(); ++uv)
                                    {
                                        PUV refUV = patchUVs[uv];
                                        if (crtUV.first == refUV.first && crtUV.second == refUV.second)
                                        {
                                            topoIds.push_back(patchIds[uv]);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (pDesc.GetType() == osd3::Far::PatchDescriptor::GREGORY_BASIS && !isTopoProcessed)
                    {
                        if (!isFVar)
                        {
                            topoIds.push_back(cvs[0]);
                            topoIds.push_back(cvs[5]);
                            topoIds.push_back(cvs[10]);
                            topoIds.push_back(cvs[10]);
                            topoIds.push_back(cvs[15]);
                            topoIds.push_back(cvs[0]);
                        }
                        else
                        {
                            topoIds.push_back(cvs[0]);
                            topoIds.push_back(cvs[1]);
                            topoIds.push_back(cvs[2]);
                            topoIds.push_back(cvs[2]);
                            topoIds.push_back(cvs[3]);
                            topoIds.push_back(cvs[0]);
                        }
                    }
                    else if (!isTopoProcessed)
                    {
                        topoIds.push_back(cvs[0]);
                        topoIds.push_back(cvs[1]);
                        topoIds.push_back(cvs[2]);
                        topoIds.push_back(cvs[2]);
                        topoIds.push_back(cvs[3]);
                        topoIds.push_back(cvs[0]);
                    }
                }
            }

            // merge fvars of different vertices with the same position (fix uv artifacts between patches with different subdiv level)
            if (isFVar)
            {
                const float * posDataFloat = (processed.vertexAttributes[processed.positionAttrIndex].frameCount > 1)
                    ? (const float *) processed.vertexAttributes[processed.positionAttrIndex].dataFloat[frame]
                    : (const float *) processed.vertexAttributes[processed.positionAttrIndex].dataFloat;
                const int32_t * posTopoIds = processed.topologies[mainTopologyId].dataInt;
                int32_t posCountComponent = processed.vertexAttributes[processed.positionAttrIndex].countComponent;
                for (int32_t i = 0; i < topoIds.size(); ++i)
                {
                    OSDVertex v(&posDataFloat[posCountComponent * posTopoIds[i]]);
                    float * dstFVar = &dataValues[attr.countComponent * topoIds[i]];
                    OSDVertex refFVar(dstFVar);
                    float nearestDist = 1.f;
                    for (int32_t j = 0; j < topoIds.size(); ++j)
                    {
                        if (i != j)
                        {
                            OSDVertex otherV(&posDataFloat[posCountComponent * posTopoIds[j]]);
                            if (v == otherV && fvarDepth[topoIds[j]] > fvarDepth[topoIds[i]])
                            {
                                float * srcFVar = &dataValues[attr.countComponent * topoIds[j]];
                                OSDVertex testedFVar(srcFVar);
                                float dist = refFVar.Distance(testedFVar);
                                if (dist < nearestDist)
                                {
                                    memcpy(dstFVar, srcFVar, 3 * sizeof(float));
                                    nearestDist = dist;
                                }
                            }
                        }
                    }
                }
            }

            // update shining data
            int32_t finalDataCount = dataValues.size() / attr.countComponent;
            if (frame == 0)
            {
                // allocate vertex attribute buffers (only for the first frame to avoid multiple allocations)
                if (attr.frameCount > 1)
                {
                    attrTimedSet(&attr, attr.dataFloat, attr.dataInt, finalDataCount, attr.countComponent, attr.frameCount, baseAttr.timeValues, (const float **) nullptr, Scene::COPY_BUFFER);
                }
                else
                {
                    attrTimedSet(&attr, attr.dataFloat, attr.dataInt, finalDataCount, attr.countComponent, (const float *) nullptr, Scene::COPY_BUFFER);
                }

                // update vertex attribute topology (only for the first frame and if not already updated to avoid multiple allocations)
                if (!isTopoProcessed)
                {
                    attrSet(
                        &processed.topologies[attr.topology], processed.topologies[attr.topology].dataInt, processed.topologies[attr.topology].dataFloat, topoIds.size(), 1, &topoIds[0],
                        Scene::COPY_BUFFER);
                }
            }

            // update vertex attribute values
            float * dstDataFloat = (attr.frameCount > 1) ? (float *) attr.dataFloat[frame] : (float *) attr.dataFloat;
            memcpy(dstDataFloat, &dataValues[0], dataValues.size() * sizeof(float));
        }
    }

    // set faces data (only faces.count, now we only have triangles)
    processed.faces.count = processed.topologies[0].count / 3;
    processed.faces.countComponent = 1;
    processed.faces.dataInt = nullptr; // triangles

    return 0;
}

void TriangulateMesh(SceneImpl::Mesh & processed, const SceneImpl::Mesh & base)
{
    assert(false);
}

int32_t computeLODSubdivisionLevel(SceneImpl::Mesh & m, M44f & projMat, float edgeLength, int32_t maxLevel)
{
    const int32_t * const tVertex = m.topologies[m.vertexAttributes[m.positionAttrIndex].topology].dataInt;
    const float * const positions =
        m.vertexAttributes[m.positionAttrIndex].frameCount > 1 ? m.vertexAttributes[m.positionAttrIndex].dataFloat[0] : (const float *) m.vertexAttributes[m.positionAttrIndex].dataFloat;
    int32_t faceCount = m.faces.count;
    int32_t countComponent = m.vertexAttributes[m.positionAttrIndex].countComponent;
    float accumLevel = 0;
    int32_t edgeCount = 0;
    int32_t baseIdx = 0;
    for (int32_t f = 0; f < faceCount; ++f)
    {
        int32_t vertCount = m.faces.dataInt ? m.faces.dataInt[f] : 3;
        for (int32_t v = 0; v < vertCount; ++v)
        {
            int32_t id0 = tVertex[baseIdx + v];
            int32_t id1 = tVertex[baseIdx + (v + 1) % vertCount];
            V3f v0, v1;
            V3f orig0 = fv3ToV3f(positions + (id0 * countComponent));
            V3f orig1 = fv3ToV3f(positions + (id1 * countComponent));
            projMat.multVecMatrix(orig0, v0);
            projMat.multVecMatrix(orig1, v1);

            // don't count edge out of screen
            if ((v0.x < -1 || v0.x > 1) && (v0.y < -1 || v0.y > 1) && (v1.x < -1 || v1.x > 1) && (v1.y < -1 || v1.y > 1))
                continue;

            v0.z = 0.f;
            v1.z = 0.f;
            float l = distance(v0, v1);
            accumLevel += l / edgeLength;
            ++edgeCount;
        }
        baseIdx += vertCount;
    }

    int32_t retLevel = (edgeCount == 0) ? 0 : (int32_t)(accumLevel / edgeCount);
    // handle integer overflow case -> use max level;
    if (retLevel < 0)
        retLevel = maxLevel;

    return std::min(retLevel, maxLevel);
}

void convertSubdivisionOptions(
    int32_t vertexBoundary, int32_t fvarBoundary, bool fvarPropagateCornerValue, osd3::Sdc::Options::VtxBoundaryInterpolation & vbi, osd3::Sdc::Options::FVarLinearInterpolation & fvli)
{
    switch (vertexBoundary)
    {
    case 0: // edges
        vbi = osd3::Sdc::Options::VTX_BOUNDARY_EDGE_ONLY;
        break;
    case 1: // edges and corners
        vbi = osd3::Sdc::Options::VTX_BOUNDARY_EDGE_AND_CORNER;
        break;
    default:
        break;
    }

    switch (fvarBoundary)
    {
    case 0: // edge
    default:
        fvli = osd3::Sdc::Options::FVAR_LINEAR_NONE;
        break;
    case 1: // edge & corner
        if (fvarPropagateCornerValue)
            fvli = osd3::Sdc::Options::FVAR_LINEAR_CORNERS_PLUS2;
        else
            fvli = osd3::Sdc::Options::FVAR_LINEAR_CORNERS_PLUS1;
        break;
    case 2: // nonebind
        fvli = osd3::Sdc::Options::FVAR_LINEAR_ALL;
        break;
    case 3: // always sharp
        fvli = osd3::Sdc::Options::FVAR_LINEAR_BOUNDARIES;
        break;
    }
}

int32_t mergeFVarValues(SceneImpl::Mesh & m, std::vector<int32_t> & fvarSources, int32_t fvarCount)
{
    for (int32_t i = 0; i < fvarCount; ++i)
    {
        SceneImpl::Mesh::VertexAttribute & baseFVarAttr = m.vertexAttributes[fvarSources[i]];
        Attribute & mainTopology = m.topologies[m.vertexAttributes[m.positionAttrIndex].topology];
        Attribute & baseFVarTopology = m.topologies[baseFVarAttr.topology];
        int32_t fvarFrameCount = baseFVarAttr.frameCount;
        for (int32_t frame = 0; frame < fvarFrameCount; ++frame)
        {
            // make fvars unique and compute corresponding topology
            std::map<std::pair<int32_t, OSDVertex>, int32_t> fvarCache;
            std::vector<int32_t> fvarTopology;

            int32_t beginFace = 0;
            const float * srcFloatData = (fvarFrameCount > 1) ? (const float *) baseFVarAttr.dataFloat[frame] : (const float *) baseFVarAttr.dataFloat;
            int32_t fvarComponents = baseFVarAttr.countComponent;
            for (int32_t f = 0; f < m.faces.count; ++f)
            {
                int32_t faceSize = m.faces.dataInt ? m.faces.dataInt[f] : 3;
                for (int32_t j = 0; j < faceSize; ++j)
                {
                    int32_t vertexId = mainTopology.dataInt[beginFace + j];
                    int32_t fvarId = baseFVarTopology.dataInt[beginFace + j];
                    OSDVertex fvarValue;
                    fvarValue.SetData(srcFloatData + fvarComponents * fvarId);
                    std::map<std::pair<int32_t, OSDVertex>, int32_t>::iterator found = fvarCache.find(std::make_pair(vertexId, fvarValue));
                    if (found == fvarCache.end())
                    {
                        int32_t finalVertexId = fvarCache.size();
                        fvarCache[std::make_pair(vertexId, fvarValue)] = finalVertexId;
                        if (frame == 0)
                            fvarTopology.push_back(finalVertexId);
                    }
                    else
                    {
                        if (frame == 0)
                            fvarTopology.push_back(found->second);
                    }
                }
                beginFace += faceSize;
            }

            // update shining data
            if (frame == 0)
            {
                attrSet(&baseFVarTopology, baseFVarTopology.dataInt, baseFVarTopology.dataFloat, fvarTopology.size(), 1, &fvarTopology[0], Scene::COPY_BUFFER);
                if (fvarFrameCount > 1)
                    attrTimedSet(
                        &baseFVarAttr, baseFVarAttr.dataFloat, baseFVarAttr.dataInt, fvarCache.size(), fvarComponents, fvarFrameCount, baseFVarAttr.timeValues, (const float **) nullptr,
                        Scene::COPY_BUFFER);
                else
                    attrTimedSet(&baseFVarAttr, baseFVarAttr.dataFloat, baseFVarAttr.dataInt, fvarCache.size(), fvarComponents, (const float *) nullptr, Scene::COPY_BUFFER);
            }

            float * dstFloatData = (fvarFrameCount > 1) ? (float *) baseFVarAttr.dataFloat[frame] : (float *) baseFVarAttr.dataFloat;
            for (auto fv: fvarCache)
            {
                const OSDVertex & fvarValue = fv.first.second;
                for (int32_t c = 0; c < fvarComponents; ++c)
                    dstFloatData[fv.second * fvarComponents + c] = fvarValue.GetData()[c];
            }
        }
    }
    return 0;
}

} // namespace shn
