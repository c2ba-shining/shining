#pragma once

#include "OslRendererAttrHelpers.h"
#include "Shining.h"
#include <iosfwd>
#include <memory>
#include <string>

namespace shn
{
/// [[[rpc ignore_function ]]]
SHINING_EXPORT void setDefaultMaxThreadCount(int32_t count);
/// [[[rpc ignore_function ]]]
SHINING_EXPORT int32_t defaultMaxThreadCount();

/**
 * @brief Must be called before createMemoryImage or deleteMemoryImage
 *
 * [[[rpc ignore_function ]]]
 */
SHINING_EXPORT void initMemoryImage();

/**
 * @brief Create a in-memory image that can be used in shader parameters.
 * @param name a filename-like string to be used as a shader parameter. The extension must be ".mem".
 *
 * [[[rpc data: count=width*height*channelCount ]]]
 */
SHINING_EXPORT void createMemoryImage(const char * name, int32_t width, int32_t height, int32_t channelCount, const float * data);

/**
 * @brief Create a in-memory image that can be used in shader parameters.
 * @param name a filename-like string to be used as a shader parameter. The extension must be ".mem".
 *
 * [[[rpc data: count=width*height*channelCount ]]]
 */
SHINING_EXPORT void createMemoryImage(const char * name, int32_t width, int32_t height, int32_t channelCount, const unsigned char * data);

SHINING_EXPORT void deleteMemoryImage(const char * name);

class OslRendererImpl;
class Scene;
class SceneImpl;
class RendererStatistics;
class DisplacementData;

/// [[[rpc remote_class ]]]
class SHINING_EXPORT OslRenderer
{
public:
    typedef int32_t Id;
    typedef int32_t Status;

    enum FramebufferFormat
    {
        FRAMEBUFFER_RGBA = 0,
        FRAMEBUFFER_R,
        FRAMEBUFFER_UNKNOWN
    };

public:
    OslRenderer();
    ~OslRenderer();

    Status attribute(const char * name, const char * value);
    Status attribute(const char * name, float value);
    /// [[[rpc value: count=count, order=9 ]]]
    Status attribute(const char * name, const float * value, int32_t count);
    Status attribute(const char * name, int32_t value);

    // get renderer attribute from a given name. If the attribute doesn't exist, returnedValue is set to null
    /// [[[rpc ignore_function ]]]
    Status getAttribute(const char * name, const char ** returnedValue);
    /// [[[rpc ignore_function ]]]
    Status getAttribute(const char * name, float * returnedValue);
    /// [[[rpc ignore_function ]]]
    Status getAttribute(const char * name, int32_t * returnedValue);

    // Get the buffer size required to hold a particular attribute
    /// [[[rpc outByteSize: out, count=1 ]]]
    Status getAttributeBufferByteSize(const char * name, uint32_t * outByteSize);

    // Get an attribute. outBuffer must point to an allocated memory area of byteSize bytes, which should be obtained from a call to getAttributeBufferByteSize
    /// [[[rpc outBuffer: out, count=byteSize ]]]
    Status getAttribute(const char * name, uint32_t byteSize, char * outBuffer);

    /// [[[rpc shadingTreeIds: out, count=count, order=9 ]]]
    Status genShadingTrees(Id * shadingTreeIds, uint32_t count);

    Status addShadingTree(Id shadingTreeId, const char * shadingTreeName, const char * jsonData, uint32_t frame);

    Id getShadingTreeIdFromName(const char * name) const;

    /// [[[rpc ignore_function ]]]
    Status clearShadingTrees();

    /// [[[rpc instanceIds: count=count, order=9 ]]]
    Status assignShadingTree(Id shadingTreeId, const Id * instanceIds, const int32_t shadingGroup, int32_t count, const char * assignType);

    /**
     * @brief Set displacement data of a mesh and add them to the displacement computation structure. Apply displacement by calling commitDisplacement()
     * @param topoIds: array of index in topology arrays, corresponding to unique vertices. Size equals to uniqueVerticeCount
     *
     * [[[rpc ignore_function ]]]
     */
    Status setDisplacement(DisplacementData data);
    /**
     * @brief Apply displacement to meshes registred by setDisplacement(). Calling commitDisplacement twice doesn't apply displacement twice (just once)
     *
     * [[[rpc ignore_function ]]]
     */
    Status commitDisplacement(SceneImpl * scene);

    /// [[[rpc lightIds: out, count=count, order=9 ]]]
    Status genLights(Id * lightIds, int32_t count);
    int32_t countLightIdsByName(const char * name) const;
    /// [[[rpc lightIds: out, count=count, order=9 ]]]
    Status lightIdsByName(const char * name, Id * lightIds, int32_t count) const;

    /// [[[rpc data: count=dataByteSize ]]]
    Status lightAttribute(Id lightId, const char * attrName, const char * attrType, int32_t dataByteSize, const char * data);

    Status commitLight(Id lightId);
    Status commitLights();

    /// [[[rpc ignore_function ]]]
    Status clearLights();

    /// [[[rpc ignore_function ]]]
    bool isEmissiveInstance(const char * instanceName) const;

    /// [[[rpc framebufferIds: out, count=count, order=9 ]]]
    // #todo remove from API
    Status genFramebuffers(Id * framebufferIds, int32_t count);
    /**
     * @brief Resize the framebuffer, and optionally initialize it.
     * @return -1 if invalid parameter or if the buffer is being rendered.
     *
     * [[[rpc data: count=width*height*channelCount, nullable ]]]
     */
    // #todo remove from API
    Status framebufferData(Id framebufferId, int32_t width, int32_t height, int32_t channelCount, FramebufferFormat format, const float * data);
    /// [[[rpc clearValue: count=channelCount ]]]
    Status clearFramebuffer(Id framebufferId, int32_t channelCount, const float * clearValue);
    /// [[[rpc clearValue: count=channelCount ]]]
    Status clearSubFramebuffer(Id framebufferId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t channelCount, const float * clearValue);
    /// [[[rpc ignore_function ]]]
    const float * getFramebufferData(Id framebufferId) const;

    int32_t getFramebufferChannelCount(Id framebufferId) const;
    /// [[[rpc data: out, count=width*height*channelCount ]]]
    Status getFramebufferData(Id framebufferId, Id bindingId, int32_t width, int32_t height, int32_t channelCount, FramebufferFormat format, float * data) const;
    /// [[[rpc data: out, count=(endx-startx)*(endy-starty)*channelCount, chunk=(endx-startx)*channelCount, stride=dataStride ]]]
    Status getFramebufferData(
        Id framebufferId, Id bindingId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t channelCount, FramebufferFormat format, float * data, int32_t dataStride) const;

    /**
     * @brief Objects to bind a framebuffer to a render location.
     *
     * [[[rpc bindingsIds: out, count=count, order=9 ]]]
     */
    Status genBindings(Id * bindingsIds, int32_t count);

    /// [[[rpc ignore_function ]]]
    int32_t getBindingLocationChannelCount(const char * location) const;

    /// Return 1 if the framebuffer is bound, 0 if not, -1 if invalid parameter.
    // #todo remove from API
    Status isBound(Id bindingId, Id framebufferId) const;

    /**
     * @brief Set a square fill buffer pattern for the binding.
     * @param squareSize the size of the square pattern.
     * @return -1 if the binding is being rendered.
     *
     * The pixel of the bound buffers are updated by the renderer
     * if the mask value for it is non-zero.
     *
     * [[[rpc mask: count=squareSize*squareSize, order=9 ]]]
     */
    Status setBindingMask(Id bindingId, const float * mask, int32_t squareSize);

    /**
     * @brief Blocks while the binding is used by rendering, no longer than timeout.
     * @param timeout_ms: -1 to wait forever, 0 to not block, or a number of milliseconds.
     * @return 1 if rendered, 0 if timeout, or -1 if invalid parameter
     */
    Status waitBinding(Id bindingId, int timeout_ms) const;

    /**
     * @brief Get the count of tiles where the rendering is done.
     * @param bindingId a binding that is being rendered
     * @param [out] totalCount the total count of tiles for the current rendering (depends on the render rect)
     *
     * [[[rpc totalCount: out ]]]
     */
    int32_t bindingRenderedTileCount(Id bindingId, int32_t * totalCount) const;

    /**
     * @brief Get the N first tiles where the rendering is done.
     * @param bindingId a binding that is being rendered
     * @param [out] tiles the ids of the rendered tiles
     * @param count the size of "tiles"
     * @param [out] tileSize the size of a (square) tile. It should be used to convert a tile id to actual pixel rectangles.
     *
     * [[[rpc tiles: out, count=count, order=9;
     *        tileSize: out ]]]
     */
    Status bindingRenderedTiles(Id bindingId, int32_t * tiles, int32_t count, int32_t * tileSize) const;

    Status bindingEnablePass(Id bindingId, const char * location);

    Status bindingInit(Id bindingId, int32_t width, int32_t height);

    /// [[[rpc outFramebufferId: out ]]]
    Status getBindingLocationFramebuffer(Id bindingId, const char * location, Id * outFramebufferId) const;

    /// [[[rpc ignore_function ]]]
    Status bindingPostProcess(Id bindingId);

    /**
     * @brief Access the kind of passes type.
     * @return the number of different passes.
     *
     * [[[rpc ignore_function ]]]
     */
    static int32_t getAvailablePassesCount();

    /**
     * @brief Access to a specific pass name
     * @param index: index of the pass type inside tthe pass info array
     * @return name of the specific pass
     *
     * [[[rpc ignore_function ]]]
     */
    static Status getAvailablePassNameById(int32_t index, const char ** name);

    /**
     * @brief Get statistic properties
     * @param propertyName:
     * "render_time_sec": Time in second for the last call of render()
     * "rays_per_second": Number of rays per second traced during last call of render()
     * "samples_per_second": Number of samples computed per second during last call of render()
     * "mean_bounce_per_sample": Mean number of bounce before absorbtion or background hit accross all samples
     *
     * [[[rpc value: out ]]]
     */
    Status statistics(const char * propertyName, float * value) const;

    /**
     * @brief Get statistic properties
     * @param propertyName:
     * "ray_count": Number of any type of rays traced during last call of render()
     * "shadow_ray_count": Number of shado rays traced during last call of render()
     * "backround_hit_count": Number of successfull background evaluations during last call of render()
     *
     * [[[rpc value: out ]]]
     */
    Status statistics(const char * propertyName, int * value) const;

    /**
     * @brief Write a JSON formatted report to the stream.
     *
     * [[[rpc ignore_function ]]]
     */
    Status statisticsReport(std::ostream & out) const;

    /**
     * @brief Start the render computation.
     *
     * The function is asynchronous. Use waitBinding to synchronize
     * and cancelRender to stop it.
     *
     * [[[rpc scene: remote_object ]]]
     */
    Status render(Scene * scene, Id cameraId, Id bindingId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t startSample, int32_t endSample);

    /**
     * @brief Stop computation as soon as possible.
     *
     * The function is asynchronous. Use waitBinding to synchronize.
     *
     * The framebuffers of the binding are left partially rendered.
     */
    Status cancelRender(Id bindingId);

    /**
     * @brief Set functions called during rendering.
     *
     * sampleProgress if called in the main thread when the ith sample is done.
     * tileProgress if called *by a compute thread* when the ith tile is done.
     *
     * Warning: it is not safe to call this function while rendering.
     *
     * [[[rpc ignore_function ]]]
     */
    Status progressFunctions(void (*sampleProgress)(int32_t, void *), void (*tileProgress)(int32_t, void *), void * appData);

    static Status setDefaultShaderPath(const char * shaderPath);
    static Status setShaderStdIncludePath(const char * includePath);

private:
    OslRenderer(const OslRenderer &);
    OslRenderer & operator=(const OslRenderer &);

private:
    std::unique_ptr<OslRendererImpl> m_impl;
};
} // namespace shn