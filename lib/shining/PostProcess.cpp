#include "PostProcess.h"
#include "Framebuffer.h"
#include "IMathUtils.h"
#include <cmath>
#include <cstdint>
#include <memory>

namespace shn
{

namespace
{
bool isNaN(const float * color, int32_t channelCount)
{
    for (int32_t i = 0; i < channelCount; ++i)
        if (shn::isNaN(color[i]))
            return true;
    return false;
}

void normalizedBuffer(float * outbuffer, const float * buffer, const float * count, int width, int height, int32_t channelCount)
{
    for (int j = 0; j < height; ++j)
    {
        for (int i = 0; i < width; ++i)
        {
            int pixelIndex = j * width + i;
            if (count[pixelIndex] > 0.f)
            {
                for (int32_t c = 0; c < channelCount; ++c)
                    outbuffer[pixelIndex * channelCount + c] = buffer[pixelIndex * channelCount + c] / count[pixelIndex];
            }
            else
            {
                for (int32_t c = 0; c < channelCount; ++c)
                    outbuffer[pixelIndex * channelCount + c] = 0.f;
            }
        }
    }
}

void normalizedAndNaNFilteredBuffer(float * outbuffer, const float * buffer, const float * count, int width, int height, int32_t channelCount)
{
    for (int j = 0; j < height; ++j)
    {
        for (int i = 0; i < width; ++i)
        {
            int pixelIndex = j * width + i;

            if (isNaN(&buffer[pixelIndex * channelCount], channelCount))
            {
                float filteredValue[4] = { 0.f, 0.f, 0.f, 0.f };
                float kernelPixelCount = 0.f;
                for (int k = 0; k < 3; ++k)
                {
                    for (int l = 0; l < 3; ++l)
                    {
                        int ngbX = i + k - 1;
                        int ngbY = j + l - 1;
                        if (ngbX < 0 || ngbX >= width || ngbY < 0 || ngbY >= height)
                            continue;

                        int ngbBase = ngbY * width + ngbX;

                        if (!isNaN(&buffer[ngbBase * channelCount], channelCount) && count[ngbBase] > 0.f)
                        {
                            for (int32_t c = 0; c < channelCount; ++c)
                                filteredValue[c] += buffer[ngbBase * channelCount + c] / count[ngbBase];
                            kernelPixelCount += 1.f;
                        }
                    }
                }
                // avoid NaN and set alpha in case this pixel is surrounding by NaN pixels
                if (channelCount == 4 && kernelPixelCount < 1.f)
                {
                    filteredValue[3] = 1.f;
                    kernelPixelCount = 1.f;
                }

                for (int32_t i = 0; i < channelCount; ++i)
                    outbuffer[pixelIndex * channelCount + i] = filteredValue[i] / kernelPixelCount;
            }
            else
            {
                if (count[pixelIndex] > 0.f)
                {
                    for (int32_t c = 0; c < channelCount; ++c)
                        outbuffer[pixelIndex * channelCount + c] = buffer[pixelIndex * channelCount + c] / count[pixelIndex];
                }
                else
                {
                    for (int32_t c = 0; c < channelCount; ++c)
                        outbuffer[pixelIndex * channelCount + c] = 0.f;
                }
            }
        }
    }
}

} // namespace

void normalize(Framebuffer & framebuffer, const float * sampleCountBuffer, bool filterNaNs)
{
    std::unique_ptr<float[]> buffer(new float[framebuffer.width() * framebuffer.height() * framebuffer.channelCount()]);

    if (filterNaNs)
        normalizedAndNaNFilteredBuffer(buffer.get(), framebuffer.data(), sampleCountBuffer, framebuffer.width(), framebuffer.height(), framebuffer.channelCount());
    else
        normalizedBuffer(buffer.get(), framebuffer.data(), sampleCountBuffer, framebuffer.width(), framebuffer.height(), framebuffer.channelCount());

    std::copy(buffer.get(), buffer.get() + framebuffer.width() * framebuffer.height() * framebuffer.channelCount(), begin(framebuffer));
}

} // namespace shn