#pragma once

#include "BitmaskOperators.h"
#include "Framebuffer.h"
#include "HighResolutionTimerGuard.h"
#include "IMathUtils.h"
#include "LoggingUtils.h"
#include "OslHeaders.h"
#include "StringUtils.h"
#include <bcd/core/SamplesAccumulator.h>
#include "lpe/accum.h"
#include <unordered_map>
#include <vector>

namespace shn
{

class OslRendererImpl;

namespace
{
template<typename T>
inline bool testAndLogInvalidOutput(const T & value, size_t pixelX, size_t pixelY, size_t sampleID, const char * where)
{
    if (shn::isNaN(value))
    {
        SHN_LOGERR << "[!] NaN DETECTED at (" << pixelX << "x" << pixelY << ") sample " << sampleID << " in " << where << ".";
        return false;
    }
    if (shn::isInf(value))
    {
        SHN_LOGERR << "[!] Inf DETECTED at (" << pixelX << "x" << pixelY << ") sample " << sampleID << " in " << where << ".";
        return false;
    }
    // if (shn::isNeg(value)) {
    //    SHN_LOGERR << "[!] Neg DETECTED at (" << pixelX << "x" << pixelY << ") sample " << sampleID << " in " << where << ".";
    //    return false;
    //}
    return true;
}
} // namespace

enum class PassFlags
{
    None = 0,
    Render = 1 << 0,
    Technical = 1 << 1,
    Stats = 1 << 2,
    Debug = 1 << 3,
    Direct = 1 << 4,
    Indirect = 1 << 5,
    Specular = 1 << 6,
    Color = 1 << 7,
    Geometry = 1 << 8,
    Visibility = 1 << 9,
    Composite = 1 << 10,
    NoNormalization = 1 << 11,
    NoDenoise = 1 << 12,
    ApplyOutline = 1 << 13,
    DirectIndirect = Direct | Indirect
};

struct OutputInfo
{
    ustring name;
    std::string pathExpression;
    uint8_t channelCount;
    PassFlags flags;

    OutputInfo()
    {
    }

    OutputInfo(ustring name, std::string pathExpression, uint8_t channelCount, PassFlags flags): name{ name }, pathExpression{ pathExpression }, channelCount{ channelCount }, flags{ flags }
    {
    }
};

struct OutputPass
{
    const OutputInfo info;

    Framebuffer * framebuffer = nullptr;
    bool denoisingEnabled = false;
    std::unique_ptr<bcd::SamplesAccumulator> sampleAccumulator;
    Framebuffer * denoisedFramebuffer = nullptr;
    int32_t pathExpressionAovIdx = -1;

    OutputPass(const OutputInfo & info): info(info)
    {
    }

    void addColorSample(size_t x, size_t y, const Color3 & color, float alpha);
};

const OutputInfo * getOutputPreset(ustring name);
size_t getOutputPresetCount();
const OutputInfo & getOutputPreset(size_t index);
int32_t getOutputPresetIndex(ustring name);

namespace PassNames
{

extern ustring ImportanceCachingMeanSampleCountDirect;
extern ustring ImportanceCachingMeanSampleCountIndirect;
extern ustring ImportanceCachingProbabilitiesDirect;
extern ustring ImportanceCachingProbabilitiesIndirect;
extern ustring ImportanceCachingTileFContribDirect;
extern ustring ImportanceCachingTileUContribDirect;
extern ustring ImportanceCachingTileTContribDirect;
extern ustring ImportanceCachingTileCContribDirect;
extern ustring ImportanceCachingTileFContribIndirect;
extern ustring ImportanceCachingTileUContribIndirect;
extern ustring ImportanceCachingTileTContribIndirect;
extern ustring ImportanceCachingTileCContribIndirect;

} // namespace PassNames

// A binding holds a set of framebuffers bound to specific output passes
struct Binding

{
public:
    Binding(OslRendererImpl & renderer): m_renderer(renderer)
    {
    }

    void init(size_t width, size_t height);

    size_t width() const
    {
        return m_width;
    }

    size_t height() const
    {
        return m_height;
    }

    const LPEAutomata & getAutomata() const
    {
        return m_automata;
    }

    void addOutput(const OutputInfo & info);

    int32_t getOutputIdx(string_view name) const;

    size_t getOutputIdxFromPathExprAovIdx(size_t pathExprAovIdx) const;

    OutputPass & getOutput(size_t outputIdx);

    const OutputPass & getOutput(size_t outputIdx) const;

    size_t getOutputCount() const;

    const std::vector<OutputPass> & outputs() const
    {
        return m_outputs;
    }

    std::vector<OutputPass> & outputs()
    {
        return m_outputs;
    }

    void enableDenoising(size_t outputIdx);

    bool needMotionVectors() const;

    bool needCoverage() const;

    bool needOcclusion() const;

    bool needLighting() const;

    bool needDepth() const;

    bool needBouncing() const;

    bool needMeanVariance() const;

    void addSamples(size_t x, size_t y, float sampleCount);

    const float * alphaBuffer() const;

    const float * sampleCountBuffer() const;

    const Framebuffer * sampleCountFramebuffer() const
    {
        return m_sampleCount;
    }

    float sampleCount(size_t x, size_t y) const;

    // #todo for now alpha is hard coded, it could be more powerful to be able to specify alpha channel with a custum LPE like explained
    // in the OSL page https://github.com/imageworks/OpenShadingLanguage/wiki/OSL-Light-Path-Expressions
    void addAlpha(size_t x, size_t y, float alpha);

    float alpha(size_t x, size_t y) const;

    void addBounces(size_t x, size_t y, float bounceCount);

    void setCoverage(size_t x, size_t y, float hitCount);

    void setMeanVariance(size_t x, size_t y, float mean, float variance);

    void add(int32_t outputIdx, size_t x, size_t y, const float * color);

    void set(int32_t outputIdx, size_t x, size_t y, const float * color);

private:
    enum FeatureEnum
    {
        Feature_MotionVectors = 1 << 0,
        Feature_Occlusion = 1 << 1,
        Feature_Lighting = 1 << 2,
        Feature_Depth = 1 << 3,
        Feature_Bouncing = 1 << 4
    };

    OslRendererImpl & m_renderer;

    size_t m_width = 0;
    size_t m_height = 0;

    LPEAutomata m_automata;
    std::vector<OutputPass> m_outputs;
    std::unordered_map<ustring, size_t> m_outputNameToIndex;
    std::vector<size_t> m_pathExprAovIdxToOutputIdx;

    Framebuffer * m_sampleCount = nullptr;
    Framebuffer * m_bounceCount = nullptr;
    Framebuffer * m_alpha = nullptr;
    Framebuffer * m_transparency = nullptr;
    Framebuffer * m_coverage = nullptr;
    Framebuffer * m_meanVariance = nullptr;

    uint32_t m_features = Feature_Bouncing;

    // Not copyable, AOVs are pointing to this
    Binding(const Binding &) = delete;
    Binding & operator=(Binding &) = delete;
};

template<>
struct EnableBitmaskOperators<PassFlags>: std::true_type
{
};

} // namespace shn
