#include "Primitive.h"
#include "IMathUtils.h"
#include "Shining_p.h"
#include "embree2/rtcore_ray.h"
#include <algorithm>
#include <cassert>

namespace i = Imath;

namespace shn
{

void timeToLocalTime(const MotionPrimitive & m, const float time, int * frame, float * localTime);

MotionPrimitive::MotionPrimitive(): frameCount(-1)
{
}

MotionPrimitive::MotionPrimitive(const Imath::Box3f & bounds, int32_t frameCount, const RTCScene * objects, const float * times, PrimitiveType type):
    bounds(bounds), frameCount(frameCount), type(type)
{
    assert(frameCount > 0);
    this->objects.resize(frameCount);
    this->times.resize(frameCount);
    for (int32_t i = 0; i < frameCount; ++i)
    {
        this->objects[i] = objects[i];
        this->times[i] = times[i];
    }
}

MotionPrimitive::MotionPrimitive(const Imath::Box3f & bounds, RTCScene object, PrimitiveType type): bounds(bounds), frameCount(1), type(type)
{
    assert(frameCount > 0);
    this->objects.resize(frameCount);
    this->times.clear();
    this->objects[0] = object;
}

void MotionPrimitive::clear()
{
    for (const RTCScene & rtcScene: objects)
    {
        rtcDeleteScene(rtcScene);
    }
    objects.clear();
    times.clear();
    frameCount = -1;
}

size_t MotionPrimitive::memoryFootprint() const
{
    size_t byteSize = sizeof(*this);
    byteSize += objects.size() * sizeof(RTCScene);
    byteSize += times.size() * sizeof(float);
    return byteSize;
}

Box3f primitiveBounds(const MotionPrimitive * primitive)
{
    return primitive->bounds;
}

RTCScene primitiveScene(const MotionPrimitive * primitive, float time, float * localTime)
{
    int32_t frame;
    // 1 Frame no motion blur
    if (!primitive->times.size())
        return primitiveScene(primitive, 0);

    // 1 Frame motion blur
    if (primitive->frameCount == 1)
    {
        *localTime = time - primitive->times[0];
        return primitiveScene(primitive, 0);
    }

    // Multi-frame motion blur
    timeToLocalTime(*primitive, time, &frame, localTime);
    return primitiveScene(primitive, frame);
}

RTCScene primitiveScene(const MotionPrimitive * primitive, int32_t frame)
{
    return primitive->objects[frame];
}

void timeToLocalTime(const MotionPrimitive & m, const float time, int * frame, float * localTime)
{
    auto lowtPtr = std::upper_bound(m.times.begin(), m.times.begin() + m.frameCount, time);
    int lowFrame = lowtPtr - m.times.begin() - 1;
    float lowTime = m.times[lowFrame];
    float highTime = lowTime + 1;
    if (time <= lowTime)
    {
        *localTime = 0.f;
        *frame = lowFrame;
        return;
    }
    if (time >= highTime)
    {
        *localTime = 1.f;
        *frame = lowFrame;
        return;
    }
    *localTime = (time - lowTime) / (highTime - lowTime);
    *frame = lowFrame;
}
} // namespace shn