#include "ShadingGrid.h"
#include "OslHeaders.h"
#include "Shining_p.h"

namespace o = OSL;
namespace oiio = OIIO_NAMESPACE;

namespace shn
{
ShadingGrid::~ShadingGrid()
{
    if (m_values)
    {
        delete[] m_values;
        m_values = nullptr;
    }
    if (m_rows)
    {
        delete[] m_rows;
        m_rows = nullptr;
    }
    if (m_cols)
    {
        delete[] m_cols;
        m_cols = nullptr;
    }
}

o::Color3 ShadingGrid::eval(const o::Vec3 & dir, float & pdf)
{
    // map from sphere to unit-square
    float u = std::atan2(dir.y, dir.x) * float(M_1_PI * 0.5f);
    if (u < 0)
        u++;
    float v = (1.f - dir.z) * 0.5f;

    // retrieve nearest neighbor
    int x = (int) (u * m_res);
    if (x < 0)
        x = 0;
    else if (x >= m_res)
        x = m_res - 1;
    int y = (int) (v * m_res);
    if (y < 0)
        y = 0;
    else if (y >= m_res)
        y = m_res - 1;
    int i = y * m_res + x;
    float row_pdf = m_rows[y] - (y > 0 ? m_rows[y - 1] : 0.0f);
    float col_pdf = m_cols[i] - (x > 0 ? m_cols[i - 1] : 0.0f);
    pdf = row_pdf * col_pdf * m_invjacobian;
    return m_values[i];
}

o::Color3 ShadingGrid::sampleAndEval(float rx, float ry, o::Dual2<o::Vec3> & dir, float & pdf)
{
    float row_pdf, col_pdf;
    unsigned x, y;
    ry = sample_cdf(m_rows, m_res, ry, &y, &row_pdf);
    rx = sample_cdf(m_cols + y * m_res, m_res, rx, &x, &col_pdf);
    dir = map(x + rx, y + ry);
    pdf = row_pdf * col_pdf * m_invjacobian;
    return m_values[y * m_res + x];
}

o::Dual2<o::Vec3> ShadingGrid::map(const float x, const float y)
{
    o::Dual2<float> u = o::Dual2<float>(x, 1, 0) * m_invres;
    o::Dual2<float> v = o::Dual2<float>(y, 0, 1) * m_invres;
    o::Dual2<float> theta = u * float(2 * M_PI);
    o::Dual2<float> st, ct;
    ShadingGrid::sincos(theta, &st, &ct);
    o::Dual2<float> cos_phi = 1.0f - 2.0f * v;
    o::Dual2<float> sin_phi = sqrt(1.0f - cos_phi * cos_phi);
    return o::make_Vec3(sin_phi * ct, sin_phi * st, cos_phi);
}

float ShadingGrid::sample_cdf(const float * data, unsigned int n, float x, unsigned int * idx, float * pdf)
{
    DASSERT(x >= 0);
    DASSERT(x < 1);
    *idx = static_cast<unsigned int>(std::upper_bound(data, data + n, x) - data);
    DASSERT(*idx < n);
    DASSERT(x < data[*idx]);
    float scaled_sample;
    if (*idx == 0)
    {
        *pdf = data[0];
        scaled_sample = x / data[0];
    }
    else
    {
        DASSERT(x >= data[*idx - 1]);
        *pdf = data[*idx] - data[*idx - 1];
        scaled_sample = (x - data[*idx - 1]) / (data[*idx] - data[*idx - 1]);
    }
    // keep result in [0,1)
    return std::min(scaled_sample, 0.99999994f);
}

void ShadingGrid::sincos(const o::Dual2<float> & a, o::Dual2<float> * sina, o::Dual2<float> * cosa)
{
    *sina = o::sin(a);
    *cosa = o::cos(a);
}

} // namespace shn
