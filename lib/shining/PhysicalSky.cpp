#include "PhysicalSky.h"
#include <algorithm>
#include <cassert>

namespace shn
{

PhysicalSky::PhysicalSky(): m_sunElevation(deg2rad(45.f)), m_groundAlbedo(0.f), m_turbidity(2.f), m_skyIntensity(0.f), m_mode(Mode::Mode_RGB)
{
    init();
}

PhysicalSky::~PhysicalSky()
{
    arhosekskymodelstate_free(m_skymodel);
}

void PhysicalSky::init()
{
    auto lock = std::unique_lock<std::mutex>(m_skyInitMutex);

    // remap sun elevation for the space [90;180] degrees
    float sunElevation = m_sunElevation;
    if (sunElevation > shn::FPI_2)
        sunElevation = shn::FPI_2 - (sunElevation - shn::FPI_2);

    if (m_mode == Mode::Mode_RGB)
        m_skymodel = arhosek_rgb_skymodelstate_alloc_init(m_turbidity, m_groundAlbedo, sunElevation);
    else if (m_mode == Mode_SPECTRAL)
        m_skymodel = arhosekskymodelstate_alloc_init(m_turbidity, m_groundAlbedo, sunElevation);
}

void PhysicalSky::setSkyParams(float sunElevation, float groundAlbedo, float turbidity, float skyIntensity, Mode mode)
{
    if (m_sunElevation != sunElevation || m_groundAlbedo != groundAlbedo || m_turbidity != turbidity || m_mode != mode)
    {
        m_sunElevation = sunElevation;
        m_groundAlbedo = groundAlbedo;
        m_turbidity = turbidity;
        m_mode = mode;
        init();
    }

    m_skyIntensity = skyIntensity;
}

void PhysicalSky::eval(const V3f & dir, float * skydomeResult, int32_t spectrumSize, int32_t wavelenghtStart, int32_t wavelengthEnd) const
{
    float sunTheta = m_sunElevation;
    V3f sunDir =
        normalize(V3f(cos(sunTheta), sin(sunTheta), 0.f)); // the sun is always on the XY plane. You can control the sun azimuth through the envmap in which is plugged the physical sky
    V3f nDir = normalize(dir);
    float gamma = acos(dot(nDir, sunDir));
    float theta = acos(nDir.y);

    if (theta > shn::FPI_2)
        return;

    if (m_mode == Mode::Mode_RGB) // rgb
    {
        assert(spectrumSize == 3);
        for (int32_t i = 0; i < spectrumSize; ++i)
            skydomeResult[i] = arhosek_tristim_skymodel_radiance(m_skymodel, theta, gamma, i) * m_skyIntensity;
    }
    else // spectral
    {
        double wavelenghtStep = (double) (wavelengthEnd - wavelenghtStart) / (double) spectrumSize;
        for (int32_t i = 0; i < spectrumSize; ++i)
        {
            double wavelengthCenter = ((double) wavelenghtStart + (double) i * wavelenghtStep) + 0.5 * wavelenghtStep;
            skydomeResult[i] = (float) arhosekskymodel_radiance(m_skymodel, theta, gamma, wavelengthCenter);
        }
    }
}

} // namespace shn