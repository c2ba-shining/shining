#include "OslRendererStats.h"

namespace shn
{

const StatInfo STATISTIC_INFO[] = {
    { "duration", StatType::Seconds },
    { "rays_central_camera", StatType::Count },
    { "rays_central_scattering", StatType::Count },
    { "rays_camera", StatType::Count },
    { "rays_scattering_fromBSDFSampling_diffuse", StatType::Count },
    { "rays_scattering_fromBSDFSampling_glossy", StatType::Count },
    { "rays_scattering_fromBSDFSampling_specular", StatType::Count },
    { "rays_scattering_fromBSDFSampling_straight", StatType::Count },
    { "rays_scattering_fromBSSRDFSampling", StatType::Count },
    { "rays_shadow_fromLightSampling", StatType::Count },
    { "rays_shadow_fromMaterialSampling", StatType::Count },
    { "autoIntersections_scattering", StatType::Count },
    { "autoIntersections_shadow", StatType::Count },
    { "backgroundHits", StatType::Count },
    { "raysPerSecond", StatType::Hz },
    { "samplesPerSecond", StatType::Hz },
    { "maxBounces", StatType::Count },
    { "bouncesPerSample", StatType::Average },
    { "samples_computed", StatType::Count },
    { "samples_dropped", StatType::Count },

    { "lightSamplingCount_direct_F", StatType::Count },
    { "lightSamplingCount_direct_U", StatType::Count },
    { "lightSamplingCount_direct_T", StatType::Count },
    { "lightSamplingCount_direct_C", StatType::Count },
    { "lightSamplingCount_direct_allLights", StatType::Count },
    { "lightSamplingCount_direct_otherLights", StatType::Count },
    { "lightSamplingCount_direct_otherLightsBSSRDF", StatType::Count },
    { "lightSamplingCount_indirect_F", StatType::Count },
    { "lightSamplingCount_indirect_U", StatType::Count },
    { "lightSamplingCount_indirect_T", StatType::Count },
    { "lightSamplingCount_indirect_C", StatType::Count },
    { "lightSamplingCount_indirect_allLights", StatType::Count },
    { "lightSamplingCount_indirect_otherLights", StatType::Count },
    { "lightSamplingCount_indirect_otherLightsBSSRDF", StatType::Count },

    { "renderFrame" },
    { "renderFrame_jitteredDistrib" },
    { "renderFrame_initialize" },

    { "renderFrame_cameraVolumeInclusion" },

    { "renderFrame_removeAdaptive" },

    { "renderFrame_technical" },
    { "renderFrame_technical_intersect" },
    { "renderFrame_technical_material" },
    { "renderFrame_technical_writeImages" },

    { "renderFrame_intersect" },
    { "renderFrame_intersect_inters" },
    { "renderFrame_intersect_missed" },

    { "renderFrame_material_eval" },
    { "renderFrame_material_sample_bssrdfPoint" },

    { "renderFrame_coverage_init" },
    { "renderFrame_coverage_intersect1" },
    { "renderFrame_coverage_intersect2" },
    { "renderFrame_coverage_add" },

    { "renderFrame_occlusion_sample" },
    { "renderFrame_occlusion_shadow" },
    { "renderFrame_occlusion_add" },

    { "renderFrame_background_sample" },
    { "renderFrame_background_eval" },
    { "renderFrame_background_shadow" },

    { "renderFrame_IC_init" },
    { "renderFrame_IC_initTile" },
    { "renderFrame_IC_buildDistributions" },
    { "renderFrame_IC_accumContribs" },

    { "renderFrame_lighting" },
    { "renderFrame_lighting_sampleMaterial" },
    { "renderFrame_lighting_IC_sampleDistributions" },
    { "renderFrame_lighting_IC_buildProcessingList" },
    { "renderFrame_lighting_IC_accumContribs" },
    { "renderFrame_lighting_evalDirectLighting" },
    { "renderFrame_lighting_evalDirectLighting_light" },
    { "renderFrame_lighting_evalDirectLighting_light_sample" },
    { "renderFrame_lighting_evalDirectLighting_light_eval" },
    { "renderFrame_lighting_evalDirectLighting_light_shadow" },
    { "renderFrame_lighting_evalDirectLighting_light_shadow_inters" },
    { "renderFrame_lighting_evalDirectLighting_light_shadow_volumetric" },
    { "renderFrame_lighting_evalDirectLighting_light_shadow_missed" },
    { "renderFrame_lighting_evalDirectLighting_light_shadow_material" },
    { "renderFrame_lighting_evalDirectLighting_light_shadow_transparent" },
    { "renderFrame_lighting_evalDirectLighting_material" },
    { "renderFrame_lighting_evalDirectLighting_material_sample" },
    { "renderFrame_lighting_evalDirectLighting_material_eval" },
    { "renderFrame_lighting_evalDirectLighting_material_shadow" },
    { "renderFrame_lighting_evalDirectLighting_material_shadow_inters" },
    { "renderFrame_lighting_evalDirectLighting_material_shadow_volumetric" },
    { "renderFrame_lighting_evalDirectLighting_material_shadow_missed" },
    { "renderFrame_lighting_evalDirectLighting_material_shadow_material" },
    { "renderFrame_lighting_evalDirectLighting_material_shadow_transparent" },

    { "renderFrame_volumetric" },
    { "renderFrame_volumetric_gatherVolumes" },
    { "renderFrame_volumetric_resetAccumBuffers" },
    { "renderFrame_volumetric_initSamples" },

    { "renderFrame_volumetric_density_sampling" },
    { "renderFrame_volumetric_density_lighting" },
    { "renderFrame_volumetric_density_lighting_sample" },
    { "renderFrame_volumetric_density_lighting_eval" },
    { "renderFrame_volumetric_density_lighting_shadow" },
    { "renderFrame_volumetric_density_lighting_shadow_inters" },
    { "renderFrame_volumetric_density_lighting_shadow_volumetric" },
    { "renderFrame_volumetric_density_lighting_shadow_missed" },
    { "renderFrame_volumetric_density_lighting_shadow_material" },
    { "renderFrame_volumetric_density_lighting_shadow_transparent" },
    { "renderFrame_volumetric_density_lighting_MIS" },

    { "renderFrame_volumetric_eas_sampling" },
    { "renderFrame_volumetric_eas_lighting" },
    { "renderFrame_volumetric_eas_lighting_sample" },
    { "renderFrame_volumetric_eas_lighting_eval" },
    { "renderFrame_volumetric_eas_lighting_shadow" },
    { "renderFrame_volumetric_eas_lighting_shadow_inters" },
    { "renderFrame_volumetric_eas_lighting_shadow_volumetric" },
    { "renderFrame_volumetric_eas_lighting_shadow_missed" },
    { "renderFrame_volumetric_eas_lighting_shadow_material" },
    { "renderFrame_volumetric_eas_lighting_shadow_transparent" },
    { "renderFrame_volumetric_eas_lighting_MIS" },

    { "renderFrame_volumetric_apply" },

    { "renderFrame_toonOutline" },

    { "renderFrame_bufferAdd" },
    { "renderFrame_bufferAdd_importanceCaching" },
};

static_assert(sizeof(STATISTIC_INFO) / sizeof(*STATISTIC_INFO) == STAT_MAX, "statistic names mismatch");

bool statisticIsFloat(int id)
{
    return STATISTIC_INFO[id].type != StatType::Count;
}

} // namespace shn