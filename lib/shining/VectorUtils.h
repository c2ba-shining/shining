#pragma once

#include <vector>

namespace shn
{
template<typename T>
class vector_view
{
public:
    using vec_type = std::vector<T>;

    using iterator = typename vec_type::iterator;
    using const_iterator = typename vec_type::const_iterator;

    template<typename U>
    friend vector_view<U> make_view(std::vector<U> & v);

    template<typename U>
    friend vector_view<U> make_view(std::vector<U> & v, size_t s);

    template<typename U>
    friend vector_view<U> make_view(std::vector<U> & v, size_t s, const U & value);

    vector_view() = default;

    vector_view(vector_view && v)
    {
        *this = std::move(v);
    }

    vector_view & operator=(vector_view && v)
    {
        std::swap(v.m_vector, m_vector);
        return *this;
    }

    ~vector_view()
    {
        if (m_vector)
        {
            m_vector->clear();
            m_vector = nullptr;
        }
    }

    T & operator[](size_t i)
    {
        return (*m_vector)[i];
    }

    const T & operator[](size_t i) const
    {
        return (*m_vector)[i];
    }

    size_t size() const
    {
        return m_vector->size();
    }

    bool empty() const
    {
        return m_vector->empty();
    }

    iterator begin()
    {
        return std::begin(*m_vector);
    }

    iterator end()
    {
        return std::end(*m_vector);
    }

    const_iterator begin() const
    {
        return std::begin(*m_vector);
    }

    const_iterator end() const
    {
        return std::end(*m_vector);
    }

    template<typename... Args>
    T & emplace_back(Args &&... args)
    {
        m_vector->emplace_back(std::forward<Args>(args)...);
        return m_vector->back();
    }

    void reserve(size_t s)
    {
        m_vector->reserve(s);
    }

    T * data()
    {
        return m_vector ? m_vector->data() : nullptr;
    }

    T * data() const
    {
        return m_vector ? m_vector->data() : nullptr;
    }

    vec_type & container()
    {
        return *m_vector;
    }

    vec_type const & container() const
    {
        return *m_vector;
    }

    T & back()
    {
        return m_vector->back();
    }

    T const & back() const
    {
        return m_vector->back();
    }

    void resize(size_t s)
    {
        m_vector->resize(s);
    }

    void resize(size_t s, const T & value)
    {
        m_vector->resize(s, value);
    }

private:
    explicit vector_view(std::vector<T> & v): m_vector(&v)
    {
    }

    vector_view(const vector_view &) = delete;
    vector_view & operator=(const vector_view &) = delete;

    vec_type * m_vector = nullptr;
};

template<typename T>
inline vector_view<T> make_view(std::vector<T> & v)
{
    return vector_view<T>{ v };
}

template<typename T>
inline vector_view<T> make_view(std::vector<T> & v, size_t s)
{
    auto view = vector_view<T>{ v };
    view.resize(s);
    return view;
}

template<typename T>
inline vector_view<T> make_view(std::vector<T> & v, size_t s, const T & value)
{
    auto view = vector_view<T>{ v };
    view.resize(s, value);
    return view;
}

} // namespace shn