#pragma once

#include "IMathUtils.h"
#include "nanoflann.hpp"
#include <algorithm>
#include <cinttypes>
#include <vector>

namespace shn
{

class KdTree
{
public:
    using IndexType = uint32_t;

private:
    struct DataSource
    {
        std::vector<V3f> points;
        Box3f bbox;

        inline size_t kdtree_get_point_count() const
        {
            return points.size();
        }

        inline float kdtree_distance(const float * p1, const size_t idx_p2, size_t size) const
        {
            const V3f p(p1[0], p1[1], p1[2]);
            return distance(p, points[idx_p2]);
        }

        inline float kdtree_get_pt(const size_t idx, int dim) const
        {
            return points[idx][dim];
        }

        template<class BBOX>
        bool kdtree_get_bbox(BBOX & bb) const
        {
            for (auto i = 0; i < 3; ++i)
                bb[i].low = bbox.min[0];
            for (auto i = 0; i < 3; ++i)
                bb[i].high = bbox.max[0];
            return true;
        }
    };

    using KdTreeT = nanoflann::KDTreeSingleIndexAdaptor<nanoflann::L2_Simple_Adaptor<float, DataSource>, DataSource, 3, IndexType>;

    DataSource m_DataSource;
    KdTreeT m_Index{ 3, m_DataSource };

public:
    template<typename PositionFunctor>
    void build(size_t count, PositionFunctor getPosition)
    {
        m_DataSource.bbox.makeEmpty();
        m_DataSource.points.clear();

        m_DataSource.points.reserve(count);
        for (size_t i = 0; i < count; ++i)
        {
            m_DataSource.points.emplace_back(getPosition(i));
            m_DataSource.bbox.extendBy(m_DataSource.points.back());
        }
        m_Index.buildIndex();
    }

    size_t knnSearch(const V3f & point, size_t nnCount, IndexType * outIdxBuffer, float * outSqrDistBuffer) const
    {
        return m_Index.knnSearch(&point[0], nnCount, outIdxBuffer, outSqrDistBuffer);
    }

    size_t radiusSearch(const V3f & point, float radius, std::vector<std::pair<IndexType, float>> & outIdxDist) const
    {
        return m_Index.radiusSearch(&point[0], radius, outIdxDist, nanoflann::SearchParams());
    }
};

} // namespace shn
