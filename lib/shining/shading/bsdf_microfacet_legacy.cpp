#include "CustomClosure.h"
#include "DebugUtils.h"
#include "OslHeaders.h"

namespace
{
inline float sqr(float x)
{
    return x * x;
}
float GTR2_aniso(float NdotH, float HdotX, float HdotY, float ax, float ay)
{
    return 1.f / (float(M_PI) * ax * ay * sqr(sqr(HdotX / ax) + sqr(HdotY / ay) + NdotH * NdotH));
}
float GTR2(float NdotH, float a)
{
    float a2 = a * a;
    float t = 1.f + (a2 - 1.f) * NdotH * NdotH;
    return a2 / (float(M_PI) * t * t);
}
/*float smithG_GGX(float Ndotv, float alphaG)
{
    float a = alphaG*alphaG;
    float b = Ndotv*Ndotv;
    return 1.f/(Ndotv + sqrtf(a + b - a*b));
}*/
float smithG_GGX(float Ndotv, float alphaG)
{
    return 2.f / (1.f + sqrtf(1.f + alphaG * alphaG * (1 - Ndotv * Ndotv) / (Ndotv * Ndotv)));
}
} // namespace

namespace shn
{

// microfacet model with GGX facet distribution
// see http://www.graphics.cornell.edu/~bjw/microfacetbsdf.pdf
template<int Refractive = 0>
class MicrofacetGGXLegacyClosure: public BSDFClosure
{
public:
    V3f m_N;
    float m_ag; // width parameter (roughness)
    float m_eta; // index of refraction (for fresnel term)
    MicrofacetGGXLegacyClosure(): BSDFClosure(ScatteringTypeBits::Glossy, Refractive ? ScatteringEventBits::Transmit : ScatteringEventBits::Reflect)
    {
    }

    void setup()
    {
    }

    bool mergeable(const ClosurePrimitive * other) const
    {
        const MicrofacetGGXLegacyClosure * comp = (const MicrofacetGGXLegacyClosure *) other;
        return m_N == comp->m_N && m_ag == comp->m_ag && m_eta == comp->m_eta && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return Refractive ? "microfacet_ggx_refraction_legacy" : "microfacet_ggx_legacy";
    }

    void print(std::ostream & out) const
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << m_ag << ", ";
        out << m_eta;
        out << ")";
    }

    float albedo(const V3f & omega_out) const
    {
        const auto cosA = m_N.dot(omega_out);
        if (cosA == 0.f)
            return 0.f;

        if (Refractive == 0)
            return fresnelDielectric(cosA, m_eta);
        else
            return 1.0f - fresnelDielectric(cosA, m_eta);
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        if (Refractive == 1)
        {
            return BxDFDirSample::MCEstimate();
        }
        const float cosNO = fabsf(m_N.dot(omega_out));
        const float cosNI = m_N.dot(omega_in);

        // Half vector
        V3f Hr = omega_in + omega_out;
        Hr.normalize();
        // eq. 20: (F*G*D)/(4*in*on)
        // eq. 33: first we calculate D(m) with m=Hr:
        const float alpha2 = m_ag * m_ag;
        const float cosThetaM = fabsf(m_N.dot(Hr));
        const float cosThetaM2 = cosThetaM * cosThetaM;

        if (cosThetaM2 == 0.f) // Well nothing can be good after that...
            return BxDFDirSample::MCEstimate();

        const float tanThetaM2 = (1 - cosThetaM2) / cosThetaM2;

        const float cosThetaM4 = cosThetaM2 * cosThetaM2;
        const float D = alpha2 / ((float) M_PI * cosThetaM4 * (alpha2 + tanThetaM2) * (alpha2 + tanThetaM2));

        // eq. 24
        const float pm = D * cosThetaM;

        // convert into pdf of the sampled direction
        // eq. 38 - but see also:
        // eq. 17 in http://www.graphics.cornell.edu/~bjw/wardnotes.pdf
        float denom = fabsf(Hr.dot(omega_out));
        if (denom == 0.f)
        {
            return BxDFDirSample::MCEstimate();
        }
        float pdf = pm * 0.25f / denom;

        if (sg.N.dot(omega_in) <= 0.f)
        { // Prevent light leaking
            return BxDFDirSample::MCEstimate::fromWeight(Color3(0), pdf);
        }

        // eq. 34: now calculate G1(i,m) and G1(o,m)
        const float G1o = 2 / (1 + sqrtf(1 + alpha2 * (1 - cosNO * cosNO) / (cosNO * cosNO)));
        const float G1i = 2 / (1 + sqrtf(1 + alpha2 * (1 - cosNI * cosNI) / (cosNI * cosNI)));
        const float G = G1o * G1i;

        return BxDFDirSample::MCEstimate::fromWeight(Color3((G * D) * 0.25f / cosNO / pdf), pdf); // Multiplied by pdf for legacy reasons
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        if (Refractive == 0)
        {
            return BxDFDirSample::MCEstimate();
        }
        const float cosNO = fabsf(m_N.dot(omega_out));
        const float cosNI = m_N.dot(omega_in);

        // compute half-vector of the refraction (eq. 16)
        const V3f ht = -(m_eta * omega_in + omega_out);
        const V3f Ht = ht.normalized();
        // compute fresnel term
        const float cosHO = Ht.dot(omega_out);

        if (cosHO == 0.f)
        {
            return BxDFDirSample::MCEstimate();
        }

        const float Ft = 1 - fresnelDielectric(cosHO, m_eta);

        if (Ft <= 0.f)
        { // skip work in case of TIR
            return BxDFDirSample::MCEstimate();
        }

        const float cosHI = Ht.dot(omega_in);
        // eq. 33: first we calculate D(m) with m=Ht:
        const float alpha2 = m_ag * m_ag;
        const float cosThetaM = m_N.dot(Ht);
        const float cosThetaM2 = cosThetaM * cosThetaM;

        if (cosThetaM2 == 0.f) // Well nothing can be good after that...
            return BxDFDirSample::MCEstimate();

        const float tanThetaM2 = (1 - cosThetaM2) / cosThetaM2;
        const float cosThetaM4 = cosThetaM2 * cosThetaM2;
        const float D = alpha2 / ((float) M_PI * cosThetaM4 * (alpha2 + tanThetaM2) * (alpha2 + tanThetaM2));

        // probability
        const float invHt2 = 1 / ht.dot(ht);
        float pdf = D * fabsf(cosThetaM) * (fabsf(cosHI) * (m_eta * m_eta)) * invHt2;

        if (sg.N.dot(omega_in) >= 0)
        { // Prevent light leaking
            return BxDFDirSample::MCEstimate::fromWeight(Color3(0), pdf);
        }

        // eq. 34: now calculate G1(i,m) and G1(o,m)
        float G1o = 2 / (1 + sqrtf(1 + alpha2 * (1 - cosNO * cosNO) / (cosNO * cosNO)));
        float G1i = 2 / (1 + sqrtf(1 + alpha2 * (1 - cosNI * cosNI) / (cosNI * cosNI)));
        float G = G1o * G1i;

        return BxDFDirSample::MCEstimate::fromWeight(Color3((fabsf(cosHI * cosHO) * (m_eta * m_eta) * (Ft * G * D) * invHt2) / cosNO), pdf);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        const float cosNO = fabsf(m_N.dot(omegaOut.val()));

        V3f X, Y, Z = m_N;
        makeOrthonormals(Z, X, Y);
        // generate a random microfacet normal m
        // eq. 35,36:
        // we take advantage of cos(atan(x)) == 1/sqrt(1+x^2)
        //                  and sin(atan(x)) == x/sqrt(1+x^2)
        const float alpha2 = m_ag * m_ag;
        const float tanThetaM2 = alpha2 * u1 / (1 - u1);
        const float cosThetaM = 1 / sqrtf(1 + tanThetaM2); // Is always >= 0.
        const float sinThetaM = cosThetaM * sqrtf(tanThetaM2);
        const float phiM = 2 * float(M_PI) * u2;
        const V3f m = cosf(phiM) * sinThetaM * X + sinf(phiM) * sinThetaM * Y + cosThetaM * Z;

        if (Refractive == 0)
        {
            // Reflective case:

            const float cosMO = m.dot(omegaOut.val());

            // eq. 33
            const float cosThetaM2 = cosThetaM * cosThetaM;
            const float cosThetaM4 = cosThetaM2 * cosThetaM2;
            const float D = alpha2 / (float(M_PI) * cosThetaM4 * (alpha2 + tanThetaM2) * (alpha2 + tanThetaM2));
            // eq. 24
            const float pm = D * cosThetaM;
            // convert into pdf of the sampled direction
            // eq. 38 - but see also:
            // eq. 17 in http://www.graphics.cornell.edu/~bjw/wardnotes.pdf
            float absCosMO = fabsf(cosMO);
            if (absCosMO <= 0.f)
                return BxDFDirSample();

            const float pdf = pm * 0.25f / fabsf(cosMO);

            OSL::Dual2<V3f> omegaIn;
            // eq. 39 - compute actual reflected direction
            omegaIn.val() = 2 * cosMO * m - omegaOut.val();

            omegaIn.dx() = (2 * m.dot(omegaOut.dx())) * m - omegaOut.dx();
            omegaIn.dy() = (2 * m.dot(omegaOut.dy())) * m - omegaOut.dy();
            // Since there is some blur to this reflection, make the
            // derivatives a bit bigger. In theory this varies with the
            // roughness but the exact relationship is complex and
            // requires more ops than are practical.
            omegaIn.dx() *= 10;
            omegaIn.dy() *= 10;

            if (sg.N.dot(omegaIn.val()) <= 0) // Prevent light leaking
            {
                return fromWeight(omegaIn, Color3(0.f), pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
            }

            const float cosNI = m_N.dot(omegaIn.val());

            // eval BRDF*cosNI
            // eq. 34: now calculate G1(i,m) and G1(o,m)
            const float G1o = 2 / (1 + sqrtf(1 + alpha2 * (1 - cosNO * cosNO) / (cosNO * cosNO)));
            const float G1i = 2 / (1 + sqrtf(1 + alpha2 * (1 - cosNI * cosNI) / (cosNI * cosNI)));
            const float G = G1o * G1i;
            // fresnel term between outgoing direction and microfacet
            // In the shader
            // float F = fresnel_dielectric(m.dot(omega_out), m_eta);
            // eq. 20: (F*G*D)/(4*in*on)
            const Color3 weight((G * D) * 0.25f / cosNO / pdf);

            return fromWeight(omegaIn, weight, pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
        }

        // Refractive case:

        // CAUTION: the i and o variables are inverted relative to the paper
        // eq. 39 - compute actual refractive direction
        const float cosMO = clamp(dot(m, omegaOut.val()), 0.f, 1.f);

        if (cosMO == 0.f)
            return BxDFDirSample();

        float Ft = 1.f - fresnelDielectric(cosMO, m_eta);
        if (Ft <= 0.f)
            return BxDFDirSample(); // total internal reflection

        const auto T = evalRefractionDirection(omegaOut.val(), m, cosMO, 1.f / m_eta);
        V3f dTdx, dTdy;
        evalRefractionDerivatives(omegaOut.dx(), omegaOut.dy(), m, cosNO, 1.f / m_eta, 0.f, dTdx, dTdy);

        OSL::Dual2<V3f> omegaIn(T, dTdx, dTdy);

        // Since there is some blur to this refraction, make the
        // derivatives a bit bigger. In theory this varies with the
        // roughness but the exact relationship is complex and
        // requires more ops than are practical.
        omegaIn.dx() *= 10;
        omegaIn.dy() *= 10;

        if (T == V3f(0)) // Total internal reflection
        {
            return fromWeight(omegaIn, Color3(0.f), 0.f, ScatteringType::Glossy, ScatteringEvent::Transmit);
        }

        // eq. 33
        const float cosThetaM2 = cosThetaM * cosThetaM;
        const float cosThetaM4 = cosThetaM2 * cosThetaM2;
        const float D = alpha2 / (float(M_PI) * cosThetaM4 * (alpha2 + tanThetaM2) * (alpha2 + tanThetaM2));
        // eq. 24
        const float pm = D * cosThetaM;
        // eval BRDF*cosNI
        const float cosNI = m_N.dot(omegaIn.val());

        // eq. 34: now calculate G1(i,m) and G1(o,m)
        const float G1o = 2 / (1 + sqrtf(1 + alpha2 * (1 - cosNO * cosNO) / (cosNO * cosNO)));
        const float G1i = 2 / (1 + sqrtf(1 + alpha2 * (1 - cosNI * cosNI) / (cosNI * cosNI)));
        const float G = G1o * G1i;
        // eq. 21
        const float cosHI = m.dot(omegaIn.val());
        const float cosHO = m.dot(omegaOut.val());
        float Ht2 = m_eta * cosHI + cosHO;
        Ht2 *= Ht2;
        // eq. 38 and eq. 17
        const float pdf = pm * (m_eta * m_eta) * fabsf(cosHI) / Ht2;

        if (Ft <= 0.f || sg.N.dot(omegaIn.val()) >= 0.f) // Prevent light leaking
            return fromWeight(omegaIn, Color3(0.f), pdf, ScatteringType::Glossy, ScatteringEvent::Transmit);

        const Color3 weight((fabsf(cosHI * cosHO) * (m_eta * m_eta) * (Ft * G * D)) / (cosNO * Ht2) / pdf);

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Glossy, ScatteringEvent::Transmit);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

// microfacet model with GGX facet distribution
// see http://www.graphics.cornell.edu/~bjw/microfacetbsdf.pdf
template<int Refractive = 0>
class MicrofacetGGXAnisoLegacyClosure: public BSDFClosure
{
public:
    V3f m_N;
    V3f m_U;
    float m_eta; // index of refraction (for fresnel term)
    float m_roughness; // isotropic roughness
    float m_ax; // Anisotropy factor (x)
    float m_ay; // Anisotropy factor (y)
    MicrofacetGGXAnisoLegacyClosure(): BSDFClosure(ScatteringTypeBits::Glossy, Refractive ? ScatteringEventBits::Transmit : ScatteringEventBits::Reflect)
    {
    }

    void setup()
    {
    }

    bool mergeable(const ClosurePrimitive * other) const
    {
        const MicrofacetGGXAnisoLegacyClosure * comp = (const MicrofacetGGXAnisoLegacyClosure *) other;
        return m_N == comp->m_N && m_U == comp->m_U && m_ax == comp->m_ax && m_ay == comp->m_ay && m_eta == comp->m_eta && m_roughness == comp->m_roughness && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return Refractive ? "microfacet_ggx_aniso_refraction_legacy" : "microfacet_ggx_aniso_legacy";
    }

    void print(std::ostream & out) const
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << "(" << m_U[0] << ", " << m_U[1] << ", " << m_U[2] << "), ";
        out << m_ax << ", ";
        out << m_ay << ", ";
        out << m_eta;
        out << ")";
    }

    float albedo(const V3f & omega_out) const
    {
        const auto cosA = m_N.dot(omega_out);
        if (cosA == 0.f)
            return 0.f;

        if (Refractive == 0)
            return fresnelDielectric(cosA, m_eta);
        else
            return 1.0f - fresnelDielectric(cosA, m_eta);
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        if (Refractive == 1)
        {
            return BxDFDirSample::MCEstimate();
        }

        float NdotV = m_N.dot(omega_out.normalized());
        float NdotL = m_N.dot(omega_in.normalized());

        if (NdotL < 0 || NdotV < 0)
        {
            return BxDFDirSample::MCEstimate();
        }

        const float anisotropic = 0.f;
        const float aspect = sqrtf(1.f - anisotropic * .9f);
        const V3f Hr = (omega_in + omega_out).normalized();
        const float HdotV = Hr.dot(omega_out.normalized());
        const float NdotH = m_N.dot(Hr);
        V3f X, Y;
        makeOrthonormals(m_N, m_U, X, Y);
        // float F = fresnel_dielectric(HdotV, m_eta);
        const float D = GTR2_aniso(NdotH, Hr.dot(X), Hr.dot(Y), m_ax, m_ay);

        float pdf = ((D * NdotH) * 0.25f) / HdotV;

        if (sg.N.dot(omega_in) <= 0.f)
        { // Prevent light leaking
            return BxDFDirSample::MCEstimate::fromWeight(Color3(0), pdf);
        }

        const float roughg = sqr(m_roughness * .5f + .5f);
        const float G = smithG_GGX(NdotL, roughg) * smithG_GGX(NdotV, roughg);

        return BxDFDirSample::MCEstimate::fromWeight(Color3(D * G * 0.25f / NdotV / pdf), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;

        if (Refractive == 1)
        {
            // Refraction, not implemented for anisotropic microfacet ggx legacy
            return BxDFDirSample();
        }

        float NdotV = m_N.dot(omegaOut.val().normalized());
        if (NdotV < 0)
            return BxDFDirSample();

        const float sinPhiH = m_ay * sinf(float(M_PI) * 2.f * u1) / m_roughness;
        const float cosPhiH = m_ax * cosf(float(M_PI) * 2.f * u1) / m_roughness;
        const float tanThetaH = m_roughness * sqrtf(u2 / (1.f - u2));
        V3f X, Y;
        makeOrthonormals(m_N, m_U, X, Y);
        const V3f h = (sqrtf(u2 / (1.f - u2)) * (m_ax * cosf(float(M_PI) * 2.f * u1) * X + m_ay * sinf(float(M_PI) * 2.f * u1) * Y) + m_N).normalized();
        const float HdotV = h.dot(omegaOut.val());
        const float NdotH = m_N.dot(h);

        omegaIn.val() = (2.f * HdotV * h - omegaOut.val()).normalized();
        omegaIn.dx() = (2.f * h.dot(omegaOut.dx())) * h - omegaOut.dx();
        omegaIn.dy() = (2.f * h.dot(omegaOut.dy())) * h - omegaOut.dy();
        omegaIn.dx() *= 10.f;
        omegaIn.dy() *= 10.f;

        const float D = GTR2_aniso(NdotH, h.dot(X), h.dot(Y), m_ax, m_ay);
        const float pdf = ((D * NdotH) * 0.25f) / HdotV;

        if (sg.N.dot(omegaIn.val()) <= 0.f) // Prevent light leaking
            return BxDFDirSample();

        const float roughg = sqr(m_roughness * .5f + .5f);

        const V3f Hr = (omegaIn.val() + omegaOut.val()).normalized();

        const float NdotL = m_N.dot(omegaIn.val());
        const float G = smithG_GGX(NdotL, roughg) * smithG_GGX(NdotV, roughg);

        Color3 weight(G * D * 0.25f / NdotV / pdf);
        return fromWeight(omegaIn, weight, pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_microfacet_ggx_legacy_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXLegacyClosure<0>, m_N), CLOSURE_FLOAT_PARAM(MicrofacetGGXLegacyClosure<0>, m_ag),
                                                     CLOSURE_FLOAT_PARAM(MicrofacetGGXLegacyClosure<0>, m_eta), CLOSURE_FINISH_PARAM(MicrofacetGGXLegacyClosure<0>) };

ClosureParam bsdf_microfacet_ggx_refraction_legacy_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXLegacyClosure<1>, m_N), CLOSURE_FLOAT_PARAM(MicrofacetGGXLegacyClosure<1>, m_ag),
                                                                CLOSURE_FLOAT_PARAM(MicrofacetGGXLegacyClosure<1>, m_eta), CLOSURE_FINISH_PARAM(MicrofacetGGXLegacyClosure<1>) };

ClosureParam bsdf_microfacet_ggx_aniso_legacy_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXAnisoLegacyClosure<0>, m_N),  CLOSURE_VECTOR_PARAM(MicrofacetGGXAnisoLegacyClosure<0>, m_U),
                                                           CLOSURE_FLOAT_PARAM(MicrofacetGGXAnisoLegacyClosure<0>, m_eta), CLOSURE_FLOAT_PARAM(MicrofacetGGXAnisoLegacyClosure<0>, m_roughness),
                                                           CLOSURE_FLOAT_PARAM(MicrofacetGGXAnisoLegacyClosure<0>, m_ax),  CLOSURE_FLOAT_PARAM(MicrofacetGGXAnisoLegacyClosure<0>, m_ay),
                                                           CLOSURE_FINISH_PARAM(MicrofacetGGXAnisoLegacyClosure<0>) };

ClosureParam bsdf_microfacet_ggx_aniso_refraction_legacy_params[] = {
    CLOSURE_VECTOR_PARAM(MicrofacetGGXAnisoLegacyClosure<1>, m_N),  CLOSURE_VECTOR_PARAM(MicrofacetGGXAnisoLegacyClosure<1>, m_U),
    CLOSURE_FLOAT_PARAM(MicrofacetGGXAnisoLegacyClosure<1>, m_eta), CLOSURE_FLOAT_PARAM(MicrofacetGGXAnisoLegacyClosure<0>, m_roughness),
    CLOSURE_FLOAT_PARAM(MicrofacetGGXAnisoLegacyClosure<1>, m_ax),  CLOSURE_FLOAT_PARAM(MicrofacetGGXAnisoLegacyClosure<1>, m_ay),
    CLOSURE_FINISH_PARAM(MicrofacetGGXAnisoLegacyClosure<1>)
};

CLOSURE_PREPARE(bsdf_microfacet_ggx_legacy_prepare, MicrofacetGGXLegacyClosure<0>);
CLOSURE_PREPARE(bsdf_microfacet_ggx_refraction_legacy_prepare, MicrofacetGGXLegacyClosure<1>);
CLOSURE_PREPARE(bsdf_microfacet_ggx_aniso_legacy_prepare, MicrofacetGGXAnisoLegacyClosure<0>);
CLOSURE_PREPARE(bsdf_microfacet_ggx_aniso_refraction_legacy_prepare, MicrofacetGGXAnisoLegacyClosure<1>);

} // namespace shn
