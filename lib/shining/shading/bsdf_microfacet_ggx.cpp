#include "BSDFClosure.h"
#include "CustomClosure.h"
#include "ShadingUtils.h"
#include "ThreadUtils.h"
#include "sampling/PiecewiseDistributions.h"
#include "sampling/RandomState.h"
#include "sampling/ShapeSamplers.h"
#include <atomic>

namespace shn
{

class MicrofacetGGXRefractionClosure: public BSDFClosure
{
public:
    V3f m_N; // shading normal
    float m_roughness;
    float m_etaI;
    float m_etaO;
    float m_absorption;
    float m_absorptionDistance;
    V3f m_absorptionColor;

    MicrofacetGGXRefractionClosure(): BSDFClosure(ScatteringTypeBits::Glossy, ScatteringEventBits::Transmit)
    {
    }

    void setup()
    {
    }

    bool mergeable(const ClosurePrimitive * other) const
    {
        const MicrofacetGGXRefractionClosure * comp = (const MicrofacetGGXRefractionClosure *) other;
        return m_N == comp->m_N && m_roughness == comp->m_roughness && m_etaI == comp->m_etaI && m_etaO == comp->m_etaO && m_absorption == comp->m_absorption &&
            m_absorptionDistance == comp->m_absorptionDistance && m_absorptionColor == comp->m_absorptionColor && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "shn_microfacet_ggx_refraction";
    }

    void print(std::ostream & out) const
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << m_roughness << ", ";
        out << m_etaI << ", ";
        out << m_etaO << ", ";
        out << m_absorption << ", ";
        out << m_absorptionDistance << ", ";
        out << "(" << m_absorptionColor[0] << ", " << m_absorptionColor[1] << ", " << m_absorptionColor[2] << "))";
    }

    float albedo(const V3f & omega_out) const;

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override;

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override;

    V3f normal() const override
    {
        return m_N;
    }
};

float MicrofacetGGXRefractionClosure::albedo(const V3f & omega_out) const
{
    if (m_etaO == m_etaI || m_etaI == 0.f)
        return 0.f;

    return GGXCompensation::ggxRefractAlbedo(m_etaO / m_etaI, m_roughness, clamp(dot(omega_out, m_N), 0.f, 1.f));
}

BxDFDirSample::MCEstimate MicrofacetGGXRefractionClosure::evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const
{
    if (m_etaO == m_etaI)
        return BxDFDirSample::MCEstimate();

    TangentFrame tf(m_N);
    const V3f I = tf.tolocal(omega_out);
    const V3f O = tf.tolocal(omega_in);
    const V3f N(0.f, 0.f, 1.f);
    const float IdotN = dot(I, N);
    const float OdotN = dot(O, N);
    const float alpha2 = m_roughness * m_roughness;
    const float IdotO = dot(I, O);

    if (IdotN <= 0.f)
        return BxDFDirSample::MCEstimate();

    // build Ht half vector for refraction - here use Ht as M
    const V3f Ht = evalRefractionHalfVector(I, O, m_etaI, m_etaO);
    const float HdotN = Ht.z;
    if (HdotN <= 0.f) // This should be positive because sampleGGXAnisoMicronormal is always sampling the up hemisphere of N
        return BxDFDirSample::MCEstimate();

    // This two dot product must have a different sign (otherwise I and O are pointing to the same hemisphere and it is not a refraction).
    // If IdotH * OdotH > 0.f (same sign), then the contribution is zero, but we still compute the remaining because we need the pdf for this zero contribution in order for MIS
    // to work correctly.
    // If we early exit here, we get high variance in rendererings.
    const float IdotH = dot(I, Ht);
    const float OdotH = dot(O, Ht);

    // compute micronormals distribution D_ht
    const float D_ht = evalGGX_D(Ht, HdotN, alpha2);

    // compute shadowing-masking term G_hr
    const float G_ht = evalGGX_G1(I, IdotH, IdotN, alpha2) * evalGGX_G1(O, OdotH, OdotN, alpha2);

    // compute fresnel F_hr
    const float F_ht = fresnelDielectric(IdotH, m_etaO / m_etaI);

    // compute the reflection BTDF Ft
    const float Ft = evalMicrofacetBTDF(D_ht, G_ht, F_ht, IdotN, IdotH, OdotN, OdotH, m_etaI, m_etaO);

    // compute the Jacobian of the half direction transform
    const float Jcb_ht = evalRefractionJacobian(IdotH, OdotH, m_etaI, m_etaO);

    // compute eval
    const float eval = microfacetEval(
        Ft, OdotN); /* *sqr(m_etaI / m_etaO); // Radiance flows from O to I, so n_t is m_etaI and n_i is m_etaO if we think in terms of radiance flow (see Veach thesis chapt5 p. 140)*/

    // compute pdf
    const float pdf = microfacetPDF(D_ht, Jcb_ht, HdotN);
    if (pdf <= 0.f)
        return BxDFDirSample::MCEstimate();

    Color3 evalColor(IdotH * OdotH < 0.f ? eval : 0.f);

    // compute absorption
    auto renderState = sg.renderstate;
    if (eval > 0.f && m_absorption > 0.f && renderState)
    {
        const shn::RenderState * state = reinterpret_cast<const shn::RenderState *>(renderState);
        evalColor *= rayTraceAbsorption(state->scene, sg.P, omega_in, sg.time, m_absorption, m_absorptionDistance, m_absorptionColor);
    }

    return BxDFDirSample::MCEstimate::fromEval(evalColor, pdf);
}

BxDFDirSample MicrofacetGGXRefractionClosure::sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const
{
    const auto u1 = uDir.x;
    const auto u2 = uDir.y;

    // Fake transparency in case etaO == etaI. In that case, the media are the same and there is no visible deformations (eg. air => air)
    if (m_etaO == m_etaI)
        return fromEval(-omegaOut, Color3(1.f), 1.f, ScatteringType::Glossy, ScatteringEvent::Transmit);

    // build tangent frame
    // we will do all the computation in tangent space to simplify thetas computing
    // To match the paper convention and be more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
    TangentFrame tf(m_N);
    const V3f I = tf.tolocal(omegaOut.val());
    const V3f dIdx = tf.tolocal(omegaOut.dx());
    const V3f dIdy = tf.tolocal(omegaOut.dy());
    const V3f N(0.f, 0.f, 1.f);
    const float IdotN = dot(I, N);
    const float alpha2 = m_roughness * m_roughness;

    // Macro surface not visible from I (omegaOut)
    if (IdotN <= 0.f)
        return BxDFDirSample();

    // sample M
    const V3f M = sampleGGXMicronormal(m_roughness, u1, u2);
    const float IdotM = dot(I, M);
    if (IdotM <= 0.f) // Lets ignore all backfacing micro-normals
        return BxDFDirSample();

    const float MdotN = M.z;
    assert(MdotN >= 0.f);

    // compute fresnel F_m
    const float F_m = fresnelDielectric(IdotM, m_etaO / m_etaI);

    if (F_m == 1.f)
    {
        // Total internal reflection
        return BxDFDirSample();
    }

    // compute O
    float etaRatio = m_etaI / m_etaO;
    const V3f O = evalRefractionDirection(I, M, IdotM, etaRatio);
    V3f dOdx, dOdy;
    evalRefractionDerivatives(dIdx, dIdy, M, IdotM, etaRatio, m_roughness, dOdx, dOdy);
    const float OdotN = dot(O, N);
    const float OdotM = dot(O, M);

    // compute micronormals distribution D_m
    const float D_m = evalGGX_D(M, MdotN, alpha2);

    // compute shadowing-masking term G_m
    const float G_m = evalGGX_G1(I, IdotM, IdotN, alpha2) * evalGGX_G1(O, OdotM, OdotN, alpha2);

    // compute the reflection BTDF Ft
    const float Ft = evalMicrofacetBTDF(D_m, G_m, F_m, IdotN, IdotM, OdotN, OdotM, m_etaI, m_etaO);

    // compute the Jacobian of the half direction transform
    const float Jcb_m = evalRefractionJacobian(IdotM, OdotM, m_etaI, m_etaO);

    // compute eval
    const float eval = microfacetEval(Ft, OdotN);

    // compute pdf
    const float pdf = microfacetPDF(D_m, Jcb_m, MdotN);
    if (pdf <= 0.f)
        return BxDFDirSample();

    V3f evalColor(eval);
    OSL::Dual2<V3f> omegaIn(tf.toworld(O), tf.toworld(dOdx), tf.toworld(dOdy));

    // compute absorption
    auto renderState = sg.renderstate;
    if (eval > 0.f && m_absorption > 0.f && renderState)
    {
        const shn::RenderState * state = reinterpret_cast<const shn::RenderState *>(renderState);
        evalColor *= rayTraceAbsorption(state->scene, sg.P, omegaIn.val(), sg.time, m_absorption, m_absorptionDistance, m_absorptionColor);
    }

    return fromEval(omegaIn, evalColor, pdf, ScatteringType::Glossy, ScatteringEvent::Transmit);
}

class MicrofacetGGXRefractionAnisotropyClosure: public BSDFClosure
{
public:
    V3f m_N; // shading normal
    V3f m_U; // shading tangent
    float m_roughness;
    float m_etaI;
    float m_etaO;
    float m_ax; // anisotropy factor alpha x
    float m_ay; // anisotropy factor alpha y
    float m_absorption;
    float m_absorptionDistance;
    V3f m_absorptionColor;

    MicrofacetGGXRefractionAnisotropyClosure(): BSDFClosure(ScatteringTypeBits::Glossy, ScatteringEventBits::Transmit)
    {
    }

    void setup()
    {
    }

    bool mergeable(const ClosurePrimitive * other) const
    {
        const MicrofacetGGXRefractionAnisotropyClosure * comp = (const MicrofacetGGXRefractionAnisotropyClosure *) other;
        return m_N == comp->m_N && m_U == comp->m_U && m_roughness == comp->m_roughness && m_etaI == comp->m_etaI && m_etaO == comp->m_etaO && m_ax == comp->m_ax && m_ay == comp->m_ay &&
            m_absorption == comp->m_absorption && m_absorptionDistance == comp->m_absorptionDistance && m_absorptionColor == comp->m_absorptionColor && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "shn_microfacet_ggx_refraction";
    }

    void print(std::ostream & out) const
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << "(" << m_U[0] << ", " << m_U[1] << ", " << m_U[2] << "), ";
        out << m_roughness << ", ";
        out << m_etaI << ", ";
        out << m_etaO << ", ";
        out << m_ax << ", ";
        out << m_ay << ", ";
        out << m_absorption << ", ";
        out << m_absorptionDistance << ", ";
        out << "(" << m_absorptionColor[0] << ", " << m_absorptionColor[1] << ", " << m_absorptionColor[2] << "))";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0 - fresnelDielectric(-m_N.dot(omega_out), m_etaO / m_etaI);
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        if (m_etaO == m_etaI)
            return BxDFDirSample::MCEstimate();

        TangentFrame tf(m_N, m_U);
        const V3f I = tf.tolocal(omega_out);
        const V3f O = tf.tolocal(omega_in);
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = dot(I, N);
        const float OdotN = dot(O, N);
        const float alpha2 = m_roughness * m_roughness;
        const float IdotO = dot(I, O);

        // light leaking early exit
        if (IdotO > 0.f || IdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        // build Ht half vector for refraction - here use Ht as M
        const V3f Ht = evalRefractionHalfVector(I, O, m_etaI, m_etaO);
        const float HdotN = Ht.z;
        if (HdotN <= 0.f) // This should be positive because sampleGGXAnisoMicronormal is always sampling the up hemisphere of N
            return BxDFDirSample::MCEstimate();

        // This two dot product must have a different sign (otherwise I and O are pointing to the same hemisphere and it is not a refraction).
        // If IdotH * OdotH > 0.f (same sign), then the contribution is zero, but we still compute the remaining because we need the pdf for this zero contribution in order for MIS
        // to work correctly.
        // If we early exit here, we get high variance in rendererings.
        const float IdotH = dot(I, Ht);
        const float OdotH = dot(O, Ht);

        // compute micronormals distribution D_ht
        const float D_ht = evalGGXAniso_D(Ht, HdotN, m_ax, m_ay);

        // compute shadowing-masking term G_hr
        const float G_ht = evalGGXAniso_G1(I, IdotH, IdotN, m_ax, m_ay) * evalGGXAniso_G1(O, OdotH, OdotN, m_ax, m_ay);

        // compute fresnel F_hr
        const float F_ht = fresnelDielectric(IdotH, m_etaO / m_etaI);

        // compute the reflection BTDF Ft
        const float Ft = evalMicrofacetBTDF(D_ht, G_ht, F_ht, IdotN, IdotH, OdotN, OdotH, m_etaI, m_etaO);

        // compute the Jacobian of the half direction transform
        const float Jcb_ht = evalRefractionJacobian(IdotH, OdotH, m_etaI, m_etaO);

        // compute eval
        const float eval = microfacetEval(Ft, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_ht, Jcb_ht, HdotN);
        if (pdf <= 0.f)
            return BxDFDirSample::MCEstimate();

        Color3 evalColor(IdotH * OdotH < 0.f ? eval : 0.f);

        // compute absorption
        auto renderState = sg.renderstate;
        if (eval > 0.f && m_absorption > 0.f && renderState)
        {
            const shn::RenderState * state = reinterpret_cast<const shn::RenderState *>(renderState);
            evalColor *= rayTraceAbsorption(state->scene, sg.P, omega_in, sg.time, m_absorption, m_absorptionDistance, m_absorptionColor);
        }

        return BxDFDirSample::MCEstimate::fromEval(evalColor, pdf);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        // Fake transparency in case etaO == etaI. In that case, the media are the same and there is no visible deformations (eg. air => air)
        if (m_etaO == m_etaI)
            return fromEval(-omegaOut, Color3(1.f), 1.f, ScatteringType::Glossy, ScatteringEvent::Transmit);

        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To match the paper convention and be more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        TangentFrame tf(m_N, m_U);
        const V3f I = tf.tolocal(omegaOut.val());
        const V3f dIdx = tf.tolocal(omegaOut.dx());
        const V3f dIdy = tf.tolocal(omegaOut.dy());
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = dot(I, N);
        const float alpha2 = m_roughness * m_roughness;

        // light leaking early exit
        if (IdotN <= 0.f)
            return BxDFDirSample();

        // sample M
        const V3f M = sampleGGXAnisoMicronormal(m_ax, m_ay, u1, u2);
        const float IdotM = dot(I, M);
        if (IdotM <= 0.f) // Lets ignore all backfacing micro-normals
            return BxDFDirSample();

        const float MdotN = M.z;
        assert(MdotN >= 0.f);

        // compute fresnel F_m
        const float F_m = fresnelDielectric(IdotM, m_etaO / m_etaI);

        if (F_m == 1.f)
        {
            // Total internal reflection
            return BxDFDirSample();
        }

        // compute O
        float etaRatio = m_etaI / m_etaO;
        const V3f O = evalRefractionDirection(I, M, IdotM, etaRatio);
        V3f dOdx, dOdy;
        evalRefractionDerivatives(dIdx, dIdy, M, IdotM, etaRatio, m_roughness, dOdx, dOdy);
        const float OdotN = dot(O, N);
        const float OdotM = dot(O, M);

        // compute micronormals distribution D_m
        const float D_m = evalGGXAniso_D(M, MdotN, m_ax, m_ay);

        // compute shadowing-masking term G_m
        const float G_m = evalGGXAniso_G1(I, IdotM, IdotN, m_ax, m_ay) * evalGGXAniso_G1(O, OdotM, OdotN, m_ax, m_ay);

        // compute the reflection BTDF Ft
        const float Ft = evalMicrofacetBTDF(D_m, G_m, F_m, IdotN, IdotM, OdotN, OdotM, m_etaI, m_etaO);

        // compute the Jacobian of the half direction transform
        const float Jcb_m = evalRefractionJacobian(IdotM, OdotM, m_etaI, m_etaO);

        // compute eval
        const float eval = microfacetEval(Ft, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_m, Jcb_m, MdotN);
        if (pdf <= 0.f)
            return BxDFDirSample();

        V3f evalColor(eval);
        OSL::Dual2<V3f> omegaIn(tf.toworld(O), tf.toworld(dOdx), tf.toworld(dOdy));

        // compute absorption
        auto renderState = sg.renderstate;
        if (eval > 0.f && m_absorption > 0.f && renderState)
        {
            const shn::RenderState * state = reinterpret_cast<const shn::RenderState *>(renderState);
            evalColor *= rayTraceAbsorption(state->scene, sg.P, omegaIn.val(), sg.time, m_absorption, m_absorptionDistance, m_absorptionColor);
        }

        return fromEval(omegaIn, evalColor, pdf, ScatteringType::Glossy, ScatteringEvent::Transmit);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_microfacet_ggx_refraction_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXRefractionClosure, m_N),
                                                         CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionClosure, m_roughness),
                                                         CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionClosure, m_etaI),
                                                         CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionClosure, m_etaO),
                                                         CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionClosure, m_absorption),
                                                         CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionClosure, m_absorptionDistance),
                                                         CLOSURE_COLOR_PARAM(MicrofacetGGXRefractionClosure, m_absorptionColor),
                                                         CLOSURE_FINISH_PARAM(MicrofacetGGXRefractionClosure) };

ClosureParam bsdf_microfacet_ggx_refraction_anisotropy_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_N),
                                                                    CLOSURE_VECTOR_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_U),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_roughness),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_etaI),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_etaO),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_ax),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_ay),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_absorption),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_absorptionDistance),
                                                                    CLOSURE_COLOR_PARAM(MicrofacetGGXRefractionAnisotropyClosure, m_absorptionColor),
                                                                    CLOSURE_FINISH_PARAM(MicrofacetGGXRefractionAnisotropyClosure) };

CLOSURE_PREPARE(bsdf_microfacet_ggx_refraction_prepare, MicrofacetGGXRefractionClosure)
CLOSURE_PREPARE(bsdf_microfacet_ggx_refraction_anisotropy_prepare, MicrofacetGGXRefractionAnisotropyClosure)

class MicrofacetGGXReflectionClosure: public TplBSDFClosure<MicrofacetGGXReflectionClosure, ScatteringTypeBits::Glossy, ScatteringEventBits::Reflect>
{
public:
    V3f m_N; // shading normal
    float m_roughness;
    float m_eta; // eta_o / eta_i

    const char * name() const override
    {
        return "shn_microfacet_ggx_reflection";
    }

    void print(std::ostream & out) const override
    {
        out << name() << " (";
        out << m_N << ", ";
        out << m_roughness << ", ";
        out << m_eta << ")";
    }

    float albedo(const V3f & omega_out) const override
    {
        return GGXCompensation::ggxReflectAlbedo(m_eta, m_roughness, clamp(dot(omega_out, m_N), 0.f, 1.f));
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To match the paper convention and be more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        TangentFrame tf(m_N);
        const V3f I = tf.tolocal(omega_out);
        const V3f O = tf.tolocal(omega_in);
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = I.z;
        const float OdotN = O.z;
        const float alpha2 = m_roughness * m_roughness;

        // light leaking early exit
        if (IdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        // build Hr half vector for reflection - here use Hr as M
        const V3f Hr = evalReflectionHalfVector(I, O);
        const float HdotN = Hr.z;
        if (HdotN <= 0.f) // This should be positive because sampleGGXAnisoMicronormal is always sampling the up hemisphere of N
            return BxDFDirSample::MCEstimate();

        const float IdotH = dot(I, Hr);
        if (IdotH <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float OdotH = dot(O, Hr);

        // compute micronormals distribution D_hr
        const float D_hr = evalGGX_D(Hr, HdotN, alpha2);

        // compute shadowing-masking term G_hr
        const float G_hr = evalGGX_G1(I, IdotH, IdotN, alpha2) * evalGGX_G1(O, OdotH, OdotN, alpha2);

        // compute fresnel F_hr
        const float F_hr = fresnelDielectric(IdotH, m_eta);

        // compute the reflection BRDF Fr
        const float Fr = evalMicrofacetBRDF(D_hr, G_hr, F_hr, IdotN, OdotN);

        // compute the Jacobian of the half direction transform
        const float Jcb_hr = evalReflectionJacobian(OdotH);

        // compute eval
        const float eval = microfacetEval(Fr, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_hr, Jcb_hr, HdotN);
        if (pdf <= 0.f)
            return BxDFDirSample::MCEstimate();

        return BxDFDirSample::MCEstimate::fromEval(Color3(eval), pdf);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To be closer to the paper and more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        TangentFrame tf(m_N);
        const V3f I = tf.tolocal(omegaOut.val());
        const V3f dIdx = tf.tolocal(omegaOut.dx());
        const V3f dIdy = tf.tolocal(omegaOut.dy());
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = I.z;
        const float alpha2 = m_roughness * m_roughness;

        // Macro surface not visible from I (omegaOut)
        if (IdotN <= 0.f)
            return BxDFDirSample();

        // sample M
        const V3f M = sampleGGXMicronormal(m_roughness, u1, u2);
        const float IdotM = dot(I, M);
        if (IdotM <= 0.f) // Lets ignore all backfacing micro-normals
            return BxDFDirSample();

        const float MdotN = M.z;
        assert(MdotN >= 0.f);

        // compute O
        const V3f O = evalReflectionDirection(I, M, IdotM);
        V3f dOdx, dOdy;
        evalReflectionDerivatives(dIdx, dIdy, M, m_roughness, dOdx, dOdy);
        const float OdotN = O.z;
        const float OdotM = dot(O, M);

        // compute micronormals distribution D_m
        const float D_m = evalGGX_D(M, MdotN, alpha2);

        // compute shadowing-masking term G_m
        const float G_m = evalGGX_G1(I, IdotM, IdotN, alpha2) * evalGGX_G1(O, OdotM, OdotN, alpha2);

        // compute fresnel F_m
        const float F_m = fresnelDielectric(IdotM, m_eta);

        // compute the reflection BRDF Fr
        const float Fr = evalMicrofacetBRDF(D_m, G_m, F_m, IdotN, OdotN);

        // compute the Jacobian of the half direction transform
        const float Jcb_m = evalReflectionJacobian(OdotM);

        // compute eval
        const float eval = microfacetEval(Fr, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_m, Jcb_m, MdotN);
        if (pdf <= 0.f)
            return BxDFDirSample();

        OSL::Dual2<V3f> omegaIn(tf.toworld(O), tf.toworld(dOdx), tf.toworld(dOdy));

        return fromEval(omegaIn, Color3(eval), pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
    }
};

static float fresnelDielectricAverage(float eta)
{
    assert(eta >= 0.f);

    if (eta == 0.f)
        return 1.f;

    if (eta < 1)
    {
        const auto eta2 = eta * eta;
        const auto eta3 = eta2 * eta;
        return 0.997118 + 0.1014 * eta - 0.965241 * eta2 - 0.130607 * eta3;
    }

    return (eta - 1) / (4.08567 + 1.00071 * eta);
}

float getIOS(float ior)
{
    return (1 / 0.08f) * sqr(1 - 2.f / (1 + ior));
}

float getIOR(float ios)
{
    return (2.f / (1.f - sqrt(0.08f * ios))) - 1.f;
}

namespace GGXCompensation
{

extern float ReflectAlbedo[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1];
extern float ReflectAlbedoAverage[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1];
extern float ReflectSamplingTable[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N];
extern float RefractAlbedo[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1];
extern float ReflectRefractAlbedo[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1];
extern float ReflectRefractAlbedoAverage[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1];
extern float ReflectRefractSamplingTable[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N];
extern float CompensatedReflectAlbedo[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1];

#ifdef SHN_PRECOMPUTE_GGX_COMPENSATION_DATA
//#define SHN_GGX_COMPENSATION_GLSL_ETA_RANGE // for GLSL porting, we only need eta in [1.0 - 1.8]
#endif

float getEta(size_t etaIdx)
{
#ifndef SHN_GGX_COMPENSATION_GLSL_ETA_RANGE
    const auto halfRange = GGX_COMP_ETA_N / 2;
    if (etaIdx < halfRange)
        return float(etaIdx) / halfRange;
    return 1.f + GGX_COMP_MAX_ETA * (etaIdx - halfRange) / halfRange;
#else
    if (etaIdx == 0)
        return 0.0;

    const float minEta = getIOR(0.f);
    const float maxEta = getIOR(1.f);
    const float rescaleEtaIdx = (float(etaIdx - 1) / float(GGX_COMP_ETA_N - 1)) * GGX_COMP_ETA_N;
    const float ratio = rescaleEtaIdx / float(GGX_COMP_ETA_N);
    return minEta + ratio * (maxEta - minEta);
#endif
}

int32_t getEtaIdx(float eta)
{
#ifndef SHN_GGX_COMPENSATION_GLSL_ETA_RANGE
    const auto halfRange = GGX_COMP_ETA_N / 2;
    if (eta < 1.f)
        return clamp<int32_t>(0.5f + eta * halfRange, 0, halfRange - 1);
    return halfRange + clamp<int32_t>(0.5f + (eta - 1) * halfRange / GGX_COMP_MAX_ETA, 0, halfRange - 1);
#else
    if (eta == 0.0)
        return 0;

    const float minEta = getIOR(0.f);
    const float maxEta = getIOR(1.f);
    const float ratio = (eta - minEta) / (maxEta - minEta);
    return clamp<int32_t>((ratio * (GGX_COMP_ETA_N - 1.f) + 1.f) + 0.5f, 1.f, GGX_COMP_ETA_N - 1.f);
#endif
}

int32_t getEtaCount()
{
    return GGX_COMP_ETA_N;
}

float computeAverageAlbedo(float * albedoTable)
{
    float avgAlbedo = 0.f;
    for (size_t cosOutIdx = 0; cosOutIdx < GGX_COMP_COS_N; ++cosOutIdx)
    {
        const float cosOut = float(cosOutIdx + 0.5f) / GGX_COMP_COS_N;

        const auto E1 = albedoTable[cosOutIdx];
        const auto E2 = albedoTable[cosOutIdx + 1];

        avgAlbedo += 0.5f * (E1 + E2) * fabsf(cosOut);
    }
    return clamp(avgAlbedo * 2.f / GGX_COMP_COS_N, 0.f, 1.f);
}

float albedo(float table[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1][GGX_COMP_COS_N + 1], float eta, float roughness, float cosTheta)
{
    const auto etaIdx = getEtaIdx(eta);

    const auto roughnessIdx = clamp<int32_t>(roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N - 1);
    const auto roughnessFrac = clamp<float>(roughness * GGX_COMP_ROUGH_N - roughnessIdx, 0.f, 1.f);
    const auto cosThetaIdx = clamp<int32_t>(cosTheta * GGX_COMP_COS_N, 0, GGX_COMP_COS_N - 1);
    const auto cosThetaFrac = clamp<float>(cosTheta * GGX_COMP_COS_N - cosThetaIdx, 0.f, 1.f);

    float albedo0 = mix(table[etaIdx][roughnessIdx][cosThetaIdx], table[etaIdx][roughnessIdx + 1][cosThetaIdx], roughnessFrac);
    float albedo1 = mix(table[etaIdx][roughnessIdx][cosThetaIdx + 1], table[etaIdx][roughnessIdx + 1][cosThetaIdx + 1], roughnessFrac);

    return mix(albedo0, albedo1, cosThetaFrac);
}

float albedoAverage(float table[GGX_COMP_ETA_N][GGX_COMP_ROUGH_N + 1], float eta, float roughness)
{
    const auto etaIdx = getEtaIdx(eta);

    const auto roughnessIdx = clamp<int32_t>(roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N - 1);
    const auto roughnessFrac = clamp<float>(roughness * GGX_COMP_ROUGH_N - roughnessIdx, 0.f, 1.f);

    return mix(table[etaIdx][roughnessIdx], table[etaIdx][roughnessIdx + 1], roughnessFrac);
}

float ggxReflectRefractAlbedo(float eta, float roughness, float cosOut)
{
    return albedo(ReflectRefractAlbedo, eta, roughness, cosOut);
}

float ggxReflectRefractAlbedoAverage(float eta, float roughness)
{
    return albedoAverage(ReflectRefractAlbedoAverage, eta, roughness);
}

float ggxReflectAlbedo(float eta, float roughness, float cosOut)
{
    return albedo(ReflectAlbedo, eta, roughness, cosOut);
}

float ggxReflectAlbedoAverage(float eta, float roughness)
{
    return albedoAverage(ReflectAlbedoAverage, eta, roughness);
}

float ggxRefractAlbedo(float eta, float roughness, float cosOut)
{
    return albedo(RefractAlbedo, eta, roughness, cosOut);
}

float ggxCompensatedReflectAlbedo(float eta, float roughness, float cosOut)
{
    return albedo(CompensatedReflectAlbedo, eta, roughness, cosOut);
}

} // namespace GGXCompensation

class MicrofacetGGXReflectionEnergyCompensationClosure: public BSDFClosure
{
public:
    V3f m_N; // shading normal
    float m_roughness;
    float m_eta; // eta_o / eta_i

    MicrofacetGGXReflectionEnergyCompensationClosure(): BSDFClosure(ScatteringTypeBits::Glossy, ScatteringEventBits::Reflect)
    {
    }

    void setup() override
    {
    }

    bool mergeable(const ClosurePrimitive * other) const override
    {
        const auto * comp = (const MicrofacetGGXReflectionEnergyCompensationClosure *) other;
        return m_N == comp->m_N && m_roughness == comp->m_roughness && m_eta == comp->m_eta && BSDFClosure::mergeable(other);
    }

    size_t memsize() const override
    {
        return sizeof(*this);
    }

    const char * name() const override
    {
        return "shn_microfacet_ggx_reflection_energy_compensation";
    }

    void print(std::ostream & out) const override
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << m_roughness << ", ";
        out << m_eta << ")";
    }

    float missingEnergy(float cosTheta) const
    {
        return 1.f - GGXCompensation::ggxReflectAlbedo(m_eta, m_roughness, cosTheta);
    }

    float directionalAlbedoAverage() const
    {
        return GGXCompensation::ggxReflectAlbedoAverage(m_eta, m_roughness);
    }

    Sample<OSL::Dual2<V3f>> sample(float u1, float u2, float & cosThetaIn) const
    {
        const auto etaIdx = GGXCompensation::getEtaIdx(m_eta);

        const auto roughnessIdx = clamp<int32_t>(m_roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N);
        const auto cosThetaInSample = sampleContinuousDistribution1D(&GGXCompensation::ReflectSamplingTable[etaIdx][roughnessIdx][0], GGX_COMP_COS_N, u1);

        cosThetaIn = cosThetaInSample.value / GGX_COMP_COS_N;
        const float sinThetaIn = cos2sin(cosThetaIn);
        const float phiIn = 2.f * FPI * u2;

        V3f Tx, Ty;
        makeOrthonormals(m_N, Tx, Ty);
        return { OSL::Dual2<V3f>(cosThetaIn * m_N + sinThetaIn * (cosf(phiIn) * Tx + sinf(phiIn) * Ty), V3f(10., 0., 0.), V3f(0., 10., 0.)), cosThetaInSample.pdf * F1_2PI };
    }

    float pdf(const V3f & omegaIn, float cosThetaIn) const
    {
        const auto etaIdx = GGXCompensation::getEtaIdx(m_eta);
        const auto roughnessIdx = clamp<int32_t>(m_roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N);
        return pdfContinuousDistribution1D(&GGXCompensation::ReflectSamplingTable[etaIdx][roughnessIdx][0], GGX_COMP_COS_N, cosThetaIn * GGX_COMP_COS_N) *
            F1_2PI; // a sinTheta factor should appear, but it is both at numerator and denominator
    }

    float albedo(const V3f & omega_out) const override
    {
        const float cosTheta = clamp(dot(omega_out, m_N), 0.f, 1.f);

        const float fresnelAvg = fresnelDielectricAverage(m_eta);
        const float albedoAvg = directionalAlbedoAverage();
        const float fresnelMultiplier = fresnelAvg * (1.f - albedoAvg) / (1.f - fresnelAvg * albedoAvg);

        return fresnelMultiplier * missingEnergy(cosTheta);
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        const float cosThetaIn = dot(omega_in, m_N);
        if (cosThetaIn <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float oneMinusE_in = missingEnergy(cosThetaIn);

        const float normalizationDivisor = FPI * (1.f - directionalAlbedoAverage());
        const float normalizationFactor = normalizationDivisor > 0.f ? 1.f / normalizationDivisor : 0.f;

        const float eval = albedo(omega_out) * oneMinusE_in * normalizationFactor * cosThetaIn;

        return BxDFDirSample::MCEstimate::fromEval(Color3(eval), pdf(omega_in, cosThetaIn));
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        float cosThetaIn = 0.f;
        const auto omegaIn = sample(u1, u2, cosThetaIn);

        const float oneMinusE_in = missingEnergy(cosThetaIn);

        const float normalizationDivisor = FPI * (1.f - directionalAlbedoAverage());
        const float normalizationFactor = normalizationDivisor > 0.f ? 1.f / normalizationDivisor : 0.f;

        const float eval = albedo(omegaOut.val()) * oneMinusE_in * normalizationFactor * cosThetaIn;

        return fromEval(omegaIn.value, Color3(eval), omegaIn.pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

namespace GGXCompensation
{
void precompute()
{
    std::atomic_uint globalEtaIdx{ 0 };

    syncParallelRun(getSystemThreadCount(), [&globalEtaIdx](size_t threadID) {
        MicrofacetGGXReflectionClosure reflect;
        MicrofacetGGXRefractionClosure refract;

        MicrofacetGGXReflectionEnergyCompensationClosure reflectCompensation;

        refract.m_etaI = 1.f;
        refract.m_absorption = 0.f;
        refract.m_absorptionDistance = 0.f;
        refract.m_absorptionColor = Color3(0.f);

        const auto N = V3f(0, 0, 1);
        reflect.m_N = N;
        refract.m_N = N;
        reflectCompensation.m_N = N;

        while (true)
        {
            auto etaIdx = globalEtaIdx++;
            if (etaIdx >= getEtaCount())
                return;

            const float eta = getEta(etaIdx);
            reflect.m_eta = eta;
            refract.m_etaO = eta;
            reflectCompensation.m_eta = eta;

            for (size_t roughnessIdx = 0; roughnessIdx < GGX_COMP_ROUGH_N + 1; ++roughnessIdx)
            {
                const float roughness = clamp(float(roughnessIdx) / GGX_COMP_ROUGH_N, 0.0001f, 1.f);
                reflect.m_roughness = roughness;
                refract.m_roughness = roughness;

                for (size_t cosOutIdx = 0; cosOutIdx < GGX_COMP_COS_N + 1; ++cosOutIdx)
                {
                    const float cosOut = clamp(float(cosOutIdx) / GGX_COMP_COS_N, 0.001f, 1.f);
                    const V3f omegaOut{ cos2sin(cosOut), 0.f, cosOut };

                    RandomState state;

                    const auto Ereflect_out = estimateDirectionalAlbedo(omegaOut, reflect, 32 * 32, state);
                    const auto Erefract_out = estimateDirectionalAlbedo(omegaOut, refract, 32 * 32, state);

                    ReflectAlbedo[etaIdx][roughnessIdx][cosOutIdx] = clamp(Ereflect_out.x, 0.f, 1.f);
                    RefractAlbedo[etaIdx][roughnessIdx][cosOutIdx] = clamp(Erefract_out.x, 0.f, 1.f);

                    ReflectRefractAlbedo[etaIdx][roughnessIdx][cosOutIdx] = clamp(ReflectAlbedo[etaIdx][roughnessIdx][cosOutIdx] + RefractAlbedo[etaIdx][roughnessIdx][cosOutIdx], 0.f, 1.f);
                }

                ReflectRefractAlbedoAverage[etaIdx][roughnessIdx] = computeAverageAlbedo(&ReflectRefractAlbedo[etaIdx][roughnessIdx][0]);
                ReflectAlbedoAverage[etaIdx][roughnessIdx] = computeAverageAlbedo(&ReflectAlbedo[etaIdx][roughnessIdx][0]);

                buildDistribution1D(
                    [&](size_t cosInIdx) {
                        const float cosIn = clamp(float(cosInIdx) / GGX_COMP_COS_N, 0.001f, 1.f);
                        return clamp(1.f - GGXCompensation::ReflectAlbedo[etaIdx][roughnessIdx][cosInIdx], 0.f, 1.f) * cosIn;
                    },
                    &ReflectSamplingTable[etaIdx][roughnessIdx][0], GGX_COMP_COS_N);

                buildDistribution1D(
                    [&](size_t cosInIdx) {
                        const float cosIn = clamp(float(cosInIdx) / GGX_COMP_COS_N, 0.001f, 1.f);
                        return clamp(1.f - GGXCompensation::ReflectRefractAlbedo[etaIdx][roughnessIdx][cosInIdx], 0.f, 1.f) * cosIn;
                    },
                    &ReflectRefractSamplingTable[etaIdx][roughnessIdx][0], GGX_COMP_COS_N);
            }

            // CompensatedReflectAlbedo[][][] must be computed after because MicrofacetGGXReflectionEnergyCompensationClosure makes use of arrays computed in the above loop
            for (size_t roughnessIdx = 0; roughnessIdx < GGX_COMP_ROUGH_N + 1; ++roughnessIdx)
            {
                const float roughness = clamp(float(roughnessIdx) / GGX_COMP_ROUGH_N, 0.0001f, 1.f);
                reflectCompensation.m_roughness = roughness;

                for (size_t cosOutIdx = 0; cosOutIdx < GGX_COMP_COS_N + 1; ++cosOutIdx)
                {
                    const float cosOut = clamp(float(cosOutIdx) / GGX_COMP_COS_N, 0.001f, 1.f);
                    const V3f omegaOut{ cos2sin(cosOut), 0.f, cosOut };

                    RandomState state;

                    const auto EreflectCompensation_out = clamp(estimateDirectionalAlbedo(omegaOut, reflectCompensation, 32 * 32, state).x, 0.f, 1.f);
                    CompensatedReflectAlbedo[etaIdx][roughnessIdx][cosOutIdx] = clamp(ReflectAlbedo[etaIdx][roughnessIdx][cosOutIdx] + EreflectCompensation_out, 0.f, 1.f);
                }
            }
        }
    });
}

class ReflectCompensationClosure: public BSDFClosure
{
public:
    V3f m_N; // shading normal
    float m_roughness;
    float m_eta; // eta_o / eta_i

    ReflectCompensationClosure(): BSDFClosure(ScatteringTypeBits::Glossy, ScatteringEventBits::Reflect)
    {
    }

    void setup() override
    {
    }

    bool mergeable(const ClosurePrimitive * other) const override
    {
        const auto * comp = (const ReflectCompensationClosure *) other;
        return m_N == comp->m_N && m_roughness == comp->m_roughness && m_eta == comp->m_eta && BSDFClosure::mergeable(other);
    }

    size_t memsize() const override
    {
        return sizeof(*this);
    }

    const char * name() const override
    {
        return "shn_ggx_glass_reflect_compensation";
    }

    void print(std::ostream & out) const override
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << m_roughness << ", ";
        out << m_eta << ")";
    }

    float albedo(const V3f & omega_out) const override
    {
        return dirAlbedo(omega_out);
    }

    float dirAlbedo(const V3f & omega_out) const
    {
        const float cosTheta = clamp(dot(omega_out, m_N), 0.f, 1.f);

        const float fresnelAvg = fresnelDielectricAverage(m_eta);
        return fresnelAvg * (1.f - ggxReflectRefractAlbedo(m_eta, m_roughness, cosTheta));
    }

    Sample<OSL::Dual2<V3f>> sample(float u1, float u2, float & cosThetaIn) const
    {
        const auto etaIdx = GGXCompensation::getEtaIdx(m_eta);

        const auto roughnessIdx = clamp<int32_t>(m_roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N);
        const auto cosThetaInSample = sampleContinuousDistribution1D(&GGXCompensation::ReflectRefractSamplingTable[etaIdx][roughnessIdx][0], GGX_COMP_COS_N, u1);

        cosThetaIn = cosThetaInSample.value / GGX_COMP_COS_N;
        const float sinThetaIn = cos2sin(cosThetaIn);
        const float phiIn = 2.f * FPI * u2;

        V3f Tx, Ty;
        makeOrthonormals(m_N, Tx, Ty);
        return { OSL::Dual2<V3f>(cosThetaIn * m_N + sinThetaIn * (cosf(phiIn) * Tx + sinf(phiIn) * Ty), V3f(10., 0., 0.), V3f(0., 10., 0.)), cosThetaInSample.pdf * F1_2PI };
    }

    float pdf(const V3f & omegaIn, float cosThetaIn) const
    {
        const auto etaIdx = GGXCompensation::getEtaIdx(m_eta);
        const auto roughnessIdx = clamp<int32_t>(m_roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N);
        return pdfContinuousDistribution1D(&GGXCompensation::ReflectRefractSamplingTable[etaIdx][roughnessIdx][0], GGX_COMP_COS_N, cosThetaIn * GGX_COMP_COS_N) *
            F1_2PI; // a sinTheta factor should appear, but it is both at numerator and denominator
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        const float cosThetaIn = dot(omega_in, m_N);

        if (cosThetaIn <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float oneMinusE_in = 1.f - ggxReflectRefractAlbedo(m_eta, m_roughness, cosThetaIn);

        const float normalizationDivisor = FPI * (1.f - ggxReflectRefractAlbedoAverage(m_eta, m_roughness));
        const float normalizationFactor = normalizationDivisor > 0.f ? 1.f / normalizationDivisor : 0.f;

        const float eval = dirAlbedo(omega_out) * oneMinusE_in * normalizationFactor * cosThetaIn;

        return BxDFDirSample::MCEstimate::fromEval(Color3(eval), pdf(omega_in, cosThetaIn));
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        float cosThetaIn = 0.f;
        const auto dirSample = sample(u1, u2, cosThetaIn);

        const float oneMinusE_in = 1.f - ggxReflectRefractAlbedo(m_eta, m_roughness, cosThetaIn);

        const float normalizationDivisor = FPI * (1.f - ggxReflectRefractAlbedoAverage(m_eta, m_roughness));
        const float normalizationFactor = normalizationDivisor > 0.f ? 1.f / normalizationDivisor : 0.f;

        const float eval = dirAlbedo(omegaOut.val()) * oneMinusE_in * normalizationFactor * cosThetaIn;

        return fromEval(dirSample.value, Color3(eval), dirSample.pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

class RefractCompensationClosure: public BSDFClosure
{
public:
    V3f m_N;
    float m_roughness;
    float m_eta;

    RefractCompensationClosure(): BSDFClosure(ScatteringTypeBits::Glossy, ScatteringEventBits::Transmit)
    {
    }

    void setup() override
    {
    }

    bool mergeable(const ClosurePrimitive * other) const override
    {
        const auto * comp = (const RefractCompensationClosure *) other;
        return m_N == comp->m_N && m_roughness == comp->m_roughness && m_eta == comp->m_eta && BSDFClosure::mergeable(other);
    }

    size_t memsize() const override
    {
        return sizeof(*this);
    }

    const char * name() const override
    {
        return "shn_ggx_glass_refract_compensation";
    }

    void print(std::ostream & out) const override
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << m_roughness << ", ";
        out << m_eta << ")";
    }

    float albedo(const V3f & omega_out) const override
    {
        return dirAlbedo(omega_out);
    }

    float dirAlbedo(const V3f & omega_out) const
    {
        const float cosTheta = clamp(dot(omega_out, m_N), 0.f, 1.f);
        const float fresnelAvg = fresnelDielectricAverage(m_eta);
        return (1.f - fresnelAvg) * (1.f - ggxReflectRefractAlbedo(m_eta, m_roughness, cosTheta));
    }

    Sample<OSL::Dual2<V3f>> sample(float u1, float u2, float & cosThetaIn) const
    {
        const auto etaIdx = GGXCompensation::getEtaIdx(1.f / m_eta);

        const auto roughnessIdx = clamp<int32_t>(m_roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N);
        const auto cosThetaInSample = sampleContinuousDistribution1D(&GGXCompensation::ReflectRefractSamplingTable[etaIdx][roughnessIdx][0], GGX_COMP_COS_N, u1);

        cosThetaIn = cosThetaInSample.value / GGX_COMP_COS_N;
        const float sinThetaIn = cos2sin(cosThetaIn);
        const float phiIn = 2.f * FPI * u2;

        V3f Tx, Ty;
        makeOrthonormals(m_N, Tx, Ty);
        return { OSL::Dual2<V3f>(-cosThetaIn * m_N + sinThetaIn * (cosf(phiIn) * Tx + sinf(phiIn) * Ty), V3f(10., 0., 0.), V3f(0., 10., 0.)), cosThetaInSample.pdf * F1_2PI };
    }

    float pdf(const V3f & omegaIn, float cosThetaIn) const
    {
        const auto etaIdx = GGXCompensation::getEtaIdx(1.f / m_eta);
        const auto roughnessIdx = clamp<int32_t>(m_roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N);
        return pdfContinuousDistribution1D(&GGXCompensation::ReflectRefractSamplingTable[etaIdx][roughnessIdx][0], GGX_COMP_COS_N, abs(cosThetaIn) * GGX_COMP_COS_N) *
            F1_2PI; // a sinTheta factor should appear, but it is both at numerator and denominator
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        const float cosThetaIn = dot(omega_in, m_N);

        if (cosThetaIn >= 0.f)
            return BxDFDirSample::MCEstimate();

        const float oneMinusE_in = 1.f - ggxReflectRefractAlbedo(1.f / m_eta, m_roughness, abs(cosThetaIn));

        const float normalizationDivisor = FPI * (1.f - ggxReflectRefractAlbedoAverage(1.f / m_eta, m_roughness));
        const float normalizationFactor = normalizationDivisor > 0.f ? 1.f / normalizationDivisor : 0.f;

        const float eval = dirAlbedo(omega_out) * oneMinusE_in * normalizationFactor * abs(cosThetaIn);

        return BxDFDirSample::MCEstimate::fromEval(Color3(eval), pdf(omega_in, cosThetaIn));
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        float cosThetaIn = 0.f;
        const auto dirSample = sample(u1, u2, cosThetaIn);

        const float oneMinusE_in = 1.f - ggxReflectRefractAlbedo(1.f / m_eta, m_roughness, abs(cosThetaIn));

        const float normalizationDivisor = FPI * (1.f - ggxReflectRefractAlbedoAverage(1.f / m_eta, m_roughness));
        const float normalizationFactor = normalizationDivisor > 0.f ? 1.f / normalizationDivisor : 0.f;

        const float eval = dirAlbedo(omegaOut.val()) * oneMinusE_in * normalizationFactor * abs(cosThetaIn);

        return fromEval(dirSample.value, Color3(eval), dirSample.pdf, ScatteringType::Glossy, ScatteringEvent::Transmit);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

} // namespace GGXCompensation

class MicrofacetGGXReflectionAnisotropyClosure: public BSDFClosure
{
public:
    V3f m_N; // shading normal
    V3f m_U; // shading tangent
    float m_roughness;
    float m_eta; // eta_o / eta_i
    float m_ax; // anisotropy factor alpha x
    float m_ay; // anisotropy factor alpha y

    MicrofacetGGXReflectionAnisotropyClosure(): BSDFClosure(ScatteringTypeBits::Glossy, ScatteringEventBits::Reflect)
    {
    }

    void setup()
    {
    }

    bool mergeable(const ClosurePrimitive * other) const
    {
        const MicrofacetGGXReflectionAnisotropyClosure * comp = (const MicrofacetGGXReflectionAnisotropyClosure *) other;
        return m_N == comp->m_N && m_roughness == comp->m_roughness && m_eta == comp->m_eta && m_ax == comp->m_ax && m_ay == comp->m_ay && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "shn_microfacet_ggx_reflection_anisotropy";
    }

    void print(std::ostream & out) const
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << "(" << m_U[0] << ", " << m_U[1] << ", " << m_U[2] << "), ";
        out << m_roughness << ", ";
        out << m_eta << ", ";
        out << m_ax << ", ";
        out << m_ay << ")";
    }

    float albedo(const V3f & omega_out) const
    {
        return fresnelDielectric(-m_N.dot(omega_out), m_eta);
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To match the paper convention and be more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        TangentFrame tf(m_N, m_U);
        const V3f I = tf.tolocal(omega_out);
        const V3f O = tf.tolocal(omega_in);
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = I.z;
        const float OdotN = O.z;
        const float alpha2 = m_roughness * m_roughness;

        // light leaking early exit
        if (IdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        // build Hr half vector for reflection - here use Hr as M
        const V3f Hr = evalReflectionHalfVector(I, O);
        const float HdotN = Hr.z;
        if (HdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float IdotH = dot(I, Hr);
        if (IdotH <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float OdotH = dot(O, Hr);

        // compute micronormals distribution D_hr
        const float D_hr = evalGGXAniso_D(Hr, HdotN, m_ax, m_ay);

        // compute shadowing-masking term G_hr
        const float G_hr = evalGGXAniso_G1(I, IdotH, IdotN, m_ax, m_ay) * evalGGXAniso_G1(O, OdotH, OdotN, m_ax, m_ay);

        // compute fresnel F_hr
        const float F_hr = fresnelDielectric(IdotH, m_eta);

        // compute the reflection BRDF Fr
        const float Fr = evalMicrofacetBRDF(D_hr, G_hr, F_hr, IdotN, OdotN);

        // compute the Jacobian of the half direction transform
        const float Jcb_hr = evalReflectionJacobian(OdotH);

        // compute eval
        const float eval = microfacetEval(Fr, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_hr, Jcb_hr, HdotN);
        if (pdf <= 0.f)
            return BxDFDirSample::MCEstimate();

        return BxDFDirSample::MCEstimate::fromEval(Color3(eval), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To be closer to the paper and more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        TangentFrame tf(m_N, m_U);
        const V3f I = tf.tolocal(omegaOut.val());
        const V3f dIdx = tf.tolocal(omegaOut.dx());
        const V3f dIdy = tf.tolocal(omegaOut.dy());
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = I.z;

        // light leaking early exit
        if (IdotN <= 0.f)
            return BxDFDirSample();

        // sample M
        const V3f M = sampleGGXAnisoMicronormal(m_ax, m_ay, u1, u2);
        const float IdotM = dot(I, M);
        // light leaking early exit
        if (IdotM <= 0.f)
            return BxDFDirSample();

        const float MdotN = M.z;
        assert(MdotN >= 0.f);

        // compute O
        const V3f O = evalReflectionDirection(I, M, IdotM);
        V3f dOdx, dOdy;
        evalReflectionDerivatives(dIdx, dIdy, M, m_roughness, dOdx, dOdy);
        const float OdotN = O.z;
        const float OdotM = dot(O, M);

        // compute micronormals distribution D_m
        const float D_m = evalGGXAniso_D(M, MdotN, m_ax, m_ay);

        // compute shadowing-masking term G_m
        const float G_m = evalGGXAniso_G1(I, IdotM, IdotN, m_ax, m_ay) * evalGGXAniso_G1(O, OdotM, OdotN, m_ax, m_ay);

        // compute fresnel F_m
        const float F_m = fresnelDielectric(IdotM, m_eta);

        // compute the reflection BRDF Fr
        const float Fr = evalMicrofacetBRDF(D_m, G_m, F_m, IdotN, OdotN);

        // compute the Jacobian of the half direction transform
        const float Jcb_m = evalReflectionJacobian(OdotM);

        // compute eval
        const float eval = microfacetEval(Fr, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_m, Jcb_m, MdotN);
        if (pdf <= 0.f)
            return BxDFDirSample();

        OSL::Dual2<V3f> omegaIn(tf.toworld(O), tf.toworld(dOdx), tf.toworld(dOdy));

        return fromEval(omegaIn, Color3(eval), pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_microfacet_ggx_reflection_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXReflectionClosure, m_N), CLOSURE_FLOAT_PARAM(MicrofacetGGXReflectionClosure, m_roughness),
                                                         CLOSURE_FLOAT_PARAM(MicrofacetGGXReflectionClosure, m_eta), CLOSURE_FINISH_PARAM(MicrofacetGGXReflectionClosure) };

ClosureParam bsdf_microfacet_ggx_reflection_energy_compensation_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXReflectionEnergyCompensationClosure, m_N),
                                                                             CLOSURE_FLOAT_PARAM(MicrofacetGGXReflectionEnergyCompensationClosure, m_roughness),
                                                                             CLOSURE_FLOAT_PARAM(MicrofacetGGXReflectionEnergyCompensationClosure, m_eta),
                                                                             CLOSURE_FINISH_PARAM(MicrofacetGGXReflectionEnergyCompensationClosure) };

ClosureParam bsdf_ggx_glass_reflect_compensation_params[] = { CLOSURE_VECTOR_PARAM(GGXCompensation::ReflectCompensationClosure, m_N),
                                                              CLOSURE_FLOAT_PARAM(GGXCompensation::ReflectCompensationClosure, m_roughness),
                                                              CLOSURE_FLOAT_PARAM(GGXCompensation::ReflectCompensationClosure, m_eta),
                                                              CLOSURE_FINISH_PARAM(GGXCompensation::ReflectCompensationClosure) };

ClosureParam bsdf_ggx_glass_refract_compensation_params[] = { CLOSURE_VECTOR_PARAM(GGXCompensation::RefractCompensationClosure, m_N),
                                                              CLOSURE_FLOAT_PARAM(GGXCompensation::RefractCompensationClosure, m_roughness),
                                                              CLOSURE_FLOAT_PARAM(GGXCompensation::RefractCompensationClosure, m_eta),
                                                              CLOSURE_FINISH_PARAM(GGXCompensation::RefractCompensationClosure) };

ClosureParam bsdf_microfacet_ggx_reflection_anisotropy_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXReflectionAnisotropyClosure, m_N),
                                                                    CLOSURE_VECTOR_PARAM(MicrofacetGGXReflectionAnisotropyClosure, m_U),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXReflectionAnisotropyClosure, m_roughness),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXReflectionAnisotropyClosure, m_eta),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXReflectionAnisotropyClosure, m_ax),
                                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXReflectionAnisotropyClosure, m_ay),
                                                                    CLOSURE_FINISH_PARAM(MicrofacetGGXReflectionAnisotropyClosure) };

CLOSURE_PREPARE(bsdf_microfacet_ggx_reflection_prepare, MicrofacetGGXReflectionClosure)
CLOSURE_PREPARE(bsdf_microfacet_ggx_reflection_energy_compensation_prepare, MicrofacetGGXReflectionEnergyCompensationClosure)
CLOSURE_PREPARE(bsdf_ggx_glass_reflect_compensation_prepare, GGXCompensation::ReflectCompensationClosure)
CLOSURE_PREPARE(bsdf_ggx_glass_refract_compensation_prepare, GGXCompensation::RefractCompensationClosure)
CLOSURE_PREPARE(bsdf_microfacet_ggx_reflection_anisotropy_prepare, MicrofacetGGXReflectionAnisotropyClosure)

class MicrofacetGGXMetalClosure: public TplBSDFClosure<MicrofacetGGXMetalClosure, ScatteringTypeBits::Glossy, ScatteringEventBits::Reflect>
{
public:
    V3f m_N; // shading normal
    float m_roughness;

    const char * name() const override
    {
        return "shn_ggx_metal";
    }

    void print(std::ostream & out) const override
    {
        out << name() << "(" << m_N << ", " << m_roughness << ")";
    }

    float missingEnergy(float cosTheta) const
    {
        return 1.f - GGXCompensation::ggxReflectAlbedo(0.f, m_roughness, cosTheta);
    }

    float albedo(const V3f & omega_out) const override
    {
        return dot(omega_out, m_N) > 0.f ? 1.f : 0.f;
    }

    BxDFDirSample::MCEstimate evalGGXReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const
    {
        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To match the paper convention and be more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        TangentFrame tf(m_N);
        const V3f I = tf.tolocal(omega_out);
        const V3f O = tf.tolocal(omega_in);
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = dot(I, N);
        const float OdotN = dot(O, N);
        const float alpha2 = m_roughness * m_roughness;

        // shading normal not visible from I (omega_out)
        if (IdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        // build Hr half vector for reflection - here use Hr as M
        const V3f Hr = evalReflectionHalfVector(I, O);
        const float HdotN = dot(Hr, N);
        if (HdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float OdotH = dot(O, Hr);
        const float IdotH = dot(I, Hr);

        // compute micronormals distribution D_hr
        const float D_hr = evalGGX_D(Hr, HdotN, alpha2);

        // compute shadowing-masking term G_hr
        const float G_hr = evalGGX_G1(I, IdotH, IdotN, alpha2) * evalGGX_G1(O, OdotH, OdotN, alpha2);

        // compute the reflection BRDF Fr
        const float Fr = evalMicrofacetBRDF(D_hr, G_hr, 1.f, IdotN, OdotN);

        // compute the Jacobian of the half direction transform
        const float Jcb_hr = evalReflectionJacobian(OdotH);

        // compute eval
        const float eval = microfacetEval(Fr, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_hr, Jcb_hr, HdotN);
        if (pdf <= 0.f)
            return BxDFDirSample::MCEstimate();

        return BxDFDirSample::MCEstimate::fromEval(Color3(eval), pdf);
    }

    BxDFDirSample::MCEstimate evalCompensationReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in, float missingEnergyOut) const
    {
        const float cosThetaIn = dot(omega_in, m_N);
        if (cosThetaIn <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float oneMinusE_in = missingEnergy(cosThetaIn);

        const float normalizationDivisor = FPI * (1.f - directionalAlbedoAverage());
        const float normalizationFactor = normalizationDivisor > 0.f ? 1.f / normalizationDivisor : 0.f;

        const float eval = missingEnergyOut * oneMinusE_in * normalizationFactor * cosThetaIn;

        const auto roughnessIdx = clamp<int32_t>(m_roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N);
        const auto pdf = pdfContinuousDistribution1D(&GGXCompensation::ReflectSamplingTable[0][roughnessIdx][0], GGX_COMP_COS_N, cosThetaIn * GGX_COMP_COS_N) *
            F1_2PI; // a sinTheta factor should appear, but it is both at numerator and denominator

        return BxDFDirSample::MCEstimate::fromEval(Color3(eval), pdf);
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        BxDFDirSample::MCEstimate::ValueType eval{ 0.f };
        float pdf{ 0.f };

        const auto ggxCompPdf = missingEnergy(clamp(dot(omega_out, m_N), 0.f, 1.f));
        if (ggxCompPdf > 0.f)
        {
            const auto ggxComp = evalCompensationReflect(sg, omega_out, omega_in, ggxCompPdf);
            eval += ggxComp.eval();
            pdf += ggxCompPdf * ggxComp.pdf();
        }

        if (ggxCompPdf < 1.f)
        {
            const auto ggx = evalGGXReflect(sg, omega_out, omega_in);
            eval += ggx.eval();
            pdf += (1.f - ggxCompPdf) * ggx.pdf();
        }

        return BxDFDirSample::MCEstimate::fromEval(eval, pdf);
    }

    BxDFDirSample sampleGGX(const OSL::ShaderGlobals & sg, const V3fD2 & omegaOut, V2f uDir) const
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To be closer to the paper and more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        TangentFrame tf(m_N);
        const V3f I = tf.tolocal(omegaOut.val());
        const V3f dIdx = tf.tolocal(omegaOut.dx());
        const V3f dIdy = tf.tolocal(omegaOut.dy());
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = dot(I, N);
        const float alpha2 = m_roughness * m_roughness;

        // light leaking early exit
        if (IdotN <= 0.f)
            return BxDFDirSample();

        // sample M
        const V3f M = sampleGGXMicronormal(m_roughness, u1, u2);
        const float IdotM = dot(I, M);
        const float MdotN = dot(M, N);

        // light leaking early exit
        if (IdotM <= 0.f)
            return BxDFDirSample();

        // compute O
        const V3f O = evalReflectionDirection(I, M, IdotM);
        V3f dOdx, dOdy;
        evalReflectionDerivatives(dIdx, dIdy, M, m_roughness, dOdx, dOdy);
        const float OdotN = dot(O, N);
        const float OdotM = dot(O, M);

        // compute micronormals distribution D_m
        const float D_m = evalGGX_D(M, MdotN, alpha2);

        // compute shadowing-masking term G_m
        const float G_m = evalGGX_G1(I, IdotM, IdotN, alpha2) * evalGGX_G1(O, OdotM, OdotN, alpha2);

        // compute the reflection BRDF Fr
        const float Fr = evalMicrofacetBRDF(D_m, G_m, 1.f, IdotN, OdotN);

        // compute the Jacobian of the half direction transform
        const float Jcb_m = evalReflectionJacobian(OdotM);

        // compute eval
        const float eval = microfacetEval(Fr, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_m, Jcb_m, MdotN);
        if (pdf <= 0.f)
            return BxDFDirSample();

        OSL::Dual2<V3f> omegaIn(tf.toworld(O), tf.toworld(dOdx), tf.toworld(dOdy));

        return fromEval(omegaIn, Color3(eval), pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
    }

    float directionalAlbedoAverage() const
    {
        return GGXCompensation::ggxReflectAlbedoAverage(0.f, m_roughness);
    }

    float pdf(const V3f & omegaIn, float cosThetaIn) const
    {
    }

    BxDFDirSample sampleCompensation(const OSL::ShaderGlobals & sg, const V3fD2 & omegaOut, V2f uDir, float missingEnergyOut) const
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        const auto roughnessIdx = clamp<int32_t>(m_roughness * GGX_COMP_ROUGH_N, 0, GGX_COMP_ROUGH_N);
        const auto cosThetaInSample = sampleContinuousDistribution1D(&GGXCompensation::ReflectSamplingTable[0][roughnessIdx][0], GGX_COMP_COS_N, u1);

        const auto cosThetaIn = cosThetaInSample.value / GGX_COMP_COS_N;
        const float sinThetaIn = cos2sin(cosThetaIn);
        const float phiIn = 2.f * FPI * u2;

        V3f Tx, Ty;
        makeOrthonormals(m_N, Tx, Ty);
        const auto omegaIn = Sample<OSL::Dual2<V3f>>{ OSL::Dual2<V3f>(cosThetaIn * m_N + sinThetaIn * (cosf(phiIn) * Tx + sinf(phiIn) * Ty), V3f(10., 0., 0.), V3f(0., 10., 0.)),
                                                      cosThetaInSample.pdf * F1_2PI };

        const float oneMinusE_in = missingEnergy(cosThetaIn);

        const float normalizationDivisor = FPI * (1.f - directionalAlbedoAverage());
        const float normalizationFactor = normalizationDivisor > 0.f ? 1.f / normalizationDivisor : 0.f;

        const float eval = missingEnergyOut * oneMinusE_in * normalizationFactor * cosThetaIn;

        return fromEval(omegaIn.value, Color3(eval), omegaIn.pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const V3fD2 & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto ggxCompPdf = missingEnergy(clamp(dot(omegaOut.val(), m_N), 0.f, 1.f));

        if (uComponent < ggxCompPdf)
        {
            auto result = sampleCompensation(sg, omegaOut, uDir, ggxCompPdf);

            if (ggxCompPdf < 1.f)
            {
                result.estimate.scalePDF(ggxCompPdf);

                auto missing = evalGGXReflect(sg, omegaOut.val(), result.direction.val());
                missing.scalePDF(1.f - ggxCompPdf);

                result.estimate += missing;
            }

            return result;
        }

        auto result = sampleGGX(sg, omegaOut, uDir);

        if (ggxCompPdf > 0.f)
        {
            result.estimate.scalePDF(1.f - ggxCompPdf);

            auto missing = evalCompensationReflect(sg, omegaOut.val(), result.direction.val(), ggxCompPdf);
            missing.scalePDF(ggxCompPdf);

            result.estimate += missing;
        }

        return result;
    }
};

ClosureParam bsdf_microfacet_ggx_metal_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXMetalClosure, m_N), CLOSURE_FLOAT_PARAM(MicrofacetGGXMetalClosure, m_roughness),
                                                    CLOSURE_FINISH_PARAM(MicrofacetGGXMetalClosure) };

CLOSURE_PREPARE(bsdf_microfacet_ggx_metal_prepare, MicrofacetGGXMetalClosure)

class MicrofacetGGXGlassClosure: public TplBSDFClosure<MicrofacetGGXGlassClosure, ScatteringTypeBits::Glossy, ScatteringEventBits::Both>
{
public:
    V3f m_N; // shading normal
    float m_roughness;
    float m_etaI;
    float m_etaO;
    float m_absorption;
    float m_absorptionDistance;
    V3f m_absorptionColor;

    const char * name() const override
    {
        return "shn_microfacet_ggx_refraction";
    }

    void print(std::ostream & out) const override
    {
        out << name() << "(" << m_N << ", " << m_roughness << ", " << m_etaI << ", " << m_etaO << ", " << m_absorption << ", " << m_absorptionDistance << ", " << m_absorptionColor << ")";
    }

    float albedo(const V3f & omegaOut) const override
    {
        if (m_etaO == m_etaI || m_etaI == 0.f)
            return 0.f;

        const float eta = m_etaO / m_etaI;
        const float cosO = clamp(dot(omegaOut, m_N), 0.f, 1.f);

        return GGXCompensation::ggxRefractAlbedo(eta, m_roughness, cosO) + GGXCompensation::ggxReflectAlbedo(eta, m_roughness, cosO);
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        if (0.f == m_etaI)
            return BxDFDirSample::MCEstimate();

        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To match the paper convention and be more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        TangentFrame tf(m_N);
        const V3f I = tf.tolocal(omega_out);
        const V3f O = tf.tolocal(omega_in);
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = dot(I, N);
        const float OdotN = dot(O, N);
        const float alpha2 = m_roughness * m_roughness;

        // light leaking early exit
        if (IdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        // build Hr half vector for reflection - here use Hr as M
        const V3f Hr = evalReflectionHalfVector(I, O);
        const float HdotN = dot(Hr, N);
        if (HdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float IdotH = dot(I, Hr);
        if (IdotH <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float OdotH = dot(O, Hr);

        // compute micronormals distribution D_hr
        const float D_hr = evalGGX_D(Hr, HdotN, alpha2);

        // compute shadowing-masking term G_hr
        const float G_hr = evalGGX_G1(I, IdotH, IdotN, alpha2) * evalGGX_G1(O, OdotH, OdotN, alpha2);

        // compute fresnel F_hr
        const float F_hr = fresnelDielectric(IdotH, m_etaO / m_etaI);

        // compute the reflection BRDF Fr
        const float Fr = evalMicrofacetBRDF(D_hr, G_hr, F_hr, IdotN, OdotN);

        // compute the Jacobian of the half direction transform
        const float Jcb_hr = evalReflectionJacobian(OdotH);

        // compute eval
        const float eval = microfacetEval(Fr, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_hr, Jcb_hr, HdotN);
        if (pdf <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float reflectProbability = /*F_hr;//*/ GGXCompensation::ggxReflectAlbedo(m_etaO / m_etaI, m_roughness, IdotN);

        return BxDFDirSample::MCEstimate::fromEval(Color3(eval), reflectProbability * pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        if (m_etaO == m_etaI)
            return BxDFDirSample::MCEstimate();

        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To match the paper convention and be more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        // In case etaI > etaO, we revert N to have the normal point to the medium with the lower index of refraction
        // We do only that in evalTransmit(), because in sample(), the formula to compute O (see evalRefractionDirection()) revert N for us
        TangentFrame tf(m_N);
        const V3f I = tf.tolocal(omega_out);
        const V3f O = tf.tolocal(omega_in);
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = I.z;
        if (IdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float OdotN = O.z;
        const float alpha2 = m_roughness * m_roughness;
        const float IdotO = dot(I, O);

        // build Ht half vector for refraction - here use Ht as M
        const V3f Ht = evalRefractionHalfVector(I, O, m_etaI, m_etaO);
        const float HdotN = Ht.z;
        if (HdotN <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float IdotH = dot(I, Ht);
        if (IdotH <= 0.f)
            return BxDFDirSample::MCEstimate();

        const float OdotH = dot(O, Ht);

        if (OdotH >= 0.f) // I and O lies on the same side of the microfacet, so they cannot be refraction of one another (see paper section 4.2 "If we excluse the cases where ...")
            return BxDFDirSample::MCEstimate();

        // compute micronormals distribution D_ht
        const float D_ht = evalGGX_D(Ht, HdotN, alpha2);

        // compute shadowing-masking term G_hr
        const float G_ht = evalGGX_G1(I, IdotH, IdotN, alpha2) * evalGGX_G1(O, OdotH, OdotN, alpha2);

        // compute fresnel F_hr
        const float F_ht = fresnelDielectric(IdotH, m_etaO / m_etaI);

        // compute the reflection BTDF Ft
        const float Ft = evalMicrofacetBTDF(D_ht, G_ht, F_ht, IdotN, IdotH, OdotN, OdotH, m_etaI, m_etaO);

        // compute the Jacobian of the half direction transform
        const float Jcb_ht = evalRefractionJacobian(IdotH, OdotH, m_etaI, m_etaO);

        // compute eval
        const float eval = microfacetEval(Ft, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_ht, Jcb_ht, HdotN);
        if (pdf <= 0.f)
            return BxDFDirSample::MCEstimate();

        Color3 evalColor(eval);

        // compute absorption
        auto renderState = sg.renderstate;
        if (eval > 0.f && m_absorption > 0.f && renderState)
        {
            const shn::RenderState * state = reinterpret_cast<const shn::RenderState *>(renderState);
            evalColor *= rayTraceAbsorption(state->scene, sg.P, omega_in, sg.time, m_absorption, m_absorptionDistance, m_absorptionColor);
        }

        const float reflectProbability = /*F_ht;//*/ GGXCompensation::ggxReflectAlbedo(m_etaO / m_etaI, m_roughness, IdotN);

        return BxDFDirSample::MCEstimate::fromEval(evalColor, (1.f - reflectProbability) * pdf);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        // Fake transparency in case etaO == etaI. In that case, the media are the same and there is no visible deformations (eg. air => air)
        if (m_etaO == m_etaI)
            return fromEval(-omegaOut, Color3(1.f), 1.f, ScatteringType::Glossy, ScatteringEvent::Transmit);

        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        // build tangent frame
        // we will do all the computation in tangent space to simplify thetas computing
        // To match the paper convention and be more readable, the input and output vector are invert in tangent space (omegaOut -> I and omegaIn -> O)
        TangentFrame tf(m_N);
        const V3f I = tf.tolocal(omegaOut.val());
        const V3f dIdx = tf.tolocal(omegaOut.dx());
        const V3f dIdy = tf.tolocal(omegaOut.dy());
        const V3f N(0.f, 0.f, 1.f);
        const float IdotN = I.z;
        const float alpha2 = m_roughness * m_roughness;

        // Macro surface not visible from I (omegaOut)
        if (IdotN <= 0.f)
            return BxDFDirSample();

        // sample M
        const V3f M = sampleGGXMicronormal(m_roughness, u1, u2);
        const float IdotM = dot(M, I);
        if (IdotM <= 0.f) // Lets ignore all backfacing micro-normals
            return BxDFDirSample();

        const float MdotN = M.z;
        assert(MdotN >= 0.f);

        // compute micronormals distribution D_m
        const float D_m = evalGGX_D(M, MdotN, alpha2);

        const float eta = m_etaO / m_etaI;

        // compute fresnel F_m
        const float F_m = fresnelDielectric(IdotM, eta);

        const float reflectProbability = /*F_m; //*/ GGXCompensation::ggxReflectAlbedo(m_etaO / m_etaI, m_roughness, IdotN);

        if (uComponent <= reflectProbability)
        {
            // Sample reflection

            // compute O
            const V3f O = evalReflectionDirection(I, M, IdotM);
            V3f dOdx, dOdy;
            evalReflectionDerivatives(dIdx, dIdy, M, m_roughness, dOdx, dOdy);
            const float OdotN = dot(O, N);
            const float OdotM = dot(O, M);

            // compute shadowing-masking term G_m
            const float G_m = evalGGX_G1(I, IdotM, IdotN, alpha2) * evalGGX_G1(O, OdotM, OdotN, alpha2);

            // compute the reflection BRDF Fr
            const float Fr = evalMicrofacetBRDF(D_m, G_m, F_m, IdotN, OdotN); // Replace F_m by 1.f in order to cancel F_m multiplier, which is the probability of chosing reflection

            // compute the Jacobian of the half direction transform
            const float Jcb_m = evalReflectionJacobian(OdotM);

            // compute eval
            const float eval = microfacetEval(Fr, OdotN);

            // compute pdf
            const float pdf = microfacetPDF(D_m, Jcb_m, MdotN);
            if (pdf <= 0.f)
                return BxDFDirSample();

            OSL::Dual2<V3f> omegaIn(tf.toworld(O), tf.toworld(dOdx), tf.toworld(dOdy));
            auto result = fromEval(omegaIn, Color3(eval), reflectProbability * pdf, ScatteringType::Glossy, ScatteringEvent::Reflect);

            auto transmitEstimate = evalTransmit(sg, omegaOut.val(), omegaIn.val());
            result.estimate += transmitEstimate;

            return result;
        }

        // Sample refraction

        // compute O
        float etaRatio = m_etaI / m_etaO;
        const V3f O = evalRefractionDirection(I, M, IdotM, etaRatio);
        V3f dOdx, dOdy;
        evalRefractionDerivatives(dIdx, dIdy, M, IdotM, etaRatio, m_roughness, dOdx, dOdy);
        const float OdotN = dot(O, N);
        const float OdotM = dot(O, M);

        // compute shadowing-masking term G_m
        const float G_m = evalGGX_G1(I, IdotM, IdotN, alpha2) * evalGGX_G1(O, OdotM, OdotN, alpha2);

        // compute the reflection BTDF Ft
        const float Ft = evalMicrofacetBTDF(
            D_m, G_m, F_m, IdotN, IdotM, OdotN, OdotM, m_etaI, m_etaO); // Replace F_m by 1.f in order to cancel (1.0 - F_m) multiplier, which is the probability of chosing refraction

        // compute the Jacobian of the half direction transform
        const float Jcb_m = evalRefractionJacobian(IdotM, OdotM, m_etaI, m_etaO);

        // compute eval
        const float eval = microfacetEval(Ft, OdotN);

        // compute pdf
        const float pdf = microfacetPDF(D_m, Jcb_m, MdotN);
        if (pdf <= 0.f)
            return BxDFDirSample();

        V3f evalColor(eval);
        OSL::Dual2<V3f> omegaIn(tf.toworld(O), tf.toworld(dOdx), tf.toworld(dOdy));

        // compute absorption
        auto renderState = sg.renderstate;
        if (eval > 0.f && m_absorption > 0.f && renderState)
        {
            const shn::RenderState * state = reinterpret_cast<const shn::RenderState *>(renderState);
            evalColor *= rayTraceAbsorption(state->scene, sg.P, omegaIn.val(), sg.time, m_absorption, m_absorptionDistance, m_absorptionColor);
        }

        auto result = fromEval(omegaIn, evalColor, (1.f - reflectProbability) * pdf, ScatteringType::Glossy, ScatteringEvent::Transmit);

        auto reflectEstimate = evalReflect(sg, omegaOut.val(), omegaIn.val());
        result.estimate += reflectEstimate;

        return result;
    }
};

ClosureParam bsdf_microfacet_ggx_glass_params[] = { CLOSURE_VECTOR_PARAM(MicrofacetGGXGlassClosure, m_N),
                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXGlassClosure, m_roughness),
                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXGlassClosure, m_etaI),
                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXGlassClosure, m_etaO),
                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXGlassClosure, m_absorption),
                                                    CLOSURE_FLOAT_PARAM(MicrofacetGGXGlassClosure, m_absorptionDistance),
                                                    CLOSURE_COLOR_PARAM(MicrofacetGGXGlassClosure, m_absorptionColor),
                                                    CLOSURE_FINISH_PARAM(MicrofacetGGXGlassClosure) };

CLOSURE_PREPARE(bsdf_microfacet_ggx_glass_prepare, MicrofacetGGXGlassClosure)

} // namespace shn