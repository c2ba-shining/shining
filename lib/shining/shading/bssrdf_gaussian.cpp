#include "CustomClosure.h"
#include "IMathUtils.h"
#include "OslHeaders.h"
#include "Shining_p.h"
#include "sampling/ShapeSamplers.h"

namespace shn
{

namespace
{
static const Color3 samplingWeights{ 1.f / 3 }; // Uniform sampling among R,G,B scatter distances
}

// Legacy closure; mainly used as reference and documentation. DisneyBSSRDFClosure gives better results (cause Disney rocks)
class GaussianBSSRDFClosure: public BSSRDFClosure
{
public:
    Color3 m_scatterDistance;

    GaussianBSSRDFClosure()
    {
    }

    bool mergeable(const ClosurePrimitive * other) const
    {
        const GaussianBSSRDFClosure * comp = (const GaussianBSSRDFClosure *) other;
        return m_scatterDistance == comp->m_scatterDistance && BSSRDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "shn_gaussian_bssrdf";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_scatterDistance[0] << ", " << m_scatterDistance[1] << ", " << m_scatterDistance[2] << "), (" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "))";
    }

    // #note: on our test, this one does not seem to be normalized
    Color3 evalDiffusion(float r) const override
    {
        const shn::V3f r2 = shn::V3f(r * r);
        const shn::V3f v2 = 2.f * m_scatterDistance * m_scatterDistance;
        return bssrdfFilterValues(shn::exp(-r2 / v2) / (shn::FPI * v2), m_scatterDistance);
    }

    float pdfRadius(float radius, const float * componentProbabilities) const
    {
        float pdf = 0.f;
        for (int32_t i = 0; i < 3; ++i)
        {
            if (componentProbabilities[i])
            {
                const float rmax = m_scatterDistance[i];
                const float rmax2 = shn::sqr(rmax);
                const float v = shn::sqr(shn::max(m_scatterDistance));
                const float v2 = 2.f * v;
                const float r2 = radius * radius;

                pdf += componentProbabilities[i] *
                    (expf(-r2 / v2) * radius / (v * (1.f - expf(-rmax2 / v2)))); // #note: pdf with respect to length on R. The formula from the original paper BSSRDF Importance Sampling [King
                                                                                 // et. al] gives a pdf with respect to area, which can be obtain by dividing by (2pi * radius)
            }
        }
        return pdf;
    }

    // Ref: BSSRDF Importance Sampling [King et. al]
    virtual float sampleRadius(float uStrategy, float uRadius, float & pdf, float * uRemap) const override
    {
        float componentProbabilities[3], componentCDF[3];
        bssrdfComputeSamplingDistribs(componentProbabilities, componentCDF, samplingWeights, m_scatterDistance);

        const auto componentIdx = bssrdfSampleScatterDistanceComponent(uStrategy, m_scatterDistance, componentProbabilities, componentCDF, uStrategy);

        const float rmax = m_scatterDistance[componentIdx];
        const float rmax2 = shn::sqr(rmax);
        const float v = shn::sqr(shn::max(m_scatterDistance));
        const float v2 = 2.f * v;
        const float radius = sqrtf(-v2 * logf(1.f - uRadius * (1.f - expf(-rmax2 / v2))));

        pdf = pdfRadius(radius, componentProbabilities);

        if (uRemap)
            *uRemap = uStrategy;

        return radius;
    }

    virtual float pdfRadius(float radius) const override
    {
        if (radius <= 0.f)
            return 0.f;

        float componentProbabilities[3];
        bssrdfComputeSamplingDistribs(componentProbabilities, nullptr, samplingWeights, m_scatterDistance);

        return pdfRadius(radius, componentProbabilities);
    }

    float maxRadius() const override
    {
        return shn::max(m_scatterDistance) * 8.f; // #todo: overbounded
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bssrdf_gaussian_params[] = { CLOSURE_VECTOR_PARAM(GaussianBSSRDFClosure, m_N), CLOSURE_COLOR_PARAM(GaussianBSSRDFClosure, m_scatterDistance),
                                          CLOSURE_FINISH_PARAM(GaussianBSSRDFClosure) };

CLOSURE_PREPARE(bssrdf_gaussian_prepare, GaussianBSSRDFClosure)

} // namespace shn