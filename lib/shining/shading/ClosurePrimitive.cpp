#include "ClosurePrimitive.h"
#include "CustomClosure.h"

namespace shn
{

using namespace OSL;
using namespace OSL::pvt;

ClosureParam toon_outline_data_params[] = { CLOSURE_INT_PARAM(ToonOutlineDataClosure, applyLighting),

                                            CLOSURE_INT_PARAM(ToonOutlineDataClosure, outerOutline),
                                            CLOSURE_FLOAT_PARAM(ToonOutlineDataClosure, outerThickness),
                                            CLOSURE_FLOAT_PARAM(ToonOutlineDataClosure, outerDistanceThreshold),
                                            CLOSURE_FLOAT_PARAM(ToonOutlineDataClosure, outerIncidentAngleThreshold),
                                            CLOSURE_COLOR_PARAM(ToonOutlineDataClosure, outerOutlineColor),

                                            CLOSURE_INT_PARAM(ToonOutlineDataClosure, innerOutline),
                                            CLOSURE_FLOAT_PARAM(ToonOutlineDataClosure, innerThickness),
                                            CLOSURE_FLOAT_PARAM(ToonOutlineDataClosure, innerDistanceThreshold),
                                            CLOSURE_FLOAT_PARAM(ToonOutlineDataClosure, innerIncidentAngleThreshold),
                                            CLOSURE_COLOR_PARAM(ToonOutlineDataClosure, innerOutlineColor),

                                            CLOSURE_INT_PARAM(ToonOutlineDataClosure, cornerOutline),
                                            CLOSURE_FLOAT_PARAM(ToonOutlineDataClosure, cornerThickness),
                                            CLOSURE_FLOAT_PARAM(ToonOutlineDataClosure, cornerAngleThreshold),
                                            CLOSURE_COLOR_PARAM(ToonOutlineDataClosure, cornerOutlineColor),

                                            CLOSURE_FINISH_PARAM(ToonOutlineDataClosure) };

CLOSURE_PREPARE(closure_toon_outline_data_prepare, ToonOutlineDataClosure);

} // namespace shn