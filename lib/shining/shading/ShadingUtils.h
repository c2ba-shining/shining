#pragma once

#include "IMathUtils.h"

namespace shn
{

/// Helper struct to build a TangentFrame from N
/// transforms vector from world to local space
struct TangentFrame
{
    // build frame from unit normal
    TangentFrame(const V3f & n): w(n)
    {
        u = (fabsf(w.x) > .01f ? V3f(w.z, 0, -w.x) : V3f(0, -w.z, w.y)).normalize();
        v = w.cross(u);
    }

    // build frame from unit normal and unit tangent
    TangentFrame(const V3f & n, const V3f & t): w(n)
    {
        v = w.cross(t);
        u = v.cross(w);
    }

    // transform vector
    V3f get(float x, float y, float z) const
    {
        return x * u + y * v + z * w;
    }

    // untransform vector
    float getx(const V3f & a) const
    {
        return a.dot(u);
    }
    float gety(const V3f & a) const
    {
        return a.dot(v);
    }
    float getz(const V3f & a) const
    {
        return a.dot(w);
    }

    V3f tolocal(const V3f & a) const
    {
        return V3f(a.dot(u), a.dot(v), a.dot(w));
    }
    V3f toworld(const V3f & a) const
    {
        return get(a.x, a.y, a.z);
    }

private:
    V3f u, v, w;
};

// Microfacet helper functions
// Equations come from the paper Microfacet Models for Refraction through Rough Surfaces (https://www.cs.cornell.edu/~srm/publications/EGSR07-btdf.pdf)

/// Eq. 35 & 36
/// Sample micronormal with GGX distribution
V3f sampleGGXMicronormal(const float alpha, const float u1, const float u2);

/// Sample micronormal with GGX distribution with anisotropic BSDF
/// see paper https://hal.archives-ouvertes.fr/hal-00996995v1/file/supplemental2.pdf, section 3.3
V3f sampleGGXAnisoMicronormal(const float alpha_x, const float alpha_y, const float u1, const float u2);

/// Eq. 33
/// Compute the GGX micronormal distribution term D
float evalGGX_D(const V3f & M, const float MdotN, const float alpha2);

/// Compute the GGX micronormal distribution term D with anisotropic BSDF
/// see paper https://hal.inria.fr/hal-00942452v1/document, section 3.3
float evalGGXAniso_D(const V3f & M, const float MdotN, const float alpha_x, const float alpha_y);

/// Eq. 34
/// Compute the GGX monodirectional shadowing-masking term G1
float evalGGX_G1(const V3f & V, const float VdotM, const float VdotN, const float alpha2);

/// Compute the GGX monodirectional shadowing-masking term G1 with anisotropic BSDF
/// see paper https://hal.inria.fr/hal-00942452v1/document, section 3.3
float evalGGXAniso_G1(const V3f & V, const float VdotM, const float VdotN, const float alpha_x, const float alpha_y);

/// Eq. 20
/// Compute the microfacet BRDF
float evalMicrofacetBRDF(const float D, const float G, const float F, const float IdotN, const float OdotN);

/// Eq. 21
/// Compute the microfacet BTDF
float evalMicrofacetBTDF(const float D, const float G, const float F, const float IdotN, const float IdotM, const float OdotN, const float OdotM, const float etaI, const float etaO);

/// Eq. 39
/// Compute output vector O from input I and normal N in case of reflection
V3f evalReflectionDirection(const V3f & I, const V3f & M, const float IdotM);

/// Compute the derivatives of O, the reflected direction from I and N in tangent space
/// from paper : https://graphics.stanford.edu/papers/trd/trd.pdf "Tracing Ray Differentials", Homan Igehy
void evalReflectionDerivatives(const V3f & dIdx, const V3f & dIdy, const V3f & M, const float roughness, V3f & dOdx, V3f & dOdy);

/// Eq.40
/// Compute output vectorO from input I, macronormal N and micronormal N in case of refraction
V3f evalRefractionDirection(const V3f & I, const V3f & M, const float IdotM, const float etaRatio);

/// Compute the derivatives of O, the refracted direction from I and M in tangent space
/// from paper : https://graphics.stanford.edu/papers/trd/trd.pdf
/// eta = etaI / etaO;
void evalRefractionDerivatives(const V3f & dIdx, const V3f & dIdy, const V3f & M, const float IdotM, const float eta, const float roughness, V3f & dOdx, V3f & dOdy);

/// Eq. 13
/// Compute half vector for reflection
V3f evalReflectionHalfVector(const V3f & I, const V3f & O);

/// Eq. 16
/// Compute half vector for refraction
V3f evalRefractionHalfVector(const V3f & I, const V3f & O, const float etaI, const float etaO, float * pHtLength = nullptr);

/// Eq. 14
/// Compute the half direction Jacobian term for reflection
float evalReflectionJacobian(const float OdotM);

/// Eq. 17
/// Compute the half direction Jacobian term for refraction
float evalRefractionJacobian(const float IdotM, const float OdotM, const float etaI, const float etaO);

/// Adapt from Eq. 37. Eval is the numerator of the weight equation
/// Compute eval of the microfacet
float microfacetEval(const float BSDFEval, const float OdotN);

/// Eq. 38
/// Compute pdf of the microfacet
float microfacetPDF(const float D, const float jacobian, const float MdotN);

/// Helper function to compute fresnel reflectance R of a dielectric. This
/// formulation does not explicitly compute the refracted vector so should
/// only be used for reflective materials. cosi is the angle between the
/// incomming ray and the surface normal, eta gives the index of refraction
/// of the surface.
float fresnelDielectric(float cosi, float eta);

/// Helper function to compute fresnel reflectance R of a dielectric.
float fresnelRefractionDielectric(const float cosi, const float cost, const float eta);

/// Helper function to compute fresnel reflectance R of a conductor. These
/// materials do not transmit any light. cosi is the angle between the
/// incomming ray and the surface normal, eta and k give the complex index
/// of refraction of the surface.
float fresnelConductor(float cosi, float eta, float k);

inline float schlickFresnel(float u)
{
    const float m = clamp(1.f - u, 0.f, 1.f);
    const float m2 = m * m;
    return m2 * m2 * m; // pow(m,5)
}

float disneyDiffuseFresnel(float NdotOmegaIn, float NdotOmegaOut, float HdotOmegaIn, float roughness);

float disneyDiffuseFresnel(const V3f & N, const V3f & omegaOut, const V3f & omegaIn, float roughness);

class SceneImpl;
/// Trace a ray to compute absorption after a refraction
Color3 rayTraceAbsorption(const SceneImpl * scene, const V3f & P, const V3f & dir, const float time, const float absorption, const float absorptionDistance, const Color3 & absorptionColor);

// BSSRDF helper functions

// Must be used to filter inf and NaN components in values produced by a zero scatter distance in the corresponding component
inline Color3 bssrdfFilterValues(const Color3 & values, const Color3 & scatterDistance)
{
    return Color3(scatterDistance.x > 0.f ? values.x : 0.f, scatterDistance.y > 0.f ? values.y : 0.f, scatterDistance.z > 0.f ? values.z : 0.f);
}

// Compute sampling probabilities and CDF for bssrdf scatter distance sampling. All scatter distance which are zero are assigned a zero probability.
void bssrdfComputeSamplingDistribs(float componentProbabilities[3], float * componentCDF, const Color3 & samplingWeights, const Color3 & scatterDistance);

// Choose a scatter distance according to precomputed probabilities
int32_t bssrdfSampleScatterDistanceComponent(float uComponent, const Color3 & scatterDistance, float componentProbabilities[3], float componentCDF[3], float & uRemap);

} // namespace shn