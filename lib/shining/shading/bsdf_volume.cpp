#include "CustomClosure.h"
#include "IMathUtils.h"
#include "OslHeaders.h"

namespace shn
{

// Volume closure for volumetric ray marching
class VolumeClosure: public VBSDFClosure
{
public:
    // anisotropy parameter
    float m_g;

    VolumeClosure(): VBSDFClosure()
    {
    }

    void setup()
    {
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "volume";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ()";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    Color3 eval_phase(const V3f & omega_in, const V3f & omega_out) const
    {
        // Henyey-Greenstein phase function
        float cosTheta = shn::dot(-omega_out, omega_in);
        float denom = 1.f + m_g * m_g + 2.f * m_g * cosTheta;
        return Color3(0.25f * (float) M_1_PI) * (1 - m_g * m_g) / (denom * sqrtf(denom));
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_volume_params[] = { CLOSURE_COLOR_PARAM(VolumeClosure, m_sigma_s), CLOSURE_COLOR_PARAM(VolumeClosure, m_sigma_t), CLOSURE_FLOAT_PARAM(VolumeClosure, m_g),
                                      CLOSURE_FINISH_PARAM(VolumeClosure) };

CLOSURE_PREPARE(bsdf_volume_prepare, VolumeClosure)

} // namespace shn