#define _USE_MATH_DEFINES

#include "CompositeBxDF.h"
#include "DebugUtils.h"
#include "LoggingUtils.h"
#include "sampling/ShapeSamplers.h"
#include <algorithm>
#include <assert.h>
#include <limits>
#include <numeric>

namespace o = OSL;

namespace shn
{

static uint8_t removeClosuresIndexWithNullProbability(uint8_t indices[], float pdfArray[], size_t count)
{
    float * pFirst = std::find_if(pdfArray, pdfArray + count, [&](float pdf) { return pdf == 0.f; });
    if (pFirst == pdfArray + count)
        return count; // All pdfs are > 0

    size_t firstNullIdx = pFirst - pdfArray;
    for (size_t i = firstNullIdx + 1; i < count; ++i)
    {
        if (pdfArray[i] != 0.f)
        {
            std::swap(pdfArray[i], pdfArray[firstNullIdx]);
            std::swap(indices[i], indices[firstNullIdx]);
            ++firstNullIdx;
        }
    }
    return firstNullIdx;
}

static bool isCut(const RayDepth & rayDepth, const BSDFClosure & bsdf)
{
    return (bsdf.scatteringTypes() == ScatteringTypeBits::Diffuse && rayDepth.d[RayDepth::DIFFUSE] < 1) ||
        (bsdf.scatteringTypes() == ScatteringTypeBits::Glossy && rayDepth.d[RayDepth::GLOSSY] == 0) ||
        (bsdf.scatteringTypes() == ScatteringTypeBits::Specular && bsdf.scatteringEvents() == ScatteringEventBits::Reflect && rayDepth.d[RayDepth::REFLECTION] == 0) ||
        (bsdf.scatteringTypes() == ScatteringTypeBits::Specular && bsdf.scatteringEvents() == ScatteringEventBits::Transmit && rayDepth.d[RayDepth::REFRACTION] == 0) ||
        (bsdf.scatteringTypes() == ScatteringTypeBits::Straight && rayDepth.d[RayDepth::TRANSPARENCY] == 0);
}

void SurfaceShadingFunction::Sampler::init(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, const Color3 & pathWeight, const RayDepth * rayDepth)
{
    std::iota(m_bsdfIndices, m_bsdfIndices + m_bsdfCount, 0);
    std::iota(m_bssrdfIndices, m_bssrdfIndices + m_bssrdfCount, 0);

    // Special case for volumes without surface
    if (bxdf.componentCount() == 0)
    {
        if (!rayDepth || rayDepth->d[RayDepth::VOLUME] >= 1)
            m_transparencySamplingProbability = 1.f;
        return;
    }

    float bsdfSamplingWeightsSum = 0.f;
    // Compute BSDF distributions
    for (auto i = 0; i < bxdf.bsdfCount(); i++)
    {
        if (rayDepth && isCut(*rayDepth, *bxdf.m_bsdfs[i]))
        {
            m_bsdfPdfs[i] = 0.f;
        }
        else
        {
            m_bsdfPdfs[i] = luminance(bxdf.m_bsdfWeights[i] * pathWeight) * bxdf.m_bsdfs[i]->albedo(-sg.I);
            assert(m_bsdfPdfs[i] >= 0.f);
            m_bsdfPdfs[i] = max(0.f, m_bsdfPdfs[i]);
            bsdfSamplingWeightsSum += m_bsdfPdfs[i];
        }
    }
    m_bsdfCount = removeClosuresIndexWithNullProbability(m_bsdfIndices, m_bsdfPdfs, m_bsdfCount);

    float bssrdfSamplingWeightsSum = 0.f;
    // Compute BSSRDFs distributions
    if (!rayDepth || rayDepth->d[RayDepth::DIFFUSE] >= 1)
    {
        for (auto i = 0; i < m_bssrdfCount; i++)
        {
            m_bssrdfPdfs[i] = luminance(bxdf.m_bssrdfWeights[i] * pathWeight); // #todo investigate pathWeight < 0
            assert(m_bssrdfPdfs[i] >= 0.f);
            m_bssrdfPdfs[i] = max(0.f, m_bssrdfPdfs[i]);
            bssrdfSamplingWeightsSum += m_bssrdfPdfs[i];
        }
        m_bssrdfCount = removeClosuresIndexWithNullProbability(m_bssrdfIndices, m_bssrdfPdfs, m_bssrdfCount);
    }
    else
    {
        std::fill(m_bssrdfPdfs, m_bssrdfPdfs + m_bssrdfCount, 0.f);
        m_bssrdfCount = 0;
    }

    // Normalize distributions
    {
        if (bsdfSamplingWeightsSum > 0)
        {
            for (int i = 0; i < m_bsdfCount; i++)
                m_bsdfPdfs[i] /= bsdfSamplingWeightsSum;
        }

        if (bssrdfSamplingWeightsSum > 0)
        {
            for (int i = 0; i < m_bssrdfCount; i++)
                m_bssrdfPdfs[i] /= bssrdfSamplingWeightsSum;
        }
    }

    // Normalize global probabilities
    const float sum = bsdfSamplingWeightsSum + bssrdfSamplingWeightsSum;
    if (sum > 0.f)
    {
        m_bsdfSamplingProbability = bsdfSamplingWeightsSum / sum;
        m_bssrdfSamplingProbability = bssrdfSamplingWeightsSum / sum;
    }

    // Filter channels according to pathWeight
    const float transparencySamplingWeight = min(luminance(bxdf.transparency() * pathWeight), 1.f);
    const float opacitySamplingWeight = min(luminance(bxdf.opacity() * pathWeight), 1.f);

    m_transparencySamplingProbability = transparencySamplingWeight / (transparencySamplingWeight + opacitySamplingWeight);

    const auto opacitySamplingProbability = 1.f - m_transparencySamplingProbability;
    m_bsdfSamplingProbability *= opacitySamplingProbability;
    m_bssrdfSamplingProbability *= opacitySamplingProbability;
}

void compare(const SurfaceShadingFunction::Sampler & s1, const SurfaceShadingFunction::Sampler & s2)
{
    SHN_LOGINFO << "cmp";
    SHN_LOGINFO << int(s1.m_bsdfCount) << ", " << int(s2.m_bsdfCount);
    SHN_LOGINFO << int(s1.m_bssrdfCount) << ", " << int(s2.m_bssrdfCount);
    SHN_LOGINFO << s1.m_transparencySamplingProbability << ", " << s2.m_transparencySamplingProbability;
    SHN_LOGINFO << s1.m_bsdfSamplingProbability << ", " << s2.m_bsdfSamplingProbability;
    SHN_LOGINFO << s1.m_bssrdfSamplingProbability << ", " << s2.m_bssrdfSamplingProbability;

    for (size_t i = 0; i < s1.m_bsdfCount; ++i)
    {
        SHN_LOGINFO << "i = " << i << " " << int(s1.m_bsdfIndices[i]) << ", " << int(s2.m_bsdfIndices[i]);
        SHN_LOGINFO << "i = " << i << " " << s1.m_bsdfPdfs[i] << ", " << s2.m_bsdfPdfs[i];
    }
}

BxDFDirSample::MCEstimate SurfaceShadingFunction::Sampler::evalBSDF(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, const V3f & omegaIn, EmissionBits emitComponent) const
{
    BxDFDirSample::MCEstimate estimate = BxDFDirSample::MCEstimate::zero();
    for (int i = 0; i < m_bsdfCount; i++)
    {
        if (m_bsdfPdfs[i] <= 0.f)
            continue;

        const auto j = m_bsdfIndices[i];

        if (EmissionBits::All != emitComponent)
        {
            // Emit in diffuse check
            if (none(emitComponent & EmissionBits::Diffuse) && bxdf.m_bsdfs[j]->scatteringTypes() == ScatteringTypeBits::Diffuse)
                continue;
            // Emit in glossy check
            if (none(emitComponent & EmissionBits::Glossy) && bxdf.m_bsdfs[j]->scatteringTypes() == ScatteringTypeBits::Glossy)
                continue;
        }
        estimate += scaleEstimate(bxdf.m_bsdfWeights[j], m_bsdfPdfs[i], bxdf.m_bsdfs[j]->eval(sg, omegaIn));
    }

    return estimate;
}

BSDFContribs SurfaceShadingFunction::Sampler::evalBSDFContribs(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, const V3f & omegaIn, EmissionBits emitComponent) const
{
    BSDFContribs contribs;

    for (ComponentCountType i = 0; i < m_bsdfCount; i++)
    {
        if (m_bsdfPdfs[i] <= 0.f)
            continue;

        const auto j = m_bsdfIndices[i];

        if (bxdf.m_bsdfs[j]->scatteringTypes() == ScatteringTypeBits::Diffuse)
        {
            if (any(emitComponent & EmissionBits::Diffuse))
            {
                const BxDFDirSample::MCEstimate estimate = scaleEstimate(bxdf.m_bsdfWeights[j], m_bsdfPdfs[i], bxdf.m_bsdfs[j]->eval(sg, omegaIn));
                if (bxdf.m_bsdfs[j]->scatteringEvents() == ScatteringEventBits::Transmit)
                    contribs[BSDFContribs::DiffuseTransmit] += estimate;
                else
                    contribs[BSDFContribs::DiffuseReflect] += estimate;
            }
        }
        else if (bxdf.m_bsdfs[j]->scatteringTypes() == ScatteringTypeBits::Glossy)
        {
            if (any(emitComponent & EmissionBits::Glossy))
            {
                const BxDFDirSample::MCEstimate estimate = scaleEstimate(bxdf.m_bsdfWeights[j], m_bsdfPdfs[i], bxdf.m_bsdfs[j]->eval(sg, omegaIn));
                if (bxdf.m_bsdfs[j]->scatteringEvents() == ScatteringEventBits::Transmit)
                    contribs[BSDFContribs::GlossyTransmit] += estimate;
                else
                    contribs[BSDFContribs::GlossyReflect] += estimate;
            }
        }
    }

    return contribs;
}

const BSDFClosure * SurfaceShadingFunction::Sampler::sampleBSDF(const SurfaceShadingFunction & bxdf, float uBSDFComponent, Color3 & weight, float & pdf, float * uRemap) const
{
    float accum = 0;
    for (int i = 0; i < m_bsdfCount; i++)
    {
        const auto j = m_bsdfIndices[i];

        if (uBSDFComponent < m_bsdfPdfs[i] + accum)
        {
            weight = bxdf.m_bsdfWeights[j];
            pdf = m_bsdfPdfs[i];

            if (uRemap)
            {
                *uRemap = (uBSDFComponent - accum) / m_bsdfPdfs[i];
            }

            return bxdf.m_bsdfs[j];
        }
        accum += m_bsdfPdfs[i];
    }

    return nullptr;
}

const BSSRDFClosure * SurfaceShadingFunction::Sampler::sampleBSSRDF(const SurfaceShadingFunction & bxdf, float uBSSRDFComponent, float & pdf, float * uRemap) const
{
    float cdf = 0;
    for (int32_t i = 0; i < m_bssrdfCount; ++i)
    {
        const auto j = m_bssrdfIndices[i];

        if (uBSSRDFComponent < (m_bssrdfPdfs[i] + cdf))
        {
            if (uRemap)
                *uRemap = (uBSSRDFComponent - cdf) / m_bssrdfPdfs[i];
            pdf = m_bssrdfPdfs[i];
            return bxdf.m_bssrdfs[j];
        }

        cdf += m_bssrdfPdfs[i];
    }
    return nullptr;
}

BxDFDirSample SurfaceShadingFunction::Sampler::sampleBSDFDirection(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, float bsdfSample, float rx, float ry) const
{
    Color3 bsdfWeight;
    float bsdfPdf = 0.f;
    const BSDFClosure * pBSDF = sampleBSDF(bxdf, bsdfSample, bsdfWeight, bsdfPdf, &bsdfSample);

    if (!pBSDF)
        return BxDFDirSample();

    BxDFDirSample dirSample = pBSDF->sample(sg, bsdfSample, V2f(rx, ry));
    if (dirSample.pdf() <= 0.f)
        return BxDFDirSample();

    BxDFDirSample::MCEstimate estimate = scaleEstimate(bsdfWeight, bsdfPdf, dirSample.estimate);

    const bool notSingular = (dirSample.scatteringType != ScatteringType::Specular && dirSample.scatteringType != ScatteringType::Straight);
    if (notSingular)
    {
        // we sampled PDF i, now figure out how much the other bsdfs contribute to the chosen direction
        for (int i = 0; i < m_bsdfCount; i++)
        {
            const auto j = m_bsdfIndices[i];

            if (pBSDF == bxdf.m_bsdfs[j] || m_bsdfPdfs[i] <= 0.f)
                continue;
            estimate += scaleEstimate(
                bxdf.m_bsdfWeights[j], m_bsdfPdfs[i], bxdf.m_bsdfs[j]->eval(sg, dirSample.direction.val())); // If side is specified, we might miss the computation of some pdf and ruin MIS
        }
    }

    return BxDFDirSample(dirSample.direction, estimate, dirSample.scatteringDirection, dirSample.scatteringType, ClosurePrimitive::BSDF);
}

BxDFDirSample sampleCosineBSSRDFLobe(const V3f & normal, float rx, float ry)
{
    float directionPdf;
    const auto wi = OSL::Dual2<V3f>(sampleCosineHemisphere(normal, rx, ry, directionPdf), V3f(10., 0., 0.), V3f(0., 10., 0.));

    if (directionPdf == 0.f)
        return BxDFDirSample();

    return BxDFDirSample(wi, BxDFDirSample::MCEstimate::fromWeight(Color3(1.f), directionPdf), ScatteringEvent::Reflect, ScatteringType::Diffuse, ClosurePrimitive::BSSRDF);
}

BxDFDirSample::MCEstimate evalCosineBSSRDFLobe(const V3f & normal, const V3f & omegaIn)
{
    if (dot(normal, omegaIn) <= 0.f)
        return BxDFDirSample::MCEstimate();

    return BxDFDirSample::MCEstimate::fromWeight(Color3(1.f), pdfCosineHemisphere(normal, omegaIn));
}

shn::BxDFDirSample SurfaceShadingFunction::Sampler::sampleTransparency(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg) const
{
    return BxDFDirSample(
        OSL::Dual2<V3f>(sg.I, sg.dIdx, sg.dIdy), BxDFDirSample::MCEstimate::fromEval(bxdf.transparency(), 1.f), ScatteringEvent::Transmit, ScatteringType::Straight, ClosurePrimitive::BSDF);
}

BxDFDirSample SurfaceShadingFunction::Sampler::sampleDirection(
    const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sgO, const OSL::ShaderGlobals & sgI, float uComponent, float u1, float u2) const
{
    float cdf = m_transparencySamplingProbability;
    if (uComponent < cdf)
    {
        return BxDFDirSample(
            OSL::Dual2<V3f>(sgO.I, sgO.dIdx, sgO.dIdy), BxDFDirSample::MCEstimate::fromEval(bxdf.transparency(), m_transparencySamplingProbability), ScatteringEvent::Transmit,
            ScatteringType::Straight, ClosurePrimitive::BSDF);
    }

    const auto bsdfProb = m_bsdfSamplingProbability;
    const auto bssrdfProb = m_bssrdfSamplingProbability;

    const auto offset = m_transparencySamplingProbability;
    cdf += bsdfProb;

    // const auto bsdfProb = m_bsdfSamplingProbability / (m_bsdfSamplingProbability + m_bssrdfSamplingProbability);
    // const auto bssrdfProb = m_bssrdfSamplingProbability / (m_bsdfSamplingProbability + m_bssrdfSamplingProbability);

    // auto cdf = bsdfProb;

    // Choose either a BSDF or a BSSRDF, depending on probabilities
    if (uComponent < cdf)
    {
        const BxDFDirSample sample = sampleBSDFDirection(bxdf, sgO, (uComponent - offset) / bsdfProb, u1, u2);
        return BxDFDirSample::scalePDF(bsdfProb, sample);
    }

    cdf += bssrdfProb;
    if (uComponent < cdf)
    {
        const BxDFDirSample sample = sampleCosineBSSRDFLobe(sgI.N, u1, u2);
        return BxDFDirSample::scalePDF(bssrdfProb, sample);
    }

    // Absorption
    return BxDFDirSample();
}

BxDFDirSample SurfaceShadingFunction::Sampler::sampleOpaqueDirection(
    const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sgO, const OSL::ShaderGlobals & sgI, float uComponent, float u1, float u2) const
{
    const auto totalProb = m_bsdfSamplingProbability + m_bssrdfSamplingProbability;
    if (totalProb == 0.f)
        return BxDFDirSample();

    const auto bsdfProb = m_bsdfSamplingProbability / totalProb;
    const auto bssrdfProb = m_bssrdfSamplingProbability / totalProb;

    if (uComponent < bsdfProb)
    {
        const BxDFDirSample sample = sampleBSDFDirection(bxdf, sgO, uComponent / bsdfProb, u1, u2);
        return BxDFDirSample::scalePDF(bsdfProb, sample);
    }

    const BxDFDirSample sample = sampleCosineBSSRDFLobe(sgI.N, u1, u2);
    return BxDFDirSample::scalePDF(bssrdfProb, sample);
}

V3f SurfaceShadingFunction::Sampler::sampleShadingNormal(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, float uComponent) const
{
    if (m_bsdfSamplingProbability == 0.f && m_bssrdfSamplingProbability == 0.f)
    {
        return sg.N;
    }

    const float probabilityScale = 1.f / (m_bsdfSamplingProbability + m_bssrdfSamplingProbability); // Rescale probabilities in order to take only surface closures into account
    const float bsdfProbability = probabilityScale * m_bsdfSamplingProbability;

    // Choose either a BSDF or a BSSRDF, depending on probabilities
    if (uComponent < bsdfProbability)
    {
        Color3 weight;
        float pdf;

        const BSDFClosure * pBSDF = sampleBSDF(bxdf, uComponent / bsdfProbability, weight, pdf);
        if (pBSDF && pBSDF->scatteringTypes() != ScatteringTypeBits::Straight)
            return pBSDF->normal();

        return sg.N;
    }

    if (m_bssrdfSamplingProbability > 0.f)
    {
        const float bssrdfProbability = probabilityScale * m_bssrdfSamplingProbability;
        float pdf;
        const BSSRDFClosure * pBSSRDF = sampleBSSRDF(bxdf, (uComponent - bsdfProbability) / bssrdfProbability, pdf);
        if (pBSSRDF)
            return pBSSRDF->normal();
    }

    return sg.N;
}

float SurfaceShadingFunction::Sampler::sampleRadiusBSSRDF(const SurfaceShadingFunction & bxdf, float uStrategy, float uRadius, float & pdf, float * uRemap) const
{
    pdf = 0.f;
    if (m_bssrdfCount == 0 || m_bssrdfSamplingProbability == 0.f)
        return 0.f;

    float bssrdfPdf = 0.f;
    const BSSRDFClosure * pBSSRDF = sampleBSSRDF(bxdf, uStrategy, bssrdfPdf, &uStrategy);
    if (!pBSSRDF)
        return 0.f;

    const float sampledRadius = pBSSRDF->sampleRadius(uStrategy, uRadius, pdf, uRemap);
    pdf *= bssrdfPdf;

    for (int i = 0; i < m_bssrdfCount; ++i)
    {
        const auto j = m_bssrdfIndices[i];
        if (pBSSRDF != bxdf.m_bssrdfs[j])
            pdf += m_bssrdfPdfs[i] * bxdf.m_bssrdfs[j]->pdfRadius(sampledRadius);
    }

    return sampledRadius;
}

float SurfaceShadingFunction::Sampler::pdfRadiusBSSRDF(const SurfaceShadingFunction & bxdf, float radius) const
{
    if (m_bssrdfCount == 0 || m_bssrdfSamplingProbability == 0.f)
        return 0.f;

    const float maxRadius = bxdf.maxRadiusBSSRDF();
    float pdf = 0.f;
    for (int32_t i = 0; i < m_bssrdfCount; ++i)
    {
        const auto j = m_bssrdfIndices[i];
        pdf += m_bssrdfPdfs[i] * bxdf.m_bssrdfs[j]->pdfRadius(radius);
    }

    return pdf;
}

float SurfaceShadingFunction::maxRadiusBSSRDF() const
{
    float maxRadius = 0.f;
    for (uint8_t i = 0; i < m_bssrdfCount; ++i)
    {
        maxRadius = shn::max(m_bssrdfs[i]->maxRadius(), maxRadius);
    }

    return maxRadius;
}

Color3 SurfaceShadingFunction::evalBSSRDFDiffusion(float r) const
{
    if (m_bssrdfCount == 0)
        return Color3(0.f);

    Color3 eval(0.f);
    for (uint8_t i = 0; i < m_bssrdfCount; ++i)
        eval += m_bssrdfWeights[i] * m_bssrdfs[i]->evalDiffusion(r);
    return eval;
}

Color3 SurfaceShadingFunction::evalBSSRDFFresnelDiffusion(const OSL::ShaderGlobals & sgO, float r) const
{
    if (m_bssrdfCount == 0)
        return Color3(0.f);

    Color3 eval(0.f);
    for (uint8_t i = 0; i < m_bssrdfCount; ++i)
        eval += m_bssrdfWeights[i] * m_bssrdfs[i]->evalFresnel(sgO, -sgO.I) * m_bssrdfs[i]->evalDiffusion(r);
    return eval;
}

Color3 SurfaceShadingFunction::color(ScatteringTypeBits scatteringTypes, ScatteringEventBits events) const
{
    Color3 result(0.f);

    if (any(scatteringTypes & ScatteringTypeBits::Straight))
    {
        result += Color3(m_transparency);
    }

    for (ComponentCountType i = 0; i < m_bsdfCount; ++i)
    {
        if (any(m_bsdfs[i]->scatteringTypes() & scatteringTypes) && any(m_bsdfs[i]->scatteringEvents() & events))
            result += m_bsdfWeights[i];
    }

    if (any(scatteringTypes & ScatteringTypeBits::Diffuse) && any(events & ScatteringEventBits::Reflect))
    {
        for (int i = 0; i < m_bssrdfCount; ++i)
            result += m_bssrdfWeights[i];
    }

    return result;
}

SurfaceShadingFunction::SurfaceShadingFunction(
    const Color3 * bsdfWeights, ClosurePrimitive * const * bsdfComponents, ComponentCountType bsdfCount, const Color3 * bssrdfWeights, ClosurePrimitive * const * bssrdfComponents,
    ComponentCountType bssrdfCount, Color3 transparency, Color3 emission, Color3 baseColor, Color3 scatterDistanceColor):
    m_bsdfCount(bsdfCount),
    m_bsdfWeights(bsdfWeights), m_bsdfs((const BSDFClosure **) bsdfComponents), m_bssrdfCount(bssrdfCount), m_bssrdfWeights(bssrdfWeights),
    m_bssrdfs((const BSSRDFClosure **) bssrdfComponents), m_transparency(transparency), m_emission(emission), m_baseColor(baseColor), m_scatterDistanceColor(scatterDistanceColor)
{
    for (size_t i = 0; i < bsdfCount; ++i)
    {
        m_allBSDFSingular = m_allBSDFSingular && (m_bsdfs[i]->scatteringTypes() == ScatteringTypeBits::Specular || m_bsdfs[i]->scatteringTypes() == ScatteringTypeBits::Straight);
        m_allBSDFStraight = m_allBSDFStraight && m_bsdfs[i]->scatteringTypes() == ScatteringTypeBits::Straight;
    }
}

std::ostream & operator<<(std::ostream & out, const SurfaceShadingFunction & bsdf)
{
    out << "{\n";
    out << "\t"
        << "bsdfCount = " << bsdf.m_bsdfCount << ",\n";
    out << "\t"
        << "bssrdfCount = " << bsdf.m_bssrdfCount << ",\n";
    out << "\t"
        << "allBSDFSingular = " << bsdf.m_allBSDFSingular << ",\n";
    out << "\t"
        << "allBSDFStraight = " << bsdf.m_allBSDFStraight << ",\n";
    out << "\t"
        << "BSDFs {\n";
    for (int32_t i = 0; i < bsdf.m_bsdfCount; ++i)
    {
        const char * name = bsdf.m_bsdfs[i]->name();
        const auto scatteringLabel = scatteringTypeString(bsdf.m_bsdfs[i]->scatteringTypes());

        out << "\t\t";
        out << "[ ";
        bsdf.m_bsdfs[i]->print(out);
        out << ", ";
        out << "scatteringLabel: " << scatteringLabel << ", ";
        out << "weight: " << bsdf.m_bsdfWeights[i] << ", ";
        // out << "pdf: " << bsdf.m_bsdfPdfs[i];
        out << " ]";
        out << "\n";
    }
    out << "\t"
        << "}\n";
    out << "}\n";

    return out;
}

Color3 VolumeShadingFunction::evalPhase(const OSL::ShaderGlobals & sg, const V3f & wi, EmissionBits emitComponent) const
{
    Color3 eval(0.f);
    for (int32_t i = 0; i < m_vbsdfCount; ++i)
        eval += m_vbsdfWeights[i] * m_vbsdfs[i]->eval_phase(-sg.I, wi) * m_vbsdfs[i]->sigma_s();
    return eval;
}

Color3 VolumeShadingFunction::extinction() const
{

    Color3 extinction(0.f);

    for (int i = 0; i < m_vbsdfCount; ++i)
    {
        extinction += m_vbsdfs[i]->sigma_t();
    }

    return extinction;
}

VolumeShadingFunction::VolumeShadingFunction(const Color3 * weights, ClosurePrimitive * const * components, ComponentCountType count, Color3 emission):
    m_vbsdfCount(count), m_vbsdfWeights(weights), m_vbsdfs((const VBSDFClosure **) components), m_emission(emission)
{
}

std::ostream & operator<<(std::ostream & out, const VolumeShadingFunction & bsdf)
{
    out << "{\n";
    out << "\t"
        << "vbsdfCount = " << bsdf.m_vbsdfCount << ",\n";
    out << "\t"
        << "BSDFs {\n";
    for (int32_t i = 0; i < bsdf.m_vbsdfCount; ++i)
    {
        const char * name = bsdf.m_vbsdfs[i]->name();

        out << "\t\t";
        out << "[ ";
        bsdf.m_vbsdfs[i]->print(out);
        out << ", ";
        out << "weight: " << bsdf.m_vbsdfWeights[i] << ", ";
        // out << "pdf: " << bsdf.m_bsdfPdfs[i];
        out << " ]";
        out << "\n";
    }
    out << "\t"
        << "}\n";
    out << "}\n";

    return out;
}

std::ostream & operator<<(std::ostream & out, const OSL::ShaderGlobals & globals)
{
    out << "{\n";
    out << "\t"
        << "P = " << globals.P << ", \n";
    out << "\t"
        << "dPdx = " << globals.dPdx << ", \n";
    out << "\t"
        << "dPdy = " << globals.dPdy << ", \n";
    out << "\t"
        << "dPdz = " << globals.dPdz << ", \n";
    out << "\t"
        << "I = " << globals.I << ", \n";
    out << "\t"
        << "dIdx = " << globals.dIdx << ", \n";
    out << "\t"
        << "dIdy = " << globals.dIdy << ", \n";
    out << "\t"
        << "N = " << globals.N << ", \n";
    out << "\t"
        << "Ng = " << globals.Ng << ", \n";
    out << "\t"
        << "u = " << globals.u << ", \n";
    out << "\t"
        << "dudx = " << globals.dudx << ", \n";
    out << "\t"
        << "dudy = " << globals.dudy << ", \n";
    out << "\t"
        << "v = " << globals.v << ", \n";
    out << "\t"
        << "dvdx = " << globals.dvdx << ", \n";
    out << "\t"
        << "dvdy = " << globals.dvdy << ", \n";
    out << "\t"
        << "dPdu = " << globals.dPdu << ", \n";
    out << "\t"
        << "dPdv = " << globals.dPdv << ", \n";
    out << "\t"
        << "time = " << globals.time << ", \n";
    out << "\t"
        << "dtime = " << globals.dtime << ", \n";
    out << "\t"
        << "dPdtime = " << globals.dPdtime << ", \n";
    out << "\t"
        << "Ps = " << globals.Ps << ", \n";
    out << "\t"
        << "Psdx = " << globals.dPsdx << ", \n";
    out << "\t"
        << "Psdy = " << globals.dPsdy << ", \n";
    out << "}";

    return out;
}

} // namespace shn