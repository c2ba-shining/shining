#include "CustomClosure.h"
#include "CompositeBxDF.h"
#include "IMathUtils.h"
#include "LoggingUtils.h"
#include "OslHeaders.h"
#include "ShiningRendererServices.h"

namespace shn
{

void processEmissionClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
void processTransparencyClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
void processBSSRDFClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
void processVolumeClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
void processBSDFClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
void processBaseColorClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);
void processScatterDistanceColorClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w);

extern OSL::ClosureParam bsdf_diffuse_params[];
extern OSL::ClosureParam bsdf_diffuse_fresnel_params[];
extern OSL::ClosureParam bsdf_diffuse_ggx_reflection_compensation_params[];
extern OSL::ClosureParam bsdf_fake_subsurface_params[];
extern OSL::ClosureParam bsdf_translucent_params[];
extern OSL::ClosureParam bsdf_reflection_params[];
extern OSL::ClosureParam bsdf_reflection_fresnel_params[];
extern OSL::ClosureParam bsdf_refraction_params[];
extern OSL::ClosureParam bsdf_dielectric_params[];
extern OSL::ClosureParam bsdf_transparent_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_reflection_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_reflection_energy_compensation_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_reflection_anisotropy_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_refraction_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_refraction_anisotropy_params[];
extern OSL::ClosureParam bsdf_ggx_glass_reflect_compensation_params[];
extern OSL::ClosureParam bsdf_ggx_glass_refract_compensation_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_metal_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_glass_params[];
extern OSL::ClosureParam bsdf_volume_params[];
#ifdef SHN_ENABLE_MERL_BSDF
extern OSL::ClosureParam bsdf_merl_params[];
#endif
extern OSL::ClosureParam bsdf_westin_sheen_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_legacy_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_refraction_legacy_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_aniso_legacy_params[];
extern OSL::ClosureParam bsdf_microfacet_ggx_aniso_refraction_legacy_params[];
extern OSL::ClosureParam bssrdf_gaussian_params[];
extern OSL::ClosureParam bssrdf_disney_params[];
extern OSL::ClosureParam bsdf_kajiya_hair_diffuse_params[];
extern OSL::ClosureParam bsdf_kajiya_hair_specular_params[];
extern OSL::ClosureParam bsdf_neulander_hair_diffuse_params[];
extern OSL::ClosureParam bsdf_neulander_hair_specular_params[];
extern OSL::ClosureParam toon_outline_data_params[];

void bsdf_diffuse_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_diffuse_ggx_reflection_compensation_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_diffuse_fresnel_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_fake_subsurface_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_translucent_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_reflection_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_reflection_fresnel_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_refraction_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_dielectric_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_transparent_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_reflection_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_reflection_energy_compensation_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_reflection_anisotropy_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_refraction_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_refraction_anisotropy_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_ggx_glass_reflect_compensation_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_ggx_glass_refract_compensation_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_metal_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_glass_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_volume_prepare(OSL::RendererServices *, int id, void * data);
#ifdef SHN_ENABLE_MERL_BSDF
void bsdf_merl_prepare(OSL::RendererServices *, int id, void * data);
#endif
void bsdf_westin_sheen_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_legacy_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_refraction_legacy_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_aniso_legacy_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_microfacet_ggx_aniso_refraction_legacy_prepare(OSL::RendererServices *, int id, void * data);
void bssrdf_gaussian_prepare(OSL::RendererServices *, int id, void * data);
void bssrdf_disney_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_kajiya_hair_diffuse_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_kajiya_hair_specular_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_neulander_hair_diffuse_prepare(OSL::RendererServices *, int id, void * data);
void bsdf_neulander_hair_specular_prepare(OSL::RendererServices *, int id, void * data);
void closure_toon_outline_data_prepare(OSL::RendererServices *, int id, void * data);

static bool genericClosureCompare(const ClosurePrimitive * dataA, const ClosurePrimitive * dataB)
{
    DASSERT(dataA && dataB);
    return strcmp(dataA->name(), dataB->name()) == 0 && dataA->mergeable(dataB); // #todo optimize this its horrible; use ustring for name and bitewiseCmp whenever possible...
}

static void genericClosureSetup(OSL::RendererServices *, int id, void * data)
{
    assert(data);
    reinterpret_cast<ClosurePrimitive *>(data)->setup();
}

const CustomClosure custom_closures[] = {
    { CustomClosure::CLOSURE_EMISSION_ID, "emission", nullptr, nullptr, nullptr, processEmissionClosure },
    { CustomClosure::CLOSURE_BSDF_DIFFUSE_ID, "diffuse", bsdf_diffuse_params, bsdf_diffuse_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_DIFFUSE_FRESNEL_ID, "diffuse_fresnel", bsdf_diffuse_fresnel_params, bsdf_diffuse_fresnel_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_DIFFUSE_GGX_REFLECTION_COMPENSATION_ID, "shn_diffuse_ggx_reflection_compensation", bsdf_diffuse_ggx_reflection_compensation_params,
      bsdf_diffuse_ggx_reflection_compensation_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_FAKE_SUBSURFACE_ID, "fake_subsurface", bsdf_fake_subsurface_params, bsdf_fake_subsurface_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_TRANSLUCENT_ID, "translucent", bsdf_translucent_params, bsdf_translucent_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_REFLECTION_ID, "shn_reflection", bsdf_reflection_params, bsdf_reflection_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_REFLECTION_FRESNEL_ID, "shn_reflection_fresnel", bsdf_reflection_fresnel_params, bsdf_reflection_fresnel_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_REFRACTION_ID, "shn_refraction", bsdf_refraction_params, bsdf_refraction_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_DIELECTRIC_ID, "shn_dielectric", bsdf_dielectric_params, bsdf_dielectric_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_TRANSPARENT_ID, "transparent", nullptr, nullptr, nullptr, processTransparencyClosure },
    { CustomClosure::CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ID, "shn_microfacet_ggx_reflection", bsdf_microfacet_ggx_reflection_params, bsdf_microfacet_ggx_reflection_prepare,
      genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ENERGY_COMPENSATION_ID, "shn_microfacet_ggx_reflection_energy_compensation",
      bsdf_microfacet_ggx_reflection_energy_compensation_params, bsdf_microfacet_ggx_reflection_energy_compensation_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_MICROFACET_GGX_REFLECTION_ANISOTROPY_ID, "shn_microfacet_ggx_reflection_anisotropy", bsdf_microfacet_ggx_reflection_anisotropy_params,
      bsdf_microfacet_ggx_reflection_anisotropy_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ID, "shn_microfacet_ggx_refraction", bsdf_microfacet_ggx_refraction_params, bsdf_microfacet_ggx_refraction_prepare,
      genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_ANISOTROPY_ID, "shn_microfacet_ggx_refraction_anisotropy", bsdf_microfacet_ggx_refraction_anisotropy_params,
      bsdf_microfacet_ggx_refraction_anisotropy_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_GGX_GLASS_REFLECTION_COMPENSATION_ID, "shn_ggx_glass_reflect_compensation", bsdf_ggx_glass_reflect_compensation_params,
      bsdf_ggx_glass_reflect_compensation_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_GGX_GLASS_REFRACTION_COMPENSATION_ID, "shn_ggx_glass_refract_compensation", bsdf_ggx_glass_refract_compensation_params,
      bsdf_ggx_glass_refract_compensation_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_GGX_METAL_ID, "shn_ggx_metal", bsdf_microfacet_ggx_metal_params, bsdf_microfacet_ggx_metal_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_GGX_GLASS_ID, "shn_ggx_glass", bsdf_microfacet_ggx_glass_params, bsdf_microfacet_ggx_glass_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_VOLUME_ID, "volume", bsdf_volume_params, bsdf_volume_prepare, genericClosureSetup, processVolumeClosure },
#ifdef SHN_ENABLE_MERL_BSDF
    { CustomClosure::CLOSURE_BSDF_MERL_ID, "merl", bsdf_merl_params, bsdf_merl_prepare, genericClosureSetup, processBSDFClosure },
#endif
    { CustomClosure::CLOSURE_BSDF_WESTIN_SHEEN_ID, "westin_sheen", bsdf_westin_sheen_params, bsdf_westin_sheen_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_MICROFACET_GGX_LEGACY_ID, "microfacet_ggx_legacy", bsdf_microfacet_ggx_legacy_params, bsdf_microfacet_ggx_legacy_prepare, genericClosureSetup,
      processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_MICROFACET_GGX_REFRACTION_LEGACY_ID, "microfacet_ggx_refraction_legacy", bsdf_microfacet_ggx_refraction_legacy_params,
      bsdf_microfacet_ggx_refraction_legacy_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_MICROFACET_GGX_ANISO_LEGACY_ID, "microfacet_ggx_aniso_legacy", bsdf_microfacet_ggx_aniso_legacy_params, bsdf_microfacet_ggx_aniso_legacy_prepare,
      genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_MICROFACET_GGX_ANISO_REFRACTION_LEGACY_ID, "microfacet_ggx_aniso_refraction_legacy", bsdf_microfacet_ggx_aniso_refraction_legacy_params,
      bsdf_microfacet_ggx_aniso_refraction_legacy_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSSRDF_GAUSSIAN_ID, "shn_gaussian_bssrdf", bssrdf_gaussian_params, bssrdf_gaussian_prepare, genericClosureSetup, processBSSRDFClosure },
    { CustomClosure::CLOSURE_BSSRDF_DISNEY_ID, "shn_disney_bssrdf", bssrdf_disney_params, bssrdf_disney_prepare, genericClosureSetup, processBSSRDFClosure },
    { CustomClosure::CLOSURE_BSDF_KAJIYA_HAIR_DIFFUSE_ID, "kajiya_hair_diffuse", bsdf_kajiya_hair_diffuse_params, bsdf_kajiya_hair_diffuse_prepare, genericClosureSetup, processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_KAJIYA_HAIR_SPECULAR_ID, "kajiya_hair_specular", bsdf_kajiya_hair_specular_params, bsdf_kajiya_hair_specular_prepare, genericClosureSetup,
      processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_NEULANDER_HAIR_DIFFUSE_ID, "neulander_hair_diffuse", bsdf_neulander_hair_diffuse_params, bsdf_neulander_hair_diffuse_prepare, genericClosureSetup,
      processBSDFClosure },
    { CustomClosure::CLOSURE_BSDF_NEULANDER_HAIR_SPECULAR_ID, "neulander_hair_specular", bsdf_neulander_hair_specular_params, bsdf_neulander_hair_specular_prepare, genericClosureSetup,
      processBSDFClosure },
    { CustomClosure::CLOSURE_TOON_OUTLINE_DATA_ID, "shn_toon_outline_data", toon_outline_data_params, closure_toon_outline_data_prepare, genericClosureSetup, nullptr },
    { CustomClosure::CLOSURE_BASE_COLOR_ID, "shn_base_color", nullptr, nullptr, nullptr, processBaseColorClosure },
    { CustomClosure::CLOSURE_SCATTER_DISTANCE_COLOR_ID, "shn_scatter_distance_color", nullptr, nullptr, nullptr, processScatterDistanceColorClosure }
};

static_assert(sizeof(custom_closures) / sizeof(custom_closures[0]) == CustomClosure::NCUSTOM_CLOSURES, "custom_closures size mismatch");

void registerCustomClosures(OSL::ShadingSystem * shadingsys)
{
    for (int cid = 0; cid < CustomClosure::NCUSTOM_CLOSURES; ++cid)
    {
        const CustomClosure * clinfo = &custom_closures[cid];
        assert(clinfo->id == cid);
        shadingsys->register_closure(clinfo->name, cid, clinfo->params, clinfo->prepare, clinfo->setup);
    }
}

ClosureTreeEvaluator::ClosureTreeEvaluator(ShadingMemoryPool & memPool): m_memPool(memPool)
{
}

void ClosureTreeEvaluator::beginEval()
{
    for (auto & v: m_closureWeights)
        v.clear();
    for (auto & v: m_closurePrimitives)
        v.clear();

    m_emission = Color3(0.f);
    m_transparency = Color3(0.f);
    m_baseColor = Color3(0.f);
    m_scatterDistanceColor = Color3(0.f);
}

void ClosureTreeEvaluator::addClosure(ClosurePrimitive::Category category, Color3 weight, const void * data)
{
    m_closureWeights[category].emplace_back(weight);
    m_closurePrimitives[category].emplace_back((const ClosurePrimitive *) data);
}

template<typename Visitor>
void ClosureTreeEvaluator::visitClosureTree(const OSL::ClosureColor * closure, Color3 w, CustomClosure::ClosureType filter, const Visitor & visitor)
{
    if (!closure)
        return;
    switch (closure->id)
    {
    case OSL::ClosureColor::MUL:
    {
        const auto cw = ((const OSL::ClosureMul *) closure)->weight;
        visitClosureTree(((const OSL::ClosureMul *) closure)->closure, cw * w, filter, visitor);
        break;
    }
    case OSL::ClosureColor::ADD:
    {
        visitClosureTree(((const OSL::ClosureAdd *) closure)->closureA, w, filter, visitor);
        visitClosureTree(((const OSL::ClosureAdd *) closure)->closureB, w, filter, visitor);
        break;
    }
    default:
    {
        const OSL::ClosureComponent * comp = (const OSL::ClosureComponent *) closure;
        if (filter != CustomClosure::NCUSTOM_CLOSURES && filter != comp->id)
        {
            break;
        }
        assert(comp->id < CustomClosure::NCUSTOM_CLOSURES);
        visitor(comp, w);
        break;
    }
    }
}

SurfaceShadingFunction ClosureTreeEvaluator::evalSurface(const OSL::ShaderGlobals & globals, const OSL::ClosureColor * Ci)
{
    beginEval();

    const auto visitor = [&](const OSL::ClosureComponent * comp, const Color3 & weight) { custom_closures[comp->id].process(*this, comp, weight); };

    visitClosureTree(Ci, Color3(1), CustomClosure::NCUSTOM_CLOSURES, visitor);

    size_t bsdfCount = 0;
    ClosurePrimitive ** bsdfComponents = nullptr;
    Color3 * bsdfWeights = nullptr;
    std::tie(bsdfCount, bsdfWeights, bsdfComponents) = allocComponents(ClosurePrimitive::Category::BSDF);

    size_t bssrdfCount = 0;
    ClosurePrimitive ** bssrdfComponents = nullptr;
    Color3 * bssrdfWeights = nullptr;
    std::tie(bssrdfCount, bssrdfWeights, bssrdfComponents) = allocComponents(ClosurePrimitive::Category::BSSRDF);

    return SurfaceShadingFunction(bsdfWeights, bsdfComponents, bsdfCount, bssrdfWeights, bssrdfComponents, bssrdfCount, m_transparency, m_emission, m_baseColor, m_scatterDistanceColor);
}

VolumeShadingFunction ClosureTreeEvaluator::evalVolume(const OSL::ShaderGlobals & globals, const OSL::ClosureColor * Ci)
{
    beginEval();

    const auto visitor = [&](const OSL::ClosureComponent * comp, const Color3 & weight) { custom_closures[comp->id].process(*this, comp, weight); };

    visitClosureTree(Ci, Color3(1), CustomClosure::NCUSTOM_CLOSURES, visitor);

    size_t count = 0;
    ClosurePrimitive ** components = nullptr;
    Color3 * weights = nullptr;
    std::tie(count, weights, components) = allocComponents(ClosurePrimitive::Category::Volume);

    return VolumeShadingFunction(weights, components, count, m_emission);
}

Color3 ClosureTreeEvaluator::evalLightEmissionColor(const OSL::ShaderGlobals & lightGlobals, const OSL::ClosureColor * Ci)
{
    beginEval();

    const auto visitor = [&](const OSL::ClosureComponent * comp, const Color3 & weight) { custom_closures[comp->id].process(*this, comp, weight); };

    visitClosureTree(Ci, Color3(1), CustomClosure::CLOSURE_EMISSION_ID, visitor);

    return m_emission;
}

Color3 ClosureTreeEvaluator::evalTransparency(const OSL::ShaderGlobals & globals, const OSL::ClosureColor * Ci)
{
    beginEval();

    const auto visitor = [&](const OSL::ClosureComponent * comp, const Color3 & weight) { custom_closures[comp->id].process(*this, comp, weight); };

    visitClosureTree(Ci, Color3(1), CustomClosure::CLOSURE_BSDF_TRANSPARENT_ID, visitor);

    return m_transparency;
}

std::tuple<size_t, Color3 *, ClosurePrimitive **> ClosureTreeEvaluator::allocComponents(ClosurePrimitive::Category category) const
{
    const size_t closureCount = m_closurePrimitives[category].size();
    ClosurePrimitive ** ptrArray = m_memPool.allocArray<ClosurePrimitive *>(closureCount);
    Color3 * weightArray = m_memPool.allocArray<Color3>(closureCount);
    std::copy(begin(m_closureWeights[category]), end(m_closureWeights[category]), weightArray);
    for (size_t i = 0; i < closureCount; ++i)
    {
        ptrArray[i] = m_memPool.alloc(m_closurePrimitives[category][i]);
    }
    return std::make_tuple(closureCount, weightArray, ptrArray);
}

void processEmissionClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w)
{
    allocator.m_emission += comp->w * w;
}

void processTransparencyClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w)
{
    allocator.m_transparency += comp->w * w;
}

void processBSSRDFClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w)
{
    const auto totalWeight = w * comp->w;

    // Try to merge with other BSSRDFs
    // #todo is it not a big waste of time ? :/
    for (size_t i = 0; i < allocator.m_closureWeights[ClosurePrimitive::BSSRDF].size(); ++i)
    {
        if (genericClosureCompare(allocator.m_closurePrimitives[ClosurePrimitive::BSSRDF][i], (const ClosurePrimitive *) comp->data()))
        {
            allocator.m_closureWeights[ClosurePrimitive::BSSRDF][i] += totalWeight;
        }
        return;
    }

    allocator.addClosure(ClosurePrimitive::BSSRDF, totalWeight, comp->data());
}

void processVolumeClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w)
{
    allocator.addClosure(ClosurePrimitive::Volume, w * comp->w, comp->data());
}

void processBSDFClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w)
{
    const bool isTransparent = ((BSDFClosure *) comp->data())->scatteringTypes() == ScatteringTypeBits::Straight;
    const auto totalWeight = w * comp->w;

    // Try to merge with other BSDFs
    // #todo is it not a big waste of time ? :/
    for (size_t i = 0; i < allocator.m_closureWeights[ClosurePrimitive::BSDF].size(); ++i)
    {
        if (genericClosureCompare(allocator.m_closurePrimitives[ClosurePrimitive::BSDF][i], (const ClosurePrimitive *) comp->data()))
        {
            if (isTransparent)
            {
                // take the max weight to keep the more "transparent" closure
                allocator.m_closureWeights[ClosurePrimitive::BSDF][i] = shn::max(allocator.m_closureWeights[ClosurePrimitive::BSDF][i], totalWeight);
            }
            else
            {
                allocator.m_closureWeights[ClosurePrimitive::BSDF][i] += totalWeight;
            }
            return;
        }
    }

    allocator.addClosure(ClosurePrimitive::BSDF, totalWeight, comp->data());
}

void processBaseColorClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w)
{
    allocator.m_baseColor += comp->w * w;
}

void processScatterDistanceColorClosure(ClosureTreeEvaluator & allocator, const OSL::ClosureComponent * comp, const Color3 & w)
{
    allocator.m_scatterDistanceColor += comp->w * w;
}

} // namespace shn
