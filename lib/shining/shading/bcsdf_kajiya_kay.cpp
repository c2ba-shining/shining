#include "CustomClosure.h"
#include "IMathUtils.h"
#include "OslHeaders.h"
#include "sampling/ShapeSamplers.h"

namespace shn
{

class KajiyaHairDiffuseClosure: public BSDFClosure
{
public:
    V3f m_T;

    KajiyaHairDiffuseClosure(): BSDFClosure(ScatteringTypeBits::Diffuse, ScatteringEventBits::Reflect)
    {
    }

    void setup(){};

    bool mergeable(const ClosurePrimitive * other) const
    {
        const KajiyaHairDiffuseClosure * comp = (const KajiyaHairDiffuseClosure *) other;
        return m_T == comp->m_T && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "kajiya_hair_diffuse";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_T[0] << ", " << m_T[1] << ", " << m_T[2] << "))";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        auto cosTheta = shn::dot(m_T, omega_in);
        auto randv = 0.5f * (1 - cosTheta);
        auto pdfNewRandv = (4.f / 9.f) * (2.5f - std::abs(randv - 0.5f));

        auto sinTheta = shn::cos2sin(cosTheta);
        auto bsdf = sinTheta * M_1_PI * M_1_PI; // Normalization factor is 1 / (pi.pi)
        float pdf = shn::pdfUniformSphere() * pdfNewRandv;

        auto weight = bsdf / pdf;

        return BxDFDirSample::MCEstimate::fromWeight(Color3(weight, weight, weight), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return evalReflect(sg, omega_out, omega_in);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;
        Color3 weight;
        // #todo: temporary implemented as a call to old fashionned sample method, waiting for hair tests in order to avoid breaking stuff by mistake
        const OSL::ustring label = sample(sg.Ng, omegaOut.val(), omegaOut.dx(), omegaOut.dy(), u1, u2, omegaIn.val(), omegaIn.dx(), omegaIn.dy(), pdf, weight);
        return fromWeight(omegaIn, weight, pdf, ScatteringType::Diffuse, (label == OSL::Labels::REFLECT) ? ScatteringEvent::Reflect : ScatteringEvent::Transmit);
    }

    OSL::ustring sample(
        const V3f & Ng, const V3f & omega_out, const V3f & domega_out_dx, const V3f & domega_out_dy, float randu, float randv, V3f & omega_in, V3f & domega_in_dx, V3f & domega_in_dy,
        float & pdf, Color3 & eval) const
    {
        // Neulander raised triangle importance sampling
        if (randv <= 0.5f)
        {
            randv = sqrtf(0.5f * (8.f + 9.f * randv)) - 2.f;
        }
        else
        {
            randv = 3.f - sqrtf(0.5f * (17.f - 9.f * randv));
        }
        auto pdfNewRandv = (4.f / 9.f) * (2.5f - std::abs(randv - 0.5f));

        float cosTheta, sinTheta;
        const auto phi = 2.f * M_PI * randu;
        cosTheta = 1.0f - 2.0f * randv;
        sinTheta = shn::cos2sin(cosTheta);
        pdf = shn::pdfUniformSphere();

        omega_in = shn::sphericalMapping(m_T, phi, cosTheta, sinTheta);

        pdf *= pdfNewRandv;

        auto bsdf = sinTheta * M_1_PI * M_1_PI; // Normalization factor is 1 / (pi.pi)
        auto weight = bsdf / pdf;

        eval.setValue(weight, weight, weight);

        domega_in_dx = V3f(10., 0., 0.);
        domega_in_dy = V3f(0., 10., 0.);

        return OSL::Labels::REFLECT;
    }

    V3f normal() const override
    {
        return m_T;
    }
};

class KajiyaHairSpecularClosure: public BSDFClosure
{
public:
    V3f m_T;
    float m_exp;

    KajiyaHairSpecularClosure(): BSDFClosure(ScatteringTypeBits::Glossy, ScatteringEventBits::Reflect)
    {
    }

    void setup()
    {
    }

    bool mergeable(const ClosurePrimitive * other) const
    {
        const KajiyaHairSpecularClosure * comp = (const KajiyaHairSpecularClosure *) other;
        return m_T == comp->m_T && m_exp == comp->m_exp && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "kajiya_hair_specular";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_T[0] << ", " << m_T[1] << ", " << m_T[2] << ")";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return evalReflect(omega_out, omega_in);
    }

    BxDFDirSample::MCEstimate evalReflect(const V3f & omega_out, const V3f & omega_in) const
    {
        float cos_i = m_T.dot(omega_in);
        float cos_o = m_T.dot(omega_out);

        float sin_i = shn::cos2sin(cos_i);
        float sin_o = shn::cos2sin(cos_o);
        float cos_diff = std::max(0.f, -cos_i * cos_o + sin_i * sin_o);

        // TODO: normalization? ha!
        float bsdf = powf(cos_diff, m_exp) * M_1_PI * M_1_PI;
        float pdf = shn::pdfUniformSphere();

        auto weight = bsdf / pdf;

        return BxDFDirSample::MCEstimate::fromWeight(Color3(weight, weight, weight), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return evalReflect(sg, omega_out, omega_in);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;
        Color3 weight;
        // #todo: temporary implemented as a call to old fashionned sample method, waiting for hair tests in order to avoid breaking stuff by mistake
        const OSL::ustring label = sample(sg.Ng, omegaOut.val(), omegaOut.dx(), omegaOut.dy(), u1, u2, omegaIn.val(), omegaIn.dx(), omegaIn.dy(), pdf, weight);
        return fromWeight(omegaIn, weight, pdf, ScatteringType::Glossy, (label == OSL::Labels::REFLECT) ? ScatteringEvent::Reflect : ScatteringEvent::Transmit);
    }

    OSL::ustring sample(
        const V3f & Ng, const V3f & omega_out, const V3f & domega_out_dx, const V3f & domega_out_dy, float randu, float randv, V3f & omega_in, V3f & domega_in_dx, V3f & domega_in_dy,
        float & pdf, Color3 & eval) const
    {
        omega_in = shn::sampleUniformSphere(m_T, randu, randv, pdf);

        eval.setValue(evalReflect(omega_out, omega_in).weight());

        // domega_in_dx = (2 * m_T.dot(domega_out_dx)) * m_T - domega_out_dx;
        // domega_in_dy = (2 * m_T.dot(domega_out_dy)) * m_T - domega_out_dy;
        // High differentials, for performance
        domega_in_dx = V3f(10., 0., 0.);
        domega_in_dy = V3f(0., 10., 0.);

        return OSL::Labels::REFLECT;
    }

    V3f normal() const override
    {
        return m_T;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_kajiya_hair_diffuse_params[] = { CLOSURE_VECTOR_PARAM(KajiyaHairDiffuseClosure, m_T), CLOSURE_FINISH_PARAM(KajiyaHairDiffuseClosure) };

ClosureParam bsdf_kajiya_hair_specular_params[] = { CLOSURE_VECTOR_PARAM(KajiyaHairSpecularClosure, m_T), CLOSURE_FLOAT_PARAM(KajiyaHairSpecularClosure, m_exp),
                                                    CLOSURE_FINISH_PARAM(KajiyaHairSpecularClosure) };

CLOSURE_PREPARE(bsdf_kajiya_hair_diffuse_prepare, KajiyaHairDiffuseClosure)
CLOSURE_PREPARE(bsdf_kajiya_hair_specular_prepare, KajiyaHairSpecularClosure)

} // namespace shn