/*
Copyright (c) 2009-2010 Sony Pictures Imageworks Inc., et al.
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of Sony Pictures Imageworks nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "CustomClosure.h"
#include "OslHeaders.h"
#include "sampling/ShapeSamplers.h"

namespace shn
{

class WestinSheenClosure: public BSDFClosure
{
public:
    V3f m_N;
    float m_edginess;
    //    float m_normalization;
    WestinSheenClosure(): BSDFClosure(ScatteringTypeBits::Diffuse)
    {
    }

    void setup(){};

    bool mergeable(const ClosurePrimitive * other) const
    {
        const WestinSheenClosure * comp = (const WestinSheenClosure *) other;
        return m_N == comp->m_N && m_edginess == comp->m_edginess && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "westin_sheen";
    }

    void print(std::ostream & out) const
    {
        out << name() << " (";
        out << "(" << m_N[0] << ", " << m_N[1] << ", " << m_N[2] << "), ";
        out << m_edginess;
        out << ")";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        float cos_in = std::max(m_N.dot(omega_in), 0.0f);
        float pdf = cos_in * (float) M_1_PI;

        if (pdf == 0.f || sg.N.dot(omega_in) <= 0.f)
        {
            return BxDFDirSample::MCEstimate::fromWeight(Color3(0), pdf);
        }

        float cosNO = m_N.dot(omega_out);
        float sinNO2 = 1 - cosNO * cosNO;
        float westin = sinNO2 > 0 ? powf(sinNO2, 0.5f * m_edginess) : 0;

        return BxDFDirSample::MCEstimate::fromWeight(Color3(westin), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;

        // we are viewing the surface from the right side - send a ray out with cosine
        // distribution over the hemisphere
        omegaIn.val() = sampleCosineHemisphere(m_N, u1, u2, pdf);

        if (dot(omegaIn.val(), sg.N) <= 0.f)
            return BxDFDirSample();

        // TODO: account for sheen when sampling
        const float cosNO = dot(m_N, omegaOut.val());
        const float sinNO2 = 1 - cosNO * cosNO;
        const float westin = sinNO2 > 0 ? powf(sinNO2, 0.5f * m_edginess) : 0;

        Color3 weight = Color3(westin);

        // TODO: find a better approximation for the diffuse bounce
        omegaIn.dx() = (2 * m_N.dot(omegaOut.dx())) * m_N - omegaOut.dx();
        omegaIn.dy() = (2 * m_N.dot(omegaOut.dy())) * m_N - omegaOut.dy();
        omegaIn.dx() *= 125;
        omegaIn.dy() *= 125;

        return fromWeight(omegaIn, weight, pdf, ScatteringType::Diffuse, ScatteringEvent::Reflect);
    }

    V3f normal() const override
    {
        return m_N;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_westin_sheen_params[] = { CLOSURE_VECTOR_PARAM(WestinSheenClosure, m_N), CLOSURE_FLOAT_PARAM(WestinSheenClosure, m_edginess), CLOSURE_FINISH_PARAM(WestinSheenClosure) };

CLOSURE_PREPARE(bsdf_westin_sheen_prepare, WestinSheenClosure)

} // namespace shn
