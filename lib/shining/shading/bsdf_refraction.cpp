#include "CustomClosure.h"
#include "IMathUtils.h"
#include "OslHeaders.h"
#include "SceneImpl.h"
#include "ShiningRendererServices.h"

namespace shn
{

// #note about m_eta:
// ratio of indices of refraction ior_in / ior_out, where ior_in is the ior of omega_in the sampled direction and ior_out is the ior of omega_out the fixed direction (ie -sg.I)
// In practice we only handle air <-> object transitions, so when coming from air (front facing), m_eta = ior_object, when coming from inside (back facing), m_eta = 1.0 / ior_object

class RefractionClosure final: public TplBSDFClosure<RefractionClosure, ScatteringTypeBits::Specular, ScatteringEventBits::Transmit>
{
public:
    V3f m_N;
    float m_eta;

    float m_absorption = 0.f;
    float m_absorptionDistance = 0.f;
    Color3 m_absorptionColor = Color3(0.f);

    const char * name() const override
    {
        return "shn_refraction";
    }

    void print(std::ostream & out) const override
    {
        out << name() << "(";
        out << m_N << ", ";
        out << m_eta << ", ";
        out << m_absorption << ", ";
        out << m_absorptionDistance << ", ";
        out << m_absorptionColor;
        out << ")";
    }

    float albedo(const V3f & omegaOut) const override
    {
        const auto cosNO = dot(m_N, omegaOut);
        return cosNO == 0.f ? 0.f : 1.0f - fresnelDielectric(cosNO, m_eta);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const V3fD2 & omegaOut, float uComponent, V2f uDir) const override
    {
        // Fake transparency in case eta == 1 (which means eta_i == eta_o). In that case, the media are the same and there is no visible deformations (eg. air => air)
        if (m_eta == 1.f)
            return fromWeight(-omegaOut, Color3(1.f), 1.f, ScatteringType::Specular, ScatteringEvent::Transmit);

        const auto Ft = albedo(omegaOut.val());
        if (Ft <= 0.f)
            return BxDFDirSample(); // total internal reflection

        const auto cosNO = clamp(dot(omegaOut.val(), m_N), 0.f, 1.f);
        const auto T = evalRefractionDirection(omegaOut.val(), m_N, cosNO, 1.f / m_eta);
        V3f dTdx, dTdy;
        evalRefractionDerivatives(omegaOut.dx(), omegaOut.dy(), m_N, cosNO, 1.f / m_eta, 0.f, dTdx, dTdy);

        const auto omegaIn = OSL::Dual2<V3f>(T, dTdx, dTdy);

        Color3 weight = Color3(Ft > 0.f && dot(sg.N, omegaIn.val()) < 0 ? Ft : 0.f);

        // compute absorption
        auto renderState = sg.renderstate;
        if (!isBlack(weight) && m_absorption > 0.f && renderState)
        {
            const shn::RenderState * state = reinterpret_cast<const shn::RenderState *>(renderState);
            weight *= rayTraceAbsorption(state->scene, sg.P, omegaIn.val(), sg.time, m_absorption, m_absorptionDistance, m_absorptionColor);
        }

        return fromWeight(omegaIn, weight, 1.f, ScatteringType::Specular, ScatteringEvent::Transmit);
    }
};

class DielectricClosure final: public TplBSDFClosure<DielectricClosure, ScatteringTypeBits::Specular, ScatteringEventBits::Both>
{
public:
    V3f m_N;
    float m_eta;

    float m_absorption = 0.f;
    float m_absorptionDistance = 0.f;
    Color3 m_absorptionColor = Color3(0.f);

    const char * name() const override
    {
        return "shn_dielectric";
    }

    void print(std::ostream & out) const override
    {
        out << name() << "(";
        out << m_N << ", ";
        out << m_eta << ", ";
        out << m_absorption << ", ";
        out << m_absorptionDistance << ", ";
        out << m_absorptionColor;
        out << ")";
    }

    float albedo(const V3f & omegaOut) const override
    {
        const auto cosNO = dot(m_N, omegaOut);
        return cosNO == 0.f ? 0.f : 1.0f;
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const V3fD2 & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto cosNO = dot(omegaOut.val(), m_N);
        if (cosNO == 0.f)
            return BxDFDirSample{};

        const auto Fr = fresnelDielectric(dot(omegaOut.val(), m_N), m_eta);

        if (uComponent < Fr)
        {
            const auto R = evalReflectionDirection(omegaOut.val(), m_N, cosNO);
            V3f dRdx, dRdy;
            evalReflectionDerivatives(omegaOut.dx(), omegaOut.dy(), m_N, 0.f, dRdx, dRdy);

            return fromWeight(V3fD2(R, dRdx, dRdy), Color3{ 1.f }, Fr, ScatteringType::Specular, ScatteringEvent::Reflect);
        }

        const auto T = evalRefractionDirection(omegaOut.val(), m_N, cosNO, 1.f / m_eta);
        V3f dTdx, dTdy;
        evalRefractionDerivatives(omegaOut.dx(), omegaOut.dy(), m_N, cosNO, 1.f / m_eta, 0.f, dTdx, dTdy);

        const V3fD2 omegaIn(T, dTdx, dTdy);
        Color3 weight{ dot(sg.Ng, omegaIn.val()) < 0 ? 1.f : 0.f }; // If we are not entering the object, set weight to zero

        // compute absorption
        auto renderState = sg.renderstate;
        if (!isBlack(weight) && m_absorption > 0.f && renderState)
        {
            const shn::RenderState * state = reinterpret_cast<const shn::RenderState *>(renderState);
            weight *= rayTraceAbsorption(state->scene, sg.P, omegaIn.val(), sg.time, m_absorption, m_absorptionDistance, m_absorptionColor);
        }

        return fromWeight(omegaIn, weight, 1 - Fr, ScatteringType::Specular, ScatteringEvent::Transmit);
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_refraction_params[] = { CLOSURE_VECTOR_PARAM(RefractionClosure, m_N),
                                          CLOSURE_FLOAT_PARAM(RefractionClosure, m_eta),
                                          CLOSURE_FLOAT_PARAM(RefractionClosure, m_absorption),
                                          CLOSURE_FLOAT_PARAM(RefractionClosure, m_absorptionDistance),
                                          CLOSURE_COLOR_PARAM(RefractionClosure, m_absorptionColor),
                                          CLOSURE_FINISH_PARAM(RefractionClosure) };

ClosureParam bsdf_dielectric_params[] = { CLOSURE_VECTOR_PARAM(DielectricClosure, m_N),
                                          CLOSURE_FLOAT_PARAM(DielectricClosure, m_eta),
                                          CLOSURE_FLOAT_PARAM(DielectricClosure, m_absorption),
                                          CLOSURE_FLOAT_PARAM(DielectricClosure, m_absorptionDistance),
                                          CLOSURE_COLOR_PARAM(DielectricClosure, m_absorptionColor),
                                          CLOSURE_FINISH_PARAM(DielectricClosure) };

CLOSURE_PREPARE(bsdf_refraction_prepare, RefractionClosure)
CLOSURE_PREPARE(bsdf_dielectric_prepare, DielectricClosure)

} // namespace shn
