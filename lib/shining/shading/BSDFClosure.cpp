#include "BSDFClosure.h"
#include "IMathUtils.h"
#include "sampling/PatternSamplers.h"
#include "sampling/ShapeSamplers.h"

namespace shn
{

std::string scatteringEventString(ScatteringEvent event)
{
    switch (event)
    {
    case ScatteringEvent::Reflect:
        return "reflect";
    case ScatteringEvent::Transmit:
        return "transmit";
    case ScatteringEvent::Absorb:
        return "absorb";
    default:
        break;
    }
    return "";
}

std::string scatteringTypeString(ScatteringTypeBits type)
{
    std::string result;
    if (any(type & ScatteringTypeBits::Diffuse))
        result += "diffuse_";
    if (any(type & ScatteringTypeBits::Glossy))
        result += "glossy_";
    if (any(type & ScatteringTypeBits::Specular))
        result += "specular_";
    if (any(type & ScatteringTypeBits::Straight))
        result += "straight_";
    return result.substr(0, result.size() - 1);
}

std::string scatteringTypeString(ScatteringType type)
{
    return scatteringTypeString(ScatteringTypeBits(1 << int(type)));
}

Color3 estimateDirectionalAlbedo(const V3f & omegaOut, const BSDFClosure & bsdf, size_t maxSampleCount, RandomState & rng)
{
    const auto w = std::max(size_t(1), size_t(std::sqrt(maxSampleCount)));
    const auto sampleCount = w * w;

    std::vector<float> componentSamples(sampleCount);
    jittered1d(rng, componentSamples.data(), sampleCount);

    std::vector<V2f> dirSamples(sampleCount);
    jittered2d(rng, dirSamples.data(), w, w);

    OSL::ShaderGlobals sg;
    memset(&sg, 0, sizeof(sg));

    sg.P = V3f(0.f);
    sg.N = sg.Ng = bsdf.normal();
    sg.I = -omegaOut;

    Color3 directionalAlbedoEstimate = Color3(0.f);
    Color3 other = Color3(0.f);

    for (size_t i = 0; i < sampleCount; ++i)
    {
        const auto dirSample = bsdf.sample(sg, componentSamples[i], dirSamples[i]);
        if (dirSample.pdf() > 0.f)
            directionalAlbedoEstimate += dirSample.weight();
    }

    return directionalAlbedoEstimate / float(sampleCount);
}

std::ostream & operator<<(std::ostream & out, const BxDFDirSample & sample)
{
    return out << "BxDFDirSample {\n"
               << "\tdirection: " << sample.direction << ",\n\testimate: " << sample.estimate << ",\n\tscatteringDirection: " << scatteringEventString(sample.scatteringDirection)
               << ",\n\tscatteringType: " << scatteringTypeString(sample.scatteringType) << ",\n\tclosureCategory: " << sample.closureCategory << "\n}";
}

} // namespace shn