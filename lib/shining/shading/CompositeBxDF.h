#pragma once

#include "BSDFClosure.h"
#include "EmissionBits.h"
#include "IMathUtils.h"
#include "OslHeaders.h"
#include "Shining.h"
#include "ShiningRendererServices.h"
#include <numeric>

OSL_NAMESPACE_ENTER
struct ShaderGlobals;
OSL_NAMESPACE_EXIT

namespace shn
{

struct BSDFContribs
{
    enum
    {
        DiffuseReflect,
        DiffuseTransmit,
        GlossyReflect,
        GlossyTransmit,
        SpecularReflect,
        SpecularTransmit,
        Count
    };
    BxDFDirSample::MCEstimate estimates[Count];

    void clear()
    {
        for (auto & estimate: estimates)
        {
            estimate = BxDFDirSample::MCEstimate::zero();
        }
    }

    BxDFDirSample::MCEstimate & operator[](size_t i)
    {
        return estimates[i];
    }

    const BxDFDirSample::MCEstimate & operator[](size_t i) const
    {
        return estimates[i];
    }

    Color3 diffuseReflectEval() const
    {
        return estimates[DiffuseReflect].eval();
    }

    Color3 diffuseTransmitEval() const
    {
        return estimates[DiffuseTransmit].eval();
    }

    Color3 diffuseEval() const
    {
        return diffuseReflectEval() + diffuseTransmitEval();
    }

    Color3 glossyReflectEval() const
    {
        return estimates[GlossyReflect].eval();
    }

    Color3 glossyTransmitEval() const
    {
        return estimates[GlossyTransmit].eval();
    }

    Color3 glossyEval() const
    {
        return glossyReflectEval() + glossyTransmitEval();
    }

    Color3 specularReflectEval() const
    {
        return estimates[SpecularReflect].eval();
    }

    Color3 specularTransmitEval() const
    {
        return estimates[SpecularTransmit].eval();
    }

    Color3 specularEval() const
    {
        return specularReflectEval() + specularTransmitEval();
    }

    Color3 eval() const
    {
        return diffuseEval() + glossyEval() + specularEval();
    }

    float pdf() const
    {
        return estimates[DiffuseReflect].pdf() + estimates[DiffuseTransmit].pdf() + estimates[GlossyReflect].pdf() + estimates[GlossyTransmit].pdf();
    }
};

/// Represents a weighted sum of BSDFS
/// NOTE: no need to inherit from BSDF here because we use a "flattened" representation and therefore never nest these
///
class SurfaceShadingFunction
{
public:
    using ComponentCountType = uint8_t;

    SurfaceShadingFunction() = default;

    // baseColor and scatterDistanceColor are useless for shading and only stored here because we output AOVs for them.
    // #todo LPE dev - use LPE and outputShader to output these attributes (and any shading tree)
    SurfaceShadingFunction(
        const Color3 * bsdfWeights, ClosurePrimitive * const * bsdfComponents, ComponentCountType bsdfCount, const Color3 * bssrdfWeights, ClosurePrimitive * const * bssrdfComponents,
        ComponentCountType bssrdfCount, Color3 transparency, Color3 emission, Color3 baseColor, Color3 scatterDistanceColor);

    bool singularOnly() const
    {
        return m_allBSDFSingular && m_bssrdfCount == 0;
    }

    bool straightOnly() const
    {
        return m_allBSDFStraight && m_bssrdfCount == 0;
    }

    ComponentCountType bsdfCount() const
    {
        return m_bsdfCount;
    }

    ComponentCountType bssrdfCount() const
    {
        return m_bssrdfCount;
    }

    ComponentCountType componentCount() const
    {
        return m_bsdfCount + m_bssrdfCount;
    }

    Color3 opacity() const
    {
        return Color3(1.f) - m_transparency;
    }

    const Color3 & transparency() const
    {
        return m_transparency;
    }

    const Color3 & emission() const
    {
        return m_emission;
    }

    class Sampler
    {
    public:
        Sampler() = default;

        // If rayDepth != nullptr, components C such that rayDepth->d[C] == 0 are assigned a null pdf and are neither sampled nor evaluated.
        template<typename Allocator>
        Sampler(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, const Color3 & pathWeight, const RayDepth * rayDepth, Allocator & allocator):
            m_bsdfPdfs(allocator.template allocArray<float>(bxdf.bsdfCount())), m_bssrdfPdfs(allocator.template allocArray<float>(bxdf.bssrdfCount())),
            m_bsdfIndices(allocator.template allocArray<ComponentCountType>(bxdf.bsdfCount())), m_bssrdfIndices(allocator.template allocArray<ComponentCountType>(bxdf.bssrdfCount())),
            m_bsdfCount(bxdf.bsdfCount()), m_bssrdfCount(bxdf.bssrdfCount())
        {
            init(bxdf, sg, pathWeight, rayDepth);
        }

        BxDFDirSample::MCEstimate evalBSDF(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, const V3f & omegaIn, EmissionBits emitComponent = EmissionBits::All) const;

        BSDFContribs evalBSDFContribs(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, const V3f & omegaIn, EmissionBits emitComponent) const;

        V3f sampleShadingNormal(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, float uComponent) const;

        BxDFDirSample sampleBSDFDirection(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, float uBSDF, float u1, float u2) const;

        BxDFDirSample sampleTransparency(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg) const;

        BxDFDirSample sampleDirection(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sgO, const OSL::ShaderGlobals & sgI, float uComponent, float u1, float u2) const;

        BxDFDirSample sampleOpaqueDirection(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sgO, const OSL::ShaderGlobals & sgI, float uComponent, float u1, float u2) const;

        float sampleRadiusBSSRDF(const SurfaceShadingFunction & bxdf, float uStrategy, float uRadius, float & pdf, float * uRemap = nullptr) const;

        float pdfRadiusBSSRDF(const SurfaceShadingFunction & bxdf, float radius) const;

        float transparencySamplingProbability() const
        {
            return m_transparencySamplingProbability;
        }

        friend void compare(const Sampler & s1, const Sampler & s2);

    public:
        void init(const SurfaceShadingFunction & bxdf, const OSL::ShaderGlobals & sg, const Color3 & pathWeight, const RayDepth * rayDepth);

        // Choose a BSDF/BSSRDF (if possible) according to random number uBSDFComponent/uBSSRDFComponent.
        // If uRemap != null, a new independant random number is stored in *uRemap
        const BSDFClosure * sampleBSDF(const SurfaceShadingFunction & bxdf, float uBSDFComponent, Color3 & weight, float & pdf, float * uRemap = nullptr) const;
        const BSSRDFClosure * sampleBSSRDF(const SurfaceShadingFunction & bxdf, float uBSSRDFComponent, float & pdf, float * uRemap = nullptr) const;

        float m_transparencySamplingProbability = 0.f;
        float m_bsdfSamplingProbability = 0.f;
        float m_bssrdfSamplingProbability = 0.f;

        float * m_bsdfPdfs = nullptr;
        float * m_bssrdfPdfs = nullptr;

        ComponentCountType * m_bsdfIndices = nullptr;
        ComponentCountType * m_bssrdfIndices = nullptr;

        ComponentCountType m_bsdfCount = 0;
        ComponentCountType m_bssrdfCount = 0;
    };

    Color3 evalBSSRDFDiffusion(float r) const;

    Color3 evalBSSRDFFresnelDiffusion(const OSL::ShaderGlobals & sgO, float r) const;

    Color3 color(ScatteringTypeBits scatteringTypes, ScatteringEventBits events) const;

    float maxRadiusBSSRDF() const;

    const Color3 & baseColor() const
    {
        return m_baseColor;
    }

    const Color3 & scatterDistanceColor() const
    {
        return m_scatterDistanceColor;
    }

    static SurfaceShadingFunction fullyTransparent()
    {
        SurfaceShadingFunction f;
        f.m_transparency = Color3(1.f);
        return f;
    }

    static SurfaceShadingFunction fullyEmissive(Color3 emission)
    {
        SurfaceShadingFunction f;
        f.m_emission = emission;
        return f;
    }

    friend std::ostream & operator<<(std::ostream & out, const SurfaceShadingFunction & bsdf);

private:
    const Color3 * m_bsdfWeights = nullptr;
    const Color3 * m_bssrdfWeights = nullptr;

    const BSDFClosure ** m_bsdfs = nullptr;
    const BSSRDFClosure ** m_bssrdfs = nullptr;

    Color3 m_transparency = Color3(0.f);
    Color3 m_emission = Color3(0.f);
    Color3 m_baseColor = Color3(0.f);
    Color3 m_scatterDistanceColor = Color3(0.f);

    ComponentCountType m_bsdfCount = 0;
    ComponentCountType m_bssrdfCount = 0;

    bool m_allBSDFSingular = true;
    bool m_allBSDFStraight = true;
};

BxDFDirSample sampleCosineBSSRDFLobe(const V3f & normal, float rx, float ry);

BxDFDirSample::MCEstimate evalCosineBSSRDFLobe(const V3f & normal, const V3f & omegaIn);

class VolumeShadingFunction
{
public:
    using ComponentCountType = uint8_t;

    VolumeShadingFunction() = default;

    VolumeShadingFunction(const Color3 * weights, ClosurePrimitive * const * components, ComponentCountType count, Color3 emission);

    ComponentCountType vbsdfCount() const
    {
        return m_vbsdfCount;
    }

    Color3 evalPhase(const OSL::ShaderGlobals & sg, const V3f & wi, EmissionBits emitComponent) const;

    Color3 extinction() const;

    const Color3 & emission() const
    {
        return m_emission;
    }

    friend std::ostream & operator<<(std::ostream & out, const VolumeShadingFunction & bsdf);

private:
    const Color3 * m_vbsdfWeights = nullptr;
    const VBSDFClosure ** m_vbsdfs = nullptr;
    ComponentCountType m_vbsdfCount = 0;

    Color3 m_emission = Color3(0.f);
};

std::ostream & operator<<(std::ostream & out, const OSL::ShaderGlobals & globals);

} // namespace shn
