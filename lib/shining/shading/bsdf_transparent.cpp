/*
Copyright (c) 2009-2010 Sony Pictures Imageworks Inc., et al.
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of Sony Pictures Imageworks nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "CustomClosure.h"
#include "OslHeaders.h"
#include "Shining_p.h"

namespace shn
{

class TransparentClosure: public BSDFClosure
{
public:
    TransparentClosure(): BSDFClosure(ScatteringTypeBits::Straight, ScatteringEventBits::Transmit)
    {
    }

    void setup()
    {
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "transparent";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ()";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return BxDFDirSample::MCEstimate();
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        return fromEval(-omegaOut, Color3(1.f), 1.f, ScatteringType::Straight, ScatteringEvent::Transmit);
    }

    V3f normal() const override
    {
        assert(false);
        return V3f(0.f);
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_transparent_params[] = { CLOSURE_FINISH_PARAM(TransparentClosure) };

CLOSURE_PREPARE(bsdf_transparent_prepare, TransparentClosure)

} // namespace shn
