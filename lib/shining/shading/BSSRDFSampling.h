#pragma once

#include "DebugUtils.h"
#include "OslHeaders.h"
#include "SceneImpl.h"
#include "shading/CompositeBxDF.h"

namespace shn
{

struct BSSRDFShadingPoint
{
    const OSL::ShaderGlobals & globals;
    const SurfaceShadingFunction & bxdf;
    const SurfaceShadingFunction::Sampler & bxdfSampler;
    const Ray & incidentRay;
    const RayDifferentials & incidentRayDiffs;
};

struct BSSRDFSamplingParams
{
    float uStrategy;
    float uRotation;
    V2f uPoint;
};

struct BSSRDFSamplePoint
{
    OSL::ShaderGlobals globals;
    float samplingWeight = 0.f;

#ifdef SHN_ENABLE_DEBUG
    float bssrdfSampledRadius = 0.f;
    size_t bssrdfIntersectionCount = 0;
    float bssrdfPointPmf = 0.f;
    float bssrdfPointPdf = 0.f;
    std::vector<Ray> bssrdfRays;
    std::vector<float> bssrdfRaysPdf;
#endif

    BSSRDFSamplePoint()
    {
        memset(&globals, 0, sizeof(globals));
    }
};

struct BSSRDFSamplingDataStorage
{
    static const size_t MaxPointCount = 64;
    Ray rays[MaxPointCount]; // Each ray defines a point
    size_t pointCount = 0;
    float pointCDF[MaxPointCount];
    float samplingWeight[MaxPointCount];
};

enum class BSSRDFMISHeuristic
{
    None, // No MIS
    Balance,
    Max,
    AlphaMax
};

enum class BSSRDFResampling : bool
{
    False,
    True
};

BSSRDFSamplePoint sampleBSSRDFPoint(
    const BSSRDFShadingPoint & point, const BSSRDFSamplingParams & sample, BSSRDFSamplingDataStorage & bssrdfSamplingData, const SceneImpl & scene, BSSRDFMISHeuristic misHeuristic,
    BSSRDFResampling resampling);
} // namespace shn