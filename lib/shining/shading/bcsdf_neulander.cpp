#include "CustomClosure.h"
#include "IMathUtils.h"
#include "OslHeaders.h"
#include "sampling/ShapeSamplers.h"

namespace shn
{

class NeulanderHairDiffuseClosure: public BSDFClosure
{
public:
    V3f m_T;
    float m_fThetaMin, m_fThetaMax;

    float m_fNormalizationFactor;
    float m_fCosThetaMin, m_fCosThetaMax;

    NeulanderHairDiffuseClosure(): BSDFClosure(ScatteringTypeBits::Diffuse, ScatteringEventBits::Reflect)
    {
    }

    void setup() override
    {
        if (m_fThetaMax < m_fThetaMin)
        {
            std::swap(m_fThetaMin, m_fThetaMax);
        }
        m_fThetaMin = std::max(0.f, m_fThetaMin);
        m_fThetaMax = std::min(float(M_PI), m_fThetaMax);

        m_fNormalizationFactor = 1.f / (M_PI * M_PI - 0.5f * M_PI * (sinf(2 * m_fThetaMax) - sinf(2 * m_fThetaMin)));
        m_fCosThetaMax = cosf(m_fThetaMax);
        m_fCosThetaMin = cosf(m_fThetaMin);
    }

    bool mergeable(const ClosurePrimitive * other) const override
    {
        const NeulanderHairDiffuseClosure * comp = (const NeulanderHairDiffuseClosure *) other;
        return m_T == comp->m_T && BSDFClosure::mergeable(other);
    }

    size_t memsize() const override
    {
        return sizeof(*this);
    }

    const char * name() const override
    {
        return "neulander_hair_diffuse";
    }

    void print(std::ostream & out) const override
    {
        out << name() << " ((" << m_T[0] << ", " << m_T[1] << ", " << m_T[2] << "))";
    }

    float albedo(const V3f & omega_out) const override
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        auto cosTheta = shn::dot(m_T, omega_in);
        auto randv = (cosTheta - m_fCosThetaMax) / (m_fCosThetaMin - m_fCosThetaMax);
        auto pdfNewRandv = (4.f / 9.f) * (2.5f - std::abs(randv - 0.5f));

        auto sinTheta = shn::cos2sin(cosTheta);
        auto bsdf = sinTheta * m_fNormalizationFactor;
        float pdf = pdfNewRandv / (2 * M_PI * (m_fCosThetaMin - m_fCosThetaMax));

        auto weight = bsdf / pdf;

        return BxDFDirSample::MCEstimate::fromWeight(Color3(weight, weight, weight), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return evalReflect(sg, omega_out, omega_in);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;
        Color3 weight;
        // #todo: temporary implemented as a call to old fashionned sample method, waiting for hair tests in order to avoid breaking stuff by mistake
        const OSL::ustring label = sample(sg.Ng, omegaOut.val(), omegaOut.dx(), omegaOut.dy(), u1, u2, omegaIn.val(), omegaIn.dx(), omegaIn.dy(), pdf, weight);
        return fromWeight(omegaIn, weight, pdf, ScatteringType::Diffuse, (label == OSL::Labels::REFLECT) ? ScatteringEvent::Reflect : ScatteringEvent::Transmit);
    }

    OSL::ustring sample(
        const V3f & Ng, const V3f & omega_out, const V3f & domega_out_dx, const V3f & domega_out_dy, float randu, float randv, V3f & omega_in, V3f & domega_in_dx, V3f & domega_in_dy,
        float & pdf, Color3 & eval) const
    {
        // Neulander raised triangle importance sampling
        if (randv <= 0.5f)
        {
            randv = sqrtf(0.5f * (8.f + 9.f * randv)) - 2.f;
        }
        else
        {
            randv = 3.f - sqrtf(0.5f * (17.f - 9.f * randv));
        }
        auto pdfNewRandv = (4.f / 9.f) * (2.5f - std::abs(randv - 0.5f));

        float cosTheta, sinTheta;
        const auto phi = 2.f * M_PI * randu;
        cosTheta = m_fCosThetaMax + randv * (m_fCosThetaMin - m_fCosThetaMax);
        sinTheta = shn::cos2sin(cosTheta);
        pdf = 1.f / (2 * M_PI * (m_fCosThetaMin - m_fCosThetaMax));

        omega_in = shn::sphericalMapping(m_T, phi, cosTheta, sinTheta);

        pdf *= pdfNewRandv;

        auto bsdf = sinTheta * m_fNormalizationFactor; // Normalization factor is 1 / (pi.pi - 0.5 * pi * (sin 2.thetamax - sin 2.thetamin)
        auto weight = bsdf / pdf;

        eval.setValue(weight, weight, weight);

        domega_in_dx = V3f(10., 0., 0.);
        domega_in_dy = V3f(0., 10., 0.);

        return OSL::Labels::REFLECT;
    }

    V3f normal() const override
    {
        return m_T;
    }
};

class NeulanderHairSpecularClosure: public BSDFClosure
{
public:
    V3f m_T;
    float m_fThetaRange;

    NeulanderHairSpecularClosure(): BSDFClosure(ScatteringTypeBits::Glossy, ScatteringEventBits::Reflect)
    {
    } //-V730 Ignore PVS studio warning

    void setup()
    {
    }

    bool mergeable(const ClosurePrimitive * other) const
    {
        const NeulanderHairSpecularClosure * comp = (const NeulanderHairSpecularClosure *) other;
        return m_T == comp->m_T && BSDFClosure::mergeable(other);
    }

    size_t memsize() const
    {
        return sizeof(*this);
    }

    const char * name() const
    {
        return "neulander_hair_specular";
    }

    void print(std::ostream & out) const
    {
        out << name() << " ((" << m_T[0] << ", " << m_T[1] << ", " << m_T[2] << "))";
    }

    float albedo(const V3f & omega_out) const
    {
        return 1.0f;
    }

    BxDFDirSample::MCEstimate evalReflect(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        float cos_i = shn::dot(m_T, omega_in);
        float cos_o = shn::dot(m_T, omega_out);

        float theta_o = acosf(cos_o);
        auto theta_spec = float(M_PI) - theta_o;
        float thetaMin = std::max(0.f, theta_spec - m_fThetaRange);
        float thetaMax = std::min(float(M_PI), theta_spec + m_fThetaRange);
        float cosThetaMin = cosf(thetaMin);
        float cosThetaMax = cosf(thetaMax);

        if (cos_i > cosThetaMin || cos_i < cosThetaMax)
        {
            return BxDFDirSample::MCEstimate();
        }

        float sinThetaMin = shn::cos2sin(cosThetaMin);
        float sinThetaMax = shn::cos2sin(cosThetaMax);

        float sin_i = shn::cos2sin(cos_i);
        float sin_o = shn::cos2sin(cos_o);
        float shadingValue = std::max(0.f, -cos_i * cos_o + sin_i * sin_o);

        float normalizationFactor = 2 * M_PI * (m_fThetaRange + sinf(m_fThetaRange) * cosf(m_fThetaRange)) * sin_o;

        auto bsdf = shadingValue / normalizationFactor;

        auto cosDifference = cosThetaMin - cosThetaMax;
        auto rcpPdf = 0.f;
        float pdf = 0.f;
        if (cosDifference > 0.f)
        {
            pdf = 1 / (2 * M_PI * cosDifference);
            rcpPdf = 2 * M_PI * cosDifference;
        }
        else
        {
            pdf = 0.f;
            rcpPdf = 0.f;
        }
        auto weight = rcpPdf * bsdf;

        return BxDFDirSample::MCEstimate::fromWeight(Color3(weight, weight, weight), pdf);
    }

    BxDFDirSample::MCEstimate evalTransmit(const OSL::ShaderGlobals & sg, const V3f & omega_out, const V3f & omega_in) const override
    {
        return evalReflect(sg, omega_out, omega_in);
    }

    BxDFDirSample sample(const OSL::ShaderGlobals & sg, const OSL::Dual2<V3f> & omegaOut, float uComponent, V2f uDir) const override
    {
        const auto u1 = uDir.x;
        const auto u2 = uDir.y;

        OSL::Dual2<V3f> omegaIn;
        float pdf = 0.f;
        Color3 weight;
        // #todo: temporary implemented as a call to old fashionned sample method, waiting for hair tests in order to avoid breaking stuff by mistake
        const OSL::ustring label = sample(sg.Ng, omegaOut.val(), omegaOut.dx(), omegaOut.dy(), u1, u2, omegaIn.val(), omegaIn.dx(), omegaIn.dy(), pdf, weight);
        return fromWeight(omegaIn, weight, pdf, ScatteringType::Glossy, (label == OSL::Labels::REFLECT) ? ScatteringEvent::Reflect : ScatteringEvent::Transmit);
    }

    OSL::ustring sample(
        const V3f & Ng, const V3f & omega_out, const V3f & domega_out_dx, const V3f & domega_out_dy, float randu, float randv, V3f & omega_in, V3f & domega_in_dx, V3f & domega_in_dy,
        float & pdf, Color3 & eval) const
    {
        float cos_o = shn::dot(m_T, omega_out);

        float theta_o = acosf(cos_o);
        auto theta_spec = float(M_PI) - theta_o;
        float thetaMin = std::max(0.f, theta_spec - m_fThetaRange);
        float thetaMax = std::min(float(M_PI), theta_spec + m_fThetaRange);
        float cosThetaMin = cosf(thetaMin);
        float cosThetaMax = cosf(thetaMax);
        float sinThetaMin = shn::cos2sin(cosThetaMin);
        float sinThetaMax = shn::cos2sin(cosThetaMax);

        float cos_i, sin_i;
        const auto phi = 2.f * M_PI * randu;
        cos_i = cosThetaMax + randv * (cosThetaMin - cosThetaMax);
        sin_i = shn::cos2sin(cos_i);
        pdf = 1.f / (2 * M_PI * (cosThetaMin - cosThetaMax));

        omega_in = shn::sphericalMapping(m_T, phi, cos_i, sin_i);

        float sin_o = shn::cos2sin(cos_o);
        float cos_diff = std::max(0.f, -cos_i * cos_o + sin_i * sin_o);

        float c = 2 * M_PI * (m_fThetaRange + sinf(m_fThetaRange) * cosf(m_fThetaRange)) * sin_o;

        auto bsdf = cos_diff / c;

        auto weight = bsdf / pdf;

        eval.setValue(weight, weight, weight);

        // domega_in_dx = (2 * m_T.dot(domega_out_dx)) * m_T - domega_out_dx;
        // domega_in_dy = (2 * m_T.dot(domega_out_dy)) * m_T - domega_out_dy;
        // High differentials, for performance
        domega_in_dx = V3f(10., 0., 0.);
        domega_in_dy = V3f(0., 10., 0.);

        return OSL::Labels::REFLECT;
    }

    V3f normal() const override
    {
        return m_T;
    }
};

using namespace OSL;
using namespace OSL::pvt;

ClosureParam bsdf_neulander_hair_diffuse_params[] = { CLOSURE_VECTOR_PARAM(NeulanderHairDiffuseClosure, m_T), CLOSURE_FLOAT_PARAM(NeulanderHairDiffuseClosure, m_fThetaMin),
                                                      CLOSURE_FLOAT_PARAM(NeulanderHairDiffuseClosure, m_fThetaMax), CLOSURE_FINISH_PARAM(NeulanderHairDiffuseClosure) };

ClosureParam bsdf_neulander_hair_specular_params[] = { CLOSURE_VECTOR_PARAM(NeulanderHairSpecularClosure, m_T), CLOSURE_FLOAT_PARAM(NeulanderHairSpecularClosure, m_fThetaRange),
                                                       CLOSURE_FINISH_PARAM(NeulanderHairSpecularClosure) };

CLOSURE_PREPARE(bsdf_neulander_hair_diffuse_prepare, NeulanderHairDiffuseClosure)
CLOSURE_PREPARE(bsdf_neulander_hair_specular_prepare, NeulanderHairSpecularClosure)

} // namespace shn