#pragma once

#include "IMathTypes.h"
#include "LPE.h"
#include "OslHeaders.h"
#include "UString.h"
#include <OSL/optautomata.h>
#include <stack>
#include <vector>

namespace shn
{

class LPEAutomata
{
public:
    LPEAutomata() = default;

    int getTransition(int state, ustring symbol) const
    {
        return m_dfoptautomata.getTransition(state, symbol);
    };

    void * const * getOutputsInState(int state, int & count) const
    {
        return m_dfoptautomata.getRules(state, count);
    };

    size_t outputCount() const
    {
        return m_outputCount;
    }

private:
    LPEAutomata(OSL::DfOptimizedAutomata dfoptautomata, size_t outputCount): m_dfoptautomata(dfoptautomata), m_outputCount(outputCount)
    {
    }

    friend LPEAutomata compile(const std::vector<std::pair<const char *, size_t>> & patterns);

    // The famous so called DF automata
    OSL::DfOptimizedAutomata m_dfoptautomata;
    size_t m_outputCount = 0;
};

inline size_t ruleToOutputIndex(void * const rule)
{
    return (size_t) rule - 1;
}

LPEAutomata compile(const std::vector<std::pair<const char *, size_t>> & patterns);

class LPEState
{
public:
    LPEState(const LPEAutomata * accauto, Color3 * outputs);

    /// If the machine is broken no result will be stored, you can cut the branch
    bool broken() const
    {
        return m_state < 0;
    }

    /// Push a single label
    void move(ustring symbol);

    /// Push a NONE terminated array of labels
    void move(const ustring * symbols);

    /// very commonly we push all labels, this helps reducing code. custom can be NULL
    /// and although the last label is always stop, we leave the argument to make the
    /// code show that there is a STOP label there.
    void move(ustring event, ustring scatt, const ustring * custom, ustring stop);

    /// Check if a given movement is possible without breaking the automata.
    /// Leaves the state untouched
    bool test(ustring dir, ustring sca, const ustring * custom, ustring stop)
    {
        auto scope = push();
        move(dir, sca, custom, stop);
        return !broken();
    }

    /// Clears all the outputs to start integrating
    void begin();

    /// Send a result to whatever rules might be active in the current state
    void accum(const Color3 & color);

    const Color3 & getOutput(int idx) const
    {
        return m_outputs[idx];
    };

    class PushStateScope
    {
        LPEState * m_pAccumulator = nullptr;
        int m_savedState = -1;

    public:
        PushStateScope(LPEState & accumulator): m_pAccumulator(&accumulator), m_savedState(accumulator.getState())
        {
        }
        ~PushStateScope()
        {
            if (m_pAccumulator)
                m_pAccumulator->setState(m_savedState);
        }
        PushStateScope(const PushStateScope &) = delete;
        PushStateScope & operator=(const PushStateScope &) = delete;
        PushStateScope(PushStateScope && other)
        {
            std::swap(m_pAccumulator, other.m_pAccumulator);
            std::swap(m_savedState, other.m_savedState);
        }
        PushStateScope & operator=(PushStateScope && other)
        {
            std::swap(m_pAccumulator, other.m_pAccumulator);
            std::swap(m_savedState, other.m_savedState);
            return *this;
        }

        const PushStateScope & accum(const Color3 & color) const
        {
            m_pAccumulator->accum(color);
            return *this;
        }

        const PushStateScope & accum(const V3f & color) const
        {
            m_pAccumulator->accum(Color3(color));
            return *this;
        }

        template<typename GetColorFunctor>
        const PushStateScope & accum(const GetColorFunctor & getColor) const
        {
            if (!m_pAccumulator->broken())
                m_pAccumulator->accum(getColor());
            return *this;
        }

        const PushStateScope & move(ustring event, ustring scattering, const ustring * customs = nullptr) const
        {
            m_pAccumulator->move(event, scattering, customs, LPE::Stop);
            return *this;
        }

        template<typename Functor>
        const PushStateScope & apply(Functor && f) const
        {
            f(*this);
            return *this;
        }
    };

    PushStateScope push()
    {
        return PushStateScope{ *this };
    }

private:
    // A reference to the stateless automata that can be shared between multiple
    // threads
    const LPEAutomata * m_accum_automata;
    // The output array has as many entries as AOV's enabled, and they share
    // the same index so m_outputs[aov->getIndex()].aov == aov for AOV's linked
    // by rules and NULL for the rest
    Color3 * m_outputs;

    int m_state = 0; // 0 is our initial state always

    int getState() const
    {
        return m_state;
    }

    void setState(int state)
    {
        m_state = state;
    }
};

} // namespace shn