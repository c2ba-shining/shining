#include "LPE.h"
#include "accum.h"

namespace shn
{
namespace LPE
{

const ustring Camera{ "C" };
const ustring Reflect{ "R" };
const ustring Transmit{ "T" };
const ustring Light{ "L" };
const ustring Volume{ "V" };
const ustring SubSurface{ "SSS" };
const ustring Geometry{ "geometry" };
const ustring Integration{ "integration" };
const ustring Material{ "material" };
const ustring ToonOutline{ "toon_outline" };
const ustring SamplingStrategy{ "sampling_strategy" };

const std::vector<ustring> EventTypes = { Camera, Reflect, Transmit, Light, Volume, SubSurface, Geometry, Integration, Material, ToonOutline, SamplingStrategy };

// Surface scattering
const ustring Diffuse{ "d" };
const ustring Glossy{ "g" };
const ustring Specular{ "s" };
const ustring Straight{ "t" };

// Geometry scattering
const ustring PositionWorld{ "position_world" };
const ustring PositionCamera{ "position_camera" };
const ustring NormalWorld{ "normal_world" };
const ustring NormalCamera{ "normal_camera" };
const ustring UV{ "uv" };
const ustring Depth{ "depth" };
const ustring MotionVector{ "motion_vector" };
const ustring MotionVectorPixel{ "motion_vector_pixel" };
const ustring TriangleNormalWorld{ "triangle_normal_world" };
const ustring TriangleNormalCamera{ "triangle_normal_camera" };
const ustring TriangleColorId{ "triangle_color_id" };
const ustring InstanceColorId{ "instance_color_id" };

// Material scattering
const ustring DiffuseColor{ "diffuse_color" };
const ustring GlossyColor{ "glossy_color" };
const ustring BaseColor{ "base_color" };
const ustring ScatterDistanceColor{ "scatter_distance_color" };

// Integration scattering
const ustring Occlusion{ "occlusion" };

// Light scattering
const ustring Radiance{ "radiance" };
const ustring Shadows{ "shadows" };
const ustring Irradiance{ "irradiance" };
const ustring Background{ "background" };
const ustring Emission{ "emission" };

// Volume scattering
const ustring Extinction{ "extinction" };

// ToonOutline scattering
const ustring OuterColor{ "outer_color" };
const ustring InnerColor{ "inner_color" };
const ustring CornerColor{ "corner_color" };
const ustring Weights{ "weights" };

// SamplingStrategy scattering
const ustring LightSampling{ "light_sampling" };
const ustring MaterialSampling{ "material_sampling" };
const ustring ImportanceCaching{ "importance_caching" };

const std::vector<ustring> ScattTypes = { Diffuse,   Glossy,        Specular,          Straight,         PositionWorld,        PositionCamera, NormalWorld, NormalCamera, UV,
                                          Depth,     MotionVector,  MotionVectorPixel, Radiance,         Irradiance,           Shadows,        Background,  Emission,     Geometry,
                                          Occlusion, DiffuseColor,  GlossyColor,       BaseColor,        ScatterDistanceColor, Extinction,     OuterColor,  InnerColor,   CornerColor,
                                          Weights,   LightSampling, MaterialSampling,  ImportanceCaching };

const ustring Stop = OSL::Labels::STOP;
const ustring None = OSL::Labels::NONE;

using ScatteringMap = std::array<LabelsPair, size_t(ScatteringType::Count) * size_t(ScatteringEvent::Count)>;

const ScatteringMap bsdfEventToLPELabels = [&]() {
    ScatteringMap m;
    std::fill(begin(m), end(m), LabelsPair{ None, None });

#define FILL(EVENT, TYPE) m[scatteringId(ScatteringEvent::EVENT, ScatteringType::TYPE)] = { EVENT, TYPE }

    FILL(Reflect, Diffuse);
    FILL(Transmit, Diffuse);
    FILL(Reflect, Glossy);
    FILL(Transmit, Glossy);
    FILL(Reflect, Specular);
    FILL(Transmit, Specular);
    FILL(Reflect, Straight);
    FILL(Transmit, Straight);

#undef FILL

    return m;
}();

const std::array<ustring, 2 * (LightingImportanceCache::DistributionNames::Count + 1)> lightingICDistribNameToLPELabels = [&]() {
    std::array<ustring, 2 * (LightingImportanceCache::DistributionNames::Count + 1)> m;

    m[2 * LightingImportanceCache::DistributionNames::F] = ustring{ "F" };
    m[2 * LightingImportanceCache::DistributionNames::F + 1] = LPE::None;
    m[2 * LightingImportanceCache::DistributionNames::U] = ustring{ "U" };
    m[2 * LightingImportanceCache::DistributionNames::U + 1] = LPE::None;
    m[2 * LightingImportanceCache::DistributionNames::T] = ustring{ "T" };
    m[2 * LightingImportanceCache::DistributionNames::T + 1] = LPE::None;
    m[2 * LightingImportanceCache::DistributionNames::C] = ustring{ "C" };
    m[2 * LightingImportanceCache::DistributionNames::C + 1] = LPE::None;
    m[2 * LightingImportanceCache::DistributionNames::Count] = ustring{ "all_lights" };
    m[2 * LightingImportanceCache::DistributionNames::Count + 1] = LPE::None;

    return m;
}();

const ustring Dummy[] = { ustring{ "__dummy__" }, LPE::None };

} // namespace LPE
} // namespace shn