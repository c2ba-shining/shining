#include "accum.h"
#include "LoggingUtils.h"
#include "OSL/automata.h"
#include "OSL/lpeparse.h"

namespace shn
{

LPEAutomata compile(const std::vector<std::pair<const char *, size_t>> & patterns)
{
    std::vector<std::unique_ptr<OSL::lpexp::LPexp>> expressions;
    expressions.reserve(patterns.size());
    OSL::Parser parser(&LPE::EventTypes, &LPE::ScattTypes);
    for (const auto & pair: patterns)
    {
        OSL::LPexp * e = parser.parse(pair.first);
        if (parser.error())
        {
            // Definitely a critical and developper error, lets just print a message and exit
            SHN_LOGERR << "LPE parsing error " << parser.getErrorMsg() << " at char " << parser.getErrorPos();
            exit(-1);
        }
        expressions.emplace_back(e);
    }
    OSL::NdfAutomata ndfautomata;
    for (size_t i = 0; i < expressions.size(); ++i)
    {
        // Code from OSL::lpexp::Rule copied here
        auto firstLast = expressions[i]->genAuto(ndfautomata); // First generate the actual automata
        firstLast.second->setRule((void *) (patterns[i].second + 1)); // Cast output index to void* in order to store it as the rule of the last state of the expression; we add 1 because
                                                                      // otherwise OSL would think its a null ptr
        // The +1 is cancelled in ruleToOutputIndex function.

        // And then make its begin state accessible from the master initial state
        // of the automata so it becomes initial too
        ndfautomata.getInitial()->addTransition(OSL::lambda, firstLast.first); // #note lambda is the empty word
    }

    OSL::DfAutomata dfautomata;
    ndfautoToDfauto(ndfautomata, dfautomata);

    OSL::DfOptimizedAutomata dfoptautomata;
    dfoptautomata.compileFrom(dfautomata);

    return { dfoptautomata, expressions.size() };
}

LPEState::LPEState(const LPEAutomata * accauto, Color3 * outputs): m_accum_automata(accauto), m_outputs(outputs)
{
}

void LPEState::move(ustring symbol)
{
    if (m_state >= 0)
        m_state = m_accum_automata->getTransition(m_state, symbol);
}

void LPEState::move(const ustring * symbols)
{
    while (m_state >= 0 && symbols && *symbols != OSL::Labels::NONE)
        m_state = m_accum_automata->getTransition(m_state, *(symbols++));
}

void LPEState::move(ustring event, ustring scatt, const ustring * custom, ustring stop)
{
    if (m_state >= 0)
        m_state = m_accum_automata->getTransition(m_state, event);
    if (m_state >= 0)
        m_state = m_accum_automata->getTransition(m_state, scatt);
    while (m_state >= 0 && custom && *custom != OSL::Labels::NONE)
        m_state = m_accum_automata->getTransition(m_state, *(custom++));
    if (m_state >= 0)
        m_state = m_accum_automata->getTransition(m_state, stop);
}

void LPEState::begin()
{
    for (size_t i = 0; i < m_accum_automata->outputCount(); ++i)
        m_outputs[i] = Color3(0.f);
}

void LPEState::accum(const Color3 & color)
{
    if (m_state >= 0)
    {
        // get the rules field, the underlying type is a std::vector
        int nrules = 0;
        void * const * rules = m_accum_automata->getOutputsInState(m_state, nrules);
        // Iterate the vector
        for (int i = 0; i < nrules; ++i)
        {
            m_outputs[ruleToOutputIndex(rules[i])] += color;
        }
    }
}

} // namespace shn
