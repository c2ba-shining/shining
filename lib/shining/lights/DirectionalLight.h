#pragma once

#include "Light.h"

namespace shn
{
struct DirectionalLight: public Light
{
    LightSamplingResult sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const override;
    Color3 evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const override;

    V3f direction;
    float angle;
    M44f worldToLocalRotation;

private:
    void doUpdate(const Attributes & attr, const ShadingContext & lsCtx) override;

    json doSerialize() const override;
};
} // namespace shn