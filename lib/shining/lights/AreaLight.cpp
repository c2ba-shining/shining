#include "AreaLight.h"
#include "Attributes.h"
#include "sampling/PatternSamplers.h"
#include "sampling/ShapeSamplers.h"
#include "shading/CompositeBxDF.h"
#include "shading/CustomClosure.h"

namespace shn
{
template<typename Shape>
LightSamplingResult AreaLight<Shape>::sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const
{
    float lightIntensity = intensity;

    V3f pos = globals.P;

    float d = 0.f;
    float solidAnglePdf = 0.f;
    V3f normal;
    V2f uv;
    V3f ldir = shape.sampleSolidAngle(uDir1, uDir2, pos, solidAnglePdf, d, normal, uv, side);

    if (solidAnglePdf <= 0.f || d <= 0.f)
        return LightSamplingResult();

    const float rangeAttenuation = computeRangeAttenuation(d);

    LightSamplingResult result;
    result.incidentRadiance = Color3(rangeAttenuation * lightIntensity);
    result.incidentDirection.value = ldir;
    result.incidentDirection.pdf = solidAnglePdf;
    result.distance = d;
    result.position = pos + d * ldir;
    result.normal = normal;
    result.texcoord = uv;
    result.shadowDirection = result.incidentDirection.value;

    return result;
}

template<typename Shape>
EquiAngularSamplingParams AreaLight<Shape>::equiAngularParams(const V3f & org, const V3f & dir, float tfar, float uLight1, float uLight2) const
{
    V3f normal;
    V3f sampledPoint = shape.samplePoint(uLight1, uLight2, normal);

    float d = tfar;

    V3f ldir = sampledPoint - org;
    V3f lpos = org + ldir;

    float delta = std::min(std::max(dot(dir, ldir), 0.f), d); // Distance from the origin of the ray to the orthogonal projection of the sampled point (ldir must NOT be normalized here)
    float a = -delta;
    float b = d - delta;

    if (side != LightSide::Double)
    {
        if (side == LightSide::Back)
            normal = -normal;

        float areaInterscT = 0.f;
        if (!intersectPlane(sampledPoint, normal, org, dir, &areaInterscT))
        {
            return EquiAngularSamplingParams();
        }
        areaInterscT = std::min(std::max(areaInterscT, 0.f), d);

        const float cosAngle = dot(normal, dir);
        if (areaInterscT <= 0.f && cosAngle < 0.f) // Ray origin is behind the lighted side and ray direction is pointing away
        {
            return EquiAngularSamplingParams();
        }

        if (areaInterscT > 0.f)
        {
            if (cosAngle > 0.f)
                a = areaInterscT - delta;
            else if (cosAngle < 0.f)
                b = areaInterscT - delta;
        }
    }

    float D = std::max(shn::distance(lpos, delta * dir + org), 0.0001f); // max to avoid nan
    float thetaA = atanf(a / D);
    float thetaB = atanf(b / D);

    EquiAngularSamplingParams result;
    result.delta = delta;
    result.thetaA = thetaA;
    result.thetaB = thetaB;
    result.D = D;

    return result;
}

template<typename Shape>
Color3 AreaLight<Shape>::evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const
{
    if (colorShadingTreeId == -1)
    {
        return color;
    }

    OSL::ShaderGlobals lg;
    memset(&lg, 0, sizeof(lg));
    lg.u = samplingResult.texcoord.x;
    lg.v = samplingResult.texcoord.y;
    lg.I = samplingResult.shadowDirection;

    return evalLightEmissionColor(lsCtx, colorShadingTreeId, lg);
}

template<typename Shape>
LightSamplingResult AreaLight<Shape>::eval(const V3f & dir, const OSL::ShaderGlobals & globals) const
{
    float lightIntensity = intensity;

    float d = 0.f;
    V3f normal;
    V2f uv;
    float solidAnglePdf = shape.solidAngleSamplingPDF(globals.P, dir, d, normal, uv, side);

    if (solidAnglePdf <= 0.f || d <= 0.f)
        return LightSamplingResult();

    const float rangeAttenuation = computeRangeAttenuation(d);

    LightSamplingResult result;
    result.incidentRadiance = Color3(rangeAttenuation * lightIntensity);
    result.incidentDirection.value = dir;
    result.incidentDirection.pdf = solidAnglePdf;
    result.distance = d;
    result.position = globals.P + d * dir;
    result.normal = normal;
    result.texcoord = uv;
    result.shadowDirection = result.incidentDirection.value;

    return result;
}

template<typename Shape>
void AreaLight<Shape>::doUpdate(const Attributes & attr, const ShadingContext & lsCtx)
{
    side = attr.get<LightSide>(getLightAttributeStringName(L_ATTR_SIDE), LightSide::Front);
    samplingStrategy = attr.get<SamplingStrategy>(getLightAttributeStringName(L_ATTR_SAMPLINGSTRATEGY), SamplingStrategy::MisLightMaterial);

    visible = shape.update(localToWorld);

    // compute an approximated color for shaded AreaLight
    if (visible && colorShadingTreeId != -1)
    {
        // We sample a uniform sphere for evaluate AreaLight heuristic
        const auto sampleUniSphereDirection = [](const float u1, const float u2, float & dirPdf) -> V3f { return sampleUniformSphere(u1, u2, dirPdf); };

        computeColorHeuristic(4, lsCtx, sampleUniSphereDirection);
    }
}

template<typename Shape>
json AreaLight<Shape>::doSerialize() const
{
    json map;

    map["side"] = to_string(side);
    map["samplingStrategy"] = to_string(samplingStrategy);

    return map;
}

template<typename UnitShape>
bool TransformedShape<UnitShape>::update(const M44f & iLocalToWorld)
{
    localToWorld = UnitShape::getLocalMatrix(iLocalToWorld, center);

    const float localToWorldDeterminant = localToWorld.determinant();
    if (localToWorldDeterminant == 0.f) // Singular transform, make the light not visible
        return false;

    worldToLocal = localToWorld.inverse();
    worldToLocalDeterminant = worldToLocal.determinant();

    return true;
}

template<typename UnitShape>
V3f TransformedShape<UnitShape>::sampleSolidAngle(float u1, float u2, const V3f & position, float & solidAnglePdf, float & dist, V3f & normal, V2f & uv, LightSide side) const
{
    const V3f pointLocalSpace = pointWorldToLocal(position);
    const V3f dirLocalSpace = UnitShape::sampleSolidAngle(u1, u2, pointLocalSpace, solidAnglePdf, side);
    const V3f dirWorldSpace = normalize(dirLocalToWorld(dirLocalSpace));

    solidAnglePdf *= fabsf(solidAngleLinearTransformJacobian(dirWorldSpace, worldToLocal, worldToLocalDeterminant));

    dist = intersect(position, dirWorldSpace, &normal, &uv);
    if (dist <= 0.f)
    {
        // Should not happen
        solidAnglePdf = 0.f;
        return V3f(0.f);
    }

    return dirWorldSpace;
}

template<typename UnitShape>
float TransformedShape<UnitShape>::solidAngleSamplingPDF(const V3f & position, const V3f & dir, float & dist, V3f & normal, V2f & uv, LightSide side) const
{
    dist = intersect(position, dir, &normal, &uv);
    if (dist <= 0.f)
        return 0.f;

    const V3f pointLocal = pointWorldToLocal(position);
    const V3f dirLocal = normalize(dirWorldToLocal(dir));

    float solidAnglePdf = UnitShape::solidAngleSamplingPDF(pointLocal, dirLocal, side);

    if (solidAnglePdf <= 0.f)
        return 0.f;

    solidAnglePdf *= fabsf(solidAngleLinearTransformJacobian(dir, worldToLocal, worldToLocalDeterminant));

    return solidAnglePdf;
}

template<typename UnitShape>
V3f TransformedShape<UnitShape>::samplePoint(float u1, float u2, V3f & normal) const
{
    const V3f localPoint = UnitShape::samplePoint(u1, u2, normal);
    normal = normalize(normalLocalToWorld(normal));
    return pointLocalToWorld(localPoint);
}

template<typename UnitShape>
float TransformedShape<UnitShape>::intersect(const V3f & org, const V3f & dir, V3f * pNormal, V2f * pUV) const
{
    const V3f orgLocal = pointWorldToLocal(org);
    const V3f dirLocal = dirWorldToLocal(dir);
    const float dist = UnitShape::intersect(orgLocal, dirLocal, pNormal, pUV);
    if (pNormal)
        *pNormal = normalize(normalLocalToWorld(*pNormal));
    return dist;
}

bool RectShape::update(const M44f & localToWorld)
{
    p[0] = multVecMatrix(localToWorld, V3f(-0.5f, 0.5f, 0.f)); // upper-left
    p[1] = multVecMatrix(localToWorld, V3f(0.5f, 0.5f, 0.f)); // upper-right
    p[2] = multVecMatrix(localToWorld, V3f(-0.5f, -0.5f, 0.f)); // lower-left
    p[3] = multVecMatrix(localToWorld, V3f(0.5f, -0.5f, 0.f)); // lower-right
    area = 0.5f * length(cross(p[0] - p[3], p[2] - p[1])); // Area of a quad is half the length of the cross product of diagonals http://www.iquilezles.org/www/articles/areas/areas.htm
    direction = -normalize(frameZ(localToWorld));

    return area > 0.f;
}

V3f RectShape::sampleSolidAngle(float u1, float u2, const V3f & position, float & solidAnglePdf, float & dist, LightSide side) const
{
    // Sample quad area light
    V3f ldir = normalize(sampleQuad(p, u1, u2) - position);
    dist = intersectQuad(p, position, ldir);
    float cosineAngle = dot(direction, -ldir);

    if (side == LightSide::Front && cosineAngle <= 0.f)
    {
        solidAnglePdf = 0.f;
        return V3f(0);
    }

    if (side == LightSide::Back && cosineAngle >= 0.f)
    {
        solidAnglePdf = 0.f;
        return V3f(0);
    }

    solidAnglePdf = (cosineAngle != 0.f) ? sqr(dist) / (fabsf(cosineAngle) * area) : 0.f;
    return ldir;
}

float RectShape::solidAngleSamplingPDF(const V3f & position, const V3f & dir, float & dist, LightSide side) const
{
    dist = intersectQuad(p, position, dir);
    float cosineAngle = dot(direction, -dir);

    if (side == LightSide::Front && cosineAngle <= 0.f)
        return 0.f;

    if (side == LightSide::Back && cosineAngle >= 0.f)
        return 0.f;

    return (cosineAngle != 0.f) ? sqr(dist) / (fabsf(cosineAngle) * area) : 0.f;
}

V3f RectShape::samplePoint(float u1, float u2, V3f & normal) const
{
    normal = direction;
    return sampleQuad(p, u1, u2);
}

float RectShape::intersect(const V3f & org, const V3f & dir, V3f * pNormal, V2f * pUV) const
{
    float d = intersectQuad(p, org, dir);

    if (pNormal)
        *pNormal = direction;

    if (pUV)
    {
        const V3f I = org + d * dir;
        *pUV = quadUVSampling(p, org, dir, d);
    }

    return d;
}

V3f UnitQuad::sampleSolidAngle(float u1, float u2, const V3f & org, float & solidAnglePdf, LightSide side)
{
    if ((side == LightSide::Front && org.z >= 0.f) || (side == LightSide::Back && org.z <= 0.f))
    {
        solidAnglePdf = 0.f;
        return V3f(0.f);
    }

    V3f normal;
    V3f dirLocal = samplePoint(u1, u2, normal) - org;
    float d = length(dirLocal);
    dirLocal /= d; // normalization
    float cosineAngle = fabsf(dirLocal.z);
    solidAnglePdf = (cosineAngle > 0.f) ? 0.25f * sqr(d) / cosineAngle : 0.f;
    return dirLocal;
}

float UnitQuad::solidAngleSamplingPDF(const V3f & org, const V3f & dir, LightSide side)
{
    if (side == LightSide::Front && (org.z >= 0.f || dir.z <= 0.f))
        return 0.f;
    else if (side == LightSide::Back && (org.z <= 0.f || dir.z >= 0.f))
        return 0.f;

    float dist = intersect(org, dir);
    if (dist <= 0.f)
        return 0.f;
    return 0.25f * sqr(dist) / fabsf(dir.z);
}

V3f UnitQuad::samplePoint(float u1, float u2, V3f & normal)
{
    normal = V3f(0, 0, -1);
    return V3f(-1.f + 2.f * u1, -1.f + 2.f * u2, 0.f);
}

float UnitQuad::intersect(const V3f & org, const V3f & dir, V3f * pNormal, V2f * pUV)
{
    const float dist = intersectZPlane(org, dir);
    if (dist <= 0.f)
        return -1.f;

    const V3f I = org + dist * dir;
    if (max(fabsf(I.x), fabsf(I.y)) > 1.0f) // Point outside the quad
        return -1.f;

    if (pNormal)
        *pNormal = V3f(0, 0, -1);

    if (pUV)
        *pUV = V2f(1 - 0.5 * (1 + I.x), 0.5 * (1 + I.y));

    return dist;
}

V3f UnitDisk::sampleSolidAngle(float u1, float u2, const V3f & org, float & solidAnglePdf, LightSide side)
{
    if ((side == LightSide::Front && org.z >= 0.f) || (side == LightSide::Back && org.z <= 0.f))
    {
        solidAnglePdf = 0.f;
        return V3f(0.f);
    }

    V3f normal;
    V3f dirLocal = samplePoint(u1, u2, normal) - org;
    float d = length(dirLocal);
    dirLocal /= d; // normalization
    float cosineAngle = fabsf(dirLocal.z);
    solidAnglePdf = (cosineAngle > 0.f) ? F1_PI * sqr(d) / cosineAngle : 0.f;
    return dirLocal;
}

float UnitDisk::solidAngleSamplingPDF(const V3f & org, const V3f & dir, LightSide side)
{
    if (side == LightSide::Front && (org.z >= 0.f || dir.z <= 0.f))
        return 0.f;
    else if (side == LightSide::Back && (org.z <= 0.f || dir.z >= 0.f))
        return 0.f;

    float dist = intersect(org, dir);
    if (dist <= 0.f)
        return 0.f;
    return F1_PI * sqr(dist) / fabsf(dir.z);
}

V3f UnitDisk::samplePoint(float u1, float u2, V3f & normal)
{
    normal = V3f(0, 0, -1);
    const V2f diskSample = sampleConcentricDisk(u1, u2);
    return V3f(diskSample.x, diskSample.y, 0.f);
}

float UnitDisk::intersect(const V3f & org, const V3f & dir, V3f * pNormal, V2f * pUV)
{
    float dist;
    if (!intersectPlane(V3f(0), V3f(0, 0, 1), org, dir, &dist))
        return -1.f;

    if (dist <= 0.f)
        return -1.f;

    const V3f I = org + dist * dir;
    if (dot(I, I) > 1.f) // Point outside the disk
        return -1.f;

    if (pNormal)
        *pNormal = V3f(0, 0, -1);

    if (pUV)
        *pUV = V2f(1 - 0.5 * (1 + I.x), 0.5 * (1 + I.y));

    return dist;
}

V3f UnitSphere::sampleSolidAngle(float u1, float u2, const V3f & org, float & solidAnglePdf, LightSide side)
{
    const float sqrDistToCenter = dot(org, org);
    if (side == LightSide::Front && sqrDistToCenter <= 1.f)
    {
        solidAnglePdf = 0.f;
        return V3f(0.f);
    }
    if (side == LightSide::Back && sqrDistToCenter >= 1.f)
    {
        solidAnglePdf = 0.f;
        return V3f(0.f);
    }

    const V3f dirLocalSpace = sampleUniformSphereSolidAngle(V3f(0.f), 1.f, org, u1, u2, solidAnglePdf);
    if (solidAnglePdf <= 0.f)
        return V3f(0.f);

    return dirLocalSpace;
}

float UnitSphere::solidAngleSamplingPDF(const V3f & org, const V3f & dir, LightSide side)
{
    const float sqrDistToCenter = dot(org, org);
    if (side == LightSide::Front && sqrDistToCenter <= 1.f)
        return 0.f;
    if (side == LightSide::Back && sqrDistToCenter >= 1.f)
        return 0.f;

    return pdfUniformSphereSolidAngle(V3f(0.f), 1.f, org, dir);
}

V3f UnitSphere::samplePoint(float u1, float u2, V3f & normal)
{
    normal = sampleUniformSphere(u1, u2);
    return normal;
}

float UnitSphere::intersect(const V3f & org, const V3f & dir, V3f * pNormal, V2f * pUV)
{
    float tmin, tmax;
    if (!intersectUnitSphere(org, dir, &tmin, &tmax) || tmax <= 0.f)
    {
        return -1.f;
    }

    const float t = tmin > 0.f ? tmin : tmax;
    const V3f intersectLocalSpace = org + t * dir;

    if (pNormal)
        *pNormal = intersectLocalSpace;

    if (pUV)
    {
        const V2f phiTheta = rcpSphericalMapping(intersectLocalSpace);
        *pUV = V2f((FPI + phiTheta.x) * F1_2PI, phiTheta.y * F1_PI);
    }

    return t;
}

V3f UnitCylinder::sampleSolidAngle(float u1, float u2, const V3f & org, float & solidAnglePdf, LightSide side)
{
    const V2f orgLocal2 = V2f(org.x, org.y);
    const float sqrDistOrgLocal2 = dot(orgLocal2, orgLocal2);
    if (side == LightSide::Front && sqrDistOrgLocal2 <= 1.f)
    {
        solidAnglePdf = 0.f;
        return V3f(0.f);
    }
    if (side == LightSide::Back && sqrDistOrgLocal2 >= 1.f)
    {
        solidAnglePdf = 0.f;
        return V3f(0.f);
    }

    V3f normalLocal;
    float localDist;
    V3f dirLocal = normalize(samplePoint(u1, u2, normalLocal) - org, localDist);

    float tminLocal, tmaxLocal;
    if (!intersectUnitCylinder(org, dirLocal, &tminLocal, &tmaxLocal) || tmaxLocal <= 0.f)
    {
        // May occur because of numerical imprecision
        solidAnglePdf = 0.f;
        return V3f(0.f);
    }

    auto computePDF = [&](float distLocal) {
        const V3f intersectLocalSpace = org + distLocal * dirLocal;
        if (fabsf(intersectLocalSpace.z) >= 0.5f)
            return 0.f;

        const V3f normalLocalSpace = V3f(intersectLocalSpace.x, intersectLocalSpace.y, 0.f);
        const float cosAngle = dot(normalLocalSpace, dirLocal);

        float areaPdfLocal = 1.f / F2PI;

        return cosAngle ? areaPdfLocal * sqr(distLocal) / fabsf(cosAngle) : 0.f;
    };

    V3f intersectLocal;
    if (tminLocal > 0.f)
    {
        solidAnglePdf = computePDF(tminLocal);
        if (solidAnglePdf > 0.f)
            solidAnglePdf += computePDF(tmaxLocal);

        intersectLocal = org + tminLocal * dirLocal;
    }
    else
    {
        solidAnglePdf = computePDF(tmaxLocal);
        intersectLocal = org + tmaxLocal * dirLocal;
    }

    return dirLocal;
}

float UnitCylinder::solidAngleSamplingPDF(const V3f & org, const V3f & dir, LightSide side)
{
    const V2f orgLocal2 = V2f(org.x, org.y);
    const float sqrDistOrgLocal2 = dot(orgLocal2, orgLocal2);
    if (side == LightSide::Front && sqrDistOrgLocal2 <= 1.f)
        return 0.f;
    if (side == LightSide::Back && sqrDistOrgLocal2 >= 1.f)
        return 0.f;

    float tminLocal, tmaxLocal;
    if (!intersectUnitCylinder(org, dir, &tminLocal, &tmaxLocal) || tmaxLocal <= 0.f)
        return 0.f;

    auto computePDF = [&](float distLocal) {
        const V3f intersectLocalSpace = org + distLocal * dir;
        if (fabsf(intersectLocalSpace.z) >= 0.5f)
            return 0.f;

        const V3f normalLocalSpace = V3f(intersectLocalSpace.x, intersectLocalSpace.y, 0.f);
        const float cosAngle = dot(normalLocalSpace, dir);

        float areaPdfLocal = 1.f / F2PI;

        return cosAngle ? areaPdfLocal * sqr(distLocal) / fabsf(cosAngle) : 0.f;
    };

    float solidAnglePdf = 0.f;
    V3f intersectLocal;
    if (tminLocal > 0.f)
    {
        solidAnglePdf = computePDF(tminLocal);
        if (solidAnglePdf > 0.f)
            solidAnglePdf += computePDF(tmaxLocal);

        intersectLocal = org + tminLocal * dir;
    }
    else
    {
        solidAnglePdf = computePDF(tmaxLocal);
        intersectLocal = org + tmaxLocal * dir;
    }

    return solidAnglePdf;
}

V3f UnitCylinder::samplePoint(float u1, float u2, V3f & normal)
{
    const float angle = u1 * F2PI;
    const V3f localPoint = V3f(cosf(angle), sinf(angle), -0.5f + u2);
    normal = V3f(localPoint.x, localPoint.y, 0.f);
    return localPoint;
}

float UnitCylinder::intersect(const V3f & org, const V3f & dir, V3f * pNormal, V2f * pUV)
{
    float tmin, tmax;
    if (!intersectUnitCylinder(org, dir, &tmin, &tmax) || tmax <= 0.f)
    {
        return -1.f;
    }

    const float t = tmin > 0.f ? tmin : tmax;
    const V3f intersectLocalSpace = org + t * dir;

    if (fabsf(intersectLocalSpace.z) >= 0.5f)
        return -1.f;

    if (pNormal)
        *pNormal = V3f(intersectLocalSpace.x, intersectLocalSpace.y, 0.f);

    if (pUV)
    {
        const float angle = atan2(intersectLocalSpace.y, intersectLocalSpace.x);
        *pUV = V2f(0.5f + intersectLocalSpace.z, (FPI + angle) * F1_2PI);
    }

    return t;
}

// Explicit template instantiations
template struct TransformedShape<UnitQuad>;
template struct TransformedShape<UnitDisk>;
template struct TransformedShape<UnitSphere>;
template struct TransformedShape<UnitCylinder>;

#ifdef SHN_USE_UNITQUAD_AREALIGHT
template struct AreaLight<TransformedShape<UnitQuad>>; // Possible to use this one, but the sampling strategy is not the same as directy sampling the rectangle area and projecting on ray
                                                       // origin
#else
template struct AreaLight<RectShape>;
#endif
template struct AreaLight<TransformedShape<UnitDisk>>;
template struct AreaLight<TransformedShape<UnitSphere>>;
template struct AreaLight<TransformedShape<UnitCylinder>>;
} // namespace shn