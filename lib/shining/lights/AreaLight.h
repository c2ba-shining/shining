#pragma once

#include "Light.h"

namespace shn
{
enum class AreaLightShape
{
    Rect = 0,
    Disk,
    Cylinder,
    Sphere,
    Count
};

inline size_t areaLightShapeCount()
{
    return size_t(AreaLightShape::Count);
}

enum class LightSide
{
    Front = 0,
    Back,
    Double
};

inline std::string to_string(LightSide side)
{
    switch (side)
    {
    case LightSide::Front:
        return "front";
    case LightSide::Back:
        return "back";
    case LightSide::Double:
        return "double";
    }
}

template<typename Shape>
struct AreaLight: public Light
{
    LightSamplingResult sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const override;

    EquiAngularSamplingParams equiAngularParams(const V3f & org, const V3f & dir, float tfar, float uLight1, float uLight2) const override;

    Color3 evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const override;

    LightSamplingResult eval(const V3f & dir, const OSL::ShaderGlobals & globals) const override;

    bool needEquiAngularSampling() const override
    {
        return true;
    }

    bool isDelta() const override
    {
        return samplingStrategy == SamplingStrategy::Light; // If only light sampling strategy is used, the light is considered as a delta light to ignore the bsdf sampling strategy
    }

    bool needImportanceSampling() const override
    {
        // The light is importance sampled only if the asked strategy is light or MIS
        return samplingStrategy == SamplingStrategy::Light || samplingStrategy == SamplingStrategy::MisLightMaterial;
    }

    SamplingStrategy samplingStrategy;
    LightSide side;

    Shape shape;

private:
    void doUpdate(const Attributes & attr, const ShadingContext & lsCtx) override;

    json doSerialize() const override;
};

// This is the interface that a Shape type must implements in order to be used as template parameter of AreaLight.
// This class is only here for reference
struct ShapeInterface
{
    // Update the shape of an AreaLight with its transform. Must return true if the light is visible, false otherwise (in case of singular transform).
    bool update(const M44f & localToWorld);

    V3f sampleSolidAngle(float u1, float u2, const V3f & position, float & solidAnglePdf, float & dist, V3f & normal, V2f & uv, LightSide side) const;

    float solidAngleSamplingPDF(const V3f & position, const V3f & dir, float & dist, V3f & normal, V2f & uv, LightSide side) const;

    V3f samplePoint(float u1, float u2, V3f & normal) const;

    float intersect(const V3f & org, const V3f & dir, V3f * pNormal = nullptr, V2f * pUV = nullptr) const;

private:
    ~ShapeInterface(); // Prevents instanciation/inheritance
};

// Shape of a rectangle in world space
struct RectShape
{
    bool update(const M44f & localToWorld);

    V3f sampleSolidAngle(float u1, float u2, const V3f & position, float & solidAnglePdf, float & dist, LightSide side) const;

    float solidAngleSamplingPDF(const V3f & position, const V3f & dir, float & dist, LightSide side) const;

    V3f samplePoint(float u1, float u2, V3f & normal) const;

    float intersect(const V3f & org, const V3f & dir, V3f * pNormal = nullptr, V2f * pUV = nullptr) const;

    V3f p[4];
    V3f direction;
    float area;
};

// Shape expressed as an affine transformation of a given unitary shape (which does not depend on any parameter)
template<typename UnitShape>
struct TransformedShape
{
    bool update(const M44f & iLocalToWorld);

    V3f sampleSolidAngle(float u1, float u2, const V3f & position, float & solidAnglePdf, float & dist, V3f & normal, V2f & uv, LightSide side) const;

    float solidAngleSamplingPDF(const V3f & position, const V3f & dir, float & dist, V3f & normal, V2f & uv, LightSide side) const;

    V3f samplePoint(float u1, float u2, V3f & normal) const;

    float intersect(const V3f & org, const V3f & dir, V3f * pNormal = nullptr, V2f * pUV = nullptr) const;

private:
    V3f pointWorldToLocal(const V3f & point) const
    {
        return multVecMatrix(worldToLocal, point - center);
    }

    V3f dirWorldToLocal(const V3f & dir) const
    {
        return multVecMatrix(worldToLocal, dir);
    }

    V3f pointLocalToWorld(const V3f & point) const
    {
        return center + multVecMatrix(localToWorld, point);
    }

    V3f dirLocalToWorld(const V3f & dir) const
    {
        return multVecMatrix(localToWorld, dir);
    }

    V3f normalLocalToWorld(const V3f & normal) const
    {
        return multVecMatrixTransposed(worldToLocal, normal);
    }

    V3f center;
    M33f worldToLocal, localToWorld;
    float worldToLocalDeterminant;
};

// This is the interface that a UnitShape type must implements in order to be used as template parameter of TransformedShape.
// This class is only here for reference
struct UnitShapeInterface
{
    static M33f getLocalMatrix(const M44f & localToWorld, V3f & center);

    static V3f sampleSolidAngle(float u1, float u2, const V3f & position, float & solidAnglePdf, LightSide side);

    static float solidAngleSamplingPDF(const V3f & position, const V3f & dir, LightSide side);

    static V3f samplePoint(float u1, float u2, V3f & normal);

    static float intersect(const V3f & org, const V3f & dir, V3f * pNormal = nullptr, V2f * pUV = nullptr);

private:
    ~UnitShapeInterface(); // Prevents inheritance
};

struct UnitQuad
{
    static M33f getLocalMatrix(const M44f & localToWorld, V3f & center)
    {
        center = frameOrigin(localToWorld);
        return frame(0.5f * frameX(localToWorld), 0.5f * frameY(localToWorld), frameZ(localToWorld));
    }

    static V3f sampleSolidAngle(float u1, float u2, const V3f & org, float & solidAnglePdf, LightSide side);

    static float solidAngleSamplingPDF(const V3f & org, const V3f & dir, LightSide side);

    static V3f samplePoint(float u1, float u2, V3f & normal);

    static float intersect(const V3f & org, const V3f & dir, V3f * pNormal = nullptr, V2f * pUV = nullptr);
};

struct UnitDisk
{
    static float getLocalRadius()
    {
        return 0.5f;
    }

    static M33f getLocalMatrix(const M44f & localToWorld, V3f & center)
    {
        center = frameOrigin(localToWorld);
        return frame(frameX(localToWorld) * getLocalRadius(), frameY(localToWorld) * getLocalRadius(), frameZ(localToWorld));
    }

    static V3f sampleSolidAngle(float u1, float u2, const V3f & org, float & solidAnglePdf, LightSide side);

    static float solidAngleSamplingPDF(const V3f & org, const V3f & dir, LightSide side);

    static V3f samplePoint(float u1, float u2, V3f & normal);

    static float intersect(const V3f & org, const V3f & dir, V3f * pNormal = nullptr, V2f * pUV = nullptr);
};

struct UnitSphere
{
    static float getLocalRadius()
    {
        return 0.5f;
    }

    static M33f getLocalMatrix(const M44f & localToWorld, V3f & center)
    {
        center = frameOrigin(localToWorld);
        return frame(frameX(localToWorld) * getLocalRadius(), frameY(localToWorld) * getLocalRadius(), frameZ(localToWorld) * getLocalRadius());
    }

    static V3f sampleSolidAngle(float u1, float u2, const V3f & org, float & solidAnglePdf, LightSide side);

    static float solidAngleSamplingPDF(const V3f & org, const V3f & dir, LightSide side);

    static V3f samplePoint(float u1, float u2, V3f & normal);

    static float intersect(const V3f & org, const V3f & dir, V3f * pNormal = nullptr, V2f * pUV = nullptr);
};

struct UnitCylinder
{
    static float getLocalRadius()
    {
        return 0.5f;
    }

    static M33f getLocalMatrix(const M44f & localToWorld, V3f & center)
    {
        center = frameOrigin(localToWorld);
        return frame(frameY(localToWorld) * getLocalRadius(), frameZ(localToWorld) * getLocalRadius(), frameX(localToWorld));
    }

    static V3f sampleSolidAngle(float u1, float u2, const V3f & org, float & solidAnglePdf, LightSide side);

    static float solidAngleSamplingPDF(const V3f & org, const V3f & dir, LightSide side);

    static V3f samplePoint(float u1, float u2, V3f & normal);

    static float intersect(const V3f & org, const V3f & dir, V3f * pNormal = nullptr, V2f * pUV = nullptr);
};

#define SHN_USE_UNITQUAD_AREALIGHT
#ifdef SHN_USE_UNITQUAD_AREALIGHT
typedef AreaLight<TransformedShape<UnitQuad>>
    RectAreaLight; // Possible to use this one, but the sampling strategy is not the same as directy sampling the rectangle area and projecting on ray origin
#else
typedef AreaLight<RectShape> RectAreaLight;
#endif
typedef AreaLight<TransformedShape<UnitDisk>> DiskAreaLight;
typedef AreaLight<TransformedShape<UnitSphere>> SphereAreaLight;
typedef AreaLight<TransformedShape<UnitCylinder>> CylinderAreaLight;
} // namespace shn