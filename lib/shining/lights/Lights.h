#pragma once

#include "AreaLight.h"
#include "Attributes.h"
#include "DirectionalLight.h"
#include "EnvMapLight.h"
#include "GeometryLight.h"
#include "Light.h"
#include "OslHeaders.h"
#include "PhysicalSky.h"
#include "PointLight.h"
#include "SpotLight.h"

namespace shn
{
class Lights
{
public:
    Lights() = default;

    // Copy not allowed:
    Lights & operator=(const Lights &) = delete;
    Lights(const Lights &) = delete;

    OslRenderer::Status genLights(OslRenderer::Id * lightIds, int32_t count);
    OslRenderer::Status setLightAttribute(OslRenderer::Id lightId, const char * data, int32_t dataByteSize, OSL::ustring attrName, const char * attrType);
    OslRenderer::Status lightIdsByName(const char * name, OslRenderer::Id * ids, int32_t count) const;
    int32_t countLightIdsByName(const char * name) const;
    OslRenderer::Status commitLight(OslRenderer::Id lightId, const ShadingContext & lsCtx);
    OslRenderer::Status commitLights(); // Must be called after all lights have been commited
    OslRenderer::Status clearLights();

    // Only call this method after calling commitLights()
    void buildLightLinking();
    bool isLit(LightLinkingMode mode, int32_t linkId, int32_t instanceId) const;
    // check function to know if an instance is linked to a GeometryLight
    // Doesn't use m_emissiveInstances because of architecture issues
    // #todo: unify with m_emissiveInstance by changing GeometryLight system (and mesh area cdf computing) & use id instead of instance name
    bool isEmissiveInstance(const char * instanceName) const;

    // Populate m_emissiveInstances bool array - need that intern SceneImpl pointer to be set by setScene()
    void listEmissiveInstance();

    inline const GeometryLight * getLinkedGeometryLight(const Scene::Id instanceId) const
    {
        assert(instanceId < m_emissiveInstances.size());
        if (m_emissiveInstances[instanceId] >= 0)
        {
            return &(m_geometryLights[m_emissiveInstances[instanceId]]);
        }
        return nullptr;
    }

    int32_t visibleLightCount(LightType type) const
    {
        return m_visibleLightCounts[type];
    }
    int32_t visibleLightCount() const; // Only visible lights
    int32_t allLightCount() const; // All lights

    const std::vector<Light const *> & visibleLights() const
    {
        return m_visibleLights;
    }

    const Light * const * visibleLights(LightType type) const;

    // Physical Sky relative functions
    PhysicalSky * createPhysicalSky() const
    {
        m_physicalSkyEngine.reset(new PhysicalSky());
        return m_physicalSkyEngine.get();
    }
    inline PhysicalSky * getPhysicalSky() const
    {
        return m_physicalSkyEngine.get();
    };

    // Scene pointer setter/getter
    inline void setScene(const SceneImpl * scene)
    {
        m_scene = scene;
    }
    inline const SceneImpl * scene() const
    {
        return m_scene;
    }

    void setAreaLightVersion(int32_t version)
    {
        for (size_t i = 0; i < areaLightShapeCount(); ++i)
            lightVersions[LightType_AreaRect + i] = version;
    }

    int32_t getLightVersion(LightType type) const
    {
        return lightVersions[type];
    }

    const Attributes & getLightAttributes(size_t lightId) const
    {
        return m_lightAttributes[lightId];
    }

private:
    Light * getLight(OslRenderer::Id lightId);

    // Get the index of a light in its corresponding vector, if it exists.
    // Otherwise returns -1
    int32_t getLightIndex(OslRenderer::Id lightId, LightType type)
    {
        assert(lightId < m_allLights.size());
        return (m_allLights[lightId].first != type) ? -1 : m_allLights[lightId].second;
    }

    template<typename VectorType>
    int32_t createLight(OslRenderer::Id lightId, LightType type, VectorType & lightVector)
    {
        assert(lightId < m_allLights.size());
        assert(getLightIndex(lightId, type) < 0); // A light should not be created if it exists already

        int32_t index = lightVector.size();
        lightVector.emplace_back();
        lightVector[index].lights = this;
        m_allLights[lightId] = std::make_pair(type, index);

        return index;
    }

    template<typename VectorType>
    void updateLight(OslRenderer::Id lightId, LightType type, VectorType & lightVector, const ShadingContext & lsCtx)
    {
        int32_t index = getLightIndex(lightId, type);
        if (index < 0)
            index = createLight(lightId, type, lightVector); // create light if necessary
        lightVector[index].update(m_lightAttributes[lightId], lsCtx);
    }

    void gatherVisibleLights();

    std::vector<Attributes> m_lightAttributes;
    std::vector<std::pair<LightType, int32_t>> m_allLights; // All lights type and index in their corresponding array. m_allLights[i] corresponds to m_lightAttributes[i].

    std::vector<Light const *> m_visibleLights; // Only visible lights, sorted by type, and filled by commitLights()
    int32_t m_visibleLightCounts[LightType_Count] = {}; // Number of visible lights for each type of light

    std::vector<SpotLight> m_spotLights;
    std::vector<DirectionalLight> m_directionalLights;
    std::vector<PointLight> m_pointLights;
    std::vector<RectAreaLight> m_rectAreaLights;
    std::vector<DiskAreaLight> m_diskAreaLights;
    std::vector<CylinderAreaLight> m_cylinderAreaLights;
    std::vector<SphereAreaLight> m_sphereAreaLights;
    std::vector<EnvMapLight> m_envMapLights;
    std::vector<GeometryLight> m_geometryLights;

    struct LightLinking
    {
        std::vector<int32_t> linkedInstanceIds;
        std::vector<int32_t> offsets;
        std::vector<int32_t> instanceCounts;
        int32_t lightCount;
    };
    LightLinking m_lightLinking;
    mutable std::unique_ptr<PhysicalSky> m_physicalSkyEngine;

    // array of scene instance count size which list what instance is linked to which GeometryLight - used to cut emission on these instances and override instance material
    std::vector<int32_t> m_emissiveInstances;

    int32_t lightVersions[LightType_Count] = {};

    const SceneImpl * m_scene = nullptr;
};

} // namespace shn