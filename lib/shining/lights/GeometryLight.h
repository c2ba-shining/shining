#pragma once

#include "Light.h"
#include "Scene.h"

namespace shn
{
struct GeometryLight: public Light
{
    LightSamplingResult sample(const OSL::ShaderGlobals & globals, float uDir1, float uDir2, float uPrimitive) const override;
    LightSamplingResult eval(const V3f & dir, const OSL::ShaderGlobals & globals) const override;
    Color3 evalColor(const OSL::ShaderGlobals & globals, const LightSamplingResult & samplingResult, SamplingStrategy strat, const ShadingContext & lsCtx) const override;
    Color3 evalEmissionOnGeometry(OSL::ShaderGlobals & globas, const ShadingContext & lsCtx) const;
    bool isDelta() const override
    {
        return samplingStrategy == SamplingStrategy::Light; // If only light sampling strategy is used, the light is considered as a delta light to ignore the bsdf sampling strategy
    }

    bool needImportanceSampling() const override
    {
        // The light is importance sampled only if the asked strategy is light or MIS
        return samplingStrategy == SamplingStrategy::Light || samplingStrategy == SamplingStrategy::MisLightMaterial;
    }

    const char * instanceName = "";
    Scene::Id instanceId = -1;
    float geometryShadowBias = 0.f;
    bool overrideMaterial = false;
    bool normalizeArea = false;
    SamplingStrategy samplingStrategy = SamplingStrategy::MisLightMaterial;

private:
    void doUpdate(const Attributes & attr, const ShadingContext & lsCtx) override;

    json doSerialize() const override;
};

} // namespace shn