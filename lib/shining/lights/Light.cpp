#include "Light.h"
#include "Attributes.h"
#include "StringUtils.h"

namespace o = OSL;

namespace shn
{
ustring lightTypeToString(LightType type)
{
    static const auto sSeparator = "_";
    static const std::string sAreaLightStrings[] = { std::string(SHN_LIGHT_TYPE_AREA) + sSeparator + std::string(SHN_LIGHT_SHAPE_RECT),
                                                     std::string(SHN_LIGHT_TYPE_AREA) + sSeparator + std::string(SHN_LIGHT_SHAPE_DISK),
                                                     std::string(SHN_LIGHT_TYPE_AREA) + sSeparator + std::string(SHN_LIGHT_SHAPE_CYLINDER),
                                                     std::string(SHN_LIGHT_TYPE_AREA) + sSeparator + std::string(SHN_LIGHT_SHAPE_SPHERE) };
    // Order matches LightType enum
    static const ustring sStrings[] = {
        ustring("none"),
        ustring(SHN_LIGHT_TYPE_POINT),
        ustring(SHN_LIGHT_TYPE_DIRECTIONAL),
        ustring(SHN_LIGHT_TYPE_SPOT),
        ustring(sAreaLightStrings[0]),
        ustring(sAreaLightStrings[1]),
        ustring(sAreaLightStrings[2]),
        ustring(sAreaLightStrings[3]),
        ustring(SHN_LIGHT_TYPE_ENVMAP),
        ustring(SHN_LIGHT_TYPE_GEOMETRY),
    };
    return sStrings[type];
}

static const ustring LIGHT_ATTR_NAMES[] = {
    ustring("lightId"),
    ustring(SHN_ATTR_LIGHT_COLORSHADINGTREEID),
    ustring(SHN_ATTR_LIGHT_TYPE),
    ustring(SHN_ATTR_LIGHT_NAME),
    ustring(SHN_ATTR_LIGHT_LOCALTOWORLD),
    ustring(SHN_ATTR_LIGHT_INTENSITY),
    ustring(SHN_ATTR_LIGHT_RADIUS),
    ustring(SHN_ATTR_LIGHT_ANGLE),
    ustring(SHN_ATTR_LIGHT_PENUMBRAANGLE),
    ustring(SHN_ATTR_LIGHT_DROPOFF),
    ustring(SHN_ATTR_LIGHT_COLOR),
    ustring(SHN_ATTR_LIGHT_SHADOWCOLOR),
    ustring(SHN_ATTR_LIGHT_RAYTRACEDSHADOWBIAS),
    ustring(SHN_ATTR_LIGHT_CASTSHADOWS),
    ustring(SHN_ATTR_LIGHT_DECAY),
    ustring(SHN_ATTR_LIGHT_EMITCOMPONENT),
    ustring(SHN_ATTR_LIGHT_IMPORTANCESAMPLING),
    ustring(SHN_ATTR_LIGHT_IMPORTANCESAMPLINGRESOLUTION),
    ustring(SHN_ATTR_LIGHT_BACKGROUND),
    ustring(SHN_ATTR_LIGHT_LINKINGMODE),
    ustring(SHN_ATTR_LIGHT_LINKINGSET),
    ustring(SHN_ATTR_LIGHT_SHAPE),
    ustring(SHN_ATTR_LIGHT_SAMPLINGSTRATEGY),
    ustring(SHN_ATTR_LIGHT_SIDE),
    ustring(SHN_ATTR_LIGHT_INSTANCENAME),
    ustring(SHN_ATTR_LIGHT_GEOMETRYSHADOWBIAS),
    ustring(SHN_ATTR_LIGHT_OVERRIDEMATERIAL),
    ustring(SHN_ATTR_LIGHT_NORMALIZEAREA),
    ustring(SHN_ATTR_LIGHT_NEAR),
    ustring(SHN_ATTR_LIGHT_FAR),
};

const OSL::ustring & getLightAttributeStringName(LightAttribute attr)
{
    return LIGHT_ATTR_NAMES[attr];
}

void Light::update(const Attributes & attr, const ShadingContext & lsCtx)
{
    id = attr.get<OslRenderer::Id>(getLightAttributeStringName(L_ATTR_ID), -1);
    type = attr.get<LightType>(getLightAttributeStringName(L_ATTR_TYPE), LightType_None);
    name = attr.getString(getLightAttributeStringName(L_ATTR_NAME));
    colorShadingTreeId = attr.get<OslRenderer::Id>(getLightAttributeStringName(L_ATTR_COLORSHADINGTREEID), -1);
    linkingMode = attr.get<LightLinkingMode>(getLightAttributeStringName(L_ATTR_LINKINGMODE), LightLinking_None);
    if (linkingMode == LightLinking_None)
        linkId = -1;

    M44f idMat;
    idMat.makeIdentity();
    localToWorld = attr.get<M44f>(getLightAttributeStringName(L_ATTR_LOCALTOWORLD), idMat);
    intensity = attr.get<float>(getLightAttributeStringName(L_ATTR_INTENSITY), 0.f);
    color = attr.get<Color3>(getLightAttributeStringName(L_ATTR_COLOR), Color3(0.f));
    colorHeuristic = color;
    shadowColor = attr.get<Color3>(getLightAttributeStringName(L_ATTR_SHADOWCOLOR), Color3(0.f));
    rayTracedShadowBias = attr.get<float>(getLightAttributeStringName(L_ATTR_RAYTRACEDSHADOWBIAS), 0.f);
    castsShadows = (attr.get<int32_t>(getLightAttributeStringName(L_ATTR_CASTSHADOWS), 1) == 1);

    nearDistRange = attr.get<V2f>(getLightAttributeStringName(L_ATTR_NEAR), V2f(0.f, 0.f));
    nearDistRange.y = nearDistRange.y > 0.f ? 1.f / nearDistRange.y : 0.f;
    farDistRange = attr.get<V2f>(getLightAttributeStringName(L_ATTR_FAR), V2f(-1.f, 0.f));
    farDistRange.x -= max(0.f, farDistRange.y);
    farDistRange.y = farDistRange.y > 0.f ? 1.f / farDistRange.y : 0.f;

    emitComponent = attr.get<EmissionBits>(getLightAttributeStringName(L_ATTR_EMITCOMPONENT), EmissionBits::None);

    visible = true;
    if (intensity == 0.f || (isBlack(color) && colorShadingTreeId < 0))
        visible = false;

    if (visible)
        doUpdate(attr, lsCtx);
}

float Light::computeRangeAttenuation(float d) const
{
    float rangeAttenuation = 1.f;
    if (nearDistRange.x >= 0.f)
    {
        if (nearDistRange.y)
            rangeAttenuation *= clamp((d - nearDistRange.x) * nearDistRange.y, 0.f, 1.f);
        else
            rangeAttenuation *= clamp(sign(d - nearDistRange.x), 0.f, 1.f);
    }

    if (farDistRange.x >= 0.f)
    {
        if (farDistRange.y)
            rangeAttenuation *= clamp(1.f - (d - farDistRange.x) * farDistRange.y, 0.f, 1.f);
        else
            rangeAttenuation *= clamp(sign(farDistRange.x - d), 0.f, 1.f);
    }
    
    return rangeAttenuation;
}

json Light::serialize() const
{
    auto map = doSerialize();

    map["type"] = lightTypeToString(type).c_str();
    map["id"] = id;
    map["name"] = name;
    map["colorShadingTreeId"] = colorShadingTreeId;
    map["linkingMode"] = linkingMode == LightLinking_None ? "none" : linkingMode == LightLinking_Inclusion ? "include" : "exclude";
    map["localToWorld"] = localToWorld;
    map["intensity"] = intensity;
    map["color"] = color;
    map["colorHeuristic"] = colorHeuristic;
    map["shadowColor"] = shadowColor;
    map["rayTracedShadowBias"] = rayTracedShadowBias;
    map["castsShadows"] = castsShadows;
    map["emitComponent"] = to_string(emitComponent);
    map["visible"] = visible;

    return map;
}
} // namespace shn