#include "Lights.h"
#include "SceneImpl.h"
#include "Shining_p.h"
#include "sampling/PatternSamplers.h"
#include "sampling/ShapeSamplers.h"
#include "shading/CompositeBxDF.h"
#include "shading/CustomClosure.h"
#include "shading/ShiningRendererServices.h"

#define _USE_MATH_DEFINES
#include <math.h>

namespace shn
{
OslRenderer::Status Lights::genLights(OslRenderer::Id * lightIds, int32_t count)
{
    assert(m_lightAttributes.size() == m_allLights.size());
    int32_t firstId = m_lightAttributes.size();
    m_lightAttributes.resize(m_lightAttributes.size() + count);
    m_allLights.resize(m_allLights.size() + count);
    for (int32_t i = 0; i < count; ++i)
    {
        int32_t id = firstId + i;
        lightIds[i] = id;
        m_lightAttributes[id].set<OslRenderer::Id>(getLightAttributeStringName(L_ATTR_ID), ATTR_TYPE_NAMES[ATTR_TYPE_INT], id);
        m_lightAttributes[id].set<OslRenderer::Id>(getLightAttributeStringName(L_ATTR_COLORSHADINGTREEID), ATTR_TYPE_NAMES[ATTR_TYPE_INT], -1);
        m_lightAttributes[id].set<LightType>(getLightAttributeStringName(L_ATTR_TYPE), ATTR_TYPE_NAMES[ATTR_TYPE_INT], LightType_None);

        m_allLights[id] = std::make_pair(LightType_None, -1);
    }

    return 0;
}

OslRenderer::Id Lights::setLightAttribute(OslRenderer::Id lightId, const char * data, int32_t dataByteSize, ustring attrName, const char * attrType)
{
    assert(lightId < m_lightAttributes.size());
    Attributes & attr = m_lightAttributes[lightId];

    if (ATTR_TYPE_NAMES[ATTR_TYPE_INT] == attrType)
    {
        const int32_t * convertedData = reinterpret_cast<const int32_t *>(data);
        int32_t count = dataByteSize / sizeof(int32_t);
        attr.set<int32_t>(attrName, ATTR_TYPE_NAMES[ATTR_TYPE_INT], convertedData, count, 1);
    }
    else if (ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT] == attrType)
    {
        const float * convertedData = reinterpret_cast<const float *>(data);
        int32_t count = dataByteSize / sizeof(float);
        attr.set<float>(attrName, ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], convertedData, count, 1);
    }
    else if (ATTR_TYPE_NAMES[ATTR_TYPE_STRING] == attrType)
    {
        // Convert stringified attributes to corresponding enums:
        if (attrName == getLightAttributeStringName(L_ATTR_SHAPE))
        {
            static const std::unordered_map<ustring, AreaLightShape> typeStrToEnum{ { ustring(SHN_LIGHT_SHAPE_RECT), AreaLightShape::Rect },
                                                                                    { ustring(SHN_LIGHT_SHAPE_DISK), AreaLightShape::Disk },
                                                                                    { ustring(SHN_LIGHT_SHAPE_CYLINDER), AreaLightShape::Cylinder },
                                                                                    { ustring(SHN_LIGHT_SHAPE_SPHERE), AreaLightShape::Sphere } };

            const auto it = typeStrToEnum.find(ustring(data));
            assert(it != end(typeStrToEnum));

            const AreaLightShape shape = [&]() {
                if (it == end(typeStrToEnum))
                    return AreaLightShape::Rect;
                return (*it).second;
            }();

            attr.set<AreaLightShape>(getLightAttributeStringName(L_ATTR_SHAPE), ATTR_TYPE_NAMES[ATTR_TYPE_INT], shape);
        }
        else if (attrName == getLightAttributeStringName(L_ATTR_SAMPLINGSTRATEGY))
        {
            const ustring str{ data };
            const SamplingStrategy strat = [&]() {
                if (str == SHN_LIGHT_SAMPLINGSTRAT_BOTH)
                    return SamplingStrategy::MisLightMaterial;
                if (str == SHN_LIGHT_SAMPLINGSTRAT_LIGHT)
                    return SamplingStrategy::Light;
                else if (str == SHN_LIGHT_SAMPLINGSTRAT_BXDF)
                    return SamplingStrategy::Material;

                assert(false);
            }();

            attr.set<SamplingStrategy>(getLightAttributeStringName(L_ATTR_SAMPLINGSTRATEGY), ATTR_TYPE_NAMES[ATTR_TYPE_INT], strat);
        }
        else if (attrName == getLightAttributeStringName(L_ATTR_SIDE))
        {
            const ustring str{ data };
            const LightSide side = [&]() {
                if (str == SHN_LIGHT_SIDE_FRONT)
                    return LightSide::Front;
                if (str == SHN_LIGHT_SIDE_BACK)
                    return LightSide::Back;
                else if (str == SHN_LIGHT_SIDE_DOUBLE)
                    return LightSide::Double;

                assert(false);
            }();

            attr.set<LightSide>(getLightAttributeStringName(L_ATTR_SIDE), ATTR_TYPE_NAMES[ATTR_TYPE_INT], side);
        }
        else if (attrName == getLightAttributeStringName(L_ATTR_TYPE))
        {
            LightType lType = LightType_None;

            static const std::unordered_map<ustring, LightType> typeStrToEnum{
                { ustring(SHN_LIGHT_TYPE_POINT), LightType_Point },   { ustring(SHN_LIGHT_TYPE_DIRECTIONAL), LightType_Directional }, { ustring(SHN_LIGHT_TYPE_SPOT), LightType_Spot },
                { ustring(SHN_LIGHT_TYPE_AREA), LightType_AreaRect }, { ustring(SHN_LIGHT_TYPE_ENVMAP), LightType_EnvMap },           { ustring(SHN_LIGHT_TYPE_GEOMETRY), LightType_Geometry }
            };

            const auto it = typeStrToEnum.find(ustring(data));
            if (it != end(typeStrToEnum))
                lType = (*it).second;

            attr.set<LightType>(getLightAttributeStringName(L_ATTR_TYPE), ATTR_TYPE_NAMES[ATTR_TYPE_INT], lType);
        }
        else
        {
            // A true string as attribute (eg. name)
            attr.setString(attrName, data);
        }
    }
    else
    {
        return -1;
    }

    return 0;
}

int32_t Lights::countLightIdsByName(const char * name) const
{
    int32_t countLightData = static_cast<int32_t>(m_lightAttributes.size());
    int32_t matches = 0;
    for (int32_t i = 0; i < countLightData; ++i)
    {
        const Attributes & attr = m_lightAttributes[i];
        const char * lightName = attr.getString(getLightAttributeStringName(L_ATTR_NAME));
        if (lightName && strcmp(lightName, name) == 0)
        {
            ++matches;
        }
    }
    return matches;
}

OslRenderer::Status Lights::lightIdsByName(const char * name, OslRenderer::Id * ids, int32_t count) const
{
    int32_t countLightData = static_cast<int32_t>(m_lightAttributes.size());
    int32_t matches = 0;
    for (int32_t i = 0; i < countLightData; ++i)
    {
        const Attributes & attr = m_lightAttributes[i];
        const char * lightName = attr.getString(getLightAttributeStringName(L_ATTR_NAME));
        if (lightName && strcmp(lightName, name) == 0)
        {
            OslRenderer::Id crtId = attr.get<OslRenderer::Id>(getLightAttributeStringName(L_ATTR_ID), -1);
            if (crtId >= 0)
            {
                ids[matches] = crtId;
                ++matches;
                if (matches >= count)
                    break;
            }
        }
    }
    return matches;
}

int32_t Lights::visibleLightCount() const
{
    return m_visibleLights.size();
}

int32_t Lights::allLightCount() const
{
    return m_allLights.size();
}

const Light * const * Lights::visibleLights(LightType type) const
{
    int32_t lightCount = visibleLightCount(type);
    if (lightCount == 0)
        return nullptr;

    for (int32_t i = 0; i < m_visibleLights.size(); ++i)
    {
        if (m_visibleLights[i]->type == type)
            return &m_visibleLights[i];
    }
}

OslRenderer::Status Lights::commitLight(OslRenderer::Id lightId, const ShadingContext & lsCtx)
{
    assert(lightId < m_allLights.size());

    LightType type = m_lightAttributes[lightId].get<LightType>(getLightAttributeStringName(L_ATTR_TYPE), LightType_None);

    switch (type)
    {
    case LightType_Point:
        updateLight(lightId, type, m_pointLights, lsCtx);
        break;

    case LightType_Directional:
        updateLight(lightId, type, m_directionalLights, lsCtx);
        break;

    case LightType_Spot:
        updateLight(lightId, type, m_spotLights, lsCtx);
        break;

    case LightType_AreaRect:
    case LightType_AreaDisk:
    case LightType_AreaCylinder:
    case LightType_AreaSphere:
    {
        AreaLightShape shape = m_lightAttributes[lightId].get<AreaLightShape>(getLightAttributeStringName(L_ATTR_SHAPE), AreaLightShape::Rect);
        switch (shape)
        {
        default:
            return -1;
        case AreaLightShape::Rect:
            m_lightAttributes[lightId].set<LightType>(getLightAttributeStringName(L_ATTR_TYPE), ATTR_TYPE_NAMES[ATTR_TYPE_INT], LightType_AreaRect);
            updateLight(lightId, LightType_AreaRect, m_rectAreaLights, lsCtx);
            break;
        case AreaLightShape::Disk:
            m_lightAttributes[lightId].set<LightType>(getLightAttributeStringName(L_ATTR_TYPE), ATTR_TYPE_NAMES[ATTR_TYPE_INT], LightType_AreaDisk);
            updateLight(lightId, LightType_AreaDisk, m_diskAreaLights, lsCtx);
            break;
        case AreaLightShape::Cylinder:
            m_lightAttributes[lightId].set<LightType>(getLightAttributeStringName(L_ATTR_TYPE), ATTR_TYPE_NAMES[ATTR_TYPE_INT], LightType_AreaCylinder);
            updateLight(lightId, LightType_AreaCylinder, m_cylinderAreaLights, lsCtx);
            break;
        case AreaLightShape::Sphere:
            m_lightAttributes[lightId].set<LightType>(getLightAttributeStringName(L_ATTR_TYPE), ATTR_TYPE_NAMES[ATTR_TYPE_INT], LightType_AreaSphere);
            updateLight(lightId, LightType_AreaSphere, m_sphereAreaLights, lsCtx);
            break;
        }
    }
    break;

    case LightType_EnvMap:
        updateLight(lightId, type, m_envMapLights, lsCtx);
        break;

    case LightType_Geometry:
        updateLight(lightId, type, m_geometryLights, lsCtx);
        break;

    default:
        return -1;
    }

    return 0;
}

Light * Lights::getLight(OslRenderer::Id lightId)
{
    LightType type = m_allLights[lightId].first;
    int32_t index = m_allLights[lightId].second;

    if (index < 0)
        return nullptr;

    switch (type)
    {
    case LightType_Point:
        return &m_pointLights[index];
    case LightType_Directional:
        return &m_directionalLights[index];
    case LightType_Spot:
        return &m_spotLights[index];
    case LightType_AreaRect:
        return &m_rectAreaLights[index];
    case LightType_AreaDisk:
        return &m_diskAreaLights[index];
    case LightType_AreaCylinder:
        return &m_cylinderAreaLights[index];
    case LightType_AreaSphere:
        return &m_sphereAreaLights[index];
    case LightType_EnvMap:
        return &m_envMapLights[index];
    case LightType_Geometry:
        return &m_geometryLights[index];
    default:
        assert(false);
    }

    return nullptr;
}

void Lights::gatherVisibleLights()
{
    // Initialize offsets
    for (int32_t i = 0; i < LightType_Count; ++i)
        m_visibleLightCounts[i] = 0;

    m_visibleLights.clear();

    for (int32_t i = 0; i < m_allLights.size(); ++i)
    {
        Light * pLight = getLight(i);

        if (pLight)
        {
            if (pLight->visible)
            {
                ++m_visibleLightCounts[pLight->type];
                m_visibleLights.emplace_back(pLight);
            }
        }
    }

    // Sort vector of visible light pointers according to type then id
    std::sort(begin(m_visibleLights), end(m_visibleLights), [&](const Light * lhs, const Light * rhs) { return lhs->type < rhs->type || (lhs->type == rhs->type && lhs->id < rhs->id); });
}

OslRenderer::Status Lights::commitLights()
{
    gatherVisibleLights();
    buildLightLinking();

    return 0;
}

OslRenderer::Status Lights::clearLights() // outdated
{
    m_lightAttributes.clear();
    m_allLights.clear();
    return commitLights(); // commits called to clean typed lights vectors
}

void Lights::buildLightLinking()
{
    m_lightLinking.linkedInstanceIds.clear();
    m_lightLinking.offsets.clear();
    m_lightLinking.instanceCounts.clear();
    m_lightLinking.lightCount = 0;

    int32_t offset = 0;
    int32_t id = 0;
    for (int32_t i = 0; i < m_allLights.size(); ++i)
    {
        Light * pLight = getLight(i);
        if (pLight->linkingMode != LightLinking_None)
        {
            int32_t count = 0;
            int32_t countComponents = 0;
            const int32_t * instanceIds = static_cast<const int32_t *>(m_lightAttributes[i].getPtr(getLightAttributeStringName(L_ATTR_LINKINGSET), 0.f, &count, &countComponents));
            if (!instanceIds)
                continue;

            pLight->linkId = id;
            m_lightLinking.offsets.push_back(offset);
            m_lightLinking.instanceCounts.push_back(count);
            m_lightLinking.linkedInstanceIds.insert(m_lightLinking.linkedInstanceIds.end(), instanceIds, instanceIds + count);
            // sort the added ids for the future binary_search
            std::sort(m_lightLinking.linkedInstanceIds.end() - count, m_lightLinking.linkedInstanceIds.end());

            offset += count;
            ++id;
        }
    }
}

bool Lights::isLit(LightLinkingMode mode, int32_t linkId, int32_t instanceId) const
{
    if (mode == LightLinking_None || linkId < 0 || instanceId == Ray::INVALID_ID) // instanceId == Ray::INVALID_ID is for light atmosphere shader
        return true;

    bool isLinked = false;
    int32_t offset = m_lightLinking.offsets[linkId];
    int32_t instCount = m_lightLinking.instanceCounts[linkId];
    if (instCount > 0)
    {
        std::vector<int32_t>::const_iterator begin = m_lightLinking.linkedInstanceIds.begin() + offset;
        if (std::binary_search(begin, begin + instCount, instanceId))
            isLinked = true;
    }

    if (isLinked && mode == LightLinking_Inclusion || !isLinked && mode == LightLinking_Exclusion)
        return true;

    return false;
}

bool Lights::isEmissiveInstance(const char * instanceName) const
{
    for (const auto & l : m_geometryLights)
    {
        if (strcmp(l.instanceName, instanceName) == 0)
            return true;
    }

    return false;
}

void Lights::listEmissiveInstance()
{
    size_t instanceCount = m_scene->countInstanceIds();
    m_emissiveInstances.clear();
    m_emissiveInstances.resize(instanceCount, -1);
    for (size_t i = 0; i < m_geometryLights.size(); ++i)
    {
        GeometryLight & geo = m_geometryLights[i];

        // convert instanceName to instanceId for each geometry lights
        if (strcmp(geo.instanceName, "") != 0)
        {
            // Because a geo light can only be attached to one instance, we took the first we met with the given name
            // #todo: break the limitation
            m_scene->getInstanceIdsFromName(OSL::ustring(geo.instanceName), &geo.instanceId, 1);

            // hide lights which have hidden instance
            bool primVisibility = m_scene->getInstanceVisibility(geo.instanceId, Scene::INSTANCE_VISIBILITY_PRIMARY);
            bool secoVisibility = m_scene->getInstanceVisibility(geo.instanceId, Scene::INSTANCE_VISIBILITY_SECONDARY);
            if (!primVisibility && !secoVisibility)
            {
                geo.visible = false;
            }
            else
            {
                geo.visible = true;
            }

            // update the emissive instance list
            if (geo.instanceId >= 0)
            {
                assert(geo.instanceId < instanceCount);
                m_emissiveInstances[geo.instanceId] = i;
            }
        }
    }

    gatherVisibleLights();
}

} // namespace shn