#include "LightingImportanceCache.h"
#include "lights/Light.h"
#include "lights/Lights.h"

namespace shn
{

const char * to_string(LightingImportanceCache::DistributionNames::Enum name)
{
    assert(name < LightingImportanceCache::DistributionNames::Count);
    static_assert(
        LightingImportanceCache::DistributionNames::F == 0 && LightingImportanceCache::DistributionNames::U == 1 && LightingImportanceCache::DistributionNames::T == 2 &&
            LightingImportanceCache::DistributionNames::C == 3,
        "Bad order for LightingImportanceCache::DistributionNames::Enum");
    const char * map[] = { "F", "U", "T", "C" };
    return map[name];
}

json LightingImportanceCache::serialize() const
{
    json map;

    map["pixelCount"] = m_nPixelCount;
    map["distribCount"] = m_nDistribCount;
    map["lightCount"] = m_nLightCount;
    map["lightIsImportanceCached"] = m_LightIsImportanceCachedFlags;
    map["importanceCachedLightCount"] = m_nImportanceCachedLightCount;
    map["conservativeProbability"] = m_fConservativeProbability;
    map["perPixelContribs_full"] = {};
    map["perPixelContribs_unoccluded"] = {};
    map["perPixelContribs_sampleCount"] = {};
    auto & perPixelContribs_full = map["perPixelContribs_full"];
    auto & perPixelContribs_unoccluded = map["perPixelContribs_unoccluded"];
    auto & perPixelContribs_sampleCount = map["perPixelContribs_sampleCount"];
    for (const auto & contrib: m_PerPixelContribs)
    {
        perPixelContribs_full.emplace_back(contrib.full);
        perPixelContribs_unoccluded.emplace_back(contrib.unoccluded);
    }
    map["lightContribsOnTile_contrib"] = {};
    map["lightContribsOnTile_sampleCount"] = {};
    auto & lightContribsOnTile_contrib = map["lightContribsOnTile_contrib"];
    auto & lightContribsOnTile_sampleCount = map["lightContribsOnTile_sampleCount"];
    for (const auto & contrib: m_LightContribsOnTile)
    {
        lightContribsOnTile_contrib.emplace_back(contrib);
    }
    map["weights"] = m_Weights;
    map["weightDistribNames"] = {};
    for (auto distribName: m_WeightDistribNames)
    {
        map["weightDistribNames"].emplace_back(to_string(distribName));
    }
    map["isUsable"] = m_IsUsable;
    map["probabilityAlpha"] = m_ProbabilityAlpha;
    map["probabilityScalingFactor"] = m_ProbabilityScalingFactor;
    map["baseProbability"] = m_BaseProbability;

    map["directLightingContribSums"] = { { "F", m_DirectLightingContribSums[LightingImportanceCache::DistributionNames::F] },
                                         { "U", m_DirectLightingContribSums[LightingImportanceCache::DistributionNames::U] },
                                         { "T", m_DirectLightingContribSums[LightingImportanceCache::DistributionNames::T] },
                                         { "C", m_DirectLightingContribSums[LightingImportanceCache::DistributionNames::C] } };
    const auto directProbas = getDistribProbabilities(0);
    map["directProbabilities"] = { { "F", directProbas[LightingImportanceCache::DistributionNames::F] },
                                   { "U", directProbas[LightingImportanceCache::DistributionNames::U] },
                                   { "T", directProbas[LightingImportanceCache::DistributionNames::T] },
                                   { "C", directProbas[LightingImportanceCache::DistributionNames::C] } };
    map["indirectLightingContribSums"] = { { "F", m_IndirectLightingContribSums[LightingImportanceCache::DistributionNames::F] },
                                           { "U", m_IndirectLightingContribSums[LightingImportanceCache::DistributionNames::U] },
                                           { "T", m_IndirectLightingContribSums[LightingImportanceCache::DistributionNames::T] },
                                           { "C", m_IndirectLightingContribSums[LightingImportanceCache::DistributionNames::C] } };
    const auto indirectProbas = getDistribProbabilities(m_nDistribCount - 1);
    map["indirectProbabilities"] = { { "F", indirectProbas[LightingImportanceCache::DistributionNames::F] },
                                     { "U", indirectProbas[LightingImportanceCache::DistributionNames::U] },
                                     { "T", indirectProbas[LightingImportanceCache::DistributionNames::T] },
                                     { "C", indirectProbas[LightingImportanceCache::DistributionNames::C] } };

    return map;
}

bool LightingImportanceCache::isImportanceCached(const Light & light) const
{
    return light.castsShadows && light.needImportanceSampling();
}

void LightingImportanceCache::init(const Lights & lights, size_t pixelCount, float probabilityAlpha)
{
    m_nPixelCount = pixelCount;
    m_nDistribCount = pixelCount + 1; // One set of direct illumination distributions for each pixel + One set of indirect illumination distribution shared by all pixels
    m_nLightCount = lights.visibleLightCount();

    m_nImportanceCachedLightCount = 0;
    m_LightIsImportanceCachedFlags.resize(m_nLightCount);
    for (size_t lightIdx = 0; lightIdx < m_nLightCount; ++lightIdx)
    {
        m_LightIsImportanceCachedFlags[lightIdx] = isImportanceCached(*lights.visibleLights()[lightIdx]);
        m_nImportanceCachedLightCount += m_LightIsImportanceCachedFlags[lightIdx];
    }
    m_fConservativeProbability = 1.f / m_nImportanceCachedLightCount;

    m_PerPixelContribs.resize(m_nDistribCount * m_nLightCount);
    m_LightContribsOnTile.resize(m_nLightCount);

    m_Weights.resize(m_nDistribCount * m_nLightCount);
    m_WeightDistribNames.resize(m_nDistribCount * m_nLightCount);
    m_PerDistribProbabilities.resize(m_nDistribCount * DistributionNames::Count);

    m_IsUsable.resize(m_nDistribCount);

    m_ProbabilityAlpha = probabilityAlpha;
    m_ProbabilityScalingFactor = expf(-m_ProbabilityAlpha);

    clear();
}

void LightingImportanceCache::clear()
{
    std::fill(begin(m_PerPixelContribs), end(m_PerPixelContribs), PerPixelContribs{});
    std::fill(begin(m_LightContribsOnTile), end(m_LightContribsOnTile), 0.f);
    std::fill(begin(m_Weights), end(m_Weights), 0.f);
    std::fill(begin(m_PerDistribProbabilities), end(m_PerDistribProbabilities), 0.f);
    std::fill(begin(m_IsUsable), end(m_IsUsable), UsabilityFlags{});
    std::fill(std::begin(m_DirectLightingContribSums), std::end(m_DirectLightingContribSums), Color3(0.f));
    std::fill(std::begin(m_IndirectLightingContribSums), std::end(m_IndirectLightingContribSums), Color3(0.f));

    m_BaseProbability = 1.f;
}

bool LightingImportanceCache::isUsable(size_t distribIdx) const
{
    const auto usability = m_IsUsable[distribIdx];
    return usability.isUsable[DistributionNames::F] || usability.isUsable[DistributionNames::T] || usability.isUsable[DistributionNames::U];
}

size_t LightingImportanceCache::getDistribIdx(size_t pixelIdx, int32_t bounce) const
{
    return (bounce == 0) ? pixelIdx : m_nPixelCount; // For primary points, use direct illumination distributions of the pixel, for others use indirect illumination distributions
}

void LightingImportanceCache::addLightContribs(
    size_t distribIdx, size_t lightIdx, const Color3 & lightContrib, const Color3 & fullColor, const Color3 & unoccludedColor, DistributionNames::Enum distribName)
{
    if (isNaN(lightContrib) || isNaN(fullColor) || isNaN(unoccludedColor))
        return; // Better to be safe here

    const auto probabilities = &m_PerDistribProbabilities[distribIdx * DistributionNames::Count];

    float fullContrib = luminance(lightContrib * fullColor);
    float unoccludedContrib = luminance(lightContrib * unoccludedColor);

    const auto pContribs = m_PerPixelContribs.data() + distribIdx * m_nLightCount;

    pContribs[lightIdx].full += fullContrib;
    pContribs[lightIdx].unoccluded += unoccludedContrib;

    if (!isIndirectLightingDistrib(distribIdx))
        m_LightContribsOnTile[lightIdx] += fullContrib;

    if (distribName < DistributionNames::Count)
    {
        if (!isIndirectLightingDistrib(distribIdx))
            m_DirectLightingContribSums[distribName] += lightContrib * fullColor * probabilities[distribName];
        else
            m_IndirectLightingContribSums[distribName] += lightContrib * fullColor * probabilities[distribName];
    }
}

void LightingImportanceCache::sampleLights(size_t count, LightingImportanceCacheDataStorage & icData) const
{
    icData.lightsToProcess.assign(icData.lightsToProcess.size(), false); // Reset light flags
    auto * pSamples = icData.lightSamplesBuffer.data();
    for (size_t i = 0; i < count; ++i)
    {
        if (!isUsable(icData.distribIdxBuffer[i]))
        {
            icData.lightSamplesIteratorBuffer[i] = std::make_pair(pSamples, pSamples);
            continue;
        }
        // Compute all samples with one call (assume that [samples[i].uChosenLight[0].uIndex, samples[renderer->m_directIllumSampleCount - 1].uChosenLight[0].uIndex] is sorted, which is done
        // in SampleBuffer::compute)
        const auto sampleCount = sampleDistributions(icData.distribIdxBuffer[i], icData.lightIndexRandomNumbersBuffer[i], icData.directIllumSampleCount, pSamples);
        icData.lightSamplesIteratorBuffer[i] = std::make_pair(pSamples, pSamples + sampleCount);

        for (uint32_t sampleIdx = 0; sampleIdx < sampleCount; ++sampleIdx)
            icData.lightsToProcess[pSamples[sampleIdx].lightIdx] = true;

        pSamples += sampleCount;
    }
}

void LightingImportanceCache::computeAllLightsProcessingList(size_t count, LightingImportanceCacheDataStorage & icData) const
{
    size_t processingCount = 0;
    for (size_t i = 0; i < count; ++i)
    {
        if (!isUsable(icData.distribIdxBuffer[i]))
        {
            icData.processingList[processingCount] = i;
            icData.samplingWeights[processingCount] = 1.f; // No sampling has been done, so the weight is 1.f
            icData.contribIndices[processingCount] =
                LightingImportanceCache::DistributionNames::Count; // The contribution to fill is the last of icData.lightContribs (corresponding to all lights)
            ++processingCount;
        }
    }
    icData.processingListSize = processingCount;
}

void LightingImportanceCache::computeSampledLightProcessingList(size_t count, size_t lightIdx, LightingImportanceCacheDataStorage & icData) const
{
    size_t processingCount = 0;
    for (size_t i = 0; i < count; ++i)
    {
        auto & pSamples = icData.lightSamplesIteratorBuffer[i].first; // Reference because we change its value in the next if
        const auto * const pSamplesEnd = icData.lightSamplesIteratorBuffer[i].second;

        if (pSamples < pSamplesEnd && pSamples->lightIdx == lightIdx)
        {
            icData.processingList[processingCount] = i;
            icData.samplingWeights[processingCount] = pSamples->samplingWeight / icData.directIllumSampleCount;
            icData.contribIndices[processingCount] = pSamples->distribution;
            icData.multiplicities[processingCount] = pSamples->multiplicity;
            ++processingCount;

            ++pSamples; // Increment the begin iterator (require the method to be called iteratively with lightIdx increasing one by one)
        }
    }
    icData.processingListSize = processingCount;
}

std::array<Color3, LightingImportanceCache::DistributionNames::Count> LightingImportanceCache::getDirectContribs() const
{
    std::array<Color3, LightingImportanceCache::DistributionNames::Count> result;
    for (auto i = 0; i < LightingImportanceCache::DistributionNames::Count; ++i)
        result[i] = m_DirectLightingContribSums[i] / m_nPixelCount;
    return result;
}

std::array<Color3, LightingImportanceCache::DistributionNames::Count> LightingImportanceCache::getIndirectContribs() const
{
    std::array<Color3, LightingImportanceCache::DistributionNames::Count> result;
    for (auto i = 0; i < LightingImportanceCache::DistributionNames::Count; ++i)
        result[i] = m_IndirectLightingContribSums[i] / m_nPixelCount;
    return result;
}

void LightingImportanceCache::getDistribProbabilities(size_t distribIdx, float outBuffer[DistributionNames::Count]) const
{
    const float finalProbabilities[DistributionNames::Count] = { 0.25f, 0.25f, 0.25f, 0.25f };

    // F, U, T, C
    const float initProbabilities[DistributionNames::Count] = { 0.25f, 0.25f, 0.25f, 0.25f };

    std::array<float, LightingImportanceCache::DistributionNames::Count> probabilities;

    float sum = 0.f;
    for (size_t i = 0; i < DistributionNames::Count; ++i)
    {
        probabilities[i] = m_IsUsable[distribIdx].isUsable[i] ? initProbabilities[i] * m_BaseProbability + (1 - m_BaseProbability) * finalProbabilities[i] : 0.f;
        sum += probabilities[i];
    }
    if (sum > 0.f)
    {
        const float rcpSum = 1.f / sum;
        for (size_t i = 0; i < DistributionNames::Count; ++i)
        {
            probabilities[i] *= rcpSum;
        }
    }

    for (size_t i = 0; i < DistributionNames::Count; ++i)
        outBuffer[i] = probabilities[i];
}

void LightingImportanceCache::trainDistributions()
{
    m_BaseProbability *= m_ProbabilityScalingFactor;
    for (size_t i = 0; i < m_nDistribCount; ++i)
        buildLightSamplingDistributions(i);
    for (size_t i = 0; i < m_nDistribCount; ++i)
        getDistribProbabilities(i, &m_PerDistribProbabilities[i * DistributionNames::Count]);
}

void LightingImportanceCache::buildLightSamplingDistributions(size_t distribIdx)
{
    const auto pWeights = m_Weights.data() + distribIdx * m_nLightCount;
    const auto pWeightsDistribNames = m_WeightDistribNames.data() + distribIdx * m_nLightCount;

    const auto pPerPixelContribs = m_PerPixelContribs.data() + distribIdx * m_nLightCount;

    // Compute the sum of F and U distribs
    float fSum = 0.f;
    float uSum = 0.f;
    for (size_t lightIdx = 0; lightIdx < m_nLightCount; ++lightIdx)
    {
        const auto & contribs = pPerPixelContribs[lightIdx];
        const auto fContrib = contribs.full;
        const auto uContrib = contribs.unoccluded;
        fSum += fContrib;
        uSum += uContrib;
    }

    const auto fNormalizationFactor = fSum > 0.f ? 1.f / fSum : 0.f;
    const auto uNormalizationFactor = uSum > 0.f ? 1.f / uSum : 0.f;

    const auto pLightTileDistrib = m_LightContribsOnTile.data();

    // Compute the sum of T distrib, only for direct lighting distributions
    float tSum = 0.f;
    if (!isIndirectLightingDistrib(distribIdx))
    {
        for (size_t lightIdx = 0; lightIdx < m_nLightCount; ++lightIdx)
        {
            tSum += pLightTileDistrib[lightIdx];
        }
    }

    const auto tNormalizationFactor = tSum > 0.f ? 1.f / tSum : 0.f;

    // Priority order for the MIS max heuristic
    const DistributionNames::Enum priorityOrder[] = { DistributionNames::F, DistributionNames::T, DistributionNames::U, DistributionNames::C };

    float sums[DistributionNames::Count] = {};
    for (size_t lightIdx = 0; lightIdx < m_nLightCount; ++lightIdx)
    {
        const auto & perPixelContribs = pPerPixelContribs[lightIdx];

        // Weight for each distrib
        const float weights[DistributionNames::Count] = { perPixelContribs.full, perPixelContribs.unoccluded, pLightTileDistrib[lightIdx],
                                                          m_LightIsImportanceCachedFlags[lightIdx] ? m_fConservativeProbability : 0.f };

        // Probability for each distrib (C is already normalized)
        const float pdfs[DistributionNames::Count] = { weights[DistributionNames::F] * fNormalizationFactor, weights[DistributionNames::U] * uNormalizationFactor,
                                                       weights[DistributionNames::T] * tNormalizationFactor, weights[DistributionNames::C] };

        // Compute the maximum of probabilities, for max heuristic
        const float maxPdf = max(pdfs[DistributionNames::F], max(pdfs[DistributionNames::T], max(pdfs[DistributionNames::U], pdfs[DistributionNames::C])));

        // Store the weight that maximize the probability in the weights array, with the name to identify from which distrib it comes from
        for (size_t i = 0; i < DistributionNames::Count; ++i)
        {
            const auto distribName = priorityOrder[i];
            if (pdfs[distribName] >= maxPdf)
            {
                pWeights[lightIdx] = weights[distribName];
                pWeightsDistribNames[lightIdx] = distribName;
                sums[distribName] += weights[distribName]; // Update the correct sum to re-normalize according to MIS
                break;
            }
        }
    }

    // Normalize weights according to MIS
    for (size_t lightIdx = 0; lightIdx < m_nLightCount; ++lightIdx)
    {
        const auto weightsSum = sums[pWeightsDistribNames[lightIdx]];
        if (weightsSum > 0.f)
            pWeights[lightIdx] /= weightsSum;
    }

    // Identify is the distribution is usable (F, U or T must be filled for direct lighting, F or U for indirect lighting)
    m_IsUsable[distribIdx] = {};
    for (size_t i = 0; i < DistributionNames::Count; ++i)
    {
        m_IsUsable[distribIdx].isUsable[i] = sums[i] > 0.f;
    }
}

size_t LightingImportanceCache::sampleDistributions(size_t distribIdx, float * pS1DBuffer, size_t sampleCount, LightSample * outBuffer) const
{
    const auto probabilities = &m_PerDistribProbabilities[distribIdx * DistributionNames::Count];

    size_t sampleCounts[DistributionNames::Count] = {};
    for (size_t i = 0; i < sampleCount; ++i)
    {
        float & s1D = pS1DBuffer[i];

        float previousCDF = 0.f;
        size_t currentDistrib = 0;
        float currentCDF = probabilities[currentDistrib];
        while (s1D >= currentCDF && currentDistrib < DistributionNames::Count)
        {
            ++currentDistrib;
            previousCDF = currentCDF;
            currentCDF += probabilities[currentDistrib];
        }

        ++sampleCounts[currentDistrib];
        s1D = (s1D - previousCDF) / probabilities[currentDistrib];
    }

    size_t offset = 0;
    float * pS1DBuffers[DistributionNames::Count] = {};
    for (size_t i = 0; i < DistributionNames::Count; ++i)
    {
        if (sampleCounts[i])
        {
            pS1DBuffers[i] = pS1DBuffer + offset;
            std::sort(pS1DBuffers[i], pS1DBuffers[i] + sampleCounts[i]);
            offset += sampleCounts[i];
        }
    }

    const auto pStart = outBuffer;

    const auto pWeights = m_Weights.data() + distribIdx * m_nLightCount;
    const auto pWeightDistribNames = m_WeightDistribNames.data() + distribIdx * m_nLightCount;

    float currentSum[DistributionNames::Count] = {}; // The current CDF of each distribution
    size_t sampleIdx[DistributionNames::Count] = {}; // The current sample idx in pS1DBuffer of each distribution

    // The weights array is traveled once, each distribution outputs its samples according to random numbers sorted in pS1DBuffer when required
    for (size_t currentLightIdx = 0; currentLightIdx < m_nLightCount; ++currentLightIdx)
    {
        const auto distribIdx = pWeightDistribNames[currentLightIdx];
        currentSum[distribIdx] += pWeights[currentLightIdx]; // Update the correct sum

        if (sampleCounts[distribIdx])
        {
            float s1D = pS1DBuffers[distribIdx][sampleIdx[distribIdx]]; // Read the current random number

            size_t occurenceCount = 0;
            while (s1D < currentSum[distribIdx] && sampleIdx[distribIdx] < sampleCounts[distribIdx])
            {
                // Consume all random numbers that sample this light
                ++occurenceCount;
                ++sampleIdx[distribIdx];
                s1D = pS1DBuffers[distribIdx][sampleIdx[distribIdx]];
            }

            if (occurenceCount > 0)
            {
                // Output a single light sample with a weight accounting for its multiplicity
                *outBuffer = LightSample(currentLightIdx, occurenceCount / (probabilities[distribIdx] * pWeights[currentLightIdx]), DistributionNames::Enum(distribIdx), occurenceCount);
                ++outBuffer;
            }
        }
    }

    return outBuffer - pStart;
}

} // namespace shn