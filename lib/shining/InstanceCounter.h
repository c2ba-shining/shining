#pragma once

#include <cstdint>

namespace shn
{

struct InstanceCounter
{
    uint64_t shadingDurationUS = 0;
    uint64_t shadingCount = 0;
    uint64_t shadowRayCount = 0;
};

} // namespace shn