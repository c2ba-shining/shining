#include "IMathTypes.h"
#include <cmath>

namespace shn
{

Color3 hsv2rgb(Color3 HSV)
{
    const float H = HSV.x == 360.f ? 0.f : HSV.x / 60.f;
    const float S = HSV.y;
    const float V = HSV.z;
    const float fract = H - floor(H);
    const float P = V * (1. - S);
    const float Q = V * (1. - S * fract);
    const float T = V * (1. - S * (1. - fract));

    if (0. <= H && H < 1.)
        return { V, T, P };
    else if (1. <= H && H < 2.)
        return { Q, V, P };
    else if (2. <= H && H < 3.)
        return { P, V, T };
    else if (3. <= H && H < 4.)
        return { P, Q, V };
    else if (4. <= H && H < 5.)
        return { T, P, V };
    else if (5. <= H && H < 6.)
        return { V, P, Q };

    return { 0, 0, 0 };
}

} // namespace shn