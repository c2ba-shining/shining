#include "Instance.h"
#include "IMathUtils.h"
#include "Shining_p.h"
#include "embree2/rtcore_ray.h"

namespace shn
{

void staticInstanceInit(StaticInstance * instance, const M44f & localToWorld, int32_t mask, uint32_t id, const MotionPrimitive * primitive)
{
    instance->worldToLocal = localToWorld.inverse();
    instance->localToWorld = localToWorld;
    instance->mask = mask;
    instance->id = id;
    instance->primitive = primitive;
    instance->bounds = xfmBounds(localToWorld, primitiveBounds(primitive));
}

void staticInstanceBounds(void * ptr, size_t item, RTCBounds & bounds_o)
{
    const StaticInstance * __restrict__ i = (const StaticInstance *) ptr;
    V3fTof3(i->bounds.min, bounds_o.lower_x, bounds_o.lower_y, bounds_o.lower_z);
    V3fTof3(i->bounds.max, bounds_o.upper_x, bounds_o.upper_y, bounds_o.upper_z);
}

void staticInstanceIntersect(void * ptr, RTCRay & ray, size_t item)
{
    const StaticInstance * __restrict__ i = (const StaticInstance *) ptr;
    if (unlikely((i->mask & ray.mask) == 0))
        return;
    const V4f org = fv3ToV4f(ray.org, 1.0);
    const V4f dir = fv3ToV4f(ray.dir, 0.0);
    const int geomID = ray.geomID;
    const int instID = ray.instID;
    const float worldTime = ray.time;
    V4fTofv3(org * i->worldToLocal, ray.org);
    V4fTofv3(dir * i->worldToLocal, ray.dir);
    ray.geomID = -1;
    ray.instID = i->id;
    float localTime = 0.f;
    RTCScene scene = primitiveScene(i->primitive, worldTime, &localTime);
    ray.time = localTime;

    rtcIntersect(scene, ray);

    V4fTofv3(org, ray.org);
    V4fTofv3(dir, ray.dir);
    ray.time = worldTime;
    if (ray.geomID == -1)
    {
        ray.geomID = geomID;
        ray.instID = instID;
    }
}

void staticInstanceOccluded(void * ptr, RTCRay & ray, size_t item)
{
    const StaticInstance * __restrict__ i = (const StaticInstance *) ptr;
    if (unlikely((i->mask & ray.mask) == 0))
        return;
    const V4f org = fv3ToV4f(ray.org, 1.0);
    const V4f dir = fv3ToV4f(ray.dir, 0.0);
    const float worldTime = ray.time;
    V4fTofv3(org * i->worldToLocal, ray.org);
    V4fTofv3(dir * i->worldToLocal, ray.dir);
    ray.instID = i->id;
    float localTime = 0.f;
    RTCScene scene = primitiveScene(i->primitive, worldTime, &localTime);
    ray.time = localTime;
    rtcOccluded(scene, ray);
    V4fTofv3(org, ray.org);
    V4fTofv3(dir, ray.dir);
    ray.time = worldTime;
}

void motionInstanceInit(MotionInstance * instance, const MotionTransform localToWorld, int32_t mask, uint32_t id, const MotionPrimitive * primitive)
{
    instance->worldToLocal = inverse(localToWorld);
    instance->localToWorld = localToWorld;
    instance->mask = mask;
    instance->id = id;
    instance->primitive = primitive;
    instance->bounds = xfmBounds(localToWorld, primitiveBounds(primitive));
}

void motionInstanceBounds(void * ptr, size_t item, RTCBounds & bounds_o)
{
    const MotionInstance * __restrict__ i = (const MotionInstance *) ptr;
    V3fTof3(i->bounds.min, bounds_o.lower_x, bounds_o.lower_y, bounds_o.lower_z);
    V3fTof3(i->bounds.max, bounds_o.upper_x, bounds_o.upper_y, bounds_o.upper_z);
}

void motionInstanceIntersect(void * ptr, RTCRay & ray, size_t item)
{
    const MotionInstance * __restrict__ i = (const MotionInstance *) ptr;
    if (unlikely((i->mask & ray.mask) == 0))
        return;
    const V4f org = fv3ToV4f(ray.org, 1.0);
    const V4f dir = fv3ToV4f(ray.dir, 0.0);
    const int geomID = ray.geomID;
    const int instID = ray.instID;
    const float worldTime = ray.time;
    M44f worldToLocal = slerp(i->worldToLocal, ray.time);
    V4fTofv3(org * worldToLocal, ray.org);
    V4fTofv3(dir * worldToLocal, ray.dir);
    ray.geomID = -1;
    ray.instID = i->id;
    float localTime = 0.f;
    RTCScene scene = primitiveScene(i->primitive, worldTime, &localTime);
    ray.time = localTime;

    rtcIntersect(scene, ray);

    V4fTofv3(org, ray.org);
    V4fTofv3(dir, ray.dir);
    ray.time = worldTime;
    if (ray.geomID == -1)
    {
        ray.geomID = geomID;
        ray.instID = instID;
    }
}

void motionInstanceOccluded(void * ptr, RTCRay & ray, size_t item)
{
    const MotionInstance * __restrict__ i = (const MotionInstance *) ptr;
    if (unlikely((i->mask & ray.mask) == 0))
        return;
    const V4f org = fv3ToV4f(ray.org, 1.0);
    const V4f dir = fv3ToV4f(ray.dir, 0.0);
    const float worldTime = ray.time;
    M44f worldToLocal = slerp(i->worldToLocal, ray.time);
    V4fTofv3(org * worldToLocal, ray.org);
    V4fTofv3(dir * worldToLocal, ray.dir);
    ray.instID = i->id;
    float localTime = 0.f;
    RTCScene scene = primitiveScene(i->primitive, worldTime, &localTime);
    ray.time = localTime;
    rtcOccluded(scene, ray);
    V4fTofv3(org, ray.org);
    V4fTofv3(dir, ray.dir);
    ray.time = worldTime;
}

} // namespace shn