#pragma once

#include "Binding.h"
#include "Displacement.h"
#include "IMathTypes.h"
#include "InstanceCounter.h"
#include "OslHeaders.h"
#include "OslRenderer.h"
#include "OslRendererStats.h"
#include "RendererOutputs.h"
#include "Scene.h"
#include "SceneImpl.h"
#include "ThreadUtils.h"
#include "UString.h"
#include "lights/Lights.h"
#include "sampling/Filter.h"
#include "shading/BSSRDFSampling.h"
#include "shading/CustomClosure.h"
#include <atomic>
#include <cstdint>
#include <unordered_set>

namespace shn
{

enum OpacityMode
{
    OPACITY_MODE_OPAQUE = 0,
    OPACITY_MODE_EVALUATE = 1,
    OPACITY_MODE_CONSTANT = 2,
    OPACITY_MODE_END,
};

enum MaterialType
{
    MATERIAL_TYPE_SURFACE = 0,
    MATERIAL_TYPE_HOMOGENEOUS_VOLUME = 1,
    MATERIAL_TYPE_HETEROGENEOUS_VOLUME = 2,
    MATERIAL_TYPE_END
};

enum
{
    R_ATTR_THREADCOUNT = 0,
    R_ATTR_MAXBOUNCES,
    R_ATTR_IGNORESTRAIGHTBOUNCES,
    R_ATTR_BOUNCESDIFFUSE,
    R_ATTR_BOUNCESGLOSSY,
    R_ATTR_BOUNCESREFLECTION,
    R_ATTR_BOUNCESREFRACTION,
    R_ATTR_BOUNCESTRANSPARENCY,
    R_ATTR_BOUNCESSHADOW,
    R_ATTR_BOUNCESVOLUME,
    R_ATTR_COUNTER,
    R_ATTR_OCCANGLE,
    R_ATTR_OCCDISTANCE,
    R_ATTR_DEPTHNEAR,
    R_ATTR_DEPTHFAR,
    R_ATTR_SHUTTEROFFSET,
    R_ATTR_AREALIGHTVERSION,
    R_ATTR_SPHERELIGHTVERSION,
    R_ATTR_LOWLIGHTTHRESHOLD,
    R_ATTR_TIMESEED,
    R_ATTR_PIXELSAMPLECLAMP,
    R_ATTR_ADAPTIVEVARIANCE,
    R_ATTR_ADAPTIVEMINSAMPLES,
    R_ATTR_ATMOSPHERESHADERID,
    R_ATTR_NANHANDLINGPOLICY,
    R_ATTR_TILEPATTERN,
    R_ATTR_RAYTRACEDINDIRECTBIAS,
    R_ATTR_VOLUMESKIPINCLUSIONTEST,
    R_ATTR_LIGHTICPROBAALPHA,
    R_ATTR_MAX
};

extern const ustring RENDERER_ATTR_NAMES[];

class OslRendererImpl
{
    friend class OslRendererTask;

public:
    // Minimum light contribution when no low light treshold is specified by the user
    static const float LOW_LIGHT_THRESHOLD;
    static const int BOUNCES_LIMIT = 2048;
    static const int DEFAULT_MAX_BOUNCES = 6;
    static const int DEFAULT_DIFFUSE_BOUNCES = 1;
    static const int DEFAULT_GLOSSY_BOUNCES = 2;
    static const int DEFAULT_REFLECTION_BOUNCES = 5;
    static const int DEFAULT_REFRACTION_BOUNCES = 5;
    static const int DEFAULT_TRANSPARENCY_BOUNCES = BOUNCES_LIMIT;
    static const int DEFAULT_SHADOW_BOUNCES = BOUNCES_LIMIT;
    static const int DEFAULT_VOLUME_BOUNCES = BOUNCES_LIMIT;

    enum PerformanceCounter
    {
        PerformanceCounter_Basic = 1, ///< general timers
        PerformanceCounter_Instance = 2, ///< shading duration per instance (stored in a instance attribute)
    };

    enum NaNHandlingPolicy
    {
        NaNHandlingPolicy_Drop = 0,
        NaNHandlingPolicy_Ignore = 1,
        NaNHandlingPolicy_Debug = 2
    };

    struct ShadingTree
    {
        std::string name;

        // Only for Material shading trees:
        OpacityMode opacityMode = OPACITY_MODE_OPAQUE;
        Color3 transparencyConstant = OSL::Color3(1.f);
        MaterialType type = MaterialType::MATERIAL_TYPE_SURFACE;
    };

    struct Assignment
    {
        enum Type
        {
            Surface = 0,
            Volume,
            Displacement,
            ToonOutline,
            Count
        };

        std::vector<OslRenderer::Id> shadingTreeIds; // For each shading group, a shading tree id
    };

public:
    OslRendererImpl();
    ~OslRendererImpl();

    OslRenderer::Status attribute(const char * name, const char * value);
    OslRenderer::Status attribute(const char * name, float value);
    OslRenderer::Status attribute(const char * name, const float * value, int32_t count);
    OslRenderer::Status attribute(const char * name, int32_t value);
    OslRenderer::Status getAttribute(ustring name, const char ** returnedValue);
    OslRenderer::Status getAttribute(ustring name, float * returnedValue);
    OslRenderer::Status getAttribute(ustring name, int32_t * returnedValue);
    OslRenderer::Status getAttribute(OSL::ustring name, std::string & returnedValue);

    OslRenderer::Status getAttributeBufferByteSize(ustring name, uint32_t * outByteSize);
    OslRenderer::Status getAttribute(ustring name, uint32_t count, char * bufferOut);

    OslRenderer::Status genShadingTrees(OslRenderer::Id * shadingTreeIds, uint32_t count);
    OslRenderer::Status addShadingTree(OslRenderer::Id shadingTreeId, string_view shadingTreeName, string_view jsonData, uint32_t frame);
    OslRenderer::Status beginShadingTree(OslRenderer::Id shadingTreeId, string_view name);
    OslRenderer::Status endShadingTree(OslRenderer::Id shadingTreeId);
    OslRenderer::Status clearShadingTrees();
    OslRenderer::Status assignShadingTree(OslRenderer::Id shadingTreeId, const OslRenderer::Id * instanceIds, const int32_t shadingGroup,
        int32_t numInstances, Assignment::Type assignType = Assignment::Surface);
    OslRenderer::Status shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, string_view value);
    OslRenderer::Status shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, const float value);
    OslRenderer::Status shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, const int32_t value);
    OslRenderer::Status shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, const float * values, int32_t count);
    OslRenderer::Status shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, const int32_t * values, int32_t count);

    void shadingNodeSource(string_view name, uint32_t count, string_view source);
    void addShadingNode(string_view name, string_view layer);
    void addShadingParameter(string_view name, string_view nodeName, string_view type, bool isArray, const json & value);
    void connectShadingNodes(string_view sourceLayer, string_view sourceParameter, string_view destinationLayer, string_view destinationParameter);

    int32_t getAtmosphereShaderId() const;

    bool hasVolumetrics() const
    {
        return m_volumeInstancesCount > 0 || getAtmosphereShaderId() != -1;
    }

    OslRenderer::Id getShadingTreeIdFromName(string_view name) const;
    const char * getShadingTreeNameFromId(OslRenderer::Id shadingTreeId) const;
    OslRenderer::Id getShadingTreeIdFromInstanceId(OslRenderer::Id instanceId, int32_t shadingGroup, Assignment::Type assignType = Assignment::Surface) const;

    bool hasVolumeShadingTree(OslRenderer::Id instanceId, int32_t shadingGroup) const
    {
        return getShadingTreeIdFromInstanceId(instanceId, shadingGroup, Assignment::Volume) >= 0;
    }

    OslRenderer::Status setDisplacement(DisplacementData data);
    OslRenderer::Status commitDisplacement(SceneImpl * scene);

    OslRenderer::Status genLights(OslRenderer::Id * lightIds, int32_t count);
    int32_t countLightIdsByName(const char * name) const;
    OslRenderer::Status lightIdsByName(const char * name, OslRenderer::Id * lightsIds, int32_t count) const;
    OslRenderer::Status lightAttribute(OslRenderer::Id lightId, ustring attrName, const char * attrType, int32_t dataByteSize, const char * data);
    OslRenderer::Status commitLight(OslRenderer::Id lightId);
    OslRenderer::Status commitLights();
    OslRenderer::Status clearLights();
    bool isEmissiveInstance(const char* instanceName) const;

    template<typename... Args>
    Framebuffer * makeFramebuffer(Args &&... args)
    {
        m_framebuffers.emplace_back(std::make_unique<Framebuffer>(std::forward<Args>(args)...));
        return m_framebuffers.back().get();
    }

    OslRenderer::Status clearFramebuffer(OslRenderer::Id framebufferId, int32_t channelCount, const float * clearValue);
    OslRenderer::Status clearSubFramebuffer(OslRenderer::Id framebufferId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t channelCount, const float * clearValue);
    const float * getFramebufferData(OslRenderer::Id framebufferId) const;
    int32_t getFramebufferChannelCount(OslRenderer::Id framebufferId) const;
    OslRenderer::Status getFramebufferData(
        OslRenderer::Id framebufferId, OslRenderer::Id bindingId, int32_t width, int32_t height, int32_t channelCount, OslRenderer::FramebufferFormat format, float * data) const;
    OslRenderer::Status getFramebufferData(
        OslRenderer::Id framebufferId, OslRenderer::Id bindingId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t channelCount, OslRenderer::FramebufferFormat format,
        float * data, int32_t dataStride) const;

    OslRenderer::Status genBindings(OslRenderer::Id * bindingsIds, int32_t count);
    OslRenderer::Status isBound(OslRenderer::Id bindingId, OslRenderer::Id framebufferId) const;
    OslRenderer::Status setBindingMask(OslRenderer::Id bindingId, const float * mask, int32_t squareSize);
    OslRenderer::Status waitBinding(OslRenderer::Id bindingId, int timeout_ms) const;
    int32_t bindingRenderedTileCount(OslRenderer::Id bindingId, int32_t * totalCount) const;
    OslRenderer::Status bindingRenderedTiles(OslRenderer::Id bindingId, int32_t * tiles, int32_t count, int32_t * tileSize) const;
    OslRenderer::Status bindingEnablePass(OslRenderer::Id bindingId, const char * location);
    OslRenderer::Status bindingInit(OslRenderer::Id bindingId, int32_t width, int32_t height);
    OslRenderer::Status bindingPostProcess(OslRenderer::Id bindingId);
    int32_t getBindingLocationChannelCount(const char * location) const;
    OslRenderer::Status getBindingLocationFramebuffer(OslRenderer::Id bindingId, string_view location, OslRenderer::Id * outFramebufferId) const;

    OslRenderer::Status statistics(const char * propertyName, int * value) const;
    OslRenderer::Status statistics(const char * propertyName, float * value) const;
    OslRenderer::Status statisticsReport(std::ostream & out) const;

    OslRenderer::Status
    render(Scene * scene, OslRenderer::Id cameraId, OslRenderer::Id bindingId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t startSample, int32_t endSample);
    OslRenderer::Status cancelRender(OslRenderer::Id bindingId);
    OslRenderer::Status progressFunctions(void (*sampleProgress)(int32_t, void *), void (*tileProgress)(int32_t, void *), void * appData);

    static OslRenderer::Status setDefaultShaderPath(const char * defaultShaderPath);
    static OslRenderer::Status setShaderStdIncludePath(const char * includePath);

    bool isPerformanceCounterEnabled(PerformanceCounter value) const;

private:
    OslRendererImpl(const OslRendererImpl &) = delete;
    OslRendererImpl & operator=(const OslRendererImpl &) = delete;

    std::vector<ShadingTree> m_shadingTrees;
    mutable std::vector<OSL::ShaderGroupRef> m_oslShaderGroups; // For each shading tree, an OSL shading group. Declared as mutable because execute() from OSL takes a ShaderGroup&, but
                                                                // OslRendererTask has a const access to OslRendererImpl

    std::array<std::vector<Assignment>, Assignment::Count> m_shadingTreeAssignments;

    OslRenderer::Id defaultMaterialShadingTreeId;
    Filter m_rayFilter;
    std::vector<std::unique_ptr<Framebuffer>> m_framebuffers;
    std::vector<std::unique_ptr<RendererOutputs>> m_rendererOutputs;
    Lights m_lights;

    enum class DirectIllumAlgorithm
    {
        AllLights,
        ImportanceCaching
    };

    DirectIllumAlgorithm m_directIllumAlgorithm = DirectIllumAlgorithm::AllLights;
    size_t m_directIllumSampleCount = 4;

    friend class OslDirectLightingImportanceCache;

    BSSRDFMISHeuristic m_bssrdfMISHeuristic = BSSRDFMISHeuristic::AlphaMax;
    BSSRDFResampling m_bssrdfResampling = BSSRDFResampling::True;

    std::vector<DisplacementData> m_accumDisplaceData;
    std::vector<Scene::Id> m_allVolumeInstanceIds;
    std::unordered_map<Scene::Id, int32_t> m_volumeIdsToVectorIndex; // #todo: remplacer par un vector avec des -1 dans les cases non associ� � un volume
    int32_t m_volumeInstancesCount = 0;

    OslRenderer::Id m_defaultToonOutlineShaderId = -1; // A toon outline shader id to be applied on the whole scene (can be overrided by a toon outline shader applied on the instance)

    // Attributes and Flags
    Attributes m_rendererAttributes;

    mutable union
    {
        uint64_t i;
        float f;
    } m_perfCounters[STAT_MAX];

    mutable std::unordered_map<int32_t, std::pair<InstanceCounter, InstanceCounter>> m_performanceInstances;
    mutable std::mutex m_performanceInstancesMutex;
    mutable std::vector<LightStats> m_lightStatistics;

    std::unordered_set<V2i> m_lightImportanceCachingStatsTileList;
    mutable json m_lightImportanceCacheTileStatistics;

    void (*m_progressSampleFunc)(int32_t, void *);
    void (*m_progressTileFunc)(int32_t, void *);
    void * m_progressAppData;

    static ustring Default_ShaderPath;
    static ustring SHADER_STD_INCLUDE_PATH;

public:
    static const ustring RayType_Camera;
    static const ustring RayType_Shadow;
    static const ustring RayType_Diffuse;
    static const ustring RayType_Glossy;
    static const ustring RayType_Straight;
    static const ustring RayType_Specular;
    static const ustring RayType_Reflection;
    static const ustring RayType_Refraction;

    static std::unique_ptr<ShiningRendererServices> s_shiningRendererServices;
    static std::unique_ptr<OSL::ShadingSystem> s_shadingSystem;
};

// Utility functions for tile rendering
size_t tileCount(const size_t dim);

size_t tileIndex(const size_t start);

} // namespace shn