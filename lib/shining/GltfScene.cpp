#include "GltfScene.h"

#include "LoggingUtils.h"
#include "IMathUtils.h"
#include "OslRenderer.h"
#include "Scene.h"
#include "EmissionBits.h"


#define TINYGLTF_IMPLEMENTATION
#define TINYGLTF_NO_STB_IMAGE
#define TINYGLTF_NO_STB_IMAGE_WRITE
#define TINYGLTF_NO_EXTERNAL_IMAGE
#include "tiny_gltf.h"

#include <OpenImageIO/filesystem.h>
#include <unordered_map>

namespace oiio = OIIO_NAMESPACE;

#define NO_NAME "no_name"

namespace shn
{

struct GltfInstance
{
    GltfInstance(std::string inName, M44f inTransform, int inMesh)
        : name(inName)
        , transform(inTransform)
        , mesh(inMesh) {}

    std::string name;
    M44f transform;
    int mesh = -1;
};

struct ShadingTreeBuilder
{
    ShadingTreeBuilder() = delete;
    ShadingTreeBuilder(const std::string & shdTreeName, const std::string & opacity)
        : name(shdTreeName)
    {
        data = {
            { "opacity", opacity },
            { "type", "surface" },
            { "nodes", {} },
            { "connections", {} }
        };
    }

    void addNode(const std::string & nodeName, const std::string & nodeType)
    {
        data["nodes"][nodeName] = {
            { "type", nodeType },
            { "parameters",{} }
        };
    }

    template<typename T>
    void addParameter(const std::string & nodeName, const std::string & paramName, const std::string & paramType,
        const T * values, size_t count = 1)
    {
        if (count <= 0)
        {
            SHN_LOGWARN << "addParameter: Param value count must be > 0.";
            return;
        }

        if (data["nodes"].find(nodeName) == data["nodes"].end())
        {
            SHN_LOGWARN << "addParameter: Node " << nodeName << " doesn't exist. Add it first.";
            return;
        }

        json valueJson;
        if (count == 1)
        {
            valueJson = *values;
        }
        else if (count > 1)
        {
            valueJson = {};
            for (size_t i = 0; i < count; ++i)
                valueJson.push_back(values[i]);
        }

        json paramJson = { {"type", paramType} };
        paramJson["value"] = valueJson;

        data["nodes"][nodeName]["parameters"][paramName] = paramJson;

        if (paramType == "color" && count == 4)
        {
            const json alphaJson = { { "type", "float" },{ "value", values[3] } };
            data["nodes"][nodeName]["parameters"][paramName + "A"] = alphaJson;
        }
    }

    void addConnection(const std::string & srcNode, const std::string & srcParam, const std::string & dstNode, const std::string & dstParam)
    {
        data["connections"].push_back(
        { { "srcnode", srcNode }, { "srcparam", srcParam }, { "dstnode", dstNode }, { "dstparam", dstParam } }
        );
    }

    std::string write() const
    {
        return data.dump();
    }

    json data;
    const std::string name;
};

enum FactorType
{
    FLOAT = 0,
    COLOR,
    COUNT
};

bool importGltfMaterialFactor(ShadingTreeBuilder & shadingTree, const tinygltf::Material & gltfMaterial,
    const std::string & factorName, const FactorType factorType, const std::string & node, const std::string & param)
{
    const tinygltf::ParameterMap * usedParameterMap = [&]() -> const tinygltf::ParameterMap *
    {
        if (gltfMaterial.values.find(factorName) != gltfMaterial.values.end())
            return &gltfMaterial.values;

        if (gltfMaterial.additionalValues.find(factorName) != gltfMaterial.additionalValues.end())
            return &gltfMaterial.additionalValues;

        return nullptr;
    }();

    if (!usedParameterMap)
        return false;

    if (factorType == FactorType::COLOR)
    {
        const tinygltf::ColorValue colorValue = usedParameterMap->at(factorName).ColorFactor();
        shadingTree.addParameter(node, param, "color", &colorValue[0], 4);
    }
    else // FactorType::FLOAT
    {
        const float floatValue = (const float)usedParameterMap->at(factorName).Factor();
        shadingTree.addParameter(node, param, "float", &floatValue);
    }

    return true;
}

using ShadingConnectionFunctor = void(ShadingTreeBuilder & shadingTree, const std::string & textureNode, const std::string & node, const std::string & params);
void connectToColorParameter(ShadingTreeBuilder & shadingTree, const std::string & textureNode, const std::string & node, const std::string & param)
{
    shadingTree.addConnection(textureNode, "_outColor", node, param);
    shadingTree.addConnection(textureNode, "_outColorA", node, param + "A");
}

void connectToNormalParameter(ShadingTreeBuilder & shadingTree, const std::string & textureNode, const std::string & node, const std::string & param)
{
    const std::string normalMapNodeName = textureNode + "_nrm";
    shadingTree.addNode(normalMapNodeName, "NormalMapNode");
    shadingTree.addConnection(textureNode, "_outColor", normalMapNodeName, "_normalMap");
    shadingTree.addConnection(normalMapNodeName, "_outShadingNormal", node, param);
}

bool importGltfMaterialTexture(ShadingTreeBuilder & shadingTree, const tinygltf::Material & gltfMaterial, const tinygltf::Model & model, const std::string & gltfDirPath,
    const std::string & textureName, const std::string & nodeName, const std::string & paramName, ShadingConnectionFunctor connectFunc)
{
    const tinygltf::ParameterMap * usedParameterMap = [&]() -> const tinygltf::ParameterMap *
    {
        if (gltfMaterial.values.find(textureName) != gltfMaterial.values.end())
        return &gltfMaterial.values;

        if (gltfMaterial.additionalValues.find(textureName) != gltfMaterial.additionalValues.end())
            return &gltfMaterial.additionalValues;

        return nullptr;
    }();

    if (!usedParameterMap)
        return false;

    const size_t textureIdx = usedParameterMap->at(textureName).TextureIndex();
    const size_t imageIdx = model.textures[textureIdx].source;
    const std::string texturePath = gltfDirPath + "/" + model.images[imageIdx].uri;
    if (!oiio::Filesystem::exists(texturePath))
        return false;
    
    const std::string textureNodeName = paramName + "_tx";
    shadingTree.addNode(textureNodeName, "TextureNode");
    shadingTree.addParameter<std::string>(textureNodeName, "_texture", "string", &texturePath);

    const std::string moveTexCoordNodeName = textureNodeName + "_move";
    shadingTree.addNode(moveTexCoordNodeName, "MoveTexCoordNode");
    float verticallyFlipedTexCoord[2] = { 1, -1 }; // in glTF, uv origin is in upper-left. So we need to vertically flip the texture
    shadingTree.addParameter<float>(moveTexCoordNodeName, "_mult", "float", &verticallyFlipedTexCoord[0], 2);
    shadingTree.addConnection(moveTexCoordNodeName, "_outUV", textureNodeName, "_uvGenerator");

    connectFunc(shadingTree, textureNodeName, nodeName, paramName);

    return true;
}

ShadingTreeBuilder createShadingTreeFromGltf(const tinygltf::Material & gltfMaterial, const tinygltf::Model & model, const std::string & gltfDirPath)
{
    std::string shadowOpacityMode = "opaque";
    if (gltfMaterial.additionalValues.find("alphaMode") != gltfMaterial.additionalValues.end())
    {
        const std::string alphaMode = gltfMaterial.additionalValues.at("alphaMode").string_value;
        if (alphaMode != "OPAQUE")
            shadowOpacityMode = "evaluate";
    }

    ShadingTreeBuilder shadingTree(gltfMaterial.name.c_str(), shadowOpacityMode);
    shadingTree.addNode("main_mat", "GltfPbrMetallicRoughnessMaterialNode");
    importGltfMaterialFactor(shadingTree, gltfMaterial, "baseColorFactor", FactorType::COLOR, "main_mat", "_baseColor");
    importGltfMaterialFactor(shadingTree, gltfMaterial, "metallicFactor", FactorType::FLOAT, "main_mat", "_metallic");
    importGltfMaterialFactor(shadingTree, gltfMaterial, "roughnessFactor", FactorType::FLOAT, "main_mat", "_roughness");
    importGltfMaterialFactor(shadingTree, gltfMaterial, "emissiveFactor", FactorType::COLOR, "main_mat", "_emissionColor");

    importGltfMaterialTexture(shadingTree, gltfMaterial, model, gltfDirPath, "baseColorTexture", "main_mat", "_baseColor", connectToColorParameter);
    importGltfMaterialTexture(shadingTree, gltfMaterial, model, gltfDirPath, "metallicRoughnessTexture", "main_mat", "_metallicRoughnessTexture", connectToColorParameter);
    importGltfMaterialTexture(shadingTree, gltfMaterial, model, gltfDirPath, "normalTexture", "main_mat", "_shadingNormal", connectToNormalParameter);
    importGltfMaterialTexture(shadingTree, gltfMaterial, model, gltfDirPath, "emissiveTexture", "main_mat", "_emissionColor", connectToColorParameter);

    return shadingTree;
}

std::string prettyName(std::string str)
{
    if (str.empty())
        return NO_NAME;
    return str;
}

M44f lookAt(V3f eye, V3f center, V3f up)
{
    V3f f = normalize(center - eye); //forward
    V3f s = normalize(cross(f, up)); //side
    V3f u = cross(s, f); //up

    return M44f(
        s.x, u.x, -f.x, 0.f,
        s.y, u.y, -f.y, 0.f,
        s.z, u.z, -f.z, 0.f,
        0.f, 0.f,  0.f, 1.f
    ).translate(-eye);
}

const std::unordered_map<int, size_t> glType = {
    { TINYGLTF_TYPE_VEC2, 2 },
    { TINYGLTF_TYPE_VEC3, 3 },
    { TINYGLTF_TYPE_VEC4, 4 },
    { TINYGLTF_TYPE_MAT2, 4 },
    { TINYGLTF_TYPE_MAT3, 9 },
    { TINYGLTF_TYPE_MAT4, 16 },
    { TINYGLTF_TYPE_SCALAR, 1 },
    { TINYGLTF_TYPE_VECTOR, 4 },
    { TINYGLTF_TYPE_MATRIX, 16 },
};

const std::unordered_map<int, size_t> glParamTypeToByteSize = {
    { TINYGLTF_PARAMETER_TYPE_BYTE, sizeof(char) },
    { TINYGLTF_PARAMETER_TYPE_UNSIGNED_BYTE, sizeof(unsigned char) },
    { TINYGLTF_PARAMETER_TYPE_SHORT, sizeof(short) },
    { TINYGLTF_PARAMETER_TYPE_UNSIGNED_SHORT, sizeof(unsigned short) },
    { TINYGLTF_PARAMETER_TYPE_INT, sizeof(int32_t) },
    { TINYGLTF_PARAMETER_TYPE_UNSIGNED_INT, sizeof(uint32_t) },
    { TINYGLTF_PARAMETER_TYPE_FLOAT, sizeof(float) }
};

template <typename T, size_t C>
size_t loadGltfBuffer(const size_t accesorIndex, const std::string & bufferName, const tinygltf::Model & model, std::vector<T> & outBuffer)
{
    const size_t componentCount = C;
    const tinygltf::Accessor & accessor = model.accessors[accesorIndex];
    const tinygltf::BufferView & bufferView = model.bufferViews[accessor.bufferView];
    const tinygltf::Buffer & buffer = model.buffers[bufferView.buffer];

    if (glType.at(accessor.type) != componentCount)
    {
        SHN_LOGWARN << "[GLTF] Buffer \'" << bufferName << "\': found component count (" << glType.at(accessor.type) << ") is not as expected (" << componentCount << "). Cancel buffer import";
        return -1;
    }

    outBuffer.resize(accessor.count * componentCount);
    const size_t offset = accessor.byteOffset + bufferView.byteOffset;
    const size_t byteStride = (bufferView.byteStride == 0) ? glParamTypeToByteSize.at(accessor.componentType) * componentCount : bufferView.byteStride;

    for (size_t i = 0; i < accessor.count; ++i)
    {
        const size_t index = offset + i * byteStride;
        for (size_t c = 0; c < componentCount; ++c)
        {
            const size_t componentIndex = index + c * glParamTypeToByteSize.at(accessor.componentType);
            T data;
            switch (accessor.componentType)
            {
            case TINYGLTF_PARAMETER_TYPE_BYTE:
                data = (T)*(char*)(&buffer.data[componentIndex]);
                break;
            case TINYGLTF_PARAMETER_TYPE_UNSIGNED_BYTE:
                data = (T)*(unsigned char*)(&buffer.data[componentIndex]);
                break;
            case TINYGLTF_PARAMETER_TYPE_SHORT:
                data = (T)*(short*)(&buffer.data[componentIndex]);
                break;
            case TINYGLTF_PARAMETER_TYPE_UNSIGNED_SHORT:
                data = (T)*(unsigned short*)(&buffer.data[componentIndex]);
                break;
            case TINYGLTF_PARAMETER_TYPE_INT:
                data = (T)*(int32_t*)(&buffer.data[componentIndex]);
                break;
            case TINYGLTF_PARAMETER_TYPE_UNSIGNED_INT:
                data = (T)*(uint32_t*)(&buffer.data[componentIndex]);
                break;
            case TINYGLTF_PARAMETER_TYPE_FLOAT:
                data = (T)*(float*)(&buffer.data[componentIndex]);
                break;
            default:
                SHN_LOGWARN << "[GLTF] Buffer \'" << bufferName << "\': data type is not supported (" << accessor.componentType << "). Cancel buffer import";
                return -1;
                break;
            }
            outBuffer[i * componentCount + c] = data;
        }
    }

    return accessor.count;
}

size_t findVertexAttributeIndex(const tinygltf::Primitive & prim, const std::string & attrName)
{
    const auto & found = prim.attributes.find(attrName);
    if (found != prim.attributes.end())
        return found->second;
    return -1;
}

void recursivelyFindInstances(const tinygltf::Model & model, const tinygltf::Node & currentNode, const M44f& parentMatrix, std::vector<GltfInstance> & outInstances)
{
    M44f currentMatrix;
    if (currentNode.matrix.size() == 16)
    {
        std::vector<float> floatMatrixData(16);
        for (size_t i = 0; i < floatMatrixData.size(); ++i)
            floatMatrixData[i] = (float)currentNode.matrix[i];
        currentMatrix = fv16ToM44f(&floatMatrixData[0]);
    }
    else
    {
        if (currentNode.translation.size() == 3)
        {
            V3f translation((float)currentNode.translation[0], (float)currentNode.translation[1], (float)currentNode.translation[2]);
            currentMatrix.translate(translation);
        }
        if (currentNode.rotation.size() == 4)
        {
            V4f quaternions((float)currentNode.rotation[0], (float)currentNode.rotation[1], (float)currentNode.rotation[2], (float)currentNode.rotation[3]);
            currentMatrix.rotate(toEulerAngle(quaternions));
        }
        if (currentNode.scale.size() == 3)
        {
            V3f scale((float)currentNode.scale[0], (float)currentNode.scale[1], (float)currentNode.scale[2]);
            currentMatrix.scale(scale);
        }
    }

    currentMatrix *= parentMatrix;

    if (currentNode.mesh != -1)
    {
        outInstances.emplace_back(currentNode.name, currentMatrix, currentNode.mesh);
    }
    else
    {
        for (const auto & childIndex : currentNode.children)
        {
            recursivelyFindInstances(model, model.nodes[childIndex], currentMatrix, outInstances);
        }
    }
}

int32_t updateSceneFromGltf(Scene * scene, OslRenderer * renderer, const char * gltfFilename, int32_t * defaultCameraId, const char * defaultEnvmapTexturePath)
{
    tinygltf::Model model;
    tinygltf::TinyGLTF loader;
    std::string errorLog;
    std::string warningLog;

    const std::string gltfDirPath = oiio::Filesystem::parent_path(std::string(gltfFilename));

    bool status = loader.LoadASCIIFromFile(&model, &errorLog, &warningLog, gltfFilename);
    if (!warningLog.empty())
        fprintf(stdout, "[GLTF] Warning: %s\n", warningLog.c_str());
    if (!errorLog.empty())
    {
        fprintf(stdout, "[GLTF] Error: %s\n", errorLog.c_str());
        exit(EXIT_FAILURE);
    }
    if (!status)
    {
        fprintf(stdout, "[GLTF] Failed to parse glTF scene file\n");
        exit(EXIT_FAILURE);
    }

    // load meshes
    size_t meshCount = 0;
    std::unordered_map<size_t, std::vector<Scene::Id>> meshGltfIdToSceneIds;
    for (size_t m = 0; m < model.meshes.size(); ++m)
    {
        const auto & mesh = model.meshes[m];
        SHN_LOGINFO << "[GLTF] Starting to load mesh " << prettyName(mesh.name);

        std::vector<Scene::Id> sceneIds;
        for (const auto & prim : mesh.primitives)
        {
            if (prim.mode != TINYGLTF_MODE_TRIANGLES)
            {
                SHN_LOGWARN << "[GLTF] Not triangulated. Cancel mesh import";
                continue;
            }

            // topology
            std::vector<int32_t> topologyData;
            const size_t verticesCount = loadGltfBuffer<int32_t, 1>(prim.indices, "topology", model, topologyData);
            if (verticesCount == -1)
                continue;

            // mandatory vertex attributes : positions and normals
            const size_t positionIndex = findVertexAttributeIndex(prim, "POSITION");
            if (positionIndex == -1)
            {
                SHN_LOGWARN << "[GLTF] Unable to find POSITION vertex attributes. Cancel mesh import";
                continue;
            }

            const size_t normalIndex = findVertexAttributeIndex(prim, "NORMAL");
            if (normalIndex == -1)
            {
                SHN_LOGWARN << "[GLTF] Unable to find NORMAL vertex attributes. Cancel mesh import";
                continue;
            }

            std::vector<float> positionData;
            const size_t positionCount = loadGltfBuffer<float, 3>(positionIndex, "position", model, positionData);

            std::vector<float> normalData;
            const size_t normalCount = loadGltfBuffer<float, 3>(normalIndex, "normal", model, normalData);

            std::vector<float> texcoordData;
            const size_t texcoordCount = [&]() -> size_t {
                const size_t texcoordIndex = findVertexAttributeIndex(prim, "TEXCOORD_0");
                if (texcoordIndex == -1)
                {
                    SHN_LOGWARN << "[GLTF] Unable to find TEXCOORD_0 vertex attributes.";
                    return 0;
                }
                return loadGltfBuffer<float, 2>(texcoordIndex, "texcoord", model, texcoordData);
            }();

            // shining calls
            Scene::Id meshId = 0;
            scene->genMesh(&meshId, 1);
            scene->meshFaceData(meshId, (int32_t)verticesCount / 3, nullptr, Scene::COPY_BUFFER); // L style meshes don't need mesh data
            scene->enableMeshTopology(meshId, 1);
            scene->meshTopologyData(meshId, 0, (int32_t)verticesCount, &topologyData[0], Scene::COPY_BUFFER);
            scene->enableMeshVertexAttributes(meshId, 2);
            scene->meshVertexAttributeData(meshId, 0, "position", (int32_t)positionCount, 3, &positionData[0], Scene::COPY_BUFFER);
            scene->meshVertexAttributeData(meshId, 0, "normal", (int32_t)normalCount, 3, &normalData[0], Scene::COPY_BUFFER);
            if (texcoordCount > 0)
                scene->meshVertexAttributeData(meshId, 0, "texCoord", (int32_t)texcoordCount, 2, &texcoordData[0], Scene::COPY_BUFFER);
            scene->commitMesh(meshId);

            sceneIds.push_back(meshId);
            ++meshCount;
        }
        meshGltfIdToSceneIds.insert(std::make_pair(m, sceneIds));
    }

    SHN_LOGINFO << "[GLTF] Load " << meshCount << " meshes";

    // load materials
    size_t shadingTreeCount = 0;
    std::unordered_map<size_t, OslRenderer::Id> materialGltfIdToOslRendererId;
    for (size_t st = 0; st < model.materials.size(); ++st)
    {
        const auto & gltfMaterial = model.materials[st];
        ShadingTreeBuilder newShadingTree = createShadingTreeFromGltf(gltfMaterial, model, gltfDirPath);

        // Shining Calls
        OslRenderer::Id shadingTreeId;
        renderer->genShadingTrees(&shadingTreeId, 1);
        renderer->addShadingTree(shadingTreeId, gltfMaterial.name.c_str(), newShadingTree.write().c_str(), 0);

        materialGltfIdToOslRendererId.insert(std::make_pair(st, shadingTreeId));
        ++shadingTreeCount;
    }

    SHN_LOGINFO << "[GLTF] Load " << shadingTreeCount << " shading trees";

    // load instances
    size_t instanceCount = 0;
    for (const auto & gltfScene : model.scenes)
    {
        // build children stack
        for (const auto & nodeIndex : gltfScene.nodes)
        {
            std::vector<GltfInstance> toImportInstances;
            recursivelyFindInstances(model, model.nodes[nodeIndex], M44f(), toImportInstances);
            for (const auto & gltfInst : toImportInstances)
            {
                SHN_LOGINFO << "[GLTF] Starting to load instance " << prettyName(gltfInst.name);
                const auto & foundMeshes = meshGltfIdToSceneIds.find(gltfInst.mesh);
                if (foundMeshes == meshGltfIdToSceneIds.end())
                {
                    SHN_LOGWARN << "[GLTF] Associated meshes hasn't been imported. Cancel instance import";
                    continue;
                }

                const std::vector<Scene::Id> & meshIds = foundMeshes->second;
                const tinygltf::Mesh & gltfMesh = model.meshes[gltfInst.mesh];
                assert(gltfMesh.primitives.size() == meshIds.size());

                for (size_t i = 0; i < gltfMesh.primitives.size(); ++i)
                {
                    // get assigned shading tree id
                    const auto & assignPrim = gltfMesh.primitives[i];
                    OslRenderer::Id shadingTreeId = materialGltfIdToOslRendererId[assignPrim.material];

                    // shining calls
                    Scene::Id instanceId;
                    scene->genInstances(&instanceId, 1);
                    scene->instanceName(&instanceId, 1, gltfInst.name.c_str());
                    scene->instanceTransform(&instanceId, 1, gltfInst.transform.getValue(), nullptr, 1);
                    scene->instanceVisibility(&instanceId, 1, Scene::INSTANCE_VISIBILITY_PRIMARY, Scene::VISIBILITY_VISIBLE);
                    scene->instanceVisibility(&instanceId, 1, Scene::INSTANCE_VISIBILITY_SECONDARY, Scene::VISIBILITY_VISIBLE);
                    scene->instanceVisibility(&instanceId, 1, Scene::INSTANCE_VISIBILITY_SHADOW, Scene::VISIBILITY_VISIBLE);
                    scene->instanceMesh(&instanceId, 1, foundMeshes->second[i]);
                    renderer->assignShadingTree(shadingTreeId, &instanceId, 0, 1, SHN_ASSIGNTYPE_SURFACE);
                    scene->commitInstance(&instanceId, 1);
                    ++instanceCount;
                }
            }
        }
    }

    SHN_LOGINFO << "[GLTF] Load " << instanceCount << " instances";

    scene->commitScene();

    // Build default camera
    if (defaultCameraId)
    {
        scene->genCameras(defaultCameraId, 1);
        CameraAttr::name(*scene, *defaultCameraId, "defaultGltfCamera");
        CameraAttr::aspectRatio(*scene, *defaultCameraId, 1.);
        //CameraAttr::aspectRatio(*scene, *defaultCameraId, 1.77778f); // 16/9
        CameraAttr::halfFovY(*scene, *defaultCameraId, 0.330297);
        //CameraAttr::halfFovY(*scene, *defaultCameraId, 0.330297); // 16/9
        CameraAttr::focalLength(*scene, *defaultCameraId, 35.f);
        CameraAttr::type(*scene, *defaultCameraId, SHN_CAM_TYPE_PINHOLE);
        M44f screenToCam = M44f(2.91667, 0, 0, 0, 0, 2.91667, 0, 0, 0, 0, -1.00002, -1, 0, 0, -0.200002, 0).invert();
        //M44f screenToCam = M44f(1.64062, 0, 0, 0, 0, 2.91667, 0, 0, 0, 0, -1.00002, -1, 0, 0, -0.200002, 0).invert(); // 16/9
        CameraAttr::screenToCamera(*scene, *defaultCameraId, screenToCam.getValue());

        // build "establishment" camera to world matrix
        Box3f sceneBB;
        scene->getSceneBoundingBox(&sceneBB.min[0], &sceneBB.max[0]);
        V3f sceneBBDiagonal = sceneBB.max - sceneBB.min;
        V3f sceneBBCenter = sceneBB.min + 0.5f * sceneBBDiagonal;
        V3f maximizedDiagonal = V3f(reduceMax(sceneBBDiagonal));
        Box3f sceneMaximizedBB(sceneBBCenter - 0.5f * maximizedDiagonal, sceneBBCenter + 0.5f * maximizedDiagonal);
        V3f distanceBonus = maximizedDiagonal * 1.1f;

        V3f camCenter = sceneBBCenter;
        V3f camEye = sceneMaximizedBB.max + distanceBonus;
        M44f camToWorld = lookAt(camEye, camCenter, V3f(0.f, 1.f, 0.f)).invert();
        CameraAttr::cameraToWorld(*scene, *defaultCameraId, camToWorld.getValue());

        CameraAttr::nearPlane(*scene, *defaultCameraId, length(distanceBonus));
        CameraAttr::farPlane(*scene, *defaultCameraId, length(sceneMaximizedBB.min - camEye));

        scene->commitCamera(*defaultCameraId);
    }
    
    // Add default env light
    std::string envmapTexture(defaultEnvmapTexturePath);
    {
        OslRenderer::Id mainEnvmapId;
        renderer->genLights(&mainEnvmapId, 1);
        LightAttr::name(*renderer, mainEnvmapId, "MainEnvmap");
        LightAttr::type(*renderer, mainEnvmapId, SHN_LIGHT_TYPE_ENVMAP);
        shn::LightAttr::linkingMode(*renderer, mainEnvmapId, 0);
        float intensity = 1.f;
        renderer->lightAttribute(mainEnvmapId, SHN_ATTR_LIGHT_INTENSITY, "float", 1 * sizeof(float), (const char *)&intensity);
        if (envmapTexture.empty())
        {
            Color3 color(0.5f);
            renderer->lightAttribute(mainEnvmapId, SHN_ATTR_LIGHT_COLOR, "float", 3 * sizeof(float), (const char *)color.getValue());
        }
        else
        {
            ShadingTreeBuilder envmapShdTree("MainEnvmapShadingTree", "opaque");
            envmapShdTree.addNode("main_mat", "EnvMapLightNode");
            envmapShdTree.addNode("color_tx", "TextureNode");
            envmapShdTree.addParameter<std::string>("color_tx", "_texture", "string", &envmapTexture);
            envmapShdTree.addConnection("color_tx", "_outColor", "main_mat", "_color");
            OslRenderer::Id envmapShdTreeId;
            renderer->genShadingTrees(&envmapShdTreeId, 1);
            renderer->addShadingTree(envmapShdTreeId, envmapShdTree.name.c_str(), envmapShdTree.write().c_str(), 0);
            renderer->lightAttribute(mainEnvmapId, SHN_ATTR_LIGHT_COLORSHADINGTREEID, "int", 1 * sizeof(int32_t), (const char *)&envmapShdTreeId);
        }
        int32_t emissionBits = (int32_t)EmissionBits::All;
        renderer->lightAttribute(mainEnvmapId, SHN_ATTR_LIGHT_EMITCOMPONENT, "int", 1 * sizeof(int32_t), (const char *)&emissionBits);
        int32_t boolTrue = 1;
        renderer->lightAttribute(mainEnvmapId, SHN_ATTR_LIGHT_IMPORTANCESAMPLING, "int", 1 * sizeof(int32_t), (const char *)&boolTrue);
        int32_t ISResolution = 256;
        renderer->lightAttribute(mainEnvmapId, SHN_ATTR_LIGHT_IMPORTANCESAMPLINGRESOLUTION, "int", 1 * sizeof(int32_t), (const char *)&ISResolution);
        renderer->lightAttribute(mainEnvmapId, SHN_ATTR_LIGHT_BACKGROUND, "int", 1 * sizeof(int32_t), (const char *)&boolTrue);
        renderer->commitLight(mainEnvmapId);
    }
    
    // Add default dir light
    if (envmapTexture.empty())
    {
        OslRenderer::Id mainDirLightId;
        renderer->genLights(&mainDirLightId, 1);
        LightAttr::name(*renderer, mainDirLightId, "MainDirLight");
        LightAttr::type(*renderer, mainDirLightId, SHN_LIGHT_TYPE_DIRECTIONAL);
        shn::LightAttr::linkingMode(*renderer, mainDirLightId, 0);
        float intensity = 1.f;
        renderer->lightAttribute(mainDirLightId, SHN_ATTR_LIGHT_INTENSITY, "float", 1 * sizeof(float), (const char *)&intensity);
        Color3 color(1.f);
        renderer->lightAttribute(mainDirLightId, SHN_ATTR_LIGHT_COLOR, "float", 3 * sizeof(float), (const char *)color.getValue());
        int32_t emissionBits = (int32_t)EmissionBits::All;
        renderer->lightAttribute(mainDirLightId, SHN_ATTR_LIGHT_EMITCOMPONENT, "int", 1 * sizeof(int32_t), (const char *)&emissionBits);
        float rayTracedShadowBias = 0.005f;
        renderer->lightAttribute(mainDirLightId, SHN_ATTR_LIGHT_RAYTRACEDSHADOWBIAS, "float", 1 * sizeof(float), (const char *)&rayTracedShadowBias);
        M44f position(0.707107, 0, -0.707107, 0, -0.5, 0.707107, -0.5, 0, 0.5, 0.707107, 0.5, 0, 0, 0, 0, 1);
        renderer->lightAttribute(mainDirLightId, SHN_ATTR_LIGHT_LOCALTOWORLD, "float", 16 * sizeof(float), (const char *)position.getValue());
        renderer->commitLight(mainDirLightId);
    }

    renderer->commitLights();

    // Set good renderer attributes
    renderer->attribute(SHN_ATTR_RENDER_LOWLIGHTTRESHOLD, 0.f);
    renderer->attribute(SHN_ATTR_RENDER_PIXELSAMPLECLAMP, 3.f);
    renderer->attribute(SHN_ATTR_RENDER_IGNORESTRAIGHTBOUNCES, 1);

    return 0;
}

};