#pragma once

#include "AttrUtils.h"
#include "TypeUtils.h"

// Vertex attributes:
#define SHN_ATTR_VERTEX_POSITION "position"
#define SHN_ATTR_VERTEX_NORMAL "normal"
#define SHN_ATTR_VERTEX_TEXCOORDS "texCoord"

// Face attributes:
#define SHN_ATTR_FACE_MATERIALGROUPINDEX "materialGroupIndex"
#define SHN_ATTR_FACE_MATERIALGROUPCHUNKS "materialGroupChunks"

// Subdiv attributes:
#define SHN_ATTR_SUBDIV_LEVEL "subdivLevel"
#define SHN_ATTR_SUBDIV_ADAPTIVE "subdivAdaptive"
#define SHN_ATTR_SUBDIV_METHOD "subdivMethod"
#define SHN_ATTR_SUBDIV_VERTEXINTERPOLATEBOUNDARY "subdivVertexInterpolateBoundary"
#define SHN_ATTR_SUBDIV_FVARINTERPOLATEBOUNDARY "subdivFVarInterpolateBoundary"
#define SHN_ATTR_SUBDIV_FVARPROPAGATECORNERVALUE "subdivFVarPropagateCornerValue"
#define SHN_ATTR_SUBDIV_COMPUTENORMALS "subdivComputeNormalsValue"
#define SHN_ATTR_SUBDIV_CREASINGMETHOD "subdivCreasingMethod"
#define SHN_ATTR_SUBDIV_VERTEXCREASES "vertexCreases"
#define SHN_ATTR_SUBDIV_EDGECREASES "edgeCreases"
#define SHN_ATTR_SUBDIV_VERTSPEREDGE "vertsPerEdge"

// Displacement attributes
#define SHN_ATTR_DISP_DISPLACED "displaced"
#define SHN_ATTR_DISP_UNIQUEVTXTOPOIDS "displaceUniqueVtxTopoIds"
#define SHN_ATTR_DISP_ONLYDISPLACEMENT "onlyDisplacement"

// Primitive attributes
#define SHN_ATTR_PRIM_TYPE "primitiveType"

// Curve primitive attributes
#define SHN_ATTR_CURVE_TYPE "curveType"
#define SHN_ATTR_CURVE_PERIODICITY "curvePeriodicity"
#define SHN_ATTR_CURVE_LENGTH "curveLength"

// Camera attributes:
#define SHN_ATTR_CAM_CAMTOWORLD "cameraToWorld"
#define SHN_ATTR_CAM_SCREENTOCAM "screenToCamera"
#define SHN_ATTR_CAM_FOCALDISTANCE "focalDistance"
#define SHN_ATTR_CAM_LENSRADIUS "lensRadius"
#define SHN_ATTR_CAM_NEARPLANE "nearPlane"
#define SHN_ATTR_CAM_FARPLANE "farPlane"
#define SHN_ATTR_CAM_FOCALLENGTH "focalLength"
#define SHN_ATTR_CAM_HALFFOVY "hfovY"
#define SHN_ATTR_CAM_ASPECTRATIO "aspectRatio"
#define SHN_ATTR_CAM_OPENTIME "openTime"
#define SHN_ATTR_CAM_CLOSETIME "closeTime"
#define SHN_ATTR_CAM_TYPE "type"
#define SHN_ATTR_CAM_INTEROCULARDISTANCE "interocularDistance"
#define SHN_ATTR_CAM_NAME "name"

// Possible values for SHN_ATTR_CAM_TYPE:
#define SHN_CAM_TYPE_PINHOLE "pinhole"
#define SHN_CAM_TYPE_DOF "dof"
#define SHN_CAM_TYPE_LATLONG "latlong"
#define SHN_CAM_TYPE_STEREO360 "stereo360"
#define SHN_CAM_TYPE_STEREO360_L "stereo360_L"
#define SHN_CAM_TYPE_STEREO360_R "stereo360_R"
#define SHN_CAM_TYPE_STEREO360TOPDOWN "stereo360TopDown"
#define SHN_CAM_TYPE_ORTHO "orthographic"

namespace shn
{

struct CameraAttrSetter
{
    template<typename CameraT, typename CamIdT>
    static auto set(CameraT & r, CamIdT id, const char * attrName, const char * str)
    {
        return r.cameraAttribute(id, attrName, typeName<const char *>(), strlen(str) + 1, str);
    }

    template<typename CameraT, typename CamIdT, typename T>
    static auto set(CameraT & r, CamIdT id, const char * attrName, int32_t count, const T * values)
    {
        return r.cameraAttribute(id, attrName, typeName<T>(), count * sizeof(T), (const char *) values);
    }
};

struct CameraAttr
{
    SHN_DEFINE_STRING_ATTR(name, SHN_ATTR_CAM_NAME, CameraAttrSetter)
    SHN_DEFINE_STRING_ATTR(type, SHN_ATTR_CAM_TYPE, CameraAttrSetter)
    SHN_DEFINE_VEC_ATTR(cameraToWorld, SHN_ATTR_CAM_CAMTOWORLD, float, 16, CameraAttrSetter)
    SHN_DEFINE_VEC_ATTR(screenToCamera, SHN_ATTR_CAM_SCREENTOCAM, float, 16, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(focalDistance, SHN_ATTR_CAM_FOCALDISTANCE, float, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(lensRadius, SHN_ATTR_CAM_LENSRADIUS, float, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(nearPlane, SHN_ATTR_CAM_NEARPLANE, float, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(farPlane, SHN_ATTR_CAM_FARPLANE, float, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(focalLength, SHN_ATTR_CAM_FOCALLENGTH, float, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(halfFovY, SHN_ATTR_CAM_HALFFOVY, float, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(aspectRatio, SHN_ATTR_CAM_ASPECTRATIO, float, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(openTime, SHN_ATTR_CAM_OPENTIME, float, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(closeTime, SHN_ATTR_CAM_CLOSETIME, float, CameraAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(interocularDistance, SHN_ATTR_CAM_INTEROCULARDISTANCE, float, CameraAttrSetter)
};

} // namespace shn