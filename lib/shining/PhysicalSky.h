#pragma once

#ifndef __SHINING_PHYSICALSKY_H__
#define __SHINING_PHYSICALSKY_H__

#include "IMathUtils.h"
#include "arhoseksky/ArHosekSkyModel.h"
#include <mutex> // mutex
#include <vector>

namespace shn
{

struct PhysicalSky
{
public:
    enum Mode
    {
        Mode_RGB = 0,
        Mode_SPECTRAL,
    };

    PhysicalSky();
    ~PhysicalSky();
    void init();
    void setSkyParams(float sunElevation, float groundAlbedo, float turbidity, float skyIntensity, Mode mode);
    void eval(const V3f & dir, float * skydomeResult, int32_t spectrumSize, int32_t waveLenghtStart = 0, int32_t waveLengthEnd = 0) const;

private:
    ArHosekSkyModelState * m_skymodel = nullptr;
    // modification of these parameters triggers a re-init
    float m_sunElevation;
    float m_groundAlbedo;
    float m_turbidity;
    Mode m_mode;

    // modification of these parameters doesn't trigger a re-init - only use for RGB version
    float m_skyIntensity;

    // mutex to lock threads during sky re-initialization
    std::mutex m_skyInitMutex;
};

} // namespace shn

#endif //  __SHINING_PHYSICALSKY_H__
