#include "OslRendererImpl.h"
#include "JsonUtils.h"
#include "LoggingUtils.h"
#include "OslHeaders.h"
#include "OslRendererStats.h"
#include "OslRendererTask.h"
#include "PostProcess.h"
#include "Shining_p.h"
#include "StringUtils.h"
#include <bcd/core/MultiscaleDenoiser.h>
#include "sampling/Filter.h"
#include "sampling/PatternSamplers.h"
#include "sampling/RandomState.h"
#include <queue>
#include <regex>

namespace o = OSL;
namespace oiio = OIIO_NAMESPACE;

namespace shn
{

size_t tileCount(const size_t dim)
{
    return (dim + OslRendererTask::TILE_SIZE - 1) / OslRendererTask::TILE_SIZE;
}

size_t tileIndex(const size_t start)
{
    assert(start % OslRendererTask::TILE_SIZE == 0);
    return start / OslRendererTask::TILE_SIZE;
}

const ustring RENDERER_ATTR_NAMES[] = { ustring("threadCount"),
                                        ustring("maxBounces"),
                                        ustring("ignoreStraightBounces"),
                                        ustring("bouncesDiffuse"),
                                        ustring("bouncesGlossy"),
                                        ustring("bouncesReflection"),
                                        ustring("bouncesRefraction"),
                                        ustring("bouncesTransparency"),
                                        ustring("bouncesShadow"),
                                        ustring("bouncesVolume"),
                                        ustring("counters"),
                                        ustring("occlusionAngle"),
                                        ustring("occlusionDistance"),
                                        ustring("depthNear"),
                                        ustring("depthFar"),
                                        ustring("shutterOffset"),
                                        ustring("areaLightVersion"),
                                        ustring("sphereLightVersion"),
                                        ustring("lowLightThreshold"),
                                        ustring("timeSeed"),
                                        ustring("pixelSampleClamp"),
                                        ustring("adaptiveVariance"),
                                        ustring("adaptiveMinSamples"),
                                        ustring("atmosphereShaderId"),
                                        ustring("NaNHandlingPolicy"),
                                        ustring("tilePattern"),
                                        ustring("rayTracedIndirectBias"),
                                        ustring("volumeSkipInclusionTest"),
                                        ustring("lightICProbaAlpha") };

static_assert(R_ATTR_MAX == sizeof(RENDERER_ATTR_NAMES) / sizeof(ustring), "RENDERER_ATTR_NAMES size mismatch");

// Opacity modes string and enum values (they should be in sync)
static const o::ustring OpacityModeNames[] = { o::ustring("opaque"), o::ustring("evaluate"), o::ustring("constant") };

static OpacityMode OpacityModes[] = { OPACITY_MODE_OPAQUE, OPACITY_MODE_EVALUATE, OPACITY_MODE_CONSTANT };

// Material types string and enum values
static const o::ustring MaterialTypeNames[] = { o::ustring("surface"), o::ustring("homogeneous_volume"), o::ustring("heterogeneous_volume") };

static MaterialType MaterialTypes[] = { MATERIAL_TYPE_SURFACE, MATERIAL_TYPE_HOMOGENEOUS_VOLUME, MATERIAL_TYPE_HETEROGENEOUS_VOLUME };

const o::ustring OslRendererImpl::RayType_Camera("camera");
const o::ustring OslRendererImpl::RayType_Shadow("shadow");
const o::ustring OslRendererImpl::RayType_Diffuse("diffuse");
const o::ustring OslRendererImpl::RayType_Glossy("glossy");
const o::ustring OslRendererImpl::RayType_Straight("straight");
const o::ustring OslRendererImpl::RayType_Specular("specular");
const o::ustring OslRendererImpl::RayType_Reflection("reflection");
const o::ustring OslRendererImpl::RayType_Refraction("refraction");
o::ustring OslRendererImpl::Default_ShaderPath("");
o::ustring OslRendererImpl::SHADER_STD_INCLUDE_PATH("stdosl.h");

const float OslRendererImpl::LOW_LIGHT_THRESHOLD = 0.001f;

std::unique_ptr<ShiningRendererServices> OslRendererImpl::s_shiningRendererServices;
std::unique_ptr<OSL::ShadingSystem> OslRendererImpl::s_shadingSystem;

OslRendererImpl::OslRendererImpl()
{
    if (!s_shiningRendererServices)
    {
        s_shiningRendererServices = std::make_unique<ShiningRendererServices>();
        s_shadingSystem = std::make_unique<OSL::ShadingSystem>(s_shiningRendererServices.get(), s_shiningRendererServices->texturesys());
        OSL::ErrorHandler::default_handler().verbosity(2);
        // OSL::ShadingSystem::create (m_oslrenderer, m_oslrenderer->texturesys(), 0);
        registerCustomClosures(s_shadingSystem.get());

#ifdef SHN_PRECOMPUTE_GGX_COMPENSATION_DATA
        uint64_t time = 0;
        SHN_LOGINFO << "Precomputing shading data...";
        {
            HighResolutionTimerGuard timer(&time);
            GGXCompensation::precompute();
        }
        SHN_LOGINFO << "Done. Time = " << us2sec(time);
#endif
    }
    // Default values for attributes
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_THREADCOUNT], ATTR_TYPE_NAMES[ATTR_TYPE_INT], defaultMaxThreadCount());
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_MAXBOUNCES], ATTR_TYPE_NAMES[ATTR_TYPE_INT], DEFAULT_MAX_BOUNCES);
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESDIFFUSE], ATTR_TYPE_NAMES[ATTR_TYPE_INT], DEFAULT_DIFFUSE_BOUNCES);
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESGLOSSY], ATTR_TYPE_NAMES[ATTR_TYPE_INT], DEFAULT_GLOSSY_BOUNCES);
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESREFLECTION], ATTR_TYPE_NAMES[ATTR_TYPE_INT], DEFAULT_REFLECTION_BOUNCES);
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESREFRACTION], ATTR_TYPE_NAMES[ATTR_TYPE_INT], DEFAULT_REFRACTION_BOUNCES);
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESTRANSPARENCY], ATTR_TYPE_NAMES[ATTR_TYPE_INT], DEFAULT_TRANSPARENCY_BOUNCES);
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESSHADOW], ATTR_TYPE_NAMES[ATTR_TYPE_INT], DEFAULT_SHADOW_BOUNCES);
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESVOLUME], ATTR_TYPE_NAMES[ATTR_TYPE_INT], DEFAULT_VOLUME_BOUNCES);
    m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_COUNTER], ATTR_TYPE_NAMES[ATTR_TYPE_INT], PerformanceCounter_Basic); ///< flags to enable performance counters.
    m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_OCCANGLE], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], 45.f);
    m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_OCCDISTANCE], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], 1.f);
    m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_DEPTHNEAR], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], 0.f);
    m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_DEPTHFAR], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], 100.f);
    m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_SHUTTEROFFSET], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], 0.f);
    m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_LOWLIGHTTHRESHOLD], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], LOW_LIGHT_THRESHOLD);
    m_rayFilter = makeBoxFilter();
    s_shadingSystem->attribute("lockgeom", 1);
    // m_shadingSystem->attribute("debug_uninit", 1);
    // m_shadingSystem->attribute("range_checking", 1);
    // m_shadingSystem->attribute("compile_report", 1);
    // m_shadingSystem->attribute("clearmemory", 1);
    s_shadingSystem->attribute("searchpath:shader", Default_ShaderPath.c_str());
    s_shadingSystem->attribute("commonspace", "world");
    const char * raytypes[] = {
        RayType_Camera.c_str(),   RayType_Shadow.c_str(),   RayType_Diffuse.c_str(),    RayType_Glossy.c_str(),
        RayType_Straight.c_str(), RayType_Specular.c_str(), RayType_Reflection.c_str(), RayType_Refraction.c_str(),
    };
    s_shadingSystem->attribute("raytypes", o::TypeDesc(o::TypeDesc::STRING, 8), raytypes);
    m_progressSampleFunc = nullptr;
    m_progressTileFunc = nullptr;

    initMemoryImage();

    genShadingTrees(&defaultMaterialShadingTreeId, 1);
    beginShadingTree(defaultMaterialShadingTreeId, "default_shader");
    addShadingNode("default_shader", "default_shader");
    endShadingTree(defaultMaterialShadingTreeId);

    for (auto & perfCounter: m_perfCounters)
    {
        perfCounter.i = 0;
    }
}

OslRendererImpl::~OslRendererImpl()
{
    for (size_t i = 0; i < m_rendererOutputs.size(); ++i)
        assert(waitBinding(i, 0));

#ifdef SHN_PRECOMPUTE_GGX_COMPENSATION_DATA
    if (m_rendererAttributes.getString(SHN_ATTR_RENDER_EXEPATH))
        GGXCompensation::writePrecomputedArrays(m_rendererAttributes.getString(SHN_ATTR_RENDER_EXEPATH), "D:/GGXCompensationPrecomputedData.cpp");
#endif
}

OslRenderer::Status OslRendererImpl::genShadingTrees(OslRenderer::Id * shadingTreeIds, uint32_t count)
{
    int32_t firstId = m_shadingTrees.size();
    m_shadingTrees.resize(firstId + count);
    for (int32_t i = 0; i < (int32_t) count; ++i)
        shadingTreeIds[i] = firstId + i;
    m_oslShaderGroups.resize(m_oslShaderGroups.size() + count);
    return 0;
}

namespace
{

static const size_t PARAMETER_ARRAY_SIZE = 16;

static const std::unordered_map<std::string, size_t> typeToComponentCount = { { "int", 1 },   { "float", 1 },   { "vector_2", 2 }, { "vector_3", 3 }, { "vector_4", 4 },
                                                                              { "color", 3 }, { "matrix", 16 }, { "string", 1 },   { "file", 1 },     { "material", 1 } };

static const std::unordered_map<std::string, std::string> typeToRawType = { { "int", "int" },        { "float", "float" },      { "vector_2", "float" }, { "vector_3", "float" },
                                                                            { "vector_4", "float" }, { "color", "float" },      { "matrix", "float" },   { "string", "string" },
                                                                            { "file", "string" },    { "material", "material" } };

struct ParamType
{
    ParamType()
    {
    }
    ParamType(string_view dataType, bool isArray): dataType(dataType), isArray(isArray)
    {
    }
    ParamType(string_view rawDataType)
    {
        size_t found = rawDataType.find("[]");
        isArray = (found != std::string::npos);
        dataType = rawDataType.substr(0, found);
    }

    std::string dataType;
    bool isArray = false;
};

struct SubParam
{
    SubParam(const size_t i): index(i)
    {
    }

    std::string name;
    ParamType type;
    size_t index;
};

struct ParamConnection
{
    std::string realParamName;
    ParamType type;
    std::vector<SubParam> subParams;

    bool isSubConnection() const
    {
        return !subParams.empty();
    }
};

std::string getScalarComponentType(string_view typeName)
{
    if (typeToRawType.at(typeName) == "string" || typeName == "int")
        return typeName;

    return "float";
}

ParamConnection parseParamConnection(string_view originalParamName, const json & dstNodeData = json())
{
    ParamConnection ret;

    // split on brackets
    std::regex reg("[\\[][0-9]+[\\]][.]*");
    std::smatch m;
    std::string workingStr = originalParamName;
    while (std::regex_search(workingStr, m, reg))
    {
        const std::string & subName = m.prefix().str();
        if (!ret.realParamName.empty() && !subName.empty())
            ret.realParamName += "_";
        ret.realParamName += subName;

        int32_t index = std::stoi(m.str().substr(1));
        if (index >= 0)
            ret.subParams.push_back(SubParam(index));

        workingStr = m.suffix().str();
    }

    // complete the real param name with the remaining string in the original param name
    // manage case of param with 0 or 1 subparams
    if (!ret.realParamName.empty() && !workingStr.empty())
        ret.realParamName += "_";
    ret.realParamName += workingStr;

    // early exit if no dstNode json has been passed
    if (dstNodeData.is_null())
        return ret;

    // get original paramName type
    const auto originalParam = getJsonIfExists(dstNodeData["parameters"], ret.realParamName);
    if (originalParam)
    {
        const auto originalParamType = getIfExists<std::string>(*originalParam, "type");
        if (originalParamType)
        {
            ret.type = ParamType(*originalParamType);
        }
    }

    // set subParams type
    ParamType workingType = ret.type;
    for (auto & sub: ret.subParams)
    {
        if (workingType.isArray)
            workingType.isArray = false;
        else
            workingType.dataType = getScalarComponentType(workingType.dataType);

        sub.type = workingType;
    }

    return ret;
}

std::string getJoinerName(string_view dstNodeName, string_view dstParamName)
{
    std::string joinerName = "Joiner_";
    joinerName += dstNodeName;
    joinerName += dstParamName;
    return joinerName;
}

std::string getJoinerInputParam(const int32_t i)
{
    return "_in" + std::to_string(i);
}

void copyInputsInJoiner(json * joiner, const ParamType & srcParamType, const ParamType & dstParamType, const json & paramValue)
{
    assert(paramValue.is_array());
    assert(!paramValue.empty());
    assert(!paramValue[0].is_string());

    const size_t subComponentCount = typeToComponentCount.at(srcParamType.dataType);
    const std::string & subComponentType = srcParamType.dataType;
    const size_t arraySize = paramValue.size() / subComponentCount;
    for (size_t i = 0; i < arraySize; ++i)
    {
        std::string inputName = "_in" + std::to_string(i);
        json parameter = { { "type", subComponentType } };

        if (subComponentCount > 1)
        {
            parameter["value"] = json::array();
            auto currentComp = paramValue.begin() + (i * subComponentCount);
            parameter["value"].insert(parameter["value"].begin(), currentComp, currentComp + subComponentCount);
        }
        else
        {
            parameter["value"] = paramValue[i];
        }

        (*joiner)["parameters"][inputName] = parameter;
    }
}

json buildJoinerNode(const ParamType & srcParamType, const ParamType & dstParamType, const json & paramValue)
{
    std::string joinerType = "Joiner";
    if (dstParamType.isArray)
        joinerType += "Array";
    joinerType += "_" + dstParamType.dataType;

    json joinerNode = { { "type", joinerType }, { "parameters", {} } };

    if (paramValue.is_array())
        copyInputsInJoiner(&joinerNode, srcParamType, dstParamType, paramValue);

    return joinerNode;
}

struct ShadingNode
{
    ShadingNode(string_view name): name(name)
    {
    }

    std::string name;
    std::vector<ShadingNode *> sources;
    bool hasDestination = false;
    uint32_t depth = 0;
};

ShadingNode * findShadingNodeByName(std::vector<ShadingNode> & nodes, string_view name)
{
    auto found = std::find_if(nodes.begin(), nodes.end(), [name](const ShadingNode n) { return n.name == name; });
    if (found != nodes.end())
        return &(*found);
    return nullptr;
}

// In the OSL shading system, when we add a connection between two nodes, the source node need to have been declared BEFORE the destination node
// We don't know why, but we have to declare the deepest nodes before the others
// This function builds and browses a pseudo three of shading nodes to give them a depth, and then sort them by depth
std::vector<ShadingNode> sortShadingNodesByDepth(const json & nodes, const json & connections)
{
    // Init ShadingNodes
    std::vector<ShadingNode> sortedNodes;
    for (auto it = nodes.begin(); it != nodes.end(); ++it)
        sortedNodes.push_back(ShadingNode(it.key()));

    // Build tree from connections list
    for (const auto & conn: connections)
    {
        const std::string srcNodeName = conn["srcnode"];
        const std::string dstNodeName = conn["dstnode"];

        ShadingNode * srcNode = findShadingNodeByName(sortedNodes, srcNodeName);
        ShadingNode * dstNode = findShadingNodeByName(sortedNodes, dstNodeName);
        if (!srcNode || !dstNode)
            continue;

        srcNode->hasDestination = true;
        dstNode->sources.push_back(srcNode);
    }

    // Get root node
    const ShadingNode * rootNode = [&sortedNodes]() -> const ShadingNode * {
        for (const auto & node: sortedNodes)
        {
            if (!node.hasDestination)
            {
                return &node;
            }
        }
        return nullptr;
    }();

    // Visit tree to build nodes depth
    std::queue<const ShadingNode *> nodesProcessingQueue;
    nodesProcessingQueue.push(rootNode);
    do
    {
        const ShadingNode * node = nodesProcessingQueue.front();
        uint32_t currentDepth = node->depth;
        for (auto srcNode: node->sources)
        {
            srcNode->depth = max(srcNode->depth, currentDepth + 1);
            nodesProcessingQueue.push(srcNode);
        }
        nodesProcessingQueue.pop();
    } while (!nodesProcessingQueue.empty());

    // Sort by depth
    std::sort(sortedNodes.begin(), sortedNodes.end(), [](const auto & node1, const auto & node2) { return node1.depth > node2.depth; });
    return sortedNodes;
}

} // namespace

OslRenderer::Status OslRendererImpl::addShadingTree(OslRenderer::Id shadingTreeId, string_view shadingTreeName, string_view jsonData, uint32_t frame)
{
    for (int i = 0; i < (int) m_rendererOutputs.size(); ++i)
    {
        assert(waitBinding(i, 0));
    }

    json shadingTree = json::parse(jsonData.c_str());

    // manage joiners creation
    std::vector<json> connectionsToAdd;
    for (auto & conn: shadingTree["connections"])
    {
        const std::string srcNode = conn["srcnode"];
        const std::string srcParam = conn["srcparam"];
        const std::string dstNode = conn["dstnode"];
        const std::string dstParam = conn["dstparam"];

        // modify source sub-connected plug name
        const ParamConnection srcConnection = parseParamConnection(srcParam);
        if (srcConnection.isSubConnection())
            conn["srcparam"] = srcConnection.realParamName + "_" + std::to_string(srcConnection.subParams[0].index);

        // modify destination sub-connected plug name and add a joiner
        const ParamConnection dstConnection = parseParamConnection(dstParam, shadingTree["nodes"][dstNode]);
        if (dstConnection.isSubConnection())
        {
            if (typeToRawType.at(dstConnection.type.dataType) == "file" && dstConnection.type.isArray) // doesn't manage string array joiner
            {
                SHN_LOGWARN << "Unsupported sub-connection into a string array : " << srcNode << ":" << srcParam << " -> " << dstNode << ":" << dstParam;
                continue;
            }

            // build joiner nodes
            std::string workingDstNode = dstNode;
            std::string workingDstParam = dstConnection.realParamName;
            ParamType workingDstParamType = dstConnection.type;
            for (const auto & sub: dstConnection.subParams)
            {
                const std::string & joinerNodeName = getJoinerName(workingDstNode, workingDstParam);

                const auto dstNodeData = getJsonIfExists(shadingTree["nodes"], workingDstNode);
                if (!dstNodeData)
                {
                    SHN_LOGWARN << "Unable to find destination node during Joiner creation process : joiner=(" << joinerNodeName << "); not found node=(" << workingDstNode
                                << "); shading tree=(" << shadingTreeName << ");";
                    break;
                }

                const auto existingJoinerNode = getJsonIfExists(shadingTree["nodes"], joinerNodeName);
                if (!existingJoinerNode)
                {
                    const auto dstParamData = getJsonIfExists((*dstNodeData)["parameters"], workingDstParam);
                    if (!dstParamData)
                    {
                        SHN_LOGWARN << "Unable to find destination param during Joiner creation process : joiner=(" << joinerNodeName << "); not found param=(" << workingDstParam
                                    << "); destination node=(" << workingDstNode << "); shading tree=(" << shadingTreeName << ");";
                        break;
                    }

                    // get the the current frame's value
                    json dstParamValue;
                    const auto startFrame = getIfExists<json::number_unsigned_t>(*dstParamData, "start");
                    const auto endFrame = getIfExists<json::number_unsigned_t>(*dstParamData, "end");
                    if (startFrame && endFrame)
                    {
                        const uint32_t currentFrame = shn::clamp(frame, (uint32_t) *startFrame, (uint32_t) *endFrame) - (*startFrame);
                        dstParamValue = (*dstParamData)["value"][currentFrame];
                    }
                    else
                    {
                        dstParamValue = (*dstParamData)["value"];
                    }

                    shadingTree["nodes"][joinerNodeName] = buildJoinerNode(sub.type, workingDstParamType, dstParamValue);
                }

                // add "from joiner" connection
                const json joinerConnection = { { "srcnode", joinerNodeName }, { "srcparam", "_out" }, { "dstnode", workingDstNode }, { "dstparam", workingDstParam } };
                connectionsToAdd.emplace_back(joinerConnection);

                // step forward
                workingDstNode = joinerNodeName;
                workingDstParam = getJoinerInputParam(sub.index);
                workingDstParamType = sub.type;
            }

            // edit source connection
            conn["dstnode"] = workingDstNode;
            conn["dstparam"] = workingDstParam;
        }
    }

    for (const auto & connToAdd: connectionsToAdd)
        shadingTree["connections"].emplace_back(connToAdd);

    /*{
        // DEBUG PRINT
        for (auto it = shadingTree["nodes"].begin(); it != shadingTree["nodes"].end(); ++it)
        {
            SHN_LOGINFO << "Node " << it.key() << " (type: " << it.value()["type"] << ")";
        }


        for (const auto conn : shadingTree["connections"])
        {
            SHN_LOGINFO << "Connection " << conn["srcnode"] << ":" << conn["srcparam"] << " >> " << conn["dstnode"] << ":" << conn["dstparam"];
        }
    }*/

    // set special shading attributes
    static const char * stringAttributes[2] = { "opacity", "type" };
    for (const auto & attr: stringAttributes)
    {
        const auto attrValue = getIfExists<std::string>(shadingTree, attr);
        if (!attrValue)
        {
            SHN_LOGWARN << "Attribute \"" << attr << "\" hasn't a string value. It'll be ignored (" << shadingTreeName << ")";
            continue;
        }

        shadingTreeAttribute(shadingTreeId, attr, attrValue->c_str());
    }

    beginShadingTree(shadingTreeId, shadingTreeName);
    {
        const json & nodes = shadingTree["nodes"];
        std::vector<ShadingNode> allNodes = sortShadingNodesByDepth(nodes, shadingTree["connections"]);
        for (const auto & node: allNodes)
        {
            const json & n = nodes[node.name];
            const std::string nodeType = n["type"];

            const json & parameters = n["parameters"];
            for (auto itParam = parameters.begin(); itParam != parameters.end(); ++itParam)
            {
                const std::string & paramName = itParam.key();
                const json & p = itParam.value();
                const std::string rawParamType = p["type"];
                const ParamType paramType(rawParamType);

                // get the right framed json value
                const json & paramValue = [p, frame]() {
                    const auto startFrame = getIfExists<json::number_unsigned_t>(p, "start");
                    const auto endFrame = getIfExists<json::number_unsigned_t>(p, "end");
                    if (startFrame && endFrame)
                    {
                        const uint32_t currentFrame = shn::clamp(frame, (uint32_t) *startFrame, (uint32_t) *endFrame) - *startFrame;
                        return p["value"][currentFrame];
                    }
                    return p["value"];
                }();

                addShadingParameter(paramName, node.name, paramType.dataType, paramType.isArray, paramValue);
            }

            const auto oslSource = getIfExists<std::string>(n, "source");
            if (oslSource)
            {
                shadingNodeSource(nodeType, oslSource->length(), *oslSource);
            }

            addShadingNode(nodeType, node.name);
        }

        // import connections
        json connections = shadingTree["connections"];
        for (auto conn: connections)
        {
            const std::string srcNode = conn["srcnode"];
            const std::string srcParam = conn["srcparam"];
            const std::string dstNode = conn["dstnode"];
            const std::string dstParam = conn["dstparam"];
            connectShadingNodes(srcNode, srcParam, dstNode, dstParam);
        }
    }
    endShadingTree(shadingTreeId);

    /*
    {
        SHN_LOGINFO << "Added Shading Tree: " << shadingTreeName;
    }
    */

    return 0;
}

OslRenderer::Id OslRendererImpl::getShadingTreeIdFromName(string_view name) const
{
    const auto it = std::find_if(begin(m_shadingTrees), end(m_shadingTrees), [&](const ShadingTree & shadingTree) { return shadingTree.name == name; });
    if (it == end(m_shadingTrees))
        return -1;
    return it - begin(m_shadingTrees);
}

const char * OslRendererImpl::getShadingTreeNameFromId(OslRenderer::Id shadingTreeId) const
{
    if (shadingTreeId >= m_shadingTrees.size())
        return "unnamed_shader";
    return m_shadingTrees[shadingTreeId].name.c_str();
}

OslRenderer::Id OslRendererImpl::getShadingTreeIdFromInstanceId(OslRenderer::Id instanceId, int32_t shadingGroup, Assignment::Type assignType) const
{
    auto & assgmts = m_shadingTreeAssignments[assignType];
    if (instanceId >= assgmts.size() || assgmts[instanceId].shadingTreeIds.empty())
        return -1;
    assert(shadingGroup < (int32_t) assgmts[instanceId].shadingTreeIds.size());

    const auto shadingTreeId = assgmts[instanceId].shadingTreeIds[shadingGroup];
    assert(shadingTreeId < (int32_t) m_shadingTrees.size());
    return assgmts[instanceId].shadingTreeIds[shadingGroup];
}

OslRenderer::Status
OslRendererImpl::assignShadingTree(OslRenderer::Id shadingTreeId, const OslRenderer::Id * instanceIds, const int32_t shadingGroup, int32_t numInstances, Assignment::Type assignType)
{
    if (!numInstances)
    {
        return -1;
    }

    auto & assgmts = m_shadingTreeAssignments[assignType];
    const auto maxId = *std::max_element(instanceIds, instanceIds + numInstances);

    int32_t numAssignments = assgmts.size();
    if (maxId >= numAssignments)
        assgmts.resize(maxId + 1);

    for (int32_t i = 0; i < numInstances; ++i)
    {
        Assignment & assignment = assgmts[instanceIds[i]];

        if (shadingGroup >= assignment.shadingTreeIds.size())
            assignment.shadingTreeIds.resize(shadingGroup + 1, -1);

        assignment.shadingTreeIds[shadingGroup] = shadingTreeId;
    }
    return 0;
}

OslRenderer::Status OslRendererImpl::shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, string_view value)
{
    if (name == "opacity")
    {
        for (int32_t i = 0; i < OPACITY_MODE_END; ++i)
        {
            if (value == OpacityModeNames[i])
            {
                m_shadingTrees[shadingTreeId].opacityMode = OpacityModes[i];
                break;
            }
        }
    }
    else if (name == "type")
    {
        for (int32_t i = 0; i < MATERIAL_TYPE_END; ++i)
        {
            if (value == MaterialTypeNames[i])
            {
                m_shadingTrees[shadingTreeId].type = MaterialTypes[i];
                break;
            }
        }
    }
    return 0;
}

OslRenderer::Status OslRendererImpl::shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, const float value)
{
    assert(false && "No float attributes exists in the renderer !");
    return 0;
}

OslRenderer::Status OslRendererImpl::shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, const int32_t value)
{
    assert(false && "No int attributes exists in the renderer !");
    return 0;
}

OslRenderer::Status OslRendererImpl::shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, const float * values, int32_t count)
{
    assert(false && "No float array attributes exists in the renderer !");
    return 0;
}

OslRenderer::Status OslRendererImpl::shadingTreeAttribute(OslRenderer::Id shadingTreeId, string_view name, const int32_t * values, int32_t count)
{
    assert(false && "No int array attributes exists in the renderer !");
    return 0;
}

OslRenderer::Status OslRendererImpl::setDisplacement(DisplacementData data)
{
    OslRenderer::Id shadingTreeId = getShadingTreeIdFromInstanceId(data.instanceId, 0, Assignment::Displacement);
    if (shadingTreeId < 0)
        return -1;

    data.shadingTreeId = shadingTreeId;
    m_accumDisplaceData.push_back(data);
    return 0;
}

OslRenderer::Status OslRendererImpl::commitDisplacement(SceneImpl * scene)
{
    for (auto & displaceData: m_accumDisplaceData)
    {
        struct ThreadData
        {
            OSL::PerThreadInfo * thread_info = s_shadingSystem->create_thread_info();
            OSL::ShadingContext * ctx = s_shadingSystem->get_context(thread_info);

            ~ThreadData()
            {
                s_shadingSystem->release_context(ctx);
                s_shadingSystem->destroy_thread_info(thread_info);
            }
        };

        syncParallelLoop<ThreadData>(
            displaceData.uniqueVerticesCount, m_rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_THREADCOUNT], defaultMaxThreadCount()),
            [this, scene, &displaceData](size_t vertexId, size_t threadId, const ThreadData & threadData) {
                applyDisplacement(s_shadingSystem.get(), threadData.ctx, m_oslShaderGroups[displaceData.shadingTreeId], displaceData, scene, vertexId);
            });
    }

    // clear displacement data
    m_accumDisplaceData.clear();
    return 0;
}

OslRenderer::Status
OslRendererImpl::render(Scene * scene, OslRenderer::Id cameraId, OslRenderer::Id bindingId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t startSample, int32_t endSample)
{
    assert(bindingId < (int) m_rendererOutputs.size());
    assert(waitBinding(bindingId, 0));

    auto & outputs = *m_rendererOutputs[bindingId];

    int w = outputs.binding.width(), h = outputs.binding.height(); // # samples
    if (w <= 0 || h <= 0)
    {
        return -1;
    }
    if (endx <= startx || endy <= starty)
    {
        return -1;
    }

    {
        const auto lock = outputs.lock();

        assert(outputs.sync._used == 0);
        outputs.sync._used += 1;
        outputs.sync._canceled = 0;

        outputs.finishedSampleCountPerTile.clear();
        outputs.finishedSampleCountPerTile.resize(tileCount(endx - startx) * tileCount(endy - starty), 0); // init -> 0

        // statistics
        GetHighResolutionTime(&outputs.beginTime);
    }

    int32_t timeSeed = m_rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_TIMESEED], 0);
    if (timeSeed)
    {
        float openTime, closeTime = 0.f;
        scene->impl()->getCameraShutter(cameraId, &openTime, &closeTime);
        timeSeed = (int32_t) openTime;
    }

    // for geometry lights, we provide a SceneImpl pointer to Lights and build a emissive instance list
    m_lights.setScene(scene->impl());
    // populate emissiveInstances array
    // We store in a bool array which instance is linked to a GeometryLight
    // It will be used to cut emission on these linked instance in non-straight ray,
    // and then avoid double intensity due to direct lighting and indirect bounces
    m_lights.listEmissiveInstance();

    // Update volume instances counter
    m_volumeInstancesCount = 0;
    m_allVolumeInstanceIds.clear();
    const auto & volumeAssignments = m_shadingTreeAssignments[Assignment::Volume];
    for (Scene::Id instId = 0; instId < (Scene::Id) volumeAssignments.size(); ++instId)
    {
        if (volumeAssignments[instId].shadingTreeIds.empty() || volumeAssignments[instId].shadingTreeIds[0] == -1 || scene->impl()->isHiddenInstance(instId))
            continue;
        m_allVolumeInstanceIds.push_back(instId);
        ++m_volumeInstancesCount;
    }

    if (m_volumeInstancesCount > 0)
    {
        // build map which converts volume ids to index inside m_allVolumeInstanceIds
        for (int32_t i = 0; i < m_volumeInstancesCount; ++i)
            m_volumeIdsToVectorIndex[m_allVolumeInstanceIds[i]] = i;

        // build volume scene for initial "in-volume" test
        scene->impl()->buildVolumeScene(&m_allVolumeInstanceIds[0], m_volumeInstancesCount);
    }

    // The OslRendererTask is shared by all threads. The shared_ptr is copied to all lambdas.
    // When the last thread finishes, the shared_ptr counter goes to zero and the destructor of OslRendererTask is called.
    std::shared_ptr<OslRendererTask> rendererTask =
        std::make_shared<OslRendererTask>(this, m_rendererAttributes, scene, &m_lights, cameraId, startx, endx, starty, endy, startSample, endSample, outputs);
    outputs.rendererTask = rendererTask.get();
    outputs.sampleCountToCompute = endSample - startSample;

    asyncParallelRun(m_rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_THREADCOUNT], defaultMaxThreadCount()), [rendererTask](size_t threadIdx) { rendererTask->renderFrame(); });

    return 0;
}

OslRenderer::Status OslRendererImpl::beginShadingTree(OslRenderer::Id shadingTreeId, string_view name)
{
    assert(shadingTreeId < m_shadingTrees.size());

    m_shadingTrees[shadingTreeId].name = name;
    m_oslShaderGroups[shadingTreeId] = s_shadingSystem->ShaderGroupBegin(name);
    return 0;
}

OslRenderer::Status OslRendererImpl::endShadingTree(OslRenderer::Id shadingTreeId)
{
    s_shadingSystem->ShaderGroupEnd();

    // bake constant opacity
    ShadingTree & shadingTree = m_shadingTrees[shadingTreeId];
    if (shadingTree.opacityMode == OPACITY_MODE_CONSTANT)
    {
        o::ShaderGlobals sg;
        memset(&sg, 0, sizeof(o::ShaderGlobals));
        sg.N = o::Vec3(0.f, 1.f, 0.f);
        sg.raytype = s_shadingSystem->raytype_bit(RayType_Shadow);

        ShadingMemoryPool memPool;
        ClosureTreeEvaluator closureEvaluator{ memPool };
        ShadingContext ctx(*s_shadingSystem, nullptr, m_oslShaderGroups, closureEvaluator);

        shadingTree.transparencyConstant = evalTransparency(ctx, shadingTreeId, sg);
    }

    return 0;
}

OslRenderer::Status OslRendererImpl::clearShadingTrees()
{
    for (size_t i = 0; i < m_rendererOutputs.size(); ++i)
        assert(waitBinding(i, 0));

    for (size_t j = 0; j < Assignment::Count; ++j)
        m_shadingTreeAssignments[j].clear();

    m_shadingTrees.clear();
    m_oslShaderGroups.clear();

    genShadingTrees(&defaultMaterialShadingTreeId, 1);
    beginShadingTree(defaultMaterialShadingTreeId, "default_shader");
    addShadingNode("default_shader", "default_shader");
    endShadingTree(defaultMaterialShadingTreeId);
    return 0;
}

void OslRendererImpl::shadingNodeSource(string_view name, uint32_t count, string_view source)
{
    o::OSLCompiler * comp = new o::OSLCompiler(&OSL::ErrorHandler::default_handler());
    // Compile OSL in OSO
    const std::vector<std::string> options = { "-O2", "-v" };
    std::string osobuffer;
    std::string oslsource(source.c_str(), count);
    bool ok = comp->compile_buffer(oslsource, osobuffer, options, SHADER_STD_INCLUDE_PATH);
    if (!ok)
        SHN_LOGWARN << "Compilation failed on osl source code : " << name;
    // Load OSO in shading system
    s_shadingSystem->LoadMemoryCompiledShader(name, osobuffer);

#if 0
    // Do note delete compiler : multiple memory corruptions
    delete comp;
#endif
}

void OslRendererImpl::addShadingNode(string_view name, string_view layer)
{
    s_shadingSystem->Shader("surface", name, layer);
}

void OslRendererImpl::addShadingParameter(string_view paramName, string_view nodeName, string_view type, bool isArray, const json & value)
{
    if (typeToRawType.at(type) == "string")
    {
        if (value.is_array() || isArray)
        {
            SHN_LOGWARN << "Unsupported string array parameter : " << nodeName << ":" << paramName;
        }
        else
        {
            assert(value.is_string());
            std::string data = value;
            ustring ustringData(data.c_str());
            s_shadingSystem->Parameter(paramName, o::TypeDesc::TypeString, &ustringData);
        }
    }
    else if (typeToRawType.at(type) == "int")
    {
        std::vector<int32_t> data;
        if (value.is_array())
        {
            assert(isArray);
            data.insert(data.begin(), value.begin(), value.end());
            // OSL shaders need arrays with multiple of 16 size
            if (data.size() < PARAMETER_ARRAY_SIZE)
                data.resize(PARAMETER_ARRAY_SIZE);
        }
        else
        {
            data.push_back(value);
        }
        const size_t count = data.size();
        s_shadingSystem->Parameter(paramName, o::TypeDesc(o::TypeDesc::INT, (count > 1) ? count : 0), &data[0]);
    }
    else if (typeToRawType.at(type) == "float")
    {
        const size_t component = typeToComponentCount.at(type);
        std::vector<float> data;
        if (value.is_array())
        {
            data.insert(data.begin(), value.begin(), value.end());
            // OSL shaders need arrays with multiple of 16 size
            if (isArray && data.size() < PARAMETER_ARRAY_SIZE * component)
                data.resize(PARAMETER_ARRAY_SIZE * component);
        }
        else
        {
            data.push_back(value);
        }

        const size_t count = data.size() / component;
        o::TypeDesc::AGGREGATE aggregate = o::TypeDesc::SCALAR;
        if (type == "vector_3" || type == "color")
            aggregate = o::TypeDesc::VEC3;
        if (type == "matrix")
            aggregate = o::TypeDesc::MATRIX44;

        o::TypeDesc::VECSEMANTICS semantics = o::TypeDesc::NOSEMANTICS;
        if (type == "vector_3")
            semantics = o::TypeDesc::VECTOR;
        else if (type == "color")
            semantics = o::TypeDesc::COLOR;

        const size_t arrayLength = (aggregate == o::TypeDesc::SCALAR) ? count * component : count;
        const o::TypeDesc t(o::TypeDesc::FLOAT, aggregate, semantics, (arrayLength > 1) ? arrayLength : 0);

        /*{
            SHN_LOGINFO << ">> Adding Parameter : " << nodeName << ":" << paramName << " (type=" << type << " [count=" << count << "], [component=" << component << "])";
        }*/

        s_shadingSystem->Parameter(paramName, t, &data[0]);
    }
    else if (type == "material")
    {
        // do nothing - material param are just here for connection purpose
    }
    else
    {
        SHN_LOGWARN << "Unsupported parameter type : " << nodeName << ":" << paramName << " (type=" << type << ")";
    }
}

void OslRendererImpl::connectShadingNodes(string_view sourceLayer, string_view sourceParameter, string_view destinationLayer, string_view destinationParameter)
{
    s_shadingSystem->ConnectShaders(sourceLayer, sourceParameter, destinationLayer, destinationParameter);
}

OslRenderer::Status OslRendererImpl::attribute(const char * name, const char * value)
{
    bool oslAttribute = s_shadingSystem->attribute(name, value);
    if (!oslAttribute)
    {
        if (!strcmp(SHN_ATTR_RENDER_RAYFILTER, name))
        {
            if (!strcmp(SHN_RAYFILTER_BSPLINE, value))
                m_rayFilter = makeBSplineFilter();
            else if (!strcmp(SHN_RAYFILTER_TRIANGLE, value))
                m_rayFilter = makeTriangleFilter();
            else if (!strcmp(SHN_RAYFILTER_CENTER, value))
                m_rayFilter = makeCenterFilter();
            else
                m_rayFilter = makeBoxFilter();

            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_COLORSPACE, name))
        {
            s_shadingSystem->attribute("colorspace", value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_TILEPATTERN, name))
        {
            m_rendererAttributes.setString(RENDERER_ATTR_NAMES[R_ATTR_TILEPATTERN], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_ATMOSPHERESHADER, name))
        {
            OslRenderer::Id atmosphereShaderId = getShadingTreeIdFromName(value);
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_ATMOSPHERESHADERID], ATTR_TYPE_NAMES[ATTR_TYPE_INT], atmosphereShaderId);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_DIRECTILLUMALGORITHM, name))
        {
            ustring strValue{ value };
            if (strValue == SHN_DIRECTILLUMALGORITHM_ALLLIGHTS)
            {
                m_directIllumAlgorithm = DirectIllumAlgorithm::AllLights;
            }
            else if (strValue == SHN_DIRECTILLUMALGORITHM_IMPORTANCECACHING)
            {
                m_directIllumAlgorithm = DirectIllumAlgorithm::ImportanceCaching;
            }
            else
            {
                SHN_LOGWARN << "Unknown direct illumination algorithm : " << strValue << ". I'm using " << SHN_DIRECTILLUMALGORITHM_ALLLIGHTS << " instead.";
                m_directIllumAlgorithm = DirectIllumAlgorithm::AllLights;
            }
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_BSSRDFMISHEURISTIC, name))
        {
            ustring strValue{ value };
            if (strValue == SHN_BSSRDFMISHEURISTIC_NONE)
            {
                m_bssrdfMISHeuristic = BSSRDFMISHeuristic::None;
            }
            else if (strValue == SHN_BSSRDFMISHEURISTIC_ALPHAMAX)
            {
                m_bssrdfMISHeuristic = BSSRDFMISHeuristic::AlphaMax;
            }
            else if (strValue == SHN_BSSRDFMISHEURISTIC_MAX)
            {
                m_bssrdfMISHeuristic = BSSRDFMISHeuristic::Max;
            }
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_DEFAULTTOONOUTLINE, name))
        {
            m_defaultToonOutlineShaderId = getShadingTreeIdFromName(name);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_REPORTPATH, name))
        {
            m_rendererAttributes.setString(ustring(SHN_ATTR_RENDER_REPORTPATH), value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_DENOISEPASSLIST, name))
        {
            m_rendererAttributes.setString(ustring(SHN_ATTR_RENDER_DENOISEPASSLIST), value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_LIGHTICSTATSTILELIST, name))
        {
            const auto tileCoords = split(value, " ");
            for (const auto & tile: tileCoords)
            {
                const auto xy = split(tile, ",");
                V2i coords;
                {
                    std::stringstream ss;
                    ss << xy[0].substr(1);
                    ss >> coords.x;
                }
                {
                    std::stringstream ss;
                    ss << xy[1];
                    ss >> coords.y;
                }

                m_lightImportanceCachingStatsTileList.insert(coords);
            }
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_EXEPATH, name))
        {
            m_rendererAttributes.setString(ustring(SHN_ATTR_RENDER_EXEPATH), value);
            return 1;
        }

        SHN_LOGERR << "Unkown renderer attribute " << name;

        return 0;
    }
    return oslAttribute;
}

OslRenderer::Status OslRendererImpl::attribute(const char * name, float value)
{
    bool oslAttribute = s_shadingSystem->attribute(name, value);
    if (!oslAttribute)
    {
        if (!strcmp(SHN_ATTR_RENDER_OCCLUSIONDISTANCE, name))
        {
            m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_OCCDISTANCE], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_OCCLUSIONANGLE, name))
        {
            m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_OCCANGLE], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_DEPTHNEAR, name))
        {
            m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_DEPTHNEAR], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_DEPTHFAR, name))
        {
            m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_DEPTHFAR], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_SHUTTEROFFSET, name))
        {
            m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_SHUTTEROFFSET], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_LOWLIGHTTRESHOLD, name))
        {
            // Low light threshold is multiplied by a constant to respect previous erroneous implementation
            static const float LOW_LIGHT_THRESHOLD_LEGACY_FACTOR = 0.1f;
            m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_LOWLIGHTTHRESHOLD], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], value * LOW_LIGHT_THRESHOLD_LEGACY_FACTOR);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_PIXELSAMPLECLAMP, name))
        {
            if (value != 0.f)
                m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_PIXELSAMPLECLAMP], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], value);
            else
                m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_PIXELSAMPLECLAMP], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], std::numeric_limits<float>::max());
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_ADAPTIVEVARIANCE, name))
        {
            m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_ADAPTIVEVARIANCE], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_RAYTRACEDINDIRECTBIAS, name))
        {
            m_rendererAttributes.set<float>(RENDERER_ATTR_NAMES[R_ATTR_RAYTRACEDINDIRECTBIAS], ATTR_TYPE_NAMES[ATTR_TYPE_FLOAT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_LIGHTICPROBAALPHA, name))
        {
            m_rendererAttributes.set(RENDERER_ATTR_NAMES[R_ATTR_LIGHTICPROBAALPHA], value);
            return 1;
        }
        return 0;
    }
    return oslAttribute;
}

OslRenderer::Status OslRendererImpl::attribute(const char * name, const float * value, int32_t count)
{
    return 0;
}

OslRenderer::Status OslRendererImpl::attribute(const char * name, int value)
{
    bool oslAttribute = s_shadingSystem->attribute(name, value);
    if (!oslAttribute)
    {
        if (!strcmp(SHN_ATTR_RENDER_MAXBOUNCES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_MAXBOUNCES], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_IGNORESTRAIGHTBOUNCES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_IGNORESTRAIGHTBOUNCES], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_DIFFUSEBOUNCES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESDIFFUSE], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_GLOSSYBOUNCES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESGLOSSY], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_REFLECTIONBOUNCES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESREFLECTION], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_REFRACTIONBOUNCES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESREFRACTION], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_TRANSPARENCYBOUNCES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESTRANSPARENCY], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_SHADOWBOUNCES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESSHADOW], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_VOLUMEBOUNCES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESVOLUME], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
        }
        if (!strcmp(SHN_ATTR_RENDER_THREADCOUNT, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_THREADCOUNT], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_BASICCOUNTERS, name))
        {
            int32_t * counter = (int32_t *) m_rendererAttributes.getPtr(RENDERER_ATTR_NAMES[R_ATTR_COUNTER]);
            if (value)
                *counter |= PerformanceCounter_Basic;
            else
                *counter &= ~PerformanceCounter_Basic;
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_INSTANCECOUNTERS, name))
        {
            int32_t * counter = (int32_t *) m_rendererAttributes.getPtr(RENDERER_ATTR_NAMES[R_ATTR_COUNTER]);
            if (value)
                *counter |= PerformanceCounter_Instance;
            else
                *counter &= ~PerformanceCounter_Instance;
            return 1;
        }
        // Shader debug attributes
        if (!strcmp(SHN_ATTR_OSL_DEBUGNAN, name))
        {
            s_shadingSystem->attribute("debugnan", value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_OSL_DEBUGUNINIT, name))
        {
            s_shadingSystem->attribute("debug_uninit", value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_OSL_RANGECHECKING, name))
        {
            s_shadingSystem->attribute("range_checking", value);
            return 1;
        }
        // Shading system attributes
        if (!strcmp(SHN_ATTR_OSL_LOCKGEOM, name))
        {
            s_shadingSystem->attribute("lockgeom", value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_OSL_LAZYLAYERS, name))
        {
            s_shadingSystem->attribute("lazylayers", value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_OSL_LAZYGLOBALS, name))
        {
            s_shadingSystem->attribute("lazyglobals", value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_TIMESEED, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_TIMESEED], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_ADAPTIVEMINSAMPLES, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_ADAPTIVEMINSAMPLES], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_NANHANDLINGPOLICY, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_NANHANDLINGPOLICY], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_VOLUMESKIPINCLUSIONTEST, name))
        {
            m_rendererAttributes.set<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_VOLUMESKIPINCLUSIONTEST], ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_DIRECTILLUMSAMPLECOUNT, name))
        {
            m_directIllumSampleCount = value <= 0 ? 0 : value;
        }
        if (!strcmp(SHN_ATTR_RENDER_BSSRDFRESAMPLING, name))
        {
            m_bssrdfResampling = value ? BSSRDFResampling::True : BSSRDFResampling::False;
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_REPORTPERFIMAGES, name))
        {
            m_rendererAttributes.set<int32_t>(SHN_ATTR_RENDER_REPORTPERFIMAGES, ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        if (!strcmp(SHN_ATTR_RENDER_TOONOUTLINEMINSAMPLES, name))
        {
            m_rendererAttributes.set<int32_t>(SHN_ATTR_RENDER_TOONOUTLINEMINSAMPLES, ATTR_TYPE_NAMES[ATTR_TYPE_INT], value);
            return 1;
        }
        return 0;
    }
    return oslAttribute;
}

OslRenderer::Status OslRendererImpl::getAttribute(OSL::ustring name, const char ** returnedValue)
{
    *returnedValue = m_rendererAttributes.getString(name);
    if (!*returnedValue)
        return 1;
    return 0;
}

OslRenderer::Status OslRendererImpl::getAttribute(OSL::ustring name, std::string & returnedValue)
{
    const char * tmp = nullptr;
    if (getAttribute(name, &tmp) != 0)
        return 1;

    returnedValue = tmp;
    return 0;
}

OslRenderer::Status OslRendererImpl::getAttribute(OSL::ustring(name), float * returnedValue)
{
    const float * value = (const float *) m_rendererAttributes.getPtr(name);
    if (!value)
        return 1;
    *returnedValue = *value;
    return 0;
}

OslRenderer::Status OslRendererImpl::getAttribute(OSL::ustring(name), int32_t * returnedValue)
{
    const int32_t * value = (const int32_t *) m_rendererAttributes.getPtr(name);
    if (!value)
        return 1;
    *returnedValue = *value;
    return 0;
}

namespace
{

const char * getPresetRoleName(const OutputInfo & info)
{
    if (any(info.flags & PassFlags::Render))
        return SHN_PASSROLE_RENDER;
    else if (any(info.flags & PassFlags::Technical))
        return SHN_PASSROLE_TECHNICAL;
    else if (any(info.flags & PassFlags::Stats))
        return SHN_PASSROLE_STATS;
    return SHN_PASSROLE_DEBUG;
}
} // namespace

OslRenderer::Status OslRendererImpl::getAttributeBufferByteSize(ustring name, uint32_t * outByteSize)
{
    assert(outByteSize);

    if (name == SHN_ATTR_RENDER_PASS_NAMES)
    {
        uint32_t count = 0;
        for (size_t i = 0; i < getOutputPresetCount(); ++i)
            count += strlen(getOutputPreset(i).name.c_str()) + 1; // +1 for \0
        *outByteSize = count;
        return 0;
    }

    if (name == SHN_ATTR_RENDER_PASS_ROLES)
    {
        uint32_t count = 0;
        for (size_t i = 0; i < getOutputPresetCount(); ++i)
        {
            const auto roleName = getPresetRoleName(getOutputPreset(i));
            count += strlen(roleName) + 1;
        }
        *outByteSize = count;
        return 0;
    }

    return -1;
}

OslRenderer::Status OslRendererImpl::getAttribute(ustring name, uint32_t count, char * bufferOut)
{
    assert(bufferOut);

    const auto startBufferOut = bufferOut;

    if (name == SHN_ATTR_RENDER_PASS_NAMES)
    {
        for (size_t i = 0; i < getOutputPresetCount(); ++i)
        {
            strcpy(bufferOut, getOutputPreset(i).name.c_str());
            bufferOut += strlen(getOutputPreset(i).name.c_str()) + 1;
        }
        assert((bufferOut - startBufferOut) == count);
        return ((bufferOut - startBufferOut) == count) ? 0 : -2;
    }

    if (name == SHN_ATTR_RENDER_PASS_ROLES)
    {
        for (size_t i = 0; i < getOutputPresetCount(); ++i)
        {
            const auto roleName = getPresetRoleName(getOutputPreset(i));
            strcpy(bufferOut, roleName);
            bufferOut += strlen(roleName) + 1;
        }
        assert((bufferOut - startBufferOut) == count);
        return ((bufferOut - startBufferOut) == count) ? 0 : -2;
    }

    return -1;
}

OslRenderer::Status OslRendererImpl::genLights(OslRenderer::Id * lightIds, int32_t count)
{
    return m_lights.genLights(lightIds, count);
}

int32_t OslRendererImpl::countLightIdsByName(const char * name) const
{
    return m_lights.countLightIdsByName(name);
}

OslRenderer::Status OslRendererImpl::lightIdsByName(const char * name, OslRenderer::Id * lightsIds, int32_t count) const
{
    return m_lights.lightIdsByName(name, lightsIds, count);
}

OslRenderer::Status OslRendererImpl::lightAttribute(OslRenderer::Id lightId, ustring attrName, const char * attrType, int32_t dataByteSize, const char * data)
{
    return m_lights.setLightAttribute(lightId, data, dataByteSize, attrName, attrType);
}

OslRenderer::Status OslRendererImpl::commitLight(OslRenderer::Id lightId)
{
    ShadingMemoryPool memPool;
    ClosureTreeEvaluator closureEvaluator{ memPool };
    ShadingContext lsCtx(*s_shadingSystem, nullptr, m_oslShaderGroups, closureEvaluator);

    auto status = m_lights.commitLight(lightId, lsCtx);

    return status;
}

OslRenderer::Status OslRendererImpl::commitLights()
{
    return m_lights.commitLights();
}

OslRenderer::Status OslRendererImpl::clearLights()
{
    return m_lights.clearLights();
}

bool OslRendererImpl::isEmissiveInstance(const char* instanceName) const
{
   return m_lights.isEmissiveInstance(instanceName);
}

const float * OslRendererImpl::getFramebufferData(OslRenderer::Id framebufferId) const
{
    assert(framebufferId < m_framebuffers.size());
    return m_framebuffers[framebufferId]->data();
}

int32_t OslRendererImpl::getFramebufferChannelCount(OslRenderer::Id framebufferId) const
{
    assert(framebufferId < m_framebuffers.size());
    return m_framebuffers[framebufferId]->channelCount();
}

OslRenderer::Status OslRendererImpl::getFramebufferData(
    OslRenderer::Id framebufferId, OslRenderer::Id bindingId, int32_t width, int32_t height, int32_t channelCount, OslRenderer::FramebufferFormat format, float * data) const
{
    assert(framebufferId < m_framebuffers.size());
    const Framebuffer * fb = m_framebuffers[framebufferId].get();
    memcpy(data, fb->data(), width * height * channelCount * sizeof(*data));

    // normalize buffer by sampleCount if bound pass need normalization
    const Binding & binding = m_rendererOutputs[bindingId]->binding;
    const auto it = std::find_if(std::begin(binding.outputs()), std::end(binding.outputs()), [&](const OutputPass & output) { return fb == output.framebuffer; });
    if (it != std::end(binding.outputs()))
    {
        if (none((*it).info.flags & PassFlags::NoNormalization))
        {
            const float * samplesBuffer = binding.sampleCountBuffer();
            size_t pixelCount = width * height;
            for (int32_t i = 0; i < pixelCount; ++i)
            {
                float sample = samplesBuffer[i];
                if (sample > 0)
                {
                    for (int32_t k = 0; k < channelCount; ++k)
                        data[channelCount * i + k] /= sample;
                }
            }
        }
    }
    return 0;
}

OslRenderer::Status OslRendererImpl::getFramebufferData(
    OslRenderer::Id framebufferId, OslRenderer::Id bindingId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t channelCount, OslRenderer::FramebufferFormat format,
    float * data, int32_t dataStride) const
{
    assert(framebufferId < m_framebuffers.size());
    const Framebuffer * fb = m_framebuffers[framebufferId].get();
    int32_t slice_width = (endx - startx) * channelCount;
    float * data_ptr = data;
    for (int32_t i = starty; i < endy; ++i)
    {
        memcpy(data_ptr, fb->data() + (i * fb->width() + startx) * channelCount, slice_width * sizeof(*data));
        data_ptr += dataStride;
    }

    // normalize buffer by sampleCount if bound pass need normalization
    const Binding & binding = m_rendererOutputs[bindingId]->binding;
    const auto it = std::find_if(std::begin(binding.outputs()), std::end(binding.outputs()), [&](const OutputPass & output) { return fb == output.framebuffer; });
    if (it != std::end(binding.outputs()))
    {
        const OutputPass & output = (*it);
        if (none(output.info.flags & PassFlags::NoNormalization))
        {
            const float * samplesBuffer = binding.sampleCountBuffer();
            int32_t width = endx - startx;
            int32_t height = endy - starty;
            size_t pixelCount = width * height;
            for (int32_t j = 0; j < height; ++j)
            {
                for (int32_t i = 0; i < width; ++i)
                {
                    int32_t samplePixelId = (j + starty) * fb->width() + (i + startx);
                    int32_t dataPixelId = (j * dataStride) + (i * channelCount);
                    float sample = samplesBuffer[samplePixelId];
                    if (sample > 0.f)
                    {
                        for (int32_t k = 0; k < channelCount; ++k)
                            data[dataPixelId + k] /= sample;
                    }
                    if (channelCount == 4)
                    {
                        data[dataPixelId + 3] = max(0.f, data[dataPixelId + 3]);
                    }
                }
            }
        }
    }
    return 0;
}

OslRenderer::Status OslRendererImpl::clearFramebuffer(OslRenderer::Id framebufferId, int32_t channelCount, const float * clearValue)
{
    assert(framebufferId < m_framebuffers.size());
    const Framebuffer & fb = *m_framebuffers[framebufferId];
    return clearSubFramebuffer(framebufferId, 0, fb.width(), 0, fb.height(), channelCount, clearValue);
}

OslRenderer::Status
OslRendererImpl::clearSubFramebuffer(OslRenderer::Id framebufferId, int32_t startx, int32_t endx, int32_t starty, int32_t endy, int32_t channelCount, const float * clearValue)
{
    assert(framebufferId < m_framebuffers.size());
    Framebuffer & fb = *m_framebuffers[framebufferId];

    float * buffer = fb.data();
    for (int32_t i = startx; i < endx; ++i)
    {
        for (int32_t j = starty; j < endy; ++j)
        {
            for (int32_t k = 0; k < channelCount; ++k)
            {
                buffer[fb.channelCount() * (i + j * fb.width()) + k] = clearValue[k];
            }
        }
    }
    return 0;
}

OslRenderer::Status OslRendererImpl::genBindings(OslRenderer::Id * bindingsIds, int32_t count)
{
    int32_t firstId = (int) m_rendererOutputs.size();
    m_rendererOutputs.resize(m_rendererOutputs.size() + count);
    for (int32_t i = 0; i < count; ++i)
    {
        const auto id = firstId + i;
        m_rendererOutputs[id] = std::make_unique<RendererOutputs>(*this);
        bindingsIds[i] = id;
    }
    return 0;
}

namespace
{

string_view getPresetName(string_view location)
{
    if (!starts_with(location, "pass:"))
        return string_view{};
    return location.substr(sizeof("pass:") - 1);
}

int32_t getBindingLocationIdx(string_view location)
{
    const auto passName = getPresetName(location);
    return getOutputPresetIndex(ustring(passName));
}

} // namespace

int32_t OslRendererImpl::getBindingLocationChannelCount(const char * location) const
{
    int32_t locationIdx = getBindingLocationIdx(location);
    if (locationIdx >= 0)
        return getOutputPreset(locationIdx).channelCount;
    return -1;
}

inline const Framebuffer * getFramebuffer(const Binding & binding, string_view name, bool denoised)
{
    const auto outputIdx = binding.getOutputIdx(name);
    if (outputIdx < 0)
        return nullptr;
    return denoised ? binding.getOutput(outputIdx).denoisedFramebuffer : binding.getOutput(outputIdx).framebuffer;
}

OslRenderer::Status OslRendererImpl::getBindingLocationFramebuffer(OslRenderer::Id bindingId, string_view location, OslRenderer::Id * outFramebufferId) const
{
    if (bindingId >= (int) m_rendererOutputs.size())
        return -1;
    const Binding & b = m_rendererOutputs[bindingId]->binding;

    // Check if we are asking for a variation of the framebuffer (e.g. denoised)
    const auto dotPos = location.find(".");
    string_view name;
    bool denoised = false;
    if (dotPos == std::string::npos)
        name = getPresetName(location);
    else
    {
        const auto tokens = split(location, ".");
        if (tokens.size() != 2)
        {
            SHN_LOGERR << "Bad binding location " << location;
            return -1;
        }

        if (tokens[1] == "denoised")
        {
            name = getPresetName(tokens[0]);
            denoised = true;
        }
        else
        {
            SHN_LOGERR << "Unknown binding attribute " << tokens[1] << " asked for location " << location;
            return -1;
        }
    }

    const auto framebuffer = getFramebuffer(b, name, denoised);
    if (!framebuffer)
    {
        *outFramebufferId = -1;
        return 0;
    }

    const auto it = std::find_if(begin(m_framebuffers), end(m_framebuffers), [&](const std::unique_ptr<Framebuffer> & fb) { return fb.get() == framebuffer; });
    assert(it != end(m_framebuffers));
    *outFramebufferId = it - begin(m_framebuffers);

    return 0;
}

OslRenderer::Status OslRendererImpl::isBound(OslRenderer::Id bindingId, OslRenderer::Id framebufferId) const
{
    if (bindingId >= (int) m_rendererOutputs.size() || framebufferId >= (int) m_framebuffers.size() || framebufferId < 0)
    {
        return -1;
    }
    const Binding & b = m_rendererOutputs[bindingId]->binding;
    return end(b.outputs()) != std::find_if(begin(b.outputs()), end(b.outputs()), [&](const auto & output) { return output.framebuffer == m_framebuffers[framebufferId].get(); });
}

OslRenderer::Status OslRendererImpl::setBindingMask(OslRenderer::Id bindingId, const float * mask, int32_t squareSize)
{
    if (bindingId >= (int) m_rendererOutputs.size() || squareSize < 1 || waitBinding(bindingId, 0) == 0)
    {
        return -1;
    }
    auto & ouputs = *m_rendererOutputs[bindingId];
    if (ouputs.maskSize < squareSize)
    {
        ouputs.mask.resize(squareSize * squareSize);
    }
    std::copy(mask, mask + squareSize * squareSize, ouputs.mask.begin());
    ouputs.maskSize = squareSize;
    return 0;
}

OslRenderer::Status OslRendererImpl::waitBinding(OslRenderer::Id bindingId, int timeout_ms) const
{
    if (bindingId >= (int) m_rendererOutputs.size())
    {
        return -1;
    }
    RendererOutputs & outputs = *m_rendererOutputs[bindingId];
    std::unique_lock<std::mutex> lock = outputs.lock();
    auto pred = [&outputs]() { return outputs.sync._used == 0; };
    if (timeout_ms == 0)
    {
        return pred() ? 1 : 0;
    }
    else if (timeout_ms < 0)
    {
        outputs.sync.condition.wait(lock, pred);
        assert(pred());
        return 1;
    }
    else
    {
        bool result = outputs.sync.condition.wait_for(lock, std::chrono::milliseconds(timeout_ms), pred);
        assert(result == pred());
        return result ? 1 : 0;
    }
    return 0;
}

int32_t OslRendererImpl::bindingRenderedTileCount(OslRenderer::Id bindingId, int32_t * totalCount) const
{
    if (bindingId >= (int) m_rendererOutputs.size())
    {
        return -1;
    }
    const RendererOutputs & outputs = *m_rendererOutputs[bindingId];
    *totalCount = (int32_t) outputs.finishedSampleCountPerTile.size();
    return (int32_t) std::count(outputs.finishedSampleCountPerTile.begin(), outputs.finishedSampleCountPerTile.end(), outputs.sampleCountToCompute);
}

OslRenderer::Status OslRendererImpl::bindingRenderedTiles(OslRenderer::Id bindingId, int32_t * tiles, const int32_t count, int32_t * tileSize) const
{
    if (bindingId >= (int) m_rendererOutputs.size() || count <= 0)
    {
        return -1;
    }
    const RendererOutputs & outputs = *m_rendererOutputs[bindingId];

    *tileSize = OslRendererTask::TILE_SIZE;

    int32_t current = 0;
    auto it = outputs.finishedSampleCountPerTile.begin();
    while (current != count)
    {
        it = std::find(it, outputs.finishedSampleCountPerTile.end(), outputs.sampleCountToCompute);
        assert(it != outputs.finishedSampleCountPerTile.end());

        if (it == outputs.finishedSampleCountPerTile.end())
        {
            // Should not happened if shining is used correctly
            return -1;
        }

        const auto tileId = std::distance(outputs.finishedSampleCountPerTile.begin(), it);
        tiles[current] = tileId;

        ++current;
        ++it;
    }
    return 0;
}

OslRenderer::Status OslRendererImpl::bindingEnablePass(OslRenderer::Id bindingId, const char * location)
{
    if (bindingId >= (int) m_rendererOutputs.size())
        return -1;

    RendererOutputs & outputs = *m_rendererOutputs[bindingId];
    const int32_t locationIdx = getBindingLocationIdx(location);
    if (locationIdx < 0)
        return -1;
    outputs.outputPresetEnabled[locationIdx] = true;
}

OslRenderer::Status OslRendererImpl::bindingInit(OslRenderer::Id bindingId, int32_t width, int32_t height)
{
    if (bindingId >= (int) m_rendererOutputs.size())
        return -1;

    RendererOutputs & outputs = *m_rendererOutputs[bindingId];
    auto & binding = outputs.binding;

    for (size_t i = 0; i < getOutputPresetCount(); ++i)
    {
        if (outputs.outputPresetEnabled[i])
        {
            const auto & preset = getOutputPreset(i);
            if (binding.getOutputIdx(preset.name) < 0)
                binding.addOutput(preset);
            else
                SHN_LOGERR << "Binding output with name " << preset.name << " already exists"; // Should never happen
        }
    }

    std::string denoisePassList;
    if (0 == getAttribute(ustring(SHN_ATTR_RENDER_DENOISEPASSLIST), denoisePassList))
    {

        if (denoisePassList == "*") // All passes must be denoised
        {
            for (size_t i = 0; i < binding.getOutputCount(); ++i)
                binding.enableDenoising(i);
        }
        else
        {
            const auto passes = split(denoisePassList, " ");
            for (const auto & pass: passes)
            {
                const auto outputIdx = binding.getOutputIdx(pass);
                if (outputIdx >= 0)
                    binding.enableDenoising(outputIdx);
            }
        }
    }

    binding.init(width, height);
}

OslRenderer::Status OslRendererImpl::bindingPostProcess(OslRenderer::Id bindingId)
{
    if (bindingId >= (int) m_rendererOutputs.size())
        return -1;

    Binding & b = m_rendererOutputs[bindingId]->binding;

    int32_t NaNPolicy = SHN_NANHANDLINGPOLICY_DROP;
    getAttribute(RENDERER_ATTR_NAMES[R_ATTR_NANHANDLINGPOLICY], &NaNPolicy);

    for (size_t i = 0; i < b.getOutputCount(); ++i)
    {
        auto & output = b.getOutput(i);
        if (none(output.info.flags & PassFlags::NoNormalization))
            normalize(*output.framebuffer, b.sampleCountBuffer(), NaNPolicy == SHN_NANHANDLINGPOLICY_DROP);

        if (output.info.channelCount == 4)
        {
            for (auto y = 0; y < b.height(); ++y)
            {
                for (auto x = 0; x < b.width(); ++x)
                {
                    auto pixelColor = ptr(*output.framebuffer, x, y);
                    pixelColor[3] = max(pixelColor[3], 0.f);
                }
            }
        }
    }

    // Denoise framebuffers
    for (size_t i = 0; i < b.getOutputCount(); ++i)
    {
        auto & output = b.getOutput(i);
        if (output.sampleAccumulator)
        {
            const auto sampleStatistics = output.sampleAccumulator->extractSamplesStatistics();

            bcd::DenoiserInputs inputs;
            inputs.m_pColors = &sampleStatistics.m_meanImage;
            inputs.m_pNbOfSamples = &sampleStatistics.m_nbOfSamplesImage;
            inputs.m_pHistograms = &sampleStatistics.m_histoImage;
            inputs.m_pSampleCovariances = &sampleStatistics.m_covarImage;

            bcd::Deepimf outputDenoisedColorImage(sampleStatistics.m_meanImage);
            bcd::DenoiserOutputs outputs;
            outputs.m_pDenoisedColors = &outputDenoisedColorImage;

            bcd::DenoiserParameters parameters;
            //parameters.m_useRandomPixelOrder = true;
            parameters.m_useCuda = false;

            const auto nbOfScales = 3;
            bcd::MultiscaleDenoiser denoiser{ nbOfScales };

            denoiser.setInputs(inputs);
            denoiser.setOutputs(outputs);
            denoiser.setParameters(parameters);

            denoiser.denoise();

            for (auto y = 0; y < b.height(); ++y)
            {
                for (auto x = 0; x < b.width(); ++x)
                {
                    auto pixelColor = ptr(*output.denoisedFramebuffer, x, y);
                    pixelColor[0] = outputDenoisedColorImage.get(x, y, 0);
                    pixelColor[1] = outputDenoisedColorImage.get(x, y, 1);
                    pixelColor[2] = outputDenoisedColorImage.get(x, y, 2);
                    pixelColor[3] = ptr(*output.framebuffer, x, y)[3]; // copy alpha from original framebuffer
                }
            }
        }
    }
}

OslRenderer::Status OslRendererImpl::cancelRender(OslRenderer::Id bindingId)
{
    if (bindingId >= (int) m_rendererOutputs.size())
    {
        return -1;
    }
    auto & outputs = *m_rendererOutputs[bindingId];
    const auto lock = outputs.lock();
    outputs.sync._canceled = 1;
    return 0;
}

OslRenderer::Status OslRendererImpl::progressFunctions(void (*sampleProgress)(int32_t, void *), void (*tileProgress)(int32_t, void *), void * appData)
{
    for (size_t i = 0; i < m_rendererOutputs.size(); ++i)
    {
        assert(waitBinding(i, 0));
    }

    m_progressSampleFunc = sampleProgress;
    m_progressTileFunc = tileProgress;
    m_progressAppData = appData;
    return 0;
}

OslRenderer::Status OslRendererImpl::statistics(const char * propertyName, float * value) const
{
    const StatInfo * it = std::find_if(STATISTIC_INFO, STATISTIC_INFO + STAT_MAX, [&](const StatInfo & info) { return info.name == propertyName; });
    const size_t offset = std::distance(STATISTIC_INFO, it);
    assert(offset < sizeof(m_perfCounters) / sizeof(*m_perfCounters));
    if (it == STATISTIC_INFO + STAT_MAX || !statisticIsFloat(offset))
    {
        return -1;
    }
    *value = m_perfCounters[offset].f;
    return 0;
}

OslRenderer::Status OslRendererImpl::statistics(const char * propertyName, int * value) const
{
    const StatInfo * it = std::find_if(STATISTIC_INFO, STATISTIC_INFO + STAT_MAX, [&](const StatInfo & info) { return info.name == propertyName; });
    const size_t offset = std::distance(STATISTIC_INFO, it);
    assert(offset < sizeof(m_perfCounters) / sizeof(*m_perfCounters));
    if (it == STATISTIC_INFO + STAT_MAX || statisticIsFloat(offset))
    {
        if (strcmp(propertyName, "internal") == 0)
        {
#ifdef NDEBUG // debug/release mismatch
            std::cout << s_shiningRendererServices->texturesys()->getstats(5) << std::endl;
            std::cout << s_shadingSystem->getstats(5) << std::endl;
#endif
        }
        return -1;
    }
    *value = m_perfCounters[offset].i;
    return 0;
}

// #todo should be related to a given renderer output
OslRenderer::Status OslRendererImpl::statisticsReport(std::ostream & out) const
{
    /*
    JSON format:
    {'report':
    [{name: 'counter1', value: value1},
    {name: 'counter5', value: value5}
    ],
    'texture_report': OIIO formatted string,
    'shading_report': OSL formatted string
    }
    */
    json statArray = { { { "name", "lightCount_count" }, { "value", m_lights.allLightCount() } }, { { "name", "lightCount_visible_count" }, { "value", m_lights.visibleLightCount() } } };

    for (int32_t i = LightType_Point; i < LightType_Count; ++i)
    {
        statArray.emplace_back(json{ { "name", "lightCount_visible_" + lightTypeToString(LightType(i)).string() + "_count" }, { "value", m_lights.visibleLightCount(LightType(i)) } });
    }

    statArray.emplace_back(json{ { "name", "lightCount_hidden_count" }, { "value", m_lights.allLightCount() - m_lights.visibleLightCount() } });

    for (int i = 0; i != STAT_MAX; ++i)
    {
        if (statisticIsFloat(i))
        {
            statArray.emplace_back(json{ { "name", STATISTIC_INFO[i].typedName().c_str() }, { "value", m_perfCounters[i].f } });
        }
        else
        {
            statArray.emplace_back(json{ { "name", STATISTIC_INFO[i].typedName().c_str() }, { "value", m_perfCounters[i].i } });
        }
    }

    json parent = { { "report", statArray } };

#ifdef NDEBUG // debug/release mismatch
    parent["texture_report"] = s_shiningRendererServices->texturesys()->getstats(5);
    parent["osl_report"] = s_shadingSystem->getstats(5);
#endif

    json visibleLights;
    for (size_t lightIdx = 0; lightIdx < m_lights.visibleLightCount(); ++lightIdx)
    {
        const auto light = m_lights.visibleLights()[lightIdx];
        visibleLights.emplace_back(light->serialize());
    }

    json lightStats;
    for (size_t lightIdx = 0; lightIdx < m_lights.visibleLightCount(); ++lightIdx)
    {
        lightStats.emplace_back(serialize(m_lightStatistics[lightIdx]));
    }

    parent["lights_report"] = { { "visible_lights", visibleLights }, { "visible_light_stats", lightStats } };

    if (!m_lightImportanceCacheTileStatistics.empty())
    {
        parent["lights_report"]["importance_caching"] = json{ { "tiles", m_lightImportanceCacheTileStatistics } };
    }

    parent["tileCount"] = { m_rendererOutputs.back()->binding.width() / OslRendererTask::TILE_SIZE, m_rendererOutputs.back()->binding.height() / OslRendererTask::TILE_SIZE };

    out << parent.dump(-1);

    return 0;
}

OslRenderer::Status OslRendererImpl::setDefaultShaderPath(const char * shaderPath)
{
    Default_ShaderPath = oiio::ustring(shaderPath);
    return 0;
}

OslRenderer::Status OslRendererImpl::setShaderStdIncludePath(const char * includePath)
{
    auto includePathCopy = std::string(includePath);
    std::replace(includePathCopy.begin(), includePathCopy.end(), '\\', '/');
    SHADER_STD_INCLUDE_PATH = oiio::ustring(includePathCopy);
    return 0;
}

int32_t OslRendererImpl::getAtmosphereShaderId() const
{
    return m_rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_ATMOSPHERESHADERID], -1);
}

bool OslRendererImpl::isPerformanceCounterEnabled(PerformanceCounter value) const
{
    const int32_t * counter = (const int32_t *) m_rendererAttributes.getPtr(RENDERER_ATTR_NAMES[R_ATTR_COUNTER]);
    return (*counter & value) > 0;
}

static int32_t g_maxThreadCount =
#ifndef NDEBUG
    1;
#else
    getSystemThreadCount();
#endif

void setDefaultMaxThreadCount(int32_t count)
{
    if (count < 0)
    {
        count = getSystemThreadCount();
    }
    else
    {
        count = std::max(count, 1);
        count = std::min(count, (int32_t) getSystemThreadCount());
    }
    g_maxThreadCount = count;
}

int32_t defaultMaxThreadCount()
{
    return g_maxThreadCount;
}

} // namespace shn