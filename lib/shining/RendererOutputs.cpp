#include "RendererOutputs.h"
#include "Binding.h"

namespace shn
{

RendererOutputs::RendererOutputs(OslRendererImpl & renderer): binding(renderer), outputPresetEnabled(getOutputPresetCount(), false)
{
}

} // namespace shn