#include "Attributes.h"
#include "IMathUtils.h"
#include "SceneImpl.h"
#include "Shining_p.h"
#include <numeric>

namespace shn
{

#ifdef SHN_CONVERT_CURVES_TO_TRIANGLES

static size_t computeCurvesEdgeCount(const int32_t * curvesSize, size_t curveCount)
{
    return std::accumulate(curvesSize, curvesSize + curveCount, 0, [](int currentEdgeCount, int curveVertexCount) { return currentEdgeCount + (curveVertexCount - 1); });
}

static void convertCurvesTopologyToTrianglesTopology(size_t cubicBezierCurvesCount, std::vector<int32_t> & positionTopology, std::vector<int32_t> & normalTopology)
{
    const auto triangleCount = 2 * cubicBezierCurvesCount;

    for (int32_t j = 0; j < cubicBezierCurvesCount; ++j)
    {
        positionTopology.emplace_back(4 * j);
        positionTopology.emplace_back(4 * j + 1);
        positionTopology.emplace_back(4 * j + 3);

        positionTopology.emplace_back(4 * j + 1);
        positionTopology.emplace_back(4 * j + 2);
        positionTopology.emplace_back(4 * j + 3);

        for (auto i = 0u; i < 6u; ++i)
            normalTopology.emplace_back(j);
    }
}

static void convertCurvesGeometryToTrianglesGeometry(
    const float * positions, // positions + radius
    const int32_t * topology, size_t cubicBezierCurvesCount, std::vector<float> & positionAttr, std::vector<float> & normalAttr, Imath::Box3f & bbox, bool addPositionFourthComponent)
{
    auto addPosition = [&](e::Vec3fa p) {
        positionAttr.emplace_back(p.x);
        positionAttr.emplace_back(p.y);
        positionAttr.emplace_back(p.z);

        if (addPositionFourthComponent)
        {
            positionAttr.emplace_back(1);
        }

        bbox.extendBy(Imath::V3f(p.x, p.y, p.z));
    };

    auto vertexOffset = 0;
    auto vertexCount = 0;
    for (int32_t j = 0; j < cubicBezierCurvesCount; ++j)
    {
        auto originPointIdx = topology[j];
        auto dstPointIdx = originPointIdx + 3;

        auto originPoint = e::Vec3fa(positions[originPointIdx * 4], positions[originPointIdx * 4 + 1], positions[originPointIdx * 4 + 2]);
        auto originRadius = positions[originPointIdx * 4 + 3];
        auto dstPoint = e::Vec3fa(positions[dstPointIdx * 4], positions[dstPointIdx * 4 + 1], positions[dstPointIdx * 4 + 2]);
        auto dstRadius = positions[dstPointIdx * 4 + 3];

        auto dir = dstPoint - originPoint;
        dir /= e::length(dir);
        auto f = e::frame(dir);
        auto dirOrtho = f.vx;

        auto a = originPoint - originRadius * dirOrtho;
        auto b = dstPoint - dstRadius * dirOrtho;
        auto c = dstPoint - dstRadius * dirOrtho;
        auto d = originPoint + originRadius * dirOrtho;

        addPosition(a);
        addPosition(b);
        addPosition(c);
        addPosition(d);

        auto normal = e::normalize(e::cross(b - a, d - a));
        normalAttr.emplace_back(normal.x);
        normalAttr.emplace_back(normal.y);
        normalAttr.emplace_back(normal.z);
    }
}

static void convertCurvesToTriangles(
    const float * positions, const int32_t * topology, size_t cubicBezierCurvesCount, std::vector<float> & positionAttr, std::vector<float> & normalAttr,
    std::vector<int32_t> & positionTopology, std::vector<int32_t> & normalTopology, Imath::Box3f & bbox, bool addPositionFourthComponent)
{
    convertCurvesTopologyToTrianglesTopology(cubicBezierCurvesCount, positionTopology, normalTopology);
    convertCurvesGeometryToTrianglesGeometry(positions, topology, cubicBezierCurvesCount, positionAttr, normalAttr, bbox, addPositionFourthComponent);
}

void SceneImpl::convertCurvesToTriangles(Scene::Id meshId)
{
    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);

    auto frameCount = m->vertexAttributes[m->positionAttrIndex].frameCount;

    if (frameCount == 1)
    {
        const float * positions = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat;

        const int32_t * const topology = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].dataInt;
        auto cubicBezierCurveCount = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].count;

        std::vector<float> positionAttr;
        std::vector<float> normalAttr;
        std::vector<int32_t> positionTopology;
        std::vector<int32_t> normalTopology;
        Imath::Box3f bbox;

        convertCurvesToTriangles(positions, topology, cubicBezierCurveCount, positionAttr, normalAttr, positionTopology, normalTopology, bbox, false);

        m->bounds = bbox;
        m->faces.count = positionTopology.size() / 3;
        m->faces.dataInt = nullptr;

        int32_t primitiveType = int32_t(PrimitiveType::TRIANGLES);
        m->processingAttributes.set<int32_t>(Attr_PrimitiveType, ATTR_TYPE_NAMES[ATTR_TYPE_INT], &primitiveType, 1, 1, Scene::COPY_BUFFER);

        enableMeshTopology(meshId, 2);
        meshTopologyData(meshId, 0, positionTopology.size(), (const int32_t *) positionTopology.data(), Scene::COPY_BUFFER);
        meshTopologyData(meshId, 1, normalTopology.size(), (const int32_t *) normalTopology.data(), Scene::COPY_BUFFER);

        enableMeshVertexAttributes(meshId, 2);
        meshVertexAttributeData(meshId, 0, Attr_Position, positionAttr.size() / 3, 3, (const float *) positionAttr.data(), Scene::COPY_BUFFER);
        meshVertexAttributeData(meshId, 1, Attr_Normal, normalAttr.size() / 3, 3, (const float *) normalAttr.data(), Scene::COPY_BUFFER);
    }
    else
    {
        Imath::Box3f bbox;
        const int32_t * const topology = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].dataInt;
        auto cubicBezierCurveCount = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].count;

        std::vector<float> positionAttr;
        std::vector<float> normalAttr;
        std::vector<int32_t> positionTopology;
        std::vector<int32_t> normalTopology;

        std::vector<float> timeValues;

        convertCurvesTopologyToTrianglesTopology(cubicBezierCurveCount, positionTopology, normalTopology);

        for (int32_t i = 0; i < frameCount; ++i)
        {
            const float * positions = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat[i];

            convertCurvesGeometryToTrianglesGeometry(positions, topology, cubicBezierCurveCount, positionAttr, normalAttr, bbox, false);

            timeValues.emplace_back(m->vertexAttributes[m->positionAttrIndex].timeValues[i]);
        }

        auto positionCountPerFrame = positionAttr.size() / (3 * frameCount); // OK
        auto normalCountPerFrame = normalAttr.size() / (3 * frameCount);

        std::vector<float *> positionPointerArray;
        std::vector<float *> normalPointerArray;
        for (int32_t i = 0; i < frameCount; ++i)
        {
            positionPointerArray.push_back(positionAttr.data() + i * 3 * positionCountPerFrame);
            normalPointerArray.push_back(normalAttr.data() + i * 3 * normalCountPerFrame);
        }

        m->bounds = bbox;
        m->faces.count = positionTopology.size() / 3;
        m->faces.dataInt = nullptr;

        int32_t primitiveType = int32_t(PrimitiveType::TRIANGLES);
        m->processingAttributes.set<int32_t>(Attr_PrimitiveType, ATTR_TYPE_NAMES[ATTR_TYPE_INT], &primitiveType, 1, 1, Scene::COPY_BUFFER);

        enableMeshTopology(meshId, 2);
        meshTopologyData(meshId, 0, positionTopology.size(), (const int32_t *) positionTopology.data(), Scene::COPY_BUFFER);
        meshTopologyData(meshId, 1, normalTopology.size(), (const int32_t *) normalTopology.data(), Scene::COPY_BUFFER);

        enableMeshVertexAttributes(meshId, 2);

        meshVertexAttributeData(meshId, 0, Attr_Position, positionCountPerFrame, 3, (const float **) positionPointerArray.data(), timeValues.data(), frameCount, Scene::COPY_BUFFER);
        meshVertexAttributeData(meshId, 1, Attr_Normal, normalCountPerFrame, 3, (const float **) normalPointerArray.data(), timeValues.data(), frameCount, Scene::COPY_BUFFER);
    }

    m->texCoordAttrIndex = -1; // Cancel texCoords mapping
}

#endif

int32_t SceneImpl::buildCurvesStaticInstancedAccel(Scene::Id meshId)
{
    struct Vertex
    {
        float x, y, z, r;
    };

    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);

    assert(m->positionAttrIndex >= 0 && "NO POSITION ATTRIBUTE");

    const auto * positions = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat; // positions also contain radius as fourth component
    const auto positionCount = m->vertexAttributes[m->positionAttrIndex].count;

    auto cubicCurveCount = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].count;
    const int32_t * const cubicCurveTopology = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].dataInt;

    RTCScene scene = rtcDeviceNewScene(m_rtcDevice, RTC_SCENE_STATIC, getRTCAlgorithmFlags());

    unsigned rtcCurvesId = rtcNewHairGeometry(scene, RTC_GEOMETRY_STATIC, cubicCurveCount, positionCount);

    auto * vertices = (float *) rtcMapBuffer(scene, rtcCurvesId, RTC_VERTEX_BUFFER);

    std::copy(positions, positions + positionCount * 4, vertices);

    for (auto i = 0u; i < positionCount; ++i)
    {
        m->bounds.extendBy(fv3ToV3f(positions + 4 * i));
    }

    rtcUnmapBuffer(scene, rtcCurvesId, RTC_VERTEX_BUFFER);

    int32_t * indices = (int32_t *) rtcMapBuffer(scene, rtcCurvesId, RTC_INDEX_BUFFER);

    std::copy(cubicCurveTopology, cubicCurveTopology + cubicCurveCount, indices);

    rtcUnmapBuffer(scene, rtcCurvesId, RTC_INDEX_BUFFER);

    rtcCommit(scene);

    m_primitives[meshId] = MotionPrimitive(m->bounds, scene, PrimitiveType::CURVES);
    m->processed = true;

    return 0;
}

int32_t SceneImpl::buildCurvesMotionAccel(Scene::Id meshId)
{
    struct Vertex
    {
        float x, y, z, r;
    };

    assert(meshId < m_meshes.size());
    Mesh * m = &(m_meshes[meshId]);

    assert(m->positionAttrIndex >= 0 && "NO POSITION ATTRIBUTE");

    const auto positionCount = m->vertexAttributes[m->positionAttrIndex].count;

    const size_t count = m->vertexAttributes[m->positionAttrIndex].frameCount - 1;
    std::vector<RTCScene> scenes;
    std::vector<float> times;
    scenes.reserve(count);
    times.reserve(count);

    const int32_t * const cubicCurveTopology = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].dataInt;
    auto cubicCurveCount = m->topologies[m->vertexAttributes[m->positionAttrIndex].topology].count;

    for (int32_t frameIdx = 0; frameIdx < count; ++frameIdx)
    {
        const auto * positions0 = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat[frameIdx];
        const auto * positions1 = (const float *) m->vertexAttributes[m->positionAttrIndex].dataFloat[frameIdx + 1];

        RTCScene scene = rtcDeviceNewScene(m_rtcDevice, RTC_SCENE_STATIC, getRTCAlgorithmFlags());

        unsigned rtcCurvesId = rtcNewHairGeometry(scene, RTC_GEOMETRY_STATIC, cubicCurveCount, positionCount, 2);

        auto * vertices0 = (float *) rtcMapBuffer(scene, rtcCurvesId, RTC_VERTEX_BUFFER0);
        auto * vertices1 = (float *) rtcMapBuffer(scene, rtcCurvesId, RTC_VERTEX_BUFFER1);

        std::copy(positions0, positions0 + positionCount * 4, vertices0);

        for (auto i = 0u; i < positionCount; ++i)
        {
            m->bounds.extendBy(fv3ToV3f(positions0 + 4 * i));
        }

        std::copy(positions1, positions1 + positionCount * 4, vertices1);

        for (auto i = 0u; i < positionCount; ++i)
        {
            m->bounds.extendBy(fv3ToV3f(positions1 + 4 * i));
        }

        rtcUnmapBuffer(scene, rtcCurvesId, RTC_VERTEX_BUFFER0);
        rtcUnmapBuffer(scene, rtcCurvesId, RTC_VERTEX_BUFFER1);

        int32_t * indices = (int32_t *) rtcMapBuffer(scene, rtcCurvesId, RTC_INDEX_BUFFER);

        std::copy(cubicCurveTopology, cubicCurveTopology + cubicCurveCount, indices);

        rtcUnmapBuffer(scene, rtcCurvesId, RTC_INDEX_BUFFER);

        rtcCommit(scene);

        scenes.emplace_back(scene);
        times.emplace_back(m->vertexAttributes[m->positionAttrIndex].timeValues[frameIdx]);
    }

    m_primitives[meshId] = MotionPrimitive(m->bounds, scenes.size(), scenes.data(), times.data(), PrimitiveType::CURVES);
    m->processed = true;

    return 0;
}

static std::vector<V2f> computeCurvesNormalizedLengthAttribute(const float * positions, const int32_t * topology, uint32_t curveCount)
{
    std::vector<V2f> cubicCurveLength; // cubicCurveLength[i].x is  the length from root to vertex i and cubicCurveLength[i].y is the length of the cubic bezier curve starting at vertex i
    auto offset = 0u;
    for (auto curveIdx = 0u; curveIdx < curveCount; ++curveIdx)
    {
        auto lengthFromRoot = 0.f;
        auto startIdx = cubicCurveLength.size();
        for (auto cubicIdx = 0u; cubicIdx < topology[curveIdx] / 3; ++cubicIdx)
        {
            auto pointIndex = offset + 3 * cubicIdx;
            auto length = cubicBezierCurveLength(
                V3f(positions[4 * pointIndex], positions[4 * pointIndex + 1], positions[4 * pointIndex + 2]),
                V3f(positions[4 * (pointIndex + 1)], positions[4 * (pointIndex + 1) + 1], positions[4 * (pointIndex + 1) + 2]),
                V3f(positions[4 * (pointIndex + 2)], positions[4 * (pointIndex + 2) + 1], positions[4 * (pointIndex + 2) + 2]),
                V3f(positions[4 * (pointIndex + 3)], positions[4 * (pointIndex + 3) + 1], positions[4 * (pointIndex + 3) + 2]));

            cubicCurveLength.emplace_back(V2f(lengthFromRoot, length));
            lengthFromRoot += length;
        }
        // Normalization:
        for (auto i = startIdx; i < cubicCurveLength.size(); ++i)
        {
            cubicCurveLength[i] /= lengthFromRoot;
        }
        offset += topology[curveIdx];
    }

    return cubicCurveLength;
}

int32_t SceneImpl::computeCurveVertexAttributes(Scene::Id meshId)
{
    assert(meshId < m_meshes.size());
    Mesh * mesh = &m_meshes[meshId];

    auto curveLengthAttrIdx = mesh->texCoordAttrIndex != -1 ? mesh->texCoordAttrIndex + 1 : mesh->normalAttrIndex + 1;
    auto vaCount = getMeshVertexAttributeCount(meshId);

    // If curve length attribute not computed, compute it
    if (vaCount <= curveLengthAttrIdx)
    {
        assert(vaCount == curveLengthAttrIdx);
        enableMeshVertexAttributes(meshId, vaCount + 1);

        const auto frameCount = mesh->vertexAttributes[mesh->positionAttrIndex].frameCount;
        if (frameCount > 1)
        {
            std::vector<std::vector<V2f>> cubicCurveLengthArray; // x is  the length from root and y is the length of the cubic bezier curve
            std::vector<const float *> ptrArray;

            for (auto frameIdx = 0; frameIdx < frameCount; ++frameIdx)
            {
                cubicCurveLengthArray.emplace_back(
                    computeCurvesNormalizedLengthAttribute(mesh->vertexAttributes[mesh->positionAttrIndex].dataFloat[frameIdx], mesh->faces.dataInt, mesh->faces.count));
            }

            for (auto frameIdx = 0; frameIdx < frameCount; ++frameIdx)
            {
                ptrArray.emplace_back((const float *) cubicCurveLengthArray[frameIdx].data());
            }

            meshVertexAttributeData(
                meshId, -1, Attr_CurveLength, cubicCurveLengthArray.back().size(), 2, ptrArray.data(), mesh->vertexAttributes[mesh->positionAttrIndex].timeValues, frameCount,
                Scene::COPY_BUFFER);
        }
        else
        {
            auto cubicCurveLength = computeCurvesNormalizedLengthAttribute((const float *) mesh->vertexAttributes[mesh->positionAttrIndex].dataFloat, mesh->faces.dataInt, mesh->faces.count);
            meshVertexAttributeData(meshId, -1, Attr_CurveLength, cubicCurveLength.size(), 2, (const float *) cubicCurveLength.data(), Scene::COPY_BUFFER);
        }
    }

    return 0;
}

} // namespace shn