#pragma once

#include "AttrUtils.h"
#include "TypeUtils.h"

// Renderer attributes:
#define SHN_ATTR_RENDER_RAYFILTER "renderer:ray_filter"
#define SHN_ATTR_RENDER_COLORSPACE "renderer:colorspace"
#define SHN_ATTR_RENDER_TILEPATTERN "renderer:tile_pattern"
#define SHN_ATTR_RENDER_ATMOSPHERESHADER "renderer:atmosphere_shader"
#define SHN_ATTR_RENDER_OCCLUSIONDISTANCE "renderer:occlusion_distance"
#define SHN_ATTR_RENDER_OCCLUSIONANGLE "renderer:occlusion_angle"
#define SHN_ATTR_RENDER_DEPTHNEAR "renderer:depth_near"
#define SHN_ATTR_RENDER_DEPTHFAR "renderer:depth_far"
#define SHN_ATTR_RENDER_SHUTTEROFFSET "renderer:shutter_offset"
#define SHN_ATTR_RENDER_LOWLIGHTTRESHOLD "renderer:low_light_threshold"
#define SHN_ATTR_RENDER_PIXELSAMPLECLAMP "renderer:pixel_sample_clamp"
#define SHN_ATTR_RENDER_ADAPTIVEVARIANCE "renderer:adaptive_variance"
#define SHN_ATTR_RENDER_RAYTRACEDINDIRECTBIAS "renderer:ray_traced_indirect_bias"
#define SHN_ATTR_RENDER_MAXBOUNCES "renderer:max_bounces" // max bounces asked by the user. It doesn't include transparency bounce if asked by the user
#define SHN_ATTR_RENDER_IGNORESTRAIGHTBOUNCES "renderer:ignore_straight_bounces" // If set to true, straight(aka transparency) bounces will not be a real bounces.
#define SHN_ATTR_RENDER_DIFFUSEBOUNCES "renderer:diffuse_bounces"
#define SHN_ATTR_RENDER_GLOSSYBOUNCES "renderer:glossy_bounces"
#define SHN_ATTR_RENDER_REFLECTIONBOUNCES "renderer:reflection_bounces"
#define SHN_ATTR_RENDER_REFRACTIONBOUNCES "renderer:refraction_bounces"
#define SHN_ATTR_RENDER_TRANSPARENCYBOUNCES "renderer:transparency_bounces"
#define SHN_ATTR_RENDER_SHADOWBOUNCES "renderer:shadow_bounces"
#define SHN_ATTR_RENDER_VOLUMEBOUNCES "renderer:volume_bounces"
#define SHN_ATTR_RENDER_THREADCOUNT "renderer:thread_count"
#define SHN_ATTR_RENDER_BASICCOUNTERS "renderer:basic_counters"
#define SHN_ATTR_RENDER_INSTANCECOUNTERS "renderer:instance_counters"
#define SHN_ATTR_RENDER_TIMESEED "renderer:time_seed"
#define SHN_ATTR_RENDER_ADAPTIVEMINSAMPLES "renderer:adaptive_min_samples"
#define SHN_ATTR_RENDER_NANHANDLINGPOLICY "renderer:nan_handling_policy"
#define SHN_ATTR_RENDER_VOLUMESKIPINCLUSIONTEST "renderer:volume_skip_inclusion_test"
#define SHN_ATTR_RENDER_DIRECTILLUMALGORITHM "renderer:direct_illumination_algorithm"
#define SHN_ATTR_RENDER_DIRECTILLUMSAMPLECOUNT "renderer:direct_illumination_sample_count"
#define SHN_ATTR_RENDER_BSSRDFMISHEURISTIC "renderer:bssrdf_mis_heuristic"
#define SHN_ATTR_RENDER_BSSRDFRESAMPLING "renderer:bssrdf_resampling" // Boolean: 0 or 1
#define SHN_ATTR_RENDER_DEFAULTTOONOUTLINE "renderer:default_toon_outline" // Name of a shader, or empty string if no default toon outline
#define SHN_ATTR_RENDER_TOONOUTLINEMINSAMPLES "renderer:toon_outline_min_samples" // Integer: minimum number of samples that will be tested for outline computation
#define SHN_ATTR_RENDER_LIGHTICPROBAALPHA "renderer:lighting_ic_proba_alpha"
#define SHN_ATTR_RENDER_REPORTPATH "renderer:report_path"
#define SHN_ATTR_RENDER_REPORTPERFIMAGES "renderer:report_perf_images" // Boolean: 0 or 1
#define SHN_ATTR_RENDER_DENOISEPASSLIST "renderer:denoise_pass_list" // string, list of output passes separated by a space ' '
#define SHN_ATTR_RENDER_LIGHTICSTATSTILELIST "renderer:light_ic_stats_tile_list" // string, list of tiles (x,y) separated by a space
#define SHN_ATTR_RENDER_EXEPATH "renderer:executables_path"

#define SHN_ATTR_RENDER_PASS_NAMES "renderer:pass:names"
#define SHN_ATTR_RENDER_PASS_ROLES "renderer:pass:roles"

#define SHN_ATTR_OSL_DEBUGNAN "renderer:debugnan"
#define SHN_ATTR_OSL_DEBUGUNINIT "renderer:debug_uninit"
#define SHN_ATTR_OSL_RANGECHECKING "renderer:range_checking"
#define SHN_ATTR_OSL_LOCKGEOM "renderer:lockgeom"
#define SHN_ATTR_OSL_LAZYLAYERS "renderer:lazylayers"
#define SHN_ATTR_OSL_LAZYGLOBALS "renderer:lazyglobals"

// Possible strings for SHN_ATTR_RENDER_RAYFILTER
#define SHN_RAYFILTER_BSPLINE "bspline"
#define SHN_RAYFILTER_TRIANGLE "triangle"
#define SHN_RAYFILTER_CENTER "center"
#define SHN_RAYFILTER_BOX "box"

// Possible strings for SHN_ATTR_RENDER_TILEPATTERN
#define SHN_TILEPATTERN_SPIRAL "spiral"
#define SHN_TILEPATTERN_SCANLINE "scanline"

// Possible strings for SHN_ATTR_RENDER_NANHANDLINGPOLICY
#define SHN_NANHANDLINGPOLICY_DROP 0
#define SHN_NANHANDLINGPOLICY_IGNORE 1
#define SHN_NANHANDLINGPOLICY_DEBUG 2

// Possible string for SHN_ATTR_RENDER_DIRECTILLUMINATIONALGORITHM
#define SHN_DIRECTILLUMALGORITHM_ALLLIGHTS "allLights" // Sample and evaluate all lights at each bounce
#define SHN_DIRECTILLUMALGORITHM_IMPORTANCECACHING "lightImportanceCaching" // Sample and evaluate "renderer:direct_illumination_sample_count" light at each bounce with importance caching

// Possible string for SHN_ATTR_RENDER_BSSRDFMISHEURISTIC
#define SHN_BSSRDFMISHEURISTIC_NONE "none"
#define SHN_BSSRDFMISHEURISTIC_BALANCE "balance"
#define SHN_BSSRDFMISHEURISTIC_ALPHAMAX "alphaMax"
#define SHN_BSSRDFMISHEURISTIC_MAX "max"

// Possible strings for assignType, passed to assignShadingTree()
#define SHN_ASSIGNTYPE_SURFACE "surface"
#define SHN_ASSIGNTYPE_VOLUME "volume"
#define SHN_ASSIGNTYPE_DISPLACEMENT "displacement"
#define SHN_ASSIGNTYPE_TOONOUTLINE "toonOutline"

// Possible strings for pass roles
#define SHN_PASSROLE_RENDER "render"
#define SHN_PASSROLE_TECHNICAL "technical"
#define SHN_PASSROLE_STATS "statistics"
#define SHN_PASSROLE_DEBUG "debug"

// Possible string for pass denoise flags
#define SHN_DENOISEFLAG_RAW "raw"
#define SHN_DENOISEFLAG_DENOISED "denoised"
#define SHN_DENOISEFLAG_BOTH "both"

// Light attributes:
#define SHN_ATTR_LIGHT_NAME "lightName"
#define SHN_ATTR_LIGHT_TYPE "lightType"
#define SHN_ATTR_LIGHT_COLORSHADINGTREEID "lightColorShadingTreeId"
#define SHN_ATTR_LIGHT_LINKINGMODE "lightLinkingMode"
#define SHN_ATTR_LIGHT_LINKINGSET "lightLinkingSet"
#define SHN_ATTR_LIGHT_LOCALTOWORLD "lightLocalToWorld"
#define SHN_ATTR_LIGHT_INTENSITY "lightIntensity"
#define SHN_ATTR_LIGHT_COLOR "lightColor"
#define SHN_ATTR_LIGHT_SHADOWCOLOR "lightShadowColor"
#define SHN_ATTR_LIGHT_RAYTRACEDSHADOWBIAS "lightRayTracedShadowBias"
#define SHN_ATTR_LIGHT_CASTSHADOWS "lightCastShadows"
#define SHN_ATTR_LIGHT_DECAY "lightDecay"
#define SHN_ATTR_LIGHT_EMITCOMPONENT "lightEmitComponent"
#define SHN_ATTR_LIGHT_RADIUS "lightRadius"
#define SHN_ATTR_LIGHT_ANGLE "lightAngle"
#define SHN_ATTR_LIGHT_PENUMBRAANGLE "lightPenumbraAngle"
#define SHN_ATTR_LIGHT_DROPOFF "lightDropoff"
#define SHN_ATTR_LIGHT_SHAPE "lightShape"
#define SHN_ATTR_LIGHT_SAMPLINGSTRATEGY "lightSamplingStrategy"
#define SHN_ATTR_LIGHT_SIDE "lightSide"
#define SHN_ATTR_LIGHT_IMPORTANCESAMPLING "lightImportanceSampling"
#define SHN_ATTR_LIGHT_IMPORTANCESAMPLINGRESOLUTION "lightResolution"
#define SHN_ATTR_LIGHT_BACKGROUND "lightBackground"
#define SHN_ATTR_LIGHT_INSTANCENAME "lightInstanceName"
#define SHN_ATTR_LIGHT_GEOMETRYSHADOWBIAS "lightGeometryShadowBias"
#define SHN_ATTR_LIGHT_OVERRIDEMATERIAL "lightOverrideMaterial"
#define SHN_ATTR_LIGHT_NORMALIZEAREA "lightNormalizeArea"
#define SHN_ATTR_LIGHT_NEAR "lightNearDistRange"
#define SHN_ATTR_LIGHT_FAR "lightFarDistRange"

// Possible strings for SHN_ATTR_LIGHT_TYPE
#define SHN_LIGHT_TYPE_POINT "point"
#define SHN_LIGHT_TYPE_DIRECTIONAL "directional"
#define SHN_LIGHT_TYPE_SPOT "spot"
#define SHN_LIGHT_TYPE_AREA "area"
#define SHN_LIGHT_TYPE_ENVMAP "envMap"
#define SHN_LIGHT_TYPE_GEOMETRY "geometry"

// Possible strings for SHN_ATTR_LIGHT_SIDE
#define SHN_LIGHT_SIDE_FRONT "front"
#define SHN_LIGHT_SIDE_BACK "back"
#define SHN_LIGHT_SIDE_DOUBLE "double"

// Possible strings for SHN_ATTR_LIGHT_SHAPE
#define SHN_LIGHT_SHAPE_RECT "rect"
#define SHN_LIGHT_SHAPE_DISK "disk"
#define SHN_LIGHT_SHAPE_CYLINDER "cylinder"
#define SHN_LIGHT_SHAPE_SPHERE "sphere"

// Possible strings for SHN_ATTR_LIGHT_SAMPLINGSTRATEGY
#define SHN_LIGHT_SAMPLINGSTRAT_BOTH "both"
#define SHN_LIGHT_SAMPLINGSTRAT_LIGHT "light"
#define SHN_LIGHT_SAMPLINGSTRAT_BXDF "bxdf"

namespace shn
{

struct LightAttrSetter
{
    template<typename OslRendererT, typename LightIdT>
    static auto set(OslRendererT & r, LightIdT id, const char * attrName, const char * str)
    {
        return r.lightAttribute(id, attrName, typeName<const char *>(), strlen(str) + 1, str);
    }

    template<typename OslRendererT, typename LightIdT, typename T>
    static auto set(OslRendererT & r, LightIdT id, const char * attrName, int32_t count, const T * values)
    {
        return r.lightAttribute(id, attrName, typeName<T>(), count * sizeof(T), reinterpret_cast<const char *>(values));
    }
};

struct LightAttr
{
    SHN_DEFINE_STRING_ATTR(name, SHN_ATTR_LIGHT_NAME, LightAttrSetter)
    SHN_DEFINE_STRING_ATTR(type, SHN_ATTR_LIGHT_TYPE, LightAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(linkingMode, SHN_ATTR_LIGHT_LINKINGMODE, int32_t, LightAttrSetter)
    SHN_DEFINE_ARRAY_ATTR(linkingSet, SHN_ATTR_LIGHT_LINKINGSET, int32_t, LightAttrSetter)
    SHN_DEFINE_SCALAR_ATTR(colorShadingTreeId, SHN_ATTR_LIGHT_COLORSHADINGTREEID, int32_t, LightAttrSetter)
};

} // namespace shn