#include "PiecewiseDistributions.h"
#include "IMathUtils.h"
#include <algorithm>
#include <iostream>

namespace shn
{

Sample1f sampleContinuousDistribution1D(const float * pCDF, size_t size, float s1D)
{
    // coarse sampling of the distribution
    const auto s = sampleDiscreteDistribution1D(pCDF, size, s1D);
    if (!s)
        return Sample1f(0.f, 0.f);

    // refine sampling linearly by assuming the distribution being a step function
    float fraction = (s1D - ((s.value > 0) ? pCDF[s.value - 1] : 0.f)) / s.pdf;
    return Sample1f(s.value + fraction, s.pdf * size);
}

Sample1i sampleDiscreteDistribution1D(const float * pCDF, size_t size, float s1D)
{
    if (pCDF[size - 1] == 0.f)
    {
        return Sample1i(0, 0.f);
    }

    auto ptr = std::upper_bound(pCDF, pCDF + size, s1D);
    int i = clamp(int(ptr - pCDF), 0, int(size) - 1);
    return Sample1i(i, pdfDiscreteDistribution1D(pCDF, i));
}

void sampleDiscreteDistribution1D(const float * pCDF, size_t size, const char * s1DBuffer, size_t byteStride, size_t sampleCount, Sample1i * outBuffer)
{
    if (pCDF[size - 1] == 0.f)
    {
        std::fill(outBuffer, outBuffer + sampleCount, Sample1i(0, 0.f));
        return;
    }

    const float * pCurrentValue = pCDF;

    for (size_t i = 0; i < sampleCount; ++i)
    {
        const float s1D = *(const float *) (s1DBuffer + i * byteStride);

        // Find first value in CDF which is > to the random number
        while (s1D > *pCurrentValue)
            ++pCurrentValue;
        const size_t idx = pCurrentValue - pCDF;
        outBuffer[i] = Sample1i(idx, pdfDiscreteDistribution1D(pCDF, idx));
    }
}

float pdfContinuousDistribution1D(const float * pCDF, size_t size, float x)
{
    auto i = clamp(int(x), 0, int(size) - 1);
    return pdfDiscreteDistribution1D(pCDF, i) * size;
}

float pdfDiscreteDistribution1D(const float * pCDF, size_t i)
{
    return (pCDF[i] - ((i > 0) ? pCDF[i - 1] : 0.f));
}

float cdfDiscreteDistribution1D(const float * pCDF, size_t i)
{
    return pCDF[i];
}

} // namespace shn
