#pragma once

#include "IMathUtils.h"
#include "OslHeaders.h"
#include "Shining.h"

namespace shn
{
OSL::Dual2<float> intersectSphere(const V3f & c, float r, const V3f & o, const V3f & d);
float evalSphere(const V3f & c, float r, const V3f & x);
V2f sampleUniformBaricentricTriangleCoord(float u1, float u2);
V3f sampleTriangle(float u1, float u2, const V3f & A, const V3f & B, const V3f & C);
V3f sampleQuad(const V3f * p, float u1, float u2);
float intersectTriangle(const V3f & A, const V3f & B, const V3f & C, const V3f & o, const V3f & d);
float intersectQuad(const V3f * p, const V3f & o, const V3f & d);
V2f quadUVSampling(const V3f * p, const V3f & o, const V3f & d, const float dist);
bool intersectDualCone(const V3f & conePos, const V3f & coneDir, const float cosTheta, const V3f & rayOrg, const V3f & rayDir, float * tmin = nullptr, float * tmax = nullptr);
bool intersectPlane(const V3f & planePosition, const V3f & planeNormal, const V3f & rayOrg, const V3f & rayDir, float * t = nullptr);
float intersectZPlane(const V3f & rayOrg, const V3f & rayDir);

// Compute tmin and tmax, if they exist, such that org + tmin * dir and org + tmax * dir are on the unit sphere of center (0,0,0) and radius 1
// #note: dir DOES NOT need to be normalized
bool intersectUnitSphere(const V3f & org, const V3f & dir, float * tmin = nullptr, float * tmax = nullptr);

// Compute tmin and tmax, if they exist, such that org + tmin * dir and org + tmax * dir are on the unit cylinder of center (0,0,0), radius 1 and infinite height along z
// #note: dir DOES NOT need to be normalized
bool intersectUnitCylinder(const V3f & org, const V3f & dir, float * tmin = nullptr, float * tmax = nullptr);

// Hemisphere sampling
V3f sampleUniformHemisphere(const V3f & N, float u1, float u2, float & pdf);
float pdfUniformHemisphere();
V3f sampleCosineHemisphere(const V3f & N, float u1, float u2, float & pdf);
float pdfCosineHemisphere(const V3f & N, const V3f & directionSample);
V3f samplePowerCosineHemisphere(const V3f & N, float exponent, float u1, float u2, float & pdf);
float pdfPowerCosineHemisphere(const V3f & N, float exponent, const V3f & dirSample);

// Sphere sampling
V2f sphereUV(const V3f & dir);
V3f sampleUniformSphere(float u1, float u2, float & pdf, float & cosTheta, float & sinTheta);
inline V3f sampleUniformSphere(float u1, float u2, float & pdf)
{
    float sinTheta, cosTheta;
    return sampleUniformSphere(u1, u2, pdf, sinTheta, cosTheta);
}
inline V3f sampleUniformSphere(float u1, float u2)
{
    float pdf;
    return sampleUniformSphere(u1, u2, pdf);
}
float pdfUniformSphere();
V3f sampleUniformSphere(const V3f & N, float u1, float u2, float & pdf);
V3f sampleUniformSphere(const V3f & N, float u1, float u2, float & pdf, float & cosTheta, float & sinTheta);
V3f sampleCosineSphere(const V3f & N, float u1, float u2, float & pdf);
V3f sampleCosineSphere(const V3f & N, float u1, float u2, float & pdf, float & cosTheta, float & sinTheta);
float pdfCosineSphere(const V3f & N, const V3f & dirSample);
V3f sampleRaisedTriangleSphere(const V3f & N, float cosThetaMin, float cosThetaMax, float u1, float u2, float & pdf);
float pdfRaisedTriangleSphere(const V3f & N, float cosThetaMin, float cosThetaMax, const V3f & dirSample);

V3f sampleUniformSphereSolidAngle(const V3f & center, float radius, const V3f & point, float u1, float u2, float & pdf); // Uniform sampling of the solid angle of a sphere subtended at a point
float pdfUniformSphereSolidAngle(const V3f & center, float radius, const V3f & point, const V3f & direction); // Uniform sampling density of the solid angle of a sphere subtended at a point

// Cone sampling
V3f sampleUniformSphericalCone(const V3f & N, float angle, float u1, float u2, float & pdf);
float pdfUniformSphericalCone(const V3f & N, float angle, const V3f & dirSample);

// Disk sampling
V2f sampleUniformDisk(float u1, float u2); // Uniformly sample a point in the unit disk of center (0,0) dans radius 1. The pdf if 1 / pi.
V2f sampleConcentricDisk(
    float u1, float u2); // Uniformly sample a point in the unit disk of center (0,0) dans radius 1 with less distortion than the basic sampleUniformDisk function. The pdf if 1 / pi.
float pdfUniformDisk(); // Return the uniform sampling pdf of any point on the unit disk, i.e. 1 / pi

// sampling functions for volumetrics and SSS
float sampleEquiAngular(float delta, float thetaA, float thetaB, float D, float u1, float & pdf);
float pdfEquiAngular(float distanceSample, float delta, float thetaA, float thetaB, float D);
float sampleDistanceVolumetric(float lumSigmaT, float d, float u1, float & pdf); // Sample t \in [0,d]
float pdfDistanceVolumetricSampling(float distanceSample, float lumSigmaT, float d);
float sampleDistanceVolumetric(float lumSigmaT, float a, float b, float u1, float & pdf); // Sample t \in [a,b]
float pdfDistanceVolumetricSampling(float distanceSample, float lumSigmaT, float a, float b);
float sampleExponentialDistance(float sigmaT, float u1, float & pdf);
float pdfExponentialDistance(float distanceSample, float lumSigmaT);

V3f sphericalMapping(const V3f & N, float phi, float cosTheta, float sinTheta);
V2f rcpSphericalMapping(const V3f & direction); // Return the pair (phi, theta) \in [-pi,+pi]x[0,pi] such that direction = (cos(phi) sin(theta), sin(phi) sin(theta), cos(theta))

// Given w_o = normalize(transform * w), return the jacobian dw_o / dw that can be used to convert probability densities: pdf(w) = pdf(w_o) * | dw_o / dw |
// So if w = normalize(M * w_o), we have pdf(w) = pdf(w_o) * solidAngleLinearTransformJacobian(w, inverse(M), determinant(inverse(M))
// ref: Real-Time Polygonal-Light Shading with Linearly Transformed Cosines [Heitz et al., 2016]
float solidAngleLinearTransformJacobian(const V3f & direction, const M33f & transform, float transformDeterminant);

inline float pdfUniformDisk(float sqrRadius)
{
    return sqrRadius > 0.f ? 1.f / (FPI * sqrRadius) : 0.f;
}

inline float pdfWrtAreaToSolidAngleFactor(float sqrDist, float cosAtSamplePoint)
{
    return sqrDist / cosAtSamplePoint;
}

} // namespace shn