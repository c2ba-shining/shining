#include "PatternSamplers.h"
#include <cassert>
#include <numeric>
#include <vector>

namespace shn
{

void jittered1d(RandomState & rng, float * samples, size_t count, size_t stride)
{
    assert(stride >= sizeof(float));

    const float oneMinusEpsilon = 1.f - float(ulp);
    const float dx = 1.f / count;

    char * pSamples = (char *) samples;
    for (int x = 0; x < count; ++x)
    {
        float jx = getFloat(rng);
        *(float *) pSamples = std::min((x + jx) * dx, oneMinusEpsilon);
        pSamples += stride;
    }
}

void shuffle1d(RandomState & rng, float * samples, size_t count, size_t stride)
{
    char * pSamples = (char *) samples;
    for (size_t i = 0; i < count; ++i)
    {
        const int j = getInt(rng, count);
        std::swap(*(float *) (pSamples + i * stride), *(float *) (pSamples + j * stride));
    }
}

void regular2d(float * samples, size_t w, size_t h, size_t stride)
{
    assert(stride >= 2 * sizeof(float));

    const float oneMinusEpsilon = 1.f - float(ulp);
    const float dx = 1.f / w, dy = 1.f / h;
    char * pSample = (char *) samples;
    for (int y = 0; y < h; ++y)
    {
        for (int x = 0; x < w; ++x)
        {
            float jx = 0.5;
            float jy = 0.5;
            ((float *) pSample)[0] = std::min((x + jx) * dx, oneMinusEpsilon);
            ((float *) pSample)[1] = std::min((y + jy) * dy, oneMinusEpsilon);
            pSample += stride;
        }
    }
}

void jittered2d(RandomState & rng, float * samples, size_t w, size_t h, size_t stride)
{
    assert(stride >= 2 * sizeof(float));

    const float oneMinusEpsilon = 1.f - float(ulp);
    const float dx = 1.f / w, dy = 1.f / h;
    char * pSample = (char *) samples;
    for (int y = 0; y < h; ++y)
    {
        for (int x = 0; x < w; ++x)
        {
            float jx = getFloat(rng);
            float jy = getFloat(rng);
            ((float *) pSample)[0] = std::min((x + jx) * dx, oneMinusEpsilon);
            ((float *) pSample)[1] = std::min((y + jy) * dy, oneMinusEpsilon);
            pSample += stride;
        }
    }
}

void random2d(RandomState & rng, float * samples, size_t count, size_t stride)
{
    assert(stride >= 2 * sizeof(float));

    const float oneMinusEpsilon = 1.f - float(ulp);
    char * pSample = (char *) samples;
    for (int i = 0; i < count; ++i)
    {
        ((float *) pSample)[0] = std::min(getFloat(rng), oneMinusEpsilon);
        ((float *) pSample)[1] = std::min(getFloat(rng), oneMinusEpsilon);
        pSample += stride;
    }
}

void shuffle2d(RandomState & rng, float * samples, size_t count, size_t stride)
{
    char * pSamples = (char *) samples;
    for (size_t i = 0; i < count; ++i)
    {
        const int j = getInt(rng, count);
        std::swap(((float *) (pSamples + i * stride))[0], ((float *) (pSamples + j * stride))[0]);
        std::swap(((float *) (pSamples + i * stride))[1], ((float *) (pSamples + j * stride))[1]);
    }
}

void makePermutation(RandomState & rng, int * perm, size_t count)
{
    std::iota(perm, perm + count, 0);
    shuffle(rng, perm, count);
}

} // namespace shn