#pragma once

#include "IMathUtils.h"
#include "RandomState.h"
#include <vector>

namespace shn
{

// Randomly shuffle an array
// Code taken from embree 1.0 maths library, may not be good in term of distribution. Perhaps use std::shuffle instead.
template<typename T>
void shuffle(RandomState & rng, T * values, size_t count)
{
    for (size_t i = 0; i < count; ++i)
        std::swap(values[i], values[getInt(rng, count)]);
}

template<typename UIntType>
inline UIntType nearestUpperSquareNumber(const UIntType x)
{
    const UIntType y = (UIntType) sqrtf(float(x));
    return x == sqr(y) ? x : sqr(y + 1);
}

void jittered1d(RandomState & rng, float * samples, size_t count, size_t stride = sizeof(float));

void shuffle1d(RandomState & rng, float * samples, size_t count, size_t stride = sizeof(float));

// Create a set of w * h multi-jittered 2D samples, using the provided RandomState.
// Output samples in a float array.
// Each sample < i > is output at (< samples + i * stride >, < samples + i * stride + 1>)
void jittered2d(RandomState & rng, float * samples, size_t w, size_t h, size_t stride = 2 * sizeof(float));

// Create a set of count random 2D samples using the provided RandomState.
// Output samples in a float array.
// Each sample < i > is output at (< samples + i * stride >, < samples + i * stride + 1>)
void random2d(RandomState & rng, float * samples, size_t count, size_t stride = 2 * sizeof(float));

void shuffle2d(RandomState & rng, float * samples, size_t count, size_t stride = 2 * sizeof(float));

void regular2d(float * samples, size_t w, size_t h, size_t stride = 2 * sizeof(float));

// Create a set of w * h multi-jittered 2D samples, using the provided RandomState.
// Each sample < i > is output at < samples + i * stride >
inline void jittered2d(RandomState & rng, V2f * samples, size_t w, size_t h)
{
    jittered2d(rng, &samples[0].x, w, h, sizeof(V2f));
}

// Create a random permutation of [0, count - 1] in perm
// [perm, perm + count] must be a valid memory range
void makePermutation(RandomState & rng, int * perm, size_t count);

inline std::vector<int> makePermutation(RandomState & rng, size_t count)
{
    std::vector<int> container(count);
    makePermutation(rng, container.data(), count);
    return container;
}

} // namespace shn