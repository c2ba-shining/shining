#pragma once

#include "IMathTypes.h"
#include "sampling/PiecewiseDistributions.h"
#include "sampling/Sample.h"
#include <cstdint>
#include <memory>
#include <vector>

namespace shn
{

// Represents a pixel filter that can be importance sampled
class Filter
{
public:
    // Constructs a center filter
    Filter() = default;

    // Constructs a filter that can be sampled from an evaluation function.
    // The evaluation function must return a weight from a location relative to the pixel center.
    template<typename EvalFunc>
    Filter(float width, float height, uint32_t border, const EvalFunc & eval, uint32_t tableSize = 256);

    // Draws a filter sample in range [-width/2, width/2] x [-height/2, height/2]
    V2f sample(float u1, float u2) const;

private:
    float width = 0.f; // Width of the filter
    float height = 0.f; // Height of the filter
    uint32_t border = 0; // Number of border pixels of the filter

    uint32_t tableSize = 0; // Size of the table used to approximate sampling.
    std::vector<float> samplingDistribution; // Distribution to select between rows followed by one 1D Distribution per row
};

template<typename EvalFunc>
Filter::Filter(float width, float height, uint32_t border, const EvalFunc & eval, uint32_t tableSize): width(width), height(height), border(border), tableSize(tableSize)
{
    const float invTableSize = 1.0f / tableSize;

    samplingDistribution.resize(tableSize + tableSize * tableSize); // Distribution to select between rows followed by one 1D Distribution per row

    buildDistribution2D(tableSize, tableSize, samplingDistribution.data(), samplingDistribution.data() + tableSize, [&](size_t i, size_t j) {
        V2f pos((i + 0.5f) * invTableSize * width - width * 0.5f, (j + 0.5f) * invTableSize * height - height * 0.5f);
        return fabsf(eval(pos));
    });
}

inline V2f Filter::sample(float u1, float u2) const
{
    if (tableSize == 0u)
        return V2f(0.f, 0.f); // Center filter

    Sample2f result = sampleContinuousDistribution2D(tableSize, tableSize, samplingDistribution.data(), samplingDistribution.data() + tableSize, u1, u2);
    result.value.x = result.value.x / tableSize * width - width * 0.5f;
    result.value.y = result.value.y / tableSize * height - height * 0.5f;
    return result.value;
}

inline Filter makeBoxFilter(const float halfWidth = 0.5f)
{
    return { 2.0f * halfWidth, 2.0f * halfWidth, uint32_t(ceil(halfWidth - 0.5f)), [&](const V2f & distanceToCenter) {
                if (fabsf(distanceToCenter.x) <= halfWidth && fabsf(distanceToCenter.y) <= halfWidth)
                    return 1.0f;
                else
                    return 0.0f;
            } };
}

inline Filter makeBSplineFilter()
{
    return { 4.0f, 4.0f, 2, [&](const V2f & distanceToCenter) {
                float d = length(distanceToCenter);
                if (d > 2.0f)
                    return 0.0f;
                else if (d < 1.0f)
                {
                    float t = 1.0f - d;
                    return ((((-3.0f * t) + 3.0f) * t + 3.0f) * t + 1.0f) / 6.0f;
                }
                else
                {
                    float t = 2.0f - d;
                    return t * t * t / 6.0f;
                }
            } };
}

inline Filter makeTriangleFilter(const float radius = 1.f)
{
    return { 2.0f * radius, 2.0f * radius, 0 /* unused */, [&](const V2f & distanceToCenter) {
                const float d = length(distanceToCenter);
                return std::max(0.f, 1.f - d / radius);
            } };
}

inline Filter makeCenterFilter()
{
    return {};
}

} // namespace shn