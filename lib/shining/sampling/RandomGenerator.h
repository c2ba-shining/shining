#pragma once

#include "IMathUtils.h"

namespace shn
{

// Minimal Standard Linear Congruential Generator
// More information:
//  - "Random Number Generators: Good Ones Are Hard to Find", Park and Miller (https://pdfs.semanticscholar.org/9af1/2693368d7c76ea5b0ee4bf7e729de0b6f089.pdf)
//  - https://www.christianpinder.com/articles/pseudo-random-number-generation/
//
// Code from embree 1.0 legacy
//
// Question: can it be replaced by std::minstd_rand0 ? (http://www.cplusplus.com/reference/random/minstd_rand0/)
class MinStdLinCongRandomGenerator
{
public:
    MinStdLinCongRandomGenerator(const int seed = 27)
    {
        setSeed(seed);
    }

    void setSeed(const int s)
    {
        const int a = 16807;
        const int m = 2147483647;
        const int q = 127773;
        const int r = 2836;
        int j, k;

        if (s == 0)
            seed = 1;
        else if (s < 0)
            seed = -s;
        else
            seed = s;

        for (j = 32 + 7; j >= 0; j--)
        {
            k = seed / q;
            seed = a * (seed - k * q) - r * k;
            if (seed < 0)
                seed += m;
            if (j < 32)
                table[j] = seed;
        }
        state = table[0];
    }

    int getInt()
    {
        const int a = 16807;
        const int m = 2147483647;
        const int q = 127773;
        const int r = 2836;

        int k = seed / q;
        seed = a * (seed - k * q) - r * k;
        if (seed < 0)
            seed += m;
        int j = state / (1 + (2147483647 - 1) / 32);
        state = table[j];
        table[j] = seed;

        return state;
    }

    int getInt(int limit)
    {
        return getInt() % limit;
    }
    float getFloat()
    {
        return min(getInt() / 2147483647.0f, 1.0f - float(ulp));
    }
    double getDouble()
    {
        return min(getInt() / 2147483647.0, 1.0 - double(ulp));
    }

private:
    int seed;
    int state;
    int table[32];
};

} // namespace shn