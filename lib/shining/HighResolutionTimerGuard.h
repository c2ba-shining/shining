#pragma once

#include <cassert>
#include <chrono>

namespace shn
{

typedef std::chrono::high_resolution_clock HighResolutionClock;
typedef HighResolutionClock::time_point TimerStruct;
typedef std::chrono::microseconds Microseconds;

inline double us2sec(uint64_t t)
{
    return double(t / uint64_t(1000000)) + double(t % uint64_t(1000000)) * 0.000001;
}

inline void GetHighResolutionTime(TimerStruct * timer)
{
    *timer = HighResolutionClock::now();
}

inline double ConvertTimeDifferenceToSec(TimerStruct * end, TimerStruct * start)
{
    return us2sec(std::chrono::duration_cast<Microseconds>(*end - *start).count());
}

struct HighResolutionTimerGuard
{
    HighResolutionTimerGuard(uint64_t * useconds): m_useconds(useconds)
    {
#ifndef DISABLE_PERFORMANCE_COUNTERS
        if (m_useconds)
        {
            m_start = HighResolutionClock::now();
        }
#endif
    }

    ~HighResolutionTimerGuard()
    {
        if (m_useconds)
        {
            release();
        }
    }

    void release()
    {
#ifndef DISABLE_PERFORMANCE_COUNTERS
        if (m_useconds)
        {
            const auto end = HighResolutionClock::now();
            const auto delta = std::chrono::duration_cast<Microseconds>(end - m_start).count();
            assert(delta >= 0);
            *m_useconds += delta;
        }
#endif
        m_useconds = nullptr;
    }

private:
    uint64_t * m_useconds;
    TimerStruct m_start;
};

struct HighResolutionTimerSecondsGuard
{
    HighResolutionTimerSecondsGuard(float * seconds): m_seconds(seconds)
    {
#ifndef DISABLE_PERFORMANCE_COUNTERS
        if (m_seconds)
        {
            m_start = HighResolutionClock::now();
        }
#endif
    }

    ~HighResolutionTimerSecondsGuard()
    {
        if (m_seconds)
        {
            release();
        }
    }

    void release()
    {
#ifndef DISABLE_PERFORMANCE_COUNTERS
        const auto end = HighResolutionClock::now();
        const auto delta = std::chrono::duration_cast<Microseconds>(end - m_start).count();
        assert(delta >= 0);
        *m_seconds += (float) us2sec(delta);
#endif
        m_seconds = nullptr;
    }

private:
    float * m_seconds;
    TimerStruct m_start;
};

} // namespace shn