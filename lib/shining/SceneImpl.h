#pragma once

#ifndef __SHINING_SCENE_IMPL_H__
#define __SHINING_SCENE_IMPL_H__

#include "Attributes.h"
#include "Cameras.h"
#include "HighResolutionTimerGuard.h"
#include "IMathUtils.h"
#include "Instance.h"
#include "MotionTransform.h"
#include "OslHeaders.h"
#include "OslRenderer.h"
#include "Primitive.h"
#include "Scene.h"
#include "Shining.h"
#include "embree2/rtcore.h"
#include "embree2/rtcore_ray.h"
#include "sampling/PiecewiseDistributions.h"
#include <unordered_map>
#include <vector>

OSL_NAMESPACE_ENTER
struct ShaderGlobals;
OSL_NAMESPACE_EXIT

namespace shn
{
// static ustring used in commitScene() and MeshProcessing.cpp
extern const ustring Attr_SubdivLevel;
extern const ustring Attr_SubdivAdaptive;
extern const ustring Attr_SubdivMethod;
extern const ustring Attr_SubdivVertexInterpolateBoundary;
extern const ustring Attr_SubdivFVarInterpolateBoundary;
extern const ustring Attr_SubdivFVarPropagateCornerValue;
extern const ustring Attr_SubdivComputeNormals;
extern const ustring Attr_SubdivCreasingMethod;
extern const ustring Attr_VertexCreases;
extern const ustring Attr_EdgeCreases;
extern const ustring Attr_VertsPerEdge;
extern const ustring Attr_Displaced;
extern const ustring Attr_DispUniqueVtxTopoIds;
extern const ustring Attr_DispOnlyDisplacement;
extern const ustring Attr_PrimitiveType;
extern const ustring Attr_CurveType;
extern const ustring Attr_CurvePeriodicity;
extern const ustring Attr_CurveLength;

struct SHINING_ALIGN(16) Ray
{
    static const uint32_t INVALID_ID = RTC_INVALID_GEOMETRY_ID;

    Ray()
    {
        // Do nothing
    }

    Ray(const V3f & org, const V3f & dir, float tnear = 0.f, float tfar = inf, float time = 0.f, uint32_t mask = 0xFFFFFFFF):
        org(org), dir(dir), tnear(tnear), tfar(tfar), time(time), mask(mask), geomID(INVALID_ID), primID(INVALID_ID), instID(INVALID_ID)
    {
    }

    // Ray
    V3f org;
    float align0;
    V3f dir;
    float align1;
    float tnear;
    float tfar;
    float time;
    uint32_t mask;

    // Hit
    V3f Ng;
    float align2;
    float u;
    float v;
    uint32_t geomID;
    uint32_t primID;
    uint32_t instID;

    friend inline V3f geometricNormal(const Ray & ray)
    {
        return -ray.Ng; // Embree 2 convention is to have the normal pointing inside
                        // #todo Embree 3:
        /*
        We flipped the direction of the geometry normal to the widely used convention that a shape with counter-clockwise layout of vertices has the normal pointing upwards (right-hand rule).
        Most modeling tools follow that convention.
        */
    }
};

struct RayDifferentials
{
    V3f dOdx;
    V3f dOdy;
    V3f dDdx;
    V3f dDdy;

    RayDifferentials() = default;

    RayDifferentials(const V3f & dOdx, const V3f & dOdy, const V3f & dDdx, const V3f & dDdy): dOdx{ dOdx }, dOdy{ dOdy }, dDdx{ dDdx }, dDdy{ dDdy }
    {
    }
};

class RayBatch
{
    Ray * m_rays = nullptr;
    RayDifferentials * m_differentials = nullptr;
    uint32_t * m_types = nullptr;
    size_t m_count = 0;
    size_t m_offset = 0;

public:
    RayBatch(Ray * rays, RayDifferentials * rayDiffs, uint32_t * types, size_t count, size_t offset = 0): m_rays{ rays }, m_differentials{ rayDiffs }, m_types{ types }, m_count{ count }
    {
    }

    RayBatch(const RayBatch & batch, size_t offset = 0): RayBatch{ batch.m_rays, batch.m_differentials, batch.m_types, batch.m_count, offset }
    {
    }

    friend inline void swap(RayBatch & rayBatch, size_t i, size_t j)
    {
        std::swap(rayBatch.rays()[i], rayBatch.rays()[j]);
        std::swap(rayBatch.differentials()[i], rayBatch.differentials()[j]);
        std::swap(rayBatch.types()[i], rayBatch.types()[j]);
    }

    Ray * rays()
    {
        return m_rays + m_offset;
    }

    RayDifferentials * differentials()
    {
        return m_differentials + m_offset;
        ;
    }

    uint32_t * types()
    {
        return m_types + m_offset;
        ;
    }

    const Ray * rays() const
    {
        return m_rays + m_offset;
    }

    const RayDifferentials * differentials() const
    {
        return m_differentials + m_offset;
    }

    const uint32_t * types() const
    {
        return m_types + m_offset;
    }

    size_t count() const
    {
        return m_count - m_offset;
    }
};

struct InstanceMeshSampledPoint
{
    int32_t triangleId = 0;
    float triangleArea = 0.f;
    V3f position = V3f(0.f);
    V3f normal = V3f(0.f);
    V2f texcoord = V2f(0.f);
    float pdf = 0.f;
};

struct VolumeInstList
{
    VolumeInstList() = default;

    VolumeInstList(int32_t * volumeInstBuffer, size_t volumeInstCount): volumeInstBuffer(volumeInstBuffer), volumeInstCount(volumeInstCount)
    {
        std::fill(volumeInstBuffer, volumeInstBuffer + volumeInstCount, -1);
    }

    VolumeInstList(int32_t * volumeInstBuffer, size_t volumeInstCount, const int32_t * initialList): volumeInstBuffer(volumeInstBuffer), volumeInstCount(volumeInstCount)
    {
        std::copy(initialList, initialList + volumeInstCount, volumeInstBuffer);
    }

    VolumeInstList(int32_t * volumeInstBuffer, const VolumeInstList & initalList): VolumeInstList(volumeInstBuffer, initalList.volumeInstCount, initalList.volumeInstBuffer)
    {
    }

    int32_t * volumeInstBuffer = nullptr; // A pointer to a list of volume instance ID
    int32_t volumeInstCount = 0; // The size of this list
};

class SceneImpl
{
public:
    struct Mesh
    {
        enum ProcessingStep
        {
            BASE = 0,
            SUBDIVISION,
            DISPLACEMENT,
            COUNT
        };

        Mesh() = default;
        Mesh(Mesh && other);
        Mesh & operator=(Mesh && other);
        void swap(Mesh & other);
        void clear();
        void copy(Mesh & other);
        size_t memoryFootPrint() const;

        Attribute faces;

        std::vector<Attribute> topologies;

        // constants attributes for subdivision, displacement and creases
        ProcessingStep step = BASE;
        Attributes processingAttributes;

        // vertices
        typedef TimedAttribute VertexAttribute;
        std::vector<VertexAttribute> vertexAttributes;
        int8_t positionAttrIndex = -1;
        int8_t normalAttrIndex = -1;
        int8_t texCoordAttrIndex = -1;

        // faces
        typedef Attribute FaceAttribute;
        std::vector<FaceAttribute> faceAttributes;
        int8_t shadingGroupAttrIndex = -1;

        // general
        Box3f bounds;

        /// If not NULL, the mesh used to generate this mesh.
        Mesh * baseMesh = nullptr;

        bool processed = false;

    private:
        Mesh(const Mesh &) = delete;
        Mesh & operator=(const Mesh &) = delete;
    };

    struct Instance
    {
        Instance() = default;
        Instance(Instance && other);
        Instance & operator=(Instance && other);
        Instance(const Instance &) = delete;
        Instance & operator=(const Instance &) = delete;

        void swap(Instance & other);
        void clear();
        size_t memoryFootprint() const;

        Scene::Id meshId;
        MotionTransform itransform;
        OSL::ustring name = OSL::ustring("");
        uint8_t visibility; // flags

        // The CDF by face area need to be on instance, because this CDF can change depending on instance's localToWorld matrix (think about non-uniform scale)
        TimedDistribution1D faceAreaCDF; // one cdf by mesh frame
        std::vector<float> meshArea;
        std::vector<float> meshAreaTimes;

        Attributes genericAttributes;
    };

public:
    SceneImpl();
    ~SceneImpl();

    Scene::Status attribute(const char * name, const char * value);
    Scene::Status attribute(const char * name, float value);
    Scene::Status attribute(const char * name, const float * value, int32_t count);
    Scene::Status attribute(const char * name, int32_t value);
    Scene::Status genMesh(Scene::Id * meshIds, int32_t count);
    Scene::Status meshAttributeData(Scene::Id meshId, OSL::ustring attrName, int32_t count, int32_t componentCount, const float * data, Scene::BufferOwnership ownership);
    Scene::Status meshAttributeData(Scene::Id meshId, OSL::ustring attrName, int32_t count, int32_t componentCount, const int * data, Scene::BufferOwnership ownership);
    Scene::Status meshAttributeData(Scene::Id meshId, OSL::ustring attrName, int32_t lenght, const char * data, Scene::BufferOwnership ownership);
    Scene::Status meshFaceData(Scene::Id meshId, int32_t count, const int32_t * faces, Scene::BufferOwnership ownership);
    Scene::Status enableMeshTopology(Scene::Id meshId, int32_t count);
    Scene::Status meshTopologyData(Scene::Id meshId, int32_t topologyId, int32_t count, const int32_t * vertices, Scene::BufferOwnership ownership);
    Scene::Status enableMeshVertexAttributes(Scene::Id meshId, int32_t count);
    size_t getMeshVertexAttributeCount(Scene::Id meshId) const;
    Scene::Status
    meshVertexAttributeData(Scene::Id meshId, int32_t topologyId, OSL::ustring attrName, uint32_t count, int32_t countComponent, const float * data, Scene::BufferOwnership ownership);
    Scene::Status meshVertexAttributeData(
        Scene::Id meshId, int32_t topologyId, OSL::ustring attrName, uint32_t count, int32_t countComponent, const float ** data, const float * timeValues, const int32_t frameCount,
        Scene::BufferOwnership ownership);
    Scene::Status enableMeshFaceAttributes(Scene::Id meshId, int32_t count);
    Scene::Status meshFaceAttributeData(Scene::Id meshId, OSL::ustring attrName, uint32_t count, int32_t countComponent, const float * data, Scene::BufferOwnership ownership);
    Scene::Status meshFaceAttributeData(Scene::Id meshId, OSL::ustring attrName, uint32_t count, int32_t countComponent, const int * data, Scene::BufferOwnership ownership);
    Scene::Status commitMesh(Scene::Id meshId);
    int32_t getShadingGroupFromFace(Scene::Id meshId, int32_t faceId) const;

    Scene::Status genInstances(Scene::Id * instanceIds, const int32_t count);
    Scene::Status instanceMesh(const Scene::Id * instanceIds, const int32_t count, const Scene::Id meshId);
    Scene::Status instanceName(const Scene::Id * instanceIds, const int32_t count, const OSL::ustring name);
    Scene::Status instanceTransform(const Scene::Id * instanceIds, const int32_t count, const float * transform, const float * timeValues, const int32_t frameCount);
    Scene::Status instanceInstantiatedTransforms(const Scene::Id * instanceIds, const float * transforms, const int32_t count, const float timeValue);
    Scene::Status instanceVisibility(const Scene::Id * instanceIds, const int32_t count, Scene::InstanceVisibility type, Scene::Visibility value);
    Scene::Status instanceInstantiatedVisibilities(const Scene::Id * instanceIds, Scene::Visibility * values, const int32_t count, Scene::InstanceVisibility type);
    Scene::Status instanceGenericAttributeData(const Scene::Id * instanceIds, const int32_t count,
        const OSL::ustring attrName, const char * typeName, const int32_t dataCount, const char * data,
        const float * timeValues, const int32_t frameCount, const Scene::BufferOwnership ownership);
    Scene::Status getInstanceTransform(Scene::Id instanceId, M44f & transform, float time = 0.f) const;
    Scene::Status getGenericInstanceAttributeData(Scene::Id instanceId, OSL::ustring attrName, int32_t * count, int32_t * countComponent, const void ** data, float time);
    Scene::Status getGenericInstanceAttributeType(Scene::Id instanceId, OSL::ustring attrName, const char ** typeName);
    bool getInstanceVisibility(Scene::Id instanceId, Scene::InstanceVisibility visibility) const;
    Scene::Status commitInstance(const Scene::Id * instanceIds, const int32_t count);
    OSL::ustring getInstanceNameFromId(Scene::Id instanceId) const;
    int32_t countInstanceIdFromName(const OSL::ustring & name) const;
    void getInstanceIdsFromName(const OSL::ustring & name, Scene::Id * instanceIds, const int32_t instanceCount) const;
    inline size_t countInstanceIds() const
    {
        return m_instances.size();
    }
    int32_t getMeshIdFromInstanceId(Scene::Id instanceId) const;
    bool isBlackholeInstance(Scene::Id instanceId, Scene::InstanceVisibility visibility) const;
    bool isHiddenInstance(Scene::Id instanceId) const;
    bool isCurves(Scene::Id meshId) const;

    Scene::Status genCameras(Scene::Id * cameraIds, int32_t count);

    Scene::Status cameraAttribute(Scene::Id cameraId, OSL::ustring attrName, OSL::ustring attrType, int32_t dataByteSize, const void * data);

    Scene::Status cameraAttribute(Scene::Id cameraId, OSL::ustring attrName, OSL::ustring attrType, int32_t count, const void ** data, int32_t frameCount, const float * timeValues);

    Scene::Status commitCamera(Scene::Id cameraId);
    size_t getCameraCommitCount(Scene::Id cameraId) const;

    std::unique_ptr<Camera> getCamera(Scene::Id cameraId, float shutterOffset) const;

    Scene::Id getCameraIdFromName(const char * name) const;
    M44f getScreenToCameraTransform(Scene::Id cameraId) const;
    M44f getCameraToWorldTransform(Scene::Id cameraId, float timeSample, float & worldTime) const;
    Scene::Status getCameraDepthOfField(Scene::Id cameraId, float * focalDistance, float * lensRadius) const;
    Scene::Status getCameraFrustrum(Scene::Id cameraId, float * nearPlane, float * farPlane, float * focalLength, float * hfovY, float * aspectRatio) const;
    Scene::Status getCameraShutter(Scene::Id cameraId, float * openTime, float * closeTime) const;
    Scene::Status getCameraStereo(Scene::Id cameraId, float * iod) const;
    CameraType getCameraType(Scene::Id cameraId) const;
    const char * getCameraName(Scene::Id cameraId) const;
    int32_t getCameraCount() const
    {
        return (int32_t) m_cameraAttributes.size();
    }
    bool isCameraInInstance(const Scene::Id cameraId, const Scene::Id instanceId) const;
    std::pair<Ray, RayDifferentials> sampleCameraRay(const Camera & camera, const OSL::Dual2<V2f> & imageSample, const V2f & lensSample, float timeSample) const;

    Scene::Status commitScene(OslRenderer * renderer);
    void getSceneBoundingBox(float * const min, float * const max);
    Scene::Status intersect(const Scene::Ray & ray, uint8_t rayType, Scene::Hit & hit) const;

    void intersect(Ray & ray) const;
    void intersect(Ray & ray, const Scene::Id instanceId) const;

    void intersect(Ray * rays, size_t count) const;
    void intersect(Ray * rays, size_t count, const Scene::Id instanceId) const;

    void buildVolumeScene(const Scene::Id * volumeInstanceIds, int32_t volumeCount);
    void initialVolumeInclusionTest(
        Ray * cameraRays, size_t count, const Scene::Id * volumeInstanceIds, int32_t volumeCount, const std::unordered_map<Scene::Id, int32_t> & volInstIdsToBufferIndex,
        VolumeInstList * perPathVolumeInstList, int32_t bounces);

    Scene::Status sampleInstanceVertexAttribute(
        Scene::Id instanceId, int32_t countVertexAttribute, const Scene::Hit & hit, Scene::TransformSpace, float * vertexAttributeData, float * derivativeData, int32_t derivativeAttributeId);

    void globalsFromHit(OSL::ShaderGlobals * sg, const Ray & r, const RayDifferentials & rd, bool flip, float & normalOffset, float & rayNear) const;
    void globalsFromVolumeHit(OSL::ShaderGlobals * sg, const Ray & r, float dist) const;
    void globalsFromCurveHit(OSL::ShaderGlobals * sg, const Ray & r, const RayDifferentials & rd, bool flip, float & normalOffset, float & rayNear) const;

    InstanceMeshSampledPoint sampleInstanceMesh(float uPoint1, float uPoint2, float uTriangle, const float time, const Scene::Id instanceId) const;
    InstanceMeshSampledPoint evalInstanceMesh(const V3f & position, const V3f & direction, const float time, const Scene::Id instanceId) const;
    float getInstanceMeshArea(const Scene::Id instanceId, const float time) const;

    Scene::Status statisticsReport(std::ostream & out) const;

    const Box3f & getBounds() const
    {
        return m_bounds;
    }

private:
    SceneImpl(const SceneImpl &);
    Scene & operator=(const SceneImpl &);

    void freeMesh(Scene::Id meshId);
    void processMesh(Scene::Id meshId);
    int32_t subdivideMesh(Scene::Id meshId);

    int32_t buildMeshAccel(Scene::Id meshId);
    int32_t buildMeshStaticAccel(Scene::Id meshId, Scene::Id instanceId);
    int32_t buildMeshStaticInstancedAccel(Scene::Id meshId);
    int32_t buildMeshMotionAccel(Scene::Id meshId);
    int32_t buildVolumeMeshStaticAccel(Scene::Id instanceId);
    int32_t computeDisplacement(const std::vector<size_t> & instanceIds, OslRenderer * renderer);
    const std::pair<Mesh *, Mesh *> allocateOneMeshDataLevel(Scene::Id meshId);

    int32_t buildCurvesAccel(Scene::Id meshId);
    int32_t buildCurvesStaticInstancedAccel(Scene::Id meshId);
    int32_t buildCurvesMotionAccel(Scene::Id meshId);
    int32_t computeCurveVertexAttributes(Scene::Id meshId);
#ifdef SHN_CONVERT_CURVES_TO_TRIANGLES
    void convertCurvesToTriangles(Scene::Id meshID);
#endif

    enum ComputeNormalCause
    {
        AFTER_SUBDIVISION,
        AFTER_DISPLACEMENT,
    };
    int32_t computeNormals(Scene::Id meshId, ComputeNormalCause cause);
    int32_t computeInstanceFaceAreaCDF(Scene::Id instanceId, const OslRenderer * renderer);

    void initCamera(Scene::Id cameraId);
    void freeCamera(Scene::Id cameraId);

    void initInstance(Scene::Id instanceId);
    void freeInstance(Scene::Id instanceId);

    void initPrimitive(Scene::Id primitiveId);
    void freePrimitive(Scene::Id primitiveId);

    // compute an estimate of the geometry footprint, including Embree data and Shining structures. Fill the corresponding member variables
    // WARNING: This is an estimate for comparison purpose only !
    // To keep the code simple, we don't track certain variables, count twice some others and ignore ustrings (which have there own memory system)
    size_t estimateGeometryMemoryFootprint();

    std::vector<Mesh> m_meshes;
    std::vector<Instance> m_instances;
    std::vector<Attributes> m_cameraAttributes;
    std::vector<size_t> m_cameraCommitCount;

    std::vector<MotionPrimitive> m_primitives;

    RTCDevice m_rtcDevice;
    RTCScene m_rtcRootScene;
    RTCScene m_rtcStaticScene;
    unsigned m_rtcStaticSceneId;
    RTCScene m_rtcVolumeScene;
    std::vector<unsigned> m_rtcInstanceIds;
    std::vector<unsigned> m_rtcStaticMeshIds;
    std::vector<unsigned> m_rtcVolumeSceneIds;
    std::vector<StaticInstance> m_staticInstances;
    std::vector<MotionInstance> m_motionInstances;

    /// Memory footprints in bytes
    size_t m_geometryMemoryFootprintEstimate = 0;
    size_t m_embreeMemoryFootprint = 0;
    size_t m_embreeIdsMemoryFootprint = 0;
    size_t m_meshesMemoryFootprint = 0;
    size_t m_instancesMemoryFootprint = 0;
    size_t m_motionPrimitivesMemoryFootprint = 0;
    size_t m_internalInstancesMemoryFootprint = 0;

    /// Time in constructor
    TimerStruct m_constructorTime;
    /// Time after commitScene
    TimerStruct m_commitSceneTime;
    // Subdivision time
    float m_subdivTime;
    // Acceleration structure build time
    float m_accelBuildTime;

    /// Scene stats
    int64_t m_visibleFaceCount;
    int64_t m_visibleInstanceCount;
    int64_t m_visibleGeometryCount;

    Box3f m_bounds;

    /// Scene attributes
    Attributes m_attributes;

public:
    static const OSL::ustring Attr_Position;
    static const OSL::ustring Attr_Normal;
    static const OSL::ustring Attr_TexCoord;
    static const OSL::ustring Attr_ShadingGroupIndex;
    static const OSL::ustring Attr_ShadingGroupChunks;

    static RTCAlgorithmFlags getRTCAlgorithmFlags()
    {
        return RTC_INTERSECT1;
    }
};
} // namespace shn

#endif // __SHINING_SCENE_IMPL_H__
