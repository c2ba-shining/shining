#pragma once

#include "Shining.h"
#include <chrono>
#include <ctime>
#include <iostream>

#define SHN_LOGINFO shn::LoggerINFO::out()
#define SHN_LOGERR shn::LoggerERROR::out()
#define SHN_LOGWARN shn::LoggerWARNING::out()

#define SHN_LOG_EXPR(EXPR) #EXPR << " = " << (EXPR)

namespace shn
{

SHINING_EXPORT void localtime(struct tm * out, const time_t * time);

struct OstreamWrapper
{
    std::ostream & stream;

    OstreamWrapper(std::ostream & stream, const char * severity): stream(stream)
    {
        auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        char buffer[32];
        struct tm localTime;
        shn::localtime(&localTime, &now);
        strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &localTime);
        stream << buffer << " " << severity << " ";
    }

    ~OstreamWrapper()
    {
        stream << "\n";
    }
};

template<typename T>
const OstreamWrapper & operator<<(const OstreamWrapper & out, T && value)
{
    out.stream << value;
    return out;
}

class LoggerINFO
{
public:
    static OstreamWrapper out()
    {
        return OstreamWrapper(std::clog, "INFO");
    }
};

class LoggerERROR
{
public:
    static OstreamWrapper out()
    {
        return OstreamWrapper(std::cerr, "ERROR");
    }
};

class LoggerWARNING
{
public:
    static OstreamWrapper out()
    {
        return OstreamWrapper(std::cerr, "WARNING");
    }
};

} // namespace shn