#include "MotionTransform.h"
#include "IMathUtils.h"
#include "OpenEXR/ImathMatrixAlgo.h"
#include "Shining_p.h"
#include <algorithm>
#include <cassert>

namespace shn
{
void timeToLocalTime(const MotionTransform & m, const float time, int * frame, float * localTime);

MotionTransform::MotionTransform(): frameCount(-1)
{
}

MotionTransform::MotionTransform(int32_t frameCount, const float * transforms, const float * times): frameCount(frameCount)
{
    assert(frameCount > 0);
    this->transforms.resize(frameCount, Imath::UNINITIALIZED);
    this->times.resize(frameCount);
    for (int32_t i = 0; i < frameCount; ++i)
    {
        this->transforms[i] = fv16ToM44f(transforms + 16 * i);
        this->times[i] = times ? times[i] : 0.f;
    }
}

MotionTransform::MotionTransform(int32_t frameCount, const float ** transforms, const float * times): frameCount(frameCount)
{
    assert(frameCount > 0);
    this->transforms.resize(frameCount, Imath::UNINITIALIZED);
    this->times.resize(frameCount);
    for (int32_t i = 0; i < frameCount; ++i)
    {
        this->transforms[i] = fv16ToM44f(transforms[i]);
        this->times[i] = times ? times[i] : 0.f;
    }
}

MotionTransform::MotionTransform(int32_t frameCount): frameCount(frameCount)
{
    assert(frameCount > 0);
    this->transforms.resize(frameCount, Imath::UNINITIALIZED);
    this->times.resize(frameCount);
}

void MotionTransform::addFrame(const float time, const float * transform)
{
    times.push_back(time);
    std::sort(times.begin(), times.end());
    auto it = std::unique(times.begin(), times.end());
    times.resize(std::distance(times.begin(), it));
    frameCount = times.size();

    auto foundTime = std::find(times.begin(), times.end(), time);
    assert(foundTime != times.end());
    size_t timeIndex = std::distance(times.begin(), foundTime);
    transforms.insert(transforms.begin() + timeIndex, fv16ToM44f(transform));
}

size_t MotionTransform::memoryFootprint() const
{
    size_t byteSize = sizeof(*this);
    byteSize += transforms.size() * sizeof(M44f);
    byteSize += times.size() * sizeof(float);
    return byteSize;
}

MotionTransform inverse(const MotionTransform & m)
{
    assert(m.frameCount > 0);
    MotionTransform out(m.frameCount);
    for (int32_t i = 0; i < m.frameCount; ++i)
    {
        out.transforms[i] = m.transforms[i].inverse();
        out.times[i] = m.times[i];
    }
    return out;
}

MotionTransform normalTransforms(const MotionTransform & m)
{
    assert(m.frameCount > 0);
    MotionTransform out(m.frameCount);
    for (int32_t i = 0; i < m.frameCount; ++i)
    {
        out.transforms[i] = m.transforms[i].inverse().transpose();
        out.times[i] = m.times[i];
    }
    return out;
}

Imath::Box3f xfmBounds(const MotionTransform & m, const Imath::Box3f & bounds)
{
    Box3f b;
    const V4f p000(bounds.min.x, bounds.min.y, bounds.min.z, 1.0);
    const V4f p001(bounds.min.x, bounds.min.y, bounds.max.z, 1.0);
    const V4f p010(bounds.min.x, bounds.max.y, bounds.min.z, 1.0);
    const V4f p011(bounds.min.x, bounds.max.y, bounds.max.z, 1.0);
    const V4f p100(bounds.max.x, bounds.min.y, bounds.min.z, 1.0);
    const V4f p101(bounds.max.x, bounds.min.y, bounds.max.z, 1.0);
    const V4f p110(bounds.max.x, bounds.max.y, bounds.min.z, 1.0);
    const V4f p111(bounds.max.x, bounds.max.y, bounds.max.z, 1.0);
    for (int32_t i = 0; i < m.frameCount; ++i)
    {
        b.extendBy(V3f(p000 * m.transforms[i]));
        b.extendBy(V3f(p001 * m.transforms[i]));
        b.extendBy(V3f(p010 * m.transforms[i]));
        b.extendBy(V3f(p011 * m.transforms[i]));
        b.extendBy(V3f(p100 * m.transforms[i]));
        b.extendBy(V3f(p101 * m.transforms[i]));
        b.extendBy(V3f(p110 * m.transforms[i]));
        b.extendBy(V3f(p111 * m.transforms[i]));
    }
    return b;
}

M44f slerp(const MotionTransform & m, const float time)
{
    if (m.frameCount == 1)
        return m.transforms[0];
    float localTime = time;
    auto lowtPtr = std::upper_bound(m.times.begin(), m.times.end(), localTime);
    int32_t lowFrame = max((int32_t)(lowtPtr - m.times.begin() - 1), 0);
    int32_t highFrame = (lowFrame < (int32_t) m.frameCount - 1) ? lowFrame + 1 : lowFrame;
    float lowTime = m.times[lowFrame];
    float highTime = m.times[highFrame];
    if (time <= lowTime)
        return m.transforms[lowFrame];
    if (time >= highTime)
        return m.transforms[highFrame];
    float t = (localTime - lowTime) / (highTime - lowTime);
#if 0 // TODO decompose matrix in translate + scale + quat in preprocess
    Imath::Quatf q1 = extractQuat(m.transforms[lowFrame]).normalized();
    Imath::Quatf q2 = extractQuat(m.transforms[highFrame]).normalized();
    return slerpShortestArc(q1, q2, t).toMatrix44();
#endif
    return (1.f - t) * m.transforms[lowFrame] + t * m.transforms[highFrame];
}

M44f slerp(float time, int32_t frameCount, const float ** transforms, const float * times)
{
    if (frameCount == 1)
        return fv16ToM44f(transforms[0]);
    float localTime = time;
    auto lowtPtr = std::upper_bound(times, times + frameCount, localTime);
    int32_t lowFrame = lowtPtr - times - 1;
    int32_t highFrame = (lowFrame < (int32_t) frameCount - 1) ? lowFrame + 1 : lowFrame;
    float lowTime = times[lowFrame];
    float highTime = times[highFrame];
    if (time <= lowTime)
        return fv16ToM44f(transforms[lowFrame]);
    if (time >= highTime)
        return fv16ToM44f(transforms[highFrame]);
    float t = (localTime - lowTime) / (highTime - lowTime);
#if 0 // TODO decompose matrix in translate + scale + quat in preprocess
    Imath::Quatf q1 = extractQuat(m.transforms[lowFrame]).normalized();
    Imath::Quatf q2 = extractQuat(m.transforms[highFrame]).normalized();
    return slerpShortestArc(q1, q2, t).toMatrix44();
#endif
    return (1.f - t) * fv16ToM44f(transforms[lowFrame]) + t * fv16ToM44f(transforms[highFrame]);
}

void timeToLocalTime(const MotionTransform & m, const float time, int * frame, float * localTime)
{
    auto lowtPtr = std::upper_bound(m.times.begin(), m.times.begin() + m.frameCount, time);
    int lowFrame = lowtPtr - m.times.begin() - 1;
    float lowTime = m.times[lowFrame];
    float highTime = lowTime + 1;
    if (time <= lowTime)
    {
        *localTime = 0.f;
        *frame = lowFrame;
        return;
    }
    if (time >= highTime)
    {
        *localTime = 1.f;
        *frame = lowFrame;
        return;
    }
    *localTime = (time - lowTime) / (highTime - lowTime);
    *frame = lowFrame;
}

} // namespace shn