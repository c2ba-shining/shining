#include "OslRendererTask.h"
#include "Binding.h"
#include "Cameras.h"
#include "DebugUtils.h"
#include "Displacement.h"
#include "HighResolutionTimerGuard.h"
#include "LoggingUtils.h"
#include "OslRenderer.h"
#include "OslRendererImpl.h"
#include "Scene.h"
#include "SceneImpl.h"
#include "Shining_p.h"
#include "importance_caching/LightingImportanceCache.h"
#include "lights/Lights.h"
#include "lpe/LPE.h"
#include "sampling/PatternSamplers.h"
#include "sampling/ShapeSamplers.h"
#include "shading/BSSRDFSampling.h"
#include "shading/CompositeBxDF.h"
#include "shading/CustomClosure.h"
#include "shading/ShiningRendererServices.h"
#include <algorithm>
#include <atomic>
#include <cassert>
#include <cmath>
#include <set>
#include <stdlib.h>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace shn
{

const int OslRendererTask::TILE_SIZE;

static const int TILE_SIZE = OslRendererTask::TILE_SIZE;
static const int TILE_PIXEL_COUNT = OslRendererTask::TILE_PIXEL_COUNT;

static const int BOUNCES_LIMIT = OslRendererImpl::BOUNCES_LIMIT;
static const int DEFAULT_MAX_BOUNCES = OslRendererImpl::DEFAULT_MAX_BOUNCES;
static const int DEFAULT_DIFFUSE_BOUNCES = OslRendererImpl::DEFAULT_DIFFUSE_BOUNCES;
static const int DEFAULT_GLOSSY_BOUNCES = OslRendererImpl::DEFAULT_GLOSSY_BOUNCES;
static const int DEFAULT_REFLECTION_BOUNCES = OslRendererImpl::DEFAULT_REFLECTION_BOUNCES;
static const int DEFAULT_REFRACTION_BOUNCES = OslRendererImpl::DEFAULT_REFRACTION_BOUNCES;
static const int DEFAULT_TRANSPARENCY_BOUNCES = OslRendererImpl::DEFAULT_TRANSPARENCY_BOUNCES;
static const int DEFAULT_SHADOW_BOUNCES = OslRendererImpl::DEFAULT_SHADOW_BOUNCES;
static const int DEFAULT_VOLUME_BOUNCES = OslRendererImpl::DEFAULT_VOLUME_BOUNCES;

namespace
{
struct TileToRect
{
    using TileSelectFunc = size_t (TileToRect::*)(size_t) const;

    const size_t m_rectStartX;
    const size_t m_rectEndX;
    const size_t m_rectStartY;
    const size_t m_rectEndY;
    const size_t m_tileCountX;
    const size_t m_tileCountY;
    const TileSelectFunc m_func;

    TileToRect(size_t startx, size_t endx, size_t starty, size_t endy, const char * pattern);

    bool eval(size_t tileId, size_t * actualTile, size_t * startx, size_t * endx, size_t * starty, size_t * endy) const;

    size_t scanline(size_t tile) const;

    size_t spiral(size_t tile) const;
};

template<typename T>
void doSwap(T * ptr, size_t i, size_t j)
{
    std::swap(ptr[i], ptr[j]);
}

void doSwap(RayBatch & rayBatch, size_t i, size_t j)
{
    swap(rayBatch, i, j);
}

template<typename T>
void doSwap(std::vector<T> & v, size_t i, size_t j)
{
    std::swap(v[i], v[j]);
}

template<typename T>
void doSwap(vector_view<T> & v, size_t i, size_t j)
{
    std::swap(v[i], v[j]);
}

void doSwap(size_t i, size_t j)
{
    // Everything has been swapped
}

template<typename T, typename... Ts>
void doSwap(size_t i, size_t j, T && swappable, Ts &&... swappables)
{
    doSwap(std::forward<T>(swappable), i, j);
    doSwap(i, j, std::forward<Ts>(swappables)...);
}

template<typename TupleT, typename Index>
struct TupleSwapHelper;

template<typename TupleT>
struct TupleSwapHelper<TupleT, typename std::tuple_size<TupleT>::type>
{
    static void doSwap(const TupleT & swappables, size_t i, size_t j)
    {
        // End of recursion
    }
};

template<typename TupleT, typename Index>
struct TupleSwapHelper
{
    static void doSwap(const TupleT & swappables, size_t i, size_t j)
    {
        shn::doSwap(std::get<Index::value>(swappables), i, j);
        TupleSwapHelper<TupleT, std::integral_constant<size_t, Index::value + 1>>::doSwap(swappables, i, j);
    }
};

template<typename... Ts>
void doSwap(size_t i, size_t j, const std::tuple<Ts...> & swappables)
{
    TupleSwapHelper<std::tuple<Ts...>, std::integral_constant<size_t, 0>>::doSwap(swappables, i, j);
}

// Given a list of swappables (arrays, ray batch,...), and a predicate, reorders the swappables such all elements such that p(i) is true
// are put first. Return the number of elements such that p(i) is true.
// This function minimizes the number of swaps performed
template<typename Predicate, typename... Ts>
size_t removeIf(size_t count, Predicate p, const std::tuple<Ts...> & swappables)
{
    for (size_t i = 0; i < count; ++i)
    {
        // Find first element matching the condition
        if (p(i))
        {
            // Find last element not matching the condition
            --count;
            while (count > i && p(count))
                --count;

            // Swap them
            if (i < count)
                doSwap(i, count, swappables);
        }
    }
    return count;
}

template<typename TupleT, typename Index>
struct TupleResizeHelper;

template<typename TupleT>
struct TupleResizeHelper<TupleT, typename std::tuple_size<TupleT>::type>
{
    static void doResize(const TupleT & swappables, size_t newSize)
    {
        // End of recursion
    }
};

template<typename TupleT, typename Index>
struct TupleResizeHelper
{
    static void doResize(const TupleT & swappables, size_t newSize)
    {
        std::get<Index::value>(swappables).resize(newSize);
        TupleResizeHelper<TupleT, std::integral_constant<size_t, Index::value + 1>>::doResize(swappables, newSize);
    }
};

template<typename... Ts>
void resizeSwappables(size_t newSize, const std::tuple<Ts...> & swappables)
{
    TupleResizeHelper<std::tuple<Ts...>, std::integral_constant<size_t, 0>>::doResize(swappables, newSize);
}

template<typename PredicateT, typename SwappableT>
size_t terminatePathsIf(
    size_t count, OslRendererTask::Path * paths, vector_view<OslRendererTask::Path> & finishedPaths, const PredicateT & removePredicate, const SwappableT & swappables, uint64_t * perfCounter)
{
    HighResolutionTimerGuard g(perfCounter);
    const auto oldCount = count;
    count = removeIf(count, removePredicate, swappables);
    for (size_t i = count; i < oldCount; ++i)
    {
        finishedPaths.emplace_back(paths[i]);
    }
    resizeSwappables(count, swappables);
    return count;
}

inline float misWeight(float invpdf, float otherpdf)
{
    // Power Heuristic is: p1^2 / (p1^2 + p2^2)
    // Simplifying: 1 / (1 + (p2 / p1) ^ 2)
    return 1.f / (1.f + otherpdf * otherpdf * invpdf * invpdf);
}

inline float misPowerHeuristic(float sampled_pdf, float other_pdf)
{
    float r, mis;
    if (sampled_pdf > other_pdf)
    {
        r = other_pdf / sampled_pdf;
        mis = 1 / (1 + r * r);
    }
    else if (sampled_pdf < other_pdf)
    {
        r = sampled_pdf / other_pdf;
        mis = 1 - 1 / (1 + r * r);
    }
    else
    {
        // avoid (possible, but extremely rare) inf/inf cases
        assert(sampled_pdf == other_pdf);
        r = 1.0f;
        mis = 0.5f;
    }
    assert(r >= 0);
    assert(r <= 1);
    assert(mis >= 0);
    assert(mis <= 1);
    return mis;
}

} // namespace

OslRendererTask::PathSamplingState scatteringTypeToPathSamplingState(ScatteringType scatteringType);
void updateRayStatistics(size_t count, const OslRendererTask::Path * paths, uint64_t * perfCounters);
int32_t updatePathDepth(int32_t originalDepth, bool ignoreStraight, const BxDFDirSample & materialSample);
void updateBounceLimits(RayDepth & limits, ScatteringType scatteringType, ScatteringEvent scatteringEvent, ClosurePrimitive::Category closureCategory);
uint32_t updateRayType(uint32_t previousRayType, ScatteringType scatteringType, ScatteringEvent scatteringEvent, const OslRendererImpl & renderer);

OslRenderer::Status OslRendererTask::renderFrame() const
{
    const ShadingPerThreadInfo shadingThreadInfo{ *renderer->s_shadingSystem };
    ShadingMemoryPool shadingMemoryPool; // Shared by all closures
    ShadingMemoryPool bouncingMemoryPool;
    ClosureTreeEvaluator closureEvaluator{ shadingMemoryPool };
    const ShadingContext shadingCtx{ *renderer->s_shadingSystem, &shadingThreadInfo, renderer->m_oslShaderGroups, closureEvaluator };

    ThreadMemoryPool memoryPool;

    MemoryPool perPathVolumeInstanceListMemPool; // #todo should not be a memorypool, just a vector<Scene::Id> in thread mem pool

    const TileToRect tileMapper(inputs.startx, inputs.endx, inputs.starty, inputs.endy, inputs.tilePattern);

    SampleBuffer sampleBuffer(
        inputs.sampleCount, inputs.adaptiveSamplingEnabled ? inputs.adaptiveMinSamples : 0, TILE_PIXEL_COUNT, inputs.maxBounces,
        renderer->m_directIllumAlgorithm == OslRendererImpl::DirectIllumAlgorithm::AllLights, renderer->m_directIllumSampleCount);

    uint64_t threadPerCounters[STAT_MAX] = {};
    uint64_t tileMaxBounceCount = 0;

    std::vector<LightStats> lightStatistics(inputs.lights->visibleLightCount());

    InstancePerformances perfInstances;
    InstancePerformances shadowPerfInstances;

    std::vector<json> lightImportanceCacheStatistics; // Vector that will contain the statistics of each LightImportanceCache computed

    const auto tileCountX = 1 + (int32_t)(inputs.width / TILE_SIZE);
    const auto tileCountY = 1 + (int32_t)(inputs.height / TILE_SIZE);

    // Tile pick loop
    while (true)
    {
        uint64_t tilePerfCounters[STAT_MAX] = {};
        HighResolutionTimerGuard tilePerfGuard(&tilePerfCounters[STAT_RENDERFRAME]);

        {
            const auto lock = outputs.lock();
            if (outputs.sync._canceled)
            {
                break;
            }
        }

        size_t tile;
        V2<size_t> tileBegin, tileEnd;
        if (!tileMapper.eval(tileID++, &tile, &tileBegin.x, &tileEnd.x, &tileBegin.y, &tileEnd.y))
            break;

#ifdef SHN_DEBUG_PIXEL
        if (!(SHN_DEBUG_PIXEL.x >= tileBegin.x && SHN_DEBUG_PIXEL.y >= tileBegin.y && SHN_DEBUG_PIXEL.x <= tileEnd.x && SHN_DEBUG_PIXEL.y <= tileEnd.y))
        {
            continue;
        }
#endif
        const auto tileX = tileBegin.x / TILE_SIZE;
        const auto tileY = tileBegin.y / TILE_SIZE;

        RandomState samplerState{ (int32_t)(tileX + tileY * tileCountX + inputs.startSample * tileCountX * tileCountY) };

        {
            HighResolutionTimerGuard g(&tilePerfCounters[STAT_JITTERED_DISTRIB]);
            sampleBuffer.computeSamples(samplerState);
        }

        std::array<bool, TILE_PIXEL_COUNT> pixelNeedToonOutlineComputation;
        pixelNeedToonOutlineComputation.fill(false);

        // coverageCounters[pixelIdx][instId] is incremented each time the instance instId is hitted by a singular path traced through pixel pixelIdx
        std::array<std::unordered_map<Scene::Id, size_t>, TILE_PIXEL_COUNT> coverageCounters;

        // For each (i, j) pixel idx in tile, gives the corresponding sample idx before swap
        std::array<int32_t, TILE_PIXEL_COUNT> tilePixelIdxToSampleIdx;
        tilePixelIdxToSampleIdx.fill(-1);

        const auto pixelsInfo = initializePixelsInfo(tileBegin, tileEnd, tilePixelIdxToSampleIdx.data(), memoryPool);
        const auto pixelCount =
            pixelsInfo.size(); // #note this is not the number of pixels in tile, but the number of pixels that will be processed (taking into account mask and image coverage)

        auto adaptiveStats = make_view(memoryPool.adaptiveStats, pixelCount);

        LightingImportanceCache lightImportanceCache;
        LightingImportanceCacheDataStorage lightImportanceCacheDataStorage; // #todo: should be declared only in lighting function, it only holds data temporary to a lighting eval...

        if (inputs.needLighting && renderer->m_directIllumAlgorithm == OslRendererImpl::DirectIllumAlgorithm::ImportanceCaching)
        {
            HighResolutionTimerGuard tilePerfGuard(&threadPerCounters[STAT_IC_INIT]);
            lightImportanceCache.init(renderer->m_lights, TILE_PIXEL_COUNT, inputs.lightICProbaAlpha);
            lightImportanceCacheDataStorage.init(renderer->m_directIllumSampleCount, renderer->m_lights.visibleLightCount(), TILE_PIXEL_COUNT);
        }

        for (auto sampleID = 0; sampleID < inputs.sampleCount; ++sampleID)
        {
            shadingMemoryPool.clear();
            perPathVolumeInstanceListMemPool.clear();

            size_t count = pixelsInfo.size(); // Starts with one path sample per pixel

            // early exit : if the current tile has no computable pixel, exit the sample loop,
            // and consider the tile has finished
            if (!count)
            {
                // Set all the sample has computed in the tile info
                // Fix a infinite looping in Vertigo while interactive rendering, with a special-width viewport
                outputs.finishedSampleCountPerTile[tile] += inputs.sampleCount;
                break;
            }

            auto paths = make_view(memoryPool.paths);
            auto finishedPaths = make_view(memoryPool.finishedPaths);

            auto pathVolumeStates = make_view(memoryPool.pathVolumeStates);

            // Not swapped: should be accessed with samples[i].accumulatorIndex
            auto accumulators = make_view(memoryPool.pathAccumulators);

            // Not swapped: should be accessed with samples[i].pixelIdx
            auto toonOutlines = make_view(memoryPool.toonOutlines, count);
            auto bindingOutputs = make_view(memoryPool.bindingOutputs, pixelCount * outputs.binding.getAutomata().outputCount(), Color3(0.f));
            auto perPixelTransparency = make_view(memoryPool.perPixelTransparency, pixelCount, Color3(0.f));
            auto internalOutputs = make_view(memoryPool.internalOutputs, pixelCount * m_internalAutomata.outputCount(), Color3(0.f));

            {
                HighResolutionTimerGuard g(&tilePerfCounters[STAT_INITIALIZE]);

                // Init path of each pixel
                for (size_t pixelIdx = 0; pixelIdx < count; ++pixelIdx)
                {
                    auto & p = paths.emplace_back();

                    p.pixelIndex = pixelIdx;

                    p.uCamera = sampleBuffer.getCameraBuffer(sampleID) + pixelIdx;
                    p.uLight = sampleBuffer.getLightBuffer(sampleID) + pixelIdx;

                    if (renderer->m_directIllumAlgorithm != OslRendererImpl::DirectIllumAlgorithm::AllLights)
                    {
                        p.uChosenLight = sampleBuffer.getChosenLightBuffer(sampleID) + pixelIdx * renderer->m_directIllumSampleCount;
                    }

#ifdef SHN_ENABLE_DEBUG
                    p.pixel = pixelsInfo[pixelIdx].pixel;
#endif
                    auto volumeInstList = renderer->m_volumeInstancesCount > 0 ? perPathVolumeInstanceListMemPool.allocArray<int32_t>(renderer->m_volumeInstancesCount) : nullptr;
                    pathVolumeStates.emplace_back(VolumeInstList(volumeInstList, renderer->m_volumeInstancesCount));

                    p.accumulatorIndex = pixelIdx;
                    accumulators.emplace_back(
                        outputs.binding, bindingOutputs.data() + pixelIdx * outputs.binding.getAutomata().outputCount(), perPixelTransparency[pixelIdx], m_internalAutomata,
                        internalOutputs.data() + pixelIdx * m_internalAutomata.outputCount());

                    p.limits = inputs.bouncesLimits;
                }
            }

            vector_view<Ray> rays;
            vector_view<RayDifferentials> rayDifferentials;
            vector_view<uint32_t> rayTypes;
            std::tie(rays, rayDifferentials, rayTypes) = sampleCameraRays(paths, pixelsInfo.data(), memoryPool);

            for (size_t i = 0; i < count; ++i)
                paths[i].time = rays[i].time;

            const auto swappables = std::make_tuple(std::ref(rays), std::ref(rayDifferentials), std::ref(rayTypes), std::ref(paths), std::ref(pathVolumeStates));

            // Clear IC
            if (inputs.needLighting && renderer->m_directIllumAlgorithm == OslRendererImpl::DirectIllumAlgorithm::ImportanceCaching)
            {
                for (size_t i = 0; i < count; ++i)
                {
                    for (size_t j = 0; j < LightingImportanceCache::DistributionNames::Count; ++j)
                    {
                        getSampleCount(i, (uint8_t) j, lightImportanceCacheDataStorage, true) = 0;
                        getSampleCount(i, (uint8_t) j, lightImportanceCacheDataStorage, false) = 0;
                    }
                }
            }

            // compute volumes in which are the camera rays for volumetric rendering
            if (renderer->m_volumeInstancesCount > 0 && !inputs.skipVolumeInclusionTest)
            {
                HighResolutionTimerGuard timerGuard(&threadPerCounters[STAT_CAMERAVOLUMEINCLUSION]);

                // copy camera rays cause we will modify them in volume inclusion test
                auto volumeTestRays = make_view(memoryPool.volumeTestRays, count);
                std::copy(std::begin(rays), std::end(rays), std::begin(volumeTestRays));
                for (Ray & ray: volumeTestRays)
                    ray.tfar = inf;

                inputs.scene->initialVolumeInclusionTest(
                    &volumeTestRays[0], count, &(renderer->m_allVolumeInstanceIds[0]), renderer->m_volumeInstancesCount, renderer->m_volumeIdsToVectorIndex, pathVolumeStates.data(),
                    BOUNCES_LIMIT);
            }

            if (inputs.adaptiveSamplingEnabled)
            {
                const auto totalCount = count;

                auto isAdaptiveRejected = [&paths, &pixelsInfo, &adaptiveStats, this](auto i) {
                    const auto pixelIdx = paths[i].pixelIndex;
                    const auto x = pixelsInfo[pixelIdx].pixel.x;
                    const auto y = pixelsInfo[pixelIdx].pixel.y;
                    float s = outputs.binding.sampleCount(x, y);
                    if (adaptiveStats[pixelIdx].sampleCount() > inputs.adaptiveMinSamples && luminance(adaptiveStats[pixelIdx].beautyVariance()) < inputs.adaptiveVarianceThreshold)
                    {
                        paths[i].dropped = true;
                        return true;
                    }
                    return false;
                };

                {
                    HighResolutionTimerGuard g(&tilePerfCounters[STAT_REMOVE_ADAPTIVE]);
                    count = removeIf(count, isAdaptiveRejected, swappables);
                }

                tilePerfCounters[STAT_SAMPLES_DROPPED] += (totalCount - count);
            }
            tilePerfCounters[STAT_SAMPLES_COMPUTED] += count;

            for (size_t i = 0; i < count; ++i)
            {
                accumulators[paths[i].accumulatorIndex].beginSample();
                accumulators[paths[i].accumulatorIndex].move(LPE::Camera, LPE::None);
            }

            // Train lighting importance cache from previous samples
            if (sampleID > 0 && inputs.needLighting && renderer->m_directIllumAlgorithm == OslRendererImpl::DirectIllumAlgorithm::ImportanceCaching)
            {
                HighResolutionTimerGuard guard(&tilePerfCounters[STAT_IC_BUILDDISTRIBUTIONS]);
                lightImportanceCache.trainDistributions();
            }

            int32_t realBounceCount;
            for (realBounceCount = 0; count > 0 && realBounceCount < BOUNCES_LIMIT; ++realBounceCount)
            {
                // Update rays info related to bounce
                for (int32_t i = 0; i < count; ++i)
                {
                    if (paths[i].depth == 1)
                        rays[i].mask = 3 << (Scene::INSTANCE_VISIBILITY_SECONDARY * 2);
                }

                // Reset BxDFs and get samples
                for (int32_t i = 0; i < count; ++i)
                {
                    paths[i].uScatter = sampleBuffer.getScatterBuffer(sampleID, realBounceCount <= inputs.maxBounces ? realBounceCount : paths[i].depth) + paths[i].pixelIndex;
                    // decorrelate bsdf choice sample when we don't know how much bounces do we have (ignoreStraightBounces == 1)
                    // We generate a random sample if the ray is still straight
                    if (inputs.ignoreStraightBounces && realBounceCount > inputs.maxBounces && (rayTypes[i] & renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Straight)))
                    {
                        paths[i].uScatter->uClosureComponent = getFloat(samplerState);
                    }

                    paths[i].uVolumetric = sampleBuffer.getVolumetricBuffer(sampleID, realBounceCount <= inputs.maxBounces ? realBounceCount : paths[i].depth) + paths[i].pixelIndex;
                    // decorrelate volumetric sample when we don't know how much bounces do we have (ignoreStraightBounces == 1)
                    // We use a random sample after we used all the samples we had
                    if (inputs.ignoreStraightBounces && realBounceCount > inputs.maxBounces && (rayTypes[i] & renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Straight)))
                    {
                        paths[i].uVolumetric->uDistance = getFloat(samplerState);
                    }

                    paths[i].uBSSRDF = sampleBuffer.getBSSRDFBuffer(sampleID, paths[i].depth) + paths[i].pixelIndex;
                }

                {
                    HighResolutionTimerGuard g(&tilePerfCounters[STAT_INTERSECT]);

                    updateRayStatistics(count, paths.data(), tilePerfCounters);

                    {
                        HighResolutionTimerGuard g(&tilePerfCounters[STAT_INTERSECT_INTERS]);
                        inputs.scene->intersect(rays.data(), count);
                    }

                    evalBackgroundEnvmap(rays.data(), paths.data(), accumulators.data(), shadingCtx, count);

                    // Accum volume direct extinction and transparency on rays leaving the scene
                    for (size_t i = 0; i < count; ++i)
                    {
                        if (isDirect(paths[i]) && rays[i].instID == Ray::INVALID_ID)
                        {
                            auto & accumulator = accumulators[paths[i].accumulatorIndex];
                            accumulator.push().move(LPE::Volume, LPE::Extinction).accum(paths[i].volumeTransmittance);
                            accumulator.transparency += paths[i].transparencyWeight;
                        }
                    }

                    {
                        const auto oldCount = count;
                        auto removeMissed = [&](auto i) { return rays[i].instID == Ray::INVALID_ID; };
                        count = terminatePathsIf(count, paths.data(), finishedPaths, removeMissed, swappables, &tilePerfCounters[STAT_INTERSECT_MISSED]);
                        tilePerfCounters[STAT_BG_HITS] += (oldCount - count);
                    }

                    if (count > 0)
                    {
                        const auto oldCount = count;
                        auto removeAutoIntersect = [&](auto i) { return paths[i].instID == rays[i].instID && paths[i].primID == rays[i].primID; };
                        count = terminatePathsIf(count, paths.data(), finishedPaths, removeAutoIntersect, swappables, &tilePerfCounters[STAT_INTERSECT_MISSED]);
                        tilePerfCounters[STAT_AUTOINTERSECTIONS_SCATTERING] += (oldCount - count);
                    }
                }

                // early exit
                if (!count)
                    break;

                for (size_t i = 0; i < count; ++i)
                {
                    paths[i].instID = rays[i].instID;
                    paths[i].primID = rays[i].primID;
                }

                // Volume processing
                if (renderer->hasVolumetrics())
                {
                    const auto volumeIds = computeVolumeList(paths.data(), pathVolumeStates.data(), count, tilePerfCounters, memoryPool);

                    if (volumeIds.volumeCount() > 0)
                    {
                        auto volumetricSamplingResult = volumetricDensitySampling(
                            count, paths.data(), pathVolumeStates.data(), RayBatch{ rays.data(), rayDifferentials.data(), rayTypes.data(), count }, volumeIds, shadingCtx, tilePerfCounters,
                            memoryPool);

                        // Lighting is done before changing path weights, otherwise we would have a bad transmission factor applied
                        if (inputs.needLighting)
                        {
                            HighResolutionTimerGuard timerGuard(&tilePerfCounters[STAT_VOLUMETRIC]);

                            volumetricLightingIntegration(
                                count, paths.data(), pathVolumeStates.data(), RayBatch{ rays.data(), rayDifferentials.data(), rayTypes.data(), count }, volumeIds, volumetricSamplingResult,
                                accumulators.data(), shadingCtx, tilePerfCounters, shadowPerfInstances, lightStatistics.data(), memoryPool);
                        }

                        // Update paths weight according with estimated transmittance
                        {
                            HighResolutionTimerGuard timerGuard(&tilePerfCounters[STAT_VOLUMETRIC_APPLY]);

                            // #todo it should not be required to store values in vectors, we should be able to directly acumulate to LPE accumulators
                            vector_view<Color3> pathTransmittance = make_view(memoryPool.pathTransmittance, count, Color3(1.f));
                            vector_view<Color3> pathTransparencyAccum = make_view(memoryPool.pathTransparencyAccum, count, Color3(0.f));

                            const auto nbVolumesInTile = volumeIds.volumeCount();
                            for (size_t v = 0, offset = 0; v < nbVolumesInTile; ++v)
                            {
                                const auto pathIndexList = volumeIds.pathIndexList(v);
                                const auto nbPathsInVolume = volumeIds.numberOfPathsInVolume(v);

                                for (size_t i = 0; i < nbPathsInVolume; ++i)
                                {
                                    const auto pathIdx = pathIndexList[i];

                                    const auto transmittanceForThisVolume = volumetricSamplingResult.transmittanceToSurface[offset + i];

                                    pathTransmittance[pathIdx] *= transmittanceForThisVolume;

                                    const auto isBlackhole = inputs.scene->isBlackholeInstance(
                                        volumeIds.volumeInfoList[v].volumeInstanceId, isDirect(paths[pathIdx]) ? Scene::INSTANCE_VISIBILITY_PRIMARY : Scene::INSTANCE_VISIBILITY_SECONDARY);

                                    if (isBlackhole)
                                        pathTransparencyAccum[pathIdx] +=
                                            paths[pathIdx].transparencyWeight * (Color3(1.f) - transmittanceForThisVolume); // It's like blackhole particles are adding transparency to the path
                                }

                                offset += nbPathsInVolume;
                            }

                            for (size_t pathIdx = 0; pathIdx < count; ++pathIdx)
                            {
                                paths[pathIdx].weight *= pathTransmittance[pathIdx];
                                paths[pathIdx].transparencyWeight *= pathTransmittance[pathIdx];
                                paths[pathIdx].volumeTransmittance *= pathTransmittance[pathIdx];

                                if (isDirect(paths[pathIdx]))
                                {
                                    accumulators[paths[pathIdx].accumulatorIndex].transparency += pathTransparencyAccum[pathIdx];
                                }
                            }
                        }
                    }
                }

                // Surface processing
                {
                    // Here we call removeIf to partition paths (and related data) according to those that didn't hit blackhole surfaces, and those that did.
                    // We do that because we don't want to compute surface lighting on blackhole objects
                    // We do that before calling evalMaterial so that we don't need to swap the shading array afterward
                    auto isBlackhole = [&](auto i) {
                        assert(rays[i].instID != Ray::INVALID_ID);
                        return inputs.scene->isBlackholeInstance(rays[i].instID, isDirect(paths[i]) ? Scene::INSTANCE_VISIBILITY_PRIMARY : Scene::INSTANCE_VISIBILITY_SECONDARY);
                    };
                    const auto notBlackholeCount = removeIf(count, isBlackhole, swappables);

                    // We need shading even for non blackhole object in order to be able to sample transparency
                    // #todo only evaluate opacity shading tree for blackhole objects
                    auto globalsFromRayHitsResult = evalGlobalsFromRayHits(count, paths.data(), rays.data(), rayDifferentials.data(), rayTypes.data(), memoryPool);
                    const auto * globals = globalsFromRayHitsResult.globals.data();
                    const auto * normalOffsets = globalsFromRayHitsResult.normalOffsets.data();
                    const auto * rayNears = globalsFromRayHitsResult.rayNears.data();
                    const auto * instIds = globalsFromRayHitsResult.instIds.data();

                    const auto evalSurfaceMaterialResult =
                        evalSurfaceMaterial(count, paths.data(), globalsFromRayHitsResult.globals.data(), instIds, tilePerfCounters, perfInstances, shadingCtx, memoryPool);
                    const auto * surfaceShadingFunctions = evalSurfaceMaterialResult.surfaceShadingFunctions.data();
                    const auto * surfaceShadingSamplers = evalSurfaceMaterialResult.surfaceShadingSamplers.data();
                    const auto * shadingNormals = evalSurfaceMaterialResult.shadingNormals.data();

                    // Accum transparency on rays hitting blackhole objects (emission of opacity as transparency)
                    for (size_t i = notBlackholeCount; i < count; ++i)
                        accumulators[paths[i].accumulatorIndex].transparency += paths[i].transparencyWeight * surfaceShadingFunctions[i].opacity();

                    vector_view<OSL::ShaderGlobals> ssGlobals = make_view(memoryPool.ssGlobals, count); // #todo temporary, we need that to access ssglobals in the same order as paths
                    vector_view<Color3> ssWeights = make_view(memoryPool.ssWeights, count);

                    {
                        const auto count = notBlackholeCount; // All processing in this block is done only on non blackhole hits

                        accumGeometryAndMaterialOutputs(count, paths.data(), globals, surfaceShadingFunctions, shadingNormals, instIds, accumulators.data(), inputs.startWorldToCamera);

                        const auto bssrdfPointSamplingResult = bssrdfPointSampling(
                            count, globals, surfaceShadingFunctions, surfaceShadingSamplers, RayBatch{ rays.data(), rayDifferentials.data(), rayTypes.data(), count }, paths.data(),
                            tilePerfCounters, memoryPool);

                        for (size_t i = 0; i < bssrdfPointSamplingResult.pathIndexList.size(); ++i)
                        {
                            const auto pathIdx = bssrdfPointSamplingResult.pathIndexList[i];
                            ssGlobals[pathIdx] = bssrdfPointSamplingResult.globals[i];
                            ssWeights[pathIdx] = bssrdfPointSamplingResult.weights[i];
                        }

                        if (outputs.binding.needOcclusion())
                        {
                            occlusionIntegration(
                                tilePerfCounters, count, RayBatch{ rays.data(), rayDifferentials.data(), rayTypes.data(), count }, paths.data(), globals, normalOffsets, rayNears,
                                shadingNormals, inputs.rayTracedIndirectBias, pathVolumeStates.data(), shadingCtx, shadowPerfInstances, accumulators.data(), memoryPool);
                        }

                        if (outputs.binding.needDepth())
                        {
                            depthIntegration(count, paths.data(), rays.data(), pixelsInfo.data(), accumulators.data());
                        }

                        if (outputs.binding.needMotionVectors() && (inputs.closeTime - inputs.openTime) > 0.f)
                        {
                            motionVectorsIntegration(
                                count, RayBatch{ rays.data(), rayDifferentials.data(), rayTypes.data(), count }, inputs.shutterOffset, globals, inputs.startWorldToCamera,
                                inputs.endWorldToCamera, *inputs.camera, inputs.camToScreen, accumulators.data(), paths.data());
                        }

                        if (outputs.binding.needCoverage() && realBounceCount == 0)
                        {
                            HighResolutionTimerGuard g(&tilePerfCounters[STAT_COVERAGE_ADD]);
                            for (int32_t i = 0; i < count; ++i)
                                ++coverageCounters[paths[i].pixelIndex][instIds[i]];
                        }

                        if (inputs.needLighting)
                        {
                            if (renderer->m_directIllumAlgorithm == OslRendererImpl::DirectIllumAlgorithm::ImportanceCaching)
                            {
                                lightImportanceCacheDataStorage.setTileSampleCount(count); // #todo Why am i doing this here ? should be done in lighting...
                            }

                            surfaceLightingIntegration(
                                count, paths.data(), pathVolumeStates.data(), globalsFromRayHitsResult, evalSurfaceMaterialResult, bssrdfPointSamplingResult, accumulators.data(),
                                lightImportanceCache, lightImportanceCacheDataStorage, shadingCtx, tilePerfCounters, shadowPerfInstances, lightStatistics.data(), memoryPool);
                        }

                        // Toon outline
                        {
                            HighResolutionTimerGuard guard(&tilePerfCounters[STAT_TOONOUTLINE]);
                            toonOutlineIntegration(
                                count, paths.data(), sampleID, inputs.toonOutlineMinSampleCount, pixelNeedToonOutlineComputation.data(), pixelsInfo.data(), toonOutlines.data(), instIds,
                                globals, shadingCtx, accumulators.data(), RayBatch{ rays.data(), rayDifferentials.data(), rayTypes.data(), count }, samplerState,
                                tilePixelIdxToSampleIdx.data());
                        }
                    } // End of non blockhole hits processing

                    const vector_view<PathDirection> directionSamples =
                        sampleNextPathDirections(count, paths.data(), globals, surfaceShadingFunctions, ssGlobals.data(), tilePerfCounters, bouncingMemoryPool, memoryPool);

                    // #todo cleanup path splitting:

                    auto splittedPaths = make_view(memoryPool.splittedPaths);
                    auto splittedRays = make_view(memoryPool.splittedRays);
                    auto splittedRaysDifferentials = make_view(memoryPool.splittedRaysDifferentials);
                    auto splittedRayTypes = make_view(memoryPool.splittedRayTypes);
                    auto splittedPathVolumeStates = make_view(memoryPool.splittedPathVolumeStates);

                    vector_view<size_t> splitCount = make_view(memoryPool.pathSplitCount, count, size_t(0));
                    for (const auto & pathDirection: directionSamples)
                    {
                        const auto pathIdx = pathDirection.pathIdx;

                        auto & newPath = splittedPaths.emplace_back(paths[pathIdx]);

                        auto & newRay = splittedRays.emplace_back(rays[pathIdx]);
                        auto & newRayDiffs = splittedRaysDifferentials.emplace_back(rayDifferentials[pathIdx]);
                        auto & newRayType = splittedRayTypes.emplace_back(rayTypes[pathIdx]);
                        auto & newPathVolumeState = splittedPathVolumeStates.emplace_back(pathVolumeStates[pathIdx]);

                        if (splitCount[pathIdx] > 0)
                        {
                            auto newAccum = accumulators[paths[pathIdx].accumulatorIndex];
                            accumulators.emplace_back(newAccum);
                            newPath.accumulatorIndex = accumulators.size() - 1;

                            newPathVolumeState.volumeInstBuffer =
                                renderer->m_volumeInstancesCount > 0 ? perPathVolumeInstanceListMemPool.allocArray<int32_t>(2 * renderer->m_volumeInstancesCount) : nullptr;
                            std::copy(
                                pathVolumeStates[pathIdx].volumeInstBuffer, pathVolumeStates[pathIdx].volumeInstBuffer + renderer->m_volumeInstancesCount, newPathVolumeState.volumeInstBuffer);
                        }

                        extendPath(
                            paths[pathIdx], rays[pathIdx], rayDifferentials[pathIdx], rayTypes[pathIdx], pathDirection.directionSample, ssWeights[pathIdx], surfaceShadingFunctions[pathIdx],
                            globals[pathIdx], ssGlobals[pathIdx], normalOffsets[pathIdx], rayNears[pathIdx], newPath, newRay, newRayDiffs, newRayType);
                        ++splitCount[pathIdx];
                    }

                    std::swap(paths.container(), splittedPaths.container());
                    std::swap(rays.container(), splittedRays.container());
                    std::swap(rayDifferentials.container(), splittedRaysDifferentials.container());
                    std::swap(rayTypes.container(), splittedRayTypes.container());
                    std::swap(pathVolumeStates.container(), splittedPathVolumeStates.container());

                    count = paths.size();
#ifdef SHN_ENABLE_DEBUG
                    // #todo change this, shading is not usable at that point because paths may have been splitted
                    // for (int32_t i = 0; i < count; ++i)
                    //    printSampleDebugInfo(pixelsInfo[path[i].pixelIndex], path[i], scatteringRays.rays()[i], scatteringRays.types()[i], shading[i], sampleID);
#endif
                }

                for (auto i = 0; i < count; ++i)
                {
                    // Accumulate path direct volume transmittance as soon as we stop having straight bounces
                    if (!straightOnly(paths[i].samplingState))
                    {
                        auto & accumulator = accumulators[paths[i].accumulatorIndex];
                        accumulator.push().move(LPE::Volume, LPE::Extinction).accum(paths[i].volumeTransmittance);
                        paths[i].volumeTransmittance = Color3(0.f);
                    }
                }

                for (size_t i = 0; i < count; ++i)
                {
                    // The path will be stopped because of max bounce reaching. We assume everything behind is empty to we need to accumulate transparency of the path.
                    if (paths[i].depth > inputs.maxBounces)
                    {
                        auto & accumulator = accumulators[paths[i].accumulatorIndex];
                        accumulator.transparency += paths[i].transparencyWeight;
                    }
                }

                // early exit
                if (!inputs.needBouncing)
                    break;

                const auto stopBouncingPredicate = [&](auto i) {
                    return paths[i].samplingState == PathSamplingState::Absorption || paths[i].samplingState == PathSamplingState::Blackhole || paths[i].depth > inputs.maxBounces;
                };

                count = terminatePathsIf(count, paths.data(), finishedPaths, stopBouncingPredicate, swappables, nullptr);

                if (!count)
                    break;

                // Update accumulators
                for (size_t i = 0; i < count; ++i)
                {
                    auto & accumulator = accumulators[paths[i].accumulatorIndex];

                    if (paths[i].closureCategory == ClosurePrimitive::BSSRDF)
                    {
                        accumulator.move(LPE::SubSurface, LPE::None, nullptr);
                    }
                    else if (paths[i].scatteringEvent != ScatteringEvent::Absorb)
                    {
                        const auto scattLabels = LPE::bsdfEventToLPELabels[scatteringId(paths[i].scatteringEvent, paths[i].scatteringType)];
                        accumulator.move(scattLabels.event, scattLabels.type, nullptr);
                    }
                }

                const auto isAccumulatorBroken = [&](auto i) { return accumulators[paths[i].accumulatorIndex].broken(); };

                count = terminatePathsIf(count, paths.data(), finishedPaths, isAccumulatorBroken, swappables, nullptr);

                for (size_t i = 0; i < count; ++i)
                {
                    const auto & pixel = pixelsInfo[paths[i].pixelIndex].pixel;
                    outputs.binding.addBounces(pixel.x, pixel.y, 1.f);
                }

                tilePerfCounters[STAT_BOUNCES_AVG] += count; // "average" is computed later

                if (renderer->m_volumeInstancesCount)
                {
                    inOutVolumeTest(count, rays.data(), pathVolumeStates.data());
                }

                for (size_t i = 0; i < count; ++i)
                {
                    // #todo maybe this should be done at trace time
                    rays[i].instID = Ray::INVALID_ID;
                    rays[i].geomID = Ray::INVALID_ID;
                    rays[i].primID = Ray::INVALID_ID;
                }
            } // end of bounce

            for (size_t i = 0; i < count; ++i)
            {
                finishedPaths.emplace_back(paths[i]);
            }

            vector_view<uint8_t> perPixelHasSample = make_view(memoryPool.perPixelHasSample, pixelCount, uint8_t(0));
            for (const Path & path: finishedPaths)
            {
                // Ignore dropped samples due to adaptive sampling
                if (path.dropped)
                    continue;

                const auto pixelIdx = path.pixelIndex;
                perPixelHasSample[pixelIdx] = true;
            }

            updateBindingFramebuffers(
                pixelCount, sampleID, realBounceCount, pixelsInfo.data(), perPixelHasSample.data(), perPixelTransparency.data(), toonOutlines.data(), tilePerfCounters, lightImportanceCache,
                lightImportanceCacheDataStorage, bindingOutputs.data());

            for (size_t i = 0; i < pixelCount; ++i)
            {
                if (perPixelHasSample[i])
                {
                    adaptiveStats[i].addSample(internalOutputs[i]);
                }
            }

            tileMaxBounceCount = std::max(tileMaxBounceCount, (uint64_t) realBounceCount);
            if (renderer->m_progressTileFunc)
                renderer->m_progressTileFunc((int32_t) tile, renderer->m_progressAppData);

            ++outputs.finishedSampleCountPerTile[tile];

        } // end of sample loop

        if (!renderer->m_lightImportanceCachingStatsTileList.empty())
        {
            if (end(renderer->m_lightImportanceCachingStatsTileList) != renderer->m_lightImportanceCachingStatsTileList.find(V2i((int32_t) tileX, (int32_t) tileY)))
            {
                auto stats = lightImportanceCache.serialize();

                stats["tileCoords"] = { tileX, tileY };
                stats["tileBegin"] = { tileBegin.x, tileBegin.y };
                stats["tileSize"] = { TILE_SIZE, TILE_SIZE };

                lightImportanceCacheStatistics.emplace_back(std::move(stats));
            }
        }

        if (outputs.binding.needCoverage())
        {
            for (size_t pixelIdx = 0; pixelIdx < pixelsInfo.size(); ++pixelIdx)
            {
                const auto & pixel = pixelsInfo[pixelIdx].pixel;
                const auto & counters = coverageCounters[pixelIdx];
                size_t maxHit = 0;
                for (auto hitCount: counters)
                    maxHit = std::max(hitCount.second, maxHit);

                const auto sampleCount = outputs.binding.sampleCount(pixel.x, pixel.y);
                outputs.binding.setCoverage(pixel.x, pixel.y, maxHit > 0 ? maxHit : sampleCount);
            }
        }

        if (outputs.binding.needMeanVariance())
        {
            for (size_t i = 0; i < pixelsInfo.size(); ++i)
            {
                outputs.binding.setMeanVariance(pixelsInfo[i].pixel.x, pixelsInfo[i].pixel.y, luminance(adaptiveStats[i].beautyMean()), luminance(adaptiveStats[i].beautyVariance()));
            }
        }

        tilePerfGuard.release(); // end of STAT_RENDERFRAME measurement

#ifndef DISABLE_PERFORMANCE_COUNTERS
        for (int i = 0; i != STAT_MAX; ++i)
        {
            threadPerCounters[i] += tilePerfCounters[i];
        }
#endif
    } // end of tile

#ifndef DISABLE_PERFORMANCE_COUNTERS
    for (int i = 0; i != STAT_MAX; ++i)
    {
        if (i == STAT_MAXBOUNCES)
        {
            std::unique_lock<std::mutex> lock(m_performanceInstancesMutex);
            uint64_t otherTileMaxBounceCount = outputs.performanceCounters[i];
            outputs.performanceCounters[i] = std::max(otherTileMaxBounceCount, tileMaxBounceCount);
        }
        else
        {
            outputs.performanceCounters[i] += threadPerCounters[i];
        }
    }

    {
        std::unique_lock<std::mutex> lock(m_performanceInstancesMutex);

        for (size_t i = 0; i < outputs.lightStatistics.size(); ++i)
            outputs.lightStatistics[i] += lightStatistics[i];

        for (const auto & tileICStats: lightImportanceCacheStatistics)
            outputs.lightImportanceCacheStatistics.emplace_back(tileICStats);
    }

    if (renderer->isPerformanceCounterEnabled(OslRendererImpl::PerformanceCounter_Instance))
    {
        std::unique_lock<std::mutex> lock(m_performanceInstancesMutex);
        for (const auto & item: perfInstances)
        {
            auto & dst = outputs.performanceInstances[item.first].first;
            dst.shadingDurationUS += item.second.shadingDurationUS;
            dst.shadingCount += item.second.shadingCount;
            assert(item.second.shadowRayCount == 0);
        }
        for (const auto & item: shadowPerfInstances)
        {
            auto & dst = outputs.performanceInstances[item.first].second;
            dst.shadingDurationUS += item.second.shadingDurationUS;
            dst.shadingCount += item.second.shadingCount;
            dst.shadowRayCount += item.second.shadowRayCount;
        }
    }
#endif

    return 0;
}

auto OslRendererTask::initializePixelsInfo(const V2i tileBegin, V2i const tileEnd, int32_t * tilePixelIdxToPixelInfoIdx, ThreadMemoryPool & memoryPool) const -> vector_view<PixelInfo>
{
    auto pixelInfo = make_view(memoryPool.pixelInfo);

    for (size_t pixelIdx = 0; pixelIdx < TILE_PIXEL_COUNT; ++pixelIdx)
    {
        const auto i = pixelIdx % TILE_SIZE;
        const auto j = pixelIdx / TILE_SIZE;
        const auto pixel = V2<size_t>(tileBegin.x + i, tileBegin.y + j);
        if (pixel.x > tileEnd.x)
            continue;
        if (pixel.y > tileEnd.y)
            break;
        if (outputs.maskedPixel(pixel.x - inputs.startx, pixel.y - inputs.starty))
            continue;

#ifdef SHN_ONLY_DEBUG_PIXEL
        if (!isDebugPixel(pixel))
            continue;
#endif

        if (tilePixelIdxToPixelInfoIdx)
            tilePixelIdxToPixelInfoIdx[pixelIdx] = pixelInfo.size();

        pixelInfo.emplace_back(pixel, V2<size_t>(i, j));
    }
    return pixelInfo;
}

std::tuple<vector_view<Ray>, vector_view<RayDifferentials>, vector_view<uint32_t>>
OslRendererTask::sampleCameraRays(const vector_view<Path> & paths, const PixelInfo * pixelsInfo, ThreadMemoryPool & memPool) const
{
    auto scatteringRays = make_view(memPool.scatteringRays);
    auto scatteringRayDifferentials = make_view(memPool.scatteringRaysDifferentials);
    auto scatteringRayTypes = make_view(memPool.scatteringRayTypes);

    for (const auto & p: paths)
    {
        const auto pixel = pixelsInfo[p.pixelIndex].pixel;

        const V2f lensSample = toV2f(p.uCamera->uLens);
        const float timeSample = (outputs.binding.needMotionVectors()) ? inputs.shutterOffset : p.uCamera->uTime;
        const V2f pixelSample = toV2f(p.uCamera->uPixel);
        const V2f filteredSample = renderer->m_rayFilter.sample(pixelSample.x, pixelSample.y) + V2f(0.5f);
        const V2f rasterPosition = V2f(pixel) + filteredSample;
        const OSL::Dual2<V2f> imageSample(rasterPosition * inputs.rcpWidthHeight, V2f(-1.f, 0.f) * inputs.rcpWidthHeight, V2f(0.f, 1.f) * inputs.rcpWidthHeight);
        std::pair<Ray, RayDifferentials> r = inputs.scene->sampleCameraRay(*inputs.camera, imageSample, lensSample, timeSample);
        r.first.mask = 3 << (Scene::INSTANCE_VISIBILITY_PRIMARY * 2);

        scatteringRays.emplace_back(r.first);
        scatteringRayDifferentials.emplace_back(r.second);
        scatteringRayTypes.emplace_back(renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Camera));
    }

    return std::make_tuple(std::move(scatteringRays), std::move(scatteringRayDifferentials), std::move(scatteringRayTypes));
}

void OslRendererTask::depthIntegration(size_t count, const Path * paths, const Ray * rays, const PixelInfo * pixelsInfo, Accumulator * accumulators) const
{
    for (size_t i = 0; i < count; ++i)
    {
        const auto & pixel = pixelsInfo[paths[i].pixelIndex].pixel;

        const auto coefWtoP = inputs.camera->getCoefWorldToProjDistance(V2f((float) pixel.x, (float) pixel.y) * inputs.rcpWidthHeight);
        const auto realDepth = (rays[i].instID == Ray::INVALID_ID) ? inputs.camera->farPlane() : rays[i].tfar * coefWtoP;

        auto & accumulator = accumulators[paths[i].accumulatorIndex];

        accumulator.push().move(LPE::Geometry, LPE::Depth).accum([&]() {
            if (inputs.depthNear != inputs.depthFar)
                return Color3(fabsf((realDepth - inputs.depthNear) / (inputs.depthFar - inputs.depthNear))); // accum each bounce depth
            else
                return Color3(realDepth);
        });
    }
}

void OslRendererTask::toonOutlineIntegration(
    size_t count, const Path * path, int sampleID, size_t toonOutlineMinSampleCount, bool * pixelNeedToonOutlineComputation, const PixelInfo * pixelsInfo, ToonOutlineContribs * toonOutlines,
    const Scene::Id * instIds, const OSL::ShaderGlobals * globals, const ShadingContext & ctx, Accumulator * accumulators, const RayBatch & scatteringRays, RandomState & samplerState,
    const int32_t * tilePixelIdxToSampleIdx) const
{
    for (size_t i = 0; i < count; ++i)
    {
        if (!isDirect(path[i]) || (sampleID >= toonOutlineMinSampleCount && !pixelNeedToonOutlineComputation[path[i].pixelIndex]))
            continue;

        auto & toonOutline = toonOutlines[path[i].pixelIndex];

        auto globalsCopy = globals[i];
        if (executeToonOutlineShader(toonOutline, instIds[i], globalsCopy, ctx))
        {
            auto & accumulator = accumulators[path[i].accumulatorIndex];

            accumulator.push().move(LPE::ToonOutline, LPE::OuterColor).accum(toonOutline.outerColor);
            accumulator.push().move(LPE::ToonOutline, LPE::InnerColor).accum(toonOutline.innerColor);
            accumulator.push().move(LPE::ToonOutline, LPE::CornerColor).accum(toonOutline.cornerColor);

            computeToonOutline(toonOutline, globalsCopy, scatteringRays.rays()[i], samplerState);

            accumulator.push().move(LPE::ToonOutline, LPE::Weights).accum(toonOutline.weights);
        }
    }

    // If we have computed toon outline for some pixel, set flag to 4-neighbors in order to try to compute it for them too
    // #todo we should iterate directly on toonOutlines instead of path
    for (size_t i = 0; i < count; ++i)
    {
        if (toonOutlines[path[i].pixelIndex].hasOutline())
        {
            pixelNeedToonOutlineComputation[path[i].pixelIndex] = true;

            const auto localX = pixelsInfo[path[i].pixelIndex].tilePixel.x;
            const auto localY = pixelsInfo[path[i].pixelIndex].tilePixel.y;

            {
                const auto neighborIdx = localX + (localY - 1) * TILE_SIZE;
                if (localY > 0 && tilePixelIdxToSampleIdx[neighborIdx] > 0)
                    pixelNeedToonOutlineComputation[neighborIdx] = true;
            }

            {
                const auto neighborIdx = localX + (localY + 1) * TILE_SIZE;
                if (localY < TILE_SIZE - 1 && tilePixelIdxToSampleIdx[neighborIdx] > 0)
                    pixelNeedToonOutlineComputation[neighborIdx] = true;
            }

            {
                const auto neighborIdx = localX - 1 + localY * TILE_SIZE;
                if (localX > 0 && tilePixelIdxToSampleIdx[neighborIdx] > 0)
                    pixelNeedToonOutlineComputation[neighborIdx] = true;
            }

            {
                const auto neighborIdx = localX + 1 + localY * TILE_SIZE;
                if (localX < TILE_SIZE - 1 && tilePixelIdxToSampleIdx[neighborIdx] > 0)
                    pixelNeedToonOutlineComputation[neighborIdx] = true;
            }
        }
    }
}

void OslRendererTask::occlusionIntegration(
    uint64_t * tilePerfCounters, size_t count, const RayBatch & scatteringRays, const Path * paths, const OSL::ShaderGlobals * globals, const float * normalOffsets, const float * rayNears,
    const V3f * shadingNormals, float rayTracedIndirectBias, VolumeInstList * pathVolumeStates, const ShadingContext & shadingCtx, InstancePerformances & shadowPerfInstances,
    Accumulator * accumulators, ThreadMemoryPool & memPool) const
{
    const auto shadowResults = [&]() {
        auto shadowRays = make_view(memPool.shadowRays, count);

        {
            HighResolutionTimerGuard g(&tilePerfCounters[STAT_OCCLUSION_SAMPLE]);
            for (size_t i = 0; i < count; ++i)
            {
                assert(scatteringRays.rays()[i].instID != Ray::INVALID_ID); // Missed rays should have been removed already

                shadowRays[i].org = globals[i].P + globals[i].Ng * (normalOffsets[i] + rayTracedIndirectBias);
                const V2f s = toV2f(paths[i].uLight->uDir);
                float occlusionSamplePdf = 0.f;
                shadowRays[i].dir = sampleUniformSphericalCone(shadingNormals[i], inputs.occlusionAngleRadians, s.x, s.y, occlusionSamplePdf);
                shadowRays[i].tnear = rayNears[i];
                shadowRays[i].tfar = inputs.occlusionDistance;
                shadowRays[i].mask = 3 << (Scene::INSTANCE_VISIBILITY_SECONDARY * 2); // occlusion on all secondary meshes (not only castsShadows meshes)
                shadowRays[i].time = scatteringRays.rays()[i].time;
                shadowRays[i].instID = shadowRays[i].primID = Ray::INVALID_ID;
            }
        }

        {
            HighResolutionTimerGuard g(&tilePerfCounters[STAT_OCCLUSION_SHADOW]);
            IntersectSolidShadowStats stats; // Dummy
            LightStats lightStats; // Dummy

            const auto shadowRayCount = count;

            auto shadowVolumeInstBuffer = make_view(memPool.shadowVolumeInstBuffer, shadowRayCount * renderer->m_volumeInstancesCount);
            auto shadowVolumeStates = make_view(memPool.shadowVolumeStates, shadowRayCount);
            auto shadowVolumeTransmissionRandomNumbers = make_view(memPool.shadowVolumeTransmissionRandomNumbers, shadowRayCount);

            for (size_t j = 0; j < shadowRayCount; ++j)
            {
                shadowVolumeStates[j] = VolumeInstList(shadowVolumeInstBuffer.data() + j * renderer->m_volumeInstancesCount, pathVolumeStates[j]);
                shadowVolumeTransmissionRandomNumbers[j] = paths[j].uVolumetric->uDistance;
            }

            return evalShadowFactors(
                std::move(shadowRays), std::move(shadowVolumeStates), std::move(shadowVolumeTransmissionRandomNumbers), shadingCtx, stats, shadowPerfInstances, lightStats, memPool);
        }
    }();

    {
        HighResolutionTimerGuard g(&tilePerfCounters[STAT_OCCLUSION_ADD]);
        for (size_t i = 0; i < count; ++i)
            accumulators[paths[i].accumulatorIndex].push().move(LPE::Geometry, LPE::Occlusion).accum(shadowResults[i].transparency);
    }
}

static void addEmissionContrib(OslRendererTask::Accumulator & accumulator, Color3 contribColor, ustring event)
{
    accumulator.push().move(event, LPE::Emission).accum(contribColor);
}

void OslRendererTask::accumGeometryAndMaterialOutputs(
    size_t count, const Path * paths, const OSL::ShaderGlobals * globals, const SurfaceShadingFunction * surfaceShadingFunctions, const V3f * shadingNormals, const Scene::Id * instIds,
    Accumulator * accumulators, const M44f & startWorldToCamera) const
{
    for (int i = 0; i != count; ++i)
    {
        auto & accumulator = accumulators[paths[i].accumulatorIndex];

        accumulator.push().move(LPE::Material, LPE::DiffuseColor).accum([&] {
            return paths[i].weight * (surfaceShadingFunctions[i].emission() + surfaceShadingFunctions[i].color(ScatteringTypeBits::Diffuse, ScatteringEventBits::Both));
        });

        accumulator.push().move(LPE::Material, LPE::GlossyColor).accum([&] {
            return paths[i].weight * surfaceShadingFunctions[i].color(ScatteringTypeBits::Glossy, ScatteringEventBits::Both);
        });

        accumulator.push().move(LPE::Material, LPE::BaseColor).accum([&] { return paths[i].weight * surfaceShadingFunctions[i].baseColor(); });

        accumulator.push().move(LPE::Material, LPE::ScatterDistanceColor).accum([&] { return paths[i].weight * surfaceShadingFunctions[i].scatterDistanceColor(); });

        accumulator.push().move(LPE::Geometry, LPE::NormalWorld).accum(shadingNormals[i]);

        accumulator.push().move(LPE::Geometry, LPE::NormalCamera).accum([&] { return normalize(multDirMatrix(startWorldToCamera, shadingNormals[i])); });

        accumulator.push().move(LPE::Geometry, LPE::TriangleNormalWorld).accum(globals[i].Ng);

        accumulator.push().move(LPE::Geometry, LPE::TriangleNormalCamera).accum([&] { return normalize(multDirMatrix(startWorldToCamera, globals[i].Ng)); });

        accumulator.push().move(LPE::Geometry, LPE::TriangleColorId).accum(int2color(paths[i].primID));
        accumulator.push().move(LPE::Geometry, LPE::InstanceColorId).accum(int2color(paths[i].instID));

        accumulator.push().move(LPE::Geometry, LPE::PositionWorld).accum(globals[i].P);

        accumulator.push().move(LPE::Geometry, LPE::PositionCamera).accum([&] {
            V3f positionCam = multVecMatrix(startWorldToCamera, globals[i].P);
            positionCam.z *= -1.f;
            return positionCam;
        });

        accumulator.push().move(LPE::Geometry, LPE::UV).accum(Color3(globals[i].u, globals[i].v, 0.f));

        const auto shaderId = renderer->getShadingTreeIdFromInstanceId(instIds[i], OslRendererImpl::Assignment::Surface);

        const GeometryLight * l = inputs.lights->getLinkedGeometryLight(instIds[i]);
        if (l)
            addEmissionContrib(accumulators[paths[i].accumulatorIndex], paths[i].weight * surfaceShadingFunctions[i].emission(), LPE::Light);
        else
            addEmissionContrib(accumulators[paths[i].accumulatorIndex], paths[i].weight * surfaceShadingFunctions[i].emission(), LPE::Material);
    }
}

void OslRendererTask::updateBindingFramebuffers(
    size_t pixelCount, size_t sampleID, size_t bounceLoopIndex, const PixelInfo * pixelInfo, const uint8_t * perPixelHasSample, const Color3 * perPixelTransparency,
    const ToonOutlineContribs * perPixelToonOutlines, uint64_t * tilePerfCounters, const LightingImportanceCache & lightingIC, const LightingImportanceCacheDataStorage & icData,
    const Color3 * bindingOutputs) const
{
    HighResolutionTimerGuard g(&tilePerfCounters[STAT_BUFFER_ADD]);

    for (auto pixelIdx = 0u; pixelIdx < pixelCount; ++pixelIdx)
    {
        // Ignore dropped samples due to adaptive sampling
        if (!perPixelHasSample[pixelIdx])
            continue;

        const auto x = pixelInfo[pixelIdx].pixel.x;
        const auto y = pixelInfo[pixelIdx].pixel.y;

        // if (!testAndLogInvalidOutput(path[i].weight, x, y, startSample + sampleID, "path weight"))
        //    continue;

        // const auto alpha = shn::clamp(1.f - perPixelTransparency[pixelIdx], 0.f, 1.f);
        const auto alpha = 1.f - reduceAdd(perPixelTransparency[pixelIdx]) / 3.f;

        outputs.binding.addSamples(x, y, 1.f);
        outputs.binding.addAlpha(x, y, alpha);

        // const auto & toonOutline = perPixelToonOutlines[pixelIdx];
        // if (toonOutline.hasOutline())
        //{
        //    data.hasToonOutline = true;
        //    data.outlineColor = toonOutline.outlineColor() * (toonOutline.applyLighting ? F1_PI * Color3(accumulator.irradianceDirect) : Color3(1.f));
        //}

        for (size_t i = 0; i < outputs.binding.getAutomata().outputCount(); ++i)
        {
            const auto c = bindingOutputs[pixelIdx * outputs.binding.getAutomata().outputCount() + i];

            if (!testAndLogInvalidOutput(V4f(c.x, c.y, c.z, alpha), x, y, sampleID, ("to aov " + std::to_string(outputs.binding.getOutput(i).info.name)).c_str()))
                return;

            const auto j = outputs.binding.getOutputIdxFromPathExprAovIdx(i);

            const auto color = [&]() -> Color3 {
                // if (any(outputs.binding.getOutput(i).info.flags & PassFlags::ApplyOutline) && hasToonOutline)
                //    return outlineColor;

                if (any(outputs.binding.getOutput(j).info.flags & PassFlags::Technical))
                    return c;

                return min(c, inputs.pixelSampleClamp);
            }();

            outputs.binding.getOutput(j).addColorSample(x, y, color, alpha); // #note: event if color is black, we have to add a sample because some code (eg. denoising) might depend on samples effectively added
        }
    } // end of tile pixels

    auto & binding = outputs.binding;
    if (binding.needLighting() && renderer->m_directIllumAlgorithm == OslRendererImpl::DirectIllumAlgorithm::ImportanceCaching)
    {
        // Write IC contributions to images
        HighResolutionTimerGuard guard(&tilePerfCounters[STAT_BUFFER_ADD_IC]);

        for (size_t i = 0; i < pixelCount; ++i)
        {
            float alpha = binding.alpha(pixelInfo[i].pixel.x, pixelInfo[i].pixel.y);

            {
                const V4f directSampleCounts = V4f((float) getSampleCount(i, LightingImportanceCache::DistributionNames::F, icData, true),
                                                   (float) getSampleCount(i, LightingImportanceCache::DistributionNames::U, icData, true),
                                                   (float) getSampleCount(i, LightingImportanceCache::DistributionNames::T, icData, true),
                                                   (float) getSampleCount(i, LightingImportanceCache::DistributionNames::C, icData, true)) /
                    float(renderer->m_directIllumSampleCount);

                const V4f indirectSampleCounts = V4f((float) getSampleCount(i, LightingImportanceCache::DistributionNames::F, icData, false),
                                                     (float) getSampleCount(i, LightingImportanceCache::DistributionNames::U, icData, false),
                                                     (float) getSampleCount(i, LightingImportanceCache::DistributionNames::T, icData, false),
                                                     (float) getSampleCount(i, LightingImportanceCache::DistributionNames::C, icData, false)) /
                    float(renderer->m_directIllumSampleCount);

                binding.add(importanceCachingOutputPasses.meanSampleCountDirect, pixelInfo[i].pixel.x, pixelInfo[i].pixel.y, &directSampleCounts.x);
                binding.add(importanceCachingOutputPasses.meanSampleCountIndirect, pixelInfo[i].pixel.x, pixelInfo[i].pixel.y, &indirectSampleCounts.x);

                {
                    const auto directDistribIdx = lightingIC.getDistribIdx(i, 0);
                    const auto probabilities = lightingIC.getDistribProbabilities(directDistribIdx);
                    binding.set(importanceCachingOutputPasses.probabilitiesDirect, pixelInfo[i].pixel.x, pixelInfo[i].pixel.y, probabilities.data());
                    const auto contribs = lightingIC.getDirectContribs();
                    for (auto distIdx = 0; distIdx < LightingImportanceCache::DistributionNames::Count; ++distIdx)
                    {
                        const auto contribWithAlpha = V4f(contribs[distIdx].x, contribs[distIdx].y, contribs[distIdx].z, alpha);
                        binding.set(importanceCachingOutputPasses.tileContribDirect[distIdx], pixelInfo[i].pixel.x, pixelInfo[i].pixel.y, &contribWithAlpha.x);
                    }
                }

                {
                    const auto indirectDistribIdx = lightingIC.getDistribIdx(i, 1);
                    const auto probabilities = lightingIC.getDistribProbabilities(indirectDistribIdx);
                    binding.set(importanceCachingOutputPasses.probabilitiesIndirect, pixelInfo[i].pixel.x, pixelInfo[i].pixel.y, probabilities.data());
                    const auto contribs = lightingIC.getIndirectContribs();
                    for (auto distIdx = 0; distIdx < LightingImportanceCache::DistributionNames::Count; ++distIdx)
                    {
                        const auto contribWithAlpha = V4f(contribs[distIdx].x, contribs[distIdx].y, contribs[distIdx].z, alpha);
                        binding.set(importanceCachingOutputPasses.tileContribIndirect[distIdx], pixelInfo[i].pixel.x, pixelInfo[i].pixel.y, &contribWithAlpha.x);
                    }
                }
            }
        }
    }
}

void updateRayStatistics(size_t count, const OslRendererTask::Path * paths, uint64_t * perfCounters)
{
    for (size_t i = 0; i < count; ++i)
    {
        if (paths[i].samplingState == OslRendererTask::PathSamplingState::Camera)
            ++perfCounters[STAT_CAMERARAYS];
        else if (paths[i].closureCategory == ClosurePrimitive::BSDF)
        {
            if (paths[i].scatteringType == ScatteringType::Count)
            {
                std::cerr << "Warning: paths[i].scatteringType == ScatteringType::Count detected in intersectSolid" << std::endl;
                continue;
            }

            static const int scatteringTypeToStatIdx[] = { STAT_SCATTERINGRAYS_BSDF_DIFFUSE, STAT_SCATTERINGRAYS_BSDF_GLOSSY, STAT_SCATTERINGRAYS_BSDF_SPECULAR,
                                                           STAT_SCATTERINGRAYS_BSDF_STRAIGHT };
            static_assert(
                int(ScatteringType::Diffuse) == 0 && int(ScatteringType::Glossy) == 1 && int(ScatteringType::Specular) == 2 && int(ScatteringType::Straight) == 3,
                "ScatteringType enum order has changed. Update the array scatteringTypeToStatIdx.");
            ++perfCounters[scatteringTypeToStatIdx[int(paths[i].scatteringType)]];
        }
        else if (paths[i].closureCategory == ClosurePrimitive::BSSRDF)
            ++perfCounters[STAT_SCATTERINGRAYS_BSSRDF];
    }
}

// Partition elements for the range [first, last) according to predicate p (elements for which p is true go first)
// Maintain relative order for the group for which the predicate is true (first elements after the call to the function)
// Return an iterator to the second group.
template<class ForwardIt, class UnaryPredicate>
ForwardIt partition(ForwardIt first, ForwardIt last, UnaryPredicate p)
{
    auto firstFalse = std::find_if_not(first, last, p);
    if (firstFalse == last)
        return firstFalse;

    auto firstTrue = std::find_if(std::next(firstFalse), last, p);
    if (firstTrue == last)
        return firstFalse;

    while (firstTrue != last)
    {
        std::iter_swap(firstFalse, firstTrue);
        ++firstFalse;
        firstTrue = std::find_if(std::next(firstTrue), last, p);
    }

    return firstFalse;
}

auto OslRendererTask::evalShadowFactors(
    vector_view<Ray> shadowRays, vector_view<VolumeInstList> shadowRayVolumeStates, vector_view<float> shadowVolumeTransmissionRandomNumbers, const ShadingContext & shadingCtx,
    IntersectSolidShadowStats & stats, InstancePerformances & perfInstances, LightStats & lightStats, ThreadMemoryPool & memPool) const -> vector_view<ShadowResult>
{

    HighResolutionTimerGuard g(&lightStats.shadowRayCost);

    auto shadowRayCount = shadowRays.size();

    auto shadowRayIndexList = make_view(memPool.shadowIndexList, shadowRayCount);
    auto shadowRayLength = make_view(memPool.shadowRayLength, shadowRayCount);
    auto shadowFactors = make_view(memPool.shadowFactors, shadowRayCount);

    auto swappables = std::make_tuple(
        std::ref(shadowRays), std::ref(shadowRayIndexList), std::ref(shadowRayVolumeStates), std::ref(shadowRayLength), std::ref(shadowVolumeTransmissionRandomNumbers),
        std::ref(shadowFactors));

    const auto initialShadowRayCount = shadowRayCount;

    for (size_t j = 0; j < shadowRayCount; ++j)
    {
        shadowRayLength[j] = shadowRays[j].tfar;
        shadowFactors[j] = Color3(1.f);
        shadowRayIndexList[j] = j; // The function will swap arrays, so we keep original indexing in order to return values in the same order
    }

    lightStats.shadowRayCount += shadowRayCount;

    auto isDisabled = [&](auto i) { return shadowRayLength[i] <= 0.f; };
    shadowRayCount = removeIf(shadowRayCount, isDisabled, swappables);

    size_t shadowBounces = 0;
    for (; shadowBounces < inputs.maxShadowBounces && shadowRayCount > 0; ++shadowBounces)
    {
        if (renderer->isPerformanceCounterEnabled(OslRendererImpl::PerformanceCounter_Instance))
        {
            // #todo repair this; we need instID of each shadow ray
            // count shadow rays
            // for (size_t i = 0; i < shadowRayCount; ++i)
            //{
            //    const auto & path = paths[pathIdxOfShadowRay[i]];
            //    if (shadowRays[i].tfar > shadowRays[i].tnear && path.instID != Ray::INVALID_ID)  // an intersection will be computed
            //    {
            //        const auto instanceId = path.instID;
            //        ++perfInstances[instanceId].shadowRayCount;
            //    }
            //}
        }

        {
            // We need a contiguous array of shadow rays for scene->intersect, so we copy the ones to process in a temporary array
            HighResolutionTimerGuard g(&stats[IntersectSolidShadowStats::Intersect]);
            inputs.scene->intersect(shadowRays.data(), shadowRayCount);
            stats[IntersectSolidShadowStats::ShadowRayCount] += shadowRayCount;
        }

        // #todo repair this; we need instID of each shadow ray
        // if (shadowBounces == 0)
        //{
        //    for (size_t i = 0; i < shadowRayCount; ++i)
        //    {
        //        const auto & path = paths[pathIdxOfShadowRay[i]];
        //        if (path.instID == shadowRays[i].instID && path.primID == shadowRays[i].primID)
        //            ++stats[IntersectSolidShadowStats::AutoIntersectionCount];
        //    }
        //}

        {
            HighResolutionTimerGuard g(&stats[IntersectSolidShadowStats::RemoveMissed]);
            auto isUnoccluded = [&](auto i) {
                return shadowRays[i].instID == Ray::INVALID_ID; // unoccluded elements will go at the end
            };
            shadowRayCount = removeIf(shadowRayCount, isUnoccluded, swappables);
        }

        // #todo we should eval volumetric transmission before removing unoccluded shadow rays because the shadow might already be in a volume
        if (renderer->hasVolumetrics())
        {
            HighResolutionTimerGuard g(&stats[IntersectSolidShadowStats::Volumetric]);
            vector_view<Color3> transmissionFactors =
                evalVolumetricTransmission(shadowRayCount, shadowRays.data(), shadowRayVolumeStates.data(), shadowVolumeTransmissionRandomNumbers.data(), shadingCtx, memPool);
            for (size_t i = 0; i < shadowRayCount; ++i)
                shadowFactors[i] *= transmissionFactors[i];
            inOutVolumeTest(
                shadowRayCount, shadowRays.data(), shadowRayVolumeStates.data()); // Directions are not changing for transparent shadow rays so we can directly test for next volume inclusion
        }

        auto transparencyFactors = [&]() {
            HighResolutionTimerGuard g(&stats[IntersectSolidShadowStats::EvalMaterial]);
            return evalRaysTransparency(shadowRayCount, shadowRays.data(), shadingCtx, perfInstances, memPool);
        }();

        for (size_t i = 0; i < shadowRayCount; ++i)
        {
            shadowFactors[i] *= transparencyFactors[i];
        }

        shadowRayCount = removeIf(shadowRayCount, [&shadowFactors](size_t i) { return isBlack(shadowFactors[i]); }, swappables);

        for (size_t i = 0; i < shadowRayCount; ++i)
        {
            // subtract the current tfar (distance to current intersection) to light_ray_length
            // to keep the initial arbitrary shadowRay length, no matter the number of transparent mesh intersections through the ray
            shadowRayLength[i] -= shadowRays[i].tfar;
            // next tfar is equal to the remaining light_ray_length to process
            shadowRays[i].tfar = shadowRayLength[i];
            shadowRays[i].instID = shadowRays[i].primID = shadowRays[i].geomID = Ray::INVALID_ID;
        }
    }

    auto shadowResults = make_view(memPool.shadowResults, initialShadowRayCount);
    for (size_t i = 0; i < initialShadowRayCount; ++i)
    {
        shadowResults[shadowRayIndexList[i]] = { shadowFactors[i], shadowRays[i].instID != Ray::INVALID_ID };
    }

    return shadowResults;
}

static void addVolumeEmissionContrib(OslRendererTask::Accumulator & accumulator, Color3 contribColor)
{
    auto scope = accumulator.push();

    accumulator.move(LPE::Volume, LPE::None, nullptr);
    accumulator.move(LPE::Material, LPE::Emission, nullptr);
    accumulator.accum(contribColor);
}

static void addDirectLightingContribs(
    OslRendererTask::Accumulator & accumulator, const Color3 & lightContrib, const BSDFContribs & bsdfContribs, ClosurePrimitive::Category category,
    OslRendererTask::LightingSamplingStrategy samplingStrat, LightingImportanceCache::DistributionNames::Enum * distribName)
{
    const auto samplingStratScattering =
        (samplingStrat == OslRendererTask::LightingSamplingStrategy::Material) ? LPE::MaterialSampling : (distribName == nullptr) ? LPE::LightSampling : LPE::ImportanceCaching;
    const auto samplingStratTokens = (distribName == nullptr) ? LPE::Dummy : &LPE::lightingICDistribNameToLPELabels[2 * *distribName];

    const auto moveRadiance = [&](const auto & accum) { accum.move(LPE::Light, LPE::Radiance).move(LPE::SamplingStrategy, samplingStratScattering, samplingStratTokens); };

    if (category == ClosurePrimitive::Volume)
    {
        accumulator.push().move(LPE::Volume, LPE::None).apply(moveRadiance).accum(bsdfContribs.diffuseReflectEval() * lightContrib);
        return;
    }

    // Surface lighting:
    if (category == ClosurePrimitive::BSSRDF)
    {
        accumulator.push().move(LPE::SubSurface, LPE::None).apply(moveRadiance).accum(bsdfContribs.diffuseReflectEval() * lightContrib);
        return;
    }

    // BSDF: #todo optimize, dont push and move if contrib is 0

    accumulator.push().move(LPE::Reflect, LPE::Diffuse).apply(moveRadiance).accum(bsdfContribs.diffuseReflectEval() * lightContrib);

    accumulator.push().move(LPE::Transmit, LPE::Diffuse).apply(moveRadiance).accum(bsdfContribs.diffuseTransmitEval() * lightContrib);

    accumulator.push().move(LPE::Reflect, LPE::Glossy).apply(moveRadiance).accum(bsdfContribs.glossyReflectEval() * lightContrib);

    accumulator.push().move(LPE::Transmit, LPE::Glossy).apply(moveRadiance).accum(bsdfContribs.glossyTransmitEval() * lightContrib);

    accumulator.push().move(LPE::Reflect, LPE::Specular).apply(moveRadiance).accum(bsdfContribs.specularReflectEval() * lightContrib);

    accumulator.push().move(LPE::Transmit, LPE::Specular).apply(moveRadiance).accum(bsdfContribs.specularTransmitEval() * lightContrib);

    // for (size_t i = 0; i < shadingContribs.size(); ++i)
    //{
    //    const auto contrib = lightContrib * shadingContribs.eval(i);
    //    if (!isBlack(contrib))
    //    {
    //        accumulator.push().
    //            move(shadingContribs.eventLabel(i), shadingContribs.scatteringLabel(i)).
    //            apply(moveRadiance).
    //            accum(contrib);
    //    }
    //}
}

auto OslRendererTask::evalGlobalsFromRayHits(size_t count, const Path * paths, const Ray * rays, const RayDifferentials * rayDiffs, const uint32_t * rayTypes, ThreadMemoryPool & memPool) const
    -> EvalGlobalsFromRayHitsResult
{
    EvalGlobalsFromRayHitsResult result;

    result.globals = make_view(memPool.globals, count);
    memset(result.globals.data(), 0, sizeof(result.globals[0]) * count);
    result.normalOffsets = make_view(memPool.normalOffsets, count);
    result.rayNears = make_view(memPool.rayNears, count);
    result.instIds = make_view(memPool.instIds, count);
    result.renderStates = make_view(memPool.renderStates, count);

    for (size_t i = 0; i < count; ++i)
    {
        result.renderStates[i].scene = const_cast<SceneImpl *>(inputs.scene);
        result.renderStates[i].cameraId = inputs.cameraId;
        result.renderStates[i].needLighting = inputs.needLighting;
        result.renderStates[i].instanceId = rays[i].instID;
        result.renderStates[i].triangleId = rays[i].primID;
        result.renderStates[i].lights = inputs.lights;
    }

    for (size_t i = 0; i < count; ++i)
    {
        inputs.scene->globalsFromHit(&result.globals[i], rays[i], rayDiffs[i], paths[i].flipHandedness, result.normalOffsets[i], result.rayNears[i]);
        result.globals[i].raytype = rayTypes[i];
        result.globals[i].renderstate = &result.renderStates[i];
    }

    for (size_t i = 0; i < count; ++i)
    {
        result.instIds[i] = rays[i].instID;
    }

    return result;
}

auto OslRendererTask::evalSurfaceMaterial(
    size_t count, const Path * paths, OSL::ShaderGlobals * globals, const Scene::Id * instIds, uint64_t * perfCounters, InstancePerformances & perfInstances, const ShadingContext & shadingCtx,
    ThreadMemoryPool & memPool) const -> EvalSurfaceMaterialResult
{
    HighResolutionTimerGuard g(&perfCounters[STAT_MATERIAL_EVAL]);

    EvalSurfaceMaterialResult result;

    result.surfaceShadingFunctions = make_view(memPool.surfaceShadingFunctions, count);
    result.surfaceShadingSamplers = make_view(memPool.surfaceShadingSamplers, count);
    result.shadingNormals = make_view(memPool.shadingNormals, count, V3f(0.f));

    for (size_t i = 0; i < count; ++i)
    {
        const auto instanceId = instIds[i];

#ifndef DISABLE_PERFORMANCE_COUNTERS
        uint64_t * useconds = nullptr;
        if (renderer->isPerformanceCounterEnabled(OslRendererImpl::PerformanceCounter_Instance))
        {
            auto & item = perfInstances[instanceId];
            useconds = &item.shadingDurationUS;
            ++item.shadingCount;
        }
        HighResolutionTimerGuard g(useconds);
#endif

#if 0 // WIP
        const auto faceId = rays[i].id1;
        const Scene::Id meshId = scene->getMeshIdFromInstanceId(instanceId);
        const int32_t shadingGroup = scene->getShadingGroupFromFace(meshId, faceId);
#endif
        shn::OslRenderer::Id shaderId = renderer->getShadingTreeIdFromInstanceId(instanceId, OslRendererImpl::Assignment::Surface);
        if (shaderId == -1)
        {
            if (renderer->hasVolumeShadingTree(instanceId, 0))
            {
                result.surfaceShadingFunctions[i] = SurfaceShadingFunction::fullyTransparent();
                continue;
            }
            shaderId = renderer->defaultMaterialShadingTreeId; // No shader at all, switch to default material
        }

        const GeometryLight * l = inputs.lights->getLinkedGeometryLight(instanceId);
        const auto isEmissiveGeometryLight = l && l->overrideMaterial && !(specularOnly(paths[i].samplingState) && none(l->emitComponent & EmissionBits::Reflect));
        if (isEmissiveGeometryLight)
            result.surfaceShadingFunctions[i] = SurfaceShadingFunction::fullyEmissive(l->evalEmissionOnGeometry(globals[i], shadingCtx));
        else
            result.surfaceShadingFunctions[i] = evalSurface(shadingCtx, shaderId, globals[i]);
    }

    for (size_t i = 0; i < count; ++i)
        result.surfaceShadingSamplers[i] = SurfaceShadingFunction::Sampler(result.surfaceShadingFunctions[i], globals[i], paths[i].weight, nullptr, shadingCtx.memPool());

    for (size_t i = 0; i < count; ++i)
        result.shadingNormals[i] = result.surfaceShadingSamplers[i].sampleShadingNormal(result.surfaceShadingFunctions[i], globals[i], paths[i].uScatter->uClosureComponent);

    return result;
}

vector_view<Color3>
OslRendererTask::evalRaysTransparency(size_t shadowRayCount, Ray * shadowRays, const ShadingContext & shadingCtx, InstancePerformances & perfInstances, ThreadMemoryPool & memPool) const
{
    auto transparencyFactors = make_view(memPool.shadowTransparencyFactors, shadowRayCount, Color3(0.f)); // everything opaque by default

    for (size_t i = 0; i < shadowRayCount; ++i)
    {
        auto instanceId = shadowRays[i].instID;

#ifndef DISABLE_PERFORMANCE_COUNTERS
        uint64_t * useconds = nullptr;
        if (renderer->isPerformanceCounterEnabled(OslRendererImpl::PerformanceCounter_Instance))
        {
            auto & item = perfInstances[instanceId];
            useconds = &item.shadingDurationUS;
            ++item.shadingCount;
        }
        HighResolutionTimerGuard g(useconds);
#endif

#if 0 // WIP
        const auto faceId = rays[i].id1;
        const Scene::Id meshId = scene->getMeshIdFromInstanceId(instanceId);
        const int32_t shadingGroup = scene->getShadingGroupFromFace(meshId, faceId);
#endif

        shn::OslRenderer::Id shaderId = renderer->getShadingTreeIdFromInstanceId(instanceId, 0);
        const bool hasVolume = renderer->hasVolumeShadingTree(instanceId, 0);

        if (!hasVolume && (shaderId == -1 || renderer->m_shadingTrees[shaderId].opacityMode == OPACITY_MODE_OPAQUE))
            continue;

        OSL::ShaderGlobals globals;
        memset(&globals, 0, sizeof(globals));

        float normalOffset = 0.f;
        float rayNear = 0.f;

        // #todo no need to globals from hit if opacity mode is not evaluate
        // find a solution for ray differentials
        inputs.scene->globalsFromHit(&globals, shadowRays[i], RayDifferentials{ V3f(0.f), V3f(0.f), V3f(0.f), V3f(0.f) }, false, normalOffset, rayNear);
        globals.raytype = renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Shadow);

        if (shaderId == -1) // no surface shader to evaluate, but hasVolume == true
        {
            transparencyFactors[i] = Color3(1.f);
        }
        else if (renderer->m_shadingTrees[shaderId].opacityMode == OPACITY_MODE_CONSTANT)
        {
            transparencyFactors[i] = renderer->m_shadingTrees[shaderId].transparencyConstant;
        }
        else
        {
            transparencyFactors[i] = evalTransparency(shadingCtx, shaderId, globals);
        }
        // Update shadow ray for next intersection
        auto N = -globals.Ng;
        shadowRays[i].org = globals.P + normalOffset * N; // #todo not sure its a good idea to offset on the normal here since we are tracing straight
        shadowRays[i].tnear = rayNear;
    }

    return transparencyFactors;
}

// Evaluate transmission along a batch of rays for volumes recorded in volumeStates
vector_view<Color3> OslRendererTask::evalVolumetricTransmission(
    size_t rayCount, const Ray * rays, const VolumeInstList * volumeStates, const float * uVolumetric, const ShadingContext & shadingCtx, ThreadMemoryPool & memPool) const
{
    auto transmissionFactors = make_view(memPool.transmissionFactors_evalVolumetricTransmission, rayCount, Color3(1.f));

    for (size_t i = 0; i < rayCount; ++i)
    {
        const auto atmosphereShaderId = renderer->getAtmosphereShaderId();
        const size_t volumeCount = volumeStates[i].volumeInstCount + ((atmosphereShaderId > 0) ? 1 : 0);

        for (size_t v = 0; v < volumeCount; ++v)
        {
            shn::OslRenderer::Id volumeShaderId;

            if (v == volumeStates[i].volumeInstCount)
            {
                // Atmosphere shader case
                volumeShaderId = atmosphereShaderId;
            }
            else
            {
                // Test if the volume is casting shadows
                if (!inputs.scene->getInstanceVisibility(volumeStates[i].volumeInstBuffer[v], Scene::INSTANCE_VISIBILITY_SHADOW))
                    continue;
                volumeShaderId = renderer->getShadingTreeIdFromInstanceId(volumeStates[i].volumeInstBuffer[v], 0, OslRendererImpl::Assignment::Volume);
            }
            assert(volumeShaderId >= 0);

            // reset shading data
            OSL::ShaderGlobals volumeSg;
            memset(&volumeSg, 0, sizeof(volumeSg));

            const float d = rays[i].tfar - rays[i].tnear;

            if (renderer->m_shadingTrees[volumeShaderId].type == MaterialType::MATERIAL_TYPE_HETEROGENEOUS_VOLUME)
            {
                // uniform sampling
                inputs.scene->globalsFromVolumeHit(&volumeSg, rays[i], uVolumetric[i] * d);
            }

            const Color3 extinctionCoefficient = evalVolumeExtinction(shadingCtx, volumeShaderId, volumeSg); // for shadows, extinction coefficient = absorption coefficient (no scattering)

            Color3 opticalThickness(0.f);
            if (renderer->m_shadingTrees[volumeShaderId].type == MaterialType::MATERIAL_TYPE_HETEROGENEOUS_VOLUME)
            {
                // uniform sampling
                const float pdf = (d > 0.f) ? 1.f / d : 1.f; // uniform pdf
                opticalThickness = extinctionCoefficient / pdf;
            }
            else
            {
                // analytic integration along the ray
                opticalThickness = extinctionCoefficient * d;
            }

            // extinction
            const Color3 absorption = Color3(shn::min(shn::exp(-opticalThickness), 1.f));
            transmissionFactors[i] *= absorption;
        }
    }

    return transmissionFactors;
}

// Test if rays are entering new volumes or leaving volumes they are inside
// The direction must be the one that will be traced when calling intersect, and the geometric normal Ng must be the one of the origin point
void OslRendererTask::inOutVolumeTest(size_t rayCount, const Ray * rays, VolumeInstList * volumeStates) const
{
    for (auto i = 0; i < rayCount; ++i)
    {
        const OslRenderer::Id instanceId = rays[i].instID;
        if (instanceId == Ray::INVALID_ID)
            continue;

        const shn::OslRenderer::Id shaderId = renderer->getShadingTreeIdFromInstanceId(instanceId, 0, OslRendererImpl::Assignment::Volume);

        // in/out volumes test and fill the right globals
        if (shaderId != -1)
        {
            M44f localToWorld;
            inputs.scene->getInstanceTransform(instanceId, localToWorld, rays[i].time);

            const auto Ng = multNormalMatrix(localToWorld, geometricNormal(rays[i]));
            const auto cosAngle = dot(rays[i].dir, Ng);
            const auto entering = cosAngle < 0.f;
            auto * const foundId = std::find(volumeStates[i].volumeInstBuffer, volumeStates[i].volumeInstBuffer + volumeStates[i].volumeInstCount, instanceId);
            const auto isInside = foundId != volumeStates[i].volumeInstBuffer + volumeStates[i].volumeInstCount;

            if (isInside && !entering)
            {
                *foundId = -1;
                --volumeStates[i].volumeInstCount;
                std::swap(*foundId, volumeStates[i].volumeInstBuffer[volumeStates[i].volumeInstCount]);
            }
            else if (!isInside && entering)
            {
                *foundId = instanceId;
                ++volumeStates[i].volumeInstCount;
            }
        }
    }
}

void OslRendererTask::surfaceLightingIntegration(
    size_t pathCount, const Path * paths, const VolumeInstList * pathVolumeStates, const EvalGlobalsFromRayHitsResult & globalsFromRayHitsResult,
    const EvalSurfaceMaterialResult & evalSurfaceMaterialResult, const BSSRDFPointSamplingResult & bssrdfPointSamplingResult, Accumulator * accumulators,
    LightingImportanceCache & lightImportanceCache, LightingImportanceCacheDataStorage & icData, const ShadingContext & shadingCtx, uint64_t * tilePerfCounters,
    InstancePerformances & shadowPerfInstances, LightStats * lightStats, ThreadMemoryPool & memPool) const
{
    if (inputs.lights->visibleLightCount() == 0)
        return;

    HighResolutionTimerGuard g(&tilePerfCounters[STAT_LIGHTING]);

    const auto * globals = globalsFromRayHitsResult.globals.data();
    const auto * normalOffsets = globalsFromRayHitsResult.normalOffsets.data();
    const auto * rayNears = globalsFromRayHitsResult.rayNears.data();
    const auto * instIds = globalsFromRayHitsResult.instIds.data();
    const auto * surfaceShadingFunctions = evalSurfaceMaterialResult.surfaceShadingFunctions.data();
    const auto * surfaceShadingSamplers = evalSurfaceMaterialResult.surfaceShadingSamplers.data();
    const auto * shadingNormals = evalSurfaceMaterialResult.shadingNormals.data();

    vector_view<Color3> lightingWeights = make_view(memPool.lightingWeights, pathCount);

    for (size_t i = 0; i < pathCount; ++i)
        lightingWeights[i] = paths[i].weight;

    // Sample material before lighting in order to use MIS
    const auto bsdfDirSamples = [&]() {
        HighResolutionTimerGuard g(&tilePerfCounters[STAT_LIGHTING_SAMPLEMATERIAL]);
        return sampleBSDFForLighting(pathCount, paths, globals, surfaceShadingFunctions, surfaceShadingSamplers, memPool);
    }();

    if (renderer->m_directIllumAlgorithm == OslRendererImpl::DirectIllumAlgorithm::AllLights)
    {
        HighResolutionTimerGuard guard(&tilePerfCounters[STAT_LIGHTING_EVALDIRECTLIGHTING]);

        EvalBSDFLightingBatch batch{ pathCount,
                                     nullptr,
                                     paths,
                                     pathVolumeStates,
                                     globals,
                                     lightingWeights.data(),
                                     instIds,
                                     surfaceShadingFunctions,
                                     surfaceShadingSamplers,
                                     normalOffsets,
                                     rayNears,
                                     bsdfDirSamples.data() };

        for (size_t lightIdx = 0; lightIdx < inputs.lights->visibleLightCount(); ++lightIdx)
        {
            const Light & l = *inputs.lights->visibleLights()[lightIdx];

            const EvalLightingInfo lightInfo{ lightIdx, l, shadowPerfInstances, lightStats[lightIdx] };

            if (l.needImportanceSampling())
            {
                const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Light>(lightInfo, batch, shadingCtx, memPool);

                accumLightingContribs(lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Light, ClosurePrimitive::BSDF, nullptr, nullptr, shadingCtx);

                accumulateStatsForLightSampling(evalLightingResult.stats, tilePerfCounters);
            }

            if (!l.isDelta())
            {
                const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Material>(lightInfo, batch, shadingCtx, memPool);

                accumLightingContribs(lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Material, ClosurePrimitive::BSDF, nullptr, nullptr, shadingCtx);

                accumulateStatsForMaterialSampling(evalLightingResult.stats, tilePerfCounters);
            }
        }
    }
    else if (renderer->m_directIllumAlgorithm == OslRendererImpl::DirectIllumAlgorithm::ImportanceCaching)
    {
        // First lighting with importance caching (only light sampling, material sampling comes after) :
        {
            // #todo use a second index list to access these arrays ?
            vector_view<OSL::ShaderGlobals> reindexedGlobals = make_view(memPool.reindexedGlobals, pathCount);
            vector_view<Scene::Id> reindexedInstIds = make_view(memPool.reindexedInstIds, pathCount);
            vector_view<SurfaceShadingFunction> reindexedSurfaceShadingFunctions = make_view(memPool.reindexedSurfaceShadingFunctions, pathCount);
            vector_view<SurfaceShadingFunction::Sampler> reindexedSurfaceShadingSamplers = make_view(memPool.reindexedSurfaceShadingSamplers, pathCount);
            vector_view<float> reindexedNormalOffsets = make_view(memPool.reindexedNormalOffsets, pathCount);
            vector_view<float> reindexedRayNears = make_view(memPool.reindexedRayNears, pathCount);
            vector_view<Color3> reindexedLightingWeights = make_view(memPool.reindexedLightingWeights, pathCount);

            const auto buildReindexedBuffers = [&]() {
                for (size_t i = 0; i < icData.processingListSize; ++i)
                {
                    const auto pathIdx = icData.processingList[i];

                    reindexedGlobals[i] = globals[pathIdx];
                    reindexedInstIds[i] = instIds[pathIdx];
                    reindexedSurfaceShadingFunctions[i] = surfaceShadingFunctions[pathIdx];
                    reindexedSurfaceShadingSamplers[i] = surfaceShadingSamplers[pathIdx];
                    reindexedNormalOffsets[i] = normalOffsets[pathIdx];
                    reindexedRayNears[i] = rayNears[pathIdx];
                    reindexedLightingWeights[i] = paths[pathIdx].weight * icData.samplingWeights[i];
                }
            };

            // Get distributions to use for each pixel
            for (size_t i = 0; i < pathCount; ++i)
                icData.distribIdxBuffer[i] = lightImportanceCache.getDistribIdx(paths[i].pixelIndex, paths[i].depth);

            // First compute lighting with all lights for pixels with no usable importance cache distribution
            {
                HighResolutionTimerGuard guard(&tilePerfCounters[STAT_LIGHTING_IC_BUILDPROCESSINGLIST]);
                lightImportanceCache.computeAllLightsProcessingList(pathCount, icData);
                buildReindexedBuffers();
            }

            {
                HighResolutionTimerGuard guard(&tilePerfCounters[STAT_LIGHTING_EVALDIRECTLIGHTING]);

                EvalBSDFLightingBatch batch{ icData.processingListSize,
                                             icData.processingList.data(),
                                             paths,
                                             pathVolumeStates,
                                             reindexedGlobals.data(),
                                             reindexedLightingWeights.data(),
                                             reindexedInstIds.data(),
                                             reindexedSurfaceShadingFunctions.data(),
                                             reindexedSurfaceShadingSamplers.data(),
                                             reindexedNormalOffsets.data(),
                                             reindexedRayNears.data(),
                                             nullptr };

                for (size_t lightIdx = 0; lightIdx < inputs.lights->visibleLightCount(); ++lightIdx)
                {
                    const Light & l = *inputs.lights->visibleLights()[lightIdx];
                    if (!lightImportanceCache.isImportanceCached(l))
                        continue;

                    const EvalLightingInfo lightInfo{ lightIdx, l, shadowPerfInstances, lightStats[lightIdx] };
                    const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Light>(lightInfo, batch, shadingCtx, memPool);

                    accumLightingContribs(
                        lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Light, ClosurePrimitive::BSDF, &lightImportanceCache, &icData, shadingCtx);

                    accumulateStatsForLightSampling(evalLightingResult.stats, tilePerfCounters);
                }
            }

            // Initialize pointers to random number buffers used by the importance cache
            for (size_t i = 0; i < pathCount; ++i)
                icData.lightIndexRandomNumbersBuffer[i] = &paths[i].uChosenLight[0].uIndex;

            // For each point to illuminate, sample a list of light to evaluate
            {
                HighResolutionTimerGuard guard(&tilePerfCounters[STAT_LIGHTING_IC_SAMPLEDISTRIBUTIONS]);
                lightImportanceCache.sampleLights(pathCount, icData);
            }

            // Loop over all lights but only process those which have been flaggued
            for (size_t lightIdx = 0; lightIdx < inputs.lights->visibleLightCount(); ++lightIdx)
            {
                const Light & l = *inputs.lights->visibleLights()[lightIdx];
                if (!icData.lightsToProcess[lightIdx] || !lightImportanceCache.isImportanceCached(l))
                    continue;

                // Compute the list of points that have sampled this light
                {
                    HighResolutionTimerGuard guard(&tilePerfCounters[STAT_LIGHTING_IC_BUILDPROCESSINGLIST]);
                    lightImportanceCache.computeSampledLightProcessingList(pathCount, lightIdx, icData);
                    buildReindexedBuffers();
                }

                EvalBSDFLightingBatch batch{ icData.processingListSize,
                                             icData.processingList.data(),
                                             paths,
                                             pathVolumeStates,
                                             reindexedGlobals.data(),
                                             reindexedLightingWeights.data(),
                                             reindexedInstIds.data(),
                                             reindexedSurfaceShadingFunctions.data(),
                                             reindexedSurfaceShadingSamplers.data(),
                                             reindexedNormalOffsets.data(),
                                             reindexedRayNears.data(),
                                             nullptr };

                {
                    HighResolutionTimerGuard guard(&tilePerfCounters[STAT_LIGHTING_EVALDIRECTLIGHTING]);

                    const EvalLightingInfo lightInfo{ lightIdx, l, shadowPerfInstances, lightStats[lightIdx] };
                    const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Light>(lightInfo, batch, shadingCtx, memPool);

                    accumLightingContribs(
                        lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Light, ClosurePrimitive::BSDF, &lightImportanceCache, &icData, shadingCtx);

                    accumulateStatsForLightSampling(evalLightingResult.stats, tilePerfCounters);

                    for (int32_t j = 0; j < icData.processingListSize; ++j)
                    {
                        const auto i = icData.processingList[j];
                        if (icData.contribIndices[j] < LightingImportanceCache::DistributionNames::Count)
                            getSampleCount(paths[i].pixelIndex, icData.contribIndices[j], icData, paths[i].samplingState != PathSamplingState::DiffuseOrGlossy) += icData.multiplicities[j];
                    }
                }
            }
        }

        // Eval contributions that are not part of importance caching:
        //  - lights that do not cast shadows
        //  - material sampling
        //  - BSSRDF (#todo: importance caching from the BSSRDF sampled point)

        EvalBSDFLightingBatch batch{ pathCount,
                                     nullptr,
                                     paths,
                                     pathVolumeStates,
                                     globals,
                                     lightingWeights.data(),
                                     instIds,
                                     surfaceShadingFunctions,
                                     surfaceShadingSamplers,
                                     normalOffsets,
                                     rayNears,
                                     bsdfDirSamples.data() };

        // Material sampling for importance cached lights
        {
            HighResolutionTimerGuard guard(&tilePerfCounters[STAT_LIGHTING_EVALDIRECTLIGHTING]);
            for (size_t lightIdx = 0; lightIdx < inputs.lights->visibleLightCount(); ++lightIdx)
            {
                const Light & l = *inputs.lights->visibleLights()[lightIdx];

                if (lightImportanceCache.isImportanceCached(l) && !l.isDelta())
                {
                    const EvalLightingInfo lightInfo{ lightIdx, l, shadowPerfInstances, lightStats[lightIdx] };
                    const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Material>(lightInfo, batch, shadingCtx, memPool);

                    accumLightingContribs(lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Material, ClosurePrimitive::BSDF, nullptr, nullptr, shadingCtx);

                    accumulateStatsForMaterialSampling(evalLightingResult.stats, tilePerfCounters);
                }
            }
        }

        // Light that are not importance cached
        {
            HighResolutionTimerGuard guard(&tilePerfCounters[STAT_LIGHTING_EVALDIRECTLIGHTING]);
            for (size_t lightIdx = 0; lightIdx < inputs.lights->visibleLightCount(); ++lightIdx)
            {
                const Light & l = *inputs.lights->visibleLights()[lightIdx];
                if (!lightImportanceCache.isImportanceCached(l))
                {
                    EvalLightingInfo lightInfo{ lightIdx, l, shadowPerfInstances, lightStats[lightIdx] };

                    if (l.needImportanceSampling())
                    {
                        const EvalLightingInfo lightInfo{ lightIdx, l, shadowPerfInstances, lightStats[lightIdx] };
                        const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Light>(lightInfo, batch, shadingCtx, memPool);

                        accumLightingContribs(lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Light, ClosurePrimitive::BSDF, nullptr, nullptr, shadingCtx);

                        accumulateStatsForLightSampling(evalLightingResult.stats, tilePerfCounters);
                    }

                    if (!l.isDelta())
                    {
                        const EvalLightingInfo lightInfo{ lightIdx, l, shadowPerfInstances, lightStats[lightIdx] };
                        const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Material>(lightInfo, batch, shadingCtx, memPool);

                        accumLightingContribs(lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Material, ClosurePrimitive::BSDF, nullptr, nullptr, shadingCtx);

                        accumulateStatsForMaterialSampling(evalLightingResult.stats, tilePerfCounters);
                    }
                }
            }
        }
    }

    // BSSRDF lighting, maybe it should be extracted in another function ?
    if (!bssrdfPointSamplingResult.pathIndexList.empty())
    {
        const auto pathWithBssrdfCount = bssrdfPointSamplingResult.pathIndexList.size();

        vector_view<Scene::Id> reindexedInstIds = make_view(memPool.reindexedInstIds, pathWithBssrdfCount);
        vector_view<SurfaceShadingFunction> reindexedSurfaceShadingFunctions = make_view(memPool.reindexedSurfaceShadingFunctions, pathWithBssrdfCount);
        vector_view<float> reindexedNormalOffsets = make_view(memPool.reindexedNormalOffsets, pathWithBssrdfCount);
        vector_view<float> reindexedRayNears = make_view(memPool.reindexedRayNears, pathWithBssrdfCount);
        vector_view<Color3> reindexedLightingWeights = make_view(memPool.reindexedLightingWeights, pathWithBssrdfCount);

        // Pack data before lighting
        for (size_t i = 0; i < pathWithBssrdfCount; ++i)
        {
            const auto pathIdx = bssrdfPointSamplingResult.pathIndexList[i];

            reindexedSurfaceShadingFunctions[i] = surfaceShadingFunctions[pathIdx];
            reindexedInstIds[i] = instIds[pathIdx];
            reindexedNormalOffsets[i] = normalOffsets[pathIdx];
            reindexedRayNears[i] = rayNears[pathIdx];
            reindexedLightingWeights[i] = bssrdfPointSamplingResult.weights[i] * paths[pathIdx].weight;
        }

        const auto bssrdfDirSamples = [&]() {
            HighResolutionTimerGuard g(&tilePerfCounters[STAT_LIGHTING_SAMPLEMATERIAL]);
            return sampleBSSRDFForLighting(bssrdfPointSamplingResult, paths, memPool);
        }();

        EvalBSSRDFLightingBatch batch{ pathWithBssrdfCount,
                                       bssrdfPointSamplingResult.pathIndexList.data(),
                                       paths,
                                       pathVolumeStates,
                                       bssrdfPointSamplingResult.globals.data(),
                                       reindexedLightingWeights.data(),
                                       reindexedInstIds.data(),
                                       reindexedNormalOffsets.data(),
                                       reindexedRayNears.data(),
                                       bssrdfDirSamples.data() };

        {
            HighResolutionTimerGuard guard(&tilePerfCounters[STAT_LIGHTING_EVALDIRECTLIGHTING]);
            for (size_t lightIdx = 0; lightIdx < inputs.lights->visibleLightCount(); ++lightIdx)
            {
                const Light & l = *inputs.lights->visibleLights()[lightIdx];
                if (none(l.emitComponent & EmissionBits::Diffuse))
                    continue;

                EvalLightingInfo lightInfo{ lightIdx, l, shadowPerfInstances, lightStats[lightIdx] };

                if (l.needImportanceSampling())
                {
                    const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Light>(lightInfo, batch, shadingCtx, memPool);

                    accumLightingContribs(lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Light, ClosurePrimitive::BSSRDF, nullptr, nullptr, shadingCtx);

                    accumulateStatsForLightSampling(evalLightingResult.stats, tilePerfCounters);
                }
                else if (!l.isDelta())
                {
                    const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Material>(lightInfo, batch, shadingCtx, memPool);

                    accumLightingContribs(lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Material, ClosurePrimitive::BSSRDF, nullptr, nullptr, shadingCtx);

                    accumulateStatsForMaterialSampling(evalLightingResult.stats, tilePerfCounters);
                }
            }
        }
    }
}

OslRendererTask::VolumeList
OslRendererTask::computeVolumeList(const Path * paths, const VolumeInstList * pathVolumeStates, size_t count, uint64_t * perfCounters, ThreadMemoryPool & memPool) const
{
    assert(renderer->hasVolumetrics());

    HighResolutionTimerGuard timerGuard(&perfCounters[STAT_VOLUMETRIC_GATHERVOLUMES]);

    VolumeList volumeList{ memPool };

    for (size_t pathIdx = 0; pathIdx < count; ++pathIdx)
    {
        for (size_t j = 0; j < pathVolumeStates[pathIdx].volumeInstCount; ++j)
        {
            const int32_t volInstId = pathVolumeStates[pathIdx].volumeInstBuffer[j];

            const auto shadingTreeId = renderer->getShadingTreeIdFromInstanceId(volInstId, 0, OslRendererImpl::Assignment::Volume);
            const auto foundId =
                std::find_if(std::begin(volumeList.volumeInfoList), std::end(volumeList.volumeInfoList), [volInstId](const auto & info) { return info.volumeInstanceId == volInstId; });
            if (foundId == std::end(volumeList.volumeInfoList))
                volumeList.volumeInfoList.emplace_back(volInstId, shadingTreeId, 0, 0, 0, 0);
        }

        // early break if all the volumes are in the tile
        if (volumeList.volumeInfoList.size() >= renderer->m_volumeInstancesCount)
            break;
    }

    for (size_t v = 0; v < volumeList.volumeInfoList.size(); ++v)
    {
        // Compute a list of path indices that are inside the current volume in order to process them
        volumeList.volumeInfoList[v].indexListBegin = volumeList.pathIndexLists.size();
        volumeList.volumeInfoList[v].lightingIndexListBegin = volumeList.lightingPathIndexLists.size();
        for (int32_t pathIdx = 0; pathIdx < count; ++pathIdx)
        {
            int32_t * pathVolumeBufferEnd = pathVolumeStates[pathIdx].volumeInstBuffer + pathVolumeStates[pathIdx].volumeInstCount;
            if (std::find(pathVolumeStates[pathIdx].volumeInstBuffer, pathVolumeBufferEnd, volumeList.volumeInfoList[v].volumeInstanceId) != pathVolumeBufferEnd)
            {
                volumeList.pathIndexLists.emplace_back(pathIdx);

                const auto isBlackhole = inputs.scene->isBlackholeInstance(
                    volumeList.volumeInfoList[v].volumeInstanceId, isDirect(paths[pathIdx]) ? Scene::INSTANCE_VISIBILITY_PRIMARY : Scene::INSTANCE_VISIBILITY_SECONDARY);
                if (!isBlackhole)
                    volumeList.lightingPathIndexLists.emplace_back(pathIdx);
            }
        }
        volumeList.volumeInfoList[v].indexListEnd = volumeList.pathIndexLists.size();
        volumeList.volumeInfoList[v].lightingIndexListEnd = volumeList.lightingPathIndexLists.size();
    }

    // #todo atmosphere shader is broken since I added lightingPathIndexLists, fix it ! see the test, it should be brighter
    const shn::OslRenderer::Id atmosphereShaderId = renderer->getAtmosphereShaderId();
    if (atmosphereShaderId != -1)
    {
        volumeList.volumeInfoList.emplace_back(
            -1, atmosphereShaderId, volumeList.pathIndexLists.size(), volumeList.pathIndexLists.size() + count, volumeList.lightingPathIndexLists.size(),
            volumeList.lightingPathIndexLists.size() + count);
        for (size_t pathIdx = 0; pathIdx < count; ++pathIdx)
        {
            volumeList.pathIndexLists.emplace_back(pathIdx);
            volumeList.lightingPathIndexLists.emplace_back(pathIdx);
        }
    }

    return volumeList;
}

auto OslRendererTask::volumetricDensitySampling(
    size_t count, const Path * paths, const VolumeInstList * pathVolumeStates, const RayBatch & scatteringRayBatch, const VolumeList & tileVolumeIds, const ShadingContext & shadingCtx,
    uint64_t * perfCounters, ThreadMemoryPool & memPool) const -> VolumeDensitySamplingResult
{
    const auto nbVolumesInTile = tileVolumeIds.volumeCount();

    VolumeDensitySamplingResult result{ tileVolumeIds.pathIndexLists.size(), tileVolumeIds.lightingPathIndexLists.size(), memPool };
    size_t offset = 0;
    size_t lightingPathIdx = 0;

    for (size_t v = 0; v < nbVolumesInTile; ++v)
    {
        const auto volumeShaderId = tileVolumeIds.volumeInfoList[v].shadingTreeId;
        assert(volumeShaderId != -1);

        const auto pathIndexList = tileVolumeIds.pathIndexList(v);
        const auto nbPathsInVolume = tileVolumeIds.numberOfPathsInVolume(v);

        // Compute density sampling
        {
            HighResolutionTimerGuard timerGuard(&perfCounters[STAT_VOLUMETRIC_DENSITY_SAMPLING]);
            for (int32_t j = 0; j < nbPathsInVolume; ++j)
            {
                const auto pathIdx = pathIndexList[j];

                const float d = scatteringRayBatch.rays()[pathIdx].tfar;
                if (d == 0.f)
                    continue;

                auto sampledDistance = 0.f;
                auto invPdf = 0.f;
                auto opticalThicknessEstimate = Color3(0.f);

                VolumeShadingFunction volumeShadingFunction;
                OSL::ShaderGlobals globals;
                memset(&globals, 0, sizeof(globals));

                if (renderer->m_shadingTrees[volumeShaderId].type == MaterialType::MATERIAL_TYPE_HOMOGENEOUS_VOLUME)
                {
                    // distance sampling
                    volumeShadingFunction = evalVolume(shadingCtx, volumeShaderId, globals); // We can evaluate the shader without initializing globals because homogeneous

                    const auto sampledExtinctionCoefficient = volumeShadingFunction.extinction();
                    float lumDensitySigmaT = luminance(sampledExtinctionCoefficient);

                    float densityPDF = 0.f;
                    float xi = paths[pathIdx].uVolumetric->uDistance;
                    sampledDistance = sampleDistanceVolumetric(lumDensitySigmaT, d, xi, densityPDF);
                    invPdf = densityPDF <= 0.f ? 0.f : 1.f / densityPDF;

                    inputs.scene->globalsFromVolumeHit(&globals, scatteringRayBatch.rays()[pathIdx], sampledDistance);

                    // extinction accumulation
                    opticalThicknessEstimate = sampledExtinctionCoefficient * d; // analytic computation
                }
                else
                {
                    // uniform sampling
                    sampledDistance = paths[pathIdx].uVolumetric->uDistance * d;
                    invPdf = d; // inverse(1/d)

                    inputs.scene->globalsFromVolumeHit(&globals, scatteringRayBatch.rays()[pathIdx], sampledDistance);

                    volumeShadingFunction = evalVolume(shadingCtx, volumeShaderId, globals);

                    const auto sampledExtinctionCoefficient = volumeShadingFunction.extinction();

                    // extinction accumulation
                    opticalThicknessEstimate = sampledExtinctionCoefficient * invPdf; // Monte carlo estimate, very low convergence for highly heterogenous volumes
                }

                // Indexing of pathIndexList:
                result.transmittanceToSurface[offset + j] = shn::min(shn::exp(-opticalThicknessEstimate), 1.f);

                if (!inputs.scene->isBlackholeInstance(
                        tileVolumeIds.volumeInfoList[v].volumeInstanceId, isDirect(paths[pathIdx]) ? Scene::INSTANCE_VISIBILITY_PRIMARY : Scene::INSTANCE_VISIBILITY_SECONDARY))
                {
                    result.volumeShadingFunctions[lightingPathIdx] = volumeShadingFunction;
                    result.globals[lightingPathIdx] = globals;
                    result.inversePdfs[lightingPathIdx] = invPdf;
                    result.sampledDistances[lightingPathIdx] = sampledDistance;

                    // Only valid for homogeneous volumes. For heterogeneous volumes, we have to resample vRay between 0 and distanceToShadingPoint
                    result.transmittanceToVolumePoint[lightingPathIdx] = shn::min(shn::exp(-sampledDistance * volumeShadingFunction.extinction()), 1.f);
                    result.instIds[lightingPathIdx] = tileVolumeIds.volumeInfoList[v].volumeInstanceId;

                    ++lightingPathIdx;
                }
            }
        }

        offset += nbPathsInVolume;
    }

    return result;
}

void OslRendererTask::volumetricLightingIntegration(
    size_t count, const Path * paths, const VolumeInstList * pathVolumeStates, const RayBatch & scatteringRayBatch, const VolumeList & tileVolumeIds,
    const VolumeDensitySamplingResult & densitySamplingResult, Accumulator * accumulators, const ShadingContext & shadingCtx, uint64_t * perfCounters, InstancePerformances & perfInstances,
    LightStats * lightStats, ThreadMemoryPool & memPool) const
{
    const auto nbVolumesInTile = tileVolumeIds.volumeInfoList.size();
    if (nbVolumesInTile == 0)
        return;

    // Accumulate contributions for each volume
    for (size_t v = 0, offset = 0; v < nbVolumesInTile; ++v)
    {
        const auto volumeShaderId = tileVolumeIds.volumeInfoList[v].shadingTreeId;
        assert(volumeShaderId != -1);

        const auto pathIndexList = tileVolumeIds.lightingPathIndexLists.data() + tileVolumeIds.volumeInfoList[v].lightingIndexListBegin;
        const auto nbPathsInVolume = tileVolumeIds.volumeInfoList[v].lightingIndexListEnd - tileVolumeIds.volumeInfoList[v].lightingIndexListBegin;

        const auto name = inputs.scene->getInstanceNameFromId(tileVolumeIds.volumeInfoList[v].volumeInstanceId);

        if (nbPathsInVolume == 0)
            continue; // May append if this volume is blackhole for all paths inside

        // Volume emission
        {
            HighResolutionTimerGuard timerGuard(&perfCounters[STAT_VOLUMETRIC_DENSITY_SAMPLING]);
            for (int32_t j = 0; j < nbPathsInVolume; ++j)
            {
                const auto pathIdx = pathIndexList[j];
                const auto & volumeShadingFunction = densitySamplingResult.volumeShadingFunctions[offset + j];
                const auto invPdf = densitySamplingResult.inversePdfs[offset + j];
                const auto transmittanceToVolumePoint = densitySamplingResult.transmittanceToVolumePoint[offset + j];
                addVolumeEmissionContrib(accumulators[paths[pathIdx].accumulatorIndex], paths[pathIdx].weight * transmittanceToVolumePoint * volumeShadingFunction.emission() * invPdf);
            }
        }

        {
            HighResolutionTimerGuard timerGuard(&perfCounters[STAT_VOLUMETRIC_DENSITY_LIGHTING]);

            // direct illumination on density sample
            for (size_t lightIdx = 0; lightIdx < inputs.lights->visibleLightCount(); ++lightIdx)
            {
                const Light & light = *inputs.lights->visibleLights()[lightIdx];

                if (none(light.emitComponent & EmissionBits::Volumetric)) // light does not affects volumetrics
                    continue;

                vector_view<Color3> lightingWeights = make_view(memPool.lightingWeights, nbPathsInVolume);

                {
                    HighResolutionTimerGuard timerGuard(&perfCounters[STAT_VOLUMETRIC_DENSITY_LIGHTING_MIS]);

                    // mis lighting on density sample contribution
                    for (int32_t j = 0; j < nbPathsInVolume; ++j)
                    {
                        const auto pathIdx = pathIndexList[j];
                        const auto distanceToShadingPoint = densitySamplingResult.sampledDistances[offset + j];
                        const auto invPdf = densitySamplingResult.inversePdfs[offset + j];
                        const auto transmittanceToVolumePoint = densitySamplingResult.transmittanceToVolumePoint[offset + j];

                        float misFactor = 1.f;
                        if (light.needEquiAngularSampling())
                        {
                            const float d = scatteringRayBatch.rays()[pathIdx].tfar;
                            auto s = toV2f(paths[pathIdx].uLight->uDir);
                            EquiAngularSamplingParams easParams = light.equiAngularParams(scatteringRayBatch.rays()[pathIdx].org, scatteringRayBatch.rays()[pathIdx].dir, d, s.x, s.y);
                            float easPDF = pdfEquiAngular(distanceToShadingPoint, easParams.delta, easParams.thetaA, easParams.thetaB, easParams.D);
                            misFactor = misWeight(invPdf, easPDF);
                        }

                        lightingWeights[j] = paths[pathIdx].weight * transmittanceToVolumePoint * invPdf * misFactor; // Set pathWeight for the next call to estimateDirectLighting
                    }
                }

                EvalVolumeLightingBatch batch{ nbPathsInVolume,
                                               pathIndexList,
                                               paths,
                                               pathVolumeStates,
                                               densitySamplingResult.globals.data() + offset,
                                               lightingWeights.data(),
                                               densitySamplingResult.instIds.data() + offset,
                                               densitySamplingResult.volumeShadingFunctions.data() + offset };

                const EvalLightingInfo lightInfo{ lightIdx, light, perfInstances, lightStats[lightIdx] };
                const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Light>(lightInfo, batch, shadingCtx, memPool);

                accumLightingContribs(lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Light, ClosurePrimitive::Volume, nullptr, nullptr, shadingCtx);

                accumulateStatsForDensitySampling(evalLightingResult.stats, perfCounters);
            }
        }

        // EAS sampling

        vector_view<OSL::ShaderGlobals> globals = make_view(memPool.volumeEasGlobals, nbPathsInVolume);
        memset(globals.data(), 0, sizeof(globals[0]) * globals.size());
        vector_view<VolumeShadingFunction> shadingFunctions = make_view(memPool.volumeEasShadingFunctions, nbPathsInVolume);
        vector_view<Color3> lightingWeights = make_view(memPool.lightingWeights, nbPathsInVolume);

        for (size_t lightIdx = 0; lightIdx < inputs.lights->visibleLightCount(); ++lightIdx)
        {
            const Light & light = *inputs.lights->visibleLights()[lightIdx];

            if (none(light.emitComponent & EmissionBits::Volumetric) || !light.needEquiAngularSampling())
                continue;

            {
                {
                    HighResolutionTimerGuard timerGuard(&perfCounters[STAT_VOLUMETRIC_EAS_SAMPLING]);
                    // mis lighting on density sample contribution
                    for (int32_t j = 0; j < nbPathsInVolume; ++j)
                    {
                        const auto pathIdx = pathIndexList[j];

                        const float d = scatteringRayBatch.rays()[pathIdx].tfar;

                        const V2f s = toV2f(paths[pathIdx].uLight->uDir);

                        EquiAngularSamplingParams easParams = light.equiAngularParams(scatteringRayBatch.rays()[pathIdx].org, scatteringRayBatch.rays()[pathIdx].dir, d, s.x, s.y);
                        float easPdf = 0.f;
                        const float easDistanceSample = sampleEquiAngular(easParams.delta, easParams.thetaA, easParams.thetaB, easParams.D, paths[pathIdx].uVolumetric->uDistance, easPdf);

                        const auto invPdf = easPdf <= 0.f ? 0.f : 1.f / easPdf;

                        inputs.scene->globalsFromVolumeHit(&globals[j], scatteringRayBatch.rays()[pathIdx], easDistanceSample);
                        shadingFunctions[j] = evalVolume(shadingCtx, volumeShaderId, globals[j]);

                        const auto extinction = shadingFunctions[j].extinction();

                        // Same as before, only working for homogenous volume:
                        const auto transmittanceToVolumePoint = Color3(shn::min(shn::exp(-easDistanceSample * extinction), 1.f));

                        float densityPDF = 0.f;
                        if (renderer->m_shadingTrees[volumeShaderId].type == MaterialType::MATERIAL_TYPE_HOMOGENEOUS_VOLUME)
                        {
                            densityPDF = pdfDistanceVolumetricSampling(easDistanceSample, luminance(extinction), d);
                        }
                        else
                        {
                            densityPDF = 1.f / d;
                        }

                        float misFactor = misWeight(invPdf, densityPDF);
                        lightingWeights[j] = paths[pathIdx].weight * transmittanceToVolumePoint * invPdf * misFactor;
                    }
                }

                {
                    HighResolutionTimerGuard timerGuard(&perfCounters[STAT_VOLUMETRIC_EAS_LIGHTING]);

                    EvalVolumeLightingBatch batch{ nbPathsInVolume,        pathIndexList, paths, pathVolumeStates, globals.data(), lightingWeights.data(), densitySamplingResult.instIds.data(),
                                                   shadingFunctions.data() };

                    const EvalLightingInfo lightInfo{ lightIdx, light, perfInstances, lightStats[lightIdx] };
                    const auto evalLightingResult = evalLighting<LightingSamplingStrategy::Light>(lightInfo, batch, shadingCtx, memPool);

                    accumLightingContribs(lightInfo, batch, evalLightingResult, accumulators, LightingSamplingStrategy::Light, ClosurePrimitive::Volume, nullptr, nullptr, shadingCtx);

                    accumulateStatsForEquiAngularSampling(evalLightingResult.stats, perfCounters);
                }
            }
        }

        offset += nbPathsInVolume;
    }
}

vector_view<BxDFDirSample> OslRendererTask::sampleBSDFForLighting(
    size_t count, const Path * paths, const OSL::ShaderGlobals * globals, const SurfaceShadingFunction * shadingFunctions, const SurfaceShadingFunction::Sampler * samplers,
    ThreadMemoryPool & memPool) const
{
    auto samples = make_view(memPool.lightingBsdfDirSamples, count);
    for (auto i = 0; i < count; ++i)
    {
        samples[i] = samplers[i].sampleBSDFDirection(shadingFunctions[i], globals[i], paths[i].uScatter->uClosureComponent, paths[i].uScatter->uDir[0], paths[i].uScatter->uDir[1]);
    }
    return samples;
}

vector_view<BxDFDirSample> OslRendererTask::sampleBSSRDFForLighting(const BSSRDFPointSamplingResult & bssrdfPointSamplingResult, const Path * paths, ThreadMemoryPool & memPool) const
{
    // If a given light cannot be importance sampled, then we will need to sample the BSSRDF to evaluate the lighting
    bool needBSSRDFDirSamples = false;
    for (const Light * pLight: inputs.lights->visibleLights())
    {
        const Light & light = *pLight;
        if (!light.needImportanceSampling() && any(light.emitComponent & EmissionBits::Diffuse))
        {
            needBSSRDFDirSamples = true;
            break;
        }
    }

    if (!needBSSRDFDirSamples)
        return {};

    auto samples = make_view(memPool.lightingBssrdfDirSamples, bssrdfPointSamplingResult.pathIndexList.size());
    for (auto j = 0; j < bssrdfPointSamplingResult.pathIndexList.size(); ++j)
    {
        const auto pathIdx = bssrdfPointSamplingResult.pathIndexList[j];
        samples[j] = sampleCosineBSSRDFLobe(bssrdfPointSamplingResult.globals[j].N, paths[pathIdx].uScatter->uDir[0], paths[pathIdx].uScatter->uDir[1]);
    }
    return samples;
}

// #todo after refactor many lambdas are now useless, remove them

template<typename EvalLightingBatchT>
void OslRendererTask::evalLightingAndShading(
    const EvalLightingInfo & lightInfo, const EvalLightingBatchT & batch, EvalLightingResult & result, ThreadMemoryPool & memPool, LightSamplingStratTag) const
{
    assert(lightInfo.l.needImportanceSampling());

    lightInfo.lightStats.samplingCount += batch.pathCount;
    result.lightSamplingResults = [&]() {
        HighResolutionTimerGuard g(&result.stats[EvalDirectLightingStats::Sample]);
        HighResolutionTimerGuard g2(&lightInfo.lightStats.samplingCost);
        return sampleLight(lightInfo, batch, result.samplingWeights.data(), memPool);
    }();

    result.shadingContribs = [&]() {
        HighResolutionTimerGuard g(&result.stats[EvalDirectLightingStats::Sample]);
        return evalShadingContribsAfterLightSampling(result.lightSamplingResults.data(), result.samplingWeights.data(), lightInfo, batch, memPool);
    }();
}

template<typename EvalLightingBatchT>
void OslRendererTask::evalLightingAndShading(
    const EvalLightingInfo & lightInfo, const EvalLightingBatchT & batch, EvalLightingResult & result, ThreadMemoryPool & memPool, MaterialSamplingStratTag) const
{
    assert(!lightInfo.l.isDelta());

    result.lightSamplingResults = [&]() {
        HighResolutionTimerGuard g(&result.stats[EvalDirectLightingStats::Sample]);
        return evalLight(lightInfo, batch, result.samplingWeights.data(), memPool);
    }();

    result.shadingContribs = [&]() {
        HighResolutionTimerGuard g(&result.stats[EvalDirectLightingStats::Sample]);
        return evalShadingContribsAfterMaterialSampling(result.lightSamplingResults.data(), result.samplingWeights.data(), lightInfo, batch, memPool);
    }();
}

template<OslRendererTask::LightingSamplingStrategy samplingStrategy, typename EvalLightingBatchT>
auto OslRendererTask::evalLighting(const EvalLightingInfo & lightInfo, const EvalLightingBatchT & batch, const ShadingContext & shadingCtx, ThreadMemoryPool & memPool) const
    -> EvalLightingResult
{
    EvalLightingResult result;
    HighResolutionTimerGuard g(&result.stats[EvalDirectLightingStats::Total]);

    result.samplingWeights = make_view(memPool.lightingSamplingWeights, batch.pathCount, 1.f);

    evalLightingAndShading(lightInfo, batch, result, memPool, std::integral_constant<LightingSamplingStrategy, samplingStrategy>());

    lowLightThresholdFiltering(
        batch.pathCount, batch.globals, batch.lightingWeights, result.lightSamplingResults.data(), result.shadingContribs.data(), result.samplingWeights.data(), lightInfo.l);

    result.shadowResults = evalShadowsForLighting(lightInfo, batch, result.lightSamplingResults.data(), result.samplingWeights.data(), result.stats, shadingCtx, memPool);

    return result;
}

vector_view<LightSamplingResult> OslRendererTask::sampleLight(const EvalLightingInfo & lightInfo, const EvalLightingBatch & batch, float * outSamplingWeights, ThreadMemoryPool & memPool) const
{
    auto results = make_view(memPool.lightingSamplingResults, batch.pathCount);

    for (size_t i = 0; i < batch.pathCount; ++i)
    {
        const auto pathIdx = batch.pathIndexList ? batch.pathIndexList[i] : i;

        if (!inputs.lights->isLit(lightInfo.l.linkingMode, lightInfo.l.linkId, batch.instIds[i]))
        {
            outSamplingWeights[i] = 0.f;
            continue;
        }

        results[i] = lightInfo.l.sample(batch.globals[i], batch.paths[pathIdx].uLight->uDir[0], batch.paths[pathIdx].uLight->uDir[1], batch.paths[pathIdx].uLight->uPrimitive);
        const auto samplingWeight = results[i].incidentDirection.pdf > 0.f ? 1.f / results[i].incidentDirection.pdf : 0.f;
        outSamplingWeights[i] *= samplingWeight;
    }

    return results;
}

// Can only be called for BSDF and BSSDF lighting batches
template<typename EvalLightingBatchT>
vector_view<LightSamplingResult> OslRendererTask::evalLight(const EvalLightingInfo & lightInfo, const EvalLightingBatchT & batch, float * outSamplingWeights, ThreadMemoryPool & memPool) const
{
    auto results = make_view(memPool.lightingSamplingResults, batch.pathCount);

    for (size_t i = 0; i < batch.pathCount; ++i)
    {
        if (!inputs.lights->isLit(lightInfo.l.linkingMode, lightInfo.l.linkId, batch.instIds[i]))
            continue;

        const auto samplingWeight = batch.materialSamples[i].pdf() > 0.f ? 1.f / batch.materialSamples[i].pdf() : 0.f;
        outSamplingWeights[i] *= samplingWeight;

        if (outSamplingWeights[i] <= 0.f)
            continue;

        results[i] = lightInfo.l.eval(batch.materialSamples[i].direction.val(), batch.globals[i]);
    }

    return results;
}

vector_view<BSDFContribs> OslRendererTask::evalShadingContribsAfterLightSampling(
    const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalBSDFLightingBatch & batch, ThreadMemoryPool & memPool) const
{
    auto results = make_view(memPool.lightingShadingContribs, batch.pathCount);

    for (size_t i = 0; i < batch.pathCount; ++i)
    {
        if (outSamplingWeights[i] <= 0.f)
            continue;

        const auto lightDir = lightSamplingResults[i].incidentDirection;
        results[i] = batch.shadingSamplers[i].evalBSDFContribs(batch.shadingFunctions[i], batch.globals[i], lightDir.value, lightInfo.l.emitComponent);
    }

    if (!lightInfo.l.isDelta())
    {
        for (size_t i = 0; i < batch.pathCount; ++i)
        {
            if (outSamplingWeights[i] <= 0.f)
                continue;

            const auto lightDir = lightSamplingResults[i].incidentDirection;
            const float misFactor = misPowerHeuristic(lightDir.pdf, results[i].pdf());
            outSamplingWeights[i] *= misFactor;
        }
    }

    return results;
}

vector_view<BSDFContribs> OslRendererTask::evalShadingContribsAfterLightSampling(
    const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalBSSRDFLightingBatch & batch, ThreadMemoryPool & memPool) const
{
    auto results = make_view(memPool.lightingShadingContribs, batch.pathCount);

    for (size_t i = 0; i < batch.pathCount; ++i)
    {
        const auto lightDir = lightSamplingResults[i].incidentDirection;
        if (lightDir.pdf <= 0.f)
            continue;
        results[i][BSDFContribs::DiffuseReflect] = evalCosineBSSRDFLobe(batch.globals[i].N, lightDir.value);
    }

    return results;
}

vector_view<BSDFContribs> OslRendererTask::evalShadingContribsAfterLightSampling(
    const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalVolumeLightingBatch & batch, ThreadMemoryPool & memPool) const
{
    auto results = make_view(memPool.lightingShadingContribs, batch.pathCount);

    for (size_t i = 0; i < batch.pathCount; ++i)
    {
        const auto lightDir = lightSamplingResults[i].incidentDirection;
        if (lightDir.pdf <= 0.f)
            continue;
        results[i][BSDFContribs::DiffuseReflect] = BxDFDirSample::MCEstimate::fromEval(batch.shadingFunctions[i].evalPhase(batch.globals[i], lightDir.value, lightInfo.l.emitComponent), 1.f);
    }

    return results;
}

vector_view<BSDFContribs> OslRendererTask::evalShadingContribsAfterMaterialSampling(
    const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalBSDFLightingBatch & batch, ThreadMemoryPool & memPool) const
{
    auto results = make_view(memPool.lightingShadingContribs, batch.pathCount);

    for (size_t i = 0; i < batch.pathCount; ++i)
    {
        if (outSamplingWeights[i] <= 0.f)
            continue;

        const auto lightDir = lightSamplingResults[i].incidentDirection;
        const auto & materialSample = batch.materialSamples[i];

        const ScatteringType scattering = materialSample.scatteringType;

        const bool notSingular = (scattering != ScatteringType::Specular && scattering != ScatteringType::Straight);
        // Eval BSDF in sampled direction
        if (notSingular)
        {
            results[i] = batch.shadingSamplers[i].evalBSDFContribs(
                batch.shadingFunctions[i], batch.globals[i], lightDir.value,
                lightInfo.l.emitComponent); // # we are doing that for each light source but it can be done once then filtered according to emit components

            if (lightInfo.l.needImportanceSampling())
            {
                const float misFactor = misPowerHeuristic(materialSample.pdf(), lightDir.pdf);
                outSamplingWeights[i] *= misFactor;
            }
        }
        else if (any(lightInfo.l.emitComponent & EmissionBits::Reflect) && scattering != ScatteringType::Straight) // Pure reflection on singular pdf
        {
            const auto contribIdx = materialSample.scatteringDirection == ScatteringEvent::Reflect ? BSDFContribs::SpecularReflect : BSDFContribs::SpecularTransmit;
            results[i][contribIdx] = materialSample.estimate;
        }
    }

    return results;
}

vector_view<BSDFContribs> OslRendererTask::evalShadingContribsAfterMaterialSampling(
    const LightSamplingResult * lightSamplingResults, float * outSamplingWeights, const EvalLightingInfo & lightInfo, const EvalBSSRDFLightingBatch & batch, ThreadMemoryPool & memPool) const
{
    auto results = make_view(memPool.lightingShadingContribs, batch.pathCount);

    for (size_t i = 0; i < batch.pathCount; ++i)
    {
        const auto & materialSample = batch.materialSamples[i];
        if (materialSample.pdf() <= 0.f)
            continue;
        const auto lightDir = lightSamplingResults[i].incidentDirection;
        results[i][BSDFContribs::DiffuseReflect] =
            evalCosineBSSRDFLobe(batch.globals[i].N, lightDir.value); // # we are doing that for each light source but it can be done once then filtered according to emit components
    }

    return results;
}

void OslRendererTask::lowLightThresholdFiltering(
    size_t pathCount, const OSL::ShaderGlobals * globals, const Color3 * lightingWeights, const LightSamplingResult * lightResults, const BSDFContribs * shadingContribs,
    float * outSamplingWeights, const Light & l) const
{
    for (size_t i = 0; i < pathCount; ++i)
    {
        Color3 lltPathContrib = lightingWeights[i] * outSamplingWeights[i] * shadingContribs[i].eval() * lightResults[i].incidentRadiance * l.evalColorHeuristic(globals[i]);
        if (reduceMax(lltPathContrib) < inputs.lowLightThreshold)
        {
            outSamplingWeights[i] = 0.f;
        }
    }
}

auto OslRendererTask::evalShadowsForLighting(
    const EvalLightingInfo & lightInfo, const EvalLightingBatch & batch, const LightSamplingResult * lightSamplingResults, const float * samplingWeights, EvalDirectLightingStats & stats,
    const ShadingContext & shadingCtx, ThreadMemoryPool & memPool) const -> vector_view<ShadowResult>
{
    HighResolutionTimerGuard g(&stats[EvalDirectLightingStats::Shadow]);

    if (!lightInfo.l.castsShadows)
        return make_view(memPool.shadowResults, batch.pathCount, { Color3(1.f), false });

    auto shadowRays = makeShadowRays(batch, lightSamplingResults, samplingWeights, lightInfo.l.rayTracedShadowBias, memPool);

    auto shadowVolumeInstBuffer = make_view(memPool.shadowVolumeInstBuffer, batch.pathCount * renderer->m_volumeInstancesCount);
    auto shadowVolumeStates = make_view(memPool.shadowVolumeStates, batch.pathCount);
    auto shadowVolumeTransmissionRandomNumbers = make_view(memPool.shadowVolumeTransmissionRandomNumbers, batch.pathCount);

    for (size_t j = 0; j < batch.pathCount; ++j)
    {
        const auto pathIdx = batch.pathIndexList ? batch.pathIndexList[j] : j;

        shadowVolumeStates[j] = VolumeInstList(shadowVolumeInstBuffer.data() + j * renderer->m_volumeInstancesCount, batch.pathVolumeStates[pathIdx]);
        shadowVolumeTransmissionRandomNumbers[j] = batch.paths[pathIdx].uVolumetric->uDistance;
    }

    return evalShadowFactors(
        std::move(shadowRays), std::move(shadowVolumeStates), std::move(shadowVolumeTransmissionRandomNumbers), shadingCtx, stats.intersectSolidShadow, lightInfo.perfInstances,
        lightInfo.lightStats, memPool);
}

vector_view<Ray> OslRendererTask::makeShadowRays(
    const EvalLightingBatch & batch, const LightSamplingResult * lightResults, const float * samplingWeight, float rayTracedShadowBias, ThreadMemoryPool & memPool) const
{
    auto shadowRays = make_view(memPool.shadowRays, batch.pathCount);

    for (size_t i = 0; i < batch.pathCount; ++i)
    {
        const auto & lightResult = lightResults[i];

        auto & shadowRay = shadowRays[i];
        shadowRay.tfar = -1.f; // Disable shadow ray by default

        if (samplingWeight[i] <= 0.f)
            continue;

        shadowRay.instID = Ray::INVALID_ID;
        shadowRay.primID = Ray::INVALID_ID;
        shadowRay.geomID = Ray::INVALID_ID;

        shadowRay.tnear = 0.f;
        shadowRay.mask = 3 << (Scene::INSTANCE_VISIBILITY_SHADOW * 2);

        const V3f N = faceForward(batch.globals[i].N, lightResult.incidentDirection.value);
        shadowRay.org = batch.globals[i].P + N * (rayTracedShadowBias + (batch.normalOffsets ? batch.normalOffsets[i] : 0.f));
        shadowRay.dir = lightResult.shadowDirection;
        shadowRay.time = batch.globals[i].time;
        shadowRay.tnear = batch.rayNears ? batch.rayNears[i] : 0.f;
        if (lightResult.distance != float(shn::inf)) // avoid rayTracedShadowBias & normalOffset to shift shadow ray
            shadowRay.tfar = distance(shadowRay.org, batch.globals[i].P + lightResult.distance * lightResult.shadowDirection);
        else
            shadowRay.tfar = lightResult.distance;
    }

    return shadowRays;
}

void OslRendererTask::accumLightingContribs(
    const EvalLightingInfo & lightInfo, const EvalLightingBatch & batch, const EvalLightingResult & evalLightingResult, Accumulator * accumulators, LightingSamplingStrategy samplingStrategy,
    ClosurePrimitive::Category closureCategory, LightingImportanceCache * ic, const LightingImportanceCacheDataStorage * icData, const ShadingContext & shadingCtx) const
{
    for (size_t i = 0; i < batch.pathCount; ++i)
    {
        const auto pathIdx = batch.pathIndexList ? batch.pathIndexList[i] : i;

        const float samplingWeight = evalLightingResult.samplingWeights[i];

        if (samplingWeight <= 0.f)
            continue;

        const auto lightContrib = [&]() {
            const auto radianceContrib = samplingWeight * evalLightingResult.lightSamplingResults[i].incidentRadiance;

            if (!ic)
            {
                const auto lightColor = [&]() {
                    if (!evalLightingResult.shadowResults[i].inShadow)
                    {
                        HighResolutionTimerGuard g(&lightInfo.lightStats.evalColorCost);
                        ++lightInfo.lightStats.evalColorCount;
                        return lightInfo.l.evalColor(
                                   batch.globals[i], evalLightingResult.lightSamplingResults[i],
                                   (samplingStrategy == LightingSamplingStrategy::Light) ? SamplingStrategy::Light : SamplingStrategy::Material, shadingCtx) *
                            evalLightingResult.shadowResults[i].transparency;
                    }
                    ++lightInfo.lightStats.occludedCount;
                    return lightInfo.l.shadowColor;
                }();

                const auto lightContrib = batch.lightingWeights[i] * radianceContrib * lightColor;

                addDirectLightingContribs(accumulators[batch.paths[pathIdx].accumulatorIndex], lightContrib, evalLightingResult.shadingContribs[i], closureCategory, samplingStrategy, nullptr);

                return lightContrib;
            }
            else
            {
                Color3 lightColor;
                {
                    HighResolutionTimerGuard g(&lightInfo.lightStats.evalColorCost);
                    lightColor = lightInfo.l.evalColor(batch.globals[i], evalLightingResult.lightSamplingResults[i], SamplingStrategy::Light, shadingCtx);
                    ++lightInfo.lightStats.evalColorCount;
                };
                Color3 fullColor = (!evalLightingResult.shadowResults[i].inShadow) ? lightColor * evalLightingResult.shadowResults[i].transparency : lightInfo.l.shadowColor;

                if (evalLightingResult.shadowResults[i].inShadow)
                    ++lightInfo.lightStats.occludedCount;

                const auto lightContrib = batch.lightingWeights[i] * radianceContrib * fullColor;
                addDirectLightingContribs(
                    accumulators[batch.paths[pathIdx].accumulatorIndex], lightContrib, evalLightingResult.shadingContribs[i], closureCategory, samplingStrategy,
                    (LightingImportanceCache::DistributionNames::Enum *) &icData->contribIndices[i]);

                const auto icLightContrib = batch.lightingWeights[i] * evalLightingResult.shadingContribs[i].eval() * radianceContrib;

                // Update importance cache with contributions
                ic->addLightContribs(
                    icData->distribIdxBuffer[pathIdx], lightInfo.lightIdx, icLightContrib, fullColor, lightColor, (LightingImportanceCache::DistributionNames::Enum) icData->contribIndices[i]);

                return lightContrib;
            }
        }();

        // Irradiance and shadows, #todo dirty as fuck.
        if (closureCategory != ClosurePrimitive::Volume)
        {
            const bool writeShadows = (batch.paths[pathIdx].depth == 0) && lightInfo.l.castsShadows;
            const auto irradianceDirect = clamp(dot(batch.globals[i].N, evalLightingResult.lightSamplingResults[i].incidentDirection.value), 0.f, 1.f) * lightContrib;

            accumulators[batch.paths[pathIdx].accumulatorIndex].push().move(LPE::None, LPE::None).move(LPE::Light, LPE::Irradiance).accum(irradianceDirect);

            if (isDirect(batch.paths[pathIdx]))
                accumulators[batch.paths[pathIdx].accumulatorIndex].irradianceDirect += irradianceDirect;

            if (writeShadows)
            {
                accumulators[batch.paths[pathIdx].accumulatorIndex].push().move(LPE::None, LPE::None).move(LPE::Light, LPE::Shadows).accum(lightContrib);
            }
        }
    }
}

void OslRendererTask::evalBackgroundEnvmap(const Ray * rays, const Path * paths, Accumulator * accumulators, const ShadingContext & shadingCtx, size_t count) const
{
    // get the envmaps
    int32_t envmapLightCount = renderer->m_lights.visibleLightCount(LightType_EnvMap);
    if (envmapLightCount == 0)
        return;

    const Light * const * envmapLighs = renderer->m_lights.visibleLights(LightType_EnvMap);

    //  eval background on missed ray
    for (int32_t i = 0; i < count; ++i)
    {
        if (isDirect(paths[i]) && rays[i].instID == Ray::INVALID_ID) // Background in only visible in direct rays (camera, transparency or volume)
        {
            Color3 bgDirectLighting(0.f);
            for (int32_t l = 0; l < envmapLightCount; ++l)
            {
                const EnvMapLight * light = (const EnvMapLight *) envmapLighs[l];
                if (light->background)
                {
                    OSL::ShaderGlobals globals;
                    memset(&globals, 0, sizeof(globals));
                    globals.N = rays[i].dir; // N is use for SHVector mode. For background evaluation, N and dir are the same
                    LightSamplingResult lightSamplingResult;
                    lightSamplingResult.incidentDirection.value = rays[i].dir;
                    lightSamplingResult.incidentDirection.pdf = 1.f;
                    lightSamplingResult.distance = inf;
                    lightSamplingResult.texcoord = sphereUV(multDirMatrix(light->worldToLocal, rays[i].dir));
                    lightSamplingResult.shadowDirection = lightSamplingResult.incidentDirection.value;
                    bgDirectLighting += light->evalColor(globals, lightSamplingResult, SamplingStrategy::Material, shadingCtx) * light->intensity;
                }
            }

            // if background is black, don't override the alpha
            if (!isBlack(bgDirectLighting))
            {
                auto & accumulator = accumulators[paths[i].accumulatorIndex];
                accumulator.push().move(LPE::Light, LPE::Background).accum(paths[i].weight * bgDirectLighting);
            }
        }
    }
}

inline int32_t updatePathDepth(int32_t originalDepth, bool ignoreStraight, const BxDFDirSample & materialSample)
{
    if (ignoreStraight && materialSample.scatteringType == ScatteringType::Straight)
        return originalDepth;
    return originalDepth + 1;
}

inline OslRendererTask::PathSamplingState scatteringTypeToPathSamplingState(ScatteringType scatteringType)
{
    switch (scatteringType)
    {
    default:
        break;
    case ScatteringType::Diffuse:
    case ScatteringType::Glossy:
        return OslRendererTask::PathSamplingState::DiffuseOrGlossy;
    case ScatteringType::Straight:
        return OslRendererTask::PathSamplingState::Straight;
    case ScatteringType::Specular:
        return OslRendererTask::PathSamplingState::Specular;
    }
    return OslRendererTask::PathSamplingState::Absorption;
}

auto OslRendererTask::sampleNextPathDirections(
    size_t count, const Path * paths, const OSL::ShaderGlobals * globals, const SurfaceShadingFunction * surfaceShadingFunctions, const OSL::ShaderGlobals * ssGlobals, uint64_t * perfCounters,
    ShadingMemoryPool & bouncingMemPool, ThreadMemoryPool & memPool) const -> vector_view<PathDirection>
{
    vector_view<PathDirection> directionSamples = make_view(memPool.directionSamples);

    for (size_t j = 0; j < count; ++j)
    {
        //// Build the sampler taking into account the maximal number of bounce specified for each scattering component
        SurfaceShadingFunction::Sampler bouncingSampler(surfaceShadingFunctions[j], globals[j], paths[j].weight, &paths[j].limits, bouncingMemPool);

        // As long as we have a straight path, we split paths on transparency in order to avoid noise through transparent objects
        if (straightOnly(paths[j].samplingState))
        {
            if (bouncingSampler.transparencySamplingProbability() < 1.f)
            {
                const auto materialSample = bouncingSampler.sampleOpaqueDirection(
                    surfaceShadingFunctions[j], globals[j], ssGlobals[j], paths[j].uScatter->uClosureComponent, paths[j].uScatter->uDir[0], paths[j].uScatter->uDir[1]);
                directionSamples.emplace_back(materialSample, j);
            }

            if (bouncingSampler.transparencySamplingProbability() > 0.f)
            {
                const auto transparentSample = bouncingSampler.sampleTransparency(surfaceShadingFunctions[j], globals[j]);
                directionSamples.emplace_back(transparentSample, j);
            }
        }
        else
        {
            const auto materialSample = bouncingSampler.sampleDirection(
                surfaceShadingFunctions[j], globals[j], ssGlobals[j], paths[j].uScatter->uClosureComponent, paths[j].uScatter->uDir[0], paths[j].uScatter->uDir[1]);
            directionSamples.emplace_back(materialSample, j);
        }

        bouncingMemPool.clear();
    }

    return directionSamples;
}

void OslRendererTask::extendPath(
    const Path & path, const Ray & ray, const RayDifferentials & rayDiff, uint32_t rayType, const BxDFDirSample & materialSample, const Color3 & bssrdfWeight,
    const SurfaceShadingFunction & surfaceShadingFunction, const OSL::ShaderGlobals & sGlobals, const OSL::ShaderGlobals & ssGlobals, float normalOffset, float rayNear, Path & newPath,
    Ray & newRay, RayDifferentials & newRayDiff, uint32_t & newRayType) const
{
    const float blackholeFactor = inputs.scene->isBlackholeInstance(ray.instID, isDirect(path) ? Scene::INSTANCE_VISIBILITY_PRIMARY : Scene::INSTANCE_VISIBILITY_SECONDARY) ? 1.f : 0.f;

    if (blackholeFactor > 0.f && materialSample.scatteringType != ScatteringType::Straight)
    {
        newPath.samplingState = PathSamplingState::Blackhole; // Path will be absorbed
        return;
    }

    Color3 materialWeight = materialSample.estimate.weight();

    if (materialSample.closureCategory == ClosurePrimitive::BSSRDF)
        materialWeight *= bssrdfWeight;

    // Update paths depth
    newPath.depth = updatePathDepth(path.depth, inputs.ignoreStraightBounces, materialSample);

    // Update path sampling state
    if ((path.samplingState == PathSamplingState::Camera) || (straightOnly(path.samplingState) && materialSample.scatteringType != ScatteringType::Straight) ||
        (specularOnly(path.samplingState) && materialSample.scatteringType != ScatteringType::Specular && materialSample.scatteringType != ScatteringType::Straight))
    {
        newPath.samplingState = scatteringTypeToPathSamplingState(materialSample.scatteringType);
    }

    if (straightOnly(newPath.samplingState))
    {
        newPath.transparencyWeight *= surfaceShadingFunction.transparency();
        newPath.volumeTransmittance *= surfaceShadingFunction.transparency();
    }
    else
    {
        newPath.transparencyWeight = Color3(0.f);
    }

    // Update paths weight
    newPath.weight = path.weight * materialWeight;

    newPath.closureCategory = materialSample.closureCategory;
    newPath.scatteringType = materialSample.scatteringType;
    newPath.scatteringEvent = materialSample.scatteringDirection;

    // ignore extinguishes paths
    if (materialSample.pdf() < float(ulp) || isBlack(materialSample.weight()))
    {
        newPath.samplingState = PathSamplingState::Absorption;
        return;
    }

    // ignore out of bounces paths
    if (newPath.depth > inputs.maxBounces)
        return;

    // update bounces token
    newPath.limits = path.limits;
    updateBounceLimits(newPath.limits, materialSample.scatteringType, materialSample.scatteringDirection, materialSample.closureCategory);

    const OSL::ShaderGlobals & globals = (materialSample.closureCategory == ClosurePrimitive::BSSRDF) ? ssGlobals : sGlobals;
    newPath.flipHandedness = path.flipHandedness ^ (globals.Ng.dot(materialSample.direction.val()) > 0);

    const V3f N = (materialSample.scatteringDirection == ScatteringEvent::Reflect) ? globals.Ng : -globals.Ng;
    newRay.org = globals.P + N * (normalOffset + inputs.rayTracedIndirectBias);
    newRay.dir = materialSample.direction.val();
    newRay.tnear = rayNear;
    newRay.tfar = inf;

    newRayDiff = { globals.dPdx, globals.dPdy, materialSample.direction.dx(), materialSample.direction.dy() };
    newRayType = updateRayType(rayType, materialSample.scatteringType, materialSample.scatteringDirection, *renderer);
}

namespace
{
V3f sampleToonOutline(const V3f & c, float r, const V3f & x, float uStrategy, float u1, float u2)
{
    float pdf = 0.f;

    const float r2 = r * r;
    const float sqrDist = sqrLength(c - x);
    if (sqrDist < r2)
        return V3f(0.f); // Inside toon outline ??

    const float cmax2 = 1 - r2 / sqrDist;
    const float cmax = cmax2 > 0 ? sqrtf(cmax2) : 0;

    V3f sw = normalize(c - x), su, sv;
    makeOrthonormals(sw, su, sv);

    const float hemisphereRatio = max(1.f - cmax, 0.f);

    if (uStrategy >= hemisphereRatio)
    {
        // Sample with area based strategy
        const auto diskSample = r * sampleUniformDisk(u1, u2);
        const auto diskPoint = c + su * diskSample.x + sv * diskSample.y;
        const auto toDisk = diskPoint - x;
        const auto d = length(toDisk);
        const auto d2 = d * d;
        const auto direction = toDisk / d;

        return direction;
    }

    // Sample with solid angle based strategy
    float cos_a = 1 - u1 + u1 * cmax;
    float sin_a = sqrtf(1 - cos_a * cos_a);
    float phi = F2PI * u2;
    return V3f(su * (cosf(phi) * sin_a) + sv * (sinf(phi) * sin_a) + sw * cos_a).normalize();
}
} // namespace

bool OslRendererTask::executeToonOutlineShader(ToonOutlineContribs & toonOutline, Scene::Id instId, OSL::ShaderGlobals & globals, const ShadingContext & ctx) const
{
    shn::OslRenderer::Id shaderId = renderer->getShadingTreeIdFromInstanceId(instId, 0, OslRendererImpl::Assignment::ToonOutline);
    if (shaderId == -1)
    {
        if (renderer->m_defaultToonOutlineShaderId == -1)
            return false;
        shaderId = renderer->m_defaultToonOutlineShaderId;
    }

    auto shadingSystem = renderer->s_shadingSystem.get();

    globals.Ci = nullptr; // #todo: maybe not the best idea, as Ci could be used outside (not currently the case, but might evolve inpredictably)
    evalShader(ctx, shaderId, globals);

    const auto cl = (const ToonOutlineDataClosure *) globals.Ci->as_comp()->data();

    toonOutline.outerColor = cl->outerOutlineColor;
    toonOutline.innerColor = cl->innerOutlineColor;
    toonOutline.cornerColor = cl->cornerOutlineColor;

    return true;
}

void OslRendererTask::computeToonOutline(ToonOutlineContribs & toonOutline, const OSL::ShaderGlobals & globals, const Ray & ray, RandomState & samplerState) const
{
    const auto cl = (const ToonOutlineDataClosure *) globals.Ci->as_comp()->data();

    const auto outerIncidentAngleThreshold = cosf(deg2rad(cl->outerIncidentAngleThreshold));
    const auto innerIncidentAngleThreshold = cosf(deg2rad(cl->innerIncidentAngleThreshold));
    const auto cornerAngleThreshold = cosf(deg2rad(cl->cornerAngleThreshold));

    const size_t outlineSampleCountX = 4;
    const size_t outlineSampleCount = outlineSampleCountX * outlineSampleCountX;

    const auto rayOrg = ray.org;
    const auto rayDst = globals.P;
    const auto time = ray.time;
    const auto instID = ray.instID;

    const auto traceOutlineRay = [rayOrg, rayDst, time, instID, this](float thickness, float uStrategy, float u1, float u2) -> Ray {
        const auto outlineDirection = sampleToonOutline(rayDst, thickness, rayOrg, uStrategy, u1, u2);
        Ray outlineRay(rayOrg, outlineDirection);
        outlineRay.time = time;
        inputs.scene->intersect(outlineRay, instID);
        return outlineRay;
    };

    const auto globalsFromHit = [this](const Ray & ray, OSL::ShaderGlobals & sg) {
        memset(&sg, 0, sizeof(sg));
        RayDifferentials rds;
        float normalOffset, rayNear;
        inputs.scene->globalsFromHit(&sg, ray, rds, false, normalOffset, rayNear);
    };

    toonOutline.weights = V3f(0.f);

    for (size_t j = 0; j < outlineSampleCount; ++j)
    {
        const auto sx = (getFloat(samplerState) + j % outlineSampleCountX) / float(outlineSampleCountX);
        const auto sy = (getFloat(samplerState) + j / outlineSampleCountX) / float(outlineSampleCountX);

        if (cl->outerOutline)
        {
            const auto outlineRay = traceOutlineRay(cl->outerThickness, getFloat(samplerState), sx, sy);

            if (outlineRay.instID == Ray::INVALID_ID)
            {
                toonOutline.weights[ToonOutlineContribs::OUTER] += 1.f;
            }
            else if (cl->outerDistanceThreshold > 0.f && ray.tfar < outlineRay.tfar)
            {
                OSL::ShaderGlobals sg;
                globalsFromHit(outlineRay, sg);

                const auto cosI = dot(-outlineRay.dir, sg.N);
                const auto dist = distance(rayDst, sg.P);

                if (cosI > outerIncidentAngleThreshold && dist > cl->outerDistanceThreshold)
                    toonOutline.weights[ToonOutlineContribs::OUTER] += 1.f;
            }
        }

        if (cl->innerOutline && toonOutline.weights[ToonOutlineContribs::OUTER] == 0.f && cl->innerDistanceThreshold > 0.f)
        {
            const auto outlineRay = traceOutlineRay(cl->innerThickness, getFloat(samplerState), sx, sy);

            if (outlineRay.instID != Ray::INVALID_ID && ray.tfar < outlineRay.tfar)
            {
                OSL::ShaderGlobals sg;
                globalsFromHit(outlineRay, sg);

                const auto cosI = dot(-outlineRay.dir, sg.N);
                const auto dist = distance(rayDst, sg.P);

                if (cosI > innerIncidentAngleThreshold && dist > cl->innerDistanceThreshold)
                    toonOutline.weights[ToonOutlineContribs::INNER] += 1.f;
            }
        }

        if (cl->cornerOutline && toonOutline.weights[ToonOutlineContribs::OUTER] == 0.f && toonOutline.weights[ToonOutlineContribs::INNER] == 0.f)
        {
            const auto outlineRay = traceOutlineRay(cl->cornerThickness, getFloat(samplerState), sx, sy);

            if (outlineRay.instID != Ray::INVALID_ID)
            {
                OSL::ShaderGlobals sg;
                globalsFromHit(outlineRay, sg);

                const auto cosA = dot(globals.N, sg.N);
                const auto dist = distance(rayDst, sg.P);
                if (cosA < cornerAngleThreshold && dist < cl->innerDistanceThreshold)
                    toonOutline.weights[ToonOutlineContribs::CORNER] += 1.f;
            }
        }
    }

    const float rcpOutlineSampleCount = 1.f / outlineSampleCount;
    toonOutline.weights *= rcpOutlineSampleCount;
    toonOutline.applyLighting = cl->applyLighting != 0;
}

void OslRendererTask::motionVectorsIntegration(
    size_t count, const RayBatch & scatteringRays, float shutterOffset, const OSL::ShaderGlobals * globals, const M44f & startWorldToCamera, const M44f & endWorldToCamera,
    const Camera & camera, const M44f & camToScreen, Accumulator * accumulators, const Path * path) const
{
    // #todo: optimize this by testing first the LPEs ?
    for (size_t i = 0; i < count; ++i)
    {
        if (scatteringRays.rays()[i].instID == Ray::INVALID_ID)
            continue; // Let motion vector to black on the background

        const float localTime = shutterOffset;
        const V3f wsCurPt = globals[i].P;
        const V3f wsDpdt = globals[i].dPdtime;
        const V3f wsStartPt = wsCurPt - localTime * wsDpdt;
        const V3f wsEndPt = wsCurPt + (1.f - localTime) * wsDpdt;

        const V3f csStartPt = multVecMatrix(startWorldToCamera, wsStartPt);
        V3f csEndPt = multVecMatrix(endWorldToCamera, wsEndPt);
        // clamp csEndPt to the camera near plane to avoid face/back motion vector artifact
        const float cameraNear = -camera.nearPlane();
        if (csEndPt.z > cameraNear)
        {
            const V3f csDpdt = csEndPt - csStartPt;
            const V3f dir = normalize(csDpdt);
            const float alpha = (cameraNear - csStartPt.z) / dir.z;
            csEndPt.x = csStartPt.x + alpha * dir.x;
            csEndPt.y = csStartPt.y + alpha * dir.y;
            csEndPt.z = cameraNear;
        }

        const V3f ssStartPt = multVecMatrix(camToScreen, csStartPt);
        const V3f ssEndPt = multVecMatrix(camToScreen, csEndPt);
        const V3f ssDpdt = ssEndPt - ssStartPt;

        auto & accumulator = accumulators[path[i].accumulatorIndex];

        accumulator.push().move(LPE::Geometry, LPE::MotionVector).accum(ssDpdt);

        // convert in pixel unit
        V3f pxDpdt(ssDpdt.x * inputs.width * 0.5f, ssDpdt.y * inputs.height * 0.5f, 0.f);

        // clamp pixel motion vector length to 2^16 to stay in F16 limit
        float pxDpdtNorm = length(pxDpdt);
        if (pxDpdtNorm > 0.f)
        {
            V3f pxDpdtDir = pxDpdt / pxDpdtNorm;
            pxDpdt = min(pxDpdtNorm, F2_POW_16) * pxDpdtDir;
        }

        accumulator.push().move(LPE::Geometry, LPE::MotionVectorPixel).accum(pxDpdt);
    }
}

auto OslRendererTask::bssrdfPointSampling(
    size_t count, const OSL::ShaderGlobals * globals, const SurfaceShadingFunction * shadingFunctions, const SurfaceShadingFunction::Sampler * shadingSamplers, const RayBatch & scatteringRays,
    const Path * paths, size_t * perfCounters, ThreadMemoryPool & memPool) const -> BSSRDFPointSamplingResult
{
    HighResolutionTimerGuard g(&perfCounters[STAT_MATERIAL_SAMPLE_BSSRDF]);

    const size_t bssrdfCount = std::count_if(shadingFunctions, shadingFunctions + count, [](const auto & f) { return f.bssrdfCount() > 0; });

    BSSRDFPointSamplingResult result{ bssrdfCount, memPool };

    BSSRDFSamplingDataStorage bssrdfSamplingData;
    for (size_t pathIdx = 0, bssrdfIdx = 0; pathIdx < count; ++pathIdx)
    {
        const auto & path = paths[pathIdx];
        if (shadingFunctions[pathIdx].bssrdfCount() > 0)
        {
            const auto point = sampleBSSRDFPoint(
                { globals[pathIdx], shadingFunctions[pathIdx], shadingSamplers[pathIdx], scatteringRays.rays()[pathIdx], scatteringRays.differentials()[pathIdx] },
                { path.uBSSRDF->uStrategy, path.uBSSRDF->uRotation, toV2f(path.uBSSRDF->uPoint) }, bssrdfSamplingData, *inputs.scene, renderer->m_bssrdfMISHeuristic,
                renderer->m_bssrdfResampling);

            result.pathIndexList[bssrdfIdx] = pathIdx;
            result.globals[bssrdfIdx] = point.globals;
            result.weights[bssrdfIdx] = Color3(point.samplingWeight) * shadingFunctions[pathIdx].evalBSSRDFFresnelDiffusion(globals[pathIdx], distance(globals[pathIdx].P, point.globals.P));

            ++bssrdfIdx;
        }
    }
    return result;
}

OslRendererTask::VolumeDensitySamplingResult::VolumeDensitySamplingResult(size_t pathIndexCount, size_t lightingPathIndexCount, ThreadMemoryPool & memPool):
    transmittanceToSurface(make_view(memPool.transmittanceToSurface, pathIndexCount, Color3(0.f))), globals(make_view(memPool.volumeDensityGlobals, lightingPathIndexCount)),
    volumeShadingFunctions(make_view(memPool.volumeDensityShadingFunctions, lightingPathIndexCount)), instIds(make_view(memPool.volumeDensityInstIds, lightingPathIndexCount, -1)),
    inversePdfs(make_view(memPool.volumeDensityInversePdfs, lightingPathIndexCount, 0.f)), sampledDistances(make_view(memPool.volumeDensitySampledDistances, lightingPathIndexCount, 0.f)),
    transmittanceToVolumePoint(make_view(memPool.volumeDensityTransmittanceToVolumePoint, lightingPathIndexCount, Color3(0.f)))
{
}

OslRendererTask::VolumeList::VolumeList(ThreadMemoryPool & memPool):
    volumeInfoList(make_view(memPool.volumeInfoList)), pathIndexLists(make_view(memPool.volumePathIndexList)), lightingPathIndexLists(make_view(memPool.volumeLightingPathIndexList))
{
}

OslRendererTask::ThreadMemoryPool::ThreadMemoryPool()
{
    pixelInfo.reserve(TILE_PIXEL_COUNT);
    adaptiveStats.reserve(TILE_PIXEL_COUNT);

    paths.reserve(TILE_PIXEL_COUNT);
    splittedPaths.reserve(TILE_PIXEL_COUNT);
    finishedPaths.reserve(TILE_PIXEL_COUNT);

    pathVolumeStates.reserve(TILE_PIXEL_COUNT);
    splittedPathVolumeStates.reserve(TILE_PIXEL_COUNT);

    pathSplitCount.reserve(TILE_PIXEL_COUNT);
    directionSamples.reserve(TILE_PIXEL_COUNT);

    pathTransmittance.reserve(TILE_PIXEL_COUNT);
    pathTransparencyAccum.reserve(TILE_PIXEL_COUNT);

    pathAccumulators.reserve(TILE_PIXEL_COUNT);
    scatteringRays.reserve(TILE_PIXEL_COUNT);
    scatteringRaysDifferentials.reserve(TILE_PIXEL_COUNT);
    scatteringRayTypes.reserve(TILE_PIXEL_COUNT);
    volumeTestRays.reserve(TILE_PIXEL_COUNT);

    splittedRays.reserve(TILE_PIXEL_COUNT);
    splittedRaysDifferentials.reserve(TILE_PIXEL_COUNT);
    splittedRayTypes.reserve(TILE_PIXEL_COUNT);

    shadowRays.reserve(TILE_PIXEL_COUNT);
    pathIdxOfShadowRays.reserve(TILE_PIXEL_COUNT);

    toonOutlines.reserve(TILE_PIXEL_COUNT);
    bindingOutputs.reserve(TILE_PIXEL_COUNT);
    internalOutputs.reserve(TILE_PIXEL_COUNT);
    perPixelTransparency.reserve(TILE_PIXEL_COUNT);
    perPixelHasSample.reserve(TILE_PIXEL_COUNT);

    globals.reserve(TILE_PIXEL_COUNT);
    instIds.reserve(TILE_PIXEL_COUNT);
    normalOffsets.reserve(TILE_PIXEL_COUNT);
    rayNears.reserve(TILE_PIXEL_COUNT);
    renderStates.reserve(TILE_PIXEL_COUNT);

    surfaceShadingFunctions.reserve(TILE_PIXEL_COUNT);
    surfaceShadingSamplers.reserve(TILE_PIXEL_COUNT);
    shadingNormals.reserve(TILE_PIXEL_COUNT);

    ssGlobals.reserve(TILE_PIXEL_COUNT);
    ssWeights.reserve(TILE_PIXEL_COUNT);

    lightingBsdfDirSamples.reserve(TILE_PIXEL_COUNT);
    lightingBssrdfDirSamples.reserve(TILE_PIXEL_COUNT);

    lightingWeights.reserve(TILE_PIXEL_COUNT);

    reindexedGlobals.reserve(TILE_PIXEL_COUNT);
    reindexedInstIds.reserve(TILE_PIXEL_COUNT);
    reindexedSurfaceShadingFunctions.reserve(TILE_PIXEL_COUNT);
    reindexedSurfaceShadingSamplers.reserve(TILE_PIXEL_COUNT);
    reindexedNormalOffsets.reserve(TILE_PIXEL_COUNT);
    reindexedRayNears.reserve(TILE_PIXEL_COUNT);
    reindexedLightingWeights.reserve(TILE_PIXEL_COUNT);

    shadowVolumeInstBuffer.reserve(TILE_PIXEL_COUNT);
    shadowVolumeStates.reserve(TILE_PIXEL_COUNT);
    shadowRayLength.reserve(TILE_PIXEL_COUNT);
    shadowVolumeTransmissionRandomNumbers.reserve(TILE_PIXEL_COUNT);
    shadowFactors.reserve(TILE_PIXEL_COUNT);
    shadowResults.reserve(TILE_PIXEL_COUNT);
    shadowTransparencyFactors.reserve(TILE_PIXEL_COUNT);
    shadowIndexList.reserve(TILE_PIXEL_COUNT);

    bssrdfPathIndexList.reserve(TILE_PIXEL_COUNT);
    bssrdfGlobals.reserve(TILE_PIXEL_COUNT);
    bssrdfWeights.reserve(TILE_PIXEL_COUNT);

    volumeInfoList.reserve(TILE_PIXEL_COUNT);
    volumePathIndexList.reserve(TILE_PIXEL_COUNT);

    transmittanceToSurface.reserve(TILE_PIXEL_COUNT);
    volumeDensityGlobals.reserve(TILE_PIXEL_COUNT);
    volumeDensityShadingFunctions.reserve(TILE_PIXEL_COUNT);
    volumeDensityInstIds.reserve(TILE_PIXEL_COUNT);
    volumeDensityInversePdfs.reserve(TILE_PIXEL_COUNT);
    volumeDensitySampledDistances.reserve(TILE_PIXEL_COUNT);
    volumeDensityTransmittanceToVolumePoint.reserve(TILE_PIXEL_COUNT);

    volumeEasGlobals.reserve(TILE_PIXEL_COUNT);
    volumeEasShadingFunctions.reserve(TILE_PIXEL_COUNT);
}

OslRendererTask::BSSRDFPointSamplingResult::BSSRDFPointSamplingResult(size_t count, ThreadMemoryPool & memPool):
    pathIndexList{ make_view(memPool.bssrdfPathIndexList, count) }, globals{ make_view(memPool.bssrdfGlobals, count) }, weights{ make_view(memPool.bssrdfWeights, count) }
{
}

OslRendererTask::RendererInputs::RendererInputs(
    size_t startx, size_t endx, size_t starty, size_t endy, size_t startSample, size_t endSample, const Attributes & rendererAttributes, const Binding & binding, SceneImpl * scene,
    Scene::Id cameraId, const Lights * lights):
    scene{ scene },
    lights{ lights }, startx{ startx }, endx{ endx }, starty{ starty }, endy{ endy }, width{ binding.width() }, height{ binding.height() },
    tilePattern{ rendererAttributes.getString(RENDERER_ATTR_NAMES[R_ATTR_TILEPATTERN]) }, startSample{ startSample }, endSample{ endSample }, sampleCount{ endSample - startSample },
    adaptiveMinSamples{ (size_t) rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_ADAPTIVEMINSAMPLES], std::numeric_limits<int32_t>::max()) },
    adaptiveStdDevThreshold{ rendererAttributes.get<float>(RENDERER_ATTR_NAMES[R_ATTR_ADAPTIVEVARIANCE], 0.f) }, adaptiveSamplingEnabled{ adaptiveStdDevThreshold > 0.f &&
                                                                                                                                          sampleCount > adaptiveMinSamples },
    nanPolicy{ rendererAttributes.get<OslRendererImpl::NaNHandlingPolicy>(RENDERER_ATTR_NAMES[R_ATTR_NANHANDLINGPOLICY], OslRendererImpl::NaNHandlingPolicy::NaNHandlingPolicy_Drop) },
    rayTracedIndirectBias{ rendererAttributes.get<float>(RENDERER_ATTR_NAMES[R_ATTR_RAYTRACEDINDIRECTBIAS], 0.f) },
    ignoreStraightBounces{ (rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_IGNORESTRAIGHTBOUNCES], 0) == 1) },
    toonOutlineMinSampleCount{ (size_t) rendererAttributes.get<int>(SHN_ATTR_RENDER_TOONOUTLINEMINSAMPLES, 16) }, needLighting{ binding.needLighting() },
    needBouncing{ binding.needBouncing() }, maxBounces{ (size_t) rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_MAXBOUNCES], DEFAULT_MAX_BOUNCES) },
    depthNear{ rendererAttributes.get<float>(RENDERER_ATTR_NAMES[R_ATTR_DEPTHNEAR], 0.f) }, depthFar{ rendererAttributes.get<float>(RENDERER_ATTR_NAMES[R_ATTR_DEPTHFAR], 100.f) },
    shutterOffset{ rendererAttributes.get<float>(RENDERER_ATTR_NAMES[R_ATTR_SHUTTEROFFSET], 0.f) }, camera{ scene->getCamera(cameraId, shutterOffset) }, cameraId{ cameraId },
    lightICProbaAlpha{ rendererAttributes.get<float>(RENDERER_ATTR_NAMES[R_ATTR_LIGHTICPROBAALPHA], LightingImportanceCache::DefaultProbaAlpha()) },
    skipVolumeInclusionTest{ rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_VOLUMESKIPINCLUSIONTEST], 0) == 1 },
    occlusionAngleRadians{ deg2rad(rendererAttributes.get<float>(RENDERER_ATTR_NAMES[R_ATTR_OCCANGLE], 45.f)) }, occlusionDistance{ rendererAttributes.get<float>(
                                                                                                                     RENDERER_ATTR_NAMES[R_ATTR_OCCDISTANCE], 1.f) },
    maxShadowBounces{ (size_t) rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESSHADOW], DEFAULT_SHADOW_BOUNCES) + 1 },
    lowLightThreshold{ rendererAttributes.get<float>(RENDERER_ATTR_NAMES[R_ATTR_LOWLIGHTTHRESHOLD], OslRendererImpl::LOW_LIGHT_THRESHOLD) }, pixelSampleClamp{
        rendererAttributes.get<float>(RENDERER_ATTR_NAMES[R_ATTR_PIXELSAMPLECLAMP], std::numeric_limits<float>::max())
    }
{
    scene->getCameraShutter(cameraId, &openTime, &closeTime);

    const auto screenToCam = scene->getScreenToCameraTransform(camera->id());
    float worldTime0;
    const auto camToWorld0 = scene->getCameraToWorldTransform(camera->id(), 0.f, worldTime0);
    float worldTime1;
    const auto camToWorld1 = scene->getCameraToWorldTransform(camera->id(), 1.f, worldTime1);

    startWorldToCamera = inverse(camToWorld0);
    endWorldToCamera = inverse(camToWorld1);
    camToScreen = inverse(screenToCam);

    bouncesLimits.d[RayDepth::TOTAL] = rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_MAXBOUNCES], DEFAULT_MAX_BOUNCES);
    bouncesLimits.d[RayDepth::DIFFUSE] = rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESDIFFUSE], DEFAULT_DIFFUSE_BOUNCES);
    bouncesLimits.d[RayDepth::GLOSSY] = rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESGLOSSY], DEFAULT_GLOSSY_BOUNCES);
    bouncesLimits.d[RayDepth::REFLECTION] = rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESREFLECTION], DEFAULT_REFLECTION_BOUNCES);
    bouncesLimits.d[RayDepth::REFRACTION] = rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESREFRACTION], DEFAULT_REFRACTION_BOUNCES);
    bouncesLimits.d[RayDepth::TRANSPARENCY] = rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESTRANSPARENCY], DEFAULT_TRANSPARENCY_BOUNCES);
    bouncesLimits.d[RayDepth::VOLUME] = rendererAttributes.get<int32_t>(RENDERER_ATTR_NAMES[R_ATTR_BOUNCESVOLUME], DEFAULT_VOLUME_BOUNCES);
}

OslRendererTask::OslRendererTask(
    const OslRendererImpl * renderer, const Attributes & rendererAttributes, Scene * scene, const Lights * lights, Scene::Id cameraId, int32_t startx, int32_t endx, int32_t starty,
    int32_t endy, int32_t startSample, int32_t endSample, RendererOutputs & outputs):
    renderer(renderer),
    inputs(startx, endx, starty, endy, startSample, endSample, rendererAttributes, outputs.binding, scene->impl(), cameraId, lights),
    outputs(outputs), m_internalAutomata{ compile({ std::make_pair(getOutputPreset(ustring("beauty"))->pathExpression.c_str(), 0) }) }
{
    tileID = 0;

    // Just in case... :
    for (auto & stat: outputs.performanceCounters)
    {
        stat = 0;
    }
    outputs.lightStatistics.resize(lights->visibleLightCount());
}

OslRendererTask::~OslRendererTask()
{
    done();
}

// #todo this function only merge statistics to the renderer, maybe it should be done elsewhere
void OslRendererTask::done()
{
    auto lock = outputs.lock();

#ifndef DISABLE_PERFORMANCE_COUNTERS
    // Task (i.e. sample) completed: update global statistics
    for (int i = 0; i != STAT_MAX; ++i)
        renderer->m_perfCounters[i].i += outputs.performanceCounters[i];

    renderer->m_lightStatistics = std::move(outputs.lightStatistics);

    for (const auto & icTileStat: outputs.lightImportanceCacheStatistics)
    {
        const auto tileCoords = V2i(icTileStat["tileCoords"][0], icTileStat["tileCoords"][1]);
        renderer->m_lightImportanceCacheTileStatistics["tile_" + to_string(tileCoords.x) + "_" + to_string(tileCoords.y)] = icTileStat;
    }

    if (renderer->isPerformanceCounterEnabled(OslRendererImpl::PerformanceCounter_Instance))
    {
        std::unique_lock<std::mutex> lock(renderer->m_performanceInstancesMutex);
        for (const auto & item: outputs.performanceInstances)
        {
            renderer->m_performanceInstances[item.first].first.shadingCount += item.second.first.shadingCount;
            renderer->m_performanceInstances[item.first].first.shadingDurationUS += item.second.first.shadingDurationUS;
            renderer->m_performanceInstances[item.first].first.shadowRayCount += item.second.first.shadowRayCount;

            renderer->m_performanceInstances[item.first].second.shadingCount += item.second.second.shadingCount;
            renderer->m_performanceInstances[item.first].second.shadingDurationUS += item.second.second.shadingDurationUS;
            renderer->m_performanceInstances[item.first].second.shadowRayCount += item.second.second.shadowRayCount;
        }
    }
#endif

    if (--outputs.sync._used == 0) // _used is the number of samples to compute, so this piece of code is called by a thread after the last sample has been processed
    {
#ifndef DISABLE_PERFORMANCE_COUNTERS
        // rendering done: update the statistics
        TimerStruct end;
        GetHighResolutionTime(&end);
        float renderTimeSec = (float) ConvertTimeDifferenceToSec(&end, &outputs.beginTime);

        const uint64_t computedSampleCount = renderer->m_perfCounters[STAT_SAMPLES_COMPUTED].i;
        const float computedSampleCountf = (float) computedSampleCount;

        uint64_t totalRayCount = 0;
        for (size_t i = STAT_CENTRALRAYS_CAMERA; i <= STAT_SHADOWRAYS_MATERIAL; ++i)
            totalRayCount += renderer->m_perfCounters[i].i;

        renderer->m_perfCounters[STAT_DURATION].f = renderTimeSec;
        renderer->m_perfCounters[STAT_RAYS_HZ].f = totalRayCount / (float) renderTimeSec;
        renderer->m_perfCounters[STAT_SAMPLES_HZ].f = computedSampleCountf / renderTimeSec;
        renderer->m_perfCounters[STAT_BOUNCES_AVG].f = renderer->m_perfCounters[STAT_BOUNCES_AVG].i / computedSampleCountf;

        for (int i = STAT_RENDERFRAME; i < STAT_MAX; ++i)
        {
            renderer->m_perfCounters[i].f = (float) us2sec(renderer->m_perfCounters[i].i);
        }

        if (renderer->isPerformanceCounterEnabled(OslRendererImpl::PerformanceCounter_Instance))
        {
            std::unique_lock<std::mutex> lock(renderer->m_performanceInstancesMutex);
            static const ustring Perf_ShadingTotal("shading_total_usecs");
            static const ustring Perf_ShadingAvg("shading_avg_usecs");
            static const ustring Perf_ShadowRayCount("shadow_rays");
            const float times[] = { 0 };
            for (const auto & item: renderer->m_performanceInstances)
            {
                const int32_t total[] = { (int32_t) item.second.first.shadingDurationUS, (int32_t) item.second.second.shadingDurationUS };
                inputs.scene->instanceGenericAttributeData(&item.first, 1, Perf_ShadingTotal, "vec2i", 1, (const char *) &total, &times[0], 1, Scene::COPY_BUFFER);

                const float avg[] = { item.second.first.shadingCount ? (item.second.first.shadingDurationUS / (float) item.second.first.shadingCount) : 0.f,
                                      item.second.second.shadingCount ? (item.second.second.shadingDurationUS / (float) item.second.second.shadingCount) : 0.f };
                inputs.scene->instanceGenericAttributeData(&item.first, 1, Perf_ShadingAvg, "vec2f", 1, (const char *) &avg, &times[0], 1, Scene::COPY_BUFFER);

                const float shadowRays = item.second.first.shadingCount ? (item.second.second.shadowRayCount / (float) item.second.first.shadingCount)
                                                                        : 0.f; // <> nb shadow rays per material eval (not true if transparency)
                inputs.scene->instanceGenericAttributeData(&item.first, 1, Perf_ShadowRayCount, "float", 1, (const char *) &shadowRays, &times[0], 1, Scene::COPY_BUFFER);
            }
        }
#endif
        if (renderer->m_progressSampleFunc)
        {
            renderer->m_progressSampleFunc((int32_t) inputs.startSample, renderer->m_progressAppData);
        }

        // ensure all tiles are added to the rendered tiles
#ifndef SHN_ENABLE_DEBUG
        assert(outputs.sync._canceled || std::all_of(outputs.finishedSampleCountPerTile.begin(), outputs.finishedSampleCountPerTile.end(), [this](int32_t value) {
                   return value == outputs.sampleCountToCompute;
               }));
#endif

        // wake up waitBinding call
        outputs.sync.condition.notify_one();

        outputs.rendererTask = nullptr;
    }
}

void updateBounceLimits(RayDepth & limits, ScatteringType scatteringType, ScatteringEvent scatteringEvent, ClosurePrimitive::Category closureCategory)
{
    if (scatteringType == ScatteringType::Diffuse)
        --limits.d[RayDepth::DIFFUSE];
    else if (scatteringType == ScatteringType::Glossy)
        --limits.d[RayDepth::GLOSSY];
    else if (scatteringType == ScatteringType::Specular && scatteringEvent == ScatteringEvent::Reflect)
        --limits.d[RayDepth::REFLECTION];
    else if (scatteringType == ScatteringType::Specular && scatteringEvent == ScatteringEvent::Transmit)
        --limits.d[RayDepth::REFRACTION];
    else if (scatteringType == ScatteringType::Straight && scatteringEvent == ScatteringEvent::Transmit && closureCategory == ClosurePrimitive::BSDF)
        --limits.d[RayDepth::TRANSPARENCY];
    else if (scatteringType == ScatteringType::Straight && scatteringEvent == ScatteringEvent::Transmit && closureCategory == ClosurePrimitive::Volume)
        --limits.d[RayDepth::VOLUME];
}

uint32_t updateRayType(uint32_t previousRayType, ScatteringType scatteringType, ScatteringEvent scatteringEvent, const OslRendererImpl & renderer)
{
    // Change current raytype only if object is not transparent
    uint32_t rayType = 0;
    if (scatteringType == ScatteringType::Diffuse)
        rayType = renderer.s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Diffuse);
    else if (scatteringType == ScatteringType::Glossy)
        rayType = renderer.s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Glossy);
    else if (scatteringType == ScatteringType::Specular)
        rayType = renderer.s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Specular);
    else if (scatteringType == ScatteringType::Straight)
        rayType = renderer.s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Straight);
    else
        rayType = renderer.s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Straight);

    uint32_t newRayType = previousRayType;

    if (rayType == renderer.s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Straight))
        newRayType |= rayType;
    else
        newRayType = rayType;

    if (scatteringEvent == ScatteringEvent::Reflect)
        newRayType |= renderer.s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Reflection);
    else if (scatteringEvent == ScatteringEvent::Transmit)
        newRayType |= renderer.s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Refraction);

    return newRayType;
}

OslRendererTask::ImportanceCachingOutputPassIndices::ImportanceCachingOutputPassIndices(const Binding & binding):
    meanSampleCountDirect{ binding.getOutputIdx(PassNames::ImportanceCachingMeanSampleCountDirect) },
    meanSampleCountIndirect{ binding.getOutputIdx(PassNames::ImportanceCachingMeanSampleCountIndirect) }, probabilitiesDirect{ binding.getOutputIdx(
                                                                                                              PassNames::ImportanceCachingProbabilitiesDirect) },
    probabilitiesIndirect{ binding.getOutputIdx(PassNames::ImportanceCachingProbabilitiesIndirect) }, tileContribDirect{ binding.getOutputIdx(PassNames::ImportanceCachingTileFContribDirect),
                                                                                                                         binding.getOutputIdx(PassNames::ImportanceCachingTileUContribDirect),
                                                                                                                         binding.getOutputIdx(PassNames::ImportanceCachingTileTContribDirect),
                                                                                                                         binding.getOutputIdx(PassNames::ImportanceCachingTileCContribDirect) },
    tileContribIndirect{ binding.getOutputIdx(PassNames::ImportanceCachingTileFContribIndirect), binding.getOutputIdx(PassNames::ImportanceCachingTileUContribIndirect),
                         binding.getOutputIdx(PassNames::ImportanceCachingTileTContribIndirect), binding.getOutputIdx(PassNames::ImportanceCachingTileCContribIndirect) }
{
}

namespace
{

TileToRect::TileToRect(size_t startx, size_t endx, size_t starty, size_t endy, const char * pattern):
    m_rectStartX(startx), m_rectEndX(endx), m_rectStartY(starty), m_rectEndY(endy), m_tileCountX(tileCount(endx - startx)), m_tileCountY(tileCount(endy - starty)),
    m_func((pattern && strcmp(pattern, SHN_TILEPATTERN_SPIRAL) == 0) ? &TileToRect::spiral : &TileToRect::scanline)
{
}

bool TileToRect::eval(size_t tileId, size_t * actualTile, size_t * startx, size_t * endx, size_t * starty, size_t * endy) const
{
    if (tileId >= m_tileCountX * m_tileCountY)
    {
        return false;
    }
    *actualTile = (this->*m_func)(tileId);
    const auto tileX = *actualTile % m_tileCountX;
    const auto tileY = *actualTile / m_tileCountX;
    *startx = tileX * TILE_SIZE + m_rectStartX;
    *starty = tileY * TILE_SIZE + m_rectStartY;
    *endx = std::min(*startx + TILE_SIZE, m_rectEndX) - 1;
    *endy = std::min(*starty + TILE_SIZE, m_rectEndY) - 1;
    return true;
}

size_t TileToRect::scanline(size_t tile) const
{
    return tile;
}

size_t TileToRect::spiral(size_t tile) const
{
    const size_t minDim = std::min(m_tileCountX, m_tileCountY);
    const size_t oddMinDim = (minDim % 2 == 0) ? minDim - 1 : minDim;

    V2<size_t> result;
    if (tile == 0)
    {
        result = V2<size_t>(m_tileCountX / 2, m_tileCountY / 2);
    }
    else if (tile < oddMinDim * oddMinDim) // in a square spiral
    {
        result = V2<size_t>(m_tileCountX / 2, m_tileCountY / 2);

        // Taken from http://stackoverflow.com/a/19287714

        // given n an index in the squared spiral
        // p the sum of point in inner square
        // a the position on the current square
        // n = p + a

        const int32_t r = (static_cast<int32_t>((std::sqrt(tile) - 1.0) * 0.5)) + 1;

        // compute radius : inverse arithmetic sum of 8+16+24+...=
        const int32_t p = (8 * r * (r - 1)) / 2;
        // compute total point on radius -1 : arithmetic sum of 8+16+24+...

        const int32_t en = r * 2;
        // points by face

        const int32_t a = (tile - p) % (r * 8);
        // compute the position and shift it so the first is (-r,-r) but (-r+1,-r)
        // so square can connect

        // find the face : 0 top, 1 right, 2, bottom, 3 left
        switch (a / (r * 2))
        {
        case 0:
            result.x += a - r;
            result.y += -r;
            break;
        case 1:
            result.x += r;
            result.y += (a % en) - r;
            break;
        case 2:
            result.x += r - (a % en);
            result.y += r;
            break;
        case 3:
            result.x += -r;
            result.y += r - (a % en);
            break;
        default:
            assert(false);
        }
    }
    else if (tile < oddMinDim * minDim) // in the (up or left)-side, if the minDim is odd.
    {
        auto & x = (m_tileCountX > m_tileCountY) ? result.x : result.y;
        auto & y = (m_tileCountX > m_tileCountY) ? result.y : result.x;
        const auto halfDiff = (std::max(m_tileCountX, m_tileCountY) - oddMinDim + 1) / 2;
        const auto rest = (tile - oddMinDim * oddMinDim);
        x = halfDiff + rest;
        y = 0;
    }
    else // in the borders, outside of the square spiral
    {
        auto & x = (m_tileCountX > m_tileCountY) ? result.x : result.y;
        auto & y = (m_tileCountX > m_tileCountY) ? result.y : result.x;
        const auto halfDiff = (std::max(m_tileCountX, m_tileCountY) - oddMinDim + 1) / 2;
        const auto rest = (tile - oddMinDim * minDim);
        const auto row = rest % std::min(m_tileCountX, m_tileCountY);
        const auto columnNum = rest / std::min(m_tileCountX, m_tileCountY); // alternating columns: left, right, left, etc.
        const auto column = (columnNum % 2 == 0) ? halfDiff - columnNum / 2 - 1 // even -> left
                                                 : halfDiff + oddMinDim + columnNum / 2; // odd -> right
        x = column;
        y = row;
    }
    return result.y * m_tileCountX + result.x;
}

} // namespace

SampleBuffer::SampleBuffer(size_t sampleCount, size_t adaptiveMinSampleCount, size_t tilePixelCount, size_t maxBounces, bool evalAllLights, size_t directLightSampleCount):
    m_adaptiveMinSampleCount(adaptiveMinSampleCount), m_sampleCount(sampleCount), m_tilePixelCount(tilePixelCount), m_maxBounces(maxBounces), m_evalAllLights(evalAllLights),
    m_directLightSampleCount(directLightSampleCount)
{
    assert(m_sampleCount > m_adaptiveMinSampleCount);

    const std::size_t perBounceSampleComponentByteSize = sizeof(SampleComponents::Scatter) + sizeof(SampleComponents::Volumetric) + sizeof(SampleComponents::BSSRDF);
    const std::size_t directIlluminationSamplesByteSize = evalAllLights ? 0 : directLightSampleCount * sizeof(SampleComponents::ChosenLight);
    const std::size_t sampleSize =
        (sizeof(SampleComponents::Camera) + sizeof(SampleComponents::Light) + directIlluminationSamplesByteSize + (m_maxBounces + 1) * perBounceSampleComponentByteSize) / sizeof(float);

    m_perBounceByteStride = m_tilePixelCount * perBounceSampleComponentByteSize;
    const std::size_t perBounceStride = m_perBounceByteStride / sizeof(float);
    const std::size_t sampleStride = m_tilePixelCount * sampleSize;
    m_sampleByteStride = sampleStride * sizeof(float);

    m_buffer.resize(m_tilePixelCount * m_sampleCount * sampleSize);
    m_pCameraStart = (char *) m_buffer.data();

    m_pLightStart = m_pCameraStart + tilePixelCount * sizeof(SampleComponents::Camera);

    if (!evalAllLights)
    {
        m_pChosenLightStart = m_pLightStart + tilePixelCount * sizeof(SampleComponents::Light);
        m_pScatterStart = m_pChosenLightStart + tilePixelCount * directIlluminationSamplesByteSize;
    }
    else
    {
        m_pScatterStart = m_pLightStart + tilePixelCount * sizeof(SampleComponents::Light);
    }

    m_pVolumStart = m_pScatterStart + tilePixelCount * sizeof(SampleComponents::Scatter);
    m_pBSSRDFStart = m_pVolumStart + tilePixelCount * sizeof(SampleComponents::Volumetric);
}

void SampleBuffer::computeSamples(RandomState & state)
{
    if (m_sampleCount == 1)
    {
        // In case of one sample per pixel, jittering does nothing so we skip function calls
        for (float & r: m_buffer)
            r = getFloat(state);
        return;
    }

    const size_t remainingSampleCount = m_sampleCount - m_adaptiveMinSampleCount;

    const auto sqrt2DAdaptiveMinSampleCount = size_t(sqrtf((float) m_adaptiveMinSampleCount));
    const size_t jittered2DAdaptiveMinSampleCount = sqr(sqrt2DAdaptiveMinSampleCount);
    const size_t unjittered2DAdaptiveSampleCount = m_adaptiveMinSampleCount - jittered2DAdaptiveMinSampleCount;

    const auto sqrt2DRemainingSampleCount =
        size_t(sqrtf((float) remainingSampleCount)); // Only sqr(m_sqrt2DSampleCount) are 2D-jittered, the remaining (m_sampleCount - sqr(m_sqrt2DSampleCount)) are purely random
    const size_t jittered2DRemainingSampleCount = sqr(sqrt2DRemainingSampleCount);
    const size_t unjittered2DRemainingSampleCount =
        remainingSampleCount - jittered2DRemainingSampleCount; // Number of samples that will not be jittered among each 2D distrib, to reach the required total sample count

    const auto genDistrib1D = [&](float * const ptr) {
        jittered1d(state, ptr, m_adaptiveMinSampleCount, m_sampleByteStride);
        shuffle1d(state, ptr, m_adaptiveMinSampleCount, m_sampleByteStride);

        jittered1d(state, (float *) ((char *) ptr + m_adaptiveMinSampleCount * m_sampleByteStride), remainingSampleCount, m_sampleByteStride);
        shuffle1d(state, (float *) ((char *) ptr + m_adaptiveMinSampleCount * m_sampleByteStride), remainingSampleCount, m_sampleByteStride);
    };

    const auto genDistrib2D = [&](float * const ptr) {
        jittered2d(state, ptr, sqrt2DAdaptiveMinSampleCount, sqrt2DAdaptiveMinSampleCount, m_sampleByteStride);
        random2d(state, (float *) ((char *) ptr + jittered2DAdaptiveMinSampleCount * m_sampleByteStride), unjittered2DAdaptiveSampleCount, m_sampleByteStride);
        shuffle2d(state, ptr, m_adaptiveMinSampleCount, m_sampleByteStride);

        jittered2d(state, (float *) ((char *) ptr + m_adaptiveMinSampleCount * m_sampleByteStride), sqrt2DRemainingSampleCount, sqrt2DRemainingSampleCount, m_sampleByteStride);
        random2d(state, (float *) ((char *) ptr + (m_adaptiveMinSampleCount + jittered2DRemainingSampleCount) * m_sampleByteStride), unjittered2DRemainingSampleCount, m_sampleByteStride);
        shuffle2d(state, (float *) ((char *) ptr + m_adaptiveMinSampleCount * m_sampleByteStride), remainingSampleCount, m_sampleByteStride);
    };

    for (size_t pixelIdx = 0; pixelIdx < m_tilePixelCount; ++pixelIdx)
    {
        {
            // Compute distributions for Camera component
            float * const pCameraStart = (float *) (m_pCameraStart + pixelIdx * sizeof(SampleComponents::Camera));

            float * const pPixelOffset = (float *) ((char *) pCameraStart + offsetof(SampleComponents::Camera, uPixel));
            genDistrib2D(pPixelOffset);

            float * const pLensOffset = (float *) ((char *) pCameraStart + offsetof(SampleComponents::Camera, uLens));
            genDistrib2D(pLensOffset);

            float * const pTimeOffset = (float *) ((char *) pCameraStart + offsetof(SampleComponents::Camera, uTime));
            genDistrib1D(pTimeOffset);
        }

        {
            // Compute distributions for Light component
            float * const pLightStart = (float *) (m_pLightStart + pixelIdx * sizeof(SampleComponents::Light));

            float * const pDirOffset = (float *) ((char *) pLightStart + offsetof(SampleComponents::Light, uDir));
            genDistrib2D(pDirOffset);

            float * const pPrimitiveOffset = (float *) ((char *) pLightStart + offsetof(SampleComponents::Light, uPrimitive));
            genDistrib1D(pPrimitiveOffset);

            if (!m_evalAllLights)
            {
                for (size_t directLightSampleIdx = 0; directLightSampleIdx < m_directLightSampleCount; ++directLightSampleIdx)
                {
                    float * const pChosenLightStart =
                        (float *) (m_pChosenLightStart + pixelIdx * m_directLightSampleCount * sizeof(SampleComponents::ChosenLight) + directLightSampleIdx * sizeof(SampleComponents::ChosenLight));

                    float * const pIndexOffset = (float *) ((char *) pChosenLightStart + offsetof(SampleComponents::ChosenLight, uIndex));
                    genDistrib1D(pIndexOffset);
                }
            }
        }

        // Per bounce components
        for (size_t d = 0; d <= m_maxBounces; ++d)
        {
            {
                // Compute distributions for Scatter component
                float * const pScatterStart = (float *) (m_pScatterStart + d * m_perBounceByteStride + pixelIdx * sizeof(SampleComponents::Scatter));

                float * const pClosureComponentOffset = (float *) ((char *) pScatterStart + offsetof(SampleComponents::Scatter, uClosureComponent));
                genDistrib1D(pClosureComponentOffset);

                float * const pDirOffset = (float *) ((char *) pScatterStart + offsetof(SampleComponents::Scatter, uDir));
                genDistrib2D(pDirOffset);
            }

            {
                // Compute distributions for Volumetric component
                float * const pVolumStart = (float *) (m_pVolumStart + d * m_perBounceByteStride + pixelIdx * sizeof(SampleComponents::Volumetric));

                float * const pDistanceOffset = (float *) ((char *) pVolumStart + offsetof(SampleComponents::Volumetric, uDistance));
                genDistrib1D(pDistanceOffset);
            }

            {
                // Compute distributions for BSSRDF component
                float * const pBSSRDFStart = (float *) (m_pBSSRDFStart + d * m_perBounceByteStride + pixelIdx * sizeof(SampleComponents::BSSRDF));

                float * const pStratOffset = (float *) ((char *) pBSSRDFStart + offsetof(SampleComponents::BSSRDF, uStrategy));
                genDistrib1D(pStratOffset);

                float * const pRotationOffset = (float *) ((char *) pBSSRDFStart + offsetof(SampleComponents::BSSRDF, uRotation));
                genDistrib1D(pRotationOffset);

                float * const pPointOffset = (float *) ((char *) pBSSRDFStart + offsetof(SampleComponents::BSSRDF, uPoint));
                genDistrib2D(pPointOffset);
            }
        }
    }
}

#ifdef SHN_ENABLE_DEBUG

void OslRendererTask::printSampleDebugInfo(const PixelInfo & pixelInfo, const Path & path, const Ray & ray, uint32_t rayType, int32_t sampleID) const
{
    return;
    // print only debug pixel if it exists
#ifdef SHN_DEBUG_PIXEL
    if (!isDebugPixel(pixelInfo.pixel))
        return;
#endif

    std::fprintf(stderr, "//////// SAMPLE %d - BOUNCE %d /////////\n", inputs.startSample + sampleID, path.depth);
    const int x = pixelInfo.pixel.x;
    const int y = pixelInfo.pixel.y;

    // NaN detection
    if (shn::isNaN(path.weight))
        std::fprintf(stderr, "[!] NaN DETECTED at (%dx%d) sample %d bounce %d in path weight\n", x, y, path.depth, inputs.startSample + sampleID);

    // Inf detection
    if (shn::isInf(path.weight))
        std::fprintf(stderr, "[!] Inf DETECTED at (%dx%d) sample %d bounce %d in path weight\n", x, y, path.depth, inputs.startSample + sampleID);

    // Neg detection
    if (shn::isNeg(path.weight))
        std::fprintf(stderr, "[!] Neg DETECTED at (%dx%d) sample %d bounce %d in path weight\n", x, y, path.depth, inputs.startSample + sampleID);

    std::fprintf(stderr, " - straighOnly: %d\n", straightOnly(path.samplingState));

    // Ray
    std::string rayTypeName = "no_type_detected";
    if (rayType == renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Diffuse))
        rayTypeName = "diffuse";
    else if (rayType == renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Glossy))
        rayTypeName = "glossy";
    else if (rayType == renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Specular))
        rayTypeName = "singular";
    else if (rayType == renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Straight))
        rayTypeName = "straight";
    else if (rayType == renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Camera))
        rayTypeName = "camera";
    else if (rayType == renderer->s_shadingSystem->raytype_bit(OslRendererImpl::RayType_Shadow))
        rayTypeName = "shadow";

    std::fprintf(stderr, "\n//\nRAY\n//\n");
    std::fprintf(stderr, " - ray.org: %.04f, %.04f, %.04f\n", ray.org.x, ray.org.y, ray.org.z);
    std::fprintf(stderr, " - ray.dir: %.04f, %.04f, %.04f\n", ray.dir.x, ray.dir.y, ray.dir.z);
    std::fprintf(stderr, " - ray.Ng: %.04f, %.04f, %.04f\n", ray.Ng.x, ray.Ng.y, ray.Ng.z);
    std::fprintf(stderr, " - ray.tnear: %.04f\n", ray.tnear);
    std::fprintf(stderr, " - ray.tfar: %.04f\n", ray.tfar);
    std::fprintf(stderr, " - ray.time: %.04f\n", ray.time);
    std::fprintf(stderr, " - ray.instID: %d\n", ray.instID);
    std::fprintf(stderr, " - raytype : %s\n", rayTypeName.c_str());

    // Instance
    std::fprintf(stderr, "\n//\nINSTANCE\n//\n");
    uint32_t instanceId = ray.instID;
    const char * instanceName = inputs.scene->getInstanceNameFromId(instanceId);
    shn::OslRenderer::Id shaderId = renderer->getShadingTreeIdFromInstanceId(instanceId, 0);
    const char * shaderName = renderer->getShadingTreeNameFromId(shaderId);

    std::fprintf(stderr, " - instanceId: %d\n", instanceId);
    std::fprintf(stderr, " - instanceName: %s\n", instanceName);
    std::fprintf(stderr, " - shaderId: %d\n", shaderId);
    std::fprintf(stderr, " - shaderName: %s\n", shaderName);

    //// Shading globals
    // std::fprintf(stderr, "\n//\nSHADING GLOBALS\n//\n");
    // std::cerr << shading.globals << std::endl;

    //// SSS weight/globals
    // std::fprintf(stderr, "\n//\nSSS GLOBALS AND WEIGHT\n//\n");
    // std::cerr << shading.bssrdfPoint.globals << std::endl;
    // std::cerr << "shading.bssrdfIntersectionCount = " << shading.bssrdfPoint.bssrdfIntersectionCount << std::endl;
    // std::cerr << "shading.bssrdfPointPmf = " << shading.bssrdfPoint.bssrdfPointPmf << std::endl;
    // std::cerr << "shading.bssrdfPointPdf = " << shading.bssrdfPoint.bssrdfPointPdf << std::endl;
    // std::cerr << "shading.bssrdfSamplingWeight = " << shading.bssrdfPoint.samplingWeight << std::endl;
    // std::cerr << "shading.bssrdfSampledRadius = " << shading.bssrdfPoint.bssrdfSampledRadius << std::endl;
    // std::cerr << "distance between globals and ssGlobals = " << distance(shading.globals.P, shading.bssrdfPoint.globals.P) << std::endl;

    // std::fprintf(stderr, "\n//\nSSS RAYS\n//\n");
    // for (size_t i = 0; i < shading.bssrdfPoint.bssrdfRays.size(); ++i)
    //{
    //    std::cerr << "RAY " << i << std::endl;
    //    std::cerr << "org = " << shading.bssrdfPoint.bssrdfRays[i].org << std::endl;
    //    std::cerr << "dir = " << shading.bssrdfPoint.bssrdfRays[i].dir << std::endl;
    //    std::cerr << "near = " << shading.bssrdfPoint.bssrdfRays[i].tnear << std::endl;
    //    std::cerr << "far = " << shading.bssrdfPoint.bssrdfRays[i].tfar << std::endl;
    //    std::cerr << "Ng = " << shading.bssrdfPoint.bssrdfRays[i].Ng << std::endl;
    //    std::cerr << "instID = " << shading.bssrdfPoint.bssrdfRays[i].instID << std::endl;
    //    std::cerr << "primID = " << shading.bssrdfPoint.bssrdfRays[i].primID << std::endl;
    //    std::cerr << "hit = " << (shading.bssrdfPoint.bssrdfRays[i].org + shading.bssrdfPoint.bssrdfRays[i].tfar * shading.bssrdfPoint.bssrdfRays[i].dir) << std::endl;
    //    std::cerr << "pdf = " << (shading.bssrdfPoint.bssrdfRaysPdf[i]) << std::endl;
    //    std::cerr << std::endl;
    //}

    //// Closures
    // std::fprintf(stderr, "\n//\nCLOSURES\n//\n");
    // std::cerr << shading.result.bsdf << std::endl;

    // Next Bounce
    std::string closureCategory = "unrecognized_category";
    if (path.closureCategory == ClosurePrimitive::BSDF)
        closureCategory = "bsdf";
    if (path.closureCategory == ClosurePrimitive::BSSRDF)
        closureCategory = "bssrdf";
    if (path.closureCategory == ClosurePrimitive::Volume)
        closureCategory = "volume";

    std::fprintf(stderr, "\n//\nNEXT BOUNCE\n//\n");
    std::fprintf(stderr, " - closureCategory: %s\n", closureCategory.c_str());
    std::fprintf(stderr, " - scatteringEvent: %d\n", path.scatteringEvent);
    std::fprintf(stderr, " - scatteringType: %d\n", path.scatteringType);

    std::fprintf(stderr, "////////////////////////////////\n\n");
}

#endif

} // namespace shn
