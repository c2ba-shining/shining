#pragma once

#ifndef __SHINING_INSTANCE_H__
#define __SHINING_INSTANCE_H__

#include "IMathUtils.h"
#include "MotionTransform.h"
#include "Primitive.h"
#include "Shining.h"
#include "embree2/rtcore.h"
#include <OpenEXR/ImathBox.h>
#include <OpenEXR/ImathMatrix.h>

namespace shn
{

struct StaticInstance
{
public:
    M44f localToWorld;
    M44f worldToLocal;
    Box3f bounds;
    int32_t mask;
    int32_t id;
    const MotionPrimitive * primitive;

    size_t memoryFootprint() const { return sizeof(*this); }
};

void staticInstanceBounds(void * ptr, size_t item, RTCBounds & bounds_o);
void staticInstanceInit(StaticInstance * instance, const M44f & localToWorld, int32_t mask, uint32_t id, const MotionPrimitive * primitive);
void staticInstanceIntersect(void * ptr, RTCRay & ray, size_t item);
void staticInstanceOccluded(void * ptr, RTCRay & ray, size_t item);

struct MotionInstance
{
public:
    MotionTransform localToWorld;
    MotionTransform worldToLocal;
    Box3f bounds;
    int32_t mask;
    int32_t id;
    const MotionPrimitive * primitive;

    size_t memoryFootprint() const { return sizeof(*this) + localToWorld.memoryFootprint() + worldToLocal.memoryFootprint(); }
};

void motionInstanceBounds(void * ptr, size_t item, RTCBounds & bounds_o);
void motionInstanceInit(MotionInstance * instance, const MotionTransform localToWorld, int32_t mask, uint32_t id, const MotionPrimitive * primitive);
void motionInstanceIntersect(void * ptr, RTCRay & ray, size_t item);
void motionInstanceOccluded(void * ptr, RTCRay & ray, size_t item);

} // namespace shn

#endif // __SHINING_INSTANCE_H__
