#pragma once

#ifndef __SHINING_SHADING_GRID__
#define __SHINING_SHADING_GRID__

#include "OslHeaders.h"

namespace shn
{
class ShadingGrid
{
public:
    ShadingGrid(): m_values(nullptr), m_rows(nullptr), m_cols(nullptr), m_res(0), m_invres(0), m_invjacobian(0)
    {
    }
    ~ShadingGrid();

    template<typename F>
    void prepare(int resolution, F cb)
    {
        m_res = resolution;
        m_invres = 1.0f / m_res;
        m_invjacobian = m_res * m_res / float(4 * M_PI);

        m_values = new OSL::Color3[m_res * m_res];
        m_rows = new float[m_res];
        m_cols = new float[m_res * m_res];
        for (int y = 0, i = 0; y < m_res; ++y)
        {
            for (int x = 0; x < m_res; ++x, ++i)
            {
                m_values[i] = cb(map(x + 0.5f, y + 0.5f));
                m_cols[i] = (m_values[i].x + m_values[i].y + m_values[i].z) + ((x > 0) ? m_cols[i - 1] : 0.0f);
            }
            m_rows[y] = m_cols[i - 1] + ((y > 0) ? m_rows[y - 1] : 0.0f);
            // normalize the pdf for this scanline (if it was non-zero)
            if (m_cols[i - 1] > 0)
            {
                for (int x = 0; x < m_res; ++x)
                {
                    m_cols[i - m_res + x] /= m_cols[i - 1];
                }
            }
        }

        // normalize the pdf across all scanlines
        if (m_rows[m_res - 1] > 0.f)
        {
            for (int y = 0; y < m_res; ++y)
            {
                m_rows[y] /= m_rows[m_res - 1];
            }
        }

        // save sampled importance table
        /*float * buffer = new float[3*m_res*m_res];
        for(int x = 0; x < m_res; ++x)
        {
            for(int y = 0; y < m_res; ++y)
            {
                float invpdf;
                OSL::Color3 c = sampleAndEval((x+0.5f)*m_invres, (y+0.5f)*m_invres, OSL::Dual2<OSL::Vec3>(), invpdf);
                buffer[3*(y*m_res + x)] = c[0];
                buffer[3*(y*m_res + x)+1] = c[1];
                buffer[3*(y*m_res + x)+2] = c[2];
            }
        }

        OIIO::ImageOutput* out = OIIO::ImageOutput::create("./bg.exr");
        OIIO::ImageSpec spec(m_res, m_res, 3, OIIO::TypeDesc::FLOAT);
        if (out->open("./bg.exr", spec))
        {
            fprintf(stdout, "Write image.\n");
            out->write_image(OIIO::TypeDesc::FLOAT, buffer);
            out->close ();
        }
        delete out;
        delete[] buffer;*/
    }

    OSL::Color3 eval(const OSL::Vec3 & dir, float & invpdf);
    OSL::Color3 sampleAndEval(float rx, float ry, OSL::Dual2<OSL::Vec3> & dir, float & invpdf);

private:
    // inverse latlong projection => direction from pixel coordinates (x,y)
    OSL::Dual2<OSL::Vec3> map(const float x, const float y);
    float sample_cdf(const float * data, unsigned int n, float x, unsigned int * idx, float * pdf);
    void sincos(const OSL::Dual2<float> & a, OSL::Dual2<float> * sina, OSL::Dual2<float> * cosa);

    OSL::Color3 * m_values;
    float * m_rows;
    float * m_cols;
    int m_res;
    // int m_resY;
    float m_invres;
    float m_invjacobian;
};
} // namespace shn

#endif //__SHINING_SHADING_GRID__