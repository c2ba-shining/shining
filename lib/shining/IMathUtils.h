#pragma once

#include "IMathTypes.h"
#include <algorithm>
#include <limits>

namespace shn
{

// Constants
static const float FPI = 3.14159265358979323846f;
static const float F2PI = 6.283185307179586232f;
static const float F1_PI = 0.31830988618379069122f;
static const float F2_PI = 0.636619772367581343076f;
static const float F1_2PI = 0.15915494309189534561f;
static const float F1_4PI = 0.079577471545947672804f;
static const float FPI_2 = 1.57079632679489661923f;
static const float FPI_4 = 0.785398163397448309616f;
static const float F2_SQRTPI = 1.12837916709551257390f;
static const float FSQRT2 = 1.41421356237309504880f;
static const float F1_SQRT2 = 0.707106781186547524401f;
static const float F2_POW_16 = 65536.f;

static struct NegInfTy
{
    inline operator double() const
    {
        return -std::numeric_limits<double>::infinity();
    }
    inline operator float() const
    {
        return -std::numeric_limits<float>::infinity();
    }
    inline operator long long() const
    {
        return std::numeric_limits<long long>::min();
    }
    inline operator unsigned long long() const
    {
        return std::numeric_limits<unsigned long long>::min();
    }
    inline operator long() const
    {
        return std::numeric_limits<long>::min();
    }
    inline operator unsigned long() const
    {
        return std::numeric_limits<unsigned long>::min();
    }
    inline operator int() const
    {
        return std::numeric_limits<int>::min();
    }
    inline operator unsigned int() const
    {
        return std::numeric_limits<unsigned int>::min();
    }
    inline operator short() const
    {
        return std::numeric_limits<short>::min();
    }
    inline operator unsigned short() const
    {
        return std::numeric_limits<unsigned short>::min();
    }
    inline operator char() const
    {
        return std::numeric_limits<char>::min();
    }
    inline operator unsigned char() const
    {
        return std::numeric_limits<unsigned char>::min();
    }
} neg_inf;

static struct PosInfTy
{
    inline operator double() const
    {
        return std::numeric_limits<double>::infinity();
    }
    inline operator float() const
    {
        return std::numeric_limits<float>::infinity();
    }
    inline operator long long() const
    {
        return std::numeric_limits<long long>::max();
    }
    inline operator unsigned long long() const
    {
        return std::numeric_limits<unsigned long long>::max();
    }
    inline operator long() const
    {
        return std::numeric_limits<long>::max();
    }
    inline operator unsigned long() const
    {
        return std::numeric_limits<unsigned long>::max();
    }
    inline operator int() const
    {
        return std::numeric_limits<int>::max();
    }
    inline operator unsigned int() const
    {
        return std::numeric_limits<unsigned int>::max();
    }
    inline operator short() const
    {
        return std::numeric_limits<short>::max();
    }
    inline operator unsigned short() const
    {
        return std::numeric_limits<unsigned short>::max();
    }
    inline operator char() const
    {
        return std::numeric_limits<char>::max();
    }
    inline operator unsigned char() const
    {
        return std::numeric_limits<unsigned char>::max();
    }
} inf, pos_inf;

inline NegInfTy operator-(const PosInfTy &)
{
    return neg_inf;
}

inline PosInfTy operator-(const NegInfTy &)
{
    return pos_inf;
}

static struct UlpTy
{
    inline operator double() const
    {
        return std::numeric_limits<double>::epsilon();
    }
    inline operator float() const
    {
        return std::numeric_limits<float>::epsilon();
    }
} ulp;

// Functions
inline void fv4ToV4f(const float * src, V4f & dst)
{
    dst.x = src[0];
    dst.y = src[1];
    dst.z = src[2];
    dst.w = src[3];
}

inline void fv3ToV4f(const float * src, float w, V4f & dst)
{
    dst.x = src[0];
    dst.y = src[1];
    dst.z = src[2];
    dst.w = w;
}

inline void fv3ToV3f(const float * src, float w, V3f & dst)
{
    dst.x = src[0];
    dst.y = src[1];
    dst.z = src[2];
}

inline V4f fv3ToV4f(const float * src, float w = 0.f)
{
    return V4f(src[0], src[1], src[2], w);
}

inline V3f fv3ToV3f(const float * src)
{
    return V3f(src[0], src[1], src[2]);
}

inline V4f fv4ToV4f(const float * src)
{
    return V4f(src[0], src[1], src[2], src[3]);
}

inline void V4fTofv4(const V4f & src, float * dst)
{
    dst[0] = src.x;
    dst[1] = src.y;
    dst[2] = src.z;
    dst[3] = src.w;
}

inline void V4fTofv3(const V4f & src, float * dst)
{
    dst[0] = src.x;
    dst[1] = src.y;
    dst[2] = src.z;
}

inline void V4fTof3(const V4f & src, float & dstX, float & dstY, float & dstZ)
{
    dstX = src.x;
    dstY = src.y;
    dstZ = src.z;
}

inline void V3fTof3(const V3f & src, float & dstX, float & dstY, float & dstZ)
{
    dstX = src.x;
    dstY = src.y;
    dstZ = src.z;
}

inline M44f fv16ToM44f(const float * src)
{
    return M44f(*((M44f *) src));
}

template<typename MatrixT>
inline MatrixT inverse(const MatrixT & m)
{
    return m.inverse();
}

template<typename MatrixT>
inline MatrixT transpose(const MatrixT & m)
{
    return m.transposed();
}

inline bool isNaN(float v)
{
#if defined(WIN32) || defined(_WIN32) || defined(_WIN64)
    return 0 != _isnanf(v);
#else
    return 0 != __isnanf(v);
#endif
}

inline bool isNaN(const V2f & v)
{
    return isNaN(v.x) || isNaN(v.y);
}

inline bool isNaN(const V3f & v)
{
    return isNaN(v.x) || isNaN(v.y) || isNaN(v.z);
}

inline bool isNaN(const V4f & v)
{
    return isNaN(v.x) || isNaN(v.y) || isNaN(v.z) || isNaN(v.w);
}

inline bool isNaN(const Color3 & c)
{
    return isNaN(c.x) || isNaN(c.y) || isNaN(c.z);
}

inline bool isInf(float v)
{
    return std::isinf(v);
}

inline bool isInf(const V2f & v)
{
    return isInf(v.x) || isInf(v.y);
}

inline bool isInf(const V3f & v)
{
    return isInf(v.x) || isInf(v.y) || isInf(v.z);
}

inline bool isInf(const V4f & v)
{
    return isInf(v.x) || isInf(v.y) || isInf(v.z) || isInf(v.w);
}

inline bool isInf(const Color3 & c)
{
    return isInf(c.x) || isInf(c.y) || isInf(c.z);
}

inline bool isNeg(float v)
{
    return v < 0.f;
}

inline bool isNeg(const V2f & v)
{
    return isNeg(v.x) || isNeg(v.y);
}

inline bool isNeg(const V3f & v)
{
    return isNeg(v.x) || isNeg(v.y) || isNeg(v.z);
}

inline bool isNeg(const Color3 & c)
{
    return isNeg(c.x) || isNeg(c.y) || isNeg(c.z);
}

inline Box3f xfmBounds(const M44f & m, const Box3f & bounds)
{
    return Imath::transform(bounds, m);
}

template<typename VecType>
inline float length(const VecType & v)
{
    return v.length();
}

template<typename VecType>
inline VecType normalize(const VecType & v)
{
    return v.normalized();
}

inline V3f normalize(const V3f & v, float & length)
{
    length = shn::length(v);
    return (length > 0.f) ? (v / length) : V3f(0.f);
}

template<typename IMathVec>
inline float dot(const IMathVec & v1, const IMathVec & v2)
{
    return v1.dot(v2);
}

template<typename T>
inline T sign(T f)
{
    if (f < T(0))
        return T(-1);
    return T(1);
}

template<typename T>
inline T clamp(T f, T min, T max)
{
    return (f < min) ? min : (f > max) ? max : f;
}

inline float mix(float a, float b, float t)
{
    return a * (1.f - t) + b * t;
}

inline float sqrLength(const V3f & v)
{
    return dot(v, v);
}

inline float distance(const V3f & v0, const V3f & v1)
{
    return length(v1 - v0);
}

inline float sqrDistance(const V3f & v0, const V3f & v1)
{
    return sqrLength(v1 - v0);
}

inline float sum(const V3f & v)
{
    return v[0] + v[1] + v[2];
}

template<typename T>
inline T max(T f1, T f2)
{
    return (f1 > f2) ? f1 : f2;
}

template<typename T>
inline T min(T f1, T f2)
{
    return (f1 < f2) ? f1 : f2;
}

inline float max(const V3f & v)
{
    float max = v[0];
    if (v[1] > max)
        max = v[1];
    if (v[2] > max)
        max = v[2];
    return max;
}

inline V3f max(const V3f & v, float f)
{
    return V3f(v[0] < f ? f : v[0], v[1] < f ? f : v[1], v[2] < f ? f : v[2]);
}

inline V3f min(const V3f & v, float f)
{
    return V3f(v[0] > f ? f : v[0], v[1] > f ? f : v[1], v[2] > f ? f : v[2]);
}

inline Color3 max(const Color3 & v1, const Color3 & v2)
{
    return Color3(v1[0] < v2[0] ? v2[0] : v1[0], v1[1] < v2[1] ? v2[1] : v1[1], v1[2] < v2[2] ? v2[2] : v1[2]);
}

template<typename T>
inline V3<T> max(const V3<T> & v1, const V3<T> & v2)
{
    return V3<T>(v1[0] < v2[0] ? v2[0] : v1[0], v1[1] < v2[1] ? v2[1] : v1[1], v1[2] < v2[2] ? v2[2] : v1[2]);
}

inline V4f max(const V4f & v1, const V4f & v2)
{
    return V4f(v1[0] < v2[0] ? v2[0] : v1[0], v1[1] < v2[1] ? v2[1] : v1[1], v1[2] < v2[2] ? v2[2] : v1[2], v1[3] < v2[3] ? v2[3] : v1[3]);
}

inline V3f min(const V3f & v1, const V3f & v2)
{
    return V3f(v1[0] > v2[0] ? v2[0] : v1[0], v1[1] > v2[1] ? v2[1] : v1[1], v1[2] > v2[2] ? v2[2] : v1[2]);
}

inline M44f normalMatrix(const M44f & m)
{
    return m.inverse().transposed();
}

inline V3f multNormalMatrix(const M44f & m, const V3f & v)
{
    V3f dst(0.f);
    normalMatrix(m).multDirMatrix(v, dst);
    return dst;
}

inline V3f multVecMatrix(const M44f & m, const V3f & v)
{
    V3f dst(0.f);
    m.multVecMatrix(v, dst);
    return dst;
}

inline V3f multDirMatrix(const M44f & m, const V3f & d)
{
    V3f dst(0.f);
    m.multDirMatrix(d, dst);
    return dst;
}

inline V3f multVecMatrix(const M33f & m, const V3f & d)
{
    return V3f(d[0] * m[0][0] + d[1] * m[1][0] + d[2] * m[2][0], d[0] * m[0][1] + d[1] * m[1][1] + d[2] * m[2][1], d[0] * m[0][2] + d[1] * m[1][2] + d[2] * m[2][2]);
}

inline V3f multVecMatrixTransposed(const M33f & m, const V3f & d)
{
    return V3f(d[0] * m[0][0] + d[1] * m[0][1] + d[2] * m[0][2], d[0] * m[1][0] + d[1] * m[1][1] + d[2] * m[1][2], d[0] * m[2][0] + d[1] * m[2][1] + d[2] * m[2][2]);
}

inline V3f frameX(const M33f & m)
{
    return V3f(m[0][0], m[0][1], m[0][2]);
}

inline V3f frameY(const M33f & m)
{
    return V3f(m[1][0], m[1][1], m[1][2]);
}

inline V3f frameZ(const M33f & m)
{
    return V3f(m[2][0], m[2][1], m[2][2]);
}

// Assuming m represents an euclidean transform (the last line is (0, 0, 0, 1)), returns the X axis of the frame associated to the transform
inline V3f frameX(const M44f & m)
{
    return V3f(m[0][0], m[0][1], m[0][2]);
}

// Assuming m represents an euclidean transform (the last line is (0, 0, 0, 1)), returns the Y axis of the frame associated to the transform
inline V3f frameY(const M44f & m)
{
    return V3f(m[1][0], m[1][1], m[1][2]);
}

// Assuming m represents an euclidean transform (the last line is (0, 0, 0, 1)), returns the Z axis of the frame associated to the transform
inline V3f frameZ(const M44f & m)
{
    return V3f(m[2][0], m[2][1], m[2][2]);
}

// Assuming m represents an euclidean transform (the last line is (0, 0, 0, 1)), returns the origin of the frame associated to the transform
inline V3f frameOrigin(const M44f & m)
{
    return V3f(m[3][0], m[3][1], m[3][2]);
}

inline M44f extractRotationMatrix(const M44f & m)
{
    M44f retM;
    retM.makeIdentity();

    const float sx = length(V3f(m[0][0], m[1][0], m[2][0]));
    const float sy = length(V3f(m[0][1], m[1][1], m[2][1]));
    const float sz = length(V3f(m[0][2], m[1][2], m[2][2]));

    retM[0][0] = m[0][0] / sx;
    retM[1][0] = m[1][0] / sx;
    retM[2][0] = m[2][0] / sx;
    retM[0][1] = m[0][1] / sy;
    retM[1][1] = m[1][1] / sy;
    retM[2][1] = m[2][1] / sy;
    retM[0][2] = m[0][2] / sz;
    retM[1][2] = m[1][2] / sz;
    retM[2][2] = m[2][2] / sz;

    return retM;
}

inline V3f pow(V3f v, float f)
{
    return V3f(powf(v[0], f), powf(v[1], f), powf(v[2], f));
}

inline V3f exp(V3f v)
{
    return V3f(expf(v[0]), expf(v[1]), expf(v[2]));
}

template<typename T>
inline T sqr(T x)
{
    return x * x;
}

template<typename T>
inline T safe_acos(T x)
{
    if (x <= T(-1))
        return T(FPI);
    if (x >= T(+1))
        return T(0);
    return std::acos(x);
}

using std::abs;

inline V3f abs(V3f v)
{
    return V3f(fabsf(v[0]), fabsf(v[1]), fabsf(v[2]));
}

inline float reduceAdd(V3f v)
{
    return v[0] + v[1] + v[2];
}

inline float reduceMax(const V3f & v)
{
    return std::max(v[0], std::max(v[1], v[2]));
}

inline float reduceMin(const V3f & v)
{
    return std::min(v[0], std::min(v[1], v[2]));
}

// Cross product surcharge
inline V3f cross(const V3f & x, const V3f & y)
{
    return x.cross(y);
}

// constructs a coordinate frame from three axis vectors and an origin
inline M44f frame(const V3f & vx, const V3f & vy, const V3f & vz, const V3f & org)
{
    return M44f(vx.x, vx.y, vx.z, 0, vy.x, vy.y, vy.z, 0, vz.x, vz.y, vz.z, 0, org.x, org.y, org.z, 1);
}

inline M33f frame(const V3f & vx, const V3f & vy, const V3f & vz)
{
    return M33f(vx.x, vx.y, vx.z, vy.x, vy.y, vy.z, vz.x, vz.y, vz.z);
}

// constructs a coordinate frame form a normal (should be normalized)
inline M44f frame(const V3f & N)
{
    auto dx0 = cross(V3f(1, 0, 0), N);
    auto dx1 = cross(V3f(0, 1, 0), N);
    auto dx = normalize(dot(dx0, dx0) > dot(dx1, dx1) ? dx0 : dx1);
    auto dy = normalize(cross(N, dx));
    return frame(dx, dy, N, V3f(0.f));
}

/// Make two unit vectors that are orthogonal to N and each other.  This
/// assumes that N is already normalized.  We get the first orthonormal
/// by taking the cross product of N and (1,1,1), unless N is 1,1,1, in
/// which case we cross with (-1,1,1).  Either way, we get something
/// orthogonal.  Then N x a is mutually orthogonal to the other two.
inline void makeOrthonormals(const V3f & N, V3f & a, V3f & b)
{
    if (N[0] != N[1] || N[0] != N[2])
        a = V3f(N[2] - N[1], N[0] - N[2], N[1] - N[0]); // (1,1,1) x N
    else
        a = V3f(N[2] - N[1], N[0] + N[2], -N[1] - N[0]); // (-1,1,1) x N
    a.normalize();
    b = cross(N, a);
}

/// Helper function: make two unit vectors that are orthogonal to N and
/// each other. The x vector will point roughly in the same direction as the
/// tangent vector T. We assume that T and N are already normalized.
inline void makeOrthonormals(const V3f & N, const V3f & T, V3f & x, V3f & y)
{
    y = N.cross(T);
    x = y.cross(N);
}

inline float luminance(const V3f & color)
{
    return 0.212671f * color.x + 0.715160f * color.y + 0.072169f * color.z;
}

inline V3f faceForward(const V3f & targetVector, const V3f & referenceVector)
{
    return dot(targetVector, referenceVector) > 0.f ? targetVector : -targetVector;
}

// Return positive sinus for a given cosine
inline float cos2sin(float cos_a)
{
    return sqrtf(std::max(1 - cos_a * cos_a, 0.0f));
}

// Return positive cosine for a give sinus
inline float sin2cos(float sin_a)
{
    return cos2sin(sin_a);
}

inline float deg2rad(float x)
{
    return x * 1.74532925199432957692e-2f;
}

inline float rad2deg(float x)
{
    return x * 5.72957795130823208768e1f;
}

/*
  Calculate parametric value of a coordinate given t and the four point
  coordinates of a cubic bezier curve. This is a separate function
  because we need it for both x and y values.
*/
template<typename VecType>
inline VecType evalCubicBezierCurve(float t, const VecType & start, const VecType & control_1, const VecType & control_2, const VecType & end)
{
    auto one_minus_t = (1.f - t);
    /* Formula from Wikipedia article on Bezier curves. */
    return start * one_minus_t * one_minus_t * one_minus_t + 3.f * control_1 * one_minus_t * one_minus_t * t + 3.f * control_2 * one_minus_t * t * t + end * t * t * t;
}

template<typename VecType>
inline double cubicBezierCurveLength(const VecType & start, const VecType & control_1, const VecType & control_2, const VecType & end, size_t steps = 10)
{
    float t;
    VecType dot;
    VecType previous_dot;
    float length = 0.0;
    for (size_t i = 0u; i <= steps; i++)
    {
        t = (float) i / (float) steps;
        dot = evalCubicBezierCurve(t, start, control_1, control_2, end);
        if (i > 0u)
        {
            length += (dot - previous_dot).length();
        }
        previous_dot = dot;
    }
    return length;
}

template<typename VecType>
inline VecType evalCubicBezierTangent(float t, const VecType & start, const VecType & control_1, const VecType & control_2, const VecType & end)
{
    auto one_minus_t = (1.f - t);
    return start * (-3.f * one_minus_t * one_minus_t) + control_1 * (3.f * one_minus_t * one_minus_t - 6.f * one_minus_t * t) + control_2 * (6.f * one_minus_t * t - 3.f * t * t) +
        end * (3.f * t * t);
}

inline bool isBlack(const Color3 & value)
{
    return !(value.x > 0.f) && !(value.y > 0.f) && !(value.z > 0.f);
}

inline V3f int2color(int32_t n)
{
    V3f x = float(n + 1) * V3f(12.9898f, 78.233f, 56.128f);
    x = V3f(sin(x.x), sin(x.y), sin(x.z)) * 43758.5453f;
    return V3f(x.x - int32_t(x.x), x.y - int32_t(x.y), x.z - int32_t(x.z));
}

template<typename T>
inline T deg2rad(const T & x)
{
    return x * T(1.74532925199432957692e-2f);
}
template<typename T>
inline T rad2deg(const T & x)
{
    return x * T(5.72957795130823208768e1f);
}

// convert quaternions to euler angles (https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles#Quaternion_to_Euler_Angles_Conversion)
inline
V3f toEulerAngle(const V4f & q)
{
    V3f eulerAngles; 

    // roll (x-axis rotation)
    double sinr_cosp = 2.f * (q.w * q.x + q.y * q.z);
    double cosr_cosp = 1.f - 2.f * (q.x * q.x + q.y * q.y);
    eulerAngles.x = atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = +2.0 * (q.w * q.y - q.z * q.x);
    if (fabs(sinp) >= 1)
        eulerAngles.y = copysign(FPI / 2.f, sinp); // use 90 degrees if out of range
    else
        eulerAngles.y = asin(sinp);

    // yaw (z-axis rotation)
    double siny_cosp = 2.f * (q.w * q.z + q.x * q.y);
    double cosy_cosp = 1.f - 2.f * (q.y * q.y + q.z * q.z);
    eulerAngles.z = atan2(siny_cosp, cosy_cosp);

    return eulerAngles;
}

} // namespace shn
