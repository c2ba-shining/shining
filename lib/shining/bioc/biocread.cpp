#include "getopt.h"
#include <bioc/bioc_reader.h>
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

/* Short command line options for get_opt */
const char * SHORT_OPTIONS = "hvl";
/* Long options for getopt_long */
const struct option LONG_OPTIONS[] = {
    { "help", no_argument, NULL, 'h' }, /*  Print help */
    { "verbose", no_argument, NULL, 'v' }, /* Verbose mode */
    { "list", required_argument, NULL, 'l' }, /*  List mode */
    { NULL, 0, NULL, 0 } /*  End of array need by getopt_long do not delete it */
};

typedef unsigned char uint8_t;

/* Handle command line options */
typedef struct cli_opts_t
{
    uint8_t values;
    uint8_t list;
} cli_opts_t;

/* Parse command line options and fill BruteOptions structure */
void parse_cli_opts(int argc, char ** argv, cli_opts_t * opts);
/* Print usage on standard output */
void usage();

int main(int argc, char ** argv)
{
    cli_opts_t opts;
    parse_cli_opts(argc, argv, &opts);

    bioc_t cache;
    char * filePath = argv[argc - 1];
    int32_t status = bioc_open(&cache, filePath, BIOC_READ_NOLOAD);

    if (status != BIOC_NO_ERROR)
    {
        std::cout << "WRONG READ " << std::endl;
        return 1;
    }

    printf("File:%s\n", argv[argc - 1]);
    printf("Version:%lu\n", bioc_get_version(&cache));
    printf("Description:%s\n", bioc_get_description(&cache));
    printf("Entry count:%lu\n", bioc_get_chunk_count(&cache));
    printf("Tag count:%lu\n", bioc_get_map_count(&cache));

    uint64_t i;
    for (i = 0; i < bioc_get_map_count(&cache); ++i)
    {
        printf("hash:%s\n", bioc_get_map_name(&cache, i));
    }
    for (i = 0; i < bioc_get_chunk_count(&cache); ++i)
    {
        bioc_desc_t desc = bioc_get_desc(&cache, i);
        printf("desc:%lu:offset:%lu:size:%lu", i, desc.offset, desc.size);
        printf("\n");
        int dataSize = static_cast<int>(desc.size) / sizeof(char);
        char * buffer = new char[dataSize];
        bioc_read_chunk(&cache, i, buffer);
        printf("%s\n", buffer);
        delete[] buffer;
        uint64_t j;
        for (j = 0; j < bioc_get_map_count(&cache); ++j)
        {
            const char * value = bioc_get_desc_key_value(&cache, i, j);
            if (value)
            {
                printf(":tag:%s/%s", bioc_get_map_name(&cache, j), value);

                if (opts.values)
                {
                    if (strcmp(value, "integer") == 0)
                    {
                        printf("\n");
                        int dataSize = static_cast<int>(desc.size) / sizeof(int);
                        int * buffer = new int[dataSize];
                        bioc_read_chunk(&cache, i, buffer);

                        uint64_t k;
                        for (k = 0; k < dataSize; ++k)
                            printf("%d;", buffer[k]);

                        delete[] buffer;
                    }
                    else if (strcmp(value, "time") == 0 || strcmp(value, "float") == 0)
                    {
                        printf("\n");
                        int dataSize = static_cast<int>(desc.size) / sizeof(float);
                        float * buffer = new float[dataSize];
                        bioc_read_chunk(&cache, i, buffer);

                        uint64_t k;
                        for (k = 0; k < dataSize; ++k)
                            printf("%.2f;", buffer[k]);

                        delete[] buffer;
                    }
                }
            }
        }
        printf("\n");
    }

    bioc_close(&cache);

    return 0;
}

void parse_cli_opts(int argc, char ** argv, cli_opts_t * opts)
{
    char c;
    int optIdx;
    opts->values = 0;
    opts->list = 0;
    while ((c = getopt_long(argc, argv, (char *) SHORT_OPTIONS, (option *) LONG_OPTIONS, &optIdx)) != -1)
    {
        switch (c)
        {
        case 'h': /* Print usage and exit */
            usage();
            exit(1);
            break;
        case 'v': /* Display values mode */
            opts->values = 1;
            break;
        case 'l': /* List mode */
            opts->list = 1;
            break;
        case '?': /* Wut? */
            fprintf(stderr, "Unknown option\n");
            usage();
            exit(1);
        default:
            break;
        }
    }
}

void usage()
{
    printf("biocread [-hvl] $filepath$\n");
}
