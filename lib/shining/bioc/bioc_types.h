/*! Common Drone types
 *  \file  bioc_types.h
 */
#ifndef __BIOC_TYPES_H__
#define __BIOC_TYPES_H__

#define BIOC_HASH_MAP_SIZE 65000
#define BIOC_WRITER_VERSION 1

#ifdef _MSC_VER
typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/*! bioc error codes */
#define BIOC_NO_ERROR 0
#define BIOC_WRITE_ERROR -1
#define BIOC_READ_ERROR -2
#define BIOC_OPEN_FILE_ERROR -3
#define BIOC_INVALID_PARAMETER_ERROR -4
#define BIOC_INVALID_FILE_ERROR -5
#define BIOC_DESCRIPTION_TOO_LONG_ERROR -6

    /*! Id of a chunk in the chunk index */
    typedef uint64_t bioc_chunk_id_t;
    /*! Id of a map in the map list */
    typedef uint64_t bioc_map_id_t;

    /*! Descriptor of a chunk
     *  Contains the offset in the file and the size of a chunk
     */
    typedef struct
    {
        uint64_t offset;
        uint64_t size;
    } bioc_desc_t;

    /*! Hash map chunk descriptor
     *  Contains the id of the referenced chunk and
     *  the offset in the key value array
     */
    typedef struct
    {
        bioc_chunk_id_t chunk_id;
        uint64_t key_value_offset;
    } bioc_hash_desc_t;

    /*! Hash map cell
     *  Contains the number of chunks referenced
     *  and the offset in the descriptor array associated
     *  to the map
     */
    typedef struct
    {
        uint32_t offset;
        uint32_t count;
    } bioc_hash_cell_t;

    /*! Container for map data
     *  Contains chunk ids of the arrays
     *  used by the map
     */
    typedef struct
    {
        /*! Array of bioc_hash_cell_t */
        bioc_chunk_id_t hash_chunk_id;
        /*! Name of the map */
        bioc_chunk_id_t name_chunk_id;
        /*! Array of bioc_hash_desc_t */
        bioc_chunk_id_t descriptors_chunk_id;
        /*! String containing all key values separated by \0 */
        bioc_chunk_id_t value_strings_chunk_id;
    } bioc_map_container_t;

    /*! Container for header data
     */
    typedef struct
    {
        char description[256];
        uint64_t version;
        uint64_t index_offset;
        uint64_t chunk_count;
        /*! Array of bioc_map_container_t */
        bioc_chunk_id_t maps_chunk_id;
    } bioc_header_container_t;

#ifdef __cplusplus
}
#endif

#endif /* __BIOC_TYPES_H__ */
