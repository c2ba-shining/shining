/*! Drone reader functions an structures
 * \file  bioc_reader.h
 */
#ifndef __BIOC_READER_H__
#define __BIOC_READER_H__

#include "bioc_types.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*! Cache loading modes
 * BIOC_READ_LOAD   : load everything in memory using a single read*
 * Can not use read_desc in this mode
 */
#define BIOC_READ_LOAD 0
/*! Cache loading modes
 * BIOC_READ_MMAP   : map the whole file using "mmap" (unix) or "MapViewOfFile" (windows)
 * Can not use read_desc in this mode
 */
#define BIOC_READ_MMAP 1
/*! Cache loading modes
 * BIOC_READ_NOLOAD : load only header and chunk metadata
 * Can not use get_desc in this mode
 */
#define BIOC_READ_NOLOAD 2

    /*! A map associates chunks to string keys
     *  A map has a name, a list of possible values,
     *  and a list of chunks referenced by the key
     *  Quick matching is done using a hash-map
     */
    typedef struct
    {
        /*! Hash map */
        const bioc_hash_cell_t * hash;
        /*! Map name */
        const char * name;
        /*! Number of chunks referenced by the key */
        uint64_t chunk_count;
        /*! Array of chunk descriptors referenced by the map */
        const bioc_hash_desc_t * descriptors;
        /*! Array of possible key values separated by \0 char */
        const char * value_strings;
    } bioc_map_t;

    /*! Cache struture for reading
     */
    typedef struct
    {
        /*! Bootstrap header data */
        bioc_header_container_t * header;
        /*! First chunk */
        void * data;
        /*! Array of chunk descriptors */
        bioc_desc_t * descriptors;
        /*! Number of keys */
        uint64_t map_count;
        /*! Array of keys */
        bioc_map_t * maps;
        /*! Size of the mapped region */
        uint64_t mmap_size;
        /*! Start pointer for the mapped file */
        void * mmap_start;
#ifdef _MSC_VER
        /*! win32 file handle for mmap mode */
        void * w_fhandle;
        /*! win32 memory map handle */
        void * w_mhandle;
#endif
        /*! Read mode */
        int32_t mode;
        /*! File descriptor */
        int32_t fd;
    } bioc_t;

    /*! Open a cache file checks version and number of chunk to see
     *  if the cache file is valid.
     *  Returns BIOC_NO_ERROR,  BIOC_OPEN_ERROR, or BIOC_INVALID_FILE _ERROR
     */
    int32_t bioc_is_valid(const char * filename);

    /*! Open a cache file
     *  Load and prepare data/metadata according to the load policy
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_open(bioc_t * cache, const char * filename, int mode);
    /*! Close a cache file
     *  Free data and metadata according to load policy
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_close(bioc_t * cache);
    /*! Read bioc API version */
    uint64_t bioc_get_version(bioc_t * cache);
    /*! Read cache fixed size description  */
    const char * bioc_get_description(bioc_t * cache);
    /*! Retrieve the map id of the map named  "map_name"
     *  Returns the map id.
     */
    bioc_map_id_t bioc_get_map_id(bioc_t * cache, const char * map_name);
    /*! Get the number of keys in the cache */
    uint64_t bioc_get_map_count(bioc_t * cache);
    /*! Get the name of the key associated to "map_id" */
    const char * bioc_get_map_name(bioc_t * cache, bioc_map_id_t map_id);
    /*! Get the number of stored chunks */
    uint64_t bioc_get_chunk_count(bioc_t * cache);
    /*! Get the descriptor of the chunk referenced by "chunk_id" */
    bioc_desc_t bioc_get_desc(bioc_t * cache, bioc_chunk_id_t chunk_id);
    /*! Get a pointer on the chunk referenced by "chunk_id"  MMAP and LOAD mode only*/
    const void * bioc_get_chunk(bioc_t * cache, bioc_chunk_id_t chunk_id);
    /*! Read the chunk referenced by "chunk_id" from the disk NOLOAD mode only
     *  The chunk is read directly into "data" no allocations are made
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_read_chunk(bioc_t * cache, bioc_chunk_id_t chunk_id, void * data);
    /*! Get the value of the key associated to the chunk in the map  "map_id"
     *  Returns associated value, or 0 if the chunk is not associated to the key
     */
    const char * bioc_get_desc_key_value(bioc_t * cache, bioc_chunk_id_t chunk_id, bioc_map_id_t map_id);
    /*! Count the number of chunk referenced by the given map/key pair
     *  Returns the number of matching chunks
     */
    uint64_t bioc_count_matching_chunks(bioc_t * cache, bioc_map_id_t map_id, const char * key_value);
    /*! Retrieve chunk ids referenced by the given map/key pair
     *  "max_chunk_count" is the maximum number of matching chunk returned
     *  Chunk ids are stored into "matching_chunks", no allocation is performed
     *  Returns the number of matching chunks
     */
    uint64_t bioc_get_matching_chunks(bioc_t * cache, bioc_map_id_t map_id, const char * key_value, uint64_t max_chunk_count, bioc_chunk_id_t * matching_chunks);
    /*! Count number of chunk referenced by the union the given map/key pair list
     *  "map_count" is the number of maps in the union
     *  "map_ids" is the list of map ids in the union
     *  "key_values" is the list of key values in the union
     *  Returns the number of matching chunks
     */
    uint64_t bioc_count_matching_chunks_union(bioc_t * cache, uint64_t map_count, const bioc_map_id_t * map_ids, const char ** key_values);
    /*! Retrieve chunk ids referenced by the  key list union
     *  "map_count" is the number of maps in the union
     *  "map_ids" is the list of map ids in the union
     *  "key_values" is the list of key values in the union
     *  "max_chunk_count" is the maximum number of matching chunk returned
     *  Chunk ids are stored into "matching_chunks", no allocation is performed
     *  Returns the number of matching chunks
     */
    uint64_t
    bioc_get_matching_chunks_union(bioc_t * cache, uint64_t map_count, const bioc_map_id_t * map_ids, const char ** key_values, uint64_t max_chunk_count, bioc_chunk_id_t * matching_chunks);

#ifdef __cplusplus
}
#endif

#endif /* __BIOC_READER_H__ */
