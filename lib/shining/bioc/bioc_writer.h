/*! Drone writer functions and structures
 * \file bioc_writer.h
 */
#ifndef __BIOC_WRITER_H__
#define __BIOC_WRITER_H__

#include "bioc_types.h"

#ifdef __cplusplus
extern "C"
{
#endif

    /*! Structure for storing a key during writing
     *  Contains associated chunk ids
     */
    typedef struct
    {
        /*! Chunk ids associated to the key */
        bioc_chunk_id_t * descriptors;
        /*! Key value */
        char * value;
    } bioc_writer_key_t;

    /*! Structure for storing a map during writing
     *  Contains all possible keys and associated chunks
     */
    typedef struct
    {
        /*! List of keys and associated chunk ids */
        bioc_writer_key_t * keys;
        /*! Name of the map */
        char * name;
    } bioc_writer_map_t;

    /*! Writer structure for storing chunk metadata
     * during cache writing
     */
    typedef struct
    {
        /*! Fixed size cache description field */
        char description[256];
        /*! Map storage */
        bioc_writer_map_t * maps;
        /*! Lost of chunk descriptors */
        bioc_desc_t * descriptors;
        /*! Drone API version number */
        uint64_t version;
        /*! Number of stored chunks */
        uint64_t chunk_count;
        /*! Offset of the first chunk position in the file */
        uint64_t desc_start_offset;
        /*! Offset of the end of the last chunk in the file */
        uint64_t desc_end_offset;
        /*! File descriptor of the cache */
        int fd;
    } bioc_writer_t;

    /*! Opens a cache file and prepare metadata for writing.
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_open_writer(bioc_writer_t * cache, const char * filename, const char * description);

    /*! Closes the cache file, consolidate and store meta data
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_close_writer(bioc_writer_t * cache);

    /*! Closes the cache file, free memory without writing chunk ids and maps
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_discard_writer(bioc_writer_t * cache);

    /*! Creates a map with name "map_name"
     *  Returns the id of the created map
     */
    bioc_map_id_t bioc_writer_create_map(bioc_writer_t * cache, const char * map_name);

    /*! Retrieve the map id of the map named  "map_name"
     *  Returns the map id.
     */
    bioc_map_id_t bioc_writer_get_map_id(bioc_writer_t * cache, const char * map_name);

    /*! Add a "size" bytes of a chunk "data" in the cache. Prepare chunk metadata
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_writer_add_chunk(bioc_writer_t * cache, const void * data, uint64_t size);

    /*! Updates the data of the chunk referenced by id "chunk_id".
     * "size" has to match exactly the size of existing chunk
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_writer_update_chunk(bioc_writer_t * cache, bioc_chunk_id_t chunk_id, const void * data, uint64_t size);

    /*! Updates the data on a ranged memory space of the chunk referenced by id "chunk_id"
     *  "size" + "offset" has to be equal to the size of existing chunk or less
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_writer_update_chunk_range(bioc_writer_t * cache, bioc_chunk_id_t chunk_id, const void * data, uint64_t size, uint64_t offset);

    /*! Map a chunk with multiple map/key pairs
     *  "map_count" is the number of map/key pairs
     *  "map_ids" is the list of map ids
     *  "key_values" is the list of keys
     *  Returns 0, or a negative number if an error has happened.
     */
    int32_t bioc_writer_map_chunk(bioc_writer_t * cache, bioc_chunk_id_t chunk_id, uint64_t map_count, const bioc_map_id_t * map_ids, const char ** key_values);

    /*! Returns the last chunk id written in the cache
     */
    bioc_chunk_id_t bioc_writer_get_last_chunk_id(bioc_writer_t * cache);

#ifdef __cplusplus
}
#endif

#endif /* __BIOC_WRITER_H__ */
