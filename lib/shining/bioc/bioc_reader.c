#define _LARGEFILE64_SOURCE
#define _FILE_OFFSET_BITS 64

#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#include <windows.h>
#include <io.h>
#else
#include <unistd.h>
#include <sys/mman.h>
#endif /*_MSC_VER */
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <string.h>
#include <limits.h>

#include "bioc_reader.h"
#include "bioc_hash.h"

#define ALLOCATE(SIZE) malloc((size_t) SIZE)
#define FREE(PTR) free(PTR)
#define RESTRICT __restrict__

#ifdef _MSC_VER
#pragma warning(disable : 4996) /* Disable deprecated warning on posix functions */
#define OPEN_MODE O_RDONLY | _O_BINARY
#define READ_COUNT_CAST (int)
#define LSEEK _lseeki64
#define CLOSE _close
#define OPEN _open
#define READ _read
#define WRITE _write
#else
#define LSEEK lseek64
#define CLOSE close
#define OPEN open
#define READ read
#define WRITE write
#define OPEN_MODE O_RDONLY
#define READ_COUNT_CAST (uint64_t)
#endif /*_MSC_VER */

static uint64_t bioc_fsize(int fd)
{
    int64_t curr = LSEEK(fd, 0L, SEEK_CUR);
    uint64_t size = (uint64_t) LSEEK(fd, 0L, SEEK_END);
    LSEEK(fd, curr, SEEK_SET);
    return size;
}

int32_t bioc_is_valid(const char * filename)
{
    int32_t fd;

    fd = OPEN(filename, OPEN_MODE);
    if (fd != -1)
    {
        bioc_header_container_t * header;
        size_t bytes;

        header = ALLOCATE(sizeof(bioc_header_container_t));
        bytes = READ(fd, header, sizeof(bioc_header_container_t));
        if ((bytes != sizeof(bioc_header_container_t)) 
            || ( header->version != BIOC_WRITER_VERSION)
            || (! header->chunk_count)) 
        {
            FREE(header);
            CLOSE(fd);
            return BIOC_INVALID_FILE_ERROR;
        }
        FREE(header);
        CLOSE(fd);
    }
    else
    {
        return BIOC_OPEN_FILE_ERROR;
    }
    return BIOC_NO_ERROR;
}

int32_t bioc_open(bioc_t * cache, const char * filename, int mode)
{
    int fd = OPEN(filename, OPEN_MODE);
    if (fd <=0)
        return BIOC_OPEN_FILE_ERROR;
    cache->fd = fd;
    cache->mmap_size = bioc_fsize(fd);
    cache->mode = mode;
    if (mode == BIOC_READ_NOLOAD)
    {
        uint64_t i;
        size_t bytes;
        bioc_desc_t hashes_desc;
        bioc_map_container_t * map_containers;

        cache->mmap_start = 0;
        cache->data = 0;
        cache->header = ALLOCATE(sizeof(bioc_header_container_t));
        bytes = READ(fd, cache->header, sizeof(bioc_header_container_t));
        if(bytes != sizeof(bioc_header_container_t) || cache->header->chunk_count == 0)
            return BIOC_INVALID_FILE_ERROR;
        if (bioc_get_version(cache) != BIOC_WRITER_VERSION)
            return BIOC_INVALID_FILE_ERROR;
        cache->descriptors = ALLOCATE(cache->header->chunk_count * sizeof(bioc_desc_t));
        LSEEK(fd, cache->header->index_offset, SEEK_SET);
        bytes = READ(fd, cache->descriptors, READ_COUNT_CAST ((size_t) cache->header->chunk_count) * sizeof(bioc_desc_t));
        if(bytes != cache->header->chunk_count * sizeof(bioc_desc_t))
            return BIOC_INVALID_FILE_ERROR;
        hashes_desc =  bioc_get_desc(cache, cache->header->maps_chunk_id);
        cache->map_count = hashes_desc.size / sizeof(bioc_map_container_t);
        cache->maps = ALLOCATE(sizeof(bioc_map_t) * (size_t) cache->map_count);
        map_containers = ALLOCATE(hashes_desc.size);
        bioc_read_chunk(cache, cache->header->maps_chunk_id, map_containers);
        for (i = 0; i < cache->map_count; ++i)
        {
            bioc_desc_t d;
            const bioc_map_container_t * c = map_containers + i;
            bioc_map_t * h = cache->maps + i;

            h->hash = ALLOCATE(bioc_get_desc(cache, c->hash_chunk_id).size);
            bioc_read_chunk(cache, c->hash_chunk_id, (void *) h->hash);
            h->name = ALLOCATE(bioc_get_desc(cache, c->name_chunk_id).size);
            bioc_read_chunk(cache, c->name_chunk_id, (void *) h->name);
            d = bioc_get_desc(cache, c->descriptors_chunk_id);
            h->chunk_count = d.size / sizeof(bioc_hash_desc_t);
            h->descriptors = ALLOCATE(d.size);
            bioc_read_chunk(cache, c->descriptors_chunk_id, (void *) h->descriptors);
            h->value_strings = ALLOCATE(bioc_get_desc(cache, c->value_strings_chunk_id).size);
            bioc_read_chunk(cache, c->value_strings_chunk_id, (void *)h->value_strings);
        }
        FREE(map_containers);
    }
    else
    {
        uint64_t i;
        bioc_desc_t maps_desc;
        bioc_map_container_t * map_containers;

        if (mode == BIOC_READ_MMAP)
        {
#ifdef _MSC_VER
            wchar_t * w_filename;
            size_t bytes;

            CLOSE(fd);
            w_filename = ALLOCATE((strlen(filename)+1) * sizeof(wchar_t));
            bytes = mbstowcs(w_filename, filename, strlen(filename)+1);
            //cache->w_fhandle = CreateFile(L"test.bioc", GENERIC_READ, 0, 0, OPEN_EXISTING, 0, 0);
            cache->w_fhandle = CreateFile(w_filename, GENERIC_READ, 0, 0, OPEN_EXISTING, 0, 0);
            FREE(w_filename);
            if ((int) cache->w_fhandle  == HFILE_ERROR)
            {
                DWORD err = GetLastError();
                printf("ERROR %Ld\n", err);
                return BIOC_OPEN_FILE_ERROR;
            }
            cache->w_mhandle = CreateFileMapping(cache->w_fhandle, NULL, PAGE_READONLY, 0, 0, NULL);
            if (cache->w_mhandle  == NULL)
            {
                DWORD err = GetLastError();
                return BIOC_OPEN_FILE_ERROR;
            }
            cache->mmap_start = MapViewOfFile(cache->w_mhandle, FILE_MAP_READ, 0, 0, (size_t) cache->mmap_size);
            if (cache->mmap_start  == NULL)
            {
                DWORD err = GetLastError();
                return BIOC_OPEN_FILE_ERROR;
            }
#else
            cache->mmap_start = mmap(0, cache->mmap_size, PROT_READ, MAP_PRIVATE, fd, 0);
            CLOSE(fd);
#endif
        }
        else if (mode == BIOC_READ_LOAD)
        {
            uint64_t bytes_to_read = cache->mmap_size;
            uint64_t bytes_read = 0;
            cache->mmap_start = ALLOCATE(cache->mmap_size);
            while(bytes_to_read > 0)
            {
                int bytes_to_read_int = READ_COUNT_CAST bytes_to_read;
                 uint64_t bytes;
                if (bytes_to_read >= INT_MAX)
                    bytes_to_read_int = INT_MAX - 1;
               bytes = READ(fd, (char *) cache->mmap_start + bytes_read, bytes_to_read_int);
                if (bytes < 0)
                    return BIOC_INVALID_FILE_ERROR;
                bytes_to_read -= bytes;
                bytes_read += bytes;
            }
            if(bytes_to_read != 0 )
                return BIOC_INVALID_FILE_ERROR;
            CLOSE(fd);
        }
        cache->data = (char *) cache->mmap_start + sizeof(bioc_header_container_t);
        cache->header = (bioc_header_container_t *) cache->mmap_start;
        if (bioc_get_version(cache) != BIOC_WRITER_VERSION)
            return BIOC_INVALID_FILE_ERROR;
        cache->descriptors = (bioc_desc_t *) ((char *)cache->mmap_start + cache->header->index_offset);
        maps_desc =  bioc_get_desc(cache, cache->header->maps_chunk_id);
        cache->map_count = maps_desc.size / sizeof(bioc_map_container_t);
        map_containers = (bioc_map_container_t *) bioc_get_chunk(cache, cache->header->maps_chunk_id);
        cache->maps = ALLOCATE(sizeof(bioc_map_t) * (size_t) cache->map_count);
        for (i = 0; i < cache->map_count; ++i)
        {
            bioc_desc_t d;
            const bioc_map_container_t * c = map_containers + i;
            bioc_map_t * h = cache->maps + i;

            h->hash =  bioc_get_chunk(cache, c->hash_chunk_id);
            h->name =  bioc_get_chunk(cache, c->name_chunk_id);
            d = bioc_get_desc(cache, c->descriptors_chunk_id);
            h->chunk_count = d.size / sizeof(bioc_hash_desc_t);
            h->descriptors =  bioc_get_chunk(cache, c->descriptors_chunk_id);
            h->value_strings =  bioc_get_chunk(cache, c->value_strings_chunk_id);
        }
    }
    return BIOC_NO_ERROR;
}

int32_t bioc_close(bioc_t * cache)
{
    if (cache->mode == BIOC_READ_MMAP)
    {
        FREE(cache->maps);
#ifdef _MSC_VER
        UnmapViewOfFile(cache->mmap_start);
        CloseHandle(cache->w_mhandle);
        CloseHandle(cache->w_fhandle);
#else
        munmap(cache->mmap_start, cache->mmap_size);
#endif
    }
    else if (cache->mode == BIOC_READ_LOAD)
    {
        FREE(cache->maps);
        FREE(cache->mmap_start);
    }
    else /* BIOC_READ_NOLOAD */
    {
        int i;
        for (i = 0; i < cache->map_count; ++i)
        {
            bioc_map_t * h = cache->maps + i;
            FREE((void*) h->hash);
            FREE((void*) h->name);
            FREE((void*) h->descriptors);
            FREE((void*) h->value_strings);
        }
        FREE(cache->maps);
        FREE(cache->header);
        FREE(cache->descriptors);
        CLOSE(cache->fd);
    }
    return BIOC_NO_ERROR;
}

uint64_t bioc_get_version(bioc_t * cache)
{
    assert(cache);
    return cache->header->version;
}

const char * bioc_get_description(bioc_t * cache)
{
    assert(cache);
    return cache->header->description;
}

bioc_map_id_t  bioc_get_map_id(bioc_t *cache, const char * name)
{
    
    uint64_t i;
    for (i = 0; i < cache->map_count; ++i)
    {
        if(strcmp(cache->maps[i].name, name) == 0)
        {
            return (bioc_map_id_t) i;
        }
    }
    return -1;
}

uint64_t bioc_get_map_count(bioc_t * cache)
{
    assert(cache);
    return cache->map_count;
}

const char * bioc_get_map_name(bioc_t * cache, uint64_t map_id)
{
    assert(cache);
    assert(cache->map_count > map_id);
    return cache->maps[map_id].name;
}

uint64_t bioc_get_chunk_count(bioc_t *cache)
{
    assert(cache);
    return cache->header->chunk_count;
}

bioc_desc_t bioc_get_desc(bioc_t * cache, bioc_chunk_id_t chunk_id)
{
    assert(cache->header->chunk_count > chunk_id);
    return * (cache->descriptors + chunk_id);
}

const void * bioc_get_chunk(bioc_t * cache, bioc_chunk_id_t chunk_id)
{
    bioc_desc_t * desc;

    assert(cache);
    assert(cache->header->chunk_count > chunk_id);
    assert(cache->mode != BIOC_READ_NOLOAD);
    desc = (cache->descriptors + chunk_id);
    return (char *) cache->data + desc->offset;
}

int32_t bioc_read_chunk(bioc_t * cache, bioc_chunk_id_t chunk_id, void * data)
{
    size_t bytes;
    bioc_desc_t * desc = (cache->descriptors + chunk_id);

    assert(cache->mode == BIOC_READ_NOLOAD);
    assert(cache->header->chunk_count > chunk_id);
    LSEEK(cache->fd, desc->offset + sizeof(bioc_header_container_t), SEEK_SET);
#ifdef _MSC_VER
    bytes = (size_t)desc->size;
    while(bytes)
    {
        unsigned char* dataPtr = (unsigned char*)data;
        unsigned int count = bytes > (size_t)INT_MAX ? INT_MAX : (unsigned int)bytes;
        if( -1 == READ(cache->fd, dataPtr, count) )
            return BIOC_READ_ERROR;
        dataPtr += count;
        bytes -= count;
    }
#else
    bytes = READ(cache->fd, data, READ_COUNT_CAST (size_t) desc->size);
    if(bytes != desc->size)
        return BIOC_READ_ERROR;
#endif
    return BIOC_NO_ERROR;
}

const char * bioc_get_desc_key_value(bioc_t * cache, bioc_chunk_id_t chunk_id, bioc_map_id_t map_id)
{
    const bioc_map_t * h = cache->maps + map_id;
    uint64_t hash_chunk_count = h->chunk_count;
    const bioc_hash_desc_t * hash_descriptors = h->descriptors;
    const char * value_strings = h->value_strings;
    uint64_t i = 0;

    while (i < hash_chunk_count)
    {
        if (hash_descriptors[i].chunk_id == chunk_id)
            return value_strings + hash_descriptors[i].key_value_offset;
        ++i;
    }
    return BIOC_NO_ERROR;
}

/*! Look for chunk_id in chunk_ids. Returns 1 if found, else returns 0 */
int32_t bioc_is_in(uint64_t count, bioc_chunk_id_t * chunk_ids, bioc_chunk_id_t chunk_id)
{
    uint64_t d_idx = 0;
    while (d_idx < count)
    {
        if (chunk_ids[d_idx] == chunk_id)
            return 1;
        ++d_idx;
    }
    return 0;
}

uint64_t bioc_count_matching_chunks_union(bioc_t * cache,
                                         uint64_t map_count, const bioc_map_id_t * map_ids, const char ** key_values)
{
    uint64_t map_idx;
    uint64_t d_idx;
    uint64_t matching_chunk_count = 0;
    /* Identify map with minimum amount of descriptors associated to corresponding value */
    uint64_t min_desc_by_val = -1;
    uint64_t * matching_chunk_counts = (uint64_t *) ALLOCATE(map_count * sizeof(uint64_t));
    bioc_chunk_id_t ** matching_chunk_ids = (bioc_chunk_id_t **) ALLOCATE(map_count * sizeof(bioc_chunk_id_t **));
    uint64_t min_desc_by_val_map_id = 0;

    for (map_idx = 0; map_idx < map_count; ++map_idx)
    {
        bioc_map_id_t map_id = map_ids[map_idx];
        uint64_t count = bioc_count_matching_chunks(cache, map_id, key_values[map_idx]);

        /* Get matching ids of each map */
        matching_chunk_counts[map_idx] = count;
        matching_chunk_ids[map_idx] = (bioc_chunk_id_t *) ALLOCATE(count * sizeof(bioc_chunk_id_t));
        bioc_get_matching_chunks(cache, map_id, key_values[map_idx], count, matching_chunk_ids[map_id]);
        if (count > 0 && count < min_desc_by_val)
        {
            min_desc_by_val = count;
            min_desc_by_val_map_id = map_idx;
        }
    }
    /* Foreach desc in map with minimum matching descriptors */
    for (d_idx = 0 ; d_idx < matching_chunk_counts[min_desc_by_val_map_id]; ++d_idx)
    {
        int32_t match = 1;
        bioc_chunk_id_t matching_desc = matching_chunk_ids[min_desc_by_val_map_id][d_idx];

        /* Look for the desc in other matching desc lists */
        map_idx = 0;
        while (map_idx < map_count && match)
        {
            if (map_idx != min_desc_by_val_map_id)
            {
                match = bioc_is_in(matching_chunk_counts[map_idx], matching_chunk_ids[map_idx], matching_desc);
            }
            ++map_idx;
        }
        if (match)
            ++matching_chunk_count;
    }

    FREE(matching_chunk_counts);
    for (map_idx = 0; map_idx < map_count; ++map_idx)
        FREE(matching_chunk_ids[map_idx]);
    FREE(matching_chunk_ids);
    return matching_chunk_count;
}

uint64_t    bioc_get_matching_chunks_union(bioc_t * cache,
                                          uint64_t map_count, const bioc_map_id_t * map_ids, const char ** key_values,
                                          uint64_t max_chunk_count, bioc_chunk_id_t * matching_chunks)
{
    uint64_t map_idx;
    uint64_t matching_chunk_count = 0;
    uint64_t d_idx;
    /* Identify map with minimum amount of descriptors associated to corresponding value */
    uint64_t * matching_chunk_counts = (uint64_t *) ALLOCATE(map_count * sizeof(uint64_t));
    bioc_chunk_id_t ** matching_chunk_ids = (bioc_chunk_id_t **) ALLOCATE(map_count * sizeof(bioc_chunk_id_t **));
    uint64_t min_desc_by_val_map_id = 0;

    for (map_idx = 0; map_idx < map_count; ++map_idx)
    {
        bioc_map_id_t map_id = map_ids[map_idx];
        uint64_t count = bioc_count_matching_chunks(cache, map_id, key_values[map_idx]);

        /* Get matching ids of each map */
        matching_chunk_counts[map_idx] = count;
        matching_chunk_ids[map_idx] = (bioc_chunk_id_t *) ALLOCATE(count * sizeof(bioc_chunk_id_t));
        bioc_get_matching_chunks(cache, map_id, key_values[map_idx], count, matching_chunk_ids[map_idx]);
        if (count > 0 && count < min_desc_by_val_map_id)
        {
            min_desc_by_val_map_id = map_idx;
        }
    }
    /* Foreach desc in map with minimum matching descriptors */
    d_idx =0;
    while (d_idx < matching_chunk_counts[min_desc_by_val_map_id] && matching_chunk_count < max_chunk_count)
    {
        int32_t match = 1;
        bioc_chunk_id_t matching_desc = matching_chunk_ids[min_desc_by_val_map_id][d_idx];

        /* Look for the desc in other matching desc lists */
        map_idx = 0;
        while (map_idx < map_count && match)
        {
            if (map_idx != min_desc_by_val_map_id)
            {
                match = bioc_is_in(matching_chunk_counts[map_idx], matching_chunk_ids[map_idx], matching_desc);
            }
            ++map_idx;
        }
        if (match)
        {
            matching_chunks[matching_chunk_count] = matching_desc;
            ++matching_chunk_count;
        }
        ++d_idx;
    }

    FREE(matching_chunk_counts);
    for (map_idx = 0; map_idx < map_count; ++map_idx)
        FREE(matching_chunk_ids[map_idx]);
    FREE(matching_chunk_ids);
    return matching_chunk_count;
}

uint64_t bioc_count_matching_chunks(bioc_t * cache,
                                   bioc_map_id_t map_id, const char * key_value)
{
    const bioc_map_t * h = cache->maps + map_id;
    const bioc_hash_cell_t * hash_map = h->hash;
    const bioc_hash_desc_t * hash_descriptors = h->descriptors;
    const char * hash_values = h->value_strings;
    uint32_t cell_idx = bioc_oat_hash((void * )key_value, (uint32_t) strlen(key_value))%BIOC_HASH_MAP_SIZE;
    uint64_t count = 0;
    uint64_t d_idx;
    for (d_idx = 0; d_idx < hash_map[cell_idx].count; ++d_idx)
    {
        if (!strcmp(hash_values + hash_descriptors[hash_map[cell_idx].offset + d_idx].key_value_offset, key_value))
            ++count;
    }
    return count;
}

uint64_t  bioc_get_matching_chunks(bioc_t * cache,
                                  bioc_map_id_t map_id, const char * key_value,
                                  uint64_t max_chunk_count, bioc_chunk_id_t * matching_chunks)
{
    const bioc_map_t * h = cache->maps + map_id;
    const bioc_hash_cell_t * hash_map = h->hash;
    const bioc_hash_desc_t * hash_descriptors = h->descriptors;
    const char * hash_values = h->value_strings;
    uint32_t cell_idx = bioc_oat_hash((void * )key_value, (uint32_t) strlen(key_value))%BIOC_HASH_MAP_SIZE;
    uint64_t count = 0;
    uint64_t d_idx = 0;
    while( count < max_chunk_count && d_idx < hash_map[cell_idx].count )
    {
        if (!strcmp(hash_values + hash_descriptors[hash_map[cell_idx].offset + d_idx].key_value_offset, key_value))
        {
            matching_chunks[count] = hash_descriptors[hash_map[cell_idx].offset + d_idx].chunk_id;
            ++count;
        }
        ++d_idx;
    }
    return count;
}
