#pragma once

#ifndef __SHINING_SCENE_H__
#define __SHINING_SCENE_H__

#include "SceneAttrHelpers.h"
#include "Shining.h"
#include <cstring>
#include <iosfwd>
#include <vector>

namespace shn
{
class SceneImpl;
class OslRenderer;

/// [[[rpc remote_class ]]]
class SHINING_EXPORT Scene
{
public:
    typedef int32_t Id;
    typedef int32_t Status;

    struct Ray
    {
        float o_x;
        float o_y;
        float o_z;

        float d_x;
        float d_y;
        float d_z;

        float tnear;
    };

    struct Hit
    {
        Id instanceId;
        Id faceId;
        float d;
        float u;
        float v;
    };

    enum TransformSpace
    {
        TRANSFORM_MODEL = 0,
        TRANSFORM_WORLD,
        TRANSFORM_NORMAL_WORLD,
        TRANSFORM_NORMAL_MODEL
    };

    enum InstanceVisibility
    {
        INSTANCE_VISIBILITY_PRIMARY = 0,
        INSTANCE_VISIBILITY_SECONDARY,
        INSTANCE_VISIBILITY_SHADOW,
        INSTANCE_VISIBILITY_COUNT,
    };

    enum Visibility
    {
        VISIBILITY_HIDDEN = 0,
        VISIBILITY_VISIBLE,
        VISIBILITY_BLACKHOLE,
        VISIBILITY_COUNT,
    };

    enum BufferOwnership
    {
        USE_BUFFER = 0,
        COPY_BUFFER
    };

    enum FramebufferFormat
    {
        FRAMEBUFFER_RGB = 0
    };

public:
    Scene();
    ~Scene();

    Status attribute(const char * name, const char * value);
    Status attribute(const char * name, float value);
    /// [[[rpc value: count=count, order=9 ]]]
    Status attribute(const char * name, const float * value, int32_t count);
    Status attribute(const char * name, int32_t value);

    /// [[[rpc meshIds: out, count=count, order=9 ]]]
    Status genMesh(Id * meshIds, int32_t count);

    /**
     *  [[[rpc data: count=count*componentCount;
     *         ownership: value=Scene::COPY_BUFFER ]]]
     */
    Status meshAttributeData(Id meshId, const char * attrName, int32_t count, int32_t componentCount, const float * data, BufferOwnership ownership);

    /**
     *  [[[rpc data: count=count*componentCount;
     *         ownership: value=Scene::COPY_BUFFER ]]]
     */
    Status meshAttributeData(Id meshId, const char * attrName, int32_t count, int32_t componentCount, const int * data, BufferOwnership ownership);

    /**
     *  [[[rpc data: count=length;
     *         ownership: value=Scene::COPY_BUFFER ]]]
     */
    Status meshAttributeData(Id meshId, const char * attrName, int32_t length, const char * data, BufferOwnership ownership);

    /**
     *  [[[rpc faces: count=count, nullable;
     *         ownership: value=Scene::COPY_BUFFER ]]]
     */
    Status meshFaceData(Id meshId, int32_t count, const int32_t * faces, BufferOwnership ownership);

    /**
     *  @brief Allocate several topologies.
     *
     *  It might be necessary to use several topology for a single mesh to
     *  support face-varying data (e.g. uv) while being ready for subdivision.
     *
     *  All topologies must be consistent against the same faces description.
     */
    Status enableMeshTopology(Id meshId, int32_t count);

    /**
     *  [[[rpc vertices: count=count;
     *         ownership: value=Scene::COPY_BUFFER ]]]
     */
    Status meshTopologyData(Id meshId, int32_t topologyId, int32_t count, const int32_t * vertices, BufferOwnership ownership);

    /// Preallocate some vertex attributes
    Status enableMeshVertexAttributes(Id meshId, int32_t count);

    /**
     *  @param attrName "position", "normal", "texCoord" or any other shading attribute.
     *                  "position" must be set.
     *
     *  [[[rpc data: count=count*componentCount;
     *         ownership: value=Scene::COPY_BUFFER ]]]
     */
    Status meshVertexAttributeData(Id meshId, int32_t topologyId, const char * attrName, uint32_t count, int32_t componentCount, const float * data, BufferOwnership ownership);

    /// [[[rpc ignore_function ]]]
    Status meshVertexAttributeData(
        Id meshId, int32_t topologyId, const char * attrName, uint32_t count, int32_t componentCount, const float ** data, const float * timeValues, const int32_t frameCount,
        BufferOwnership ownership);

    Status enableMeshFaceAttributes(Id meshId, int32_t count);

    /**
     *  [[[rpc data: count=count*componentCount;
     *         ownership: value=Scene::COPY_BUFFER ]]]
     */
    Status meshFaceAttributeData(Id meshId, const char * attrName, uint32_t count, int32_t componentCount, const float * data, BufferOwnership ownership);

    /**
     *  @param attrName "materialGroupIndex", "materialGroupChunks".
     *
     *  [[[rpc data: count=count*componentCount;
     *         ownership: value=Scene::COPY_BUFFER ]]]
     */
    Status meshFaceAttributeData(Id meshId, const char * attrName, uint32_t count, int32_t componentCount, const int * data, BufferOwnership ownership);

    Status commitMesh(Id meshId);

    /// [[[rpc instanceIds: out, count=count, order=9 ]]]
    Status genInstances(Id * instanceIds, int32_t count);

    /// [[[rpc instanceIds: count=count, order=9 ]]]
    Status instanceMesh(const Scene::Id * instanceIds, const int32_t count, const Scene::Id meshId);

    /// [[[rpc instanceIds: count=count, order=9 ]]]
    Status instanceName(const Scene::Id * instanceIds, const int32_t count, const char * name);

    /**
     * [[[rpc instanceIds: count=count, order=9;
              transform: count=16*frameCount, order=9;
     *        timeValues: count=frameCount, nullable, order=9 ]]]
     */
    Status instanceTransform(const Id * instanceIds, const int32_t count, const float * transform, const float * timeValues, const int32_t frameCount);

    /**
    * [[[rpc instanceIds: count=count, order=9;
             transforms: count=16*count, order=9 ]]]
    */
    Status instanceInstantiatedTransforms(const Id * instanceIds, const float * transforms, const int32_t count, const float timeValue);

    /// [[[rpc instanceIds: count=count, order=9 ]]]
    Status instanceVisibility(const Id * instanceIds, const int32_t count, InstanceVisibility type, Visibility value);

    /**
    * [[[rpc instanceIds: count=count, order=9;
    */
    Status instanceInstantiatedVisibilities(const Id * instanceIds, Visibility * values, const int32_t count, InstanceVisibility type);

    /**
    *  [[[rpc instanceIds: count=count, order=9;
              data: count=dataByteSize, order=9;
              timeValues: count=frameCount, order=9;
              ownership: value=Scene::COPY_BUFFER ]]]
    */
    Status instanceGenericAttributeData(
        const Scene::Id * instanceIds, const int32_t count,
        const char * attrName, const char * typeName, const int32_t dataByteSize, const int32_t dataCount, const char * data,
        const float * timeValues, int32_t frameCount, BufferOwnership ownership);

    /// [[[rpc ignore_function ]]]
    Status getGenericInstanceAttributeData(Scene::Id instanceId, const char * attrName, int32_t * count, int32_t * countComponent, const void ** data, float time);

    /// [[[rpc ignore_function ]]]
    Status getGenericInstanceAttributeType(Scene::Id instanceId, const char * attrName, const char ** typeName);

    /// [[[rpc ignore_function ]]]
    bool getInstanceVisibility(Scene::Id instanceId, InstanceVisibility visibility) const;

    /// [[[rpc instanceIds: count=count, order=9 ]]]
    Status commitInstance(const Scene::Id * instanceIds, const int32_t count);

    int32_t countInstanceIdFromName(const char * name) const;

    /// [[[rpc ignore_function ]]]
    void getInstanceIdsFromName(const char * name, Id * instanceIds, const int32_t instanceCount) const;

    /// [[[rpc cameraIds: out, count=count, order=9 ]]]
    Status genCameras(Id * cameraIds, int32_t count);

    /// [[[rpc data: count=dataByteSize ]]]
    Status cameraAttribute(Id cameraId, const char * attrName, const char * attrType, int32_t dataByteSize, const char * data);

    /// [[[rpc ignore_function ]]]
    Status cameraAttribute(Id cameraId, const char * attrName, const char * attrType, int32_t count, const void ** data, int32_t frameCount, const float * timeValues);

    Status commitCamera(Id cameraId);

    Id getCameraIdFromName(const char * name) const;

    /**
     * [[[rpc cameraToWorld: out, count=16;
     *        screenToCamera: out, count=16 ]]]
     */
    Status getCameraTransforms(Id cameraId, float * cameraToWorld, float * screenToCamera, float timeSample = 0.f) const;

    /// [[[rpc ignore_function ]]]
    const char * getCameraName(Id cameraId) const;

    /**
     * [[[rpc focalDistance: out, count=1;
     *        lensRadius: out, count=1 ]]]
     */
    Status getCameraDepthOfField(Id cameraId, float * focalDistance, float * lensRadius) const;
    int32_t getCameraCount() const;

    /// [[[rpc renderer: remote_object ]]]
    Status commitScene(OslRenderer * renderer = nullptr);

    /// [[[rpc ignore_function ]]]
    /// Waiting float-3 buffers for min and max
    /// No allocation will be done by Shining
    void getSceneBoundingBox(float * const min, float * const max);

    /// [[[rpc hit: out ]]]
    Status intersect(const Scene::Ray & ray, uint8_t rayType, Scene::Hit & hit);

    /**
     * @brief Compute a vertex attribute where the ray intersects the geometry.
     * @param [out] vertexData: the interpolated data.
     * @param [out] derivativeData: the partial derivative of the attribute against another attribute (param derivativeAttributeId).
     *                              sizeof(derivativeData) == sizeof(vertexAttributeId) * sizeof(derivativeAttributeId).
     *                              NULL to avoid computation.
     *
     * [[[rpc ignore_function ]]]
     */
    Status sampleInstanceVertexAttribute(
        Id instanceId, int32_t vertexAttributeId, const Scene::Hit & hit, TransformSpace space, float * vertexData, float * derivativeData, int32_t derivativeAttributeId);

    /**
     * @brief Write a JSON formatted report to the stream.
     *
     * [[[rpc ignore_function ]]]
     */
    Status statisticsReport(std::ostream & out) const;

    /// [[[rpc ignore_function ]]]
    SceneImpl * impl();

    /// [[[rpc ignore_function ]]]
    const SceneImpl * impl() const;

private:
    Scene(const Scene &);
    Scene & operator=(const Scene &);

private:
    SceneImpl * m_impl;
};
}; // namespace shn

#endif // __SHINING_SCENE_H__
