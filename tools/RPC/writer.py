import logging


BOOST_NS = 'boost'
BOOST_THREAD_INC = '#include <boost/thread.hpp>'
BOOST_TRAITS_INC = '#include <boost/type_traits.hpp>'
BOOST_NULL = 'NULL'

CPP11_NS = 'std'
CPP11_THREAD_INC = '#include <thread>\n#include <future>'
CPP11_TRAITS_INC = '#include <type_traits>'
CPP11_NULL = 'nullptr'


def iter_parents(element):
    element = element['parent']
    while element:
        yield element
        element = element['parent']


def is_ctor(func):
    return not func['type'] and '~' not in func['name']


def is_dtor(func):
    return not func['type'] and '~' in func['name']


def is_free_function(func):
    return (not func['parent'] or
            func['static'] == True or
            func['parent']['kind'] not in ('class', 'struct'))


def _get_naked_typename(type_name):
    "remove const, * and & from the type"
    return type_name.replace('const ', '').replace('*', '').replace('&', '').strip()


def _is_in_content(element, name, is_type):
    name = _get_naked_typename(name.split('::')[0])
    for child in element['content']:
        if ( child['name'] == name and
             (not is_type or child['kind'] != 'function')):
            logging.debug('%s in %s', name, element['name'])
            return True
    logging.debug('%s not in %s', name, element['name'])
    return False


def _get_full_typename(func, name, is_type=True):
    "return the type with the full scope"
    import itertools
    scope = itertools.dropwhile(lambda x: not _is_in_content(x, name, is_type),
                                iter_parents(func))
    naked = _get_naked_typename(name)
    return name.replace(naked, '::'.join(list(reversed([p['name'] for p in scope]))+[naked]))


def _get_element(el, type_name):
    type_name = _get_naked_typename(type_name)
    import itertools
    try:
        parent = next(itertools.dropwhile(lambda x: not _is_in_content(x, type_name, True),
                                          iter_parents(el)))
    except StopIteration:
        return None
    
    for scope_el in type_name.split('::'):
        for child in parent['content']:
            if child['name'] == scope_el:
                parent = child
                break
    return child


def _get_external_elements(filename, curr_el):
    "get the children elements declared in another file"
    if isinstance(curr_el, list):
        result = []
        for child in curr_el:
            result += _get_external_elements(filename, child)
        return result
    
    if (curr_el['location']['file'] != filename
         and curr_el['kind'] != 'namespace'):
        return []
    
    if curr_el['kind'] in ('struct', 'class', 'namespace'):
        # recurs
        result = []
        for child in curr_el['content']:
            result += _get_external_elements(filename, child)
        return result
    elif curr_el['kind'] == 'function':
        el = _get_element(curr_el, curr_el['type'])
        result = [el] if (el and el['location']['file'] != filename) else []
        for p in curr_el['params']:
            if not p.has_key('meta_value'):
                el = _get_element(curr_el, p['type'])
                if el and el['location']['file'] != filename:
                    result.append(el)
        return result
    elif curr_el['kind'] == 'typedef':
        el = _get_element(curr_el, curr_el['type'])
        return [el] if (el and el['location']['file'] != filename) else []
    return []

class RPCFile(object):
    def __init__(self, filename):
        self._f = open(filename, 'w+')
        self._indent = ''
        self._ns = []
        self._classes = []
    def indent(self):
        self._indent = ' ' * (len(self._indent)+4)
    def unindent(self):
        self._indent = ' ' * (len(self._indent)-4)
    def write(self, s):
        self._f.write('{0}{1}\n'.format(self._indent, s))
    def close(self):
        self.close_scope()
        self._f.close()
    def update_scope(self, element):
        if element['kind'] == 'namespace':
            return
        
        if not element['parent']:
            self.close_scope()
        elif element['parent']['kind'] == 'namespace':
            for _ in self._classes:
                self._remove_compound()
            ns_list = reversed([p['name'] for p in iter_parents(element)])
            for i, ns in enumerate(ns_list):
                if i >= len(self._ns):
                    self._add_namespace(ns)
                elif ns != self._ns[i]:
                    for _ in self._ns[i:]:
                        self._remove_namespace()
                    self._add_namespace(ns)
        elif element['parent']['kind'] in ('class', 'struct'):
            while self._classes:
                if self._classes[-1] == element['parent']['name']:
                    break
                self._remove_compound()
    def close_scope(self):
        for _ in self._classes:
            self._remove_compound()
        for _ in self._ns:
            self._remove_namespace()
    def _add_namespace(self, ns):
        self.write('namespace {0} {{'.format(ns))
        self.indent()
        self._ns.append(ns)
    def _remove_namespace(self):
        self._ns.pop()
        self.unindent()
        self.write('}')
    def _add_compound(self, kind, name):
        self.write('{0} {1}'.format(kind, name))
        self.write('{')
        self.indent()
        self._classes.append(name)
    def _remove_compound(self):
        self._classes.pop()
        self.unindent()
        self.write('};')


class ClientH(RPCFile):
    def __init__(self, filename, elements, is_cpp11):
        super(ClientH, self).__init__('rpc_{0}_client.h'.format(filename))
        self.write('#pragma once')
        self.write(CPP11_THREAD_INC if is_cpp11 else BOOST_THREAD_INC)
        self.write('#include <cstdint>')

        includes = set()
        forwards = set()
        for ext_el in _get_external_elements(filename+'.h', elements):
            # include if cannot forward declare it
            if ( ext_el['kind'] not in ('struct', 'class') or
                 ext_el['parent'] and ext_el['parent']['kind'] != 'namespace'):
                if ext_el['location']['file'] not in includes:
                    self.close_scope()
                    self.write('#include "rpc_{0}_client.h"'.format(ext_el['location']['file'].replace('.h','')))
                    includes.add(ext_el['location']['file'])
            elif ext_el['name'] not in forwards:
                self.update_scope(ext_el)
                self.write('{0} {1};'.format(ext_el['kind'], ext_el['name']))
                forwards.add(ext_el['name'])
    
    def write_element(self, el, is_cpp11):
        ns = CPP11_NS if is_cpp11 else BOOST_NS
        self.update_scope(el)
        if el['kind'] in ('struct', 'class'):
            self._add_compound(el['kind'], el['name'])
            if el.get('meta_remote'):
                self.unindent()
                self.write('    mutable {0}::future<int> m_futureId;'.format(ns))
                self.write('    mutable int m_id;')
                self.write('public:')
                self.write('    int rpcInstance() const { return (m_id==-1) ? (m_id=m_futureId.get()) : m_id; }')
                self.indent()
        
        elif el['kind'] == 'typedef':
            self.write('typedef {0} {1};'.format(el['type'], el['name']))
        
        elif el['kind'] == 'enum':
            self.write('enum {0} {{'.format(el['name']))
            for v in el['values']:
                self.write('    {0},'.format(v))
            self.write('};')
        
        elif el['kind'] == 'function':
            docs = [doc for doc in el['doc'] if not doc.startswith('[[[rpc')]
            if docs:
                self.write('/**')
                for doc in docs:
                    self.write(' *  {0}'.format(doc))
                    self.write(' *')
                self.write(' */')
            
            callback = {'type': '{0}::function<void ({1})>'.format(ns, '' if (el['type'] == 'void') else el['type']),
                        'name': 'callback',
                        'default': CPP11_NULL if is_cpp11 else BOOST_NULL}
            
            if is_ctor(el) or is_dtor(el):
                func = '{0}('.format(el['name'])
            else:
                func = '{0}::future< {1} > {2}('.format(ns, el['type'], el['name'])
            func += ', '.join('{0} {1}{2}'.format(p['type'], 
                                                  p['name'],
                                                  ' = %(default)s' % p if p.has_key('default') else '') 
                              for p in el['params'] + ([] if (is_ctor(el) or is_dtor(el)) else [callback] )
                              if not p.has_key('meta_value'))
            func += ');'
            self.write(func)
        
        elif el['kind'] == 'variable':
            is_remote = el['parent'].get('meta_remote')
            if is_remote:
                logging.warning('public attribute on remote class are not supported')
                return
            self.write('{0} {1};'.format(el['type'], el['name']))


def _meta_count_is_constant(func, param):
    "rpc_read the meta data to decide whether a container has a constant count of items"
    count = param.get('meta_count')
    if not count:
        # implicitly = 1
        return True
    
    # if only digits and sizeof, it is const
    # first, replace sizeof by "1"
    while True:
        sizeof_start = count.find('sizeof')
        if sizeof_start == -1:
            break
        sizeof_end = sizeof_start
        parenthesis_count = 0
        started = False
        for i, c in enumerate(count[sizeof_start:]):
            if c == '(':
                parenthesis_count += 1
                started = True
            elif c == ')':
                parenthesis_count -= 1
            
            if started and parenthesis_count <= 0:
                sizeof_end = sizeof_start + i + 1
                break
        assert parenthesis_count == 0
        assert sizeof_end > sizeof_start
        count = count[:sizeof_start] + '1' + count[sizeof_end:]
        break
    try:
        eval(count, {})
    except NameError:
        return False
    return True


def _get_dynamic_size(func, is_cpp11, param, is_NULL=False):
    "get the size that must be allocated dynamically"
    typename = _get_full_typename(func, param['type'])
    count = param.get('meta_count')
    flags = []
    if param.get('meta_nullable'):
        flags.append('RPC_NULLABLE')
    if is_NULL:
        flags.append('RPC_FORCE')
    size = 'rpc< {0} >::dynamic_size({1}, {2}, {3})'.format(typename,
      '{0}::remove_reference< {1} >::type()'.format(CPP11_NS if is_cpp11 else BOOST_NS, typename) if is_NULL else param['name'],
      count or -1,
      ' | '.join(flags) or '0')
    return size


def _get_static_size(func, param):
    "get the size that can be allocated statically"
    typename = _get_full_typename(func, param['type'])
    size = 'rpc< {0} >::static_size'.format(typename)
    count = param.get('meta_count')
    if count:
        if not _meta_count_is_constant(func, param):
            return '0'
        size += '*({0})'.format(count)
    if param.get('meta_nullable'):
        size = '(sizeof(char)+{0})'.format(size)
    return size


def _is_in(param):
    "Decide if the parameter should be sent to the server or retrieve from it"
    #if 'direction' in parameter:
    #    return parameter['direction'] == 'in'
    #return is_by_value(parameter) or is_const(parameter) or is_remote_object(parameter)
    return param.get('meta_direction') != 'out'


def _has_fixed_value(param):
    """
    Two cases:
    - the parameter should be omitted from the declaration, or
    - it is useful on the client side, not on the server side.
    """
    return param.has_key('meta_value') or param.has_key('meta_server_value')

def _get_fixed_value(param):
    if param.has_key('meta_value'): return param['meta_value']
    elif param.has_key('meta_server_value'): return param['meta_server_value']
    return None


def _get_flags(el, param):
    flags = 'RPC_NULLABLE' if param.get('meta_nullable') else '0'
    flags += ' | '
    flags += '({0} ? RPC_FORCE : 0)'.format(_get_static_size(el, param))
    return flags


class RPCImplFile(RPCFile):
    def _write_traits(self, is_cpp11):
        ns = CPP11_NS if is_cpp11 else BOOST_NS
        null = CPP11_NULL if is_cpp11 else BOOST_NULL
        
        self.write('enum {')
        self.write('    RPC_NULLABLE = 1,')
        self.write('    RPC_FORCE = 2,')
        self.write('};')
        
        self.write('template <class T> struct rpc')
        self.write('{')
        self.write('    enum { static_size = sizeof(T) };')
        self.write('    typedef typename {0}::remove_reference<T>::type TNoRef;'.format(ns))
        self.write('    typedef typename {0}::add_pointer<typename {0}::remove_const<TNoRef>::type>::type TNoCPtr;'.format(ns))
        self.write('    typedef typename {0}::add_pointer<T>::type TPtr;'.format(ns))
        self.write('    typedef typename {0}::add_lvalue_reference<T>::type TRef;'.format(ns))
        self.write('    typedef typename {0}::add_lvalue_reference<typename {0}::add_const<TNoRef>::type>::type TCRef;'.format(ns))
        self.write('    static inline int dynamic_size(TCRef value, int count, int flags)')
        self.write('    {')
        self.write('        return static_size;')
        self.write('    }')
        self.write('    static inline int write(char * buffer, TCRef value, int count, int chunk, int stride, int flags)')
        self.write('    {')
        self.write('        *reinterpret_cast<TNoCPtr>(buffer) = value;')
        self.write('        return static_size;')
        self.write('    }')
        self.write('    static inline int read_copy(TPtr value, char * buffer, int count, int chunk, int stride, int flags)')
        self.write('    {')
        self.write('        *value = *reinterpret_cast<TPtr>(buffer);')
        self.write('        return static_size;')
        self.write('    }')
        self.write('    static inline TRef read_ptr(char * buffer, int count, int * incr, int flags)')
        self.write('    {')
        self.write('        *incr += static_size;')
        self.write('        return *reinterpret_cast<TPtr>(buffer);')
        self.write('    }')
        self.write('};')
        
        self.write('template <class T> struct rpc<T *>')
        self.write('{')
        self.write('    enum { static_size = sizeof(T) };')
        self.write('    typedef typename {0}::remove_const<T>::type TNoC;'.format(ns))
        self.write('    static inline int dynamic_size(const T * value, int count, int flags)')
        self.write('    {')
        self.write('        count = (count == -1) ? 1 : count;')
        self.write('        return (((value || flags & RPC_FORCE) ? count : 0) * static_size) + ((flags & RPC_NULLABLE) ? sizeof(char) : 0);')
        self.write('    }')
        self.write('    static inline int write(char * buffer, const T * value, int count, int chunk, int stride, int flags)')
        self.write('    {')
        self.write('        if(flags & RPC_NULLABLE)')
        self.write('        {')
        self.write('            *buffer++ = (value != {0});'.format(null))
        self.write('        }')
        self.write('        if(value)')
        self.write('        {')
        self.write('            count = (count == -1) ? 1 : count;')
        self.write('            chunk = (chunk == -1) ? count : chunk;')
        self.write('            const T * current = value;')
        self.write('            int countLeft = count;')
        self.write('            while(countLeft > 0) {')
        self.write('                memcpy(buffer, current, std::min(countLeft, chunk)*sizeof(*current));')
        self.write('                countLeft -= chunk;')
        self.write('                current += stride;')
        self.write('                buffer += chunk*sizeof(*current);')
        self.write('            }')
        self.write('        }')
        self.write('        return dynamic_size(value, count, flags);')
        self.write('    }')
        self.write('    static inline int read_copy(T ** value, char * buffer, int count, int chunk, int stride, int flags)')
        self.write('    {')
        self.write('        if((flags & RPC_NULLABLE) && *buffer++ == 0)')
        self.write('        {')
        self.write('            *value = {0};'.format(null))
        self.write('        }')
        self.write('        else')
        self.write('        {')
        self.write('            count = (count == -1) ? 1 : count;')
        self.write('            chunk = (chunk == -1) ? count : chunk;')
        self.write('            T * current = *value;')
        self.write('            int countLeft = count;')
        self.write('            while(countLeft > 0) {')
        self.write('                memcpy(current, buffer, std::min(countLeft, chunk)*sizeof(*current));')
        self.write('                countLeft -= chunk;')
        self.write('                current += stride;')
        self.write('                buffer += chunk*sizeof(*current);')
        self.write('            }')
        self.write('        }')
        self.write('        return dynamic_size(*value, count, flags);')
        self.write('    }')
        self.write('    static inline T * read_ptr(char * buffer, int count, int * incr, int flags)')
        self.write('    {')
        self.write('        if((flags & RPC_NULLABLE) && *buffer++ == 0)')
        self.write('        {')
        self.write('            *incr += dynamic_size({0}, count, flags);'.format(null))
        self.write('            return {0};'.format(null))
        self.write('        }')
        self.write('        *incr += dynamic_size(reinterpret_cast<T *>(buffer), count, flags);')
        self.write('        return reinterpret_cast<T *>(buffer);')
        self.write('    }')
        self.write('};')
        
        self.write('template <> struct rpc<const char *>')
        self.write('{')
        self.write('    enum { static_size = 0 };  // strings are never used in the static part of the buffer')
        self.write('    static inline int dynamic_size(const char * value, int count, int flags)')
        self.write('    {')
        self.write('        if((flags & RPC_NULLABLE) && !value)  return 1;')
        self.write('        return ((count == -1) ? (strlen(value)+1) : count*sizeof(*value)) + ((flags & RPC_NULLABLE) ? sizeof(char) : 0);')
        self.write('    }')
        self.write('    static inline int write(char * buffer, const char * value, int count, int chunk, int stride, int flags)')
        self.write('    {')
        self.write('        if(flags & RPC_NULLABLE)  *buffer++ = (value != {0});'.format(null))
        self.write('        if(!value)  return 1;')
        self.write('        if(count==-1)  count = strlen(value)+1;')
        self.write('        memcpy(buffer, value, count);')
        self.write('        return count;')
        self.write('    }')
        self.write('    static inline int read_copy(char ** value, char * buffer, int count, int chunk, int stride, int flags)')
        self.write('    {')
        self.write('        if((flags & RPC_NULLABLE) && *buffer++ == 0)')
        self.write('        {')
        self.write('            *value = {0};'.format(null))
        self.write('            return 1;')
        self.write('        }')
        self.write('        if(count==-1) count = strlen(buffer)+1;')
        self.write('        memcpy(*value, buffer, count);')
        self.write('        return count;')
        self.write('    }')
        self.write('    static inline char * read_ptr(char * buffer, int count, int * incr, int flags)')
        self.write('    {')
        self.write('        if((flags & RPC_NULLABLE) && *buffer++ == 0)')
        self.write('        {')
        self.write('            *incr += 1;')
        self.write('            return {0};'.format(null))
        self.write('        }')
        self.write('        if(count==-1) count = strlen(buffer)+1;')
        self.write('        *incr += count;')
        self.write('        return buffer;')
        self.write('    }')
        self.write('};')
        
        self.write('template <> struct rpc<char *> : public rpc<const char *>')
        self.write('{};')
    
    def _write_responsereader_decl(self, is_cpp11):
        self.write('struct IResponseReader')
        self.write('{')
        self.write('    virtual ~IResponseReader()  { }')
        self.write('    virtual void operator() (char *buffer) = 0;')
        self.write('};')
        
    def _write_responsereader_impl(self, is_cpp11):
        ns = CPP11_NS if is_cpp11 else BOOST_NS
        self.write('template <class R> struct ResponseReader : public IResponseReader')
        self.write('{')
        self.write('    {0}::promise<R> promise;'.format(ns))
        self.write('    {0}::function<R (char *)> functor;'.format(ns))
        self.write('    {0}::function<void (R)> callback;'.format(ns))
        self.write('    void operator() (char *buffer)')
        self.write('    {')
        self.write('        R result = functor(buffer);')
        self.write('        promise.set_value(result);')
        self.write('        if(callback) callback(result);')
        self.write('    }')
        self.write('};')
        
        self.write('template <> struct ResponseReader<void> : public IResponseReader')
        self.write('{')
        self.write('    {0}::promise<void> promise;'.format(ns))
        self.write('    {0}::function<void (char *)> functor;'.format(ns))
        self.write('    {0}::function<void ()> callback;'.format(ns))
        self.write('    void operator() (char *buffer)')
        self.write('    {')
        self.write('        if(functor) functor(buffer);')
        self.write('        promise.set_value();')
        self.write('        if(callback) callback();')
        self.write('    }')
        self.write('};')


class ClientC(RPCImplFile):
    def __init__(self, filename, elements, is_cpp11):
        super(ClientC, self).__init__('rpc_{0}_client.cpp'.format(filename))
        self.write('#include "rpc_{0}_client.h"'.format(filename))
        self.write(CPP11_TRAITS_INC if is_cpp11 else BOOST_TRAITS_INC)
        self.write('#include <cassert>')
        
        includes = set()
        for ext_el in _get_external_elements(filename+'.h', elements):
            # opposite of .h
            if not ( ext_el['kind'] not in ('struct', 'class') or
                 ext_el['parent'] and ext_el['parent']['kind'] != 'namespace'):
                if ext_el['location']['file'] not in includes:
                    self.close_scope()
                    self.write('#include "rpc_{0}_client.h"'.format(ext_el['location']['file'].replace('.h','')))
                    includes.add(ext_el['location']['file'])
        
        self._write_responsereader_decl(is_cpp11)
        self.write('namespace')
        self.write('{')
        self.indent()
        self._write_traits(is_cpp11)
        self._write_responsereader_impl(is_cpp11)
        self.unindent()
        self.write('}')
        self.write('int rpc_create_request_id();')
        self.write('void rpc_send(char *, bool, int, IResponseReader *);')
    
    def _write_readerfunction(self, el):
        if is_ctor(el):
            self.write('static int rpc_read_{0}{1}(char * rpc_buffer)'.format(el['name'], el['function_id']))
            self.write('{')
            self.write('    int rpc_startIndex = 2*sizeof(int);')
            self.write('    return rpc<int>::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0);')
            self.write('}')
            return
        elif is_dtor(el):
            # do nothing
            return
        
        out_params = [p for p in el['sorted_params'] if not _is_in(p)]
        has_return_value = (el['type'] and el['type'] != 'void')
        result_typename = _get_full_typename(el, el['type'])
        func = 'static {0} rpc_read_{1}{2}(char *rpc_buffer'.format(result_typename, el['name'], el['function_id'])
        if out_params:
            func += ', '
            # if any out param size depends on input parameters
            #   put all input parameters
            # else put only out parameters
            if [p for p in out_params if not _meta_count_is_constant(el, p)]:
                params = el['sorted_params']
            else:
                params = out_params
            func += ', '.join('{0} {1}'.format(_get_full_typename(el, p['type']), p['name']) for p in params)
        
        func += ')'
        self.write(func)
        self.write('{')
        self.indent()
        
        self.write('static const int HEADER_SIZE = 2*sizeof(int);')
        self.write('int rpc_startIndex = HEADER_SIZE;')
        for p in out_params:
            count = p.get('meta_count', -1)
            chunk = p.get('meta_chunk', count)
            stride = p.get('meta_stride', chunk)
            self.write('rpc_startIndex += rpc< {0} >::read_copy(&{1}, rpc_buffer+rpc_startIndex, {2}, {3}, {4}, 0);'.format(_get_full_typename(el, p['type']), p['name'], count, chunk, stride))
        if has_return_value:
            self.write('{0} rpc_result;'.format(result_typename))
            self.write('rpc_startIndex += rpc< {0} >::read_copy(&rpc_result, rpc_buffer+rpc_startIndex, -1, -1, -1, 0);'.format(result_typename))
        
        self.write('assert(rpc_startIndex == reinterpret_cast<int *>(rpc_buffer)[0]);')
        if has_return_value:
            self.write('return rpc_result;')
        
        self.unindent()
        self.write('}')

    def _get_bound_readerfunction(self, el, is_cpp11):
        out_params = [p for p in el['sorted_params'] if not _is_in(p)]
        if not out_params:
            return '&rpc_read_{0}{1}'.format(el['name'], el['function_id'])
        bound = '{0}::bind(&rpc_read_{1}{2}, {3}_1'.format(CPP11_NS if is_cpp11 else BOOST_NS, el['name'], el['function_id'], 'std::placeholders::' if is_cpp11 else '')
        if out_params:
            bound += ', '
            # if any out param size depends on input parameters
            #   put all input parameters
            # else put only out parameters
            if [p for p in out_params if not _meta_count_is_constant(el, p)]:
                params = el['sorted_params']
            else:
                params = out_params
            bound += ', '.join('{0}'.format(p['name']) for p in params)
        bound += ')'
        return bound
    
    def _write_function(self, el, is_cpp11):
        self._write_function_prototype(el, is_cpp11)
        self.write('{')
        self.indent()
        self._write_function_definition(el, is_cpp11)
        self.unindent()
        self.write('}')

    def _write_function_prototype(self, el, is_cpp11):
        ns = CPP11_NS if is_cpp11 else BOOST_NS
        callback = {'type': '{0}::function<void ({1})>'.format(ns, '' if (el['type'] == 'void') else el['type']),
                    'name': 'callback'}
        
        func = ''
        if el['type']:
            func += '{0}::future< {1} > '.format(ns,
                                                 _get_full_typename(el, el['type']))
        func += _get_full_typename(el, el['name'], False)
        func += '('
        func += ', '.join('{0} {1}'.format(p['type'], p['name'])
                          for p in el['params'] + ([] if (is_ctor(el) or is_dtor(el)) else [callback] )
                          if not p.has_key('meta_value'))
        func += ')'
        self.write(func)
        if is_ctor(el):
            self.write(' : m_id(-1)')

    def _write_function_definition(self, el, is_cpp11):
        ns = CPP11_NS if is_cpp11 else BOOST_NS
        self.write('static const int RPC_FUNCTION_ID = {0};'.format(el['function_id']))
        self.write('static const int RPC_HEADER_SIZE = 4*sizeof(int);')
        in_params = [p for p in el['sorted_params'] if (_is_in(p) and not _has_fixed_value(p))]
        for p in in_params:
            if p.get('meta_remote_object'):
                self.write('static const int RPC_{0}_STATIC_SIZE = sizeof(int);'.format(p['name']))
                self.write('const int rpc_{0}_dyn_size = 0;'.format(p['name']))
            else:
                self.write('static const int RPC_{0}_STATIC_SIZE = {1};'.format(p['name'], _get_static_size(el, p)))
                self.write('const int rpc_{0}_dyn_size = RPC_{0}_STATIC_SIZE ? 0 : {1};'.format(p['name'], _get_dynamic_size(el, is_cpp11, p)))
        
        self.write('static const int RPC_STATIC_SIZE = ')
        for p in in_params:
            self.write('    RPC_{0}_STATIC_SIZE +'.format(p['name']))
        self.write('    0;')
        self.write('char _rpc_static_buffer[RPC_HEADER_SIZE+RPC_STATIC_SIZE];')
        
        self.write('const int rpc_dyn_size = ')
        for p in in_params:
            self.write('    rpc_{0}_dyn_size +'.format(p['name']))
        self.write('    0;')
        
        self.write('char * rpc_buffer = _rpc_static_buffer;')
        self.write('if(rpc_dyn_size)')
        self.write('    rpc_buffer = new char[RPC_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];')
        
        # write header
        self.write('reinterpret_cast<int *>(rpc_buffer)[0] = RPC_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;')
        self.write('const int rpc_requestId = rpc_create_request_id();')
        self.write('reinterpret_cast<int *>(rpc_buffer)[1] = rpc_requestId;')
        if is_ctor(el) or is_free_function(el):
            self.write('reinterpret_cast<int *>(rpc_buffer)[2] = 0;')
        else:
            self.write('reinterpret_cast<int *>(rpc_buffer)[2] = rpcInstance();')
        self.write('reinterpret_cast<int *>(rpc_buffer)[3] = RPC_FUNCTION_ID;')
        
        # write body
        self.write('int rpc_startIndex = RPC_HEADER_SIZE;')
        for p in in_params:
            if p.get('meta_remote_object'):
                self.write('rpc_startIndex += rpc<int>::write(rpc_buffer+rpc_startIndex, {1}->rpcInstance(), -1, -1, -1, 0);'.format(_get_full_typename(el, p['type']), p['name']))
            else:
                count = p.get('meta_count', -1)
                chunk = p.get('meta_chunk', count)
                stride = p.get('meta_stride', chunk)
                self.write('rpc_startIndex += rpc< {0} >::write(rpc_buffer+rpc_startIndex, {1}, {2}, {3}, {4}, {5});'.format(
                    _get_full_typename(el, p['type']),
                    p['name'],
                    count, chunk, stride,
                    _get_flags(el, p)))
        self.write('assert(rpc_startIndex == RPC_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);')
        
        # send
        if is_dtor(el):
            self.write('IResponseReader * rpc_functor = {0};'.format(CPP11_NULL if is_cpp11 else BOOST_NULL))
        else:
            promise_type = 'int' if is_ctor(el) else el['type']
            self.write('{0}::promise< {1} > rpc_promise;'.format(ns, promise_type))
            self.write('{0}::future< {1} > rpc_result = rpc_promise.get_future();'.format(ns, promise_type))

            self.write('ResponseReader< {0} > * rpc_functor = new ResponseReader< {0} >;'.format(promise_type))
            self.write('rpc_functor->promise.swap(rpc_promise);')
            self.write('rpc_functor->functor = {0};'.format(self._get_bound_readerfunction(el, is_cpp11)))
            if not (is_ctor(el) or is_dtor(el)):
                self.write('rpc_functor->callback = callback;')
        self.write('rpc_send(rpc_buffer, (rpc_dyn_size>0), rpc_requestId, rpc_functor);')
        
        # end
        #self.write('if(rpc_dyn_size)')
        #self.write('    delete [] rpc_buffer;')
        
        if is_ctor(el):
            self.write('m_futureId = {0}::move(rpc_result);'.format(ns))
        elif not is_dtor(el):
            self.write('return {0}::move(rpc_result);'.format(ns))

    def write_element(self, el, is_cpp11):
        if el['kind'] == 'function':
            self._write_readerfunction(el)
            self._write_function(el, is_cpp11)


class ServerC(RPCImplFile):
    def __init__(self, filename, elements, is_cpp11):
        super(ServerC, self).__init__('rpc_{0}_server.cpp'.format(filename))
        if filename != 'Check':
            self.write('#include "{0}.h"'.format(filename))
        self.write(CPP11_TRAITS_INC if is_cpp11 else BOOST_TRAITS_INC)
        self.write('#include <cassert>')
        self.write('#include <cstring>')
        
        includes = set()
        for ext_el in _get_external_elements(filename+'.h', elements):
            if ext_el['location']['file'] not in includes:
                self.write('#include "{0}"'.format(ext_el['location']['file']))
                includes.add(ext_el['location']['file'])
        
        self.write('namespace')
        self.write('{')
        self.indent()
        self._write_traits(is_cpp11)
        self.unindent()
        self.write('}')
        self.write('void * rpc_getInstance(int instanceId);')
        self.write('int rpc_registerInstance(void * inst);')
        self.write('void rpc_unregisterInstance(int instanceId);')
        self.write('void rpc_send(char *, bool, int);')
    
    def _get_func_name(self, el):
        return _get_full_typename(el, el['name'], False).replace('::', '_').replace('~', 'dtor_') + str(el['function_id'])
    
    def write_element(self, el, is_cpp11):
        if not el['kind'] == 'function':
            return
        
        func_name = self._get_func_name(el)
        has_return_value = (el['type'] and el['type'] != 'void')
        
        self.write('void {0}(int rpc_clientId, int rpc_requestId, char *rpc_buffer)'.format(func_name, el['function_id']))
        self.write('{')
        self.indent()
        
        #self.write('puts("{0}");'.format(func_name))
        
        # header
        self.write('static const int RPC_FUNCTION_ID = {0};'.format(el['function_id']))
        self.write('static const int RPC_INPUT_HEADER_SIZE = 4*sizeof(int);')
        self.write('static const int RPC_OUTPUT_HEADER_SIZE = 2*sizeof(int);')
        self.write('assert(reinterpret_cast<int *>(rpc_buffer)[1] == rpc_requestId);')
        self.write('int rpc_instanceId = reinterpret_cast<int *>(rpc_buffer)[2];')
        if is_ctor(el) or is_free_function(el):
            self.write('assert(rpc_instanceId == 0);')
        else:
            class_name = _get_full_typename(el['parent'], el['parent']['name'], False)
            self.write('assert(rpc_instanceId > 0);')
            self.write('{0} * rpc_instance = reinterpret_cast<{0} *>(rpc_getInstance(rpc_instanceId));'.format(class_name))
            self.write('assert(rpc_instance);')
        self.write('assert(reinterpret_cast<int *>(rpc_buffer)[3] == RPC_FUNCTION_ID);')
        
        self.write('int rpc_startIndex = RPC_INPUT_HEADER_SIZE;')
        
        # in params
        in_params = [p for p in el['sorted_params'] if (_is_in(p) and not _has_fixed_value(p))]
        for p in in_params:
            if p.get('meta_remote_object'):
                self.write('{0} {1} = reinterpret_cast< {0} >(rpc_getInstance(rpc<int>::read_ptr(rpc_buffer+rpc_startIndex, -1, &rpc_startIndex, 0)));'.format(
                    _get_full_typename(el, p['type']),
                    p['name']))
            else:
                self.write('{0} {1} = rpc< {0} >::read_ptr(rpc_buffer+rpc_startIndex, {2}, &rpc_startIndex, {3});'.format(
                    _get_full_typename(el, p['type']),
                    p['name'],
                    p.get('meta_count', -1),
                    _get_flags(el, p)))
        
        # fixed in params (may depend on other in params)
        for p in el['sorted_params']:
            if _has_fixed_value(p):
                self.write('{0} {1} = {2};'.format(_get_full_typename(el, p['type']),
                                                   p['name'],
                                                   _get_full_typename(el, _get_fixed_value(p))))
        
        # out params and return value
        out_params = [p for p in el['sorted_params'] if (not _is_in(p) and not _has_fixed_value(p))]
        for p in out_params:
            self.write('static const int RPC_{0}_STATIC_SIZE = {1};'.format(p['name'], _get_static_size(el, p)))
            self.write('const int rpc_{0}_dyn_size = RPC_{0}_STATIC_SIZE ? 0 : {1};'.format(p['name'], _get_dynamic_size(el, is_cpp11, p, True)))
        
        self.write('static const int RPC_STATIC_SIZE = ')
        for p in out_params:
            self.write('    RPC_{0}_STATIC_SIZE +'.format(p['name']))
        if el['type'] and el['type'] != 'void':
            self.write('    {0} +'.format(_get_static_size(el, el)))
        elif is_ctor(el):
            self.write('    sizeof(int) +')
        self.write('    0;')
        self.write('char _rpc_static_buffer[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE];')
        
        self.write('const int rpc_dyn_size = ')
        for p in out_params:
            self.write('    rpc_{0}_dyn_size +'.format(p['name']))
        self.write('    0;')
        
        self.write('char * rpc_outBuffer = _rpc_static_buffer;')
        self.write('if(rpc_dyn_size)')
        self.write('    rpc_outBuffer = new char[RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size];')
        
        self.write('rpc_startIndex = RPC_OUTPUT_HEADER_SIZE;')
        for p in out_params:
            self.write('{0} {1} = rpc< {0} >::read_ptr(rpc_outBuffer+rpc_startIndex, {2}, &rpc_startIndex, RPC_FORCE);'.format(
                _get_full_typename(el, p['type']),
                p['name'],
                p.get('meta_count', -1)))
        
        # call
        if is_dtor(el):
            self.write('delete rpc_instance;')
            self.write('rpc_unregisterInstance(rpc_instanceId);')
        else:
            call = ''
            if is_ctor(el):
                call = 'rpc<int>::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = rpc_registerInstance(new '
            elif has_return_value:
                call = 'rpc< {0} >::read_ptr(rpc_outBuffer+rpc_startIndex, -1, &rpc_startIndex, 0) = '.format(_get_full_typename(el, el['type']))
            
            if is_ctor(el):
                call += '::'.join(_get_full_typename(el, el['name'], False).split('::')[:-1])
            elif is_free_function(el):
                call += _get_full_typename(el, el['name'], False)
            else:
                call += 'rpc_instance->{0}'.format(el['name'])
            
            call += '({0})'.format(', '.join(p['name'] for p in el['params']))
            if is_ctor(el):
                call += ')'
            call += ';'
            self.write(call)
            
        self.write('assert(rpc_startIndex == RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size);')
        
        self.write('reinterpret_cast<int *>(rpc_outBuffer)[0] = RPC_OUTPUT_HEADER_SIZE+RPC_STATIC_SIZE+rpc_dyn_size;')
        self.write('reinterpret_cast<int *>(rpc_outBuffer)[1] = rpc_requestId;')
        self.write('rpc_send(rpc_outBuffer, (rpc_dyn_size>0), rpc_clientId);')
        
        # end
        #self.write('if(rpc_dyn_size)')
        #self.write('    delete [] rpc_outBuffer;')
        
        self.unindent()
        self.write('}')
    def close(self):
        pass


class ServerDispatcher(RPCFile):
    def __init__(self, is_cpp11):
        super(ServerDispatcher, self).__init__('rpc_dispatch.cpp')
        
        self._functions = []
        
        self.write('#include <vector>')
        self.write('static std::vector<void *> rpc_instances;')
        self.write('void * rpc_getInstance(int instanceId)')
        self.write('{')
        self.write('    return rpc_instances[instanceId-1];')
        self.write('}')
        self.write('int rpc_registerInstance(void * inst)')
        self.write('{')
        self.write('    rpc_instances.push_back(inst);')
        self.write('    return (int)rpc_instances.size();')
        self.write('}')
        self.write('void rpc_unregisterInstance(int instanceId)')
        self.write('{')
        self.write('    rpc_instances[instanceId-1] = {0};'.format(CPP11_NULL if is_cpp11 else BOOST_NULL))
        self.write('}')
    
    def _get_func_name(self, el):
        return _get_full_typename(el, el['name'], False).replace('::', '_').replace('~', 'dtor_') + str(el['function_id'])
    
    def close(self):
        self.close_scope()
        self._write_dispatch()
        super(ServerDispatcher, self).close()
    
    def write_element(self, el, is_cpp11):
        if not el['kind'] == 'function':
            return
        
        func_name = self._get_func_name(el)
        self.write('void {0}(int rpc_clientId, int rpc_requestId, char *rpc_buffer);'.format(func_name, el['function_id']))
        self._functions.append(el)
    
    def _write_dispatch(self):
        self.write('void rpc_dispatch(char * buffer, int clientId)')
        self.write('{')
        self.indent()
        self.write('const int requestId = reinterpret_cast<int *>(buffer)[1];')
        self.write('const int instanceId = reinterpret_cast<int *>(buffer)[2];')
        self.write('const int functionId = reinterpret_cast<int *>(buffer)[3];')
        self.write('switch(functionId)')
        self.write('{')
        
        for el in self._functions:
            self.write('case {0}:'.format(el['function_id']))
            self.write('    {0}(clientId, requestId, buffer);'.format(self._get_func_name(el)))
            self.write('    break;')
        self.write('}')
        self.unindent()
        self.write('}')


def _sorted(elements):
    "sort by file and line number"
    # flatten
    elements_by_file = {}
    
    def recurs(l):
        for i in l:
            elements_by_file.setdefault(i['location']['file'], []).append(i)
            if 'content' in i:
                recurs(i['content'])
    
    recurs(elements)
    
    # sort by line number
    for k in elements_by_file:
        elements_by_file[k].sort(key=lambda i: i['location']['line'])
    
    return elements_by_file


def _get_rpc_metadata(el):
    try:
        metadata = next(doc for doc in el['doc'] if doc.startswith('[[[rpc'))
    except StopIteration:
        # no meta data
        return []
    else:
        metadata = metadata[6:].split(']]]', 1)[0]
        return [s.strip() for s in metadata.split(';')]


def parse_compound_metadata(el):
    metadata = _get_rpc_metadata(el)
    for rule in metadata:
        if rule == 'remote_class':
            el['meta_remote'] = True
        elif rule == 'ignore_class':
            el['meta_ignore'] = True


def parse_function_metadata(el):
    param_by_name = dict((p['name'], p) for p in el['params'])
    metadata = _get_rpc_metadata(el)
    for rules in metadata:
        if rules == 'ignore_function':
            el['meta_ignore'] = True
            return
        
        if ':' not in rules:
            continue
        
        pname, l = rules.split(':', 1)
        pname = pname.strip()
        if pname not in param_by_name:
            logging.warning('%s is not a parameter of the function %s', pname, el['name'])
            continue
        for rule in l.split(','):
            rule = rule.strip()
            if rule == 'out':
                param_by_name[pname]['meta_direction'] = 'out'
            elif rule == 'inout':
                logging.warning('"inout" is not (yet) a valid token.')
            elif rule == 'in':
                param_by_name[pname]['meta_direction'] = 'in'
            elif rule == 'remote_object':
                param_by_name[pname]['meta_remote_object'] = True
            elif rule == 'nullable':
                param_by_name[pname]['meta_nullable'] = True
            elif '=' in rule:
                rname, rvalue = rule.split('=', 1)
                rname = rname.strip()
                rvalue = rvalue.strip()
                if rname == 'count':
                    #if not rvalue.isdigit() and rvalue not in param_by_name:
                    #    logging.warning('%s is not a parameter of the function %s', rvalue, el['name'])
                    param_by_name[pname]['meta_count'] = rvalue
                elif rname == 'value':
                    param_by_name[pname]['meta_value'] = rvalue
                elif rname == 'order':
                    param_by_name[pname]['meta_order'] = int(rvalue)
                elif rname == 'chunk':
                    param_by_name[pname]['meta_chunk'] = rvalue
                    param_by_name.get(rvalue, {})['meta_server_value'] = param_by_name[pname]['meta_count']  # if chunk is a parameter, always pass "count" to the server == single chunk
                elif rname == 'stride':
                    param_by_name[pname]['meta_stride'] = rvalue
                    param_by_name.get(rvalue, {})['meta_server_value'] = param_by_name[pname]['meta_chunk']  # if stride is a parameter, always pass "chunk" to the server == contiguous memory
                else:
                    logging.warning('%s is an unknown meta for the function %s', rname, el['name'])
            else:
                logging.warning('%s is an unknown rule for the function %s', rule, el['name'])


UNIQUE_ID = 1
def prepare_element(el):
    global UNIQUE_ID
    if el['kind'] == 'function':
        parse_function_metadata(el)
        if el.get('meta_ignore'):
            el['done'] = True
        el['function_id'] = UNIQUE_ID
        el['sorted_params'] = sorted(el['params'], key=lambda x: x.get('meta_order', 5))
        UNIQUE_ID += 1
    elif el['kind'] in ('struct', 'class'):
        parse_compound_metadata(el)
        if el.get('meta_ignore'):
            el['done'] = True


def write(all_elements, config):
    if not (config.getboolean('library', 'has_boost') \
            or config.getboolean('library', 'has_cpp11')):
        raise Exception, 'has_boost or has_cpp11 must be true'

    is_cpp11 = config.getboolean('library', 'has_cpp11')

    dispatcher = ServerDispatcher(is_cpp11)
    sorted_elements = _sorted(all_elements)
    
    # checks
    client_h = ClientH('Check', [], is_cpp11)
    client_c = ClientC('Check', [], is_cpp11)
    server_c = ServerC('Check', [], is_cpp11)
    server_c.write('#include <cstdio>')
    for filename, elements in sorted_elements.items():
        name = filename.split('.', 1)[0]
        client_c.write('#include "rpc_{0}_client.h"'.format(name))
        server_c.write('#include "{0}.h"'.format(name))
    for name, value in config.items('check'):
        element = {'kind': 'function',
                   'name': 'check'+name,
                   'doc': '',
                   'params': [],
                   'type': 'unsigned char',
                   'parent': None,
                   }
        prepare_element(element)

        client_h.write_element(element, is_cpp11)
        client_c._write_readerfunction(element)
        client_c._write_function_prototype(element, is_cpp11)

        element['doc'] = ['[[[rpc buffer: count=count]]]']
        element['params'] = [{'name': 'count', 'type': 'unsigned char'},
                             {'name': 'buffer', 'type': 'unsigned char *'}]
        element['sorted_params'] = element['params']
        parse_function_metadata(element)
        client_c.write('{')
        client_c.indent()
        buffer = [l for l in value.split() if l]
        client_c.write('const unsigned char count = {0};'.format(len(buffer)))
        client_c.write('const unsigned char buffer[] = {{ {0} }};'.format(',\n        '.join(buffer)))
        client_c.write('assert(count == sizeof(buffer));')
        client_c._write_function_definition(element, is_cpp11)
        client_c.unindent()
        client_c.write('}')

        server_c.write('unsigned char {0}(unsigned char count, unsigned char *buffer)'.format('check'+name))
        server_c.write('{')
        server_c.indent()
        server_c.write('const unsigned char count_ref = {0};'.format(len(buffer)))
        server_c.write('const unsigned char buffer_ref[] = {{ {0} }};'.format(',\n        '.join(buffer)))
        server_c.write('assert(count_ref == sizeof(buffer_ref));')
        server_c.write('const bool result = (count == count_ref && memcmp(buffer, buffer_ref, count_ref)==0);')
        server_c.write('if(!result)')
        server_c.write('{')
        server_c.write('    fprintf(stderr, "Check Failed: {0}\\n");'.format(name))
        server_c.write('    fprintf(stderr, " Server:");')
        server_c.write('    for(int i=0; i<{0}; ++i)'.format(len(buffer)))
        server_c.write('        fprintf(stderr, " %02X", buffer_ref[i]);')
        server_c.write('    fprintf(stderr, "\\n");')
        server_c.write('    fprintf(stderr, " Client:");')
        server_c.write('    for(int i=0; i<{0}; ++i)'.format(len(buffer)))
        server_c.write('        fprintf(stderr, " %02X", buffer[i]);')
        server_c.write('    fprintf(stderr, "\\n");')
        server_c.write('}')
        server_c.write('return result;')
        server_c.unindent()
        server_c.write('}')
        server_c.write_element(element, is_cpp11)

        dispatcher.write_element(element, is_cpp11)

    client_h.close()
    client_c.close()
    server_c.close()

    # declared elements
    def _recurs_prepare_element(el):
        prepare_element(el)
        if el.has_key('content'):
            [_recurs_prepare_element(i) for i in el['content']]
    [_recurs_prepare_element(i) for i in all_elements]

    for filename, elements in sorted_elements.items():
        name = filename.split('.', 1)[0]
        client_h = ClientH(name, all_elements, is_cpp11)
        client_c = ClientC(name, all_elements, is_cpp11)
        server_c = ServerC(name, all_elements, is_cpp11)
        for element in elements:
            if not element.get('done'):
                client_h.write_element(element, is_cpp11)
                client_c.write_element(element, is_cpp11)
                server_c.write_element(element, is_cpp11)
                dispatcher.write_element(element, is_cpp11)
                element['done'] = True
        client_h.close()
        client_c.close()
        server_c.close()

    dispatcher.close()
