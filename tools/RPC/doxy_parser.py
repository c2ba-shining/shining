import xml.sax

START_ELEMENT = 'start'
END_ELEMENT = 'end'
TEXT = 'text'


class EventHandler(xml.sax.ContentHandler):
    def __init__(self, target):
        self.target = target
    def startElement(self, name, attrs):
        self.target.send((START_ELEMENT, (name, attrs._attrs)))
    def characters(self, text):
        self.target.send((TEXT, text))
    def endElement(self, name):
        self.target.send((END_ELEMENT, name))


def coroutine(func):
    "decorator to initialize coroutine automatically"
    def start(*args, **kwargs):
        cr = func(*args, **kwargs)
        cr.next()
        return cr
    return start


@coroutine
def set_key(k, v, target=None):
    "set the 'parent' key on the incoming value"
    while True:
        key, value = yield
        value[k] = v
        if target:
            target.send((key, value))


@coroutine
def put_in_list(l, target=None):
    "append the incoming value to a list"
    while True:
        key, value = yield
        l.append(value)
        if target:
            target.send((key, value))


@coroutine
def put_in_dict(d, target=None):
    "write the incoming value (key, value) to a dict"
    while True:
        key, value = yield
        d[key] = value
        if target:
            target.send((key, value))


@coroutine
def append_in_dict(d, target=None):
    "append the incoming value (key, value) to a list in a dict"
    while True:
        key, value = yield
        d.setdefault(key, []).append(value)
        if target:
            target.send((key, value))


@coroutine
def read_compound(current_scope, target):
    event, value = yield
    element_name = value[0]
    assert event == START_ELEMENT and element_name == 'compound'
    refid = value[1]['refid']
    
    while True:
        event, value = yield
        if event == END_ELEMENT and value == element_name:
            break
    
    parse('{0}.xml'.format(refid), current_scope, target)


@coroutine
def read_dummy():
    "skip an element"
    event, value = yield
    assert event == START_ELEMENT
    element = value[0]
    while True:
        event, value = yield
        if event == END_ELEMENT and value == element:
            break


@coroutine
def read_text(target):
    "read text, ignoring enclosed elements"
    text = ''
    event, value = yield
    assert event == START_ELEMENT
    element = value[0]
    while True:
        event, value = yield
        if event == END_ELEMENT and value == element:
            break
        if event == TEXT:
            text += value
    
    target.send((element, text))


@coroutine
def read_inner(current_scope, target):
    event, value = yield
    element_name = value[0]
    assert event == START_ELEMENT and (element_name[:5] == 'inner' or element_name == 'compound')
    refid = value[1]['refid']
    name = ''
    
    while True:
        event, value = yield
        if event == END_ELEMENT and value == element_name:
            break
        if event == TEXT:
            name += value
    
    name = name.strip()
    parts = name.rsplit('::', 1)
    if (not current_scope and '::' not in name or
        current_scope and name.startswith(current_scope)):
        parse('{0}.xml'.format(refid), current_scope, target)


@coroutine
def read_enum(target):
    "read a single enum type"
    result = {'location': {},
              'kind': 'enum',
              'name': '',
              'values': []}
    
    event, value = yield
    assert event == START_ELEMENT and value[0] == 'memberdef'
    while True:
        event, value = yield
        if event == END_ELEMENT and value == 'memberdef':
            break
        
        if event == START_ELEMENT and value[0] == 'name':
            # read name and open bracket
            while True:
                event, value = yield
                if event == END_ELEMENT:
                    break
                result['name'] += value
        elif event == START_ELEMENT and value[0] == 'enumvalue':
            # read a value
            enumvalue = ''
            while True:
                event, value = yield
                if event == END_ELEMENT and value == 'enumvalue':
                    result['values'].append(enumvalue)
                    break
                
                if event == START_ELEMENT and value[0] == 'name':
                    # read name
                    while True:
                        event, value = yield
                        if event == END_ELEMENT:
                            break
                        enumvalue += value

        elif event == START_ELEMENT and value[0] == 'location':
            result['location'] = {'file': value[1]['file'].rsplit('/')[-1],
                                  'line': int(value[1]['line'])
                                  }

    target.send(('enum', result))


@coroutine
def _read_type_and_name(target, kind):
    result = {'location': {},
              'kind': kind,
              'type': '',
              'name': ''}
    
    event, value = yield
    assert event == START_ELEMENT and value[0] == 'memberdef'
    while True:
        event, value = yield
        
        if event == END_ELEMENT and value == 'memberdef':
            break
        
        elif event == START_ELEMENT and value[0] == 'type':
            # read name
            while True:
                event, value = yield
                if event == END_ELEMENT:
                    break
                result['type'] += value

        elif event == START_ELEMENT and value[0] == 'name':
            # read name
            while True:
                event, value = yield
                if event == END_ELEMENT:
                    break
                result['name'] += value

        elif event == START_ELEMENT and value[0] == 'location':
            result['location'] = {'file': value[1]['file'].rsplit('/')[-1],
                                  'line': int(value[1]['line'])
                                  }

    target.send((kind, result))

def read_typedef(target):
    "read a single typedef"
    return _read_type_and_name(target, 'typedef')
def read_variable(target):
    "read a single typedef"
    return _read_type_and_name(target, 'variable')


@coroutine
def read_function(target):
    "read a single function"
    event, value = yield
    
    result = {'location': {},
              'kind': 'function',
              'type': '',
              'name': '',
              'params': [],
              'doc': [],
              'static': value[1]['static'],
              'const': value[1]['const'],
              'explicit': value[1]['explicit'],
              'virtual': value[1]['virt'],
              }
    
    assert event == START_ELEMENT and value[0] == 'memberdef'
    while True:
        event, value = yield
        
        if event == END_ELEMENT and value == 'memberdef':
            break
        
        elif event == START_ELEMENT and value[0] == 'type':
            # read return type
            reader = read_text(put_in_dict(result))
            reader.send((event, value))
            while True:
                event, value = yield
                try:
                    reader.send((event, value))
                except StopIteration:
                    break
        
        elif event == START_ELEMENT and value[0] == 'name':
            # read name
            while True:
                event, value = yield
                if event == END_ELEMENT:
                    break
                result['name'] += value
        
        elif event == START_ELEMENT and value[0] == 'param':
            # read a single param
            param = {'type': '',
                     'name': ''}
            while True:
                event, value = yield
                if event == END_ELEMENT and value == 'param':
                    result['params'].append(param)
                    break
                elif event == START_ELEMENT and value[0] == 'type':
                    # read type
                    reader = read_text(put_in_dict(param))
                    reader.send((event, value))
                    while True:
                        event, value = yield
                        try:
                            reader.send((event, value))
                        except StopIteration:
                            break
                elif event == START_ELEMENT and value[0] == 'declname':
                    # read name
                    while True:
                        event, value = yield
                        if event == END_ELEMENT:
                            break
                        param['name'] += value
                elif event == START_ELEMENT and value[0] == 'defval':
                    # read default value
                    param['default'] = ''
                    while True:
                        event, value = yield
                        if event == END_ELEMENT:
                            break
                        param['default'] += value
        elif event == START_ELEMENT and value[0] == 'para':
            doc = ''
            level = 1
            while True:
                event, value = yield
                if event == START_ELEMENT and value[0] == 'para':
                    level += 1
                elif event == END_ELEMENT and value == 'para':
                    level -= 1
                    result['doc'].append(doc)
                    doc = ''
                    if level == 0:
                        break
                elif event == TEXT:
                    doc += value.strip()
        
        elif event == START_ELEMENT and value[0] == 'location':
            result['location'] = {'file': value[1]['file'].rsplit('/')[-1],
                                  'line': int(value[1]['line'])
                                  }
    
    target.send(('function', result))


@coroutine
def read_compounddef(current_scope, target):
    event, value = yield
    assert event == START_ELEMENT and value[0] == 'compounddef'
    
    result = {'location': {},
              'kind': value[1]['kind'],
              'name': '',
              'doc': [],
              'content': []}
    
    # we do not need file xml representation
    # simply forward the target for them.
    is_file = (value[1]['kind'] == 'file')
    must_send_result = not is_file
    result_handler = target if is_file else set_key('parent', result, put_in_list(result['content']))
    
    while True:
        event, value = yield
        if event == START_ELEMENT and value[0] == 'compoundname':
            # read name
            while True:
                event, value = yield
                if event == END_ELEMENT:
                    # remove namespaces and enclosing compound
                    result['name'] = result['name'].strip()
                    if not is_file:
                        result['name'] = result['name'].lstrip(current_scope)
                        current_scope += '{0}::'.format(result['name'])
                    break
                result['name'] += value
        
        elif event == START_ELEMENT and value[0] == 'location':
            result['location'] = {'file': value[1]['file'].rsplit('/')[-1],
                                  'line': int(value[1].get('line', 0))
                                  }
        
        elif event == START_ELEMENT and value[0] == 'para':
            # <para> is a paragraph in the doc
            # it is used in both brief and detail description
            doc = ''
            while True:
                event, value = yield
                if event == END_ELEMENT:
                    result['doc'].append(doc.strip())
                    break
                doc += value
        
        elif event == START_ELEMENT and value[0][:5] == 'inner':
            # inner class or namespace
            if value[1].get('prot', 'public') != 'public':
                reader = read_dummy()
            else:
                reader = read_inner(current_scope, result_handler)
            reader.send((event, value))
            while True:
                event, value = yield
                try:
                    reader.send((event, value))
                except StopIteration:
                    break
        
        elif event == START_ELEMENT and value[0] == 'memberdef':
            if value[1].get('prot', 'public') != 'public':
                reader = read_dummy()
            elif value[1]['kind'] == 'enum':
                reader = read_enum(result_handler)
            elif value[1]['kind'] == 'typedef':
                reader = read_typedef(result_handler)
            elif value[1]['kind'] == 'variable':
                reader = read_variable(result_handler)
            elif value[1]['kind'] == 'function':
                reader = read_function(result_handler)
            else:
                reader = read_dummy()
            
            reader.send((event, value))
            while True:
                event, value = yield
                try:
                    reader.send((event, value))
                except StopIteration:
                    break
        
        elif event == END_ELEMENT and value == 'compounddef':
            break
    
    if must_send_result:
        target.send((result['name'], result))


@coroutine
def read_xml(current_scope, target):
    while True:
        event, value = yield
        
        if event == START_ELEMENT and value[0] == 'compound':
            # read file declaration (from index.xml)
            is_file = (value[1]['kind'] == 'file')
            reader = read_compound(current_scope, target) if is_file else read_dummy()
            reader.send((event, value))
            while True:
                event, value = yield
                try:
                    reader.send((event, value))
                except StopIteration:
                    break
        
        if event == START_ELEMENT and value[0] == 'compounddef':
            # read file, class and struct
            reader = read_compounddef(current_scope, target)
            reader.send((event, value))
            while True:
                event, value = yield
                try:
                    reader.send((event, value))
                except StopIteration:
                    break


_parsed_files = set()
_root = ''
def parse(filename, current_scope, target):
    import os
    if filename not in _parsed_files:
        xml.sax.parse(os.path.join(_root, filename), 
                      EventHandler(read_xml(current_scope, target)))
        _parsed_files.add(filename)


def parse_index(index_filename):
    import os
    global _root
    _root, index_filename = os.path.split(index_filename)
    l = []
    parse(index_filename, '', set_key('parent', None, put_in_list(l)))
    return l


if __name__ == '__main__':
    l = parse_index('index.xml')
    print l
