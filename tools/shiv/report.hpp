#pragma once

#include <cstdint>
#include <string>

class shn::Scene;
class shn::OslRenderer;
class shn::BiocScene;

struct ShibStatistics {
    uint64_t loadBiocScenesTime = 0;
    uint64_t loadJSONTime = 0;

    uint64_t sceneBuildTime = 0;
    uint64_t updateSceneFromBioc = 0;
    uint64_t loadShadingAssignmentFromJSONBuffer = 0;
    uint64_t loadLayersFromJSONBuffer = 0;
    uint64_t loadMemoryImages = 0;
    uint64_t commitScene = 0;

    uint64_t rendererTime = 0;

    uint64_t bindingPostProcessTime = 0;
    uint64_t writeImagesTime = 0;

    std::string shiningVersion;
    std::string shiningDescription;
    std::string vertigoVersion;
    std::string vdbFilename;
    std::string commandLine;

    uint64_t instanceCount = 0;

    uint64_t totalTime() const {
        return loadBiocScenesTime + loadJSONTime + sceneBuildTime + rendererTime + bindingPostProcessTime + writeImagesTime;
    }
};

void report(shn::Scene * scene, const shn::OslRenderer * renderer,
    const int32_t cameraId, const int32_t width, const int32_t height,
    const ShibStatistics& shibStatistics,
    const char * templatePath,
    const char * output, bool reportPerfImages);