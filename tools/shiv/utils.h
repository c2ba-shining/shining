#ifndef __UTILS_H__
#define __UTILS_H__

#include <shining/Shining.h>

int32_t loadFile(const char * fileName, char ** buffer, uint64_t * bufferSize);

#endif // __UTILS_H__