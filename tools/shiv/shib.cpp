#include "getopt.h"
#include "utils.h"
#include "Transform.hpp"
#include "LinearAlgebra.hpp"

#include <shining/Shining.h>
#include <shining/Scene.h>
#include <shining/BiocScene.h>
#include <shining/JSONShaderAssignment.h>
#include <shining/GltfScene.h>
#include <shining/OslRenderer.h>
#include <shining/HighResolutionTimerGuard.h>
#include <shining/IMathUtils.h>
#include <shining/LoggingUtils.h>

#include <OpenImageIO/imageio.h>
#include <OpenImageIO/filesystem.h>

#ifdef _WIN32 // include for Windows 7 fix
#include <OpenEXR/ImfThreading.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <atomic>
#include <thread>
#include <regex>
#include <numeric>
#include <memory>

#include "report.hpp"

namespace oiio = OIIO_NAMESPACE;

#ifdef _WIN32
#define FILE_SEPARATOR   "\\"
#else
#define FILE_SEPARATOR   "/"
#endif

#ifndef DEBUG_PRINT
#define DEBUG_PRINT 1
#endif

#if DEBUG_PRINT == 0
#define debug_print(FORMAT, ...) ((void)0)
#else
#ifdef _MSC_VER
#define debug_print(FORMAT, ...) \
    fprintf(stderr, "%s() in %s, line %i: " FORMAT "\n", \
        __FUNCTION__, __FILE__, __LINE__, __VA_ARGS__)
#else
#define debug_print(FORMAT, ...) \
    fprintf(stderr, "%s() in %s, line %i: " FORMAT "\n", \
        __func__, __FILE__, __LINE__, __VA_ARGS__)
#endif
#endif

/* Short command line options for get_opt */
const char * SHORT_OPTIONS = "hr:s:c:l:o:b:f:p:j:";
/* Long options for getopt_long */
const char * LONG_OPTION_REPORT = "report";
const char * LONG_OPTION_REPORT_PERF_IMAGES = "report-perf-images";
const char * LONG_OPTION_PROGRESS = "progress";
const char * LONG_OPTION_TIME = "time";
const char * LONG_OPTION_ROOT = "root";
const char * LONG_OPTION_CAMERATYPE = "camera-type";
const char * LONG_OPTION_NOTEXTURECHECK = "no-texturecheck";
const char * LONG_OPTION_PIXEL_SAMPLE_CLAMP = "pixel-sample-clamp";
const char * LONG_OPTION_SAMPLE_OFFSET = "sample-offset";
const char * LONG_OPTION_IGNORE_STRAIGHT_BOUNCES = "ignore-straight-bounces";
const char * LONG_OPTION_PASS_FLAGS = "pass-flags";
const char * LONG_OPTION_GLTF = "gltf";
const char * LONG_OPTION_GLTF_ENVMAP = "gltf_envmap";
const struct option LONG_OPTIONS[]=
{
    {"help", no_argument, NULL,'h'}, /*  Print help */
    {LONG_OPTION_REPORT, optional_argument, NULL, 0}, /* Report mode */
    {LONG_OPTION_PROGRESS, no_argument, NULL, 0}, /* Progress bar mode */
    {"resolution", required_argument, NULL,'r'}, /*  Image resolution */
    {"samples", required_argument, NULL,'s'}, /*  Number of samples */
    {"camera", required_argument, NULL,'c'}, /*  Camera name */
    {"layer", required_argument, NULL, 'l'}, /* Layer name */
    {"max_bounces", required_argument, NULL,'b'}, /*  Maximum number of bounces */
    {"output", required_argument, NULL,'o'}, /*  Output file */
    {"threads", required_argument, NULL,'j'}, /*  Number of threads */
    {"filter", required_argument, NULL,'f'}, /*  Filter type */
    {"pass", required_argument, NULL,'p'}, /*  Passes */
    {LONG_OPTION_PASS_FLAGS, required_argument, NULL, 0},
    {LONG_OPTION_ROOT, required_argument, NULL, 0}, /*  Root rewrite */
    {LONG_OPTION_TIME, required_argument, NULL, 0}, /*  Camera time */
    {LONG_OPTION_CAMERATYPE, required_argument, NULL, 0}, /* Type of the camera */
    {LONG_OPTION_NOTEXTURECHECK, no_argument, NULL, 0}, /* Active no texture check */
    {LONG_OPTION_PIXEL_SAMPLE_CLAMP, required_argument, NULL, 0}, /* Override pixel sample clamp */
    {LONG_OPTION_SAMPLE_OFFSET, required_argument, NULL, 0}, /* Specify the index of the first sample to render */
    {LONG_OPTION_IGNORE_STRAIGHT_BOUNCES, required_argument, NULL, 0}, /* 0: no effect; 1: active straight bounces ignorance */
    {LONG_OPTION_REPORT_PERF_IMAGES, required_argument, nullptr, 0},
    {LONG_OPTION_GLTF, required_argument, nullptr, 0 },
    {LONG_OPTION_GLTF_ENVMAP, required_argument, nullptr, 0},
    {NULL, 0, NULL, 0} /*  End of array need by getopt_long do not delete it */
};

/* Handle command line options */
struct cli_opts_t
{
    int32_t width;
    int32_t height;
    int32_t samples;
    int32_t sampleOffset;
    bool overrideMaxBounce;
    int32_t maxBounces;
    int32_t threadCount;
    std::string report;
    bool reportPerfImages;
    bool overrideReportPerfImages;
    int32_t progress;
    float openTime;
    float closeTime;
    float shutterOffset;
    bool overrideFilter;
    std::string filter;
    std::string biocFileName;
    std::string jsonFileName;
    std::string gltfFileName;
    std::string gltfEnvmap;
    std::vector<std::string> longPassNames;
    std::vector<std::string> shortPassNames;
    std::vector<std::string> passesDenoiseFlags;
    std::string cameraName;
    std::string cameraType;
    const char * layerName;
    std::string outputFileName;
    std::string rootSource;
    std::string rootDestination;
    shn::TexturePolicy texturePolicy;
    bool overridePixelSampleClamp;
    float pixelSampleClamp = 0.f;
    int ignoreStraightBounces;
};

/* Parse command line options and fill BruteOptions structure */
void parse_cli_opts(int argc, char **argv, cli_opts_t * opts);
/* Print usage on standard output */
void usage();

void updateReportPath(std::string & reportPath, const std::string & newReportPath, float time);

static void progressCallback(int32_t, void * appData)
{
    auto & progress = *static_cast<std::atomic<int32_t> *>(appData);
    ++progress;
}

void copyFramebufferForImageWriting(float * outImageRGBA, const float * inImage, const float * inAlpha, int width, int height, int32_t inImageChannelCount)
{
    assert(inImageChannelCount <= 4);

    std::fill(outImageRGBA, outImageRGBA + width * height * 4, 0.f);

    for (int j = 0; j < height; ++j)
    {
        for (int i = 0; i < width; ++i)
        {
            int base = j * width + i;
            int outBase = (height - (j + 1)) * width + i;

            for (int32_t c = 0; c < inImageChannelCount; ++c)
                outImageRGBA[outBase * 4 + c] = inImage[base * inImageChannelCount + c];

            if (inImageChannelCount < 4)
                outImageRGBA[outBase * 4 + 3] = inAlpha[base];
        }
    }
}

std::string getCleanExePath(const char * argv0)
{
    std::string exePath(argv0);
    const size_t sepPos = exePath.find_last_of(FILE_SEPARATOR);
    if (sepPos != std::string::npos) {
        exePath = exePath.substr(0, sepPos + 1);
    }
    else {
        exePath.clear();
    }
    return exePath;
}

oiio::ImageSpec initImageSpec(size_t width, size_t height, const std::string & shibCommandLine);

void completeImageSpecWithBiocJsonScene(oiio::ImageSpec & spec, const shn::VertigoInfo & vertigoInfo, shn::BiocScene * biocScene, const std::string & cameraName, float openTime);

void writeEXRImage(const std::string & outputFileName, size_t width, size_t height, const float * framebufferData, size_t framebufferChannelCount, const float * alphaFramebufferData,
    const oiio::ImageSpec & spec);

std::string getEXROutputFilename(cli_opts_t &options, int32_t p, bool isStereoRendering, int32_t eyeId, bool isDenoised = false);

using namespace shn;

int main(int argc, char **argv)
{
    fprintf(stdout, "Shining in a Boat\n\nVersion %s\n\n%s\n\n", SHINING_VERSION, SHINING_DESCRIPTION);

    // get useful paths
    const std::string exePath = getCleanExePath(argv[0]);
    const char * shaderPathEnv = getenv("SHINING_SHADER_PATH");
    std::string shaderPath = (shaderPathEnv) ? shaderPathEnv : "";
    std::string shaderPathDir = exePath + "shaders";
    std::string templatePath = exePath + "report.tpl";
    if(!shaderPath.empty())
        shaderPath += std::string(";");
    shaderPath += shaderPathDir; // add current_directory, 'shaders' directory and SHINING_SHADER_PATH to shader path
    std::string shaderStdIncludePath = shaderPathDir + std::string(FILE_SEPARATOR) + std::string("stdosl.h");

    // must be setup before creating an OslRenderder object
    shn::OslRenderer::setDefaultShaderPath(shaderPath.c_str());
    shn::OslRenderer::setShaderStdIncludePath(shaderStdIncludePath.c_str());
#ifdef _WIN32
    // let OIIO choose the thread count
    OIIO_NAMESPACE::attribute("threads", 0); // Fix for OIIO 1.8.14 on Windows 7
#endif

    // parse command line arguments
    setvbuf(stdout, NULL, _IONBF, 0);
    cli_opts_t options;
    parse_cli_opts(argc, argv, &options);
    if(options.progress)
        fprintf(stdout, "TR_PROGRESS 000%%\n");

    const int32_t width = options.width;
    const int32_t height = options.height;
    const int32_t samples = options.samples;
    const int32_t sampleOffset = options.sampleOffset;
    int32_t status;

    // OSLRENDERER
    shn::OslRenderer renderer;
    renderer.attribute(SHN_ATTR_RENDER_EXEPATH, exePath.c_str());
    renderer.attribute("renderer:shutter_offset", options.shutterOffset);
    if (options.threadCount)
        renderer.attribute("renderer:thread_count", options.threadCount);

    // SCENE
    shn::Scene scene;
    scene.attribute("scene:static_scene", 0);

    // CAMERA
    shn::Scene::Id cameraId;

    // Check for stereo rendering
    bool isStereoRendering = (options.cameraType == SHN_CAM_TYPE_STEREO360);
    int32_t eyeCount = (isStereoRendering) ? 2 : 1;

    // SHIBSTATISTICS
    ShibStatistics shibStatistics;
    shibStatistics.shiningVersion = SHINING_VERSION;
    shibStatistics.shiningDescription = SHINING_DESCRIPTION;
    shibStatistics.commandLine = "";
    for (int i = 0; i < argc; ++i)
        shibStatistics.commandLine += std::string(argv[i]) + " ";

    // IMAGE METADATA
    oiio::ImageSpec imageSpec = initImageSpec(width, height, shibStatistics.commandLine);

    // Init progress thread
    std::atomic<int32_t> progress(0);
    int32_t progressMax = 0;
    std::thread progressPrinter;
    if (options.progress)
    {
        const auto tileSize = 16;
        const auto tileX = (width + tileSize - 1) / tileSize;  // nearest bigger int
        const auto tileY = (height + tileSize - 1) / tileSize;
        progressMax = samples * tileX * tileY * eyeCount;
        renderer.progressFunctions(nullptr, &progressCallback, &progress);

        progressPrinter = std::thread([&progress, &progressMax]()
        {
            while (progress != progressMax)
            {
                std::this_thread::sleep_for(std::chrono::seconds(1));
                fprintf(stdout, "TR_PROGRESS %03d%%\n", (int)((progress * 100) / progressMax)); // Tractor progress format
            }
        });
    }

    if (!options.gltfFileName.empty()) // GLTF
    {
        fprintf(stdout, "Load glTF scene : %s\n", options.gltfFileName.c_str());
        status = shn::updateSceneFromGltf(&scene, &renderer, options.gltfFileName.c_str(), &cameraId, options.gltfEnvmap.c_str());
    }
    else if (!options.biocFileName.empty() || !options.jsonFileName.empty()) // BIOC / JSON
    {
        // Load cache files
        fprintf(stdout, "Load Bioc/Json scene : %s %s", options.biocFileName.c_str(), options.jsonFileName.c_str());
        shn::BiocScene biocScene;
        char * jsonDataBuffer = nullptr;
        uint64_t jsonDataSize = 0;
        {
            HighResolutionTimerGuard guard(&shibStatistics.loadBiocScenesTime);
#ifdef _MSC_VER
            status = shn::loadBioc(&biocScene, options.biocFileName.c_str(), BIOC_READ_LOAD);
#else
            status = shn::loadBioc(&biocScene, options.biocFileName.c_str(), BIOC_READ_MMAP);
#endif 
            if (status == -2)
            {
                fprintf(stderr, "Wrong Bioc scene version in %s : %d (current version is %d) \n", options.biocFileName.c_str(), biocScene.container->version, SHINING_BIOC_VERSION);
                exit(EXIT_FAILURE);
            }
            else if (status < 0)
            {
                fprintf(stderr, "Cannot read Bioc file or Bioc file does not contain a scene\n");
                exit(EXIT_FAILURE);
            }
            fprintf(stdout, "Bioc scene %s : %d instances : %d meshes : %d instanceSets : %d lights : %d cameras\n",
                options.biocFileName.c_str(),
                biocScene.container->instanceCount,
                biocScene.container->meshCount,
                biocScene.container->instanceSetCount,
                biocScene.container->lightCount,
                biocScene.container->cameraCount);

            shibStatistics.instanceCount = biocScene.container->instanceCount;
        }

        {
            HighResolutionTimerGuard guard(&shibStatistics.loadJSONTime);
            status = loadFile(options.jsonFileName.c_str(), &jsonDataBuffer, &jsonDataSize);
            if (status < 0)
            {
                fprintf(stderr, "Impossible to load JSON file : %s - Exit now !\n", options.jsonFileName.c_str());
                exit(EXIT_FAILURE);
            }
        }

        shn::VertigoInfo vertigoInfo;
        shn::getVertigoInfo(&biocScene, &vertigoInfo);
        shibStatistics.vertigoVersion = vertigoInfo.version;
        shibStatistics.vdbFilename = vertigoInfo.vdbFilename;
        completeImageSpecWithBiocJsonScene(imageSpec, vertigoInfo, &biocScene, options.cameraName, options.openTime);

        // Build the scene
        {
            HighResolutionTimerGuard guard(&shibStatistics.sceneBuildTime);

            // LOAD BIOC : geometry, cameras and lights
            {
                HighResolutionTimerGuard innerGuard(&shibStatistics.updateSceneFromBioc);
                status = shn::updateSceneFromBioc(&scene, &renderer, options.openTime, options.closeTime, &biocScene);

                cameraId = scene.getCameraIdFromName(options.cameraName.c_str());
                if (cameraId == -2)
                {
                    fprintf(stderr, "Multiple camera matching name : %s. Error code : %d\n", options.cameraName.c_str(), cameraId);
                    exit(EXIT_FAILURE);
                }
                else if (cameraId < 0)
                {
                    fprintf(stderr, "Impossible to find camera %s. Error code : %d\n", options.cameraName.c_str(), cameraId);
                    exit(EXIT_FAILURE);
                }
            }

            // LOAD JSON : shading trees, assignments and renderer attributes
            std::set<std::string> memoryImagesFilenames;
            {
                HighResolutionTimerGuard innerGuard(&shibStatistics.loadShadingAssignmentFromJSONBuffer);
                status = shn::loadShadingAssignmentFromJSONBuffer(&scene, &renderer, jsonDataSize, jsonDataBuffer,
                    options.rootSource.c_str(), options.rootDestination.c_str(), static_cast<int>(options.openTime + (options.closeTime - options.openTime) / 2.f),
                    memoryImagesFilenames, options.texturePolicy);

                if (status < 0)
                {
                    fprintf(stderr, "Impossible to use JSON shading assignment file : %s - Exit now !\n", options.jsonFileName.c_str());
                    exit(EXIT_FAILURE);
                }

                // Override JSON renderer attributes with command line
                if (options.overrideMaxBounce)
                    renderer.attribute(SHN_ATTR_RENDER_MAXBOUNCES, options.maxBounces);
                if (options.overrideFilter)
                    renderer.attribute(SHN_ATTR_RENDER_RAYFILTER, options.filter.c_str());
                if (options.overridePixelSampleClamp)
                    renderer.attribute(SHN_ATTR_RENDER_PIXELSAMPLECLAMP, options.pixelSampleClamp);
                if (options.ignoreStraightBounces >= 0)
                    renderer.attribute(SHN_ATTR_RENDER_IGNORESTRAIGHTBOUNCES, (options.ignoreStraightBounces == 1));

                // Override command line with JSON renderer attributes
                const char * rendererReportPath = nullptr;
                if (0 == renderer.getAttribute(SHN_ATTR_RENDER_REPORTPATH, &rendererReportPath) && options.report.empty())
                    updateReportPath(options.report, rendererReportPath, options.openTime);
                int32_t reportPerfImages;
                if (0 == renderer.getAttribute(SHN_ATTR_RENDER_REPORTPERFIMAGES, &reportPerfImages) && !options.overrideReportPerfImages)
                    options.reportPerfImages = bool(reportPerfImages);

                if (!options.report.empty() && options.reportPerfImages)
                {
                    renderer.attribute("renderer:instance_counters", 1);
                }
            }

            // LOAD JSON : render layers and passes
            {
                HighResolutionTimerGuard innerGuard(&shibStatistics.loadLayersFromJSONBuffer);
                status = shn::loadLayersFromJSONBuffer(&scene, &renderer, jsonDataSize, jsonDataBuffer, options.layerName, options.openTime,
                    options.longPassNames, options.shortPassNames, options.passesDenoiseFlags, &biocScene);

                if (status < 0)
                {
                    fprintf(stderr, "Impossible to use JSON layer file : %s - Exit now !\n", options.jsonFileName.c_str());
                    exit(EXIT_FAILURE);
                }

                if (!options.passesDenoiseFlags.empty())
                {
                    std::string denoisePassList;
                    for (size_t i = 0u; i < options.passesDenoiseFlags.size(); ++i)
                    {
                        const auto denoiseFlag = options.passesDenoiseFlags[i];
                        if (denoiseFlag != SHN_DENOISEFLAG_RAW)
                            denoisePassList += options.shortPassNames[i] + " ";
                    }
                    if (!denoisePassList.empty())
                    {
                        denoisePassList = denoisePassList.substr(0, denoisePassList.size() - 1); // Remove last space
                        renderer.attribute(SHN_ATTR_RENDER_DENOISEPASSLIST, denoisePassList.c_str());
                    }
                }
            }

            // LOAD BIOC : memory images found in the JSON
            {
                HighResolutionTimerGuard innerGuard(&shibStatistics.loadMemoryImages);
                loadMemoryImages(&biocScene, memoryImagesFilenames, options.openTime);
            }

            // COMMIT SCENE
            {
                HighResolutionTimerGuard innerGuard(&shibStatistics.commitScene);
                status = scene.commitScene(&renderer);
            }
        }
    }
    else
    {
        fprintf(stderr, "No scene files were given in arguments\n");
        exit(EXIT_FAILURE);
    }
    
    // Check that output directories exist:
    // #todo : let Shining create the output directories
    const int32_t selectedPassCount = (int32_t)options.longPassNames.size();
    for (int32_t i = 0; i < selectedPassCount; ++i)
    {
        // Test if the output directory exists
        auto outputFileName = options.outputFileName;

        auto sepPos = outputFileName.find_last_of('\\'); // Windows Farm
        if(sepPos == std::string::npos) {
            sepPos = outputFileName.find_last_of('/'); // Good systems
        }

        if(sepPos != std::string::npos) {
            outputFileName = outputFileName.substr(0, sepPos);
        }

        size_t passPos;
        do 
        {
            passPos = outputFileName.find("%p");
            if (passPos != std::string::npos)
                outputFileName.replace(passPos, 2, options.shortPassNames[i]);
        } while(passPos != std::string::npos);

        if(!oiio::Filesystem::is_directory(outputFileName)) {
            fprintf( stderr, "error: Directory %s does not exists.\n", outputFileName.c_str());
            exit( EXIT_FAILURE );
        }
    }

    // EYE LOOP BEGIN
    for (int32_t eyeId = 0; eyeId < eyeCount; ++eyeId)
    {
        // set camera type
        if (isStereoRendering)
        {
            if (eyeId == 0)
                CameraAttr::type(scene, cameraId, SHN_CAM_TYPE_STEREO360_L);
            else
                CameraAttr::type(scene, cameraId, SHN_CAM_TYPE_STEREO360_R);
        }
        else
        {
            CameraAttr::type(scene, cameraId, options.cameraType.c_str());
        }
        scene.commitCamera(cameraId);

        shn::OslRenderer::Id bindingId;
        renderer.genBindings(&bindingId, 1);

        {
            HighResolutionTimerGuard guard(&shibStatistics.rendererTime);
    
            for (int32_t i = 0; i < selectedPassCount; ++i)
                renderer.bindingEnablePass(bindingId, options.longPassNames[i].c_str());

            renderer.bindingInit(bindingId, width, height);

            /* Accumulate */
            status = renderer.render(&scene, cameraId, bindingId, 0, width, 0, height, sampleOffset, sampleOffset + samples);
            if (status == -1)
            {
                fprintf(stderr, "Nothing will be rendered\n");
                exit( EXIT_FAILURE );
            }
            renderer.waitBinding(bindingId, -1);
        }

        SHN_LOGINFO << "> Render finished";

        {
            HighResolutionTimerGuard g(&shibStatistics.bindingPostProcessTime);
            renderer.bindingPostProcess(bindingId);
        }

        SHN_LOGINFO << "> Post Process finished";

        {
            HighResolutionTimerGuard g(&shibStatistics.writeImagesTime);

            int32_t alphaFramebufferId = -1;
            renderer.getBindingLocationFramebuffer(bindingId, "pass:alpha", &alphaFramebufferId);
            const auto alphaFramebufferData = renderer.getFramebufferData(alphaFramebufferId);
            assert(alphaFramebufferData);

            for (int32_t p = 0; p < selectedPassCount; ++p)
            {
                int32_t framebufferId = -1;
                renderer.getBindingLocationFramebuffer(bindingId, options.longPassNames[p].c_str(), &framebufferId);

                if (framebufferId < 0)
                {
                    fprintf(stderr, "Warning: skipping %s pass not recognized by Shining.\n", options.shortPassNames[p].c_str());
                    continue;
                }

                const auto writeBoth = [&]()
                {
                    writeEXRImage(getEXROutputFilename(options, p, isStereoRendering, eyeId),
                        width, height,
                        renderer.getFramebufferData(framebufferId), renderer.getFramebufferChannelCount(framebufferId), alphaFramebufferData,
                        imageSpec);

                    framebufferId = -1;
                    renderer.getBindingLocationFramebuffer(bindingId, (options.longPassNames[p] + ".denoised").c_str(), &framebufferId);
                    if (framebufferId >= 0)
                    {
                        writeEXRImage(getEXROutputFilename(options, p, isStereoRendering, eyeId, true),
                            width, height,
                            renderer.getFramebufferData(framebufferId), renderer.getFramebufferChannelCount(framebufferId), alphaFramebufferData,
                            imageSpec);
                    }
                };

                if (!options.passesDenoiseFlags.empty())
                {
                    if (options.passesDenoiseFlags[p] == SHN_DENOISEFLAG_BOTH)
                    {
                        writeBoth();
                    }
                    else if (options.passesDenoiseFlags[p] == SHN_DENOISEFLAG_RAW)
                    {
                        writeEXRImage(getEXROutputFilename(options, p, isStereoRendering, eyeId),
                            width, height,
                            renderer.getFramebufferData(framebufferId), renderer.getFramebufferChannelCount(framebufferId), alphaFramebufferData,
                            imageSpec);
                    }
                    else
                    {
                        framebufferId = -1;
                        renderer.getBindingLocationFramebuffer(bindingId, (options.longPassNames[p] + ".denoised").c_str(), &framebufferId);
                        if (framebufferId >= 0)
                        {
                            writeEXRImage(getEXROutputFilename(options, p, isStereoRendering, eyeId),
                                width, height,
                                renderer.getFramebufferData(framebufferId), renderer.getFramebufferChannelCount(framebufferId), alphaFramebufferData,
                                imageSpec);
                        }
                    }
                }
                else
                {
                    writeBoth();
                }
            }
        }

        SHN_LOGINFO << "> Images written";
    }
    // EYE LOOP END

    //assert(progress == progressMax);
    if(options.progress)
    {
        progress = progressMax;
        progressPrinter.join();
    }

    uint64_t reportTime = 0;
    if(!options.report.empty())
    {
        HighResolutionTimerGuard g(&reportTime);
        report(&scene, &renderer, cameraId, width, height,
            shibStatistics, templatePath.c_str(), options.report.c_str(), options.reportPerfImages);
    }
    SHN_LOGINFO << "Time to write report: " << us2sec(reportTime) << " sec.";

#ifdef _WIN32
    OIIO_NAMESPACE::attribute("threads", 1); // Fix for OIIO 1.8.14 on Windows 7
    // need to manually kill threads, else the dll unload will freeze on exit
    Imf::setGlobalThreadCount(0); // Fix for OIIO 1.8.14 on Windows 7
#endif

    exit( EXIT_SUCCESS );
}

void parse_cli_opts(int argc, char **argv, cli_opts_t * opts)
{
    char c;
    int optIdx = 0;
    opts->width = 800;
    opts->height = 600;
    opts->samples = 4;
    opts->sampleOffset = 0;
    opts->outputFileName = "";
    opts->cameraName;
    opts->cameraType = SHN_CAM_TYPE_DOF;
    opts->layerName = "default";
    opts->progress = 0;
    opts->openTime = 0.f;
    opts->closeTime = 0.f;
    opts->shutterOffset = 0.f;
    opts->overrideMaxBounce = false;
    opts->overrideFilter = false;
    opts->overridePixelSampleClamp = false;
    opts->maxBounces = 6;
    opts->threadCount = 0;
    opts->texturePolicy = shn::TEXTURE_CHECK;
    opts->ignoreStraightBounces = -1;
    opts->overrideReportPerfImages = false;
    opts->reportPerfImages = true;
    std::string passArg = "";
    std::string passFlagsArg = "";
    while ((c = shiv_getopt_long(argc, argv, (char *) SHORT_OPTIONS, (option *) LONG_OPTIONS, &optIdx)) != -1)
    {
        switch (c)
        {
        case 0:
            if (LONG_OPTIONS[optIdx].name == LONG_OPTION_REPORT)
            {
                if(optarg)
                {
                    char * ext = strrchr(optarg, '.');
                    if(ext && (strcmp(ext, ".json")==0 || strcmp(ext, ".html")==0))
                    {
                        opts->report = optarg;
                    }
                    else
                    {
                        fprintf(stderr, "--report parameter must be empty or a JSON file path or an HTML file path");
                        usage();
                        exit(EXIT_FAILURE);
                    }
                }
                else
                {
                    opts->report = "";  // non-NULL
                }
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_REPORT_PERF_IMAGES)
            {
                auto reportPerfImages = int32_t{ opts->reportPerfImages };
                sscanf(optarg, "%d", &reportPerfImages);
                opts->reportPerfImages = reportPerfImages ? true : false;
                opts->overrideReportPerfImages = true;
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_PROGRESS)
            {
                opts->progress = 1;
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_TIME)
            {
                std::stringstream ss(optarg);
                std::string timeValue;
                int32_t timeCount = 0;
                while(std::getline(ss, timeValue, ':')) //split time values inside a trio
                {
                    switch(timeCount)
                    {
                    case 0: //openTime
                        sscanf(timeValue.c_str(), "%f", &(opts->openTime));
                        opts->closeTime = opts->openTime;
                        break;
                    case 1: //closeTime
                        sscanf(timeValue.c_str(), "%f", &(opts->closeTime));
                        break;
                    case 2: //shutterOffset
                        sscanf(timeValue.c_str(), "%f", &(opts->shutterOffset));
                        break;
                    default:
                        fprintf(stderr, "Too many values in time triplet(%s)\n", ss.str().c_str());
                        usage();
                        exit(EXIT_FAILURE);
                        break;
                    }
                    ++timeCount;
                }
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_ROOT)
            {
                if (strchr(optarg, ','))
                {
                    std::string rootArg = optarg;
                    char * tokens = new char[rootArg.size() + 1];
                    strcpy(tokens, rootArg.c_str());
                    char * token = strtok (tokens, ",");
                    opts->rootSource = token;
                    token = strtok (NULL, ",");
                    opts->rootDestination = token;
                    delete[] tokens;
                }
                else
                {
                    fprintf(stderr, "Root option improperly formatted\n");
                    usage();
                    exit(EXIT_FAILURE);
                }
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_CAMERATYPE)
            {
                opts->cameraType = optarg;
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_NOTEXTURECHECK)
            {
                opts->texturePolicy = shn::NO_TEXTURE_CHECK;
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_PIXEL_SAMPLE_CLAMP)
            {
                opts->overridePixelSampleClamp = true;
                sscanf(optarg, "%f", & (opts->pixelSampleClamp));
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_SAMPLE_OFFSET)
            {
                sscanf(optarg, "%d", & (opts->sampleOffset));
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_IGNORE_STRAIGHT_BOUNCES)
            {
                sscanf(optarg, "%d", &opts->ignoreStraightBounces);
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_PASS_FLAGS)
            {
                passFlagsArg = optarg;
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_GLTF)
            {
                opts->gltfFileName = optarg;
            }
            else if (LONG_OPTIONS[optIdx].name == LONG_OPTION_GLTF_ENVMAP)
            {
                opts->gltfEnvmap = optarg;
            }
            break;
        case 'h' : /* Print usage and exit */
            usage();
            exit(EXIT_SUCCESS);
            break;
        case 'r' : /* Resolution */
            sscanf(optarg, "%dx%d", & (opts->width), & (opts->height));
            break;
        case 's' : /* Number of samples */
            sscanf(optarg, "%d", & (opts->samples));
            break;
        case 'c' :  /* Camera name */
            opts->cameraName = std::string(strdup(optarg));
            break;
        case 'l' :
            opts->layerName = strdup(optarg);
            break;
        case 'o' :  /* Output file */
            opts->outputFileName = optarg;
            break;
        case 'b' : /* Maximum number of bounces */
            sscanf(optarg, "%d", & (opts->maxBounces));
            opts->overrideMaxBounce = true;
            break;
        case 'j' : /* Maximum number of threads (0 is auto) */
            sscanf(optarg, "%d", & (opts->threadCount));
            break;
        case 'f' : /* Filter */
            opts->overrideFilter = true;
            opts->filter = optarg;
            break;
        case 'p' :
            fprintf(stderr, "Pass '%s' \n", optarg);
            passArg = optarg;
            break;         
        case '?' : /* Wut? */
            fprintf(stderr, "Unknown option '%s' %d \n", argv[optind], optind);
            usage();
            exit(EXIT_FAILURE);
        default :
            break;
        }
    }

    for (int32_t i = optind; i < argc; ++i)
    {
        const char * fileName = argv[i];
        const char * match = strstr(fileName, ".bioc");
        if (match)
        {
            opts->biocFileName = fileName;
        }
        else
        {
            match = strstr(fileName, ".json");
            if (match)
            {
                opts->jsonFileName = fileName;
            }
        }
    }

    if (passArg != "")
    {
        std::unique_ptr<char[]> tokens = std::make_unique<char[]>(passArg.size() + 1);
        strcpy(tokens.get(), passArg.c_str());
        char * token = strtok (tokens.get(), ",");
        while (token != NULL)
        {
            opts->longPassNames.push_back(std::string("pass:")+ std::string(token));
            opts->shortPassNames.push_back(std::string(token));
            token = strtok (NULL, ",");
        }
    }
    if (passFlagsArg != "")
    {
        std::unique_ptr<char[]> tokens = std::make_unique<char[]>(passFlagsArg.size() + 1);
        strcpy(tokens.get(), passFlagsArg.c_str());
        char * token = strtok(tokens.get(), ",");
        while (token != NULL)
        {
            opts->passesDenoiseFlags.push_back(std::string(token));
            token = strtok(NULL, ",");
        }
    }
}

void updateReportPath(std::string & reportPath, const std::string & newReportPath, float time)
{
    std::string path = newReportPath;
    const auto idx = path.find_first_of("${");
    if (idx != std::string::npos)
    {
        const auto n = path.find_first_of('}', idx);
        if (n != std::string::npos)
        {
            const auto pattern = path.substr(idx, n - idx + 1);
            const auto timePos = pattern.find_first_of("time");
            if (timePos != std::string::npos)
            {
                size_t digitCount = 0;
                const auto commaIdx = pattern.find_first_of(",", timePos);
                if (commaIdx != std::string::npos)
                {
                    std::stringstream ss;
                    ss << pattern.substr(commaIdx + 1);
                    ss >> digitCount;
                }
                std::stringstream ss;
                if (digitCount)
                    ss << std::setw(digitCount) << std::setfill('0');
                ss << time;
                path.replace(idx, n - idx + 1, ss.str());
                reportPath = path;
            }
            else
                SHN_LOGERR << "JSon renderer report path with bad pattern (not containing 'time' keyword): " << pattern;
        }
        else
            SHN_LOGERR << "JSon renderer report path with ${ but no }";
    }
}

void usage()
{
    fprintf(stdout, "shib [-h] -r [RESOLUTION] -l [LAYER] --time [TIMES] -c [CAMERA] -s [SAMPLES] -b [BOUNCES] -o [OUTPUT FILE]\n \
\t[-f [FILTER]] [--camera-type [CAMERA TYPE]] [--pixel-sample-clamp [SAMPLE CLAMP]] [--sample-offset [OFFSET]]\n \
\t[-p [PASSES]] [--pass-flags [FLAGS]] [--ignore-straight-bounces] [--root [TEXTURE_ROOT_DIR]] [--no-texturecheck]\n \
\t[--report [REPORT FILE PATH]] [--report-perf-image] [--progress] [-j [THREAD COUNT]]\n \
\tBIOC_FILE JSON_FILE\n \
\n \
Render image with Shining, from a given scene\n \
\n \
required arguments:\n \
  -h, --help\n \
  \t(no arg) show this help message and exit\n \
  -r, --resolution\n \
  \t(string) 'wxh': output image resolution\n \
  -l, --layer\n \
  \t(string) render layer\n \
  --time\n \
  \t(float[:float:float]) 'start frame:end frame:shutter offset': frame shutter to render. endframe and shutteroffset are optional if no motion blur is needed\n \
  -c, --camera\n \
  \t(string) complete or partial name of the camera. If zero or multiple camera names match the input string, shib will exit\n \
  -s, --samples\n \
  \t(int) AA samples count\n \
  -b, --max_bounces\n \
  \t(int) maximum bounces for each paths\n \
  -o, --output\n \
  \t(string) path and filename of the output render image. Special rules :\n \
  \t\t - '%%p' will be replaced by a pass name for each requested passes (WARNING: Shining won't create directories)\n \
  \t\t - '%%0Xd' will be replaced by the frame number\n \
  \t\t - '_L' and '_R' suffixes will be added if the camera type is 'stereo360'\n \
  \t\t - '_denoised' suffix will be added to denoised pass images\n \
  BIOC_FILE\n \
  \t(string) Bioc filename to load\n \
  JSON_FILE\n \
  \t(string) Json filename to load\n \
\n \
optional arguments:\n \
  -f, --filter\n \
  \t(string) ray filter : 'bspline', 'triangle', 'center', 'box'\n \
  --camera-type\n \
  \t(string) camera type : 'pinhole', 'dof', 'latlong', 'stereo360', 'stereo360_L', 'stereo360_R', 'stereo360TopDown', 'orthographic' (default: 'dof')\n \
  --pixel-sample-clamp\n \
  \t(float) pixel sample clamp. 0 = no clamp (default: 0)\n \
  --sample-offset\n \
  \t(int) offset on samples to compute. Useful for stopping and restarting rendering at the same sample\n \
  -p, --pass\n \
  \t(string) passes list, separated by a ','\n \
  --pass-flags\n \
  \t(string) pass flags list, separated by a ','\n \
  --ignore-straight-bounces\n \
  \t(no arg) flag to go through transparent surfaces without increasing the bounces counter\n \
  --root\n \
  \t(string) 'path1,path2' : replace 'path1' by 'path2' in the media pathes list by the Json file\n \
  --no-texturecheck\n \
  \t(no arg) flag to not check and exit if an external media is not accessible\n \
  --report\n \
  \t(string) path and filename of the statistics report\n \
  -report-perf-image\n \
  \t(int) lag to compute instance counter images for the report. 1 to activate it, else 0\n \
  --progress\n \
  \t(no arg) print percentage progress\n \
  -j, --threads\n \
  \t(int) threads count to use\n \
");
}

oiio::ImageSpec initImageSpec(size_t width, size_t height, const std::string & shibCommandLine)
{
    oiio::ImageSpec spec(width, height, 4, oiio::TypeDesc::HALF);
    spec.attribute("oiio:ColorSpace", "Linear");
    spec.attribute("shiningVersion", SHINING_VERSION);
    spec.attribute("shiningDescription", SHINING_DESCRIPTION);
    spec.attribute("shibCommandLine", shibCommandLine);

    return spec;
}

void completeImageSpecWithBiocJsonScene(oiio::ImageSpec & spec, const shn::VertigoInfo & vertigoInfo, shn::BiocScene * biocScene, const std::string & cameraName, float openTime)
{
    spec.attribute("vertigoVersion", vertigoInfo.version.string());
    spec.attribute("vdbFilename", vertigoInfo.vdbFilename.string());

    /* Camera parameters and  metadata*/
    shn::CameraParameters cp;
    int32_t cameraCount = shn::getCameraCount(biocScene);
    for (int32_t j = 0; j < cameraCount; ++j)
    {
        std::string cameraFullName = shn::getCameraName(biocScene, j);
        if (cameraFullName.find(cameraName) != std::string::npos)
        {
            shn::getCameraParameters(biocScene, j, openTime, &cp);
            std::ostringstream ostr;
            ostr << cp.cameraToWorld[0];
            for (int i = 1; i < 16; ++i)
                ostr << "," << cp.cameraToWorld[i];
            spec.attribute("cameraTransform", OIIO_NAMESPACE::TypeDesc::TypeMatrix, cp.cameraToWorld);
            spec.attribute("cameraFocalPlane", cp.focalDistance);
            spec.attribute("cameraLensRadius", cp.lensRadius);
            spec.attribute("cameraNear", cp.nearPlane);
            spec.attribute("cameraFar", cp.farPlane);
            spec.attribute("cameraFocalLength", cp.focalLength);
            spec.attribute("cameraHalfFOVY", cp.hfovY);
            spec.attribute("cameraAspectRatio", cp.aspectRatio);
            spec.attribute("cameraVerticalAperture", cp.verticalAperture);
            spec.attribute("cameraHorizontalAperture", cp.horizontalAperture);
            spec.attribute("cameraName", cameraFullName);
        }
    }

    /* Production metadata */
    shn::MetadataInfo md;
    shn::getMetadata(biocScene, openTime, &md);
    for (int32_t j = 0, nMd = md.datas.size(); j < nMd; ++j)
    {
        oiio::TypeDesc crtType((oiio::TypeDesc::BASETYPE)md.types[j].basetype, (oiio::TypeDesc::AGGREGATE)md.types[j].aggregate, (oiio::TypeDesc::VECSEMANTICS)md.types[j].semantics);
        if (crtType == oiio::TypeDesc::STRING)
        {
            spec.attribute(md.names[j], std::string(static_cast<const char *>(md.datas[j])));
        }
        else
        {
            spec.attribute(md.names[j], crtType, md.datas[j]);
        }
    }
}

std::string getEXROutputFilename(cli_opts_t &options, int32_t chosenPassIdx, bool isStereoRendering, int32_t eyeId, bool isDenoised)
{
    // parse output image filename
    std::string outputFileName = options.outputFileName;

    // set the pass and the eye in case of stereo rendering
    size_t passPos;
    do
    {
        passPos = outputFileName.find("%p");
        if (passPos != std::string::npos)
        {
            std::string passName = options.shortPassNames[chosenPassIdx];
            if (isStereoRendering)
            {
                if (passPos > 0 && (outputFileName[passPos - 1] != '\\' && outputFileName[passPos - 1] != '/')) // modify passName only in filename and not in its path
                    passName += (eyeId == 0) ? "_L" : "_R";
            }
            outputFileName.replace(passPos, 2, passName);
        }
    } while (passPos != std::string::npos);

    if (isDenoised)
    {
        auto filename = oiio::Filesystem::filename(outputFileName);
        const auto dotPos = filename.find_first_of(".");
        if (dotPos != std::string::npos)
            filename = filename.substr(0, dotPos) + "_denoised" + filename.substr(dotPos);
        outputFileName = oiio::Filesystem::parent_path(outputFileName) + "/" + filename;
    }

    // set the frame number
    std::smatch matchResult;
    if (std::regex_search(outputFileName, matchResult, std::regex("\%0[0-9]d", std::regex_constants::basic)))
    {
        char padding[100];
        sprintf(padding, matchResult.str().c_str(), static_cast<int>(floor(options.openTime + options.shutterOffset * (options.closeTime - options.openTime))));
        outputFileName.replace(matchResult.position(), 4, padding);
    }

    return outputFileName;
}

void writeEXRImage(const std::string & outputFileName, size_t width, size_t height, const float * framebufferData, size_t framebufferChannelCount, const float * alphaFramebufferData,
    const oiio::ImageSpec & spec)
{
    oiio::ImageOutput * out = oiio::ImageOutput::create(outputFileName);
    bool writeStatus = out->open(outputFileName, spec);
    if (!writeStatus)
    {
        fprintf(stderr, "Impossible to write image %s \n", outputFileName.c_str());
        exit(EXIT_FAILURE);
    }

    std::unique_ptr<float[]> outbuffer(new float[width * height * 4]);
    copyFramebufferForImageWriting(outbuffer.get(), framebufferData, alphaFramebufferData, width, height, framebufferChannelCount);

    writeStatus = out->write_image(oiio::TypeDesc::FLOAT, outbuffer.get());
    if (!writeStatus)
    {
        fprintf(stderr, "Impossible to write image %s \n", outputFileName.c_str());
        exit(EXIT_FAILURE);
    }
    out->close();
    delete out;
#define CRAZY_CHECK
#ifdef CRAZY_CHECK
    oiio::ImageInput *in = oiio::ImageInput::open(outputFileName.c_str());
    if (!in)
    {
        fprintf(stderr, "Impossible to read back written image %s \n", outputFileName.c_str());
        exit(EXIT_FAILURE);
    }
    oiio::ImageSpec inspec = in->spec();
    float * b = new float[inspec.width * inspec.height * inspec.nchannels];
    bool ok = in->read_image(inspec.format, b);
    if (!ok)
    {
        fprintf(stderr, "Shining wrote a corrupted file %s \n", outputFileName.c_str());
        delete[] b;
        in->close();
        delete in;
        exit(EXIT_FAILURE);
    }
    delete[] b;
    in->close();
    delete in;
#endif
}