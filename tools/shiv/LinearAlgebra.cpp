#include "LinearAlgebra.hpp"

#include <cmath>
#include <sstream>

void mat4fToIdentity(float * mat)
{
	mat4fCopy(mat, MAT4FIDENTITY);
}

void mat4fTranspose(const float * __restrict__ matA, float * __restrict__ matB)
{
	matB[0] = matA[0]; matB[1] = matA[4]; matB[2] = matA[8]; matB[3] = matA[12];
	matB[4] = matA[1]; matB[5] = matA[5]; matB[6] = matA[9]; matB[7] = matA[13];
	matB[8] = matA[2]; matB[9] = matA[6]; matB[10] = matA[10]; matB[11] = matA[14];
	matB[12] = matA[3]; matB[13] = matA[7]; matB[14] = matA[11]; matB[15] = matA[15];
}

void mat4fInverse_old(const float * __restrict__ matA, float * __restrict__ matB)
{
	float det;
	matB[0] =	matA[5]*matA[10]*matA[15] - matA[5]*matA[11]*matA[14] - matA[9]*matA[6]*matA[15]
		+ matA[9]*matA[7]*matA[14] + matA[13]*matA[6]*matA[11] - matA[13]*matA[7]*matA[10];
	matB[4] =  -matA[4]*matA[10]*matA[15] + matA[4]*matA[11]*matA[14] + matA[8]*matA[6]*matA[15]
		- matA[8]*matA[7]*matA[14] - matA[12]*matA[6]*matA[11] + matA[12]*matA[7]*matA[10];
	matB[8] =	matA[4]*matA[9]*matA[15] - matA[4]*matA[11]*matA[13] - matA[8]*matA[5]*matA[15]
		+ matA[8]*matA[7]*matA[13] + matA[12]*matA[5]*matA[11] - matA[12]*matA[7]*matA[9];
	matB[12] = -matA[4]*matA[9]*matA[14] + matA[4]*matA[10]*matA[13] + matA[8]*matA[5]*matA[14]
		- matA[8]*matA[6]*matA[13] - matA[12]*matA[5]*matA[10] + matA[12]*matA[6]*matA[9];
	matB[1] =  -matA[1]*matA[10]*matA[15] + matA[1]*matA[11]*matA[14] + matA[9]*matA[2]*matA[15]
		- matA[9]*matA[3]*matA[14] - matA[13]*matA[2]*matA[11] + matA[13]*matA[3]*matA[10];
	matB[5] =	matA[0]*matA[10]*matA[15] - matA[0]*matA[11]*matA[14] - matA[8]*matA[2]*matA[15]
		+ matA[8]*matA[3]*matA[14] + matA[12]*matA[2]*matA[11] - matA[12]*matA[3]*matA[10];
	matB[9] =  -matA[0]*matA[9]*matA[15] + matA[0]*matA[11]*matA[13] + matA[8]*matA[1]*matA[15]
		- matA[8]*matA[3]*matA[13] - matA[12]*matA[1]*matA[11] + matA[12]*matA[3]*matA[9];
	matB[13] =	matA[0]*matA[9]*matA[14] - matA[0]*matA[10]*matA[13] - matA[8]*matA[1]*matA[14]
		+ matA[8]*matA[2]*matA[13] + matA[12]*matA[1]*matA[10] - matA[12]*matA[2]*matA[9];
	matB[2] =	matA[1]*matA[6]*matA[15] - matA[1]*matA[7]*matA[14] - matA[5]*matA[2]*matA[15]
		+ matA[5]*matA[3]*matA[14] + matA[13]*matA[2]*matA[7] - matA[13]*matA[3]*matA[6];
	matB[6] =  -matA[0]*matA[6]*matA[15] + matA[0]*matA[7]*matA[14] + matA[4]*matA[2]*matA[15]
		- matA[4]*matA[3]*matA[14] - matA[12]*matA[2]*matA[7] + matA[12]*matA[3]*matA[6];
	matB[10] =	matA[0]*matA[5]*matA[15] - matA[0]*matA[7]*matA[13] - matA[4]*matA[1]*matA[15]
		+ matA[4]*matA[3]*matA[13] + matA[12]*matA[1]*matA[7] - matA[12]*matA[3]*matA[5];
	matB[14] = -matA[0]*matA[5]*matA[14] + matA[0]*matA[6]*matA[13] + matA[4]*matA[1]*matA[14]
		- matA[4]*matA[2]*matA[13] - matA[12]*matA[1]*matA[6] + matA[12]*matA[2]*matA[5];
	matB[3] =  -matA[1]*matA[6]*matA[11] + matA[1]*matA[7]*matA[10] + matA[5]*matA[2]*matA[11]
		- matA[5]*matA[3]*matA[10] - matA[9]*matA[2]*matA[7] + matA[9]*matA[3]*matA[6];
	matB[7] =	matA[0]*matA[6]*matA[11] - matA[0]*matA[7]*matA[10] - matA[4]*matA[2]*matA[11]
		+ matA[4]*matA[3]*matA[10] + matA[8]*matA[2]*matA[7] - matA[8]*matA[3]*matA[6];
	matB[11] = -matA[0]*matA[5]*matA[11] + matA[0]*matA[7]*matA[9] + matA[4]*matA[1]*matA[11]
		- matA[4]*matA[3]*matA[9] - matA[8]*matA[1]*matA[7] + matA[8]*matA[3]*matA[5];
	matB[15] =	matA[0]*matA[5]*matA[10] - matA[0]*matA[6]*matA[9] - matA[4]*matA[1]*matA[10]
		+ matA[4]*matA[2]*matA[9] + matA[8]*matA[1]*matA[6] - matA[8]*matA[2]*matA[5];

	det = matA[0]*matB[0] + matA[1]*matB[4] + matA[2]*matB[8] + matA[3]*matB[12];
	for (unsigned int i = 0; i < 16; ++i)
		matB[i] = matB[i] / det;
}

void mat4fInverse(const float * __restrict__ matA, float * __restrict__ matB)
{
    float d00, d01, d02, d03;
    float d10, d11, d12, d13;
    float d20, d21, d22, d23;
    float d30, d31, d32, d33;
    float m00, m01, m02, m03;
    float m10, m11, m12, m13;
    float m20, m21, m22, m23;
    float m30, m31, m32, m33;
    float D;

    m00 = matA[0];
    m01 = matA[1];
    m02 = matA[2];
    m03 = matA[3];
    m10 = matA[4];
    m11 = matA[5];
    m12 = matA[6];
    m13 = matA[7];
    m20 = matA[8];
    m21 = matA[9];
    m22 = matA[10];
    m23 = matA[11];
    m30 = matA[12];
    m31 = matA[13];
    m32 = matA[14];
    m33 = matA[15];

    d00 = m11*m22*m33 + m12*m23*m31 + m13*m21*m32 - m31*m22*m13 - m32*m23*m11 - m33*m21*m12;
    d01 = m10*m22*m33 + m12*m23*m30 + m13*m20*m32 - m30*m22*m13 - m32*m23*m10 - m33*m20*m12;
    d02 = m10*m21*m33 + m11*m23*m30 + m13*m20*m31 - m30*m21*m13 - m31*m23*m10 - m33*m20*m11;
    d03 = m10*m21*m32 + m11*m22*m30 + m12*m20*m31 - m30*m21*m12 - m31*m22*m10 - m32*m20*m11;

    d10 = m01*m22*m33 + m02*m23*m31 + m03*m21*m32 - m31*m22*m03 - m32*m23*m01 - m33*m21*m02;
    d11 = m00*m22*m33 + m02*m23*m30 + m03*m20*m32 - m30*m22*m03 - m32*m23*m00 - m33*m20*m02;
    d12 = m00*m21*m33 + m01*m23*m30 + m03*m20*m31 - m30*m21*m03 - m31*m23*m00 - m33*m20*m01;
    d13 = m00*m21*m32 + m01*m22*m30 + m02*m20*m31 - m30*m21*m02 - m31*m22*m00 - m32*m20*m01;

    d20 = m01*m12*m33 + m02*m13*m31 + m03*m11*m32 - m31*m12*m03 - m32*m13*m01 - m33*m11*m02;
    d21 = m00*m12*m33 + m02*m13*m30 + m03*m10*m32 - m30*m12*m03 - m32*m13*m00 - m33*m10*m02;
    d22 = m00*m11*m33 + m01*m13*m30 + m03*m10*m31 - m30*m11*m03 - m31*m13*m00 - m33*m10*m01;
    d23 = m00*m11*m32 + m01*m12*m30 + m02*m10*m31 - m30*m11*m02 - m31*m12*m00 - m32*m10*m01;

    d30 = m01*m12*m23 + m02*m13*m21 + m03*m11*m22 - m21*m12*m03 - m22*m13*m01 - m23*m11*m02;
    d31 = m00*m12*m23 + m02*m13*m20 + m03*m10*m22 - m20*m12*m03 - m22*m13*m00 - m23*m10*m02;
    d32 = m00*m11*m23 + m01*m13*m20 + m03*m10*m21 - m20*m11*m03 - m21*m13*m00 - m23*m10*m01;
    d33 = m00*m11*m22 + m01*m12*m20 + m02*m10*m21 - m20*m11*m02 - m21*m12*m00 - m22*m10*m01;

    D = m00*d00 - m01*d01 + m02*d02 - m03*d03;

    matB[0] =  d00/D;
    matB[1] = -d10/D;
    matB[2] =  d20/D;
    matB[3] = -d30/D;
    matB[4] = -d01/D;
    matB[5] =  d11/D;
    matB[6] = -d21/D;
    matB[7] =  d31/D;
    matB[8] =  d02/D;
    matB[9] = -d12/D;
    matB[10] =  d22/D;
    matB[11] = -d32/D;
    matB[12] = -d03/D;
    matB[13] =  d13/D;
    matB[14] = -d23/D;
    matB[15] =  d33/D;
}

void mat4fRMSetCol(float * __restrict__ matA, const float * __restrict__ vecCol, unsigned int col)
{
	matA[col]	 = vecCol[0];
	matA[4+col]	 = vecCol[1];
	matA[8+col]	 = vecCol[2];
	matA[12+col] = vecCol[3];
}

void mat4fRMSetRow(float * __restrict__ matA, const float * __restrict__ vecRow, unsigned int row)
{
	matA[row * 4]	 = vecRow[0];
	matA[row * 4 + 1]	 = vecRow[1];
	matA[row * 4 + 2]	 = vecRow[2];
	matA[row * 4 + 3] = vecRow[3];
}

void mat4fCMSetRow(float * __restrict__ matA, const float * __restrict__ vecRow, unsigned int row)
{
	matA[row]	 = vecRow[0];
	matA[4+row]	 = vecRow[1];
	matA[8+row]	 = vecRow[2];
	matA[12+row] = vecRow[3];
}

void mat4fCMSetCol(float * __restrict__ matA, const float * __restrict__ vecCol, unsigned int col)
{
	matA[col * 4]	 = vecCol[0];
	matA[col * 4 + 1]	 = vecCol[1];
	matA[col * 4 + 2]	 = vecCol[2];
	matA[col * 4 + 3] = vecCol[3];
}


void mat4fCopy(float * __restrict__ matD, const float * __restrict__ matS)
{
	matD[0] = matS[0]; matD[1] = matS[1]; matD[2] = matS[2]; matD[3] = matS[3];
	matD[4] = matS[4]; matD[5] = matS[5]; matD[6] = matS[6]; matD[7] = matS[7];
	matD[8] = matS[8]; matD[9] = matS[9]; matD[10] = matS[10]; matD[11] = matS[11];
	matD[12] = matS[12]; matD[13] = matS[13]; matD[14] = matS[14]; matD[15] = matS[15];
}

void mat4fMul(const float * __restrict__ matA, const float * __restrict__ matB, float * __restrict__ matC)
{
	matC[0]	 = matA[0]*matB[0]	+ matA[1]*matB[4]  + matA[2]*matB[8]   + matA[3]*matB[12];
	matC[1]	 = matA[0]*matB[1]	+ matA[1]*matB[5]  + matA[2]*matB[9]   + matA[3]*matB[13];
	matC[2]	 = matA[0]*matB[2]	+ matA[1]*matB[6]  + matA[2]*matB[10] + matA[3]*matB[14];
	matC[3]	 = matA[0]*matB[3] + matA[1]*matB[7] + matA[2]*matB[11] + matA[3]*matB[15];

	matC[4]	 = matA[4]*matB[0]	+ matA[5]*matB[4]  + matA[6]*matB[8]   + matA[7]*matB[12];
	matC[5]	 = matA[4]*matB[1]	+ matA[5]*matB[5]  + matA[6]*matB[9]   + matA[7]*matB[13];
	matC[6]	 = matA[4]*matB[2]	+ matA[5]*matB[6]  + matA[6]*matB[10] + matA[7]*matB[14];
	matC[7]	 = matA[4]*matB[3] + matA[5]*matB[7] + matA[6]*matB[11] + matA[7]*matB[15];

	matC[8]	 = matA[8]*matB[0]	+ matA[9]*matB[4]  + matA[10]*matB[8]	+ matA[11]*matB[12];
	matC[9]	 = matA[8]*matB[1]	+ matA[9]*matB[5]  + matA[10]*matB[9]	+ matA[11]*matB[13];
	matC[10]  = matA[8]*matB[2]	 + matA[9]*matB[6]	+ matA[10]*matB[10] + matA[11]*matB[14];
	matC[11]  = matA[8]*matB[3] + matA[9]*matB[7] + matA[10]*matB[11] + matA[11]*matB[15];

	matC[12]  = matA[12]*matB[0]  + matA[13]*matB[4]  + matA[14]*matB[8]   + matA[15]*matB[12];
	matC[13]  = matA[12]*matB[1]  + matA[13]*matB[5]  + matA[14]*matB[9]   + matA[15]*matB[13];
	matC[14]  = matA[12]*matB[2]  + matA[13]*matB[6]  + matA[14]*matB[10] + matA[15]*matB[14];
	matC[15]  = matA[12]*matB[3] + matA[13]*matB[7] + matA[14]*matB[11] + matA[15]*matB[15];
}

void mat4fMulV(const float * __restrict__ matA, const float * __restrict__ vecA, float * __restrict__ vecB)
{
	vecB[0]	 = matA[0]*vecA[0]	+ matA[1]*vecA[1]  + matA[2]*vecA[2]   + matA[3]*vecA[3];
	vecB[1]	 = matA[4]*vecA[0]	+ matA[5]*vecA[1]  + matA[6]*vecA[2]   + matA[7]*vecA[3];
	vecB[2]	 = matA[8]*vecA[0]	+ matA[9]*vecA[1]  + matA[10]*vecA[2]	+ matA[11]*vecA[3];
	vecB[3]	 = matA[12]*vecA[0]	 + matA[13]*vecA[1]	 + matA[14]*vecA[2]	  + matA[15]*vecA[3];
}

void mat4fMulV3(const float * __restrict__ matA, const float * __restrict__ vecA, float * __restrict__ vecB)
{
	vecB[0]	 = matA[0]*vecA[0]	+ matA[1]*vecA[1]  + matA[2]*vecA[2]   + matA[3];
	vecB[1]	 = matA[4]*vecA[0]	+ matA[5]*vecA[1]  + matA[6]*vecA[2]   + matA[7];
	vecB[2]	 = matA[8]*vecA[0]	+ matA[9]*vecA[1]  + matA[10]*vecA[2]	+ matA[11];
	float w	 = matA[12]*vecA[0]	 + matA[13]*vecA[1]	 + matA[14]*vecA[2]	  + matA[15];
	vecB[0] /= w;
	vecB[1] /= w;
	vecB[2] /= w;
}

std::string mat4fToString(const float * matA)
{
	std::ostringstream oss;
	for (unsigned int i = 0; i < 16; ++i)
	{
		oss << matA[i];
		if ((i+1)%4)
			oss << " , ";
		else
			oss << std::endl;
	}
	return oss.str();
}
std::string mat4dToString(const double * matA)
{
	std::ostringstream oss;
	for (unsigned int i = 0; i < 16; ++i)
	{
		oss << matA[i];
		if ((i+1)%4)
			oss << " , ";
		else
			oss << std::endl;
	}
	return oss.str();
}
void vec4fCopy(float * __restrict__ vecD, const float * __restrict__ vecS)
{
	vecD[0] = vecS[0];
	vecD[1] = vecS[1];
	vecD[2] = vecS[2];
	vecD[3] = vecS[3];
}


void vec4fAdd(const float * __restrict__ vecA, const float * __restrict__ vecB, float * __restrict__ vecC)
{
	vecC[0] = vecA[0] + vecB[0];
	vecC[1] = vecA[1] + vecB[1];
	vecC[2] = vecA[2] + vecB[2];
	vecC[3] = vecA[3] + vecB[3];
}

void vec4fSub(const float * __restrict__ vecA, const float * __restrict__ vecB, float * __restrict__ vecC)
{
	vecC[0] = vecA[0] - vecB[0];
	vecC[1] = vecA[1] - vecB[1];
	vecC[2] = vecA[2] - vecB[2];
	vecC[3] = vecA[3] - vecB[3];
}

void vec4fCCart(float * vecA)
{
	vecA[0] = vecA[0] / vecA[3];
	vecA[1] = vecA[1] / vecA[3];
	vecA[2] = vecA[2] / vecA[3];
	vecA[3] = 1.f;
}

float vec3fNorm(const float * vecA)
{
	float n1 = vecA[0] * vecA[0];
	float n2 = vecA[1] * vecA[1];
	float n3 = vecA[2] * vecA[2];
	return sqrt(n1 + n2 + n3);
}

void vec3fAdd(const float * __restrict__ vecA, const float * __restrict__ vecB, float * __restrict__ vecC)
{
	vecC[0] = vecA[0] + vecB[0];
	vecC[1] = vecA[1] + vecB[1];
	vecC[2] = vecA[2] + vecB[2];
}

void vec3fSub(const float * __restrict__ vecA, const float * __restrict__ vecB, float * __restrict__ vecC)
{
	vecC[0] = vecA[0] - vecB[0];
	vecC[1] = vecA[1] - vecB[1];
	vecC[2] = vecA[2] - vecB[2];
}

void vec3fNormalize(float * vecA, float norm)
{
	vecA[0] /= norm;
	vecA[1] /= norm;
	vecA[2] /= norm;
}

void vec3fScale(float * vecA, float s)
{
	vecA[0] *= s;
	vecA[1] *= s;
	vecA[2] *= s;
}

std::string vec4fToString(const float * vec)
{
	std::ostringstream oss;
	oss << vec[0] << " , "
		<< vec[1] << " , "
		<< vec[2] << " , "
		<< vec[3];
	return oss.str();
}

std::string vec4dToString(const double * vec)
{
	std::ostringstream oss;
	oss << vec[0] << " , "
		<< vec[1] << " , "
		<< vec[2] << " , "
		<< vec[3];
	return oss.str();
}
void vec3fCopy(float * __restrict__ vecD, const float * __restrict__ vecS)
{
	vecD[0] = vecS[0];
	vecD[1] = vecS[1];
	vecD[2] = vecS[2];
}

void vec3fCross(const float * __restrict__ vecA, const float * __restrict__ vecB, float * __restrict__ vecC)
{
	vecC[0] = vecA[1] * vecB[2] - vecA[2] * vecB[1];
	vecC[1] = vecA[2] * vecB[0] - vecA[0] * vecB[2];
	vecC[2] = vecA[0] * vecB[1] - vecA[1] * vecB[0];
}

float vec3fDot(const float * __restrict__ vecA, const float * __restrict__ vecB)
{
	return vecA[0] * vecB[0] + vecA[1] * vecB[1] + vecA[2] * vecB[2];
}

void vecnfLerp(const float * __restrict__ vecA, const float * __restrict__ vecB, float * __restrict__ vecC,
			   unsigned int size, float ratio)
{
	for (unsigned int i = 0; i < size; ++i)
	{
		vecC[i] = ratio * vecA[i] + (1.f-ratio) * vecB[i];
	}
}

void barycentricToCart(const float * __restrict__ a,
					   const float * __restrict__ b,
					   const float * __restrict__ c,
					   const float * __restrict__ bc,
					   float * __restrict__ coord)
{
	float pA[4];
	vec4fCopy(pA, a);
	vec3fScale(pA, bc[0]);
	float pB[4];
	vec4fCopy(pB, b);
	vec3fScale(pB, bc[1]);
	float pC[4];
	vec4fCopy(pC, c);
	vec3fScale(pC, bc[2]);
	float pAB[4];
	vec3fAdd(pA, pB, pAB);
	vec3fAdd(pAB, pC, coord);
	coord[3] = 1.f;
}

void vectorizeMatrix(const float in[][4], float* out)
{
	for(unsigned int u=0; u<4; ++u)
		for(unsigned int v=0; v<4; ++v)
			out[u*4 + v] = in[u][v];
}
