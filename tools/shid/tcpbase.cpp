#include <event2/bufferevent.h>
#include <event2/event.h>
#include <event2/listener.h>

#include <thread>
#include <algorithm>
#include <cassert>

#include "lz4.h"


#include "tcpbase.h"
#include "tcpwindowsize.h"

#define baseSize 4096
#define minCompressedSize 1024

void TCPOutBuffer::deallocate()
{
    if(buffer)
    {
        delete [] buffer;
        buffer = NULL;
    }
    size = 0;
}

void TCPOutBuffer::compress()
{
    if(size < minCompressedSize)
        return;
    DataPtr compressedBuffer = new char[LZ4_COMPRESSBOUND(size - sizeof(packet_size_t)) +  2 * sizeof(packet_size_t)];

    packet_size_t newSize = LZ4_compress(buffer + sizeof(packet_size_t), compressedBuffer + 2 * sizeof(packet_size_t), size - sizeof(packet_size_t));
    if(newSize < (size - (2*sizeof(packet_size_t))) )
    {
        size = newSize;
        size += 2*sizeof(packet_size_t);
        ((packet_size_t*)compressedBuffer)[0] = ( size |  0x80000000 );
        ((packet_size_t*)compressedBuffer)[1] = ((packet_size_t*)buffer)[0];

        delete [] buffer;
        buffer = compressedBuffer;
    }
    else
    {
        delete [] compressedBuffer;
    }

}

void TCPInBuffer::allocate()
{
    packet_size_t wantedSize = m_inExpectedSize;
    packet_size_t mask = ~(baseSize - 1);
    if( (wantedSize & mask) != wantedSize)
        wantedSize = (wantedSize & mask) + baseSize;

    if(wantedSize == 0)
        wantedSize = baseSize;

    if(m_inCurrentIndex + baseSize > wantedSize)
        wantedSize += baseSize;

    if(m_inBufferSize < wantedSize)
    {
        DataPtr newBuffer = new DataType[wantedSize]; 
        if(m_inCurrentSize)
            memcpy(newBuffer, m_inBuffer + m_inStartIndex, m_inCurrentSize);
        m_inStartIndex = 0;
        delete [] m_inBuffer;
        m_inBuffer = newBuffer;
        m_inBufferSize = wantedSize;
        m_inCurrentIndex = m_inCurrentSize;
    }
}

void TCPInBuffer::deallocate()
{
    if(m_inBuffer)
    {
        delete [] m_inBuffer;
        m_inBuffer = NULL;
        m_inBufferSize = 0;

        m_inCurrentSize = 0;
        m_inExpectedSize = 0;            
        m_inStartIndex = 0;
        m_inCurrentIndex = 0;
    }

    delete [] m_inUncompressedBuffer;
    m_inUncompressedBuffer = NULL;
    m_inUncompressedBufferSize = 0;
}


int TCPBase::s_currentBufferListId = 0;

TCPBase::TCPBase() :  
m_thread(NULL), 
m_threadRunning(false), 
m_finished(false),
m_compress(true)
{
    staticInit();
    m_eventBase = event_base_new();
}

TCPBase::~TCPBase()
{
    stopThread();    
    event_base_free(m_eventBase);

    clearSentBufferPool();
    clearPendingBufferPool();
    finalize();
}

void TCPBase::stopThread()
{
    if(!m_thread)
        return;
    m_finished = true;
    if(m_thread)
    {
        m_thread->join();
        delete m_thread;
        m_thread = NULL;
    }
}

void TCPBase::staticInit()
{
    // check endian type
#ifdef _DEBUG
    uint64_t myInt = 0x1122334455667788L;
    unsigned char* myIntPtr = (unsigned char*)&myInt;
    assert(myIntPtr[0] == 0x88);
    assert(myIntPtr[1] == 0x77);
    assert(myIntPtr[2] == 0x66);
    assert(myIntPtr[3] == 0x55);
    assert(myIntPtr[4] == 0x44);
    assert(myIntPtr[5] == 0x33);
    assert(myIntPtr[6] == 0x22);
    assert(myIntPtr[7] == 0x11);
#endif

#ifdef _MSC_VER
    WORD wVersionRequested;
    WSADATA wsaData;
    int wsaerr;
    wVersionRequested = MAKEWORD(2, 2);
    wsaerr = WSAStartup(wVersionRequested, &wsaData);
    if(wsaerr != 0)
        return;
#endif
}

void TCPBase::finalize()
{
#ifdef _MSC_VER
    WSACleanup();
#endif
}

bufferevent* TCPBase::createBufferEventSocket(evutil_socket_t fd) const
{
    setsock_tcp_windowsize((int)fd, 0xFFFF, 1);
    return bufferevent_socket_new(m_eventBase, fd, BEV_OPT_CLOSE_ON_FREE);
}

void TCPBase::deleteBufferEventSocket(bufferevent * bev)
{
    clearSentBufferPool(bev);
    clearPendingBufferPool(bev);
    bufferevent_free(bev);
}

evconnlistener * TCPBase::createListener(evutil_socket_t fd, TCPClientConnectCallback cb, void * arg) const
{
    return evconnlistener_new(m_eventBase, cb, arg, LEV_OPT_CLOSE_ON_FREE|LEV_OPT_REUSEABLE, -1, fd);
}

void TCPBase::deleteListener(evconnlistener * listener)
{
    evconnlistener_free(listener);
}

namespace {
    struct BufferEventCmp
    {
        const bufferevent* be;
        bool operator() (const TCPOutBuffer & item) const
        {
            return item.bufferEvent != be;
        }
    };
}

void TCPBase::clearBufferPool(OutBufferList * pool, const bufferevent* be)
{
    BufferEventCmp pred = {be};
    OutBufferList::iterator newEnd = std::partition(pool->begin(), pool->end(), pred);

    for(OutBufferList::iterator it = newEnd ; it != pool->end() ; ++it)
    {
        it->deallocate();
    }
    pool->erase(newEnd, pool->end());
}

void TCPBase::clearSentBufferPool( const bufferevent* be )
{
    m_mutex.lock();
    clearBufferPool(&m_sentBuffers, be);
    m_mutex.unlock();
}

void TCPBase::clearPendingBufferPool(const bufferevent* be)
{
    m_mutex.lock();
    clearBufferPool(&m_pendingBuffers, be);
    m_mutex.unlock();
}

void TCPBase::clearSentBufferPool()
{
    m_mutex.lock();
    for(OutBufferList::iterator it = m_sentBuffers.begin() ; it != m_sentBuffers.end() ; ++it)
    {
        it->deallocate();
    }
    m_sentBuffers.clear();
    m_mutex.unlock();
}

void TCPBase::clearPendingBufferPool()
{
    m_mutex.lock();
    for(OutBufferList::iterator it = m_pendingBuffers.begin() ; it != m_pendingBuffers.end() ; ++it)
    {
        it->deallocate();
    }
    m_pendingBuffers.clear();
    m_mutex.unlock();
}

void TCPInBuffer::read(struct bufferevent *bev)
{
    bool compressed = false;
    while(true)
    {
        allocate();
        packet_size_t size = (packet_size_t)bufferevent_read(bev, m_inBuffer + m_inCurrentIndex, baseSize);
        if(!size)
            break;
        m_inCurrentSize += size;
        m_inCurrentIndex += size;
        if(m_inCurrentSize >= sizeof(packet_size_t))
        {
            m_inExpectedSize = *(packet_size_t*)(m_inBuffer + m_inStartIndex);
            compressed = (m_inExpectedSize & 0x80000000) != 0;
            m_inExpectedSize &= 0x7FFFFFFF;
        }
        while(m_inExpectedSize && m_inExpectedSize <= m_inCurrentSize)
        {
            if(m_dataReceivedCallback)
            {
                if(compressed)
                {
                    packet_size_t decompressedSize = ((packet_size_t*)(m_inBuffer + m_inStartIndex))[1];
                    if(m_inUncompressedBufferSize < decompressedSize)
                    {
                        delete [] m_inUncompressedBuffer;
                        m_inUncompressedBuffer = new DataType[decompressedSize];
                        m_inUncompressedBufferSize = decompressedSize;
                    }
                    LZ4_decompress_fast(m_inBuffer + m_inStartIndex + 2*sizeof(packet_size_t), m_inUncompressedBuffer + sizeof(packet_size_t), decompressedSize - sizeof(packet_size_t));
                    *(packet_size_t*)m_inUncompressedBuffer = decompressedSize;
                    (*m_dataReceivedCallback)(m_inUncompressedBuffer, decompressedSize, m_senderId);
                }
                else
                {
                    (*m_dataReceivedCallback)(m_inBuffer + m_inStartIndex, m_inExpectedSize, m_senderId);
                }
            }
            m_inCurrentSize -= m_inExpectedSize;
            m_inStartIndex += m_inExpectedSize;            
            m_inExpectedSize = 0;

            if(m_inCurrentSize >= sizeof(packet_size_t))
            {
                m_inExpectedSize = *(packet_size_t*)(m_inBuffer + m_inStartIndex);
                compressed = (m_inExpectedSize & 0x80000000) != 0;
                m_inExpectedSize &= 0x7FFFFFFF;
            }
        }
    }

    if( m_inStartIndex )
    {
        for(packet_size_t i = 0 ; i < m_inCurrentSize ; i++)
            m_inBuffer[i] = m_inBuffer[i + m_inStartIndex];
    }

    m_inCurrentIndex -= m_inStartIndex;
    m_inStartIndex = 0;
}

void TCPBase::flush(struct bufferevent *bev)
{
    unsigned char temp[baseSize];
    size_t size = 0;
    do 
    {
        size = bufferevent_read(bev, temp, baseSize);
    } while (size);
}

void TCPBase::dispatch()
{
    if(m_eventBase)
        event_base_loop(m_eventBase, EVLOOP_NONBLOCK);
}

void TCPBase::startThread()
{
    m_finished = false;
    m_thread = new std::thread(std::ref(*this));
    waitThreadStarted();
}

void TCPBase::waitThreadStarted()
{
    while(!m_threadRunning)
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
}

// thread loop
void TCPBase::operator()()
{
    m_threadRunning = true;
    while(!m_finished)
    {
        dispatch();
        if(!m_pendingBuffers.empty())
        {
            m_mutex.lock();
            if(!m_pendingBuffers.empty())
            {
                for(OutBufferList::iterator it = m_pendingBuffers.begin() ; it != m_pendingBuffers.end() ; ++it)
                {
                    TCPOutBuffer& buffer = *it;
                    if(buffer.bufferEvent)
                    {
                        if(useCompression())
                            buffer.compress();

                        bufferevent_write(buffer.bufferEvent, buffer.buffer, buffer.size);
                        m_sentBuffers.push_back(buffer);
                    }
                    else
                    {
                        buffer.deallocate();
                    }
                }
                m_pendingBuffers.clear();
            }
            m_mutex.unlock();
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }   
    m_threadRunning = false;
}

bool TCPBase::beginFragmentBuffer()
{
    m_fragmentedMutex.lock();
    return true;
}

void TCPBase::sendFragmentBuffer( bufferevent* be, DataPtr data, packet_size_t dataSize )
{
    m_mutex.lock();
    m_pendingBuffers.push_back(TCPOutBuffer(be, data, dataSize, false));
    m_mutex.unlock();
}

void TCPBase::endFragmentBuffer()
{
    m_fragmentedMutex.unlock();
}

bool TCPBase::sendBuffer( bufferevent* be, DataPtr data, bool giveOwnership )
{
    m_fragmentedMutex.lock();

    packet_size_t dataSize = *(packet_size_t*)data;

    DataPtr copyBuffer;
    if(!giveOwnership)
    {
        copyBuffer = new DataType [dataSize];
        memcpy(copyBuffer, data, dataSize);
    }
    else
    {
        copyBuffer = data;
    }

    m_mutex.lock();
    m_pendingBuffers.push_back(TCPOutBuffer(be, copyBuffer, dataSize, giveOwnership));
    m_mutex.unlock();

    m_fragmentedMutex.unlock();

    return true;
}
