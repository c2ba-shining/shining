#ifndef TCPCLIENT
#define TCPCLIENT

#include "tcpbase.h"
#include <list>

struct bufferevent;
struct event_base;

class TCPOutBuffer;

class TCPClient
{
public:
    TCPClient(TCPBase* tcpBase);
    ~TCPClient();    

    bool connectToServer(const char* hostname, int port);
    bool sendBuffer( DataPtr data, bool giveOwnership = false );
    bool isConnected() const { return m_connected == Connection_succeed; }

    void setId(ConnectedId id) { m_inBuffer.setSenderId(id); }
    ConnectedId id() const { return m_inBuffer.senderId(); }

    // callbacks
    void setDataReceivedCallback(TCPDataReceivedCallback callback) { m_dataReceivedCallback = callback; }
    void setConnectedCallback(TCPConnectedCallback callback) { m_connectedCallback = callback; }
    void setDisconnectedCallback(TCPDisconnectedCallback callback) { m_disconnectedCallback = callback; }


    bool beginFragmentBuffer();
    void sendFragmentBuffer( DataPtr data, packet_size_t dataSize );
    void endFragmentBuffer();

    void readCallback(struct bufferevent *bev);
    void writeCallback(struct bufferevent *bev);
    void eventCallback(struct bufferevent *bev, short events);

private:
    bufferevent* bufferEventFromClientId(ConnectedId clientId);

private:
    enum
    {
        Connection_pending,
        Connection_failed,
        Connection_succeed,
    };

    bufferevent* m_bufferEvent;
    TCPInBuffer m_inBuffer;
    int m_connected;
    TCPBase* m_tcpBase;

    TCPDataReceivedCallback m_dataReceivedCallback;
    TCPConnectedCallback m_connectedCallback;
    TCPDisconnectedCallback m_disconnectedCallback;
};

#endif
