#include "tcpclient.h"

#include <event2/event.h>
#include <event2/listener.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/bufferevent_struct.h>

#include <iostream>

TCPClient::TCPClient(TCPBase* tcpBase) :
    m_tcpBase(tcpBase),
    m_bufferEvent(NULL),
    m_connected(Connection_pending),
    m_dataReceivedCallback(NULL),
    m_connectedCallback(NULL),
    m_disconnectedCallback(NULL)
{ }

TCPClient::~TCPClient()
{
    // terminate buffer event
    if(m_bufferEvent)
        bufferevent_free(m_bufferEvent); 
}

void TCPClient::readCallback( struct bufferevent *bev )
{
    m_inBuffer.read(bev);
}

void TCPClient::writeCallback( struct bufferevent *bev )
{
    m_tcpBase->clearSentBufferPool(bev);
}

void TCPClient::eventCallback( struct bufferevent *bev, short events )
{
    if (events & BEV_EVENT_CONNECTED)
    {
        m_connected = Connection_succeed; 
        if(m_connectedCallback)
            m_connectedCallback(0);
    }

    if( (events & BEV_EVENT_ERROR) || (events & BEV_EVENT_EOF) )
    {
        // disconnected
        if(isConnected() && m_disconnectedCallback)
            m_disconnectedCallback(0);

        if(m_bufferEvent)
        {
            m_tcpBase->deleteBufferEventSocket(m_bufferEvent); 
            m_bufferEvent = NULL;
        }
        m_inBuffer.deallocate();

        m_connected = Connection_failed;
    }
}

static void _readCallback( struct bufferevent *bev, void *ctx )
{
    TCPClient* tcpObject = static_cast<TCPClient*>(ctx);
    tcpObject->readCallback(bev);    
}

static void _writeCallback( struct bufferevent *bev, void *ctx )
{
    TCPClient* tcpObject = static_cast<TCPClient*>(ctx);
    tcpObject->writeCallback(bev);    
}

static void _eventCallback( struct bufferevent *bev, short events, void *ctx )
{
    TCPClient* tcpObject = static_cast<TCPClient*>(ctx);
    tcpObject->eventCallback(bev, events);
}

bool TCPClient::connectToServer( const char* hostname, int port )
{
    if(isConnected())
        return true;

    m_connected = Connection_pending;

    m_inBuffer.setDataReceivedCallback(m_dataReceivedCallback);
    m_inBuffer.setSenderId(0);

    evutil_socket_t fd = socket(AF_INET, SOCK_STREAM, 0);
    m_bufferEvent = m_tcpBase->createBufferEventSocket(fd);
    bufferevent_setcb(m_bufferEvent, &_readCallback, &_writeCallback, &_eventCallback, this);
    bufferevent_enable(m_bufferEvent, EV_WRITE | EV_READ);

    if (bufferevent_socket_connect_hostname(m_bufferEvent, NULL, AF_INET, hostname, port) < 0)
        return false;

    while(m_connected == Connection_pending)
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

    return (m_connected == Connection_succeed);
}

bool TCPClient::sendBuffer( DataPtr data, bool giveOwnership )
{
    if(!isConnected())
        return false;
    return m_tcpBase->sendBuffer(m_bufferEvent, data, giveOwnership);
}

bufferevent* TCPClient::bufferEventFromClientId(ConnectedId clientId)
{
    return m_bufferEvent;
}

bool TCPClient::beginFragmentBuffer()
{
    if(!isConnected())
        return false;
    return m_tcpBase->beginFragmentBuffer();
}

void TCPClient::sendFragmentBuffer( DataPtr data, packet_size_t dataSize )
{
    if(!isConnected())
        return;

    m_tcpBase->sendFragmentBuffer(m_bufferEvent, data, dataSize);
}

void TCPClient::endFragmentBuffer()
{
    if(!isConnected())
        return;

    m_tcpBase->endFragmentBuffer();
}
